package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Dimension;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaFunction;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaPathFunction;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaReductionOperation;
import org.pickcellslab.foundationj.dbm.meta.MetaStrategy;
import org.pickcellslab.foundationj.dbm.meta.MetaTraversal;
import org.pickcellslab.foundationj.dbm.meta.MetaTraversingFunction;
import org.pickcellslab.foundationj.dbm.meta.PredictableMetaSplit;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.ui.wizard.WizardLeaf;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class MetaDescriptionPanel<E> extends WizardLeaf<MetaModel> {


	private JFormattedTextField namePane;
	private JTextPane  descPane;
	private ExplicitFunction<?,E> function;


	private ExplicitPredicate<?> predicate;
	private final TraversalDefinition trav;

	private final MetaQueryable target;
	private boolean isValid = false;

	private final MetaType type;
	private final int length;
	private MetaTraversal mt;
	private PredictableMetaSplit ms;
	private MetaReductionOperation mmf;
	private MetaFilter mf;
	private MetaClass generator;
	
	private final MetaInstanceDirector director;


	//private final Preferences prefs = Preferences.userRoot();

	private enum MetaType{
		FUNCTION, MTF, SGF, FILTER, TRAVERSAL, CATEGORY
	}


	public  MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, ExplicitFunction<?,E> function, int length, MetaQueryable target){
		super(parent);
		Objects.requireNonNull(function);
		Objects.requireNonNull(target);
		Objects.requireNonNull(director);
		this.director = director;

		this.function = function;
		this.target = target;

		this.type = MetaType.FUNCTION;

		this.predicate = null;
		this.trav = null;
		this.length = length;

		buildPanel(function.description());

	}





	public MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, ExplicitPredicate<?> current, MetaQueryable target) {
		super(parent);
		Objects.requireNonNull(current);
		Objects.requireNonNull(target);
		Objects.requireNonNull(director);
		this.director = director;

		this.predicate = current;
		this.target = target;

		this.function = null;
		this.trav = null;
		this.type = MetaType.FILTER;
		this.length = -1;

		buildPanel(current.description());
	}


	public MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, TraversalDefinition trav, MetaQueryable target) {
		super(parent);

		Objects.requireNonNull(trav);
		Objects.requireNonNull(target);
		Objects.requireNonNull(director);
		this.director = director;

		this.trav = trav;
		this.target = target;

		this.predicate = null;		
		this.function = null;
		this.length = -1;
				
		this.type = MetaType.TRAVERSAL;

		buildPanel(trav.description());

	}




	public MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, Collection<MetaStrategy> values) {
		super(parent);

		Objects.requireNonNull(values);
		Objects.requireNonNull(director);
		this.director = director;
		
		this.type = MetaType.CATEGORY;

		this.target = null;
		this.function = null;
		this.predicate = null;
		this.trav = null;
		this.length = -1;

		String defaultDescription = "<HTML><b>Categorical dataset: </b>";
		for(MetaStrategy k : values)
			defaultDescription += "<br> "+k.toHTML();
		defaultDescription += "</HTML>";

		buildPanel(defaultDescription);
	}


	public MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, MetaReadable<?> variable, int dim) {
		super(parent);

		Objects.requireNonNull(variable);
		Objects.requireNonNull(director);
		this.director = director;
		
		this.length = dim;

		this.type = MetaType.CATEGORY;
		this.target = null;
		this.function = null;
		this.predicate = null;
		this.trav = null;


		buildPanel(variable.toHTML());
	}





	public  MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, MetaTraversal mt, PredictableMetaSplit ms, MetaReductionOperation mmf,  MetaQueryable target){
		super(parent);

		Objects.requireNonNull(mt);
		Objects.requireNonNull(mmf);
		Objects.requireNonNull(target);
		Objects.requireNonNull(director);
		this.director = director;
		
		this.function = null;

		this.predicate = null;
		this.trav = null;
		this.length = -1;
		
		this.mt = mt;
		this.ms = ms;
		this.mmf = mmf;
		this.type = MetaType.MTF;
		this.target = target;

		buildPanel(mt.description()+" "+mmf.description()+ (ms == null ? "" : " "+ms.toString()));

	}



	public  MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, MetaClass generator, MetaReductionOperation mmf,  MetaQueryable target){
		super(parent);
		Objects.requireNonNull(generator);
		Objects.requireNonNull(mmf);
		Objects.requireNonNull(target);
		Objects.requireNonNull(director);
		this.director = director;
		
		this.function = null;

		this.predicate = null;
		this.trav = null;
		this.length = -1;		
		this.mt = null;		
		this.ms = null;

		this.mmf = mmf;
		this.type = MetaType.SGF;
		this.generator = generator;
		this.target = target;

		buildPanel(generator.name()+" "+mmf.description()+" on "+target.name());

	}


	public  MetaDescriptionPanel(WizardNode parent, MetaInstanceDirector director, MetaClass generator, MetaFilter mf, MetaReductionOperation mmf,  MetaQueryable target){
		super(parent);
		Objects.requireNonNull(generator);
		Objects.requireNonNull(mmf);
		Objects.requireNonNull(target);
		Objects.requireNonNull(director);
		this.director = director;
		
		this.function = null;

		this.predicate = null;
		this.trav = null;
		this.length = -1;		
		this.mt = null;		
		this.ms = null;

		this.mmf = mmf;
		this.mf = mf;
		this.type = MetaType.SGF;
		this.generator = generator;
		this.target = target;

		buildPanel(generator.name()+" "+mmf.description()+" on "+target.name());

	}






	private void buildPanel(String defaultDescription) {



		JLabel lblName = new JLabel("Name");

		namePane = new JFormattedTextField();
		namePane.addPropertyChangeListener(l->checkValid());		


		JLabel lblShortDescription = new JLabel("Short Description");


		descPane = new JTextPane ();
		descPane.setContentType("text/html");
		descPane.setText(defaultDescription);
		descPane.addPropertyChangeListener(l->checkValid());

		JScrollPane scroll = new JScrollPane(descPane);
		scroll.setPreferredSize(new Dimension(280, 125));




		//Layout
		//---------------------------------------------------------------------------------------------------



		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(scroll, Alignment.LEADING)
								.addComponent(lblShortDescription, Alignment.LEADING)
								.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblName))
										.addGap(85)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(namePane, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(15, Short.MAX_VALUE))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
										.addComponent(namePane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED))
								.addGroup(groupLayout.createSequentialGroup()
										.addGap(3)
										.addComponent(lblName)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGap(3)))
						.addGap(24)
						.addComponent(lblShortDescription)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(scroll, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(22, Short.MAX_VALUE))
				);
		//	groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {namePane});
		setLayout(groupLayout);
		setPreferredSize(new Dimension(320, 200));

	}


	public void setFunction(ExplicitFunction<?,E> f){
		this.function = f;
	}

	public void setPredicate(ExplicitPredicate<?> function2) {
		this.predicate = function2;
		descPane.setText(function2.description());
	}


	private void checkValid(){

		boolean newValidity = 
				!namePane.getText().isEmpty() &&
				!descPane.getText().isEmpty();

		if(isValid!=newValidity){
			isValid = newValidity;
			this.fireNextValidityChanged();
		}		

	}




	@Override
	protected WizardNode getNextStep() {
		return null;
	}

	@Override
	public int numberOfForks() {
		return 0;
	}

	@Override
	public boolean validity() {
		return isValid;
	}



	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Optional<MetaModel> getConstruct() {

		//prefs.put("DEFAULT_CREATOR", creatorPane.getText());
		
		try {
			
			switch(type){
			case FILTER:

				return Optional.of(MetaFilter.getOrCreate(
						director,
						namePane.getText(),
						noHTML(descPane.getText()),
						predicate,
						target
						));		
			case FUNCTION:
				return Optional.of(MetaFunction.getOrCreate(
						director,
						namePane.getText(),
						noHTML(descPane.getText()),
						(ExplicitFunction)function,
						(AKey) AKey.get(namePane.getText(), function.getReturnType()),
						length, 
						target
						));
			case TRAVERSAL:
				return Optional.of(MetaTraversal.getOrCreate(
						director,
						namePane.getText(),
						noHTML(descPane.getText()),
						trav,
						target
						));
			case MTF:
				return Optional.of(MetaTraversingFunction.getOrCreate(
						director,
						namePane.getText(),
						noHTML(descPane.getText()),
						mt,ms,mmf,target
						));
			case SGF:
				return Optional.of(MetaPathFunction.getOrCreate(
						director,
						namePane.getText(),
						noHTML(descPane.getText()),
						generator, mf, mmf, target
						));


			default: return Optional.empty();		
			}


		} catch (NonDataMappingException e) {
			ErrorInfo error = 
					new ErrorInfo(
							"Custom Annotation Error", 
							"Unable to create the custom annotation",
							null, null,
							e,
							Level.SEVERE,
							null);

			JXErrorPane.showDialog(null, error);	
			return Optional.empty();
			
		}catch (InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return Optional.empty();
		}
	}


	private String noHTML(String s){
		return s.replaceAll("\\<.*?>","").replace("\n", "").trim().replaceAll(" +", " ");
	}

		
}

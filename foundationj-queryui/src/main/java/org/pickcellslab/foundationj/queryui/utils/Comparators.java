package org.pickcellslab.foundationj.queryui.utils;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Comparator;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.dimensions.DataTyped;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.ui.utils.AlphanumComparator;

public final class Comparators {

	private Comparators(){/*Cannot be instantiated*/}

	public static <T> Comparator<T> natural(){
		return new AlphanumComparator<>(null);
	}



	public static <T> Comparator<T> natural(Function<T,String> f){
		return new AlphanumComparator<>(f);
	}


	public static Comparator<MetaModel> byMetaType(){
		return new Comparator<MetaModel>(){

			Comparator<DataTyped> keyComp = byType();

			@Override
			public int compare(MetaModel m1, MetaModel m2) {
				if(m1.getClass() == m2.getClass())
					if(DataTyped.class.isAssignableFrom(m1.getClass()))
						return keyComp.compare((DataTyped)m1, (DataTyped)m2);
					else
						return m1.toString().compareTo(m2.toString());
				else
					return m1.getClass().getSimpleName().compareTo(m2.getClass().getSimpleName());						
			}		

			public String toString(){
				return "By Type";
			}

		};			
	}

	public static Comparator<DataTyped> byType(){
		return new Comparator<DataTyped>(){

			@Override
			public int compare(DataTyped m1, DataTyped m2) {
				return m1.dataType().compareTo(m2.dataType());					
			}		

			public String toString(){
				return "By Type";
			}

		};			
	}



	public static Comparator<MetaModel>[] createOptions(){
		@SuppressWarnings("unchecked")
		Comparator<MetaModel>[] comps = new Comparator[2];
		comps[1] = natural();
		comps[0] = byMetaType();
		return comps;
	}
}

package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.pickcellslab.foundationj.datamodel.tools.Decision;

public class NodeFWRenderer implements ListCellRenderer<NodeFilterWrapper>{

	@Override
	public Component getListCellRendererComponent(JList<? extends NodeFilterWrapper> list, NodeFilterWrapper value,
			int index, boolean isSelected, boolean cellHasFocus) {


		JLabel container = new JLabel();


		if(value != null){

			//Decision label
			JLabel modeContainer = new JLabel("                  ");
			modeContainer.setLayout(new BoxLayout(modeContainer, BoxLayout.X_AXIS));

			JLabel inEx = new JLabel();
			JLabel stopCont = new JLabel();

			if(value.decision() == Decision.EXCLUDE_AND_CONTINUE){
				inEx.setIcon(new ImageIcon(getClass().getResource("/icons/Exclude.png")));
				stopCont.setIcon(new ImageIcon(getClass().getResource("/icons/Continue.png")));
			}else if(value.decision()==Decision.INCLUDE_AND_CONTINUE){
				inEx.setIcon(new ImageIcon(getClass().getResource("/icons/Accept.png")));
				stopCont.setIcon(new ImageIcon(getClass().getResource("/icons/Continue.png")));
			}else if(value.decision()==Decision.EXCLUDE_AND_STOP){
				inEx.setIcon(new ImageIcon(getClass().getResource("/icons/Exclude.png")));
				stopCont.setIcon(new ImageIcon(getClass().getResource("/icons/Stop.png")));
			}else if(value.decision()==Decision.INCLUDE_AND_STOP){
				inEx.setIcon(new ImageIcon(getClass().getResource("/icons/Accept.png")));
				stopCont.setIcon(new ImageIcon(getClass().getResource("/icons/Stop.png")));
			}

			modeContainer.add(inEx);
			modeContainer.add(stopCont);


			JLabel typeLabel = new JLabel();
			typeLabel.setIcon(new ImageIcon(getClass().getResource("/icons/Filter.png"))); 


			container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));
			container.add(modeContainer);
			container.add(typeLabel);

			container.setText("                      "+value.filter());
			container.setToolTipText(value.filter().description());

		}else 
			container.setText("None");



		if(isSelected){
			container.setForeground(Color.WHITE);
			container.setBackground(Color.BLACK);
		}else{
			container.setForeground(Color.BLACK);
		}

		return container;

	}

}
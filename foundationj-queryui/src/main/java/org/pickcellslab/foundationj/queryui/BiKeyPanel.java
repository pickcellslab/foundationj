package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Supplier;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;

@SuppressWarnings("serial")
public class BiKeyPanel<T extends Supplier<MetaReadable<?>> & Highlightable>  extends ValidityTogglePanel {

	private MetaReadable<?> key1, key2;
	private JLabel lblNone1, lblNone2;

	public BiKeyPanel(UITheme theme, DataRegistry registry, T supplier1, T supplier2) {

		this.setLayout(new VerticalFlowLayout());
		
		Objects.requireNonNull(supplier1);
 
		
				
		JLabel lblSelectedAttribute = new JLabel("Selected Attribute 1: ");
		lblSelectedAttribute.setHorizontalAlignment(SwingConstants.LEFT);
		
		lblNone1 = new JLabel("None");
		lblNone1.setHorizontalAlignment(SwingConstants.LEFT);
		
		JButton btnChange1 = new JButton("Change");
		
		GroupPanel gp1 = new GroupPanel(lblSelectedAttribute, lblNone1, btnChange1 );
		add(gp1);
		
		btnChange1.addActionListener(l->{

			supplier1.setHighlighted(true);
			final MetaReadable<?> mr = supplier1.get();
			if(mr == null){
				JOptionPane.showMessageDialog(null, "No attributes selected");
				return;
			}
			key1 = mr;
			lblNone1.setText(mr.toString());
			lblNone1.setIcon(MetaToIcon.get(mr, registry, theme, 16));
			this.fireNextValidityChanged();
		});
		
		JLabel lblSelectedAttribute2 = new JLabel("Selected Attribute 2 : ");
		lblSelectedAttribute2.setHorizontalAlignment(SwingConstants.LEFT);
		
		lblNone2 = new JLabel("None");
		lblNone2.setHorizontalAlignment(SwingConstants.LEFT);
		
		JButton btnChange2 = new JButton("Change");
		
		final GroupPanel gp2 = new GroupPanel(lblSelectedAttribute2, lblNone2, btnChange2);
		add(gp2);
		
		btnChange2.addActionListener(l->{

			supplier2.setHighlighted(true);
			final MetaReadable<?> mr = supplier2.get();
			if(mr == null){
				JOptionPane.showMessageDialog(null, "No attributes selected");
				return;
			}
			key2 = mr;
			lblNone2.setText(mr.toString());
			lblNone2.setIcon(MetaToIcon.get(mr, registry, theme, 16));
			this.fireNextValidityChanged();
		});

	}

	@Override
	public boolean validity() {
		return key1!=null && key2!=null;
	}

	
	MetaReadable<?> getChosenReadable1(){
		return key1;
	}
	
	MetaReadable<?> getChosenReadable2(){
		return key2;
	}
	
	

}

package org.pickcellslab.foundationj.ui;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer.Mode;
import org.pickcellslab.foundationj.services.theme.UITheme;

import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.WebCollapsiblePane;

@SuppressWarnings("serial")
public class DimensionDialog<T,D> extends JPanel {

	

	private final LinkedHashMap<String, DimensionChoice<T,D>> choices = new LinkedHashMap<>();

	
	
	public <C extends DimensionContainer<T,D>> DimensionDialog(List<C> readables, String[] dimensions, Mode mode, UITheme theme) {
		this(readables, dimensions, null, mode, theme);
	}
	
	
	/**
	 * @param readables
	 * @param dimensions The names of the dimensions to be defined by the user
	 * @param selected Dimensions initially selected (same order as dimensions)
	 */
	public <C extends DimensionContainer<T,D>> DimensionDialog(List<C> readables, String[] dimensions, Dimension[] selected, Mode mode, UITheme theme) {

		if(readables == null || readables.isEmpty())
			throw new IllegalArgumentException("readables cannot be empty or null");

		if(dimensions == null || dimensions.length == 0)
			throw new IllegalArgumentException("dimensions cannot be empty or null");


		List<Dimension<T, D>> dims = null;

		if(mode == Mode.DECOMPOSED)
			dims = DimensionContainer.getAllDimensions(readables);
		else
			dims = (List)readables.stream().map(r->r.getRaw()).collect(Collectors.toList());

				
		GroupPanel panel = new GroupPanel(4,false);

		int d = 0;
		for(String s : dimensions){
			DimensionChoice<T,D> dc = new DimensionChoice<>(dims, selected[d++], theme);
			WebCollapsiblePane wcp = new WebCollapsiblePane(s, dc);
			panel.add(wcp);
			choices.put(s, dc);
		}	

		JScrollPane scroll = new JScrollPane(panel);

		this.add(scroll);
	}



	public Dimension<T,D> getChoice(String s){
		return choices.get(s).currentDimension();
	}


	public List<Dimension<T,D>> orderedChoice(){
		System.out.println("Number of chosen dims : "+choices.size());
		return choices.values().stream().map(dc->dc.getSelected()).collect(Collectors.toList());
	}



}

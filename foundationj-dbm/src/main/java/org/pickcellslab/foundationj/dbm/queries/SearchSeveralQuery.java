package org.pickcellslab.foundationj.dbm.queries;

import org.pickcellslab.foundationj.dbm.queries.actions.Action;

public interface SearchSeveralQuery<V, E, T, K, O, S extends ResultAccess<K, O>> {

	Action<? super T, O> action();

	String name();

	String shortDescription();

	String searchDescription();

	String actionDescription();

}
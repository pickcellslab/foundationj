package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Objects;
import java.util.function.Supplier;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.slf4j.LoggerFactory;

import com.alee.laf.list.WebListModel;

public class SplitByFilterPanel<T extends Supplier<MetaFilter> & Highlightable> extends SplitterUI<SplitByFilterPanel<T>> {

	private final WebListModel<MetaFilter> model;

	public SplitByFilterPanel(UITheme theme, DataRegistry registry, MetaQueryable mq, T supplier) {
		
		Objects.requireNonNull(mq);
		Objects.requireNonNull(supplier);
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		
		
		JList<MetaFilter> list = new JList<>();
		model = new WebListModel<>();
		list.setModel(model);
		list.setCellRenderer(new MetaRenderer(theme, registry));
		scrollPane.setViewportView(list);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(l->{
			MetaFilter mf = supplier.get();
			if(mf == null){
				JOptionPane.showMessageDialog(this, "No filter selected");
				return;
			}
			//check the list does not already contains the filter
			if(model.contains(mf)){
				JOptionPane.showMessageDialog(this, "This filter is already in the list");
				return;
			}
			
			model.add(mf);
			this.fireNextValidityChanged();			
		});
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(l->{
			MetaFilter mf = list.getSelectedValue();
			if(mf == null){
				JOptionPane.showMessageDialog(this, "No filter selected");
				return;
			}
			model.remove(mf);
			this.fireNextValidityChanged();	
		});
		
		
		setBorder(new TitledBorder(null, "Group "+mq.name()+" by filters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new BorderLayout(0, 0));
			
		
		
		
		JPanel panel = new JPanel();
		
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		add(panel, BorderLayout.NORTH);		
		panel.add(btnAdd);		
		panel.add(btnRemove);
		add(scrollPane);
	
	}

	
	
	
	@Override
	public boolean validity() {
		return model.size() >= 2;
	}

	@Override
	MetaSplit getSplitter(Meta meta) {
		try {
			return MetaSplitByFilter.getOrCreate(meta.getInstanceManager(), model.getElements());
		} catch (InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}

	
	
	
	@Override
	public void copyState(SplitByFilterPanel<T> other) {
		this.model.addElements(other.model.getElements());
		this.fireNextValidityChanged();
	}

}

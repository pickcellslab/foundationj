package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.graph.EdgeFactory;
import org.pickcellslab.foundationj.datamodel.graph.EdgeSetListener;

public class FlowFactory implements EdgeFactory<Factory<Flow>,Flow>{

	
	List<EdgeSetListener<Flow>> lstrs = new ArrayList<>();
	
	@Override
	public void addListener(EdgeSetListener<Flow> l) {
		lstrs.add(l);
	}


	@Override
	public Flow create(Factory<Flow> source, Factory<Flow> target) {
		Flow edge = new Flow(source, target);
		lstrs.forEach(l->l.added(edge));
		return edge;
	}




	

}

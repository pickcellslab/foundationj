package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * An interface defining objects in need of some sort of input which might or might not be present.
 * A {@link Director} declares explicitly what are its requirements.
 * 
 * @param <I> the type of the definitions of input this {@link Director} requires.
 * 
 * @author Guillaume Blin
 *
 */

public interface Director<I> {

	/**
	 * @return The requirements of this {@link Director}
	 */
	public I requirements();
			
	/**
	 * Evaluates the provided contract
	 * @return An estimation of the benefits/cost ratio that the contract will bring to the {@link Director} .
	 * A value >= 1 will indicate a sufficiently high benefits over cost ratio so that the specified contract
	 * will potentially be accepted in the future
	 */
	public double evaluate(I contract);
	
	
	
	
	
}

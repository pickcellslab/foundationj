package org.pickcellslab.foundationj.processors;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.annotations.Scope;



public class SameScopeAsProcessor extends AbstractProcessor {

	@Override
	public Set<String> getSupportedAnnotationTypes(){
		return Collections.singleton(SameScopeAs.class.getName());
	};


	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;
		
		// Find Data elements from the current compilation sources
		for (TypeElement type : (Collection<TypeElement>)roundEnv.getElementsAnnotatedWith(SameScopeAs.class)) {

			// Check that the value is annotated with @SameScopeAs
			SameScopeAs ws = type.getAnnotation(SameScopeAs.class);
			
			try {
				ws.value();
			}catch(MirroredTypeException e) {
				Element tm = processingEnv.getTypeUtils().asElement(e.getTypeMirror());
				if(tm.getAnnotation(Scope.class) == null){
					processingEnv.getMessager().printMessage(Kind.ERROR, "The value of @SameScopeAs must be a "
							+ "class which is annotated with @Scope", type);
				}
			}

		}

		return false;
	}



}

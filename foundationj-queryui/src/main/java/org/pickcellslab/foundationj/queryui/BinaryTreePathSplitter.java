package org.pickcellslab.foundationj.queryui;

import java.util.Iterator;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.CombinedBinary;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="PathGroupingByPredicate")
class BinaryTreePathSplitter<V,E> implements ExplicitPathSplit<V, E, String> {
	
	@Ignored
	private FjAdaptersFactory<V,E> f;
	@Supported		
	private final ExplicitPredicate<? super DataItem> predicate;
	private final boolean isLink;
	private final CombinedBinary<DataItem> root;
	
	

	public BinaryTreePathSplitter(ExplicitPredicate<? super DataItem> predicate, boolean isLink, CombinedBinary<DataItem> root) {
		MappingUtils.exceptionIfNullOrInvalid(predicate);
		this.predicate = predicate;
		this.isLink = isLink;
		this.root = root;
	}

	@Override
	public String description() {
		return "group path by predicate";
	}

	@Override
	public String getKeyFor(Path<V, E> p) {

		if(isLink){
			Iterator<E> ns = p.edges();
			while(ns.hasNext()){
				Link i = f.getLinkReadOnly(ns.next());
				if(predicate.test(i)){
					return CombinedBinary.get(root, i).toString();
				}
			}
		}
		else{
			Iterator<V> ns = p.nodes();
			while(ns.hasNext()){
				NodeItem i = f.getNodeReadOnly(ns.next());
				if(predicate.test(i)){
					return CombinedBinary.get(root, i).toString();
				}
			}
		}
		return ExplicitSplit.OTHER;
	}

	@Override
	public void setFactory(FjAdaptersFactory<V, E> f) {
		this.f = f;
	}

}

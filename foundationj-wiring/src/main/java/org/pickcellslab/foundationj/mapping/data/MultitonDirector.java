package org.pickcellslab.foundationj.mapping.data;

import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

/**
 * 
 * The {@link MultitonDirector} manages <a href=""> Multiton instances</a> albeit in the absence of static methods since
 * data types in foundationj are bound to a {@link Scope} of the application. 
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 * 
 * @see Multiton
 */
public interface MultitonDirector<T> {

	/**
	 * Create an instance based on a {@link DbElement}
	 * @param registry
	 * @param dbObject
	 * @return
	 */
	public T getOrCreate(DataRegistry registry, DbElement dbObject) throws InstantiationHookException;
	
	public <E extends T> E getOrCreate(Class<E> clazz, String uid) throws InstantiationHookException;
	
	public MultitonOutcome<T> getOrCreateWithOutcome(DataRegistry registry, DbElement dbObject) throws InstantiationHookException;
	
	public <E extends T> MultitonOutcome<E> getOrCreateWithOutcome(Class<E> clazz, String uid) throws InstantiationHookException;

	
}

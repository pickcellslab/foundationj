package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * A Factory for {@link DistributionFitter}
 * 
 * @author Guillaume Blin
 *
 * @param <F> The type of DistributionFitter created by this factory
 * @param <D> The data type of the Distribution created by the Fitter
 * @param <O> Options for the fitter
 * @param <R> The type of Distribution return by the Fitter
 */
public interface DistributionFitterFactory<F extends DistributionFitter<D,O,R>, D,O, R extends Distribution<D>> extends FittingFactory<F,List<D>,O,R> {

}

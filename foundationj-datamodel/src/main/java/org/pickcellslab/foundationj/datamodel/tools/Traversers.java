package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;

public class Traversers {

	private static TraverserConstraints noConstraints;


	public static FromDepth<ToDepth<LinkDecisions<TraverserConstraints>>> newConstraints(){
		return new TraverserConstraints.ConstraintsBuilder();
	}


	public static TraverserConstraints doNotConstrain(){
		if(null == noConstraints)
			noConstraints = new TraverserConstraints.ConstraintsBuilder()
			.fromMinDepth().toMaxDepth()
			.traverseAllLinks().includeAllNodes();
		return noConstraints;
	}




	/**
	 * Identifies the disconnected subgraph from a collection of nodes and given some constraints.
	 * @param nodes
	 * @param constraints
	 * @return A List of {@link Traversal} wher each Traversal corresponds to a subgraph given the constraints.
	 */
	public static <C extends Collection<T>, T extends NodeItem> List<DefaultTraversal<NodeItem,Link>> disconnected(C nodes, TraverserConstraints constraints){

		List<DefaultTraversal<NodeItem,Link>> roots = new ArrayList<>();
		Set<NodeItem> remaining = new HashSet<>(nodes);

		while(!remaining.isEmpty()){
			NodeItem r = remaining.iterator().next();
			DefaultTraversal<NodeItem, Link> g =  Traversers.breadthfirst(r, constraints).traverse();
			remaining.removeAll(g.nodes());
			roots.add(g);
		}	

		return roots;
	}


	public static <N extends Node<E>, E extends Edge<? extends N>>
	SimpleBreadthFirst<N,E> breadthFirst(N item) {
		return new SimpleBreadthFirst<>(item);
	}

	public static BreadthFirstWithConstraints breadthfirst(NodeItem item, TraverserConstraints constraints){
		return new BreadthFirstWithConstraints(item, constraints);
	}


	/*
	public static DFTraverser depthFirst(NodeItem item) {
		//TODO Depth first is not yet a Traverser


		return new DFTraverser(item);
	}
	 */


	public static class SimpleBreadthFirst<N extends Node<E>, E extends Edge<? extends N>>
	implements BreadthFirst<N,E>{

		private Deque<N> queue = new LinkedList<N>();
		private Set<N> visitedNodes = new LinkedHashSet<>();    
		private Set<E> visitedLinks = new LinkedHashSet<>();

		private List<E> prevEdges = new ArrayList<>();
		private int nextDepthAt = 1;
		private int nextDepthFinder = 0;
		private int currentDepth = -1;

		private final Supplier<LinkedList<E>> listSupplier = () -> new LinkedList<>();


		private SimpleBreadthFirst(N item) {
			queue.add(item);
			visitedNodes.add(item);
		}

		@Override
		public synchronized TraversalStep<N,E> nextStep(){ 

			final List<N> nodes = new ArrayList<>();
			final List<E> edges = new ArrayList<>();

			while (!queue.isEmpty()) {

				final N node = queue.remove();
				nodes.add(node);


				final LinkedList<E> outList = 
						node.links()
						.filter(l->visitedLinks.add(l))
						.collect(Collectors.toCollection(listSupplier));


				E edge = null;

				while ((edge = outList.poll()) != null) {

					//can be outgoing, incoming or self-ref
					N other = null;
					if(edge.source() == node)
						other = edge.target();
					else if(edge.target() == node)
						other = edge.source();

					if(visitedNodes.add(other)){//if already visited, do not add to queue
						queue.add(other);
						nextDepthFinder++;
					}
					edges.add(edge);

				}

				//Monitoring depth
				nextDepthAt--;
				if(nextDepthAt == 0){
					nextDepthAt = nextDepthFinder;
					nextDepthFinder = 0;
					currentDepth++;

					TraversalStep<N,E> step = new TraversalStep<>(currentDepth-1, nodes, prevEdges);
					prevEdges = edges;					
					return step;
				}
			}

			//In case we visited all nodes but some edges between the nodes remained
			if(!prevEdges.isEmpty()){
				TraversalStep<N,E> tr = new TraversalStep<>(currentDepth-1, nodes, prevEdges);
				prevEdges = Collections.emptyList();
				return tr;
			}

			return null;
		}



		@Override
		public synchronized DefaultTraversal<N,E> traverse() {

			while (!queue.isEmpty()) {

				final N node = queue.remove();

				final LinkedList<E> outList = 
						node.links()
						.filter(l->visitedLinks.add(l))
						.collect(Collectors.toCollection(listSupplier));

				E edge = null;


				while ((edge = outList.poll()) != null) {

					N other = null;
					if(edge.source() == node)
						other = edge.target();
					else if(edge.target() == node)
						other = edge.source();

					if(visitedNodes.add(other)){//if already visited, do not add to queue
						queue.add(other);
						nextDepthFinder++;
					}
					visitedLinks.add(edge);

				}

			}


			return new DefaultTraversal<>(visitedNodes, visitedLinks);


		}	

	}






	public static class BreadthFirstWithConstraints implements BreadthFirst<NodeItem,Link>{

		private final TraverserConstraints constraints;

		private final Deque<NodeItem> queue = new LinkedList<>();

		private final Set<NodeItem> visitedNodes = new LinkedHashSet<>();   
		private final Set<NodeItem> includedNodes = new HashSet<>();

		private final Set<Link> visitedLinks = new LinkedHashSet<>();
		private final Set<Link> includedLinks = new HashSet<>();

		private List<Link> prevEdges = new ArrayList<>();
		private int nextDepthAt = 1;
		private int nextDepthFinder = 0;
		private int currentDepth = 0;

		private static final Supplier<LinkedList<Link>> listSupplier = () -> new LinkedList<>();


		private List<NodeItem> next = new ArrayList<>();


		private BreadthFirstWithConstraints(NodeItem item, TraverserConstraints constraints){      
			this.constraints = constraints;
			visitedNodes.add(item); 
			queue.add(item); 
		}    





		/**
		 * Return true if we should continue to screen the edges from this node
		 */
		private boolean handleItem(NodeItem node, Collection<NodeItem> nodes) {      

			switch(constraints.decide(node)){
			case EXCLUDE_AND_CONTINUE : 
				visitedNodes.add(node); 
				return true;
			case EXCLUDE_AND_STOP : 
				visitedNodes.add(node); 
				return false;
			case INCLUDE_AND_CONTINUE : 
				nodes.add(node);
				includedNodes.add(node);
				return true;
			case INCLUDE_AND_STOP : 
				includedNodes.add(node); 
				nodes.add(node);
				return false;         
			}
			System.out.println("Default case in Traversers handle Item");
			return true;
		}


		/**
		 * Return true if we should continue to screen the edges from this node
		 */
		private boolean handleItem(NodeItem node, Collection<NodeItem> nodes, boolean includeDepth) {      

			switch(constraints.decide(node)){
			case EXCLUDE_AND_CONTINUE : 
				visitedNodes.add(node); 
				return true;
			case EXCLUDE_AND_STOP : 
				visitedNodes.add(node); 
				return false;
			case INCLUDE_AND_CONTINUE : 
				if(includeDepth)
					nodes.add(node);
				return true;
			case INCLUDE_AND_STOP : 
				if(includeDepth)
					nodes.add(node);
				return false;         
			}
			System.out.println("Default case in Traversers handle Item");
			return true;
		}



		@Override
		public synchronized TraversalStep<NodeItem,Link> nextStep(){

			//System.out.println("Current Depth = "+currentDepth);
			//System.out.println("max Depth = "+constraints.maxDepth());

			if(currentDepth <= constraints.maxDepth()){

				//System.out.println("Below max depth!");

				List<NodeItem> nodes = new ArrayList<>();
				List<Link> edges = new ArrayList<>();

				queue.addAll(next);
				next.clear();

				while (!queue.isEmpty()) {

					NodeItem node = queue.remove();

					if(handleItem(node, nodes)){

						LinkedList<Link> outList = 
								node.getLinks(Direction.OUTGOING)
								.filter(l->visitedLinks.add(l) && constraints.accept(l, Direction.OUTGOING))
								.collect(Collectors.toCollection(listSupplier));

						LinkedList<Link> inList = 
								node.getLinks(Direction.INCOMING)
								.filter(l->visitedLinks.add(l) && constraints.accept(l, Direction.INCOMING))
								.collect(Collectors.toCollection(listSupplier));

						Link out = null;
						Link in = null;

						while ((out = outList.poll()) != null){
							if(visitedNodes.add(out.target())){
								next.add(out.target());
								nextDepthFinder++;
							}
							edges.add(out);
						}

						while ((in = inList.poll()) != null){
							if(visitedNodes.add(in.source())){
								next.add(in.source());
								nextDepthFinder++;	
							}
							edges.add(in);
						}


					}


					//Monitoring depth
					nextDepthAt--;
					if(nextDepthAt == 0){
						nextDepthAt = nextDepthFinder;
						nextDepthFinder = 0;
						currentDepth++;
						if(constraints.accept(currentDepth-1)){
							TraversalStep<NodeItem,Link> step = new TraversalStep<>(currentDepth-1, nodes, prevEdges);
							prevEdges = edges;
							return step;
						}
						else{
							return nextStep();
						}
					}
				}

				//In case we visited all nodes but some edges between the nodes remained
				if(!prevEdges.isEmpty()){
					TraversalStep<NodeItem,Link> tr = new TraversalStep<>(currentDepth-1, nodes, prevEdges);
					prevEdges = Collections.emptyList();
					return tr;
				}


			}

			return null;
		}



		@Override
		public synchronized DefaultTraversal<NodeItem,Link> traverse() {

			queue.addAll(next);
			next.clear();

			while (!queue.isEmpty()) {

				if(currentDepth > constraints.maxDepth())
					return new DefaultTraversal<NodeItem,Link>(includedNodes, includedLinks);

				boolean includeDepth = constraints.accept(currentDepth);//+1 as links are included after the depth at which they are encountered
				boolean includeLinks = includeDepth && constraints.accept(currentDepth+1);


				NodeItem node = queue.remove();

				if(handleItem(node, includedNodes, includeDepth)){

					Link out = null;
					Link in = null;


					LinkedList<Link> outList = 
							node.getLinks(Direction.OUTGOING)
							.filter(l->visitedLinks.add(l) && constraints.accept(l, Direction.OUTGOING))
							.collect(Collectors.toCollection(listSupplier));

					LinkedList<Link> inList = 
							node.getLinks(Direction.INCOMING)
							.filter(l->visitedLinks.add(l) && constraints.accept(l, Direction.INCOMING))
							.collect(Collectors.toCollection(listSupplier));


					while ((out = outList.poll()) != null){
						queue.add(out.target());
						nextDepthFinder++;	
						if(includeLinks)
							includedLinks.add(out);
					}

					while ((in = inList.poll()) != null){
						queue.add(in.source());
						nextDepthFinder++;	
						if(includeLinks)
							includedLinks.add(in);
					}



				}

				//Monitoring depth
				nextDepthAt--;
				if(nextDepthAt == 0){
					nextDepthAt = nextDepthFinder;
					nextDepthFinder = 0;
					currentDepth++;
				}

			}

			return new DefaultTraversal<NodeItem,Link>(includedNodes, includedLinks);
		}
	}








	public  static class DFTraverser
	<N extends Node<E>, E extends Edge<? extends N>>
	implements Traverser<N,E>
	{

		private Deque<N> stack = new LinkedList<>();
		private Set<N> visitedNodes = new LinkedHashSet<>();
		private Set<E> visitedLinks = new LinkedHashSet<>();

		private DFTraverser(N item) {
			stack.add(item);
			visitedNodes.add(item);
		}

		/**
		 * Traverse the instance graph using Depth First Search algorithm
		 */
		public void depthFirst() {
			while (!stack.isEmpty()) {
				N node = stack.peek();
				E out = getUnvisitedLink(node);
				if (out != null) {
					visitedNodes.add(out.target());
					visitedLinks.add(out);
					stack.push(out.target());
				}
				else
					stack.pop();
			}
		}



		protected E getUnvisitedLink(N node) {
			return node.links().filter(out-> !visitedLinks.contains(out)).findFirst().orElse(null);
		}



		@Override
		public DefaultTraversal<N, E> traverse() {//FIXME can be outgoing or incoming or self-ref
			while (!stack.isEmpty()) {
				N node = stack.peek();
				E out = getUnvisitedLink(node);
				if (out != null) {
					visitedNodes.add(out.target());
					visitedLinks.add(out);
					stack.push(out.target());
				}
				else
					stack.pop();
			}
			return new DefaultTraversal<>(visitedNodes, visitedLinks);
		}



	}


}

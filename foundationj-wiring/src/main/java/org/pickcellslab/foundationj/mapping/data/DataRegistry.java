package org.pickcellslab.foundationj.mapping.data;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Service;
import org.pickcellslab.foundationj.mapping.exceptions.DataTypeException;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;


/**
 * 
 * Instance Factory for {@link Data} objects using {@link DbElement} as input.
 * 
 * @author Guillaume Blin
 *
 */
@Core
@Service
public interface DataRegistry {

	/**
	 * @return a Stream of data types defined in the data model, including classes which have no stored instance
	 * in the database and therefore not yet available via the {@link MetaModel}
	 */
	Stream<Class<?>> dataTypes();

	Class<?> classOf(DbElement data);

	/**
	 * @param typeId
	 * @return The java Class corresponding to the specified typeId or null if the given typeId is not known.
	 */
	Class<?> classOf(String typeId);

	/**
	 * @param proxy The persisted DataElement with the generic backend implementation to be rebuilt into the corresponding data model implementation.
	 * @return A new DataElement with the data model implementation
	 * @throws DataTypeException
	 * @throws InstantiationHookException 
	 */
	Object getDataInstance(DbElement proxy) throws DataTypeException, InstantiationHookException;

	/**
	 * @param itfc
	 * @return A {@link Stream} of the known {@link Data} types implementing the given interface
	 */
	<T> Stream<Class<? extends T>> knownTypes(Class<T> itfc);

	/**
	 * Registers the given {@link InstantiationListener} with the {@link NotifyingInstantiationHook} instance maintained by this 
	 * {@link DataRegistry}
	 * @param l
	 * @param hookClass
	 */
	<T> void addInstatiationListener(InstantiationListener<T> l,
			Class<? extends NotifyingInstantiationHook<T>> hookClass);

	<H extends InstantiationHook> H getHook(Class<H> hookClass);
	
	
	/**
	 * Java classes are represented in the persistence framework using a unique typeId/alias/label, this method
	 * returns this String given a {@link DbElement} class.
	 * @param t The desired DataElement Class
	 * @return The unique alias/label which is associated with the provided Class or null if the given class
	 * is not a {@link Data} type 
	 */
	public static String typeIdFor(Class<?> t) {
		final Data d = t.getAnnotation(Data.class);
		return d == null ? null : d.typeId();
	}

}
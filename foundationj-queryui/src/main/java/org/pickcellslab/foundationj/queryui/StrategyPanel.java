package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Optional;

import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaDataSet;
import org.pickcellslab.foundationj.dbm.meta.MetaPathDataSet;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.dbm.meta.MetaSplitTraversal;
import org.pickcellslab.foundationj.dbm.meta.MetaTargetedDataSet;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig.PathMode;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Copyable;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class StrategyPanel<T,D> extends ValidityTogglePanel implements Copyable<StrategyPanel<T,D>>{

	private static final Logger log = LoggerFactory.getLogger(StrategyPanel.class);

	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final JRadioButton rdbtnAll;
	private final JRadioButton rdbtnPath;

	private final ConsiderAllPanel cap;
	private CreatePathPanel<D> cpp;

	private final QueryWizardConfig<T,D> config;

	private final UITheme theme;
	private final DataAccess access;
	private final ChoiceSet choices;

	private JPanel cardPanel;


	public StrategyPanel(UITheme theme, DataAccess access, ChoiceSet choices, QueryWizardConfig<T,D> config) {

		this.theme = theme;
		this.config = config;
		this.access = access;
		this.choices = choices;


		setLayout(new BorderLayout(0, 0));

		setPreferredSize(new Dimension(650,550));

		JPanel radioPanel = new JPanel();
		radioPanel.setBorder(new TitledBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Search Strategy", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)), "Search Strategy", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(radioPanel, BorderLayout.NORTH);


		cardPanel = new JPanel();
		add(cardPanel, BorderLayout.CENTER);
		CardLayout card = new CardLayout(0, 0);
		cardPanel.setLayout(card);



		rdbtnAll = new JRadioButton("Consider All");
		rdbtnAll.addActionListener(l->{			
			if(config.getAllowedQueryables()!=null)
				choices.filterQueryable(config.getAllowedQueryables());			
			card.show(cardPanel, "Consider All");			
			this.fireNextValidityChanged();
		});
		buttonGroup.add(rdbtnAll);
		radioPanel.add(rdbtnAll);


		rdbtnPath = new JRadioButton("Create A Path");
		rdbtnPath.addActionListener(l->{
			if(cpp == null){
				try {
					cpp = new CreatePathPanel<>(theme, access, choices, config, false);
				} catch (Exception e) {
					log.error("An error has occured while reading the database",e);
					return;
				}
				cpp.addValidityListener(v->this.fireNextValidityChanged());
				choices.highlightMaster();
				cardPanel.add("Create Path", cpp);
			}
			card.show(cardPanel, "Create Path");
			choices.removeFilterOnQueryable();
			choices.highlightMaster();
			this.fireNextValidityChanged();
		});
		buttonGroup.add(rdbtnPath);
		radioPanel.add(rdbtnPath);



		cap = new ConsiderAllPanel(theme, access.dataRegistry(), choices, config, false);
		cap.addValidityListener(l->this.fireNextValidityChanged());
		cardPanel.add("Consider All", cap);


		//Configure
		rdbtnAll.setEnabled(config.isConsiderAllEnabled());
		rdbtnPath.setEnabled(config.isPathEnabled());

		//If only path with a filter on queryable
		if(config.isConsiderAllEnabled()){
			rdbtnAll.setSelected(true);
			if(config.getAllowedQueryables()!=null)
				choices.filterQueryable(config.getAllowedQueryables());
		}
		else{
			rdbtnPath.doClick();
		}





	}


	@Override
	public boolean validity() {
		if(rdbtnAll.isSelected())
			return cap.validity();
		else
			return cpp.validity();
	}


	@SuppressWarnings("unchecked")
	public MetaDataSet<T,D> getDataSet(String name){

		final MetaInstanceDirector director = access.metaModel().getInstanceManager();

		try {
		
		if(this.rdbtnAll.isSelected()){
			return (MetaDataSet<T,D>) MetaTargetedDataSet.getOrCreate(
					director,
					name,
					"NA",
					cap.getTarget(),
					cap.getMetaFilter(access.metaModel()).orElse(null),
					cap.getGrouping(access.metaModel()).orElse(null)
					);

		}
		else{

			if(config.getPathMode() == PathMode.TARGET){
				//Check if there is a path split
				Optional<MetaSplit> pathSplit = cpp.getSplitting(access.metaModel());
				if(pathSplit.isPresent()){

					return (MetaDataSet<T,D>) MetaTargetedDataSet.getOrCreate(
							director,
							name,
							"NA",
							cpp.getTarget(),
							MetaSplitTraversal.getOrCreate(
									director,
									"name", //TODO change name
									cpp.getMetaTraversal(access.metaModel()),
									pathSplit.get()
									),
							cpp.getGrouping(access.metaModel()).orElse(null)
							);

				}else

					return (MetaDataSet<T,D>) MetaTargetedDataSet.getOrCreate(
							director,
							name,
							"NA",
							cpp.getTarget(),
							cpp.getMetaTraversal(access.metaModel()),
							cpp.getGrouping(access.metaModel()).orElse(null)
							);
			}
			else{ // If Path
				//Check if there is a path split
				Optional<MetaSplit> pathSplit = cpp.getSplitting(access.metaModel());

				//	cpp.getGrouping().ifPresent(split -> System.out.println("StartegyPanel : Split Class -> "+split.getClass().getSimpleName()));

				if(pathSplit.isPresent()){

					return (MetaDataSet<T,D>) MetaPathDataSet.getOrCreate(
							director,
							name,
							"NA",
							MetaSplitTraversal.getOrCreate(
									director,
									"name", //TODO change name
									cpp.getMetaTraversal(access.metaModel()),
									pathSplit.get()
									),
							cpp.getGrouping(access.metaModel()).orElse(null)
							);

				}else

					return (MetaDataSet<T,D>) MetaPathDataSet.getOrCreate(
							director,
							name,
							"NA",
							cpp.getMetaTraversal(access.metaModel()),
							cpp.getGrouping(access.metaModel()).orElse(null)
							);
			}
		}
		
		}catch(InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}


	public QueryInfo getInfo(String name) {

		QueryInfo i = config.create(name);

		if(this.rdbtnAll.isSelected()){			
			i.set(QueryInfo.TARGET, cap.getTarget());
		}
		else{
			if(config.getPathMode() == PathMode.PATH)
				i.set(QueryInfo.INCLUDED, cpp.getIncluded());
			if(config.getFixedRootKey()!=null)
				try {
					i.set(QueryInfo.FIXED_ROOT, cpp.getRootInstance());
				} catch (DataAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			i.set(QueryInfo.TARGET, cpp.getTarget());
		}




		return i;
	}


	@Override
	public void copyState(StrategyPanel<T, D> other) {
		// TODO Auto-generated method stub
		if(other.rdbtnAll.isSelected()){
			this.rdbtnAll.setSelected(true);
			this.cap.copyState(other.cap);
		}
		else if(other.rdbtnPath.isSelected()){
			this.rdbtnPath.setSelected(true);
			try {
				this.cpp = new CreatePathPanel<>(theme, access, choices, config, false);
				cpp.addValidityListener(v->this.fireNextValidityChanged());
				choices.highlightMaster();
				cardPanel.add("Create Path", cpp);
				((CardLayout)cardPanel.getLayout()).show(cardPanel, "Create Path");
				choices.removeFilterOnQueryable();
				choices.highlightMaster();
				this.fireNextValidityChanged();
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.cpp.copyState(other.cpp);
		}
	}


}

package org.pickcellslab.foundationj.queryui.renderers;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.UITheme;

@SuppressWarnings("serial")
public class MetaTreeRenderer extends DefaultTreeCellRenderer {
	
	private final UITheme theme;
	private final DataRegistry registry;
	
	public MetaTreeRenderer(UITheme theme, DataRegistry registry) {
		this.theme = theme;
		this.registry = registry;
	}
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus) {
		
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

		Object v = value;
		if(MetaModel.class.isAssignableFrom(node.getClass())){
			v = ((MetaModel) node).toString();
			Icon i = MetaToIcon.get((MetaModel) node, registry, theme, 16);
			setOpenIcon(i);
			setClosedIcon(i);
		}
		
		super.getTreeCellRendererComponent(
				tree, v, sel, exp, leaf, row, hasFocus);
		return this;
	}
}

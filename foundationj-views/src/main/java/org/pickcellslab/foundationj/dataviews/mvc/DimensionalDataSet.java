package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;

/**
 * A {@link DimensionalDataSet} is a {@link DataSet} which defines {@link Dimension}s compatible with the data it can query.
 *
 * @param <T> The type of database objects queried by this DataSet
 * @param <D> The output type of the {@link Dimension}s defined by this {@link DimensionalDataSet}
 */
public interface DimensionalDataSet<T, D> extends DataSet<T> {
	
	/**
	 * @param query The index of the query of interest
	 * @param predicate A {@link Predicate} used to filter the {@link DimensionContainer}s returned by this method
	 * @return A {@link List} of {@link DimensionContainer} objects which can be queried for the possible {@link Dimension}s
	 * defined on this {@link DataSet}
	 */
	public List<DimensionContainer<T,D>> possibleDimensions(int query, Predicate<DimensionContainer<T,?>> predicate);
	
}

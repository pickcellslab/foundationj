package org.pickcellslab.foundationj.datamodel.functions;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.ExplicitOperation;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.StreamContains;
import org.pickcellslab.foundationj.datamodel.predicates.StreamEmpty;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;

public interface ExplicitStream<I,O> extends ExplicitOperation, ExplicitFunction<I,Stream<O>>{


	public default ExplicitFunction<I,O> findOne(Class<O> clazz){
		return new StreamFindOne<I, O>(this, clazz);
	}
	
	
	public default <N> ExplicitStream<I,N> map(ExplicitFunction<? super O, N> mapper){
		return new StreamMap<>(this,mapper);
	}
	
	public default ExplicitStream<I,O> filter(ExplicitPredicate<? super O> p){
		return new StreamFilter<>(this,p);
	}
	
	
	public default <R> ExplicitFunction<I,R> reduce(ExplicitReduction<? super O, R> reduction){
		return new StreamReduction<>(this, reduction);
	}
	
	
	public default ExplicitPredicate<I> contains(O value){
		return new StreamContains<I,O>(this,value);
	}
	
	public default ExplicitPredicate<I> isEmpty(){
		return new StreamEmpty<I,O>(this);
	}
	
	
	public default StreamCount<I> count() {
		return new StreamCount<>(this);
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public default Class<Stream<O>> getReturnType() {
		return (Class)Stream.class;
	}
	
}

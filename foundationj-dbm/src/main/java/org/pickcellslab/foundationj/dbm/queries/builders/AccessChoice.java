package org.pickcellslab.foundationj.dbm.queries.builders;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

/**
 * Defines one step of the Stepwise Builder Pattern used to create queries
 * @param <T> The target type of the query
 * @param <B> The next builder in the pattern
 */
public interface AccessChoice<T,B> {

	/**
	 * Specifies that all elements encountered during the query should be read
	 * @return The next step in the building process
	 */
	public B getAll();
	

	/**
	 * @param test The {@link ExplicitPredicate} to use to filter elements encountered during the query
	 * @return The next step in the building process
	 */
	public B useFilter(ExplicitPredicate<? super T> test);
	
	/**
	 * Specify that a {@link TraversalDefinition} should be used to identify the elements of the database that are going to be read.
	 * @param definition The {@link TraversalDefinition} which specifies how the data graph should be traversed.
	 * @return The next step in the building process
	 * @see {@link Traversals}
	 */
	public B useTraversal(TraversalDefinition definition);
	
	public B useTraversals(SuppliedTraversalOperation operations);
	
	
}

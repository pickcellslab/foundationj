package org.pickcellslab.foundationj.bootstrap;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Service;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BootStrapper<T> {


	private static final Logger log =  LoggerFactory.getLogger(BootStrapper.class);


	private static final ScopeManagerImpl scopes = new ScopeManagerImpl();

	//TODO put this in a config file
	private final String binPath = "bin"+File.separator;
	private final String pluginsPath = "plugins"+File.separator;;
	private final String splashPic = "resources"+File.separator+"Laras.jpg";

	private final String corePath = "META-INF/foundationj/core";
	private final String coreImplPath = "META-INF/foundationj/coreImpl/";

	private final String modulePath = "META-INF/foundationj/modules/";
	private final String dataPath = "META-INF/foundationj/data";
	private final String mappingPath = "META-INF/foundationj/mapping";


	public BootStrapper() {



		final String root = scopes.getRootPath();


		// Start by showing the Splash screen
		final Splash splash = new Splash(new File(root+File.separator+splashPic));
		splash.show();	




		// TODO Updater

		// Phase 1 -> discovery with configuration checks
		final Map<Scope, List<Class<?>>> components = new HashMap<>(); // Core and Modules with @Scope / @SameScopeAs / @Service or @WithService
		final Map<Class<?>, Class<?>> services = new HashMap<>(); // Core with @Service


		final Map<Scope,List<Class<?>>> dataScoped = new HashMap<>(); // Data with @Scope or @SameScopeA
		final Map<Class<?>,List<Class<?>>> dataServices = new HashMap<>(); // Data @WithService

		final Set<Class<?>> mappingSet = new HashSet<>(); // Mapping
		final Set<Class<?>> deniedSet = new HashSet<>(); // DenyMapping
		try {

			splash.setProgress("Discovery Phase : Finding Core Modules");
			int count = coreDiscovery(root, components, services);
			splash.setProgress("Discovery Phase : "+count+" Core Modules Found");
			Thread.sleep(200);


			splash.setProgress("Discovery Phase : Finding Optional Modules");
			count = modulesDiscovery(root, components);
			splash.setProgress("Discovery Phase : "+count+" Optional Modules Found");
			Thread.sleep(200);

			splash.setProgress("Discovery Phase : Finding Data Types");
			count = dataDiscovery(root, dataPath, dataScoped, dataServices);
			splash.setProgress("Discovery Phase : "+count+" Data Types Found");
			Thread.sleep(200);

			splash.setProgress("Discovery Phase : Finding Mapping Annotations");
			count = mappingDiscovery(root, mappingPath, mappingSet, deniedSet);
			splash.setProgress("Discovery Phase : "+count+" Mapping Annotations Found");
			Thread.sleep(200);


		}catch(IOException | InterruptedException e) {	
			splash.error();
			BootStrapper.logErrorAndExit("An error occured during the component discovery phase while booting the application", e);		
		}


		// Phase 2 Wiring
		try {


			splash.setProgress("Wiring Phase : Setting up components");
			scopes.setComponents(components);

		}catch(Exception e) {
			logErrorAndExit("Setting up components failed, the error has been logged", e);
		}

		try {

			splash.setProgress("Wiring Phase : Setting up services");
			scopes.setServices(services);

		}catch(Exception e) {
			logErrorAndExit("Setting up services failed, the error has been logged", e);
		}

		try {

			splash.setProgress("Wiring Phase : Setting up Data Scopes ");
			scopes.setupDataConfiguration(dataScoped, dataServices);

		}catch(Exception e) {
			logErrorAndExit("Setting up data types failed, the error has been logged", e);
		}


		// Phase 3 Start
		splash.setProgress("Wiring Done -> Starting the application");

		try {

			scopes.startApplication(mappingSet, deniedSet);
			splash.close();

		}catch(Exception e) {
			logErrorAndExit("Wiring succeeded, however the application failed to start. The error has been logged", e);
		}




	}














	private int coreDiscovery(String rootDir, Map<Scope, List<Class<?>>> components, Map<Class<?>, Class<?>> services) throws IOException {

		// bin only
		int counter = 0;

		// Get the necessary core interface for the application to work as expected
		final Set<Class<?>> coreInterfaces = ScanUtils.getTypeList(new File(rootDir+File.separator+binPath), corePath);

		// Add Core from foundationj-wiring (annotations don't work when in the same folder so not discovered)
		coreInterfaces.add(DataRegistry.class);
		coreInterfaces.add(NonDataMapper.class);

		if(coreInterfaces.isEmpty()) {
			logErrorAndExit("No Core interface found on the classpath", null);
		}

		// Find implementations
		final Map<Class<?>, Set<Class<?>>>  coreImpls = ScanUtils.getInterfacesAndImpl(new File(rootDir+File.separator+binPath), coreImplPath);

		// Ensure that we find one impl for each core interface otherwise log error and exit
		for(Class<?> itfc : coreInterfaces) {
			final Set<Class<?>> impls = coreImpls.get(itfc);
			if(impls == null || impls.isEmpty())
				BootStrapper.logErrorAndExit("A core interface has no implementation, please check your configuration ("+itfc+")", null);

			if(impls.size()>1)
				log.warn("Multiple implementations were found for a core interface, an arbitrary choice as been made ("+itfc+")");

			counter++;

			// Now assign to components or services
			final Class<?> coreImpl = impls.iterator().next();
			if(itfc.getAnnotation(Service.class)!=null) {
				// Assign to services
				services.put(itfc, coreImpl);
				log.debug("Service Added: "+coreImpl);
			}
			else {
				// Assign to Components
				Scope s = getScopeFrom(itfc, "Core");
				List<Class<?>> comps = components.get(s);
				if(comps == null) {
					comps = new ArrayList<>();
					components.put(s, comps);					
				}
				comps.add(coreImpl);
			}
		}

		return counter;

	}














	private int modulesDiscovery(String rootDir, Map<Scope, List<Class<?>>> components) throws IOException {
		// bin and plugins

		// Find implementations
		final Map<Class<?>,Set<Class<?>>> binMods = ScanUtils.getInterfacesAndImpl(
				new File(rootDir+File.separator+binPath), modulePath);

		int counter = registerModule(binMods, components);

		final Map<Class<?>,Set<Class<?>>> pluginsMods = ScanUtils.getInterfacesAndImpl(
				new File(rootDir+File.separator+pluginsPath), modulePath);

		counter += registerModule(pluginsMods, components);

		return counter;
	}





	// Modules can only be Scope not Service
	private int registerModule(Map<Class<?>, Set<Class<?>>> itToImpl, Map<Scope, List<Class<?>>> components) {

		int counter = 0;
		
		// FIXME check if scope is overriden by impl
		for(Class<?> itfc : itToImpl.keySet()) {
			Scope scope = getScopeFrom(itfc, "Modular");

			List<Class<?>> comps = components.get(scope);
			if(comps == null) {
				comps = new ArrayList<>();
				components.put(scope, comps);
			}
			comps.addAll(itToImpl.get(itfc));
			counter+=itToImpl.get(itfc).size();
		}

		return counter;
	}





	// Here we have @Data types
	private int dataDiscovery(String rootDir, String inJarPath, Map<Scope, List<Class<?>>> dataScoped, Map<Class<?>, List<Class<?>>> dataServices) throws IOException {
		// bin and plugins

		final Set<Class<?>> data = ScanUtils.getTypeList(new File(rootDir+File.separator+binPath), inJarPath);
		data.addAll(ScanUtils.getTypeList(new File(rootDir+File.separator+pluginsPath), inJarPath));

		final Map<Scope,Set<String>> known = new HashMap<>();

		for(Class<?> d : data) {
			// Is it @WithService?
			final WithService ws = d.getAnnotation(WithService.class);
			if(ws != null) {
				final Class<?> service = ws.value();
				List<Class<?>> set = dataServices.get(service);
				if(set == null) {
					set = new ArrayList<>();
					dataServices.put(service, set);
				}
				set.add(d);
			}
			else {
				// Then it must be scope
				Scope scope = getScopeFrom(d, "Data type");
				List<Class<?>> comps = dataScoped.get(scope);
				Set<String> knownForScope = known.get(scope);
				if(comps == null) {
					comps = new ArrayList<>();
					dataScoped.put(scope, comps);
					knownForScope = new HashSet<>();
					known.put(scope, knownForScope);
				}
				if(knownForScope.add(d.getAnnotation(Data.class).typeId()))
					comps.add(d);
				else
					throw new RuntimeException("The class path contains data types with the same typeId "+
							d.getAnnotation(Data.class).typeId()+" for scope "+scope);
			}
		}

		return data.size();

	}






	private Scope getScopeFrom(Class<?> itfc, String hint) {
		Scope scope = itfc.getAnnotation(Scope.class);
		if(scope==null) {
			SameScopeAs ssa = itfc.getAnnotation(SameScopeAs.class);
			if(ssa == null)
				BootStrapper.logErrorAndExit("A "+hint+" Interface" + itfc + " is not annotated with Scope or SameScope As", null);
			scope = ssa.value().getAnnotation(Scope.class);
			if(scope == null)
				BootStrapper.logErrorAndExit("The class (" + ssa.value() + ") given by SameScopeAs found on "
						+ itfc+ "  is not annotated with Scope", null);

		}
		return scope;
	}





	// Here we have @Mapping or @DenyMapping types
	private int mappingDiscovery(String rootDir, String inJarPath, Set<Class<?>> mappingSet, Set<Class<?>> deniedSet) throws IOException {
		// bin and plugins

		final Set<Class<?>> data = ScanUtils.getTypeList(new File(rootDir+File.separator+binPath), inJarPath);
		data.addAll(ScanUtils.getTypeList(new File(rootDir+File.separator+pluginsPath), inJarPath));

		for(Class<?> d : data) {
			// Is it @Mapping?
			final Mapping ma = d.getAnnotation(Mapping.class);
			if(ma != null) {				
				mappingSet.add(d);
			}
			else {
				deniedSet.add(d);
			}
		}

		return data.size();

	}










	static void logErrorAndExit(String msg, Throwable t) {
		log.error(msg, t);
		System.out.println(msg);
		JOptionPane.showMessageDialog(null, msg, "Startup Failure!", JOptionPane.ERROR_MESSAGE);
		System.exit(1);
	}





	public static void main(String[] args) {

		new BootStrapper<>();

	}



	/*




	public static void main(String ... argv) throws EvalError, InterruptedException {

		final JConsole console = new JConsole();
		console.setMinimumSize(new Dimension(500, 250));

		final JFrame f = new JFrame("App Control Shell");		
		f.add(console, BorderLayout.CENTER);
		f.setSize(800,400);
		f.setLocationRelativeTo(null);
		f.setVisible(true);


		final Interpreter interpreter = new Interpreter( console );	
		System.setOut(interpreter.getOut());
		System.setErr(interpreter.getErr());
		final PicoTest<?> pico = new PicoTest<>();
		interpreter.set("control", pico.getAppController());
		interpreter.set("view", pico.getApplicationScope().getComponentInstance(PickCellsPresentation.class));
		interpreter.set("shell", f);		


		new Thread( interpreter ).start(); // start a thread to call the run() method


		while(f.isVisible()) {
			Thread.sleep(1000);
		}

		System.exit(0);

	}

	 */





}

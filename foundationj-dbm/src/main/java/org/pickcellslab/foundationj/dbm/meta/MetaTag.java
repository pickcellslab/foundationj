package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

/**
 * Represent a tag (see {@link Tag}) in the meta model
 * @author Guillaume
 *
 */


@Data(typeId = "MetaTag", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaTag extends MetaItem implements DataPointer{

	private MetaTag(String uid){
		super(uid);
	}

	public static MetaTag getOrCreate(MetaInstanceDirector director, String tag, String description) throws InstantiationHookException {		
		
		final MetaTag t = director.getOrCreate(MetaTag.class, tag);
		
		t.setAttribute(MetaItem.name, tag);
		t.setAttribute(MetaItem.desc, description);
		
		return t;
	}


	String tag(){
		return this.getAttribute(MetaModel.recognition).get();
	}


	

	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}
	
	
	

	public List<MetaClass> getLabelled(){
		return this.getLinks(Direction.INCOMING, MetaModel.tagLink)
				.map(l-> (MetaClass)l.source()).collect(Collectors.toList());
	}






	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>Tag</strong>"+
				"<p><ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"</li></p></HTML>";
	}

	@Override
	public String toString(){
		return name()+" (Tag)";
	}

	@Override
	public TraversalDefinition deletionDefinition() {
		return TraversalDefinition.newDefinition()
				.startFrom(DataRegistry.typeIdFor(MetaTag.class)).having(MetaModel.recognition, recognitionString())
				.fromDepth(0).toDepth(0)
				.traverseAllLinks()
				.includeAllNodes().build();
	}

	@Override
	public int deleteData(DataAccess access) throws DataAccessException {
		return access.queryFactory().delete(name()).completely().getAll().run();
	}
	
	
	@Override
	public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
		return access.queryFactory().readOneNode(name(), dbId).using(action);
	}


	@Override
	public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
		return access.queryFactory().readAnyNode(name()).using(action).getAll().run();
	}
	

	@Override
	public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
		return access.queryFactory().read(name(), limit).with(action).inOneSet().getAll().run(); 
	}

	@Override
	public void updateData(DataAccess access, Updater action) throws DataAccessException {
		access.queryFactory().update(name(), action).getAll().run();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<MetaQueryable> queriedMetaObjects() {
		return (List)getLabelled();
	}

	@Override
	public DataPointer subset(ExplicitPredicate<DataItem> predicate) {
		
		Objects.requireNonNull(predicate);
		
		return new DataPointer() {

			@Override
			public int deleteData(DataAccess access) throws DataAccessException {
				throw new RuntimeException("Forbidden for now");
			}

			@Override
			public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
				return access.queryFactory().readOneNode(name(), dbId).using(action);
			}


			@Override
			public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
				return access.queryFactory().readAnyNode(name()).using(action).useFilter(predicate).run();
			}
			

			@Override
			public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
				return access.queryFactory().read(name(), limit).with(action).inOneSet().useFilter(predicate).run(); 
			}

			@Override
			public void updateData(DataAccess access, Updater action) throws DataAccessException {
				access.queryFactory().update(name(), action).useFilter(predicate).run();
			}

			@Override
			public List<MetaQueryable> queriedMetaObjects() {
				return MetaTag.this.queriedMetaObjects();
			}

			@Override
			public DataPointer subset(ExplicitPredicate<DataItem> predicate2) {
				Objects.requireNonNull(predicate2);
				return MetaTag.this.subset(predicate.and(predicate2));
			}
			
		};
	}
	


}

package org.pickcellslab.foundationj.dbm.queries;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.tools.DataItemBuild;


public class RegeneratedItems implements DataItemBuild, Iterable<DataItem>{


	


	private final List<DataItem> dependencies;
	private final List<DataItem> targets;

	private RegeneratedItems(RegeneratedItemsBuilder builder) {
		this.dependencies = builder.dependencies;
		this.targets = new ArrayList<>(builder.targets);
	}



	

	
	public RegeneratedItems merge(RegeneratedItems other) {
		final Set<DataItem> thisTg = new HashSet<>(targets);
		final Set<DataItem> thisDp = new HashSet<>(dependencies);
		thisTg.addAll(other.targets);
		thisDp.addAll(other.dependencies);
		targets.clear();
		targets.addAll(thisTg);
		dependencies.clear();
		dependencies.addAll(thisDp);
		return this;
	}
	
	
	@Override
	public List<? extends DataItem> getAllTargets(){
		return targets;
	}
	
	
	@Override
	public List<? extends DataItem> getAllDependencies(){
		return dependencies;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <E extends DataItem> Stream<E> getTargets(Class<E> clazz) {
		return targets.stream().filter(p-> clazz.isAssignableFrom(p.getClass())).map(c->(E)c);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <E extends DataItem> Stream<E> getDependencies(Class<E> clazz) {
		return dependencies.stream().filter(p-> clazz.isAssignableFrom(p.getClass())).map(c->(E)c);
	}
	
	@Override
	public <E extends DataItem> Stream<E> getAllItemsFor(Class<E> clazz) {
		return Stream.concat(getTargets(clazz), getDependencies(clazz));
	}
	
	

	
	@SuppressWarnings("unchecked")
	@Override
	public <E> Optional<E> getOneTarget(Class<E> clazz){
		return targets.stream().parallel().filter(p-> clazz.isAssignableFrom(p.getClass())).map(c->(E)c).findAny();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <E> Optional<E> getOneDependency(Class<E> clazz){
		return dependencies.stream().parallel().filter(p-> clazz.isAssignableFrom(p.getClass())).map(c->(E)c).findAny();
	}



	@Override
	public Iterator<DataItem> iterator() {
		return targets.iterator();
	}


	public static class RegeneratedItemsBuilder {

		private final List<DataItem> dependencies = new ArrayList<>();
		private final List<DataItem> targets = new ArrayList<>();


		/**
		 * Adds the specified {@link DataItem} as a target of the query
		 * @param item
		 */
		
		public RegeneratedItemsBuilder addAsTarget(DataItem item){
			targets.add(item);
			return this;
		}

		/**
		 * Adds the specified {@link DataItem} as a known target dependency.
		 * In other words if a DataItem is part of a graph and the query {@link 
		 * ReconstructionRuleImpl} stipulates that connections must be attached, then items that are
		 * part of the graph but not directly a target of the query must be added to the {@link RegeneratedItemsImpl}
		 * using the method
		 */
		
		public RegeneratedItemsBuilder addTargetDependency(DataItem item) {
			dependencies.add(item);
			return this;
		}

		
		public void reset(){
			targets.clear();
			dependencies.clear();
		}
		
		
		public RegeneratedItems build() {
			return new RegeneratedItems(this);
		}

	}

}

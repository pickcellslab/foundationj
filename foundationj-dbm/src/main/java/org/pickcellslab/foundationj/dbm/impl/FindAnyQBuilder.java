package org.pickcellslab.foundationj.dbm.impl;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.impl.MonolithicSearchDefinition.Builder;
import org.pickcellslab.foundationj.dbm.queries.SearchingQuery;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

class FindAnyQBuilder<V,E,T,O> implements AccessChoice<T,QueryRunner<O>>, QueryRunner<O>{


	private final Builder<V,E,T> delegate;
	private final Function<? super T,O> action;
	private final TargetType<V,E,T> target;

	FindAnyQBuilder(TargetType<V,E,T> target, Function<? super T,O> action){
		this.delegate = new MonolithicSearchDefinition.Builder<>(target);
		this.action = action;
		this.target = target;
	}	



	@Override
	public QueryRunner<O> getAll() {
		return this;
	}

	@Override
	public QueryRunner<O> useFilter(ExplicitPredicate<? super T> test) {
		delegate.setFilter(test);
		return this;
	}
	


	@Override
	public QueryRunner <O> useTraversal(TraversalDefinition definition) {
		assert definition != null;
		target.checkTraversalDefinition(definition);
		delegate.setTraversal(definition);
		return this;
	}

	public QueryRunner <O> useTraversals(SuppliedTraversalOperation operations) throws IllegalStateException {
		delegate.setTraversalOperation(operations);
		return this;
	}

	@Override
	public O run() throws DataAccessException {	
		return 
				target
				.session()
				.runQuery(new FindAnyQuery(delegate.build(), action));
	}

	
	
	private class FindAnyQuery implements SearchingQuery<V, E, O>{

		
		private final MonolithicSearchDefinition<V, E, T> definition;
		private final Function<? super T, O> action;
		
		public FindAnyQuery(MonolithicSearchDefinition<V, E, T> build, Function<? super T, O> action) {
			this.definition = build;
			this.action = action;
		}

		@Override
		public String description() {
			return "NA";
		}

		@Override
		public O createResult(DatabaseAccess<V, E> db) throws DataAccessException {
			Objects.requireNonNull(db, "The provided DatabaseAccess is null");
			final Iterator<T> it = definition.accessor().iterator(db);
			O result = null;
			if(it.hasNext()){
				result = action.apply(it.next());
			}
			return result;
		}
		
	}
	
	
	
}

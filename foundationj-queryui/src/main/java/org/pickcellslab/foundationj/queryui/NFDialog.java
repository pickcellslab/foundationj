package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;

@SuppressWarnings("serial")
public class NFDialog extends JDialog{

	private boolean wasCancelled = true;
	private NodeFilterWrapper wrapper;

	public NFDialog(UITheme theme, DataRegistry registry, MetaFilter[] filters) {

		if(filters.length == 0)
			throw new IllegalArgumentException("At least one metafilter must be provided");

		JComboBox<Decision> modeBox = new JComboBox<>(Decision.values());

		JComboBox<MetaFilter> filterBox = new JComboBox<>(filters);
		filterBox.setRenderer(new MetaRenderer(theme, registry));

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{
			wasCancelled = false;
			wrapper = new NodeFilterWrapper((Decision) modeBox.getSelectedItem(), (MetaFilter)filterBox.getSelectedItem());
			this.dispose();
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->this.dispose());

		//Layout
		//----------------------------------------------------------------------------------------------------


		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addContainerGap()
										.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
												.addComponent(modeBox, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)
												.addComponent(filterBox, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE)))
												.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
														.addContainerGap()
														.addComponent(btnOk)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(btnCancel)))
														.addContainerGap(13, Short.MAX_VALUE))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(modeBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(filterBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnCancel)
								.addComponent(btnOk))
								.addContainerGap(24, Short.MAX_VALUE))
				);
		getContentPane().setLayout(groupLayout);
	}


	public boolean wasCancelled(){
		return wasCancelled;
	}

	public NodeFilterWrapper wrapper(){
		return wrapper;
	}

}

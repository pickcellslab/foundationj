package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;

class MultiSearchDefinition<V,E,T,K> {

	
	private final MonolithicSearchDefinition<V,E,T> mono;
	private final ExplicitSplit<? super T,K> splitter;
	
	MultiSearchDefinition(MonolithicSearchDefinition<V,E,T> mono, ExplicitSplit<? super T,K> splitter){		
		this.mono = mono;
		this.splitter = splitter;
	}
	
	
	public String descrition(){
		return mono.description()+" then "+splitter.description();
	}


	public DatabaseAccessor<V,E,T> accessor() {
		return mono.accessor();
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<K, Iterable<T>> split(Iterator<T> itr) {
		
		Objects.requireNonNull(itr);
				
		Map<K, List<T>> map = new HashMap<>();
	
		while(itr.hasNext()){		
			T t = itr.next();
			K k = splitter.getKeyFor(t);
			List<T> list = map.get(k);
			if(list == null){
				list = new ArrayList<>();
				map.put(k, list);
			}
			list.add(t);			
		}		
		return	(Map)map;
		
	}
	
	
	
}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.impl.MetaInfoImpl.MetaBuilder;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;
import org.pickcellslab.foundationj.dbm.queries.StorageBox;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This query is used to store or update {@link WritableDataItem} into the database. By "update", we mean that any attribute
 * already present in the database will be overwritten. However, no delete will be performed if attributes present in the database
 * are no longer present in the DataItem to store. A {@link DeleteQuery} is dedicated too this.
 * @author Guillaume Blin
 *
 */
class StorageBoxImpl implements StorageBox {

	
	private static final Logger log = LoggerFactory.getLogger(StorageBoxImpl.class);
	
	private final List<StepTraverser<NodeItem,Link>> traversers;
	private final List<WritableDataItem> standAlones;
	private final String description;
	private final boolean metaOff;
	private final int limit;
	private final boolean forceUnknownId;


	private StorageBoxImpl(List<StepTraverser<NodeItem,Link>> trav, List<WritableDataItem> dataList, boolean metaIsOFF, int limit, boolean forceUnknownId, String description) {
		traversers = trav;
		standAlones = dataList;
		this.metaOff = metaIsOFF;
		this.description = description;
		this.limit = limit;
		this.forceUnknownId = forceUnknownId;
	}

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.StorgareBox#transactionSizeLimit()
	 */
	@Override
	public int transactionSizeLimit(){
		return limit;
	}

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.StorgareBox#hasMetaInfo()
	 */
	@Override
	public boolean hasMetaInfo(){
		return !metaOff;
	}

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.StorgareBox#getTraversers()
	 */
	@Override
	public List<StepTraverser<NodeItem,Link>> getTraversers(){
		return traversers;
	}

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.StorgareBox#getStandAlones()
	 */
	@Override
	public List<WritableDataItem> getStandAlones(){
		return standAlones;
	}
	


	@Override
	public MetaInfo getMetaInfoForStandalones() {
		return ((MetaTrackingCollection) standAlones).getMetaInfo();
	}

	@Override
	public MetaInfo getMetaInfoForTraverser(int i) {
		return ((MetaTrackingTraverser) traversers.get(i)).getMetaInfo();
	}

	

	
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.StorgareBox#forceUnknownId()
	 */
	@Override
	public boolean forceUnknownId() {
		return forceUnknownId;
	}
	
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.StorgareBox#description()
	 */
	@Override
	public String description() {
		return description;
	}









	// Builder
	//-----------------------------------------------------------------------------------


	/**
	 * A builder for {@link StorageBoxImpl}
	 * 
	 * @author Guillaume
	 *
	 */
	public static class BoxBuilderImpl implements StorageBoxBuilder{

		private List<WritableDataItem> standAlones = new ArrayList<>();	  
		private Map<NodeItem, TraverserConstraints> roots = new HashMap<>();
		private String description = "Store: ";
		private MetaBuilder builder;
		private boolean metaIsOFF = false;
		private int limit = 20000;
		private boolean forceUnknownId = false;
		private final Session<?, ?> session;
		
		BoxBuilderImpl(Session<?,?> session) {
			this.session = session;
			builder = new MetaBuilder(session);
		}

		
		
		@Override
		public StorageBoxBuilder add(NodeItem item) {
			if(!description.contains("Whole DataGraph from a root of type "+item.declaredType()))
				description = description + "Whole DataGraph from a root of type "+item.declaredType();
			roots.put(item, null);
			return this;
		}


	
		@Override
		public StorageBoxBuilder add(NodeItem item, TraverserConstraints constraints) {
			if(!description.contains("DataGraph with constraints from a root of type "+item.declaredType()))
				description = description + "DataGraph with constraints from a root of type "+item.declaredType();
			roots.put(item, constraints);
			return this;
		}
		
		
		
		@Override
		public StorageBoxBuilder forceUnknownId(){
			forceUnknownId = true;
			return this;
		}
		

		
		@Override
		public StorageBoxBuilder addStandAlone(WritableDataItem item) {    
			standAlones.add(item);
			return this;
		}

		
		
		@Override
		public StorageBoxBuilder setMaxCommitSize(int limit){
			this.limit = limit;
			return this;
		}
		
		
		@Override
		public StorageBoxBuilder turnMetaModelOff(){
			metaIsOFF = true;
			return this;
		}

		
		@Override
		public StorageBoxBuilder addAll(Collection<? extends WritableDataItem> standAloneItems) {
			standAlones.addAll(standAloneItems);
			return this;
		}

	
		@Override
		public StorageBoxBuilder feed(NodeItem prototype, String creator, String description){
			try {
				builder.feed(prototype, creator, description);
			} catch (InstantiationHookException e) {
				log.error("Unable to build the MetaModel for "+prototype.toString());
			}	    
			return this;
		}

		
		@Override
		public StorageBoxBuilder feed(
				 Link prototype,
				 String creator,
				 String description){
			try {
				builder.feed(prototype, creator, description);
			} catch (InstantiationHookException e) {
				log.error("Unable to build the MetaModel for "+prototype.toString());
			}
			return this;
		}


		
		@Override
		public <E> StorageBoxBuilder feed(
				 AKey<? extends E> k, 
				 E prototype, 
				 String creator, 
				 String description, 
				 NodeItem owner){			
			builder.feed(k, prototype, creator, description, owner);
			return this;
		}
		
		
		
		/* (non-Javadoc)
		 * @see org.pickcellslab.foundationj.dbm.impl.StorageBoxBuilder#feed(org.pickcellslab.foundationj.datamodel.AKey, E, java.lang.String, java.lang.String, java.lang.Class, java.lang.String)
		 */
		@Override
		public <E> StorageBoxBuilder feed(
				 AKey<? extends E> k, 
				 E prototype, 
				 String creator, 
				 String description, 
				 Class<? extends NodeItem> ownerClazz,
				 String ownerType){
			builder.feed(k, prototype, creator, description, ownerClazz, ownerType);
			return this;
		}


		
		/* (non-Javadoc)
		 * @see org.pickcellslab.foundationj.dbm.impl.StorageBoxBuilder#run()
		 */
		@Override
		public Void run() throws DataAccessException{
			return session.runQuery(build());
		}
		

		private StorageBoxImpl build(){

			List<WritableDataItem> dataList = null;
			List<StepTraverser<NodeItem,Link>> trav = new ArrayList<>();

			if(metaIsOFF){
				dataList = standAlones;
				for(Entry<NodeItem, TraverserConstraints> e : roots.entrySet()){
					if(e.getValue()!=null)
						trav.add(Traversers.breadthfirst(e.getKey(), e.getValue()));
					else
						trav.add(Traversers.breadthFirst(e.getKey()));
				}
			}
			else{
				dataList = new MetaTrackingCollection(builder);
				dataList.addAll(standAlones);
				for(Entry<NodeItem, TraverserConstraints> e : roots.entrySet()){
					if(e.getValue()!=null)
						trav.add(new MetaTrackingTraverser(e.getKey(), e.getValue(), builder));
					else
						trav.add(new MetaTrackingTraverser(e.getKey(), builder));
				}
			}
			if(!dataList.isEmpty())
				description = description + "A collection of DataItem of size "+standAlones.size();

			return new StorageBoxImpl(trav, dataList, metaIsOFF, limit, forceUnknownId, description); 
		}






	}



}

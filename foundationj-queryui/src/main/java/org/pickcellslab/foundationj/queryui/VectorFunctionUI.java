package org.pickcellslab.foundationj.queryui;

import java.awt.CardLayout;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.functions.AngleMaker;
import org.pickcellslab.foundationj.datamodel.functions.ArrayFunction;
import org.pickcellslab.foundationj.datamodel.functions.ArrayOperation;
import org.pickcellslab.foundationj.datamodel.functions.CombinedFunction;
import org.pickcellslab.foundationj.datamodel.functions.Constant;
import org.pickcellslab.foundationj.datamodel.functions.DistanceMaker;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.VectorRotation;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.ValueSelector;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;

public class VectorFunctionUI<T extends DataItem, N extends Number> extends WizardNode{


	private static final String ITEM = "Item Vector", SOURCE = "Source Vector",
			LINK = "Link Vector", TARGET = "Target Vector", CONSTANT = "Manual Entry",
			VECTOR_X = "Norm X", VECTOR_Y = "Norm Y", VECTOR_Z = "Norm Z",
			DISTANCE = "Distance", ANGLE = "Angle", ROTATION = "Rotation", ARRAY_OP = "Array Operation";


	private final UITheme theme;
	
	private final MetaQueryable target;
	private	ExplicitFunction<T,N[]> left = null;
	private	ExplicitFunction<T,N[]> right = null;
	private	ExplicitFunction<T,N[]> vector = null;
	private int length;

	private final JComboBox<String> box1;
	private final JComboBox<ArrayOperation> box2;


	private final MetaInstanceDirector director;


	protected VectorFunctionUI(WizardNode parent, UITheme theme, MetaInstanceDirector director, MetaQueryable target) {
		super(parent);

		this.theme = theme;
		this.target = target;
		this.director = director;

		setLayout(new VerticalFlowLayout());


		final JLabel l1 = new JLabel("Type of Function : ");
		box1 = new JComboBox<>(new String[] {DISTANCE, ANGLE, ROTATION, ARRAY_OP});

		final GroupPanel fTypePanel = new GroupPanel(10, l1, box1);

		final JPanel cards = new JPanel();
		cards.setBorder(BorderFactory.createTitledBorder("Options"));
		final CardLayout lyt = new CardLayout();
		cards.setLayout(lyt);


		// Angle group
		final GroupPanel angleGroup = new GroupPanel(10, false, 
				new VectorSelector(target, "V1 : ", false, df->left=df),
				new VectorSelector(target, "V2 : ", true, df->right=df));
		// Distance group
		final GroupPanel distanceGroup = new GroupPanel(10, false,
				new VectorSelector(target, "V1 : ", false,df->left=df),
				new VectorSelector(target, "V2 : ", true,df->right=df));
		// Rotation group
		final GroupPanel rotationGroup = new GroupPanel(10, false, 
				new VectorSelector(target, "Z axis of new basis : ", true, df->left=df),
				new VectorSelector(target, "Y Axis of new basis : ", true, df->right=df),
				new VectorSelector(target, "Vector to be rotated : ", false, df->vector=df));
		
		// Array Operation panel		
		final JLabel l2 = new JLabel("Type of Operation : ");
		box2 = new JComboBox<>(ArrayOperation.values());
		final GroupPanel arrayOpGroup = new GroupPanel(10, false,
				new GroupPanel(10, true, l2, box2),
				new VectorSelector(target, "V1 : ", false,df->left=df),
				new VectorSelector(target, "V2 : ", true,df->right=df));

		cards.add(angleGroup, ANGLE);
		cards.add(distanceGroup, DISTANCE);
		cards.add(rotationGroup, ROTATION);
		cards.add(arrayOpGroup, ARRAY_OP);


		box1.addActionListener(l->lyt.show(cards, (String)box1.getSelectedItem()));


		add(fTypePanel);
		add(cards);
		
	}






	@Override
	public boolean validity() {
		return left!=null && right!=null && box1.getSelectedItem()==ROTATION ? vector!=null : true;
	}



	@Override
	protected WizardNode getNextStep() {
		if(box1.getSelectedItem() == ANGLE)		
			return new MetaDescriptionPanel<>(this, director, new CombinedFunction<>(left,right, new AngleMaker()), -1, target);
		else if(box1.getSelectedItem() == DISTANCE)	
			return new MetaDescriptionPanel<>(this, director, new CombinedFunction<>(left,right, new DistanceMaker()), -1, target);
		else if(box1.getSelectedItem() == ARRAY_OP)	
			return new MetaDescriptionPanel<>(this, director, new CombinedFunction<>(left,right, new ArrayFunction((ArrayOperation) box2.getSelectedItem())), length, target);
		else
			return new MetaDescriptionPanel<>(this, director, new VectorRotation<>(left, right, vector), length, target);
	}

	@Override
	public int numberOfForks() {
		return 1;
	}





	private class VectorSelector extends JPanel{


		public VectorSelector(MetaQueryable target, String label, boolean allowConstants, Consumer<ExplicitFunction> consumer) {

			//check if the target is an item or a link
			boolean isClass = false;
			if(MetaClass.class.isAssignableFrom(target.getClass()))
				isClass = true;



			final JLabel lbl = new JLabel(label+"    ");
			final JComboBox<String> cb = new JComboBox<>();
			cb.addItem(" ");
			if(allowConstants) {
				cb.addItem(CONSTANT);
				cb.addItem(VECTOR_X);
				cb.addItem(VECTOR_Y);
				cb.addItem(VECTOR_Z);
			}
			if(isClass)
				cb.addItem(ITEM);
			else{
				cb.addItem(SOURCE);
				cb.addItem(LINK);
				cb.addItem(TARGET);
			}


			add(lbl);
			add(cb);



			cb.addActionListener(l->{
				if(cb.getSelectedIndex() != 0){
					ExplicitFunction df = launchChoice(target, (String) cb.getSelectedItem());
					if(df!=null){
						consumer.accept(df);
						cb.insertItemAt(df.toString(), 0);
						cb.removeItemAt(1);					
					}else{
						cb.insertItemAt(" ", 0);
						cb.removeItemAt(1);
					}
					cb.setSelectedIndex(0);
				}
				fireNextValidityChanged();
			});

		}	






		private ExplicitFunction launchChoice(MetaQueryable target, String owner) {

			//Get the owner of the MetaKeys to select
			MetaQueryable mq = null;
			switch(owner){
			case ITEM : mq = target; break;
			case LINK : mq = target; break;
			case SOURCE : mq = ((MetaLink) target).source(); break;
			case TARGET : mq = ((MetaLink) target).target(); break;
			case VECTOR_X: return new Constant<>(new double[] {1,0,0});
			case VECTOR_Y: return new Constant<>(new double[] {0,1,0});
			case VECTOR_Z: return new Constant<>(new double[] {0,0,1});
			case CONSTANT : 

				//fakeKey for a ValueSelector
				AKey<?> fake = AKey.get("fake", double[].class);
				@SuppressWarnings({ "rawtypes", "unchecked" })
				ValueSelector<?> vs = new ValueSelector(fake);
				vs.setModal(true);
				vs.setVisible(true);

				Object value = vs.getValue();
				if(null == value){
					return null;
				}
				return new Constant<>(value);	
			}

			
			final List<MetaReadable<?>> readables = mq.getReadables()
					.filter(p->p.isArray() && p.dataType() == dType.NUMERIC).collect(Collectors.toList());
			if(readables.isEmpty()) {
				JOptionPane.showMessageDialog(this, owner + " has no valid attribute (array required)", 
						"No Valid Attribute", JOptionPane.PLAIN_MESSAGE);
				return null;
			}

			ReadableSelection ui = new ReadableSelection(theme, director.registry(), readables, true, true);
			ui.setModal(true);
			ui.pack();
			ui.setLocationRelativeTo(this);		
			ui.setVisible(true);

			ExplicitFunction df = null;

			Optional<Dimension> opt = (Optional)ui.getSelected();

			System.out.println("Optional selected = "+opt);

			if(opt.isPresent()){

				length = opt.get().length();

				switch(owner){
				case ITEM : df = opt.get(); break;
				case LINK : df = opt.get(); break;
				case SOURCE : df = F.applyOnSource((ExplicitFunction)opt.get()); break;
				case TARGET : df = F.applyOnTarget((ExplicitFunction)opt.get()); break;			
				}

				System.out.println("df = "+df.description());
			}

			return df;		
		}


	}


}

package org.pickcellslab.foundationj.queryui;

import java.util.ArrayList;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.PredictableMetaSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.ToStringFilterSetSplitter;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

@Data(typeId = "MetaGroupByFilter", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaSplitByFilter extends PredictableMetaSplit {

	private MetaSplitByFilter(String uid) {
		super(uid);
	}


	public static MetaSplitByFilter getOrCreate(MetaInstanceDirector director, List<MetaFilter> elements) throws InstantiationHookException {

		assert elements.size()!=0 : "elements cannot be an empty list";

		final MetaSplitByFilter split = director.getOrCreate(MetaSplitByFilter.class, MetaItem.randomUID(MetaSplitByFilter.class));

		if(!split.getAttribute(MetaModel.name).isPresent()){// Check if it was retrieved or if it already existed

			String d = "Group into categories -> ";
			for(MetaFilter mf : elements)
				d+=mf.name()+"|";

			split.setAttribute(MetaItem.name, "Categories for "+elements.get(0).getTarget().name());
			split.setAttribute(MetaItem.desc, d);

			elements.forEach(e->{
				new DataLink(MetaModel.requiresLink, split, e, true);
			});

		}

		return split;

	}

	
	
	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}
	
	
	



	@Override
	public String toHTML() {
		return "<HTML> GROUPING: " + name() 
		+ " <br> Description : "+ description()
		+ "</HTML>";
	}


	@Override
	public String[] getPrediction() {
		return getLinks(Direction.OUTGOING, MetaModel.requiresLink).map(l->((MetaFilter)l.target()).name()).toArray(String[]::new);
	}



	@Override
	public boolean pathSplitSupported() {
		return true;
	}

	@Override
	public boolean targetSplitSupported() {
		return true;
	}

	@Override
	public <E> ExplicitSplit<E, String> toSplitter() {

		final String d = this.getAttribute(MetaModel.desc).get();
		final List<ExplicitPredicate> predicates = new ArrayList<>();
		final List<String> names = new ArrayList<>();
		getLinks(Direction.OUTGOING, MetaModel.requiresLink).forEach(l->{
			final MetaFilter mf = (MetaFilter)l.target();
			predicates.add(mf.toFilter());
			names.add(mf.name());
		});
				
		return new ToStringFilterSetSplitter(d, predicates.toArray(new ExplicitPredicate[predicates.size()]), names.toArray(new String[names.size()]));
	}
	

	@Override
	public <V, E> ExplicitPathSplit<V, E, String> toPathSplitter() {

		final boolean isLink = ((MetaFilter) getLinks(Direction.OUTGOING, MetaModel.requiresLink).iterator().next().target()).getTarget() instanceof MetaLink;
		final LinkedHashSet<ExplicitPredicate<WritableDataItem>> predicates = new LinkedHashSet<>();
		final Map<ExplicitPredicate<WritableDataItem>, String>	acceptMap = new HashMap<>();		
		final String d = this.getAttribute(MetaModel.desc).get();


		getLinks(Direction.OUTGOING, MetaModel.requiresLink)
		.forEach(l->{
			ExplicitPredicate<WritableDataItem> p = ((MetaFilter)l.target()).toFilter();
			predicates.add(p);
			acceptMap.put(p, ((MetaFilter)l.target()).name());
		});


		return new ExplicitPathSplit<V, E, String>(){

			private FjAdaptersFactory<V, E> f;

			@Override
			public String description() {
				return d;
			}

			@Override
			public String getKeyFor(Path<V, E> p) {

				if(isLink){
					Iterator<E> ns = p.edges();
					while(ns.hasNext()){
						Link i = f.getLinkReadOnly(ns.next());
						for(Predicate<WritableDataItem> pr : predicates){
							if(pr.test(i))
								return acceptMap.get(pr);
						}
					}
				}
				else{
					Iterator<V> ns = p.nodes();
					while(ns.hasNext()){
						NodeItem i = f.getNodeReadOnly(ns.next());
						for(Predicate<WritableDataItem> pr : predicates){
							if(pr.test(i))
								return acceptMap.get(pr);
						}
					}
				}
				return ExplicitSplit.OTHER;
			}

			@Override
			public void setFactory(FjAdaptersFactory<V, E> f) {
				this.f = f;
			}

		};
	}



}

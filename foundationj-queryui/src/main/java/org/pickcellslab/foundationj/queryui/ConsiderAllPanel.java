package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReductionOperation;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.CardPanel;
import org.pickcellslab.foundationj.ui.wizard.Copyable;
import org.pickcellslab.foundationj.ui.wizard.HighlightListener;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.pickcellslab.foundationj.ui.wizard.ValidityListener;
import org.pickcellslab.foundationj.ui.wizard.ValidityToggle;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

import com.alee.extended.panel.AccordionListener;
import com.alee.extended.panel.WebAccordion;
import com.alee.extended.panel.WebAccordionStyle;
import com.alee.extended.panel.WebCollapsiblePane;

@SuppressWarnings("serial")
public class ConsiderAllPanel extends ValidityTogglePanel implements ValidityListener, HighlightListener, Copyable<ConsiderAllPanel>{

	private final UITheme theme;
	private final DataRegistry registry;

	private Optional<MetaQueryable> mq;
	private final JLabel lblNone;
	private boolean currentFilter = true;
	private boolean currentGrouping = true;
	private boolean currentMecha = true;
	private final WebCollapsiblePane  filteringPane;
	private final WebCollapsiblePane  groupingPane;
	private final WebCollapsiblePane mechaPane; 
	private final CardPanel<MetaQueryable, SplitterChoicePanel> groupingCard;
	private final CardPanel<MetaQueryable, CombineFilterPanel> filterCard;
	private final CardPanel<MetaQueryable, MechanismChoicePanel> mechaCard;

	
	public ConsiderAllPanel(UITheme theme, DataRegistry registry, ChoiceSet choices, QueryWizardConfig config, boolean includeReduction) {

		this.theme = theme;
		this.registry = registry;
				
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder(""));

		JPanel targetPanel = new JPanel();
		targetPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		targetPanel.setLayout(new BoxLayout(targetPanel,BoxLayout.X_AXIS));

		add(targetPanel, BorderLayout.NORTH);


		JLabel lblTargetType = new JLabel("Target Type : ");
		lblTargetType.setBorder(BorderFactory.createEmptyBorder(20,10,20,10));
		targetPanel.add(lblTargetType);

		lblNone = new JLabel("None");
		lblNone.setBorder(BorderFactory.createEmptyBorder(20,10,20,10));
		targetPanel.add(lblNone);

		


		WebAccordion accordion = new WebAccordion(WebAccordionStyle.accordionStyle);
		accordion.setMultiplySelectionAllowed ( false );
		add(accordion, BorderLayout.CENTER);


		//Create CardPanel with filtering choice
		//Factory for a new filter choice
		Function<MetaQueryable, CombineFilterPanel> filterPanelFactory = (q)->{
			final ChoicePanel<MetaFilter> master = choices.getFilterChoice(q);
			CombineFilterPanel<Object> cfp = new CombineFilterPanel<>(master);
			cfp.addValidityListener(this);
			return cfp;
		};

		filterCard = new CardPanel<>(filterPanelFactory);
		JScrollPane filterScroll = new JScrollPane();
		filterScroll.setViewportView(filterCard);
		filteringPane = accordion.addPane(null,"Filtering",filterScroll);



		//Factory for a new SplitterChoice
		Function<MetaQueryable, SplitterChoicePanel> splitterPanelFactory = (q)->{
			SplitterChoicePanel scp = new SplitterChoicePanel(theme, registry, choices, q, false, !includeReduction);
			scp.addValidityListener(this);
			return scp;
		};
		groupingCard = new CardPanel<>(splitterPanelFactory);
		JScrollPane groupingScroll = new JScrollPane();
		groupingScroll.setViewportView(groupingCard);
		groupingPane = accordion.addPane("Grouping", groupingScroll);

		if(!config.isGroupingEnabled())
			groupingPane.setEnabled(false);




		// Add reduction pane if required
		if(includeReduction){
			//Factory for a new SplitterChoice
			Function<MetaQueryable, MechanismChoicePanel> mechaPanelFactory = (q)->{
				MechanismChoicePanel mcp = new MechanismChoicePanel(theme, registry, choices.getReadableChoice(q), false);
				mcp.addValidityListener(this); 
				return mcp;
			};
			mechaCard = new CardPanel<>(mechaPanelFactory);
			mechaPane = accordion.addPane("Reduction Operation", mechaCard);
		}
		else{
			mechaCard = null;
			mechaPane = null;
		}







		accordion.addAccordionListener(new AccordionListener(){

			@Override
			public void selectionChanged() {
				if(accordion.getSelectedPanes().get(0) == filteringPane)
					choices.highlightFilter(mq.orElse(null));
			}

		});














		JButton btnChange = new JButton("Change");

		btnChange.addActionListener(l->{
			if(mq!=null && mq.equals(choices.getCurrentQueryable())){
				choices.highlightMaster();
				return;
			}

			mq = choices.getCurrentQueryable();
			if(!mq.isPresent()){				
				choices.highlightMaster();
				JOptionPane.showMessageDialog(null, "Please Select a target in the \"Available Types\" panel");
				return;
			}
			else{		
				lblNone.setText(mq.get().name()+"             ");
				lblNone.setIcon(MetaToIcon.get(mq.get(), registry, theme, 16));
				filterCard.show(mq.get());
				groupingCard.show(mq.get());
				if(mechaPane!=null)
					mechaCard.show(mq.get());
				filteringPane.expand();
				choices.highlightFilter(mq.get());
			}
			this.fireNextValidityChanged();
			if(currentFilter = filterCard.current().validity())
				filteringPane.setIcon(theme.icon(IconID.Misc.VALID, 16));
			else
				filteringPane.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16));
			if(currentGrouping = groupingCard.current().validity())
				groupingPane.setIcon(theme.icon(IconID.Misc.VALID, 16));
			else
				groupingPane.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16));
		});
		targetPanel.add(btnChange);


	}




	@Override
	public void nextValidityChanged(ValidityToggle v) {
		if(SplitterChoicePanel.class.isAssignableFrom(v.getClass())){
			currentGrouping = v.validity();
			if(currentGrouping)
				groupingPane.setIcon(theme.icon(IconID.Misc.VALID, 16));
			else
				groupingPane.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16));
		}
		else if(filteringPane.isExpanded()){
			currentFilter = v.validity();
			if(currentFilter)
				filteringPane.setIcon(theme.icon(IconID.Misc.VALID, 16));
			else
				filteringPane.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16));
		}
		else{
			currentMecha = v.validity();
			if(currentMecha)
				mechaPane.setIcon(theme.icon(IconID.Misc.VALID, 16));
			else
				mechaPane.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16));
		}

		this.fireNextValidityChanged();
	}



	@Override
	public boolean validity() {
		if(mq == null || !mq.isPresent())
			return false;
		return currentFilter && currentGrouping && currentMecha;
	}



	public MetaQueryable getTarget(){
		return mq.get();
	}


	public Optional<MetaFilter> getMetaFilter(Meta meta) {

		MetaFilter mf = null; 

		Optional<ExplicitPredicate> oP = (Optional)filterCard.current().getPredicate();
		if(oP.isPresent()){
			try {
				mf = MetaFilter.getOrCreate(
						meta.getInstanceManager(),
						filterCard.current().name(),
						filterCard.current().expression(),
						oP.get(),
						mq.get()
						);
				mf.setTransient();
			} catch (NonDataMappingException e) {
				ErrorInfo error = 
						new ErrorInfo(
								"Custom Annotation Error", 
								"Unable to create the custom filter",
								null, null,
								e,
								Level.SEVERE,
								null);
				JXErrorPane.showDialog(null, error);
				mf = null;
			}
		}

		return Optional.ofNullable(mf);
	}


	public Optional<MetaSplit> getGrouping(Meta meta){
		return Optional.ofNullable(groupingCard.current().getSplitter(meta));
	}



	public Optional<MetaReductionOperation> getMechanism(Meta meta) {
		return Optional.ofNullable(mechaCard.current().getMechanism(meta));
	}





	@Override
	public void highlighted(Highlightable source, boolean isHighlighted) {
		// TODO Auto-generated method stub

	}




	@SuppressWarnings("unchecked")
	@Override
	public void copyState(ConsiderAllPanel other) {		

		this.mq = Optional.ofNullable(other.mq.orElse(null));
		this.currentFilter = other.currentFilter;
		this.currentGrouping = other.currentGrouping;

		if(mq.isPresent()){

			lblNone.setText(mq.get().name()+"             ");
			lblNone.setIcon(MetaToIcon.get(mq.get(), registry, theme, 16));

			groupingCard.show(mq.get());
			groupingCard.current().copyState(other.groupingCard.current());

			filterCard.show(mq.get());
			filterCard.current().copyState(other.filterCard.current());

			if(other.filteringPane.isExpanded())
				this.filteringPane.expand();
			if(other.groupingPane.isExpanded())
				this.groupingPane.expand();

		}



	}




}

package org.pickcellslab.foundationj.datamodel.functions;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;

@Mapping(id = "SpecialLinks")
public class StreamSpecificLinks<T extends NodeItem> implements ExplicitLinkStream<T> {

	private final String[] types;
	
	private final Direction direction;
	
	public StreamSpecificLinks(Direction dir, String... types) {
		Objects.requireNonNull(dir);
		Objects.requireNonNull(types);
		this.direction = dir;
		this.types = types;
	}
	
	
	@Override
	public Stream<Link> apply(T t) {
		return t.getLinks(direction, types);
	}

	@Override
	public String description() {
		return types.length == 0 ? "links with direction "+direction.name() :
			"links with types "+Arrays.toString(types)+" and direction "+direction.name();
	}


	@Override
	public String toString() {
		return "-"+Arrays.toString(types)+" "+direction.name()+"->";
	}
	

}

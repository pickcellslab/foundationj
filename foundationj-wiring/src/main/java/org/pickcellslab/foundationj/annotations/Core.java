package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * Marks an interface whose implementation provides a function essential to the full application.
 * <br> At runtime, the booting process will ensure that one and only one implementation is available in the class path. 
 * If none is found, booting will fail. If several are found warnings will be generated and an arbitrary choice will be made. 
 * 
 * <b>NB:</b> An interface annotated with {@link Core} must also be annotated with either {@link Scope} or {@link Service}
 * 
 * @see Scope
 * @see Service
 * @see Modular
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(CLASS)
@Target(TYPE)
public @interface Core {

}

package org.pickcellslab.foundationj.dbm.queries.builders;

import java.util.Collection;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;

public interface StorageBoxBuilder {

	/**
	 * Adds the specified {@link NodeItem} to the items that need to be stored. All the items that are linked to
	 * the specified item will also be stored (or updated if they already exist in the database). Be aware when 
	 * using this method that the graph the specified item is part of may be big and that therefore the query might
	 * take up time to be accomplished. If you wish to have more control on which elements of the graph to store or update
	 *, you should use {@link #add(NodeItem, TraverserConstraints)}
	 * @param item
	 * @return the updated Query
	 */
	StorageBoxBuilder add(NodeItem item);

	/**
	 * Adds the specified {@link NodeItem} to the items that need to be stored. The provided {@link TraverserConstraints}
	 * will be used to filter the graph the provided NodeItem is part of. Only the components that pass the filter will
	 * be stored or updated
	 * @param item
	 * @return the updated Query
	 */
	StorageBoxBuilder add(NodeItem item, TraverserConstraints constraints);

	/**
	 * Calling this method will force the database to write objects with a database id which is unknown to this database.
	 * Warning: call this method only if you know very well what you are doing.
	 * @return The updated BoxBuilder
	 */
	StorageBoxBuilder forceUnknownId();

	/**
	 * Adds the specified {@link WritableDataItem} to the items that need to be stored (or updated). Here, even if
	 * the provided DataItem is a {@link NodeItem}, only the node itself will be stored and the connected 
	 * components will be ignored. This method is equivalent to calling {@link #add(NodeItem, TraverserConstraints)}
	 * with constraints : exclude all links
	 * @param item
	 * @return the updated Query
	 */
	StorageBoxBuilder addStandAlone(WritableDataItem item);

	/**
	 * This method allows to set the maximum number of nodes / links to be committed in one go.
	 * Default is 20000. Setting the limit lower may prevent a complete rollback in case of failure
	 * of the transaction. However, in some cases, it might be necessary to prevent OutOfMemory errors
	 * by creating periodic commits for a single query.
	 * @param limit The limit to set.
	 * @return The update builder.
	 */
	StorageBoxBuilder setMaxCommitSize(int limit);

	/**
	 * If you do not wish the {@link MetaImpl} to be created when storing the items,
	 * you must call this method. The MetaModel is otherwise created by default
	 */
	StorageBoxBuilder turnMetaModelOff();

	/**
	 * Adds the provided items as stand alone items to store (Connected items are ignored).
	 * @param standAloneItems A Collection of items to store
	 * @return The updated builder
	 * @see #addStandAlone(WritableDataItem)
	 */
	StorageBoxBuilder addAll(Collection<? extends WritableDataItem> standAloneItems);

	StorageBoxBuilder feed(NodeItem prototype, String creator, String description);

	StorageBoxBuilder feed(Link prototype, String creator, String description);

	/**
	 * Feeds the {@link MetaInfo} with the specified information
	 * @param k The AKey object to meta model
	 * @param prototype A prototype object stored with the AKey object
	 * @param creator The name of the module which created the AKey
	 * @param description A description of the stored object
	 * @param owner An example instance of the NodeItem to document or its corresponding MetaClass 
	 */
	<E> StorageBoxBuilder feed(AKey<? extends E> k, E prototype, String creator, String description, NodeItem owner);

	<E> StorageBoxBuilder feed(AKey<? extends E> k, E prototype, String creator, String description,
			Class<? extends NodeItem> ownerClazz, String ownerType);

	Void run() throws DataAccessException;

}
package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.util.Objects;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.functions.AngleMaker;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.reductions.CountOperation;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.datamodel.reductions.GetOne;
import org.pickcellslab.foundationj.datamodel.reductions.RootReferential;
import org.pickcellslab.foundationj.datamodel.reductions.SingleStatistics;
import org.pickcellslab.foundationj.datamodel.reductions.SingleStatisticsArray;
import org.pickcellslab.foundationj.datamodel.reductions.StatsType;
import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaReductionOperation;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.slf4j.LoggerFactory;

public class MechanismChoicePanel extends ValidityTogglePanel{


	private final ButtonGroup buttonGroup = new ButtonGroup();

	private final JRadioButton rbStats;
	private final JRadioButton rbAngle;
	private final JRadioButton rbMean;
	private final JRadioButton rbCount;
	private final JRadioButton rbOne;
	private final SplitByKeyPanel mrChoiceStats;
	private final BiKeyPanel mrChoiceAngle;
	private final SplitByKeyPanel mrChoiceMean;
	private final SplitByKeyPanel mrChoiceOne;

	private SplitByKeyPanel mrChoiceSTD;
	private JRadioButton rbStd;

	private SplitByKeyPanel mrChoiceSUM;
	private JRadioButton rbSum;

	private SplitByKeyPanel mrChoiceMIN;
	private JRadioButton rbMin;

	private SplitByKeyPanel mrChoiceMAX;
	private JRadioButton rbMax;

	private static final String COUNT = "COUNT", ONE = "ONE", 
			MEAN = "MEAN", SUM = "SUM", STD = "STD", MIN = "MIN", MAX = "MAX",  
			STATS = "STATS", ANGLE = "ANGLE"; 

	public MechanismChoicePanel(UITheme theme, DataRegistry registry, ChoicePanel<MetaReadable<?>> targetReadsChoices, boolean includePathSpecificReductions) {
		this(theme, registry, targetReadsChoices,targetReadsChoices, includePathSpecificReductions);
	}


	public MechanismChoicePanel(UITheme theme, DataRegistry registry, ChoicePanel<MetaReadable<?>> sourceReadsChoices, ChoicePanel<MetaReadable<?>> targetReadsChoices, boolean includePathSpecificReductions) {

		Objects.requireNonNull(sourceReadsChoices);
		Objects.requireNonNull(targetReadsChoices);

		setLayout(new BorderLayout(0, 0));


		JPanel radioPanel = new JPanel();
		radioPanel.setBorder(new TitledBorder(null, "Reduction Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(radioPanel, BorderLayout.NORTH);
		radioPanel.setLayout(new GridLayout(2, 1, 0, 2));

		JPanel configPanel = new JPanel();
		CardLayout card = new CardLayout(0, 0);
		configPanel.setLayout(card);

		// Counter Factory
		configPanel.add(new JLabel("         Count The Number of objects encountered as Target of the Path     "), COUNT);
		rbCount = new JRadioButton("Counter");
		radioPanel.add(rbCount);
		buttonGroup.add(rbCount);
		rbCount.addActionListener(l->{			
			card.show(configPanel, COUNT);
			this.fireNextValidityChanged();			
		});

		rbCount.setSelected(true);


		// Get One Factory
		mrChoiceOne = new SplitByKeyPanel<>(theme, registry, targetReadsChoices);
		mrChoiceOne.addValidityListener(l->this.fireNextValidityChanged());

		configPanel.add( mrChoiceOne, ONE );
		rbOne = new JRadioButton("Get One");
		rbOne.setToolTipText("Retrieve One Attribute from one target of the path. (If multiple objects are encountered, the first one is retrieved)");
		radioPanel.add(rbOne);
		buttonGroup.add(rbOne);
		rbOne.addActionListener(l->{			
			card.show(configPanel, ONE);
			this.fireNextValidityChanged();			
		});

		// Mean Factory
		mrChoiceMean = new SplitByKeyPanel<>(theme, registry, targetReadsChoices);
		mrChoiceMean.addValidityListener(l->this.fireNextValidityChanged());

		configPanel.add( mrChoiceMean, MEAN );
		rbMean = new JRadioButton("Mean");
		radioPanel.add(rbMean);
		buttonGroup.add(rbMean);
		rbMean.addActionListener(l->{			
			card.show(configPanel, MEAN);
			this.fireNextValidityChanged();			
		});




		// Std Factory
		mrChoiceSTD = new SplitByKeyPanel<>(theme, registry, targetReadsChoices);
		mrChoiceSTD.addValidityListener(l->this.fireNextValidityChanged());

		configPanel.add( mrChoiceSTD, STD );
		rbStd = new JRadioButton("Std");
		radioPanel.add(rbStd);
		buttonGroup.add(rbStd);
		rbStd.addActionListener(l->{			
			card.show(configPanel, STD);
			this.fireNextValidityChanged();			
		});


		// Sum Factory
		mrChoiceSUM = new SplitByKeyPanel<>(theme, registry, targetReadsChoices);
		mrChoiceSUM.addValidityListener(l->this.fireNextValidityChanged());

		configPanel.add( mrChoiceSUM, SUM );
		rbSum = new JRadioButton("Sum");
		radioPanel.add(rbSum);
		buttonGroup.add(rbSum);
		rbSum.addActionListener(l->{			
			card.show(configPanel, SUM);
			this.fireNextValidityChanged();			
		});



		// Min Factory
		mrChoiceMIN = new SplitByKeyPanel<>(theme, registry, targetReadsChoices);
		mrChoiceMIN.addValidityListener(l->this.fireNextValidityChanged());

		configPanel.add( mrChoiceMIN, MIN );
		rbMin = new JRadioButton("Min");
		radioPanel.add(rbMin);
		buttonGroup.add(rbMin);
		rbMin.addActionListener(l->{			
			card.show(configPanel, MIN);
			this.fireNextValidityChanged();			
		});


		// Max Factory
		mrChoiceMAX = new SplitByKeyPanel<>(theme, registry, targetReadsChoices);
		mrChoiceMAX.addValidityListener(l->this.fireNextValidityChanged());

		configPanel.add( mrChoiceMAX, MAX );
		rbMax = new JRadioButton("Max");
		radioPanel.add(rbMax);
		buttonGroup.add(rbMax);
		rbMax.addActionListener(l->{			
			card.show(configPanel, MAX);
			this.fireNextValidityChanged();			
		});




		// Summary Statistics Factory
		mrChoiceStats = new SplitByKeyPanel<>(theme, registry, targetReadsChoices);
		mrChoiceStats.addValidityListener(l->this.fireNextValidityChanged());


		configPanel.add( mrChoiceStats, STATS );
		rbStats = new JRadioButton("Summary Statistics");
		radioPanel.add(rbStats);
		buttonGroup.add(rbStats);
		rbStats.addActionListener(l->{			
			card.show(configPanel, STATS);
			this.fireNextValidityChanged();			
		});


		mrChoiceAngle = new BiKeyPanel<>(theme, registry, sourceReadsChoices, targetReadsChoices);
		mrChoiceAngle.addValidityListener(l->this.fireNextValidityChanged());


		configPanel.add( mrChoiceAngle, ANGLE );
		rbAngle = new JRadioButton("Angle Statistics");
		radioPanel.add(rbAngle);
		buttonGroup.add(rbAngle);
		rbAngle.addActionListener(l->{			
			card.show(configPanel, ANGLE);
			this.fireNextValidityChanged();			
		});


		add(configPanel,BorderLayout.CENTER);


	}

	@Override
	public boolean validity() {
		if(rbCount.isSelected())
			return true;
		else if(rbOne.isSelected())
			return mrChoiceOne.validity();
		else if(rbMean.isSelected())
			return mrChoiceMean.validity();
		else if(rbStd.isSelected())
			return mrChoiceSTD.validity();
		else if(rbSum.isSelected())
			return mrChoiceSUM.validity();
		else if(rbMin.isSelected())
			return mrChoiceMIN.validity();
		else if(rbMax.isSelected())
			return mrChoiceMAX.validity();
		else if(rbAngle.isSelected())
			return mrChoiceAngle.validity();
		return mrChoiceStats.validity();
	}


	@SuppressWarnings("unchecked")
	public MetaReductionOperation getMechanism(Meta meta) {
		final MetaInstanceDirector director = meta.getInstanceManager();
		try {

			if(rbCount.isSelected())
				return MetaReductionOperation.getOrCreate(director, "Counter", new CountOperation<>(), -1);
			else if(rbOne.isSelected()) {
				final MetaReadable<?> mr = mrChoiceOne.getChosenReadable();
				return MetaReductionOperation.getOrCreate(director, new GetOne<>(mr.getRaw()), mr);
			}
			else if(rbAngle.isSelected()) {
				final MetaReadable<?> mr1 = mrChoiceAngle.getChosenReadable1();
				final MetaReadable<?> mr2 = mrChoiceAngle.getChosenReadable2();
				return MetaReductionOperation.getOrCreate(
						director,
						"Angle between "+mr1.name()+" and "+mr2.name(),
						new RootReferential(
								mr1.getRaw(),
								mr2.getRaw(),
								new AngleMaker(),
								new SingleStatistics<>(StatsType.MEAN)),
						mr1, mr2);
			}
			else {
				MetaReadable<?> mr = null;			
				StatsType stats = null;
				if(rbMean.isSelected()) {
					mr = mrChoiceMean.getChosenReadable();
					stats = StatsType.MEAN;
				}
				else if(rbStd.isSelected()) {
					mr = mrChoiceSTD.getChosenReadable();
					stats = StatsType.STD;					
				}
				else if(rbSum.isSelected()){
					mr = mrChoiceSUM.getChosenReadable();
					stats = StatsType.SUM;					
				}
				else if(rbMin.isSelected()){
					mr = mrChoiceMIN.getChosenReadable();
					stats = StatsType.MIN;					
				}				
				else if(rbMax.isSelected()){
					mr = mrChoiceMAX.getChosenReadable();
					stats = StatsType.MAX;					
				}
				
				ExplicitReduction<DataItem,?> op;
				if(mr.isArray())
					op = new SingleStatisticsArray((ExplicitFunction<DataItem, ? extends Number>) mr.getRaw(), stats);
				else
					op = SingleStatistics.of((ExplicitFunction<DataItem, ? extends Number>) mr.getRaw(), stats);
				
				return MetaReductionOperation.getOrCreate(director, op, mr);
				
			}
		}catch(InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		} catch (NonDataMappingException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a Non data Mapping", e);
			JOptionPane.showMessageDialog(this, "Unable to create a Non data Mapping, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}

}

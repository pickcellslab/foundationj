package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.impl.MonolithicSearchDefinition.Builder;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;


class MonolithicQBuilder<V,E,T,O> implements AccessChoice<T,QueryRunner<O>>, QueryRunner<O>{

	private final Builder<V,E,T> delegate;
	private final Action<? super T,O> action;
	private final TargetType<V,E,T> target;
	private String name;

	MonolithicQBuilder(TargetType<V,E,T> target, Action<? super T,O> action, String name){
		this.delegate = new MonolithicSearchDefinition.Builder<>(target);
		this.action = action;
		this.target = target;
		this.name = name;
	}	



	@Override
	public QueryRunner<O> getAll() {
		return this;
	}

	@Override
	public QueryRunner<O> useFilter(ExplicitPredicate<? super T> test) {
		delegate.setFilter(test);
		return this;
	}
	


	@Override
	public QueryRunner <O> useTraversal(TraversalDefinition definition) {
		assert definition != null;
		target.checkTraversalDefinition(definition);
		delegate.setTraversal(definition);
		return this;
	}

	public QueryRunner <O> useTraversals(SuppliedTraversalOperation operations) throws IllegalStateException {
		delegate.setTraversalOperation(operations);
		return this;
	}

	@Override
	public O run() throws DataAccessException {	
		return 
				target
				.session()
				.runQuery(new MonolithicSearchQuery<>(delegate.build(), action, name))
				.get();
	}

	


}

package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractDimensionalModel<T,B extends DimensionBroker<T,D>,D> extends AbstractModel<T> implements RedefinableDimensionsModel<T,B,D> {

	private final List<BrokerChangedListener<T,B,D>> bLstrs = new ArrayList<>();
		
	private final Map<String, B> brokers = new HashMap<>();
	


	@Override
	public B dimensionBroker(String series) {
		return brokers.get(series);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public DimensionalDataSet<T,D> getDataSet(){
		return (DimensionalDataSet<T, D>) dataset;
	}

	@Override
	public void setDataSet(DimensionalDataSet<T,D> ds){		
		if(!(ds instanceof DimensionalDataSet))
			throw new IllegalArgumentException("The given dataset must be dimensional");
		@SuppressWarnings("unchecked")
		DimensionalDataSet<T,D> oldDataSet = (DimensionalDataSet<T, D>) dataset;
		dataset = ds;
		brokers.clear();
		//TODO bLstrs.forEach(l->l.brokerChanged(this));
		dataSetHasChanged();
		this.fireDataEvt(oldDataSet, dataset);
	}
	
	
	
	



	@Override
	public void setBroker(String series, B broker) {
			B prev = brokers.put(series, broker);
			bLstrs.forEach(l->l.brokerChanged(this,series, prev, broker));
	}



	@Override
	public void addBrokerChangedListener(BrokerChangedListener<T,B,D> l) {
		bLstrs.add(l);
	}



	@Override
	public void removeBrokerChangedListener(BrokerChangedListener<T,B,D> l) {
		bLstrs.remove(l);
	}





	

}

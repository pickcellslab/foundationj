package org.pickcellslab.foundationj.optim.cspImpl;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;
import org.pickcellslab.foundationj.optim.csp.Solver;
import org.pickcellslab.foundationj.optim.csp.Validator;
import org.pickcellslab.foundationj.optim.staging.Agent;
import org.pickcellslab.foundationj.optim.staging.Director;


/**
 * An {@link ExclusiveRecruiter} is very similar to {@link Recruiter} except that it will not permit
 * two {@link Director} to depend on the same {@link Agent}
 * 
 * @author Guillaume Blin
 *
 * @param <N> The type of node which must also implement {@link Agent} and {@link Director} with the same type
 * of requirement and capability.
 * @param <I> The type of requirements and capability of Agents and director.
 * @param <E> The type of edge linking the nodes.
 */
public class ExclusiveRecruiter

<N extends Node<E> & Agent<I> & Director<I>, //Must implements all these interfaces
I,//The type of inputs for Agents and directors must be the same
E extends Edge<? extends N>> //edges can be any type

implements Validator<N,E> {




	@Override
	public boolean isValid(Solver<N, E> source, Collection<Solver<N, E>> neighbours) {
		//TODO
				return false;
	}



	@Override
	public boolean potential(Solver<N, E> source, Solver<N, E> neighbour) {
		//TODO
		return false;
	}



}

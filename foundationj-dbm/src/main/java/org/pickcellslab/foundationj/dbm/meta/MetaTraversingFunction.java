package org.pickcellslab.foundationj.dbm.meta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToLinksIterator;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToNodesIterator;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.AKeyInfo;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;

/**
 * 
 * A {@link MetaReadable} which applies a {@link ExplicitFunction} on its target. 
 * As the term "traversing" in the name indicates, this object often represents a mean to traverse the graph from the {@link NodeItem} 
 * it is applied on in order to perform a reducing operation on the values collected among the objects encountered during the traversal.
 * For example: a {@link MetaTraversingFunction} could describe a method to collect summary statistics on the distance between objects belonging to a 
 * same cluster by starting from Cluster, then moving towards the clustered objects and further to the link connecting neighbouring clustered objects
 * and collecting the property "distance" stored within these links and returning the stats about this set of variables. 
 * This type of MetaReadable is generally used once (cast on its target type) and then hidden (kept hidden to not distract while still remaining available
 * for import into another database).
 * 
 *  
 * @author Guillaume Blin
 *
 * @param <T>
 */


@Data(typeId = "MetaTraversingFunction", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaTraversingFunction<T> extends MetaItem implements MetaCastable<T> {

	/**
	 * Type of the links between the MetaTraversingFunction and its dependencies (traversal, splits, target..)
	 */
	public static final String STRATEGY = "STRATEGY", GROUPING = "GROUPED", REDUCTION = "REDUCTION";


	// Cache
	private CachingDimension<T> raw;  // weakref?


	private MetaTraversingFunction(String uid){
		super(uid);
	}


	public static <T> MetaTraversingFunction<T> getOrCreate(
			MetaInstanceDirector director,
			String name, 
			String description,
			MetaTraversal strategy,
			PredictableMetaSplit split,
			MetaReductionOperation reduction,
			MetaQueryable usedOn
			) {


		Objects.requireNonNull(strategy, "strategy is null");
		Objects.requireNonNull(reduction, "reduction is null");
		Objects.requireNonNull(usedOn, "usedOn is null");

		try{


			System.out.println("Creating MTF");

			//final MetaTraversingFunction mtf = meta.getOrCreate(MetaTraversingFunction.class, "MetaTraversingFunction : "+name+"/"+usedOn.name()+"/"+strategy.getTarget().name() 
			//		+ (split == null ? "" : "/"+split.name()));
			final MetaTraversingFunction mtf = director.getOrCreate(MetaTraversingFunction.class, "MTFunction_" + UUID.randomUUID().getMostSignificantBits());

			if(!mtf.getAttribute(MetaItem.name).isPresent()){// Check if it was retrieved or if it already existed

				mtf.setAttribute(MetaItem.name, name);
				mtf.setAttribute(MetaItem.desc, description);

				// Transform the name of the mechanism to be more informative to the user
				//mechanisms.setAttribute(MetaItem.name, name + " " + mechanisms.getAttribute(MetaItem.name).get());


				new DataLink(STRATEGY, mtf, strategy, true);
				new DataLink(MetaModel.requiresLink, mtf, strategy, true);
				new DataLink(REDUCTION, mtf, reduction, true);
				new DataLink(MetaModel.requiresLink, mtf, reduction, true);
				new DataLink(MetaReadable.APPLICABLE_TO, mtf, usedOn, true);
				new DataLink(MetaModel.requiresLink, usedOn, mtf, true);

				if(split!=null) {
					new DataLink(GROUPING, mtf, split, true);
					new DataLink(MetaModel.requiresLink, mtf, split, true);
				}

			}

			return mtf;

		} catch(Exception e){
			throw new RuntimeException(e);
		}

	}






	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.creator, MetaModel.desc);
	}





	@Override
	public String toString(){
		return name()+" (Graph Function)";
	}



	@Override
	public dType dataType() {
		return getMechanismFactory().generatedKey().dType();
	}

	@Override
	public MetaQueryable owner() {
		return (MetaQueryable) typeMap.get(MetaReadable.APPLICABLE_TO).iterator().next().target();
	}

	@Override
	public int numDimensions() {
		return getRaw().numDimensions();
	}


	@Override
	public Dimension<DataItem,T> getDimension(int index) {
		if(index<0 || index>=numDimensions())
			throw new ArrayIndexOutOfBoundsException(index);
		return getRaw().getDimension(index);
	}



	@Override
	public CachingDimension<T> getRaw() {
		if(raw == null)
			raw = TraversingDimensionFactory.create(this);
		return raw;
	}




	@Override
	public String toHTML() {
		return "<HTML>"+
				"<p><strong>MetaTraversingFunction</strong>"+
				"<ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"</li></p></HTML>";
	}



	@Override
	public Class<?> dataClass() {
		return getRaw().getReturnType();
	}


	@Override
	public void cast(DataAccess access) throws DataAccessException {

		Objects.requireNonNull(access, "Access is null");


		final MetaQueryable owner = owner();

		final Updater u = createUpdater();

		owner.updateData(access, u);

		// Now delete this MetaFunction		
		access.queryFactory().deleteAnnotation(this);

		//this.setHidden(true);
		//access.queryFactory().meta(this).run();

	}


	@Override
	public void cast(DataAccess access, ExplicitPredicate<WritableDataItem> p) throws DataAccessException {

		Objects.requireNonNull(access, "Access is null");

		final MetaQueryable owner = owner();
		final Updater u = createUpdater();

		owner.initiateUpdate(access, u).useFilter(p).run();

		// Now hide
		access.queryFactory().deleteAnnotation(this);

		//this.setHidden(true);
		//access.queryFactory().meta(this).run();
	}


	@Override
	public void cast(DataAccess access, TraversalDefinition td) throws DataAccessException, IllegalArgumentException {

		Objects.requireNonNull(access, "Access is null");

		final MetaQueryable owner = owner();
		final Updater u = createUpdater();

		owner.initiateUpdate(access, u).useTraversal(td).run();

		// Now hide
		access.queryFactory().deleteAnnotation(this);

		//this.setHidden(true);
		//access.queryFactory().meta(this).run();
	}


	private Updater createUpdater(){

		// First perform the update operation
		final CachingDimension<T> td = getRaw();		
		final AKey<?> k = td.createKey();
		final List<AKeyInfo> updates = new ArrayList<>(1);
		updates.add(new AKeyInfo(owner(), k, description(), td.numDimensions()));

		return new Updater(){

			@Override
			public void performAction(Updatable i) throws DataAccessException {
				final Object result = td.apply(i);
				if(result!=null) {
					i.setAttribute((AKey)k, result); // FIXME if result is an array and any element is null
					//this will create a problem during storage (null forbidden)
				}
			}

			@Override
			public List<AKeyInfo> updateIntentions() {
				return updates;
			}

		};

	}






	private MetaReductionOperation getMechanismFactory(){
		return ((MetaReductionOperation) this.typeMap.get(REDUCTION).iterator().next().target());
	}

	private MetaTraversal getStrategy(){
		return (MetaTraversal) this.typeMap.get(STRATEGY).iterator().next().target();
	}

	private MetaQueryable getTarget(){
		return getStrategy().getTarget();
	}


	private PredictableMetaSplit getSplit(){
		Set<Link> set = typeMap.get(GROUPING);
		if(set == null)
			return null;
		return (PredictableMetaSplit) set.iterator().next().target();
	}





	private static class TraversingDimensionFactory {

		private static <T> CachingDimension<T> create(MetaTraversingFunction<T> meta){

			ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> f = null;

			boolean targetLinks = !(meta.getTarget() instanceof MetaClass);
			if(!targetLinks)
				f = new TraversalToNodesIterator();
			else
				f = new TraversalToLinksIterator();


			PredictableMetaSplit split = meta.getSplit();

			// 1- Monolithic
			if(split == null)
				return new MonolithicTraversingDimension<>(
						meta.name(),
						meta.description(),
						((MetaTraversal)meta.getStrategy()).toTraversal(), targetLinks,
						meta.getMechanismFactory(),
						f);

			// 2- With splitter
			else
				return new SplittedTraversingDimension<>(meta.name(),
						meta.description(),
						((MetaTraversal)meta.getStrategy()).toTraversal(), targetLinks,
						meta.getMechanismFactory(),
						f,
						split);


			// FIXME  The TraversalDefinition in these constructors should be changed for TraverserConstraints in the future.
			// The root definition is never used. This will mean creating aliases in ModelExtensions and refactoring TraverserConstraints
			// to prevent anonymous classes in fields. Is this worth it?

		}

	}


}

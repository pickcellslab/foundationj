package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ListAction;

public class DefaultCategoricalModel extends AbstractModel<DataItem> {


	private Action<DataItem, Map<AKey<?>,SummaryStatistics>> stats;

	//Data -> queryID, ResultAccess to [serieID, data]
	private Map<String, ResultAccess<String, Map<AKey<?>, SummaryStatistics>>> data = new HashMap<>();
	private Map<String, ResultAccess<String,List<Integer>>> ids = new HashMap<>(); 

	/**
	 * Constructs a new DefaultCategoricalModel which
	 * will use the given {@link StatsAction} to retrieve information from the database.
	 * 
	 * @param stats (null allowed in which case, {@link DefaultCategoricalModel#stats()} always return null)
	 */
	public DefaultCategoricalModel(Action<DataItem, Map<AKey<?>,SummaryStatistics>> stats) {
		this.stats = stats;
	}

	@Override
	protected void dataSetHasChanged() {
		//  Load series with stats and ids
		try {


			for(int q = 0; q<getDataSet().numQueries(); q++){
				if(stats != null)
					data.put(getDataSet().queryID(q), getDataSet().loadQuery(q, stats));
				ids.put(getDataSet().queryID(q), getDataSet().loadQuery(q, new ListAction<>(F.select(DataItem.idKey, -1))));
			}

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param queryID
	 * @param seriesID
	 * @return The list of {@link DataItem#idKey}
	 */
	public List<Integer> ids(String queryID, String seriesID){
		return ids.get(queryID).getResult(seriesID);		
	}

	
	/**
	 * @param queryID
	 * @return The total number of ids found in the specified query
	 */
	public int count(String queryID){
		ResultAccess<String, List<Integer>> r = ids.get(queryID);
		int count = 0;
		for(int i = 0; i<r.size(); i++)
			count += r.getResult(i).size();
		return count;
	}
	
	
	public SummaryStatistics stats(String queryID, String seriesID, AKey<?> k){
		if(stats == null)
			return null;		
		return data.get(queryID).getResult(seriesID).get(k);
	}
	

}

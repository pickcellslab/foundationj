package org.pickcellslab.foundationj.dbm.meta;

import java.util.Objects;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.mapping.data.MultitonOutcome;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapping;



/**
 * A {@link MetaReductionOperation} accepts {@link DataItem}s sequentially and creates a result.  
 * They are a special type of {@link ExplicitReduction} part of the {@link MetaModel}
 * <br><br>
 * <b>Important:</b> All implementing class should be annotated with {@link Data} and {@link Multiton}
 * 
 * @author Guillaume Blin
 *
 */
@Data(typeId = "MetaReductionOperation", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public final class MetaReductionOperation extends MetaItem implements Hideable, ManuallyStored, MetaAutoDeletable{



	static final AKey<String> operationKey = AKey.get("Reduction Operation", String.class);
	static final AKey<Integer> lengthKey = AKey.get("Returned Value Length", int.class);

	private ExplicitReduction<DataItem, ?> op;


	private MetaReductionOperation(String uuid) {
		super(uuid);
	}


	public static MetaReductionOperation getOrCreate(
			MetaInstanceDirector director,		
			ExplicitReduction<DataItem, ?> op,
			MetaReadable<?> required
			) throws NonDataMappingException, InstantiationHookException {
		//System.out.println("MetaReductionOperation C1 used with "+required.toString()); 
		Objects.requireNonNull(required);		
		return MetaReductionOperation.getOrCreate(director, op.description()+" "+required.name(), op, required.numDimensions(), required);
	}
	
	
	/**
	 * Same as {@link #getOrCreate(MetaInstanceDirector, String, ExplicitReduction, int, MetaReadable...)} but
	 * assuming that the reduction operation does not return an array type.
	 */
	public static MetaReductionOperation getOrCreate(
			MetaInstanceDirector director,
			String name,			
			ExplicitReduction<DataItem, ?> op,
			MetaReadable<?>... required
			) throws NonDataMappingException, InstantiationHookException {
		if(op.getReturnType().isArray())
			throw new IllegalArgumentException("The length of the array type returned by the provided ReductionOperation cannot"
					+ "be inferred");
		//System.out.println("MetaReductionOperation C2 used with name = "+name); 
		return MetaReductionOperation.getOrCreate(director, name, op, 1, required);
	}
	

	/**
	 * @param director The MetaInstanceDirector associated with the {@link DataAccess} where the new {@link MetaReductionOPeration}
	 * should be created
	 * @param name A name for the new {@link MetaReductionOperation}
	 * @param op The {@link ExplicitReduction} the new {@link MetaReductionOperation} will represent
	 * @param lengthIfArray The length of the array returned by the provided {@link ExplicitReduction} or 1 if the returned
	 *  value is not an array
	 * @param required {@link MetaReadable} required by the new {@link MetaReductionOperation}
	 * @return The new {@link MetaReductionOperation}
	 * @throws NonDataMappingException
	 * @throws InstantiationHookException
	 */
	public static MetaReductionOperation getOrCreate(
			MetaInstanceDirector director,
			String name,			
			ExplicitReduction<DataItem, ?> op,
			int lengthIfArray,
			MetaReadable<?>... required
			) throws NonDataMappingException, InstantiationHookException {

		Objects.requireNonNull(director, "MetaInstanceDirector is null");
		Objects.requireNonNull(name, "name is null");
		MappingUtils.exceptionIfNullOrInvalid(op);
		//System.out.println("MetaReductionOperation C3 used with name = "+name); 
		if(op.getReturnType().isArray() && lengthIfArray<1)
			throw new IllegalArgumentException("Negative or 0 length provided while the ReductionOperation returns an array type");

		String recSequence = op.description();
		if(required!=null) {
			for(int i = 0; i<required.length; i++) {
				if(required[i]!=null)
					recSequence += required[i].recognitionString();
			}
		}

		final MultitonOutcome<MetaReductionOperation> out = director.getOrCreateWithOutcome(
				MetaReductionOperation.class, recSequence);

		if(out.wasCreated()) {
			out.getInstance().setAttribute(operationKey, NonDataMapping.convertToString(op));
			out.getInstance().setAttribute(MetaItem.name, name);
			if(op.getReturnType().isArray())
				out.getInstance().setAttribute(lengthKey, lengthIfArray);
			if(required!=null) {
				String description = op.description();
				for(int i = 0; i<required.length; i++) {
					if(required[i]!=null) {
						new DataLink(MetaModel.requiresLink, out.getInstance(), required[i], true);
						description+=" "+required[i].name();						
					}
				}
				out.getInstance().setAttribute(MetaItem.desc, description);
			}
			else {
				out.getInstance().setAttribute(MetaItem.desc, op.description());
			}
		}

		return out.getInstance();
	}



	/**
	 * @return A ReductionOperation which returns Object which are always arrays.
	 */
	@SuppressWarnings("unchecked")
	public ExplicitReduction<DataItem, ?> toReductionOperation(){
		if(op==null)
			try {
				op = (ExplicitReduction<DataItem, ?>) NonDataMapping.restoreFromString(getAttribute(operationKey).get());
			} catch (NonDataMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		return op;
	}


	/**
	 * @return The length of the array returned by the ReductionOperation created by this {@link MetaReductionOperation}
	 */
	public int lengthOfReturnedValue() {
		return getAttribute(lengthKey).orElse(1);
	}


	public AKey<?> generatedKey() {
		return AKey.get(name(), toReductionOperation().getReturnType());
	}



	@Override
	public String toHTML() {
		return "<HTML>"
				+ "<b>Reduction Operation:</b>"
				+ "<br>Name: "+name()
				+ "<br>Description: "+description()
				+ "</HTML>";
	}



	@Override
	public String name() {
		return getAttribute(MetaItem.name).get();
	}



	@Override
	public String description() {
		return getAttribute(MetaItem.desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaItem.name,MetaItem.desc,MetaItem.recognition,operationKey,lengthKey);
	}




}

package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Tag;

/**
 * A ThreadSafe implementation of {@link NodeItem}. Links maybe added or deleted while consuming {@link Stream} from this object.
 * 
 * @annotation.required {@link Data}
 * @author Guillaume Blin
 * 
 */
public abstract class DataNode extends DefaultData implements NodeItem {


	/**
	 * The {@link AKey} to be used in case the {@link #declaredType()} differs from its {@link #typeId()}
	 */
	public static final AKey<String> declaredTypeKey = AKey.get("Type", String.class);


	/**
	 * This HashMap stores the links by direction ({@code <Direction, Set<Link>>})
	 */
	protected final Map<Direction, Set<Link>> directionMap = new HashMap<>(2);
	/**
	 * This HashMap stores the links by type ({@code <link type, Set<Link>>})
	 */
	protected final Map<String, Set<Link>> typeMap = new ConcurrentHashMap<>(8, 0.9f, 1);






	public DataNode(){
		directionMap.put(Direction.INCOMING, Collections.newSetFromMap(new ConcurrentHashMap<>(8, 0.9f, 1)));
		directionMap.put(Direction.OUTGOING, Collections.newSetFromMap(new ConcurrentHashMap<>(8, 0.9f, 1)));    
	}

	
	
	@Override
	public boolean hasTag(String tagToFind) {
		// Queue to go up the tree if necessary
		final Deque<Class<?>> queue = new LinkedList<>();
		queue.add(getClass());
		
		do {
			Class<?> mayHaveTag = queue.poll(); // this can be either class or interface
			
			// if this is not an interface it may be annotated with @Data
			if(!mayHaveTag.isInterface()) {
				final Data d = mayHaveTag.getAnnotation(Data.class);
				if(d!=null && d.typeId().equals(tagToFind))
					return true;
				
				// Add the superclass to the queue if it exists
				final Class<?> superClass = mayHaveTag.getSuperclass();
				if(superClass!=null)
					queue.push(superClass);
			}
			
			//Check directly implemented interfaces
			for(Class<?> itrfc : mayHaveTag.getInterfaces()){				
				if(itrfc.getAnnotation(Tag.class)!=null) {
					Tag t = itrfc.getAnnotation(Tag.class);
					if(t!=null && t.tag().equals(tagToFind))
						return true;
				}
				queue.push(itrfc);
			}
			
		}
		while(!queue.isEmpty());

		return false;		
	}
	
	


	@Override
	public boolean addLink(Link link) {    

		boolean success = false;
		Set<Link> typeSet =  typeMap.get(link.declaredType());

		if(typeSet == null){  
			typeSet  = Collections.newSetFromMap(new ConcurrentHashMap<>(8, 0.9f, 1));
			typeMap.put(link.declaredType(), typeSet);
		}

		if (link.source().equals(this))
			success = directionMap.get(Direction.OUTGOING).add(link);  
		else if (link.target().equals(this))
			success = directionMap.get(Direction.INCOMING).add(link);

		typeSet.add(link);

		return success;
	}



	@Override
	public Link addOutgoing(String type, NodeItem target) {
		return new DataLink(type, this, target, true);    
	}

	@Override
	public Link addIncoming(String type, NodeItem source) {
		return new DataLink(type, source, this, true);
	}



	@Override
	public int getDegree(Direction direction, String linkType){
		if(Direction.BOTH == direction){
			Set<Link> set = typeMap.get(linkType);
			if(null == set)
				return 0;
			else return set.size();
		}
		else 
			return (int) directionMap.get(direction).stream().filter(l->l.declaredType().equals(linkType)).count();
	}




	@Override
	public Stream<Link> links() {

		if(typeMap.isEmpty())
			return Stream.empty();

		final Iterator<Set<Link>> it = typeMap.values().iterator();		
		Stream<Link> all = it.next().stream();
		while(it.hasNext())
			all = Stream.concat(all, it.next().stream());

		return all;

	}



	@Override
	public Stream<Link> getLinks(Direction direction, String... linkTypes) {

		if(linkTypes.length == 0){
			if(direction != Direction.BOTH)
				return directionMap.get(direction).stream();			
			else 
				return Stream.concat(directionMap.get(Direction.INCOMING).stream(), directionMap.get(Direction.OUTGOING).stream());

		}
		else{

			final Set<String> types = new HashSet<>(Arrays.asList(linkTypes));

			if(direction != Direction.BOTH)
				return directionMap.get(direction).stream().filter(l->types.contains(l.declaredType()));	

			else 
				return Stream.concat(directionMap.get(Direction.INCOMING).stream(), directionMap.get(Direction.OUTGOING).stream()).filter(l->types.contains(l.declaredType()));

		}

	}


	@Override
	public boolean removeLink(Link link, boolean updateNeighbour) {
		Set<Link> set = typeMap.get(link.declaredType());
		if(null==set)return false;		
		boolean removed = set.remove(link);			
		if(removed){
			removed = directionMap.get(Direction.INCOMING).remove(link);
			if(!removed){
				removed = directionMap.get(Direction.OUTGOING).remove(link);
				if(updateNeighbour)
					link.target().removeLink(link, false);
			}else if(updateNeighbour)
				link.source().removeLink(link, false);
		}

		return removed;
	}


	@Override
	public Collection<Link> removeLink(Predicate<Link> predicate, boolean updateNeighbour) {
		List<Link> remove = getLinks(Direction.BOTH).filter(predicate).collect(Collectors.toList());
		remove.forEach(l->this.removeLink(l, updateNeighbour));
		return remove;
	}

	@Override
	public String toString(){
		return getClass().getSimpleName()+" : "+getAttribute(WritableDataItem.idKey).orElse(-1);
	}

}

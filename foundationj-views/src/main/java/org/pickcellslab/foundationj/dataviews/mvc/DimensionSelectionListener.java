package org.pickcellslab.foundationj.dataviews.mvc;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

public interface DimensionSelectionListener {

	public void selectionChanged(SelectableDimensionBroker<?,?> source, Dimension<?,?> dim, boolean selected);
	
}

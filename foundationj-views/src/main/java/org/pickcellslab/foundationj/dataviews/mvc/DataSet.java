package org.pickcellslab.foundationj.dataviews.mvc;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;

/**
 * 
 * A {@link DataSet} contains one or several database queries which together form the definition of a <a href="https://en.wikipedia.org/wiki/Data_set">dataset</a>.
 * 
 * <br>Each query may result in one or several groups of data. These groups are termed 'series' in the naming of the methods included in this interface.
 * 
 * <br>{@link DataSet} instances are generally provided by {@link DataViewModel} objects and consumed by {@link DataView}s
 * 
 * @param <T> The type of database object queried by this dataset.
 * 
 * @author Guillaume Blin
 *
 */
public interface DataSet<T> {


	
	/**
	 * @return The number of queries contained in this {@link DataSet}
	 */
	public int numQueries();	

	/**
	 * @param query The index of the query of interest
	 * @return A String id for the query at the specified index
	 */
	public String queryID(int query);

	/**
	 * @param queryId The index of the desired query of interest, or -1 if not found
	 * @return
	 */
	public int queryIndex(String queryId);

	
	/**
	 * @param query
	 * @return The {@link QueryInfo} instance associated with the query at the given index
	 */
	public QueryInfo queryInfo(int query);
	
	

	/**
	 * Performs the database query for the given query index using the provided {@link Action}
	 * @param <O> The type of output resulting from the query
	 * @param query The index of the desired query
	 * @param action An {@link Action} instance which will be used for processing the query
	 * @return A {@link ResultAccess} instance containing the output of the query.
	 * @throws DataAccessException
	 */
	public <O> ResultAccess<String,O> loadQuery(int query, Action<T,O> action) throws DataAccessException;
	
	
	
	/**
	 * @param query
	 * @return The number of 'series' or categories defined in this dataset
	 */
	public int numSeries(int query);	
	
	/**
	 * @param query The index of the query of interest
	 * @return The index of the first series in the given query assuming that indices of all queries are cumulative.
	 * For example if query 1 has 3 series and query 2 has 2 series, then this method would return 4 if the given index is 1 (query 2);
	 */
	public int firstSeriesIndex(int query);	

	/**
	 * @param queryID The id of the query of interest
	 * @return The index of the first series in the given query (assuming that indices of all queries are cumulative)
	 * For example if "query 1" has 3 series and "query 2" has 2 series, then this method would return 4 if the given id is "query 2";
	 */
	public int firstSeriesIndex(String queryID);	
	
	/**
	 * @param query The index of the query of interest (between 0 and < numQueries())
	 * @param i The index of the series of interest (between 0 and < numSeries(query))
	 * @return A String id for the desired series
	 */
	public String seriesID(int query, int i);
	
	/**
	 * @param seriesID The id of the series of interest
	 * @return The index of the Query which contains the given series, or -1 if not found
	 * @see #seriesID(int, int)
	 */
	public int queryIndexOf(String seriesID);
	
	/**
	 * @param seriesID The id of the series of interest
	 * @return The id of the query which contains the given series id, or null if not found.
	 * @see #seriesID(int, int)
	 */
	public String queryIDOf(String seriesID);
	
	/**
	 * @param seriesID The id of the series of interest
	 * @return The index of the query which contains the given series id, or -1 if not found.
	 * @see #seriesID(int, int)
	 */
	public int seriesIndex(String seriesID);
		

	/**
	 * Adds a {@link QueryProgressListener} to this {@link DataSet}
	 * @param l The listener to be added
	 */
	public void addQueryProgressListener(QueryProgressListener l);

	/**
	 * Removes the given {@link QueryProgressListener} from this {@link DataSet}
	 * @param l The listener to be removed
	 */
	public void removeQueryProgressListener(QueryProgressListener l);

	

}

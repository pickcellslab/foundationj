package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.QueryFactory;
import org.pickcellslab.foundationj.dbm.db.DatabaseNode;
import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;
import org.pickcellslab.foundationj.dbm.db.UpdatableLink;
import org.pickcellslab.foundationj.dbm.db.UpdatableNode;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener.MetaChange;
import org.pickcellslab.foundationj.dbm.impl.StorageBoxImpl.BoxBuilderImpl;
import org.pickcellslab.foundationj.dbm.meta.ManuallyStored;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;
import org.pickcellslab.foundationj.dbm.queries.ReadAnyBuilder;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.AllOrSome;
import org.pickcellslab.foundationj.dbm.queries.builders.DeletionConfig;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludeExcludeKeyInit;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadItemsBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.SchemaQueryBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

/**
 * A Factory for database operations
 * 
 * @param <V> The type of nodes (vertices) in the database
 * @param <E> The type of links (edges) in the database
 * 
 * @author Guillaume Blin
 *
 */
final class QueryFactoryImpl<V,E> implements QueryFactory {


	private final Session<V, E> session;


	QueryFactoryImpl(Session<V,E> s){
		Objects.requireNonNull(s, "The given session is null, this is forbidden");
		this.session = s;
	}

	/**
	 * Creates a Builder to define writes to the database. The Builder will take {@link WritableDataItem} as inputs,
	 * NB: Once the query is committed, the DataItem entered are guaranteed to possess a  database id ({@link WritableDataItem#idKey})), it
	 * will be unchanged if the item already possessed an id, it will be a newly generated one if not.
	 * @return A dedicated builder
	 * 
	 */
	@Override
	public StorageBoxBuilder store(){
		return new BoxBuilderImpl(session);
	}

	/**
	 * Creates a new Query to store an element of the {@link MetaModel}
	 * Note that {@link MetaQueryable} or {@link MetaKey} cannot be stored using
	 * this method. Such items are created automatically when actual data are stored 
	 * to the database
	 * @param item
	 * @return The query to store the provided item
	 */
	@Override
	public MetaBox meta(ManuallyStored item){
		return new MetaBoxImpl(item, session);
	}




	/**
	 * Internal use only
	 */
	public MetaBoxImpl protectedMeta(Session<?,?> s, MetaModel item){
		return new MetaBoxImpl(item, session);
	}




	//-------------------------------------------------------------------------------------------
	// Read Queries

	

	/**
	 * Initiate a query to read one specific Node from the database with the given database id.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @param dbId The database Id of the object to read.
	 * @return A dedicated {@link ReadOneBuilderImpl}
	 */
	@Override
	public ReadOneBuilderImpl<V, E, DatabaseNode> readOneNode(String label, int dbId){
		return new ReadOneBuilderImpl<>(session, Targets.getLabel(label, session), dbId);
	}
	
	@Override
	public ReadAnyBuilderImpl<V, E, DatabaseNode> readAnyNode(String label){
		return new ReadAnyBuilderImpl<>(Targets.getLabel(label, session));
	}


	/**
	 * Initiate a query to read one specific Link from the database with the given database id.
	 * @param linkType The type of link to be read
	 * @param dbId The database Id of the Link to read.
	 * @return A dedicated {@link ReadOneBuilderImpl}
	 */
	@Override
	public ReadOneBuilderImpl<V, E, Link> readOneLink(String linkType, int dbId){
		return new ReadOneBuilderImpl<>(session, Targets.getLink(linkType, session), dbId);
	}
	
	@Override
	public ReadAnyBuilderImpl<V, E, Link> readAnyLink(String linkType){
		return new ReadAnyBuilderImpl<>(Targets.getLink(linkType, session));
	}

	
	
	@Override
	public ReadAnyBuilder<Link> readAnyLink(String linkType, String srcType, String tgType) {
		return new ReadAnyBuilderImpl<>(Targets.getLink(linkType, srcType, tgType, session));
	}


	/**
	 * Start building a Query which will return {@link Path}(s)
	 * @param name A name for this query.
	 * @return A {@link ReadBuilderImpl} dedicated to build the query
	 */
	@Override
	public ReadBuilder<Path<NodeItem, Link>> readPaths(String name){
		return new ReadBuilderImpl<>(Targets.getPathTypes(name, session));
	}

	/**
	 * @deprecated use {@link #read(String)} instead
	 */
	@Override
	@Deprecated
	public ReadItemsBuilder<DatabaseNode> read(Class<? extends NodeItem> clazz){
		return new ReadItemsBuilderImpl<>(Targets.get(clazz, session));			
	}

	/**
	 * Initiate a query to read Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @return A dedicated {@link ReadItemsBuilderImpl}
	 */
	@Override
	public ReadItemsBuilder<DatabaseNode> read(String label){
		return new ReadItemsBuilderImpl<>(Targets.getLabel(label, session));			
	}

	@Override
	public ReadItemsBuilder<Link> readLinks(String linkType){
		return new ReadItemsBuilderImpl<>(Targets.getLink(linkType, session));			
	}


	
	
	/**
	 * @deprecated use {@link #read(String, long)} instead
	 */
	@Override
	@Deprecated
	public ReadItemsBuilder<DatabaseNode> read(Class<? extends NodeItem> clazz, long limit){
		return new ReadItemsBuilderImpl<>(Targets.get(clazz, session, limit));			
	}

	/**
	 * Initiate a query to read Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @param long limit The max number of objects to read
	 * @return A dedicated {@link ReadItemsBuilderImpl}
	 */
	@Override
	public ReadItemsBuilder<DatabaseNode> read(String label, long limit){
		return new ReadItemsBuilderImpl<>(Targets.getLabel(label, session, limit));			
	}

	@Override
	public ReadItemsBuilder<Link> readLinks(String linkType, long limit){
		return new ReadItemsBuilderImpl<>(Targets.getLink(linkType, session, limit));			
	}


	@Override
	public ReadItemsBuilder<Link> readLinks(String linkType, String srcType, String tgType, long limit){
		return new ReadItemsBuilderImpl<>(Targets.getLink(linkType, srcType, tgType, session, limit));			
	}



	// Read Queries using MetaModel elements as inputs

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ReadItemsBuilder<WritableDataItem> read(MetaQueryable mq, long limit) {		
		if(mq instanceof MetaClass)
			return new ReadItemsBuilderImpl(Targets.getLabel(((MetaClass) mq).itemDeclaredType(), session, limit));	
		else
			return new ReadItemsBuilderImpl(Targets.getLink(mq.itemDeclaredType(), session, limit));
	}






	//-------------------------------------------------------------------------------------------------------
	// Direct Update query


	/**
	 * Initiate a query to update Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @param Updater The {@link Updater} responsible for the update of each encountered object}
	 * @return A dedicated {@link UpdateBuilder}
	 */
	@Override
	public AccessChoice<UpdatableNode,QueryRunner<Void>> update(String label, Updater action){		
		UpdateInfosImpl ui = new UpdateInfosImpl(action);
		return new UpdateBuilder<>(Targets.getUpdatableLabel(label, session,ui), ui, action);
	}


	@Override
	public AccessChoice<UpdatableLink,QueryRunner<Void>> updateLinks(String linkType, String srcType, String tgType, Updater action){
		UpdateInfosImpl ui = new UpdateInfosImpl(action);
		return new UpdateBuilder<>(Targets.getUpdatableLink(linkType, srcType, tgType, session, ui), ui, action);
	}





	//--------------------------------------------------------------------------------------------
	// Deletions

	/**
	 * @deprecated use {@link #delete(String)} instead
	 */
	@Override
	@Deprecated
	public DeletionConfig<DatabaseNodeDeletable> delete(Class<? extends NodeItem> clazz){

		Objects.requireNonNull(clazz, "Clazz is null");
		if(MetaModel.class.isAssignableFrom(clazz))
			throw new IllegalArgumentException("It is forbidden to delete object from the MetaModel: "+clazz.getSimpleName());

		return new DeletionBuilder<>(Targets.getDeletableNode(clazz, session));			
	}



	/**
	 * Initiate a query to delete Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is strongly discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @return A dedicated {@link UpdateBuilder}
	 */
	@Override
	public DeletionConfig<DatabaseNodeDeletable> delete(String label){

		Objects.requireNonNull(label,"label type is null");

		if(label.equals(MetaLink.class.getTypeName())
				|| label.equals(MetaClass.class.getTypeName())
				|| label.equals("MetaReadable")
				|| label.equals("MetaModel")
				|| label.equals("MetaQueryable")
				)
			throw new IllegalArgumentException("It is forbidden to delete objects from the MetaModel");

		return new DeletionBuilder<>(Targets.getDeletableLabel(label, session));			
	}



	@Override
	public DeletionConfig<Link> deleteLinks(String linkType){

		Objects.requireNonNull(linkType,"link type is null");

		if(linkType.equals("FROM")
				|| linkType.equals("TO"))
			throw new IllegalArgumentException("It is forbidden to delete links used by the core MetaModel ("+linkType+")");


		return new DeletionBuilder<>(Targets.getDeletableLink(linkType, session));			
	}


	@Override
	public DeletionConfig<Link> deleteLinks(String linkType, String srcType, String tgType){

		Objects.requireNonNull(linkType,"link type is null");

		if(linkType.equals("FROM")
				|| linkType.equals("TO"))
			throw new IllegalArgumentException("It is forbidden to delete links used by the core MetaModel ("+linkType+")");


		return new DeletionBuilder<>(Targets.getDeletableLink(linkType, srcType, tgType, session));			
	}





	/**
	 * Deletes the provided {@link ManuallyStored} object from the database and objects which depends on it.
	 * @param s
	 * @throws DataAccessException
	 */
	@Override
	public void deleteAnnotation(ManuallyStored s) throws DataAccessException{
		if(s!=null)
			session.notify(deleteMeta().run(s.deletionDefinition(),-1),	MetaChange.DELETED);
	}

	


	//A Package protected deletion query to update the MetaModel
	MetaDeletionBuilder<V,E> deleteMeta(){
		return new MetaDeletionBuilder<>(Targets.getRegenerable("MetaModel", session));			
	}






	//--------------------------------------------------------------------------------------------
	// Regenerate Java objects



	/*
	 * Regenerate MetaModel objects
	 * @param nodeClass
	 * @return A dedicated query builder
	 */
	/*
	public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>>>> regenerateMeta(Meta meta, Class<? extends MetaModel> metaClass){
		return new MetaRegenerationBuilder<V,E>(Targets.getRegenerable(metaClass, session));
	}
	 */


	/**
	 * @deprecated use {@link #regenerate(String)} instead
	 */
	@Override
	@Deprecated
	public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>>>> regenerate(Class<? extends NodeItem> nodeClass){
		return new RegenerationBuilder<V,E>(Targets.getRegenerable(nodeClass, session));
	}

	/**
	 * Initiate a query to regenerate Node java objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is strongly discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param tag The label to use for the search
	 * @return A dedicated {@link RegenerationBuilder}
	 */
	@Override
	public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> regenerate(String tag){
		return new RegenerationBuilder<V,E>(Targets.getRegenerable(tag, session));
	}








	//------------------------------------------------------------------------------------------------
	// Schema Queries

	/**
	 * @deprecated use {@link #schema(String)} instead
	 */
	@Override
	@Deprecated
	public SchemaQueryBuilder schema(Class<? extends NodeItem> clazz){
		return new SchemaQueryBuilderImpl(Targets.getDeletableNode(clazz, session));			
	}


	/**
	 * Initiate a schema query on Nodes within the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is strongly discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @return A dedicated {@link SchemaQueryBuilderImpl}
	 */
	@Override
	public SchemaQueryBuilder schema(String label){
		return new SchemaQueryBuilderImpl(Targets.getDeletableLabel(label, session));			
	}






}

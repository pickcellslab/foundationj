package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.queries.MonolithicResult;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.ActionBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.data.MultitonOutcome;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

/**
 * 
 * A {@link MetaDataSet} for which the target type is known and for which we know that we will perform an {@link Action} on this type only. 
 * It is possible to reach the target via a traversal operation but the nodes which will be reached are all of the same type.
 * 
 * @author Guillaume Blin
 * 
 * @see MetaPathDataSet
 *
 * @param <D>
 */


@Data(typeId = "MetaTargetedDataSet", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public final class MetaTargetedDataSet<D> extends MetaDataSet<DataItem,D>{


	private List<DimensionContainer<DataItem,D>> dims;


	private MetaTargetedDataSet(String uid){
		super(uid);
	};	

	/**
	 * @param name The name of this dataset
	 * @param creator The creator of this dataset
	 * @param description The description of this dataset
	 * @param target The {@link MetaQueryable} defining the target type of this dataset
	 * @param strategy The {@link MetaStrategy} used by this {@link MetaDataSet} to find objects in the database (if null, then all objects of
	 * the target type will be included)
	 * @param split The {@link MetaSplit} used by this {@link MetaDataSet} to group objects (if null, then objects will be pooled into one 
	 * unique group)
	 * @throws InstantiationHookException 
	 */
	public static MetaTargetedDataSet getOrCreate(MetaInstanceDirector director, String name, String description, MetaQueryable target, MetaStrategy strategy, MetaSplit split) throws InstantiationHookException{


		final MultitonOutcome<MetaTargetedDataSet> d = director.getOrCreateWithOutcome(MetaTargetedDataSet.class, buildRecognition(target, strategy, split));

		if(d.wasCreated()) {
			d.getInstance().setAttribute(MetaItem.name, name);
			d.getInstance().setAttribute(MetaItem.desc, description);

			new DataLink(MetaStrategy.POINTS_TO, d.getInstance(), target, true);

			if(strategy != null) {
				new DataLink(STRATEGY, d.getInstance(), strategy, true);
				new DataLink(MetaModel.requiresLink, d.getInstance(), strategy, true);
			}

			if(split!=null) {
				new DataLink(GROUPING, d.getInstance(), split, true);
				new DataLink(MetaModel.requiresLink, d.getInstance(), split, true);
			}
		}
		return d.getInstance();
	}

	
	
	private static String buildRecognition(MetaQueryable target, MetaStrategy strategy, MetaSplit split) {
		final StringBuilder b = new StringBuilder();
		b.append("MTD_");
		b.append(target.name());
		if(strategy != null)
			b.append("_"+strategy.recognitionString());
		if(split != null)
			b.append("_"+split.recognitionString());
		return b.toString();
	}
	


	public MetaStrategy getStrategy(){
		if(!typeMap.containsKey(STRATEGY))
			return null;
		return (MetaStrategy) typeMap.get(STRATEGY).iterator().next().target();
	}

	public MetaSplit getSplitter(){
		if(!typeMap.containsKey(GROUPING))
			return null;
		return (MetaSplit) typeMap.get(GROUPING).iterator().next().target();
	}


	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>DataBase Query</strong>"+
				"<p><ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"</li></p></HTML>";
	}


	@Override
	public DataPointer getDataPointer() {
		return getTarget();
	}


	//@SuppressWarnings("unchecked")
	@Override
	protected <O> ResultAccess<String, O> load(DataAccess access, Action<DataItem, O> action) throws DataAccessException {


		boolean monolithic = false;

		ActionBuilder<?, O> fork = getTarget().initiateRead(access, -1).with(action);


		MetaStrategy strategy = getStrategy();
		MetaSplit split = getSplitter();
		QueryRunner<?> runner = null;
		if(null == strategy){			
			if(split == null){
				monolithic = true;
				runner = fork.inOneSet(name()).getAll();
			}				
			else
				runner = fork.inGroups().getAll().withSplitter(split.toSplitter());
		}
		else if(strategy instanceof MetaFilter){

			if(split == null){
				monolithic = true;
				runner = fork.inOneSet(name()).useFilter(((MetaFilter) strategy).toFilter());
			}
			else
				runner = fork.inGroups().useFilter(((MetaFilter) strategy).toFilter()).withSplitter(split.toSplitter());
		}
		else if(strategy instanceof MetaTraversal){			
			if(split == null){
				monolithic = true;
				runner = fork.inOneSet(name()).useTraversal( ((MetaTraversal) strategy).toTraversal());
			}
			else
				runner = fork.inGroups().useTraversal( ((MetaTraversal) strategy).toTraversal()).withSplitter(split.toSplitter());
		}
		else if(strategy instanceof MetaSplitTraversal){
			if(split == null)
				runner = fork.inGroups()
				.splitTraversal(((MetaSplitTraversal) strategy).metaTraversal().toTraversal())
				.basedOnCustom(((MetaSplitTraversal) strategy).metaSplit().toPathSplitter())
				.noFurtherSplit();
			else{
				runner = fork.inGroups()
						.splitTraversal(((MetaSplitTraversal) strategy).metaTraversal().toTraversal())
						.basedOnCustom(((MetaSplitTraversal) strategy).metaSplit().toPathSplitter())
						.splitFurtherUsing(split.toSplitter());

				return new ToStringOrganisedResult((OrganisedResult) runner.run());
			}
		}


		Object result = runner.run();

		if(monolithic)
			return new MonolithicResult<O>((O) result, name());

		return (ResultAccess<String,O>) result;

	}





	public MetaQueryable getTarget(){
		return (MetaQueryable) typeMap.get(MetaStrategy.POINTS_TO).iterator().next().target();
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<? extends DimensionContainer<DataItem,D>> possibleDimensions(Predicate<DimensionContainer<DataItem,?>> p){	

		if(dims==null){
			dims = (List)getTarget().getReadables().filter(p).collect(Collectors.toList());

			if(getTarget() instanceof MetaLink){

				MetaLink ml = (MetaLink) getTarget();

				ml.source()
				.keys().filter(p)
				.map(mr -> (DimensionContainer)ml.readOnSource(mr.toAKey()))				
				.forEach(d->dims.add(d));

				ml.target()
				.keys().filter(p)
				.map(mr -> (DimensionContainer)ml.readOnTarget(mr.toAKey()))				
				.forEach(d->dims.add(d));

			}
		}
		return dims;
	}


	@Override
	public TraversalDefinition deletionDefinition() {
		return TraversalDefinition.newDefinition().startFrom(DataRegistry.typeIdFor(MetaTargetedDataSet.class)).having(MetaModel.recognition, recognitionString())
				.fromDepth(0).toDepth(0)
				.traverseAllLinks().includeAllNodes().build();
	}



	@Override
	public String name() {
		return getAttribute(name).orElse("No Name");
	}

	@Override
	public String description() {
		return getAttribute(desc).orElse("Unavailable");
	}

	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(name, desc);
	}



}

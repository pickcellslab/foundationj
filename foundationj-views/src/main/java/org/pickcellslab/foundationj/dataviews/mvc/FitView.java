package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dataviews.events.FittingEvtListener;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;


/**
 * @author Guillaume Blin
 *
 * @param <A> The type of appearances used by this view for the displayed fit
 */
public interface FitView<A> extends FittingEvtListener{

	public A getFitAppearance(FitResult<?> fit);

	public void setFitAppearance(FitResult<?> fit, A appearance);

	public void setFitVisible(FitResult<?> fit, boolean isVisible);




}

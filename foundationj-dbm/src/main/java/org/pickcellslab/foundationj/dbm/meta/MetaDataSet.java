package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;





public abstract class MetaDataSet<T,D> extends MetaItem implements Hideable, ManuallyStored{

	
	/**
	 * Type of the links between the MetaDataSet and its dependencies (filter, traversal, tagtest)
	 */
	public static final String STRATEGY = "STRATEGY", GROUPING = "GROUPED";

	
	
	protected MetaDataSet(String uid) {
		super(uid);
	}

	
	public abstract DataPointer getDataPointer();
	
	
	/**
	 * @param p
	 * @return A List containing the {@link Dimension}s that could be encountered within the dataset. NB: some types of dataset
	 * might not be able to provide this information a priori.
	 */
	public abstract List<? extends DimensionContainer<T,D>> possibleDimensions(Predicate<DimensionContainer<T,?>> p);
		
	
	
	protected abstract <O> ResultAccess<String,O> load(DataAccess access, Action<T,O> action) throws DataAccessException;
		
	
	
	/**
	 * Loads this {@link MetaDataSet} to obtain a {@link ResultAccess}
	 * @param access
	 * @param action
	 * @return
	 * @throws DataAccessException
	 */
	public final <O> ResultAccess<String,O> loadDataSet(DataAccess access, Action<T,O> action) throws DataAccessException{
		
		Objects.requireNonNull(access,"Access is null");
		Objects.requireNonNull(action, "Action is null");
		
		return load(access,action);
		
		
	}

	

	
	
	
	
}

package org.pickcellslab.foundationj.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Specifies that the annotated class needs to be provided to a scoped container when a given {@link Service}
 * is required by object included in the scope.
 * 
 *  @see Scope
 *  @see Service
 * 
 * @author Guillaume Blin
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WithService {

	/**
	 * @return The class of the service the annotated object is part of
	 */
	Class<?> value();
	
}

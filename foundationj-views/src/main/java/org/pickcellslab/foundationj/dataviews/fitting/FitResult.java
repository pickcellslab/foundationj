package org.pickcellslab.foundationj.dataviews.fitting;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

public interface FitResult<T> {

	String getId();

	public T getFit();

	public String getDescription();

	public int numDimensions();

	public Dimension<?,?> getDimension(int i);

}
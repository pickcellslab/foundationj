package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.SearchSeveralQuery;
import org.pickcellslab.foundationj.dbm.queries.SearchingQuery;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dbm.queries.actions.Action;

/**
 * {@link SearchSeveralQueryImpl} objects contains the logic to perform the search in the 
 * database. Given the type of the {@link Action} injected in the constructor,
 * the {@link SearchSeveralQueryImpl} should be able to optimise the search to use as few resources
 * as possible to create the result.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of object which will be the target of the {@link Search} in the database and thus input
 * of the associated {@link Action}
 * @param <O> The type of the output of the provided {@link Action}
 * @param <S> The specific type of {@link ResultAccess} to the outputs
 */
abstract class SearchSeveralQueryImpl<V,E,T,K,O,S extends ResultAccess<K,O>> implements SearchingQuery<V, E, S>, SearchSeveralQuery<V, E, T, K, O, S> {

	protected final Action<? super T, O> action;

	protected SearchSeveralQueryImpl(Action<? super T,O> action){
		this.action = action;
	}


	
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.queries.SearchSeveralQuery#action()
	 */
	@Override
	public final Action<? super T,O> action(){
		return action;
	}

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.queries.SearchSeveralQuery#name()
	 */
	@Override
	public abstract String name();

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.queries.SearchSeveralQuery#shortDescription()
	 */
	@Override
	public abstract String shortDescription();

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.queries.SearchSeveralQuery#searchDescription()
	 */
	@Override
	public abstract String searchDescription();
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.queries.SearchSeveralQuery#actionDescription()
	 */
	@Override
	public abstract String actionDescription();



}

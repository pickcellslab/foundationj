package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.AbstractAction;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public class VectorAction<T> extends AbstractAction<T,List<double[]>>{

	
	private final Function<T,double[]> d;

	public VectorAction(Function<T,double[]> d){
		super("Obtain data for fitting");
		this.d = d;
	}



	@Override
	protected ActionMechanism<T, List<double[]>> create(String key) {
		return new VectorMechanism(d);
	}



	private class VectorMechanism implements ActionMechanism<T, List<double[]>>{

		private final Function<T,double[]> d;
		private final List<double[]> data = new ArrayList<>();

		VectorMechanism(Function<T,double[]> d){
			this.d = d;
		}

		@Override
		public void performAction(T t) throws DataAccessException {
			double[] coords = d.apply(t);
			if(null!=coords)
				data.add(coords);
		}

		public List<double[]> get(){
			return data;
		}
	}




}

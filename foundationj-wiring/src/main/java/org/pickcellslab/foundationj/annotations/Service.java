package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specifies that the annotated {@link Core} or {@link Modular} interface should be regarded by the framework as a service. 
 * {@link Service}s are not bound to any specific {@link Scope} and can be injected into consumers in scopes which
 * are fully independent.
 * 
 * @see Scope
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
public @interface Service {

}

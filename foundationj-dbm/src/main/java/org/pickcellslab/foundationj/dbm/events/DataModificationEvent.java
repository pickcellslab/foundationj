package org.pickcellslab.foundationj.dbm.events;

import java.util.stream.IntStream;

import org.pickcellslab.foundationj.datamodel.DataItem;

/**
 * 
 * Provides information about modifications occurring on the data graph after a database operation.
 * 
 * @author Guillaume Blin
 *
 */
public interface DataModificationEvent {

	/**
	 * Describes a change occurring for a list of DataItem in the database
	 */
	public enum EventScheme{
		INSERTION, DELETION, AMENDEMENT
	}

	public EventScheme eventScheme();
	
	/**
	 * @return The typeId of the modified {@link DataItem}s
	 */
	public String modifiedType();

	/**
	 * @return An {@link IntStream} providing the sequence of database ids (see {@link DataItem#idKey}) of 
	 * {@link DataItem}s which have been affected by the event 
	 */
	public IntStream ids();

	
	

}

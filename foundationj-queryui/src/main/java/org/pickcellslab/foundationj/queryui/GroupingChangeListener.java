package org.pickcellslab.foundationj.queryui;

import org.pickcellslab.foundationj.queryui.SplitterUI.SplitID;

@FunctionalInterface
public interface GroupingChangeListener {

	public void groupingMethodSelected(SplitID id);
	
}

package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.pickcellslab.foundationj.datamodel.AttributeChangeEvent.AEventBuilder;

/**
 * An abstract base class for {@link ListenableDataItem}
 *
 */
public abstract class DefaultListenableDataItem extends DefaultData implements ListenableDataItem{


  private List<AttributeChangeListener> lstrs = new ArrayList<>();
  
  @Override
  public <T> void setAttribute(AKey<T> key, T v) {

    Object oldValue = attributes.put(key, v);

    if (oldValue != null) {
      AttributeChangeEvent evt = new AEventBuilder(this, key, AttributeChangeEvent.MODE.UPDATED).setOldValue(oldValue)
          .setNewValue(v).build();
      fireEvent(evt);
    }
    else {
      AttributeChangeEvent evt = new AEventBuilder(this, key, AttributeChangeEvent.MODE.ADDED).setNewValue(v).build();
      fireEvent(evt);
    }

  }


  @Override
  public void removeAttribute(AKey<?> key) {
    if (attributes.remove(key) != null) {
      AttributeChangeEvent evt = new AEventBuilder(this, key, AttributeChangeEvent.MODE.DELETED).build();
      fireEvent(evt);
    }

  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> Optional<T> getAttribute(AKey<T> key) {
    return Optional.of((T) attributes.get(key));
  }


  /* (non-Javadoc)
 * @see org.pickcellslab.foundationj.datamodel.ListenableDataItem#addListener(org.pickcellslab.foundationj.datamodel.AttributeChangedListener)
 */
@Override
public void addAttributeChangeListener(AttributeChangeListener l) {
    lstrs.add(l);
  }
  /* (non-Javadoc)
 * @see org.pickcellslab.foundationj.datamodel.ListenableDataItem#removeListener(org.pickcellslab.foundationj.datamodel.AttributeChangedListener)
 */
@Override
public void removeAttributeChangeListener(AttributeChangeListener l) {
    lstrs.remove(l);
  }
  /* (non-Javadoc)
 * @see org.pickcellslab.foundationj.datamodel.ListenableDataItem#removeALLListeners()
 */
@Override
public void removeAllAttributeChangeListeners() {
    lstrs = new ArrayList<>();
  }

  private void fireEvent(AttributeChangeEvent evt) {
    for (AttributeChangeListener l : lstrs) {
      l.attributeChanged(evt);
    }
  }


  
  
}

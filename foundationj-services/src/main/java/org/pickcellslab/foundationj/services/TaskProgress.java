package org.pickcellslab.foundationj.services;

/**
 * 
 * A {@link TaskProgress} is a task which execution is controlled by a {@link ProgressControl} and which delivers a result.
 * Such objects can be obtained from a {@link ProgressFactory}
 * 
 * @author Guillaume Blin
 *
 * @param <V> The type of the output of the task
 */
public interface TaskProgress<V> {

	/**
	 * The status of the task
	 *
	 */
	public enum ProgressStatus{
		RUNNING, FAILED, SUCCESS, WAITING
	}
	
	
	/**
	 * @return The name of the task
	 */
	public String getName();
	
	/**
	 * @return The status of the task
	 */
	public ProgressStatus getStatus();
	
	/**
	 * Registers the given {@link TaskResultConsumer}
	 * @param consumer The {@link TaskResultConsumer} to register
	 */
	public void addTaskResultConsumer(TaskResultConsumer<V> consumer);
	
	/**
	 * Registers the given {@link ProgressStatusListener}
	 * @param consumer The {@link ProgressStatusListener} to register
	 */
	public void addProgressStatusListener(ProgressStatusListener lst);
	
	/**
	 * Registers the given {@link ProgressLogListener}
	 * @param consumer The {@link ProgressLogListener} to register
	 */
	public void addProgressLogListener(ProgressLogListener lst);
	
}

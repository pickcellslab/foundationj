package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;

/**
 * A {@link ExplicitFunction} returning {@code true} if a DataItem has a specific {@link AKey}, {@code false} otherwise
 * 
 * @author Guillaume Blin
 *
 */
@Mapping(id = "KeyMatch")
@Scope
public class KeyMatcher implements ExplicitFunction<WritableDataItem,Boolean>{

	private final AKey<?> k;

	/**
	 * Constructs a new KeyMatcher with the specified {@link AKey} to match
	 */
	KeyMatcher(AKey<?> k){
		this.k = k;
	}
	
	@Override
	public Boolean apply(WritableDataItem t) {
		return t.getAttribute(k) == null;
	}

	@Override
	public String description() {
		return "HAS "+k.name;
	}
	
	@Override
	public String toString() {
		return description();
	}

	@Override
	public Class<Boolean> getReturnType() {
		return Boolean.class;
	}

	@Override
	public ExplicitOperator<?, ?>[] getOperatorCompatibility() {
		return Op.Bool.values();
	}

}

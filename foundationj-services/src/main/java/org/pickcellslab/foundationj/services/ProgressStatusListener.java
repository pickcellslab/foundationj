package org.pickcellslab.foundationj.services;

/**
 * 
 * Listens for changes of status in {@link TaskProgress}
 * 
 * @author Guillaume Blin
 *
 */
public interface ProgressStatusListener {

	public void statusChanged(TaskProgress<?> source);
	
}

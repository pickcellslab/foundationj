package org.pickcellslab.foundationj.datamodel.graph;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.Link;

/**
 * The root interface defining a <a href="https://en.wikipedia.org/wiki/Vertex_(graph_theory)">node</a> in a 
 * <a href="https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)"> graph </a>
 *
 * @param <T> The type of {@link Edge} used by this Node
 */
public interface Node<T extends Edge<? extends Node<? extends T>>> {

	/**
	 * @return A {@link Stream} holding all the {@link Edge} associated to this {@link Node}
	 */
	public Stream<T> links();
	

	/**
	 * @param link
	 * @return true if the {@link Link} was successfully added, false if it was already contained into
	 * this item or if the specified link does not point to this item (either source or target)
	 */
	public boolean addLink(T link);

	/**
	 * @param link The Link to remove
	 * @param updateNeighbour a flag indicating whether this link reference should also be removed from the adjacent node 
	 * @return true if the {@link Link} was successfully removed, false otherwise
	 */
	public boolean removeLink(T link, boolean updateNeighbour);


}

package org.pickcellslab.foundationj.dbm.access;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Service;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dbm.events.DataUpdateListener;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

/**
 * 
 * Provides access to the database. Database operations are accessible via {@link #queryFactory()}. A {@link DataAccess}
 * also provides a {@link Meta} instance which facilitates queries and navigation of the {@link MetaModel}.
 * Finally, {@link DataAccess} accepts both {@link DataUpdateLitener}s and {@link MetaModelListener}s.
 * <br><br>
 * <b>NB:</b> Use constructor injection to obtain a reference to the active {@link DataAccess}
 * 
 * @author Guillaume Blin
 *
 */
@Core
@Service
public interface DataAccess {
	
	/**
	 * @return A {@link QueryFactory} associated wit this {@link DataAccess}
	 */
	public QueryFactory queryFactory();
	
	/**
	 * @return A {@link Meta} instance to navigate the {@link MetaModel} found in the database accessible via this{@link DataAccess}
	 */
	public Meta metaModel();
	
	/**
	 * @return The {@link DataRegistry} associated to this {@link DataAccess} instance.
	 */
	public DataRegistry dataRegistry();
	
	/**
	 * Adds the given (@link DataUpdateListener} to this {@link DataAccess} instance
	 * @param lstr The {@link DataUpdateListener} which needs to listen for data updates
	 */
	public void addUpdateListener(DataUpdateListener lstr);

	/**
	 * Removes the given (@link DataUpdateListener} from this {@link DataAccess} instance
	 * @param lstr The {@link DataUpdateListener} which no longer needs to listen for data updates
	 */
	public boolean removeUpdateListener(DataUpdateListener lstr);

	/**
	 * Adds the given (@link MetaModelListener} to this {@link DataAccess} instance
	 * @param lstr The {@link MetaModelListener} which needs to listen for data updates
	 */
	public void addMetaListener(MetaModelListener lstr);

	/**
	 * Removes the given (@link MetaModelListener} from this {@link DataAccess} instance
	 * @param lstr The {@link MetaModelListener} which no longer needs to listen for data updates
	 */
	public boolean removeMetaListener(MetaModelListener lstr);
}

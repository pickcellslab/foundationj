package org.pickcellslab.foundationj.datamodel.functions;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Map.Entry;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.Logical;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.fathzer.soft.javaluator.StaticVariableSet;


/**
 * A {@link ExplicitFunction} that parses a mathematical expression and calculates the result
 * using values stored in the {@link DataItem} it is applied to.
 * 
 * @author Guillaume Blin
 *
 */

@Mapping(id = "MathsExpression")
@Scope
public class DataTransform<T extends DataItem> implements ExplicitFunction<T,Double>{

	@Supported
	private final Map<String, ExplicitFunction<T,? extends Number>> keyMap;
	private final String expression;	
	@Ignored
	private StaticVariableSet<Double> variables;
	@Ignored
	private DoubleEvaluator evaluator;

	/**
	 * @param expression The mathematical expression to be parsed
	 * @param dfMap The names of the variable in the expression mapped to the {@link ExplicitFunction} objects
	 *  in the DataItem where the data are stored
	 * @throws IllegalArgumentException
	 */
	public DataTransform(String expression, Map<String, ExplicitFunction<T,? extends Number>> dfMap) throws IllegalArgumentException	{	

		this.expression = expression;
		this.keyMap = dfMap;
		variables = new StaticVariableSet<Double>();
		
		//test the expression is ok -> will throw an IllegalArgumentException if there is a problem
		for(Entry<String, ExplicitFunction<T,? extends Number>> e : dfMap.entrySet()) {			
			variables.set(e.getKey(), 1d);
		}
		evaluator = new DoubleEvaluator();
		evaluator.evaluate(expression, variables);
	}


	@Override
	public Double apply(T t) { 

		if(null == evaluator) {
			evaluator = new DoubleEvaluator();
			variables = new StaticVariableSet<Double>();
		}
		
		for(Entry<String, ExplicitFunction<T,? extends Number>> e : keyMap.entrySet()){

			Number n = e.getValue().apply(t);
			if(null!=n)
				variables.set(e.getKey(), n.doubleValue());
			else return Double.NaN;
		}

		try {
			return evaluator.evaluate(expression, variables);
		}
		catch(IllegalArgumentException e) {//
			System.out.println("DataTransform: Result of evaluation is NaN"); //TODO replace by log.warn()
			return Double.NaN;
		}
	}


	@Override
	public String description() {
		return expression;
	}


	@Override
	public String toString() {
		return "Expression "+hashCode();
	}

	@Override
	public Logical[] getOperatorCompatibility() {		
		return Op.Logical.values();
	}

	@Override
	public Class<Double> getReturnType() {
		return Double.class;
	}



}

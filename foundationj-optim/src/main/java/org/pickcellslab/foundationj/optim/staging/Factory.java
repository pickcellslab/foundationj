package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;

/**
 * A {@link Factory} is both a {@link Agent} and a {@link Director}
 * requiring and fulfilling contracts of the same type
 * 
 * @author Guillaume Blin
 *
 * @param <E> The type of the declared requirements which is the same as the objects produced by this
 */
public interface Factory<E extends Edge<? extends Node<E>>> 
extends Director<Collection<Parcel>>,
Agent<Collection<Parcel>>,
Node<E>
{
	
	public String getName();
}

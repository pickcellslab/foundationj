package org.pickcellslab.foundationj.dbm.queries.actions;

import java.util.List;
import java.util.Map;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dbm.queries.DataCollectorBuilder.DataCollector;
import org.pickcellslab.foundationj.dbm.queries.DataCollectorBuilder.Gatherer;
import org.pickcellslab.foundationj.dbm.queries.ReadTable;

public class TableMaker<T extends DataItem> extends AbstractAction<T, ReadTable>{

	private final DataCollector<T> template;
	
	
	
	
	public TableMaker(DataCollector<T> build) {
		super(buildDescription(build));
		this.template = build;
	}


	@Override
	protected ActionMechanism<T, ReadTable> create(String key) {
		return new CollectorMechanism(template);
	}


	
	
	
	private static <T extends DataItem> String buildDescription(DataCollector<T> template) {
		String description = "Collect:";
		Gatherer<T> gatherer = template.accumulator();
		for(AKey<?> k : gatherer.keys())
			description += "|"+k.name+"|";
		return description;
	}

	
	private class CollectorMechanism implements ActionMechanism<T, ReadTable>{

		private DataCollector<T> collector;
		private List<Map<AKey<?>,Object>> result;

		//Used by ReadQuery
		public CollectorMechanism(DataCollector<T> template) {
			this.collector = template.duplicate();
			result = collector.supplier().get();
		}

	

		@Override
		public void performAction(T i) {
			collector.accumulator().accept(result, i);			
		}

		
		/**
		 * Builds and return a ReadResult object
		 */
		@Override
		public ReadTable get(){
			return new ReadTable(result, collector.accumulator().keys());
		}


		
	}
	
	
	
}

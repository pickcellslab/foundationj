package org.pickcellslab.foundationj.optim.csp;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;

public interface CostFunction
<N extends Node<D>, D extends Edge<? extends N>>

{
	
	
	/**
	 * Computes the cost for the specified coordinates
	 * @param source The source coordinate
	 * @param neighbours The selected solvers coordinate
	 * @return The computed cost for the specified coordinates
	 */
	double compute(Solver<N,D> source, Collection<Solver<N,D>> neighbours);
	
	
	/**
	 * @return The value of the minimum cost possible
	 */
	public double minCost();
	
}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.queries.SearchingQuery;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadOneBuilder;

class ReadOneBuilderImpl<V,E,T> implements ReadOneBuilder<T> {

	protected final  TargetType<V,E,T> pointer;
	private Session<V,E> access;
	private final int dbId;

	ReadOneBuilderImpl(Session<V,E> access, TargetType<V,E,T> pointer, int dbId){
		this.pointer = pointer;	
		this.access = access;
		this.dbId = dbId;
	}


	@Override
	public <O> O using(Function<? super T,O> mecha) throws DataAccessException{
		Objects.requireNonNull(mecha);
		return access.runQuery(new SearchingQuery<V,E,O>(){

			@Override
			public String description() {
				return "Getting one "+pointer.toString();
			}

			@Override
			public O createResult(DatabaseAccess<V,E> db) throws DataAccessException {
				Optional<T> t = pointer.getOne(db, dbId);
				if(t.isPresent())
					return mecha.apply(t.get());
				return null;
			}			
		});
	}
	
	
	


	
	



	@Override
	public <N> N dimension(Dimension<T, N> dim, N defaultValue) throws DataAccessException{

		Objects.requireNonNull(dim);

		return access.runQuery(
				new SearchingQuery<V,E,N>(){

					@Override
					public String description() {
						return "Read one "+pointer.toString()+" Dimension : "+dim;
					}

					@Override
					public N createResult(DatabaseAccess<V,E> db) throws DataAccessException {
						Optional<T> t = pointer.getOne(db, dbId);
						if(t.isPresent())
							return dim.apply(t.get());
						else
							return defaultValue;
					}			
				});

	}

	
	


}

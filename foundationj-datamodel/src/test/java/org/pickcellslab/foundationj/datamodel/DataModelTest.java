package org.pickcellslab.foundationj.datamodel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DataModelTest {

	
	@Test
	public void testDefaultData(){

		DefaultNodeItem data = new DefaultNodeItem();
		data.setAttribute(AKey.get("test", String.class), "Test Value");
		Assertions.assertTrue(data.getAttribute(AKey.get("test", String.class)).get()=="Test Value");    

	}

	@Test
	public void testSilentLink(){

		AKey<String> key = AKey.get("identity", String.class);

		DefaultNodeItem source = new DefaultNodeItem();
		source.setAttribute(key, "I am the source");

		DefaultNodeItem target = new DefaultNodeItem();
		target.setAttribute(key, "I am the target");


		DataLink link = new DataLink("test", source, target, true);
		link.setAttribute(key, "I am the link");


		Assertions.assertTrue(link.source().getAttribute(key).get()==source.getAttribute(key).get()); 
		Assertions.assertTrue(link.target().getAttribute(key).get()==target.getAttribute(key).get()); 

	}

	@Test
	public void testDeleteSilentLink(){

		AKey<String> key = AKey.get("identity", String.class);

		DefaultNodeItem source = new DefaultNodeItem();
		source.setAttribute(key, "I am the source");

		DefaultNodeItem target = new DefaultNodeItem();
		target.setAttribute(key, "I am the target");


		DataLink link = new DataLink("test", source, target, true);
		link.setAttribute(key, "I am the link");

		Assertions.assertTrue(source.links().count()!=0); 
		Assertions.assertTrue(target.links().count()!=0); 

		link.delete();

		Assertions.assertTrue(link.source()==null); 
		Assertions.assertTrue(link.target()==null); 
		Assertions.assertTrue(link.declaredType()==null); 
		Assertions.assertTrue(source.links().count()==0); 
		Assertions.assertTrue(target.links().count()==0); 

	}


	@Test
	public void testDefaultNode(){

		AKey<String> key = AKey.get("identity", String.class);

		DefaultNodeItem source = new DefaultNodeItem();
		source.setAttribute(key, "I am the source");

		DefaultNodeItem target = new DefaultNodeItem();
		target.setAttribute(key, "I am the target");

		DefaultNodeItem target1 = new DefaultNodeItem();
		target.setAttribute(key, "I am the target1");
		source.addOutgoing("out", target1);

		DefaultNodeItem target2 = new DefaultNodeItem();
		target.setAttribute(key, "I am the target2");
		source.addOutgoing("out", target2);


		Link sTot = source.addOutgoing("TestLink", target);
		Link tTos = source.addIncoming("TestLink", target);


		Assertions.assertTrue(source.getLinks(Direction.OUTGOING).anyMatch(l->l.equals(sTot)));
		Assertions.assertTrue(!source.getLinks(Direction.INCOMING).anyMatch(l->l.equals(sTot)));
		Assertions.assertTrue(source.getLinks(Direction.INCOMING).anyMatch(l->l.equals(tTos)));

		Assertions.assertTrue(!source.getLinks(Direction.OUTGOING, "wrongType").anyMatch(l->l.equals(sTot)));


		Assertions.assertTrue(source.getLinks(Direction.BOTH).count() == 4);
		Assertions.assertTrue(source.getLinks(Direction.OUTGOING).count() == 3);
		Assertions.assertTrue(source.getLinks(Direction.OUTGOING, "out").count() == 2);

		Assertions.assertTrue(source.getLinks(Direction.BOTH).anyMatch(l->l.equals(sTot)));

		Assertions.assertTrue(source.getLinks(Direction.BOTH,"TestLink").anyMatch(l->l.equals(sTot)));

		Assertions.assertTrue(sTot.source().getAttribute(key).get()==source.getAttribute(key).get()); 
		Assertions.assertTrue(sTot.target().getAttribute(key).get()==target.getAttribute(key).get());    

	}


}

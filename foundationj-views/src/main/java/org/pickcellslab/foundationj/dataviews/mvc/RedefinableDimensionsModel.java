package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer.Mode;

/**
 * 
 * A {@link DimensionalModel} which allows to redefine its dimensions.
 * 
 * @author Guillaume Blin
 *
 * @param<T> The generic type of the {@link DataSet} handled by this model which is also the type of objects the dimensions
 * are defined for.
 */
public interface RedefinableDimensionsModel<T,B extends DimensionBroker<T,D>,D> extends DimensionalModel<T,D>, RedefinableBrokerContainer<T,B,D>{




	/**
	 * A Factory method to create a new {@link DimensionBroker} which can be used by this model
	 * @param list the {@link DImensionContainer} the broker must contain
	 * @return A new DimensionBroker
	 */
	public B createBroker(List<Dimension<T,D>> list);

	// Definition of dimensions to display for the represented objects
	@Override
	public B dimensionBroker(String series);


	public List<DimensionContainer<T,D>> possibleDimensions(int queryIndex);
	
	/**
	 * @return A {@link Predicate} which specify which dimensions can be handled by this model
	 */
	public Predicate<DimensionContainer<T,?>> dimensionPredicate();

	public Mode getDimensionMode();

}

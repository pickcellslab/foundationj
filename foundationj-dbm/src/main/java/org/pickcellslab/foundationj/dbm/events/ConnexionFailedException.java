package org.pickcellslab.foundationj.dbm.events;

import org.pickcellslab.foundationj.dbm.db.DataStoreConnector;

/**
 * A checked Exception thrown by a {@link DataStoreConnector} when a connection request has failed.
 *
 */
public class ConnexionFailedException extends Exception{

	
	public ConnexionFailedException(String msg) {
		super(msg);
	}
	
	public ConnexionFailedException(String msg, Throwable t) {
		super(msg, t);
	}
}

package org.pickcellslab.foundationj.mapping.extra;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.mapping.exceptions.UnsupportedMappingException;

/**
 * 
 * Provides static methods to test {@link Mapping} compatibility at runtime
 * 
 * @author Guillaume Blin
 *
 */
public final class MappingUtils {

	private MappingUtils() {/*Not instantiable*/}

	/**
	 * Checks whether a given object is currently supported for {@link Mapping} and throws an exception if it is not. 
	 * <br> 
	 * <b>NB1:</b> This check is not required if the runtime class of the object is known at compile time.
	 * <br> 
	 * <b>NB2:</b> Does not perform a null check
	 * 
	 * @see #exceptionIfNullOrInvalid(Object)
	 * @param o The (non null) object which needs to be checked
	 * @throws UnsupportedMappingException if the provided object is unsupported
	 */
	public static void exceptionIfInvalid(Object o) throws UnsupportedMappingException{
		if(!canBeMapped(o)) {
			throw new UnsupportedMappingException(o);
		}
	}

	/**
	 * Checks whether a given object is currently supported for {@link Mapping} and throws an exception if it is not. 
	 * <br> 
	 * <b>NB1:</b> This check is not required if the runtime class of the object is known at compile time.
	 * 
	 * @see #exceptionIfInvalid(Object)
	 * @param o The (non null) object which needs to be checked
	 * @throws UnsupportedMappingException if the provided object is unsupported
	 * @throws NullPointerException if the provided object is null
	 */
	public static void exceptionIfNullOrInvalid(Object o) throws UnsupportedMappingException, NullPointerException{
		Objects.requireNonNull(o);
		if(!canBeMapped(o)) {
			throw new UnsupportedMappingException(o);
		}
	}


	/**
	 * Checks whether a given object is currently supported for {@link Mapping}. 
	 * <br> 
	 * <b>NB1:</b> This check is not required if the runtime class of the object is known at compile time.
	 * <br> 
	 * <b>NB2:</b> Does not perform a null check and null will throw a NullPointerException
	 * 
	 * @param o The (non null) object which needs to be checked
	 * @return {@code true} if the provided object is supported, {@code false} otherwise.
	 * @throws NullPointerException if the given object is null
	 */
	public static boolean canBeMapped(Object o) {

		// Is class?
		if(o instanceof Class)
			return ((Class) o).getAnnotation(Mapping.class)!=null;

		Class<?> clazz = o.getClass();

		// Array?
		if(o.getClass().isArray())
			clazz = o.getClass().getComponentType();

		// Is primitive?
		if(clazz.isPrimitive())
			return true;

		// Is enum?
		if(Enum.class.isAssignableFrom(clazz))
			return true;// class.isEnum() does not do the job if enum implements interface

		// Is boxed type
		if(clazz == Double.class) return true;
		if(clazz == Float.class) return true;
		if(clazz == Long.class) return true;
		if(clazz == Integer.class) return true;
		if(clazz == Short.class) return true;
		if(clazz == Byte.class) return true;
		if(clazz == Boolean.class) return true;
		if(clazz == Character.class) return true;		
		// Is String?
		if(clazz == String.class) return true;

		// Collection?
		else if (Collection.class.isAssignableFrom(clazz)) {
			final Collection<?> map = ((Collection)o);
			if(map.isEmpty())// due to type erasure we cannot check this thoroughly
				return true;
			else {
				final Object e = map.iterator().next();
				return canBeMapped(e);
			}
		}
		// Map?
		else if (Map.class.isAssignableFrom(clazz)) {
			final Map<?,?> map = ((Map)o);
			if(map.isEmpty())// due to type erasure we cannot check this thoroughly
				return true;
			else {
				final Entry<?, ?> e = map.entrySet().iterator().next();
				return canBeMapped(e.getKey()) && canBeMapped(e.getValue());
			}
		}

		// Other class?
		return o.getClass().getAnnotation(Mapping.class)!=null;
	}


}

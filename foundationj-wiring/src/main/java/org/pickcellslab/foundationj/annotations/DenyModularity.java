package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Specifies that an interface can neither be {@link Modular} nor {@link Core}
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(SOURCE)
@Target(TYPE)
public @interface DenyModularity {

}

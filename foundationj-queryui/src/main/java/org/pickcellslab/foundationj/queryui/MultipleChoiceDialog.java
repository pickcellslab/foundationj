package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListCellRenderer;

import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.PluggableSelection;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

@SuppressWarnings("serial")
public class MultipleChoiceDialog<T> extends JDialog {


	private boolean wasCancelled = true;
	private final PluggableSelection<T> choSel;


	public MultipleChoiceDialog(
			UITheme theme,
			Collection<T> toChooseFrom,
			Supplier<ListCellRenderer<? super T>> sRenderer,
			Comparator<? super T>[] comparators,
			boolean allowEmptyChoice
			) {

		JLabel lblMessage = new JLabel("Message");

		PluggableSelection<T> avSel = new PluggableSelection<>((WizardNode) null, "Available Items", comparators);
		avSel.setRenderer(sRenderer.get());
		toChooseFrom.forEach(t->avSel.add(t));

		choSel = new PluggableSelection<>((WizardNode) null, "Chosen Items", comparators);
		choSel.setRenderer(sRenderer.get());

		JButton btnR = new JButton(theme.icon(IconID.Arrows.ARROW_RIGHT, 16));
		btnR.addActionListener(l->{
			T item = avSel.getSelectedItem();
			if(null!=item){
				avSel.remove(item);
				choSel.add(item);
			}				
		});

		JButton btnL = new JButton(theme.icon(IconID.Arrows.ARROW_LEFT, 16));
		btnL.addActionListener(l->{
			T item = choSel.getSelectedItem();
			if(null!=item){
				choSel.remove(item);
				avSel.add(item);
			}				
		});


		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{
			if(!allowEmptyChoice && choSel.getAllItems().isEmpty()){
				JOptionPane.showMessageDialog(null, "Please choose at least on item");
				return;
			}
			wasCancelled = false;
			this.dispose();
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->this.dispose());






		//Layout
		//-----------------------------------------------------------------------------------------------------


		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(avSel, GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(btnL)
												.addComponent(btnR))
												.addGap(11)
												.addComponent(choSel, GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))
												.addComponent(lblMessage))
												.addContainerGap())
												.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
														.addContainerGap(307, Short.MAX_VALUE)
														.addComponent(btnOk)
														.addPreferredGap(ComponentPlacement.UNRELATED)
														.addComponent(btnCancel)
														.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addGap(71)
										.addComponent(btnR)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(btnL))
										.addGroup(groupLayout.createSequentialGroup()
												.addContainerGap()
												.addComponent(lblMessage)
												.addPreferredGap(ComponentPlacement.UNRELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(choSel, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
														.addComponent(avSel, GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))))
														.addPreferredGap(ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
														.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
																.addComponent(btnOk)
																.addComponent(btnCancel))
																.addContainerGap())
				);
		getContentPane().setLayout(groupLayout);
	}
	
	
	
	
	public boolean wasCancelled(){
		return wasCancelled;
	}
	
	
	public List<T> chosenItems(){
		return choSel.getAllItems();
	}
	
	
	
	
	
	
	
	
}

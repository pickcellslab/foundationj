package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.annotations.Tag;

@Tag(creator = "MetaModel", description = "Defines a strategy to find specific objects in the database", tag = "MetaStrategy")
public interface MetaStrategy extends MetaModel, Hideable, ManuallyStored {

	/**
	 * The type of links which point to the target of this {@link MetaStrategy}
	 */
	public static final String POINTS_TO = "POINTS_TO";
	
	public MetaQueryable getTarget();
	
}

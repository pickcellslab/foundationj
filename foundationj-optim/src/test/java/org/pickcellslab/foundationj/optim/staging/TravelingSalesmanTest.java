package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.junit.BeforeClass;
import org.junit.Test;
import org.pickcellslab.foundationj.optim.algorithms.SimulatedAnnealing;
import org.pickcellslab.foundationj.optim.algorithms.SolutionAssignment;

public class TravelingSalesmanTest {

	static List<Point2D> points = new ArrayList<>();

	@BeforeClass
	public static void init(){


		for(int i = 0; i<50; i++){
			points.add(new City());
		}

	}


	@Test
	public void TSTest1() throws InterruptedException, IOException{



		DrawArea p = new DrawArea();
		p.setBackground(Color.WHITE);
		p.setPreferredSize(new Dimension(500,500));

		JFrame f = new JFrame();
		f.setContentPane(p);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);

		
		//p.setPoints(points, Color.GRAY);
		
		SolutionAssignment<List<Point2D>> a = new SolutionAssignment<List<Point2D>>(){
			@Override
			public List<Point2D> apply(List<Point2D> t) {
				return new ArrayList<>(t);
			}			
		};
		
		SimulatedAnnealing<List<Point2D>> sa = new SimulatedAnnealing<List<Point2D>>(
				new CityTour(points),
				SimulatedAnnealing.defaultFunction(),
				CityTour.costFunction(),
				a
				);
		//sa.setLogLevel(Level.INFO);
		
		sa.solve(100, 0.00001);
				
		//sa.save("/home/ghiomm/documents/Notes/sa.txt");
		
		p.setPoints(sa.getSolution().get(), Color.RED);

		Thread.sleep(1000);


	}


	@SuppressWarnings("serial")
	public static class DrawArea extends JPanel{

		List<Point2D> points = new ArrayList<>();
		private Color c = Color.BLACK;

		@Override
		public void paint(Graphics g){
			Graphics2D g2 = (Graphics2D) g;
			g.setColor(c);
			
			points.forEach(p->g2.drawRect((int)p.getX()-10, (int) p.getY()-10, 20, 20));
				for(int i = 0; i<points.size(); i++){
					Point2D p1 = points.get(i);
					Point2D p2 = null;
					if(i==points.size()-1){
						p2 = points.get(0);
					}else{
						p2 = points.get(i+1);
					}					
					g2.drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(),(int)p2.getY());
					
				}			
		}
		
		
		public void setPoints(List<Point2D> points, Color c){
			this.points = points;
			this.c = c;
			this.repaint();		
		}

	}


}

package org.pickcellslab.foundationj.dbm.sequences;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * {@link GraphSequence} is dedicated to annotate {@link GraphSequencePointer} implementing classes.
 * The purpose of this annotation is to enable the framework to determine at runtime which type of
 * {@link GraphSequenceHints} should be associated with a particular class of {@link GraphSequencePointer} 
 * (without the need to load actual {@link GraphSequencePointer} instances).
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface GraphSequence {

	/**
	 * @return The type of {@link GraphSequenceHints} to be associated with the annotated {@link GraphSequencePointer} 
	 */
	public Class<? extends GraphSequenceHints> value();
}

package org.pickcellslab.foundationj.datamodel.functions;

import java.util.Objects;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "FindOne")
@Scope
public class StreamFindOne<I,O> implements ExplicitFunction<I,O>{

	@Supported
	private final ExplicitFunction<I,Stream<O>> stream;
	private final String type;
	

	public StreamFindOne(ExplicitFunction<I,Stream<O>> stream, Class<? super O> type) {
		MappingUtils.exceptionIfNullOrInvalid(stream);
		Objects.requireNonNull(type);
		this.stream = stream;
		this.type = type.getName();
	}
	
		@Override
		public O apply(I t) {
			return stream.apply(t).findAny().orElse(null);
		}

		@Override
		public String description() {
			return " find any ";
		}


		@Override
		public String toString() {
			return " find any ";
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public Class<O> getReturnType() {
			try {
				return (Class<O>) Class.forName(type);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Unable to load the class with name : "+type, e);
			}
		}

	
	
}

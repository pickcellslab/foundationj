package org.pickcellslab.foundationj.queryui;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.NamedExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.BinaryTreeBFS;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.PredictableMetaSplit;
import org.pickcellslab.foundationj.dbm.queries.CombinedBinary;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.ToStringBinaryTreeSplitter;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

@Data(typeId = "BinaryTreeMetaSplit", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
class BinaryTreeMetaSplit extends PredictableMetaSplit {

	static final AKey<String> accept = AKey.get("Accepted Name",String.class);
	static final AKey<String> reject = AKey.get("Rejected Name",String.class);



	private BinaryTreeMetaSplit(String uid) {
		super(uid);
	}

	static BinaryTreeMetaSplit getOrCreate(MetaInstanceDirector director, List<SplitIn2Panel<?>> splits) throws InstantiationHookException {

		final BinaryTreeMetaSplit split = director.getOrCreate(BinaryTreeMetaSplit.class, MetaItem.randomUID(BinaryTreeMetaSplit.class));

		//Build description first
		StringBuilder b = new StringBuilder();
		splits.stream().forEach(s->{
			b.append("|"+s.getPredicate().name()+"|");
		});

		//Now add required filters
		splits.stream().forEach(s->{
			Link l = new DataLink(MetaModel.requiresLink, split, s.getPredicate(), true);
			l.setAttribute(accept, s.getAcceptedName());
			l.setAttribute(reject, s.getRejectedName());
		});
		split.setAttribute(MetaItem.desc, b.toString());

		return split;
	}




	@Override
	public String name() {
		return recognitionString();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}



	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>Grouping:</strong>"+
				"<p><ul><li>Name:"+name()+"</li>"+
				"<li>Description:"+description()+
				"</li></p></HTML>";
	}


	@Override
	public String[] getPrediction() {		
		BinaryTreeBFS<CombinedBinary<Object>> bfs = new BinaryTreeBFS<>(createCombinedBinary());
		List<CombinedBinary<Object>> leaves = bfs.getAllLeaves();
		String[] groups = new String[leaves.size()];
		for(int i = 0; i<groups.length; i++)
			groups[i] = leaves.get(i).toString();		
		return groups;
	}


	@Override
	public boolean pathSplitSupported() {
		return true;
	}

	@Override
	public boolean targetSplitSupported() {
		return true;
	}


	private <E> CombinedBinary<E> createCombinedBinary(){

		final List<ExplicitPredicate<? super E>> predicates = new ArrayList<>();

		getLinks(Direction.OUTGOING, MetaModel.requiresLink)
		.forEach(l->{
			MetaFilter mf = ((MetaFilter)l.target());
			predicates.add(new NamedExplicitPredicate<>(mf.toFilter(), l.getAttribute(accept).get(), l.getAttribute(reject).get()));
		});

		return CombinedBinary.createTree(predicates);
	}



	@Override
	public <E> ExplicitSplit<E, String> toSplitter() {		

		final List<ExplicitPredicate<? super E>> predicates = new ArrayList<>();		
		getLinks(Direction.OUTGOING, MetaModel.requiresLink).forEach(l->{
			MetaFilter mf = ((MetaFilter)l.target());
			predicates.add(new NamedExplicitPredicate<>(mf.toFilter(), l.getAttribute(accept).get(), l.getAttribute(reject).get()));
		});
		return new ToStringBinaryTreeSplitter<>(predicates);
	}




	@Override
	public <V, E> ExplicitPathSplit<V, E, String> toPathSplitter() {
		final MetaQueryable target = getLinks(Direction.OUTGOING, MetaModel.requiresLink)
				.map(l->((MetaFilter)l.target()).getTarget()).findFirst().get();
		final ExplicitPredicate<DataItem> predicate = P.isDeclaredType(target.itemDeclaredType());
		final boolean isLink = target instanceof MetaLink;
		final CombinedBinary<DataItem> root = createCombinedBinary();
		return new BinaryTreePathSplitter<>(predicate, isLink, root);
	}




}

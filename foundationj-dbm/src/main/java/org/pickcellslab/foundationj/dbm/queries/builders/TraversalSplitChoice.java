package org.pickcellslab.foundationj.dbm.queries.builders;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;

public interface TraversalSplitChoice<V, E, T, O> {

	public <K> SplittedSplitChoiceNoneAllowed<T,K,O> basedOnACategoricalKey(ExplicitPredicate<? super NodeItem>p, AKey<K> k);
	
	public <K> SplittedSplitChoiceNoneAllowed<T,Boolean,O> basedOnBinary(ExplicitPredicate<? super NodeItem> p);

	public CategoryAdder<SplittedSplitChoiceNoneAllowed<T,ExplicitPredicate<? super NodeItem>,O>,NodeItem>
		basedOnFilters(ExplicitPredicate<? super NodeItem> p);

	public SplittedSplitChoiceNoneAllowed<T,Integer,O> basedOnDepth();

	public <K> SplittedSplitChoiceNoneAllowed<T,K,O> basedOnCustom(ExplicitPathSplit<V,E,K> splitter);
}

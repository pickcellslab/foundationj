package org.pickcellslab.foundationj.dbm.queries.builders;

public interface ActionBuilder<T, O> {

	/**
	 * The query will search the database for one set of objects only. Should be used when a one off
	 * search of the database is planned. If instead you would like to create a query which is capable
	 * of grouping multiple types of objects into different accessible sets in one go, then use {@link #inGroups()}.
	 * @return A dedicated Builder for the desired type of Query.
	 */
	AccessChoice<T, QueryRunner<O>> inOneSet();

	/**
	 * The query will search the database for one set of objects only. Should be used when a one off
	 * search of the database is planned. If instead you would like to create a query which is capable
	 * of grouping multiple types of objects into different accessible sets in one go, then use {@link #inGroups()}.
	 * @return A dedicated Builder for the desired type of Query.
	 */
	AccessChoice<T, QueryRunner<O>> inOneSet(String name);

	/**
	 * The query will allow to group objects based on some criteria defined later while searching the database.
	 * For example, if you define a Path from class A to class B with a filter on As and multiple filters on Bs, then
	 * this query will look for roots As only once, traverse the graph to Bs and create one set per filters on Bs in which
	 * the Bs instances will be sorted. You will thus be able to access each group in the output independently.
	 * Obviously the advantage on using this method over multiple {@link #inOneSet()} calls is that As will be filtered
	 * only once, and the graph also traversed only once.
	 * @return A dedicated Builder for the desired type of Query.
	 */
	SplittableAccessChoice<?, ?, T, O, SplitChoice<T, O>> inGroups();

}
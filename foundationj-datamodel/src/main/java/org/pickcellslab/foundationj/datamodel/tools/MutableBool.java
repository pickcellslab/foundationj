package org.pickcellslab.foundationj.datamodel.tools;

public class MutableBool {

	private boolean bool;
	
	public MutableBool(boolean b) {
		bool = b;
	}
	
	public boolean get() {
		return bool;
	};
	
	
	public void set(boolean b) {
		bool = b;
	}
	
}

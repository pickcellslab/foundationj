package org.pickcellslab.foundationj.dbm.meta;

import java.lang.reflect.Array;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

interface CachingDimension<T> extends Dimension<DataItem,Object>, DimensionContainer<DataItem,T>{

	static final Logger log = LoggerFactory.getLogger(CachedDimension.class);

	public String getName(int index);

	public String description(int index);



	@Mapping(id = "GraphNavigationCache")
	@WithService(DataAccess.class)
	class CachedDimension<E> implements Dimension<DataItem, E>{



		@Supported
		private final CachingDimension<E> td;
		private final int index;

		public CachedDimension(CachingDimension<E> td, int index) {
			MappingUtils.exceptionIfNullOrInvalid(td);
			this.td = td;
			this.index = index;
		}


		@Override
		public dType dataType() {
			return td.dataType();
		}

		@Override
		public String description() {
			return td.description(index);
		}


		@SuppressWarnings("unchecked")
		@Override
		public Class<E> getReturnType() {
			return td.length()==-1 ? (Class<E>) td.getReturnType().getComponentType() : (Class<E>) td.getReturnType();
		}

		@SuppressWarnings("unchecked")
		@Override
		public E apply(DataItem t) {
			Object array = td.apply(t);
			if(array==null)
				return null;
			try {
				return array.getClass().isArray() ? (E)Array.get(array, index) : (E) array;
			}
			catch(Exception e) {
				log.error("Computing "+name()+" for "+Objects.toString(t)+" led to an error",e);
				return null;
			}
		}

		@Override
		public int index() {
			return index;
		}

		@Override
		public int length() {
			return -1;
		}

		@Override
		public String name() {
			return td.getName(index);
		}


		@Override
		public String toString() {
			return name();
		}

		@Override
		public String info() {
			return td.info();
		}

	}

}
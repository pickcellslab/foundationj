package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.CardLayout;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import javax.swing.JPanel;

import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.ui.wizard.HighlightablePanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link JPanel} which displays one {@link ChoicePanel} at a time based on events the ChoicePanels receive. 
 * The intent of this class is primarily to allow dependent objects to request display and highlight of a desired ChoicePanel.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of {@link MetaModel} objects handled by the ChoicePanels.
 */
@SuppressWarnings("serial")
public class ChoicePanelCard<T extends MetaModel> extends HighlightablePanel {

	private final Logger log = LoggerFactory.getLogger(ChoicePanelCard.class);

	private final Map<String,ChoicePanel<T>> mapping = new HashMap<>();
	private ChoicePanel<T> current;

	public ChoicePanelCard(ChoicePanel<MetaQueryable> master, Function<MetaQueryable, ChoicePanel<T>> factory) {


		Objects.requireNonNull(master, "The master ChoicePanel cannot be null");
		Objects.requireNonNull(factory, "The factory of ChoicePanel cannot be null");

		CardLayout card = new CardLayout(0,0);
		setLayout(card);
		

		Optional<MetaQueryable> opt = master.getSelected();
		opt.ifPresent(mq->{
			ChoicePanel<T> newPanel = factory.apply(mq);
			String s = mq.getAttribute(MetaModel.name).get();
			mapping.put(s, newPanel);
			this.add(newPanel, s);
			card.show(this, s);
			current = newPanel;
		});
		
		this.setPreferredSize(new Dimension(250,200));

		master.addSelectionListener(l->{

			log.debug("Selection evt received");
			if(!l.getValueIsAdjusting())
				master.getSelected().ifPresent(mq->{

					log.debug("Queryable selected in master");

					String s = mq.getAttribute(MetaModel.name).get();
					log.debug("name is "+s);
					ChoicePanel<T> newPanel = mapping.get(s);
					if(newPanel == null){					
						newPanel = factory.apply(mq);
						mapping.put(s, newPanel);
						this.add(newPanel, s);						
						current = newPanel;
					}
					card.show(this, s);
					revalidate();
				});
		});
	}




	public ChoicePanel<T> get(MetaQueryable mq){
		return mapping.get(mq.getAttribute(MetaModel.name).get());
	}






	@Override
	public void setHighlighted(boolean highlighted) {
		if(current!=null)
			current.setHighlighted(highlighted);
		this.fireHighlighted(highlighted);
	}




	

}

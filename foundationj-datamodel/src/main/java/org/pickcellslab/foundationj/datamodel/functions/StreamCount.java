package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.FunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.ValuePredicate;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "Count")
public class StreamCount<T> implements ExplicitFunction<T,Long> {

	@Supported
	private final ExplicitStream<T,?> stream;
	
	public StreamCount(ExplicitStream<T,?> stream) {
		MappingUtils.exceptionIfNullOrInvalid(stream);		
		this.stream = stream;
	}
	
	public ExplicitPredicate<T> isSuperiorOrEqualTo(long value){
		return makePredicate(Op.Logical.SUP_EQ, value);
	}
	
	public ExplicitPredicate<T> isSuperiorTo(long value){
		return makePredicate(Op.Logical.SUP, value);
	}
	
	public ExplicitPredicate<T> isInferiorTo(long value){
		return makePredicate(Op.Logical.INF, value);
	}
	
	public ExplicitPredicate<T> isInferiorOrEqualTo(long value){
		return makePredicate(Op.Logical.INF_EQ, value);
	}
	
	private ExplicitPredicate<T> makePredicate(Op.Logical op, long value){
		return new FunctionPredicate<>(this, new ValuePredicate<>(op, value));
	}
	

	@Override
	public Long apply(T t) {
		return stream.apply(t).count();
	}

	@Override
	public String description() {
		return "count "+ stream.description();
	}
	

	@Override
	public String toString() {
		return "count "+ stream.toString();
	}

	@Override
	public Class<Long> getReturnType() {
		return long.class;
	}



}

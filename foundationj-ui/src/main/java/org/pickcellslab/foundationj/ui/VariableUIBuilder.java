package org.pickcellslab.foundationj.ui;

import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.datamodel.tools.Builder;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.WebLookAndFeel;
import com.alee.managers.tooltip.TooltipManager;

/**
 * Builds a JPanel with input dialogs to enable the user to adjust the values of {@link Variable}s.
 */
public class VariableUIBuilder implements Builder<JPanel>{


	private final JPanel pane = new JPanel();

	public VariableUIBuilder() {
		pane.setLayout(new VerticalFlowLayout());
	}

	
	public VariableUIBuilder(List<Variable<?>> vars) {
		this();
		vars.forEach(v->add(v));
	}


	public VariableUIBuilder add(Variable<?> var) {
		
		if(var instanceof BooleanVariable)	return add((BooleanVariable)var);
		else if(var instanceof DoubleVariable)	return add((DoubleVariable)var);
		else
			throw new UnsupportedOperationException("Cannot create a UI for variable of type "+var.getClass().getSimpleName());
	}
	

	public VariableUIBuilder add(BooleanVariable var) {
		
		final JCheckBox cb = new JCheckBox(var.name());
		TooltipManager.addTooltip(cb, var.description());
		
		cb.setSelected(var.value());
		cb.addActionListener(l->var.setValue(cb.isSelected()));
		
		cb.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		pane.add(cb);
		
		return this;		
	}
	
	
	public VariableUIBuilder add(DoubleVariable var) {

		final JLabel label = new JLabel(var.name());
		TooltipManager.addTooltip(label, var.description());
		
		final SpinnerNumberModel sModel = new SpinnerNumberModel(var.currentValue.doubleValue(), var.minValue(), var.maxValue(), var.increment());
		sModel.addChangeListener((e)-> var.setValue(sModel.getNumber().doubleValue()));

		final JSpinner spinner = new JSpinner(sModel);
		
		final GroupPanel gp = new GroupPanel(10, label, spinner);
		gp.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		pane.add(gp);

		return this; 
	}



	@Override
	public JPanel build() {
		return pane;
	}





	public static void main(String[] args) {

		WebLookAndFeel.install();
		
		
		final Variable<Double> var1 = new DoubleVariable(
				"Test",
				87633d,
				"some description",
				10, -1100, 100000);
		var1.addVariableListener((var, old, novel)->System.out.println(var1+ " new value = "+novel));
		
		final Variable<Boolean> var2 = new BooleanVariable("boolean test", true, null);
		var2.addVariableListener((var, old, novel)->System.out.println(var2+ " new value = "+novel));
		
		final JPanel pane = 
				new VariableUIBuilder().add(var1).add(var2)
				.build();
		
		final JFrame f = new JFrame();
		f.setContentPane(pane);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		

	}


}

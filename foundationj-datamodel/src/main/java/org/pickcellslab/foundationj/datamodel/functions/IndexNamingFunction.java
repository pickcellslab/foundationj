package org.pickcellslab.foundationj.datamodel.functions;

import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;

@Mapping(id = "Index Name")
public class IndexNamingFunction implements ExplicitFunction<Integer, String> {


	private final String[] names;


	public IndexNamingFunction(String[] names) {
		Objects.requireNonNull(names);
		this.names = names;
	}


	@Override
	public String apply(Integer t) {
		if(names.length<t)
			return " " + t;
		else
			return " " + Objects.toString(names[t]);
	}

	@Override
	public String description() {
		return "Maps array attributes indices to more explicit names";
	}

	@Override
	public Class<String> getReturnType() {
		return String.class;
	}
	

}

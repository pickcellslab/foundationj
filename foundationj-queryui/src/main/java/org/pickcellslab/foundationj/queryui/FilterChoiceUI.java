package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;




@SuppressWarnings("serial")
public class FilterChoiceUI extends WizardNode {

	private JRadioButton attOnlyRB, conOnlyRB;

	public static final int ATT_ONLY = 0, CONN_ONLY = 1;
	
	private final MetaQueryable mq;
	private final DataAccess access;
	private final UITheme theme;

	private AttributeFilterUI attFilterUI;



	public FilterChoiceUI(MetaQueryable mq, UITheme theme, DataAccess access) {
		this(null, mq, theme, access);
	}



	
	public FilterChoiceUI(WizardNode parent, MetaQueryable mq, UITheme theme, DataAccess access) {
		super(parent);

		this.mq = mq;
		this.access = access;
		this.theme = theme;

		setBorder(new TitledBorder(null, "Type of Filter", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		final ButtonGroup buttonGroup = new ButtonGroup();
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));


		if(MetaLink.class.isAssignableFrom(mq.getClass())){
		

			attOnlyRB = new JRadioButton("<HTML>Create a filter based on the the source, the target "
					+ "<br> or the link's attributes</HTML>");
			buttonGroup.add(attOnlyRB);
			attOnlyRB.setVerticalAlignment(SwingConstants.TOP);
			add(attOnlyRB);
		
		}
		else{
		
		setLayout(new GridLayout(0, 1, 0, 0));

		attOnlyRB = new JRadioButton("Create a filter based on the nodes attributes");
		add(attOnlyRB);
		attOnlyRB.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup.add(attOnlyRB);		

		conOnlyRB = new JRadioButton("Create a filter based on the connections of the nodes");
		add(conOnlyRB);
		buttonGroup.add(conOnlyRB);

		}

		attOnlyRB.setSelected(true);

	}







	@Override
	public int numberOfForks() {
		return 4;
	}



	@Override
	public boolean validity() {
		return true;
	}



	@Override
	protected WizardNode getNextStep() {
		WizardNode wn = null; 
		if(attOnlyRB.isSelected()){
			if(attFilterUI == null)
				attFilterUI = new AttributeFilterUI(this, theme, access.metaModel().getInstanceManager(), mq);
			wn = attFilterUI;
		}		
		else if(conOnlyRB.isSelected())
			try {
				wn = new ConnectionFilterUI(this,(MetaClass)mq, access, theme);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return wn;
	}
}

package org.pickcellslab.foundationj.datamodel.builders;

import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;

@Mapping(id="AsString")
public class AsString<T> implements ExplicitFunction<T, String> {

	@Override
	public String apply(T t) {
		return Objects.toString(t);
	}

	@Override
	public String description() {
		return "as string";
	}

	@Override
	public Class<String> getReturnType() {
		return String.class;
	}

}

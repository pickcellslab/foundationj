package org.pickcellslab.foundationj.ui.wizard;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.ui.wizard.WizardListener.Outcome;

/**
 * A dialog implementing the wizard pattern. The underlying model is a
 * tree which holds the different steps the user has to go through to complete the process. 
 * 
 * @param <T> The type of object returned by the wizard when the construction process is completed
 * 
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("serial")
public class Wizard<T> extends JPanel {

	private boolean wasCancelled = true;
	private final List<WizardListener<T>> lstrs = new ArrayList<>();
		
	private WizardNode current;

	private JButton btnBack;
	private JButton btnNext;
	private JButton btnFinish;
	private JButton btnCancel;

	private Window container;

	public Wizard(WizardNode root){

		Objects.requireNonNull(root);
		
		System.out.println("New Wizard for root: "+root.getClass().getSimpleName());

		root.setWizard(this);
		current = root;


		//TODO traverse the tree to find out if the tree is properly constructed
		//Or maybe instead wrap into an actual tree model
		//For the moment it is possible to construct a tree leading to leaves returning the wrong type of
		//construct

		//Setup the CardLayout
		JPanel cardPanel = new JPanel();
		CardLayout card = new CardLayout();
		cardPanel.setLayout(card);
		cardPanel.add(root, ""+root.id());



		//Back Next Finish Buttons Panel

		JPanel btnPanel = new JPanel();

		btnBack = new JButton("Back");
		btnPanel.add(btnBack);
		btnBack.addActionListener(l->{	
			current = current.getPreviousStep();
			current.setWizard(this);
			card.show(cardPanel, ""+current.id());

			if(container!=null)
				container.pack();			
			System.out.println("Now showing "+current.getClass().getSimpleName()+ " : "+current.id() );			
			updateBtns();
		});

		btnNext = new JButton("Next");
		btnPanel.add(btnNext);
		btnNext.addActionListener(l->{
			current = current.getNextStep();
			current.setWizard(this);
			cardPanel.add(current, ""+current.id());
			card.show(cardPanel, ""+current.id());

			if(container!=null)
				container.pack();

			System.out.println("Now showing "+current.getClass().getSimpleName()+ " : "+current.id() );		
			updateBtns();
		});

		btnFinish = new JButton("Finish");
		btnPanel.add(btnFinish);
		btnFinish.addActionListener(l->{
			wasCancelled = false;
			lstrs.forEach(wl->wl.done(this, Outcome.SUCCESS));
		});

		btnCancel = new JButton("Cancel");
		btnPanel.add(btnCancel);
		btnCancel.addActionListener(l->lstrs.forEach(wl->wl.done(this, Outcome.CANCELLED)));

		updateBtns();		


		//Layout
		//--------------------------------------------------------------------------
		setLayout(new BorderLayout(0, 0));
		add(cardPanel, BorderLayout.CENTER);	
		add(btnPanel, BorderLayout.SOUTH);	
	}



	private void updateBtns(){

		System.out.println("Updating buttons");
		boolean backAllowed = current.parent!=null;
		if(current.isLeaf()){
			btnFinish.setEnabled(current.validity());
			btnNext.setEnabled(false);
			btnBack.setEnabled(backAllowed);
		}else{
			btnFinish.setEnabled(false);
			btnNext.setEnabled(current.validity());
			btnBack.setEnabled(backAllowed);
		}
		if(current.isRoot())
			btnBack.setEnabled(false);
	}


	/**
	 * @return {@code true} if the user cancelled the process, {@code false} otherwise.
	 */
	boolean wasCancelled(){
		return wasCancelled;
	}



	/**
	 * @return An {@link Optional} holding the result of the user inputs into this wizard
	 */
	@SuppressWarnings("unchecked")
	private Optional<T> getConstruct(){
		if(wasCancelled)
			return Optional.empty();
		else return ((WizardLeaf<T>) current).getConstruct();					
	}


	public WizardNode currentNode(){
		return current;
	}

	void nextValidityChanged(WizardNode wizardNode) {
		updateBtns();
	};




	public void addWizardListener(WizardListener<T> ls){
		Objects.requireNonNull(ls,"Listener s null");
		lstrs.add(ls);
	}


	public void removeWizardListener(WizardListener<T> ls){
		lstrs.remove(ls);
	}




	public static <T> Optional<T> showWizardDialog(WizardNode node){
		Objects.requireNonNull(node, "The Wizard node is null");
		return showWizardDialog(new Wizard<>(node));
	}


	public static <T> Optional<T> showWizardDialog(Wizard<T> w){

		Objects.requireNonNull(w, "The Wizard is null");


		JDialog dialog = new JDialog();
		dialog.setContentPane(w);
		w.setWindow(dialog);

		w.addWizardListener((s,o)->dialog.dispose());

		dialog.setModal(true);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);

		return w.getConstruct();

	}


	public static <T> Optional<T> showWizardDialog(Wizard<T> w, JPanel otherPanel){

		Objects.requireNonNull(w, "The Wizard is null");

		JPanel combined = new JPanel(new BorderLayout());
		combined.add(w, BorderLayout.WEST);
		combined.add(otherPanel, BorderLayout.EAST);

		JDialog dialog = new JDialog();
		dialog.setContentPane(combined);
		w.setWindow(dialog);

		w.addWizardListener((s,o)->dialog.dispose());

		dialog.setModal(true);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);

		return w.getConstruct();

	}




	private void setWindow(Window dialog) {
		this.container = dialog;
	}



}

package org.pickcellslab.foundationj.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * The {@link Scope} of an annotated interface defines the container where implementations should be registered
 * as well as the container hierarchy.
 * All {@link Core}, {@link Modular} and {@link Data} interfaces must define a {@link Scope} unless they define 
 * themselves as {@link Service}.
 * 
 * @see SameScopeAs
 * @see Service
 * 
 * @author Guillaume Blin
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Scope {

	
	/**
	 * The name of the APPLICATION Scope
	 */
	public static final String APPLICATION = "APPLICATION";
	
	/**
	 * @return The name of the Scope
	 */
	String name() default APPLICATION;
	
	/**
	 * @return The name of the parent Scope
	 */
	String parent() default "";
	
	/**
	 * @return The {@link ScopePolicy} of the Scope
	 */
	ScopePolicy policy() default ScopePolicy.SINGLETON;
	
	/*
	 * Defines the {@link Service}s required by this {@link Scope}. Generally, only the root parent scope
	 * of a scope tree needs to define these services since children scopes can obtain their dependencies
	 * from parent scopes.
	 * @return An array of interfaces annotated with @{@link Service}
	 *
	Class<?>[] services() default {};
	*/
	
}

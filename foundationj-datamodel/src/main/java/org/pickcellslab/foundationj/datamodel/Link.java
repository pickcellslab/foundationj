package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.graph.Edge;


/**
 * A Link corresponds to an edge in a graph, it connects a source and a target. In this case, source and targets
 * are {@link NodeItem} objects. A Link is also a WritableDataItem object which means that it has a type and a store of key / value pairs
 *
 */

public interface Link extends WritableDataItem, Edge<NodeItem>, Deletable{

	/**
	 * Static strings to be used as standard names for connections
	 */
	public static final String OWNS = "OWNS", OWNER = "OWNED BY", BELONGS = "BELONGS TO";  


	/**
	 * @return the source of this link
	 */
	public NodeItem source();

	/**
	 * @return the target of this link
	 */
	public NodeItem target();

	

	@Override
	public default Stream<AKey<?>> minimal(){
		return Stream.of(idKey);
	}
	
}

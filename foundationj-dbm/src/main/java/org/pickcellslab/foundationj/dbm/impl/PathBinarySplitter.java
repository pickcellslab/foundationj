package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="SinglePredicatePathGrouping")
class PathBinarySplitter<V,E> implements ExplicitPathSplit<V,E,Boolean>{

	@Supported
	private final ExplicitPredicate<? super NodeItem> predicate;
	@Ignored
	private FjAdaptersFactory<V, E> f;

	public PathBinarySplitter(ExplicitPredicate<? super NodeItem> p) {
		MappingUtils.exceptionIfNullOrInvalid(p);
		this.predicate = p;
	}
	
	@Override
	public String description() {
		return "Split path in 2 based on "+predicate.description();
	}

	@Override
	public void setFactory(FjAdaptersFactory<V, E> f) {
		this.f = f;		
	}

	@Override
	public Boolean getKeyFor(Path<V, E> p) {
		
		Iterator<V> ns = p.nodes();
		while(ns.hasNext()){
			NodeItem i = f.getNodeReadOnly(ns.next());
			if(predicate.test(i)){
				return true;
			}
		}
		
		return false;
	}

}

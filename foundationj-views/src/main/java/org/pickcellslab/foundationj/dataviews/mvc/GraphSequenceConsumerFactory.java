package org.pickcellslab.foundationj.dataviews.mvc;

import java.util.function.Consumer;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;

public interface GraphSequenceConsumerFactory{

	public Consumer<Path<NodeItem,Link>> createConsumer(String id, int i);
	
}

package org.pickcellslab.foundationj.datamodel.predicates;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.ExplicitOperation;

/**
 * A Factory class for Operator objects
 * 
 * @author Guillaume
 *
 */
public final class Op {


	/**
	 * @param left The class of the left operand
	 * @param right The class of the right operand
	 * @return An array of Operators holding all the known Operators that are compatible with the specified
	 * operand types
	 */
	@SuppressWarnings("unchecked")
	public static <L,R> ExplicitOperator<L,R>[] getAllCompatible(Class<L> left, Class<R> right){


		if(Bool.AND.isLeftValid(left) && Bool.AND.isRightValid(right))
			return (ExplicitOperator<L, R>[]) Bool.values();
		else if(Logical.EQUALS.isLeftValid(left) && Logical.EQUALS.isRightValid(right))
			return (ExplicitOperator<L, R>[]) Logical.values();
		else if(TextOp.CONTAINS.isLeftValid(left) && TextOp.CONTAINS.isRightValid(right))
			return (ExplicitOperator<L, R>[]) TextOp.values();
		else if(ElementOp.ELEMENT_OF.isLeftValid(left) && ElementOp.ELEMENT_OF.isRightValid(right))
			return (ExplicitOperator<L, R>[]) ElementOp.values();

		return new ExplicitOperator[0];

	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <L> ExplicitOperator<L,?>[] findLeftValid(Class<L> left){
				
		if(Bool.AND.isLeftValid(left))
			return (ExplicitOperator<L, ?>[]) Bool.values();
		else if(Logical.EQUALS.isLeftValid(left))
			return (ExplicitOperator<L, ?>[]) Logical.values();
		else if(TextOp.CONTAINS.isLeftValid(left))
			return (ExplicitOperator<L, ?>[]) TextOp.values();
		else if(ElementOp.ELEMENT_OF.isLeftValid(left)){//same for SetOps
			List<ExplicitOperator> valid = new ArrayList<>();	
			Collections.addAll(valid,ElementOp.values());
			Collections.addAll(valid,SetOp.values());
			return (ExplicitOperator<L, ?>[]) valid.toArray(new ExplicitOperator[valid.size()]);			
		}
		return new ExplicitOperator[0];
	}



	/**
	 * 
	 * ExplicitOperator objects are {@link Mapping} compatible Operators acting as
	 * {@link BiFunction} returning a Boolean.
	 * 
	 * @see Op
	 *
	 * 
	 */
	public interface ExplicitOperator<L, R> extends BiFunction<L,R,Boolean>, ExplicitOperation{

		@Override
		public Boolean apply(L t, R u);

		/**
		 * @return The Unicode symbol of this operator
		 */
		@Override
		public String toString();

		/**
		 * @return {@code true} if objects of the provided Class would be accepted as a left operand, {@code false} otherwise
		 */
		public boolean isLeftValid(Class<?> clazz);

		/**
		 * @return {@code true} if objects of the provided Class would be accepted as a right operand, {@code false} otherwise
		 */
		public boolean isRightValid(Class<?> clazz);

		public default boolean isRightValid(String dataClass){
			Class<?> clazz = null;
			try {
				clazz = Class.forName(dataClass);
			} catch (ClassNotFoundException e) {
				//Check primitive classes
				if(dataClass.equals(byte.class.getTypeName())
						||dataClass.equals(byte.class.getTypeName())
						||dataClass.equals(short.class.getTypeName())
						||dataClass.equals(int.class.getTypeName())
						||dataClass.equals(long.class.getTypeName()))
					return isRightValid(Number.class);
				else if(dataClass.equals(boolean.class.getTypeName()))
						return isRightValid(boolean.class);
				else if(dataClass.equals(char.class.getTypeName()))
					return isRightValid(char.class);
				return false;
			}			
			return isRightValid(clazz);			
		}
	}

	/**
	 * Boolean operators
	 * 
	 * @author Guillaume
	 *
	 */
	@Mapping(id = "BooleanOperator")
	public enum Bool implements BinaryOperator<Boolean>, ExplicitOperator<Boolean,Boolean>{
		AND{
			@Override
			public Boolean apply(Boolean t, Boolean u) {
				return t&&u;
			}

			@Override
			public String toString() {
				return "\u2227";
			}
			
		}
		, OR{
			@Override
			public Boolean apply(Boolean t, Boolean u) {
				return t||u;
			}

			@Override
			public String toString() {
				return "\u2228";
			}
		}
		, XOR{
			@Override
			public Boolean apply(Boolean t, Boolean u) {
				return t^u;
			}

			@Override
			public String toString() {
				return "\u22BB";
			}
		}
		, NOT{
			@Override
			public Boolean apply(Boolean t, Boolean u) {
				return t!=u;
			}

			@Override
			public String toString() {
				return "\u00AC";
			}
		};	


		@Override
		public boolean isLeftValid(Class<?> clazz) {
			return clazz == Boolean.class || clazz == boolean.class;
		}

		@Override
		public boolean isRightValid(Class<?> clazz) {
			return isLeftValid(clazz);
		}
		

		@Override
		public String description() {
			return toString();
		}
	}


	/**
	 * Logical operators
	 * 
	 * @author Guillaume
	 *
	 */
	@Mapping(id = "LogicalOperator")
	public enum Logical implements ExplicitOperator<Number,Number>{
		SUP{
			@Override
			public Boolean apply(Number t, Number u) {
				return t!=null && t.doubleValue()>u.doubleValue();
			}

			@Override
			public String toString() {
				return "\u003E";
			}			
		},
		INF{
			@Override
			public Boolean apply(Number t, Number u) {
				return t!=null && t.doubleValue()<u.doubleValue();
			}

			@Override
			public String toString() {
				return "\u003C";
			}			
		},
		SUP_EQ{
			@Override
			public Boolean apply(Number t, Number u) {
				return t!=null && t.doubleValue() >= u.doubleValue();
			}

			@Override
			public String toString() {
				return "\u2265";
			}			
		},
		INF_EQ{
			@Override
			public Boolean apply(Number t, Number u) {
				return t!=null && t.doubleValue() <= u.doubleValue();
			}

			@Override
			public String toString() {
				return "\u2264";
			}			
		},
		EQUALS{
			@Override
			public Boolean apply(Number t, Number u) {
				return t!=null && t.doubleValue() == u.doubleValue();
			}

			@Override
			public String toString() {
				return "\u003D";
			}
		},
		DIF{
			@Override
			public Boolean apply(Number t, Number u) {
				return t!=null && t.doubleValue() != u.doubleValue();
			}

			@Override
			public String toString() {
				return "\u2260";
			}
			
			
		};		


		@Override
		public boolean isLeftValid(Class<?> clazz) {
			return Number.class.isAssignableFrom(clazz) 
					|| clazz == short.class
					|| clazz == int.class
					|| clazz == long.class
					|| clazz == float.class
					|| clazz == double.class;
		}

		@Override
		public boolean isRightValid(Class<?> clazz) {
			return isLeftValid(clazz);
		}
		

		@Override
		public String description() {
			return toString();
		}
	}


	@Mapping(id = "TextOperator")
	public enum TextOp implements ExplicitOperator<String, String>{
		CONTAINS{

			@Override
			public Boolean apply(String t, String u) {
				return t!=null && t.contains(u);
			}

			@Override
			public String toString() {
				return "\u220B";
			}

		},
		CONTAINS_NOT{
			@Override
			public Boolean apply(String t, String u) {
				return t!=null && !t.contains(u);
			}

			@Override
			public String toString() {
				return "\u220C";
			}			
		},
		EQUALS{
			@Override
			public Boolean apply(String t, String u) {
				return (t==null && u==null) || t.equals(u);
			}

			@Override
			public String toString() {
				return "\u2261";
			}			
		},
		DISTINCT{
			@Override
			public Boolean apply(String t, String u) {
				return t!=null && !t.equals(u);
			}

			@Override
			public String toString() {
				return "\u2262";
			}			
		};

		@Override
		public boolean isLeftValid(Class<?> clazz) {
			return clazz == String.class;
		}

		@Override
		public boolean isRightValid(Class<?> clazz) {
			return isLeftValid(clazz);
		}
		

		@Override
		public String description() {
			return toString();
		}
	}



	/**
	 * Operators for elements in sets (http://en.wikipedia.org/wiki/Element_(mathematics)) 
	 * 
	 * @author Guillaume
	 *
	 */
	@Mapping(id = "ElementOperator")
	public enum ElementOp implements ExplicitOperator<Object[], Object>{
		ELEMENT_OF{

			@Override
			public Boolean apply(Object[] t, Object u) {
				if(t == null || u == null)
					return false;

				for(Object i : t)
					if(u.equals(i))
						return true;
				return false;
			}

			@Override
			public String toString() {
				return "\u2208";
			}			
		}, 
		UNIQUE{

			@Override
			public Boolean apply(Object[] t, Object u) {
				if(t == null || u == null)
					return false;

				int counter = 0;
				for(Object i : t)
					if(u.equals(i))
						counter++;
				return counter == 1;
			}

			@Override
			public String toString() {
				return "\u2203!";
			}			
		};

		@Override
		public boolean isLeftValid(Class<?> clazz) {
			return clazz.isArray();
		}

		@Override
		public boolean isRightValid(Class<?> clazz) {
			return true;
		}
		

		@Override
		public String description() {
			return toString();
		}
	}

	/**
	 * Operators for <a href="http://en.wikipedia.org/wiki/Element_(mathematics)">elements</a> in a Set 
	 * 
	 * @author Guillaume
	 *
	 */
	@Mapping(id = "SetOperator")
	public enum SetOp implements ExplicitOperator<Object[], Object[]>{
		ANY_OF{

			@Override
			public Boolean apply(Object[] t, Object[] u) {
				if(t == null || u == null)
					return false;
				for(Object i : t)
					for(Object j : u)
						if(j.equals(i))
							return true;
				return false;
			}

			@Override
			public String toString() {
				return "\u220B";
			}			
		}, 
		ANY_ONE_OF{

			@Override
			public Boolean apply(Object[] t, Object[] u) {
				if(t == null || u == null)
					return false;
				int counter = 0;
				for(Object i : t)
					for(Object j : u)
						if(j.equals(i))
							counter++;
				return counter == 1;
			}

			@Override
			public String toString() {
				return "\u0021";
			}			
		};

		@Override
		public boolean isLeftValid(Class<?> clazz) {
			return clazz.isArray();
		}

		@Override
		public boolean isRightValid(Class<?> clazz) {
			return true;
		}
		

		@Override
		public String description() {
			return toString();
		}
	}
	
	@Mapping(id = "ObjectOperator")
	public enum ObjectOp implements ExplicitOperator<Object, Object>{
		
		EQUALS(){

			@Override
			public Boolean apply(Object t, Object u) {
				
				return t==null? t==u : t.equals(u);
			}

			@Override
			public boolean isLeftValid(Class<?> clazz) {
				return true;
			}

			@Override
			public boolean isRightValid(Class<?> clazz) {
				return true;
			}
			
			@Override
			public String toString() {
				return "\u2261";
			}
					
		},
		
		DISTINCT(){

			@Override
			public Boolean apply(Object t, Object u) {				
				return t==null? t!=u : !t.equals(u);
			}

			@Override
			public boolean isLeftValid(Class<?> clazz) {
				return true;
			}

			@Override
			public boolean isRightValid(Class<?> clazz) {
				return true;
			}
			
			@Override
			public String toString() {
				return "\u2262";
			}
					
		};
		
		@Override
		public String description() {
			return toString();
		}
		
	}

}

package org.pickcellslab.foundationj.dbm.queries.builders;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

/**
 * A step in a stepwise builder pattern to create a {@link TraversalDefinition}
 *
 * @param <T> The type of the next builder step
 * @param <S> The type of the data element which will be encountered as the query processes.
 */
public interface RootDefinition<T, S> {

	/**
	 * Specifies that the root node must possess the given key / value pair
	 * @param k The {@link AKey} of the desired attribute
	 * @param value The required value for the desired attribute
	 * @return The next step in the build process
	 */
	public <E> T having(AKey<E> k, E value);
	
	/**
	 * Specifies a filter to define the node(s) which will be used as root(s) of the 
	 * <a href="https://en.wikipedia.org/wiki/Graph_traversal">traversal</a>.
	 * @param test {@link ExplicitPredicate}
	 * @return The next step in the build process
	 */
	public T acceptedBy(ExplicitPredicate<? super S> test);
	
}

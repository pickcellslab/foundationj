package org.pickcellslab.foundationj.datamodel.functions;

import java.lang.reflect.Array;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.annotations.Mapping;

@Mapping(id = "Angle")
public class AngleMaker implements ExplicitCombiner<Object,Object,Double> {


	
	@Override
	public Double combine(Object input1, Object input2) {
		
		if(input1 == null || input2 == null)
			return null;
		
		if(Array.getLength(input1)!=Array.getLength(input2))
			return Double.NaN;
				
		if(Array.getLength(input1) == 2){
			Vector2D v = new Vector2D(Array.getDouble(input1, 0), Array.getDouble(input1, 1));
			//Check for zero norm
			if(v.getNorm()==0)
				return Double.NaN;
			Vector2D ref = new Vector2D(Array.getDouble(input2, 0), Array.getDouble(input2, 1));
			if(ref.getNorm()==0)
				return Double.NaN;
			return FastMath.toDegrees(Vector2D.angle(v, ref));
		}
		else if(Array.getLength(input1) > 2){
			Vector3D v = new Vector3D(Array.getDouble(input1, 0), Array.getDouble(input1, 1), Array.getDouble(input1, 2));
			if(v.getNorm()==0)
				return Double.NaN;
			Vector3D ref = new Vector3D(Array.getDouble(input2, 0), Array.getDouble(input2, 1), Array.getDouble(input2, 2));	
			if(ref.getNorm()==0)
				return Double.NaN;		
			return FastMath.toDegrees(Vector3D.angle(v, ref));
		}
		else
			return Double.NaN;
	}
	
	
	@Override
	public String toString(){
		return "Angle";
	}

	@Override
	public String description() {
		return "Angle";
	}

	@Override
	public Class<Double> getReturnType() {
		return double.class;
	}


	

}

package org.pickcellslab.foundationj.datamodel.predicates;

import java.util.Objects;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Predicate;

import org.pickcellslab.foundationj.annotations.DenyMapping;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.mapping.exceptions.UnsupportedMappingException;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;


/**
 * ExplicitPredicates are designed to be stored as part of the user-defined description of the data property graph.
 * To fulfill this role, ExplicitPredicate objects build and store a human readable description of the operation they perform.
 * <br>
 * <br> 
 * <b>NB:</b> Implementing class MUST be annotated with @{@link Mapping}.
 * 
 * @see P 
 * @see Mapping
 */

public interface ExplicitPredicate<T> extends Predicate<T>{

	
	/**
     * @param input The input to be tested
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    public boolean test(T input);
	
	
	/**
	 * @return A description of what this ExplicitPredicate is about
	 */
	public String description();

	
	public default ExplicitPredicate<T> and(ExplicitPredicate<? super T> other){
		return new Merge<>(this, Op.Bool.AND, other);
	}
	
	public default ExplicitPredicate<T> or(ExplicitPredicate<? super T> other){
		return new Merge<>(this, Op.Bool.OR, other);
	}
	
	public default ExplicitPredicate<T> xOr(ExplicitPredicate<? super T> other){
		return new Merge<>(this, Op.Bool.XOR, other);
	}
	
	
	/**
	 * Performs an operation between this ExplicitPredicate and the provided ExplicitPredicate. The performed operation
	 * is dictated by the specified {@link ExplicitOperator} 
	 * @param o The Operator for this merge
	 * @param p the ExplicitPredicate to merge
	 * @return A new ExplicitPredicate resulting from the merge operation
	 */
	public default ExplicitPredicate<T> merge(ExplicitOperator<Boolean,Boolean> o, ExplicitPredicate<? super T> p){				
		return new Merge<>(this, o, p);		
	}
	
	
	
	public default ExplicitPredicate<T> negate(){
		return new Negate<>(this);		
	}
		
	
	
	
	/**
	 * WARNING: The returned {@link ExplicitPredicate} will NOT be {@link Mapping} compatible
	 * @param predicate
	 * @param name
	 * @param description
	 * @return An ExplicitPredicate using the given {@link Predicate} to evaluate inputs.
	 */
	@Deprecated
	public static <T> ExplicitPredicate<T> fromPredicate(Predicate<T> predicate, String name, String description){
		assert predicate != null : "predicate must not be null";
		return predicate instanceof ExplicitPredicate ? (ExplicitPredicate<T>)predicate : new FromPredicate<>(predicate, name, description);		
	}
	
	
	
	

	@DenyMapping
	static class FromPredicate<T> implements ExplicitPredicate<T>{

		private final Predicate<T> predicate;
		private final String name, description;
		
		public FromPredicate(Predicate<T> predicate, String name, String description) {
			assert predicate != null : "predicate must not be null";
			assert name != null : "name must not be null";
			this.predicate = predicate;
			this.name = name;
			this.description = description == null ? "NA" : description;
		}
		
		@Override
		public boolean test(T t) {
			return predicate.test(t);
		}

		@Override
		public String description() {
			return description;
		}
		
		@Override
		public String toString() {
			return name;
		}
		
	}
	
	@Mapping(id = "NegatedTest")
	@Scope
	static class Negate<T> implements ExplicitPredicate<T>{

		@Supported
		private final ExplicitPredicate<T> predicate;
		
		public Negate(ExplicitPredicate<T> predicate) {
			Objects.requireNonNull(predicate,"predicate is null");
			MappingUtils.exceptionIfInvalid(predicate);
			this.predicate = predicate;
		}
		
		@Override
		public boolean test(T i) {
			return !predicate.test(i);
		}

		@Override
		public String description() {
			return "NOT "+predicate.description();
		}
		
		@Override
		public String toString() {
			return "Not"+predicate.toString();
		}
		
	}

	@Mapping(id = "ComposedTest")
	@Scope
	static class Merge<T> implements ExplicitPredicate<T>{

		@Supported
		private final ExplicitPredicate<T> left;
		@Supported
		private final ExplicitOperator<Boolean,Boolean> operator;
		@Supported
		private final ExplicitPredicate<? super T> right;
		
		private Merge(ExplicitPredicate<T> left, ExplicitOperator<Boolean,Boolean> o, ExplicitPredicate<? super T> right) throws UnsupportedMappingException {
			Objects.requireNonNull(left,"left is null");
			Objects.requireNonNull(o,"operator is null");
			Objects.requireNonNull(right,"right is null");
			MappingUtils.exceptionIfInvalid(left);
			MappingUtils.exceptionIfInvalid(o);
			MappingUtils.exceptionIfInvalid(right);
			this.left = left;
			this.right = right;
			this.operator = o;
		}
		
		@Override
		public boolean test(T i) {
			return operator.apply(left.test(i), right.test(i));
		}

		@Override
		public String description() {
			return left.description() + " " +operator.toString()+ " " + right.description();
		}
		
		@Override
		public String toString() {
			return left.toString() + " " +operator.toString()+ " " + right.toString();
		}
		
	}
	
	
}

package org.pickcellslab.foundationj.datamodel.builders;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.Constant;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.DataTransform;
import org.pickcellslab.foundationj.datamodel.functions.KeySelector;
import org.pickcellslab.foundationj.datamodel.predicates.BiFunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.FilteringBehaviour.Behaviour;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.datamodel.predicates.SimpleFilter;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

public final class SimpleFilterBuilder {



	public final FirstTransition<WritableDataItem> includeAll(){
		SimpleFilter<WritableDataItem> f = new SimpleFilter<>(Behaviour.INCLUDE, new String[0]);
		return new FirstTransition<>(f);
	}
	public final FirstTransition<NodeItem> includeAnyNode(){
		SimpleFilter<NodeItem> f = new SimpleFilter<>(Behaviour.INCLUDE, new String[0]);
		return new FirstTransition<>(f);
	}
	public final FirstTransition<Link> includeAnyLink(){
		SimpleFilter<Link> f = new SimpleFilter<>(Behaviour.INCLUDE, new String[0]);
		return new FirstTransition<>(f);
	}
	
	@SafeVarargs
	public final FirstTransition<NodeItem> include(Class<? extends NodeItem>... classes){
		SimpleFilter<NodeItem> f = new SimpleFilter<>(Behaviour.INCLUDE, classesToTypeIds(classes));
		return new FirstTransition<>(f);
	}
	@SafeVarargs
	public final FirstTransition<NodeItem> exclude(Class<? extends NodeItem>... classes){
		SimpleFilter<NodeItem> f = new SimpleFilter<>(Behaviour.EXCLUDE, classesToTypeIds(classes));
		return new FirstTransition<>(f);
	}
	@SafeVarargs
	public final FirstTransition<Link> include(String... types){
		SimpleFilter<Link> f = new SimpleFilter<>(Behaviour.INCLUDE,  types);
		return new FirstTransition<>(f);
	}
	@SafeVarargs
	public final FirstTransition<Link> exclude(String... types){
		SimpleFilter<Link> f = new SimpleFilter<>(Behaviour.EXCLUDE,  types);
		return new FirstTransition<>(f);
	}

	@SafeVarargs
	private final String[] classesToTypeIds(Class<? extends NodeItem>... classes){		
		String[] strings = new String[classes.length];
		for(int i = 0; i<classes.length; i++) {
			final String typeId = DataRegistry.typeIdFor((Class<? extends NodeItem>) classes[i]);
			if(typeId==null)
				throw new IllegalArgumentException(classes[i]+ "is not a known data type (no @Data annotation found)");
			strings[i] = typeId;
		}
		return strings;
	}



	/**
	 * An Intermediate in the process of building a {@link SimpleFilter}
	 * @author Guillaume Blin
	 *
	 */
	public static class FirstTransition<T extends WritableDataItem>{

		private SimpleFilter<T> f;
		
		private FirstTransition(SimpleFilter<T> f){
			this.f = f;
		}
		
		public <L,R> NextTransition<T> with(AKey< ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<T,L,R> p = new BiFunctionPredicate<T,L,R>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(null, p);
			return new NextTransition<>(f);
		}
		
		
		public <L> NextTransition<T> with(AKey<L> k, ExplicitPredicate<L> p){
			f.addConstrainsts(null, P.keyTest(k,p));
			return new NextTransition<>(f);
		}
		
		
		public NextTransition<T> with(DataTransform<T> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<T,Double,Double> p = new BiFunctionPredicate<T,Double,Double>(
					t, new Constant<>(value), o);
			f.addConstrainsts(null, p);
			return new NextTransition<>(f);
		}
		
		public <N,R> NextTransition<T> with(ExplicitFunction<T,N> t, ExplicitOperator<? super N,? super R> o, R value){
			BiFunctionPredicate<T,N,R> p = new BiFunctionPredicate<T,N,R>(
					t, new Constant<>(value), o);
			f.addConstrainsts(null, p);
			return new NextTransition<>(f);
		}
		
		
		public ExplicitPredicate<T> create() {
			return f;
		}


	}

	/**
	 * An Intermediate in the process of building a {@link SimpleFilter}
	 * @author Guillaume Blin
	 */
	public static class NextTransition<T extends WritableDataItem>{

		private SimpleFilter<T> f;
		
		private NextTransition(SimpleFilter<T> f){
			this.f = f;
		}
		
		public <L,R> NextTransition<T> OR(AKey< ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<T,L,R> p = new BiFunctionPredicate<T,L,R>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.OR, p);
			return this;
		}
		public NextTransition<T> OR(DataTransform<T> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<T,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.OR, p);
			return this;
		}
		public <L> NextTransition<T> OR(AKey<L> k, ExplicitPredicate<L> p){
			f.addConstrainsts(Op.Bool.OR, P.keyTest(k,p));
			return this;
		}

		public <L,R> NextTransition<T> XOR(AKey< ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<T,L,R> p = new BiFunctionPredicate<T,L,R>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.XOR, p);
			return this;
		}
		public <L> NextTransition<T> XOR(AKey<L> k, ExplicitPredicate<L> p){
			f.addConstrainsts(Op.Bool.XOR, P.keyTest(k,p));
			return this;
		}
		public NextTransition<T> XOR(DataTransform<T> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<T,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.XOR, p);
			return this;
		}
		public <L,R> NextTransition<T> AND(AKey< ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<T,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.AND, p);
			return this;
		}
		public <L> NextTransition<T> AND(AKey<L> k, ExplicitPredicate<L> p){
			f.addConstrainsts(Op.Bool.AND, P.keyTest(k,p));
			return this;
		}
		public NextTransition<T> AND(DataTransform<T> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<T,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.AND, p);
			return this;
		}

		public ExplicitPredicate<T> create() {
			return f;
		}



	}





}

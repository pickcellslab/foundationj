package org.pickcellslab.foundationj.bootstrap;

/*-
 * #%L
 * foundationj-modules
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A few utility methods useful mainly to locate specific classes in directories (registered in META-INF/services/)
 * @author Guillaume Blin
 *
 */
public abstract class ScanUtils {

	private static final Logger log = LoggerFactory.getLogger(ScanUtils.class);


	/**
	 * Scans the specified folder for classes implementing an interface. This is done the way service locator works but can take
	 * a custom path within the jar
	 * @param jarParentDir the directory in which the jar files are to be scanned
	 * @param inJarPath The path within the jar
	 * @param itfc The service to look for
	 * @return A List of class that can provide the specified service
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static <E> Set<Class<? extends E>>  getImplTypes(File jarParentDir, String inJarPath, Class<E> itfc) throws IOException, ClassNotFoundException{

		//Use a set to avoid duplicates
		Set<Class<? extends E>> result = new HashSet<>();

		for (File jar : jarParentDir.listFiles()){

			if(jar.getName().endsWith(".jar")){

				//System.out.println("found :" + jar.getName());

				JarFile jf = new JarFile(jar);

				List<JarEntry> entriesToRead = 				
						jf.stream()
						.filter(e -> e.getName().equals(inJarPath+itfc.getTypeName()))
						.collect(Collectors.toList());


				for(JarEntry je : entriesToRead)
					result.addAll((List)getClassesIn(jf,je));

				jf.close();				
			}
		}

		return result;

	}





	/**
	 * Scans the specified folder for jars containing the given entry. 
	 * @param jarParentDir the directory in which the jar files are to be scanned
	 * @param inJarPath The jar entry
	 * @return A List of types defined inside the jar entries
	 * @throws IOException 
	 */
	public static Set<Class<?>>  getTypeList(File jarParentDir, String inJarPath) throws IOException{

		//Use a set to avoid duplicates
		Set<Class<?>> result = new HashSet<>();

		for (File jar : jarParentDir.listFiles()){

			if(jar.getName().endsWith(".jar")){

				//System.out.println("found :" + jar.getName());

				final JarFile jf = new JarFile(jar);

				jf.stream()
				//.map(e->{
				//	System.out.println("entry :" + e.getName());
				//	return e;
				//})
				.filter(e -> e.getName().equals(inJarPath))
				.forEach(e->{
					log.debug(e+" found in "+jar.getName());
					result.addAll(getClassesIn(jf, e));

				});

				jf.close();				
			}
		}

		return result;

	}





	/**
	 * Scans the specified folder for classes implementing an interface. This is done the way service locator works but can take
	 * a custom path within the jar
	 * @param jarParentDir the directory in which the jar files are to be scanned
	 * @param inJarPath The path within the jar
	 * @param itfc The service to look for
	 * @return A List of class that can provide the specified service
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static Map<Class<?>, Set<Class<?>>>  getInterfacesAndImpl(File jarParentDir, String inJarPath) throws IOException{

		//Use a set to avoid duplicates		
		final Map<String, Set<String>> tmp = new HashMap<>();

		// Iterate over jar files in the given directory
		for (File jar : jarParentDir.listFiles()){

			if(jar.getName().endsWith(".jar")){

				final JarFile jf = new JarFile(jar);

				// Find all entries inside the inJarPath
				final List<JarEntry> entriesToRead = 				
						jf.stream()
						.filter(e -> e.getName().startsWith(inJarPath) && e.getName().length()>inJarPath.length())
						.collect(Collectors.toList());

				// Now for each entry get the set of classes declared within				
				for(JarEntry je : entriesToRead) {

					final String itfcName = je.getName().substring(inJarPath.length());

					Set<String> set = tmp.get(itfcName);
					if(set == null) {
						set = new HashSet<>();
						tmp.put(itfcName, set);
					}

					for(String implName : getLinesIn(jf, je)){
						set.add(implName);
					}

				}

				jf.close();				
			}
		}


		// Now convert the Strings to classes
		final Map<Class<?>, Set<Class<?>>> result = new HashMap<>();




		for(Entry<String, Set<String>> e : tmp.entrySet()) {
			Class<?> itfcClazz = null;
			try {
				itfcClazz = Class.forName(e.getKey());
			}catch(ClassNotFoundException x) {
				log.error("Unable to get the class"+ e.getKey() +"while scanning for types", x);
				continue;
			}

			final Set<Class<?>> set = new HashSet<>();
			result.put(itfcClazz, set);
			for(String s : e.getValue()) {
				try {
					set.add(Class.forName(s));
				}catch(ClassNotFoundException x) {
					log.error("Unable to get the class"+ e.getValue() +"while scanning for types", x);
					continue;
				}
			}
		}



		return result;

	}











	/**
	 * @param jarParentDir
	 * @param inJarPath
	 * @param itfc
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static <E> Set<Class<? extends E>>  getImplList(File jarParentDir, String inJarPath, Class<E> itfc) throws IOException, ClassNotFoundException{

		//Use a set to avoid duplicates
		Set<Class<? extends E>> result = new HashSet<>();

		for (File jar : jarParentDir.listFiles()){

			if(jar.getName().endsWith(".jar")){

				//System.out.println("found :" + jar.getName());

				JarFile jf = new JarFile(jar);

				jf.stream()
				.filter(e -> e.getName().startsWith(inJarPath))
				.map(e->e.getName().substring(inJarPath.length()))
				.map(s->{
					try {
						return (Class<? extends E>)Class.forName(s);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
						return null;
					}
				})
				.forEach(c->result.add(c));



				jf.close();				
			}
		}

		return result;

	}





	private static List<String> getLinesIn(JarFile f, JarEntry s) {

		InputStream is = null;
		BufferedReader br = null;
		String line;
		List<String> list = new ArrayList<>();

		try { 
			is = f.getInputStream(s);
			br = new BufferedReader(new InputStreamReader(is));
			while (null != (line = br.readLine())) {
				list.add(line);
				//System.out.println(line);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (br != null) br.close();
				if (is != null) is.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}




	private static List<Class<?>> getClassesIn(JarFile f, JarEntry s) {

		final List<Class<?>> classes = new ArrayList<>();
		for(String name : getLinesIn(f,s)){
			try {
				classes.add(Class.forName(name));
			} catch (ClassNotFoundException e1) {
				log.error("Unable to get the clazz while scanning for types", e1);
			}
		}

		return classes;
	}

}

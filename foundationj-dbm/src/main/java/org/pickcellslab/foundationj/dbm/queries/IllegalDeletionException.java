package org.pickcellslab.foundationj.dbm.queries;

/**
 * 
 * Checked Exception thrown when a deletion has been attempted on objects which cannot be deleted
 * 
 * @author Guillaume Blin
 *
 */
public class IllegalDeletionException extends Exception {

	
	public IllegalDeletionException(String msg) {
		super(msg);
	}
	
	public IllegalDeletionException(String msg, Throwable t) {
		super(msg,t);
	}
	
}

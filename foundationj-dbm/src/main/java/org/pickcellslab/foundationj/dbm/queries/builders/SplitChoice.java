package org.pickcellslab.foundationj.dbm.queries.builders;

import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.CombinedBinary;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;

public interface SplitChoice<T,O> {

	public QueryRunner<OrganisedResult<String,O>> groupBy(ExplicitFunction<? super T,String> categorical);
	
	public QueryRunner<OrganisedResult<Integer,O>> asCategories(ExplicitFunction<? super T,Integer> categorical);
	
	public CategoryAdder<QueryRunner<OrganisedResult<ExplicitPredicate<? super T>,O>>,T> createCategory(ExplicitPredicate<? super T> filter);
	
	public BinaryCombinationAdder<QueryRunner<OrganisedResult<CombinedBinary<T>,O>>,T> withBinaryCategories(ExplicitPredicate<? super T> filter);
	
	public QueryRunner<OrganisedResult<Boolean,O>> as2Groups(ExplicitPredicate<? super T> test);

	public <K> QueryRunner<OrganisedResult<K,O>> withSplitter(ExplicitSplit<? super T,K> splitter);
	

	//TODO groupByDepth
	
	
}

package org.pickcellslab.foundationj.ui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.util.Arrays;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.ui.parsers.TextFieldParser;

/**
 * A Simple JDialog displaying an editable JTextField for a user to enter a value for a specific {@link AKey} object. 
 * Once the user presses Ok, the value is parsed and a check is perform to ensure that the value is compatible with
 * a specific AKey type.
 * The value can be retrieved using the getValue() method. 
 * 
 * @author Guillaume Blin
 *
 * @param <E> The type of the value the user will be allowed to enter
 */
@SuppressWarnings("serial")
public class ValueSelector<E> extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Class<E> clazz;
	private String labelContent = "Please enter ";

	private JTextField textField;

	private E value;

	/**
	 * Creates a new ValueSelector configured to allow the user to enter a value compatible with the given AKey 
	 */
	public ValueSelector(AKey<E> k){

		this.setTitle("Value Selector");

		this.clazz = k.type();

		if(clazz == String.class)
			labelContent = labelContent+ ("a text");
		else if(clazz == Boolean.class || clazz == boolean.class)
			labelContent = labelContent+ ("a boolean value");
		else if(!clazz.isArray())
			labelContent = labelContent+ ("a number");
		else {
			if(k.dType() == dType.NUMERIC)
				labelContent = labelContent+ ("a sequence of numbers separated by comas");
			else if(clazz == String[].class)
				labelContent = labelContent+ ("a sequence of texts separated by comas");
			else if(clazz == Boolean[].class || clazz == boolean.class)
				labelContent = labelContent+ ("a sequence of booleans separated by comas");
		}





		setBounds(100, 100, 285, 140);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		JLabel lblPleaseEnterSome = new JLabel(labelContent);

		textField = new JTextField();
		textField.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(textField, GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
								.addComponent(lblPleaseEnterSome))
						.addContainerGap())
				);
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblPleaseEnterSome)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(e-> {					
					//Parse the value and check that it is compatible
					//If yes, then the UI can be disposed of

					try{
						if(textField.getText().isEmpty())
							throw new ParseException("You need to enter a value first.", 0);
						value = TextFieldParser.parse(textField.getText(), clazz);

					}catch(ParseException exception){
						JOptionPane.showMessageDialog(null, exception.getMessage());
						return;
					}

					this.dispose();
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(e-> {
					value = null;
					this.dispose();
				});
				buttonPane.add(cancelButton);
			}
		}
	}




	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ValueSelector<double[]> dialog = new ValueSelector<double[]>(AKey.get("someKey", double[].class));
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setModal(true);
			dialog.setVisible(true);
			System.out.println(Arrays.toString(dialog.getValue()));
			
			ValueSelector<String> dialog2 = new ValueSelector<>(AKey.get("someKey", String.class));
			dialog2.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog2.setModal(true);
			dialog2.setVisible(true);
			System.out.println(dialog2.getValue());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




	public E getValue() {
		return value;
	}


}

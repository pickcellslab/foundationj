package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

@SuppressWarnings("serial")
public class FitMethodChoiceDialog extends JDialog {
	
	public boolean wasCancelled = true;
	private FittingMethod<?> method;
	
	
	public FitMethodChoiceDialog(List<FittingMethod<?>> methods) {
		
		Objects.requireNonNull(methods);
		if(methods.size() == 0)
			throw new IllegalArgumentException("There are no methods available");
		
		
		JLabel lblAvailableMethods = new JLabel("Available Methods");
		
		JComboBox<FittingMethod<?>> comboBox = new JComboBox<>();
		comboBox.setRenderer(new FitMethodRenderer());
		for(FittingMethod<?> m : methods)
			comboBox.addItem(m);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{
			method = (FittingMethod<?>) comboBox.getSelectedItem();
			wasCancelled = false;
			this.dispose();
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->this.dispose());
		
		
		
		//Layout
		//-----------------------------------------------------------------------------------------------
		
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblAvailableMethods)
					.addGap(28)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(72, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(215, Short.MAX_VALUE)
					.addComponent(btnOk)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCancel)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAvailableMethods)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 194, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnOk)
						.addComponent(btnCancel))
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);
	}
	
	
	public boolean wasCancelled(){
		return wasCancelled;
	}
	
	public FittingMethod<?> get(){
		return method;
	}
	
}

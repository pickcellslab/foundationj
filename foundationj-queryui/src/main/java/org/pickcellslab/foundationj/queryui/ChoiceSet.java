package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.swing.event.ListSelectionListener;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.services.theme.UITheme;

import com.alee.extended.panel.WebAccordion;
import com.alee.extended.panel.WebAccordionStyle;
import com.alee.extended.panel.WebCollapsiblePane;


@SuppressWarnings("serial")
public class ChoiceSet extends WebAccordion{

	private final ChoicePanel<MetaQueryable> master;
	private final ChoicePanelCard<MetaFilter> filterPane;
	private final ChoicePanelCard<MetaReadable<?>> readPane;



	public ChoiceSet(UITheme theme, DataAccess access, Predicate<? super MetaQueryable> p) throws Exception {


		super(WebAccordionStyle.accordionStyle);
		
	    setMultiplySelectionAllowed ( false );

		
		
		//Queryable choices
		master = ChoicePanels.queryables(theme, access, p, "");		
		WebCollapsiblePane cp1 = addPane(null,"Available Types",master);
		master.addHihlightListener((s,b)->cp1.expand());
		//FilterChoices
		Function<MetaQueryable, ChoicePanel<MetaFilter>> 
		filterChoiceFactory =	(mq)-> {
			try {
				return ChoicePanels.allFilters(theme, access, "", mq);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		};


		filterPane = new ChoicePanelCard<>( master, filterChoiceFactory);		
		WebCollapsiblePane cp2 = addPane(null, "Available Filters", filterPane);
		filterPane.addHihlightListener((s,b)->cp2.expand());


		//ReadableChoice
		Function<MetaQueryable, ChoicePanel<MetaReadable<?>>> 
		readChoiceFactory =	(mq)-> {
			try {
				return ChoicePanels.allReadables(theme, access, "", mq);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		};
		readPane = new ChoicePanelCard<>( master, readChoiceFactory);		
		WebCollapsiblePane cp3 = addPane(null, "Available Attributes", readPane);
		readPane.addHihlightListener((s,b)->cp3.expand());

	}
	
		

	public void highlightMaster(){
		master.setHighlighted(true);
	}

	public void highlightFilter(MetaQueryable mq){
		master.setSelected(mq);
		filterPane.setHighlighted(true);
	}

	public void highlightReads(MetaQueryable mq){
		master.setSelected(mq);
		readPane.setHighlighted(true);
	}


	public Optional<MetaQueryable> getCurrentQueryable(){
		return master.getSelected();
	}

	public Optional<MetaFilter> getCurrentFilter(MetaQueryable mq){
		ChoicePanel<MetaFilter> p = filterPane.get(mq);
		if(null == p)
			return Optional.empty();
		return p.getSelected();
	}

	public Optional<MetaReadable<?>> getCurrentReadable(MetaQueryable mq){
		ChoicePanel<MetaReadable<?>> p = readPane.get(mq);
		if(null == p)
			return Optional.empty();
		return p.getSelected();
	}

	public ChoicePanel<MetaQueryable> getQueryableChoice(){
		return master;
	}

	public ChoicePanel<MetaFilter> getFilterChoice(MetaQueryable mq){
		return filterPane.get(mq);
	}

	public ChoicePanel<MetaReadable<?>> getReadableChoice(MetaQueryable mq){
		return readPane.get(mq);
	}

	
	public void filterQueryable(Predicate<? super MetaQueryable> predicate){
		Objects.requireNonNull(predicate);
		master.addFilter(predicate);
	}
	
	public void removeFilterOnQueryable(){
		master.removeFilters();
	}
	

	public void removeMasterSelectionListener(ListSelectionListener l){		
		master.removeSelectionListener(l);
	}

	public void addMasterSelectionListener(ListSelectionListener l){
		//Will be added after the read and filter choice panel were added so we are safe
		master.addSelectionListener(l);
	}


}

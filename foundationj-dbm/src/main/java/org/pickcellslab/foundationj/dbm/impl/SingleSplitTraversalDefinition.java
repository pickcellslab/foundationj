package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.PathTraverser;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.SplitTraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

class SingleSplitTraversalDefinition<V, E, T, K> implements SplitTraversalDefinition<V,E,T,K>{

	private final TraversalDefinition td;
	private final ExplicitPathSplit<V, E, K> splitter;
	private TargetType<V, E, T> target;

	SingleSplitTraversalDefinition(TargetType<V,E,T> target, TraversalDefinition td, ExplicitPathSplit<V,E,K> splitter){		
		this.td = td;
		this.target = target;
		this.splitter = splitter;
	}

	@Override
	public String descrition(){
		String d = td.description()+" "+splitter.description();
		return d;
	}



	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<K, Iterable<T>> split(DatabaseAccess<V, E> db) {		

		FjAdaptersFactory<V,E> f = db.createFactory();		
		PathTraverser<V,E> tr = db.traverser(td);		
		splitter.setFactory(f);	

		Map<K, List<T>> map = new HashMap<>();

		// Iterate over Paths
		Iterator<Path<V,E>> itr = tr.paths();
		while(itr.hasNext()){			
			Path<V,E> p = itr.next();
			K k = splitter.getKeyFor(p);
			List<T> list = map.get(k);
			if(list == null){
				list = new ArrayList<>();
				map.put(k, list);
			}
			T t = target.last(p,f);
			if(null!=t)
				list.add(t);			
		}

		return	(Map)map;
	}

}

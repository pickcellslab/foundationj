package org.pickcellslab.foundationj.dataviews.mvc;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

/**
 * @author Guillaume Blin
 *
 * @param <M> The type of model used by the view * 
 * @param <T> The type of database element the view is defined for
 * @param <B> The type of DimensionBroker used by the view
 * @param <D> The type of data returned by the Dimensions handled by the view
 */
public interface ReconfigurableView<M extends RedefinableDimensionsModel<T,B,D>,T,B extends DimensionBroker<T,D>,D> extends DataView<M,T>, BrokerChangedListener<T,B,D>{

	/**
	 * @return The id of the query for which the dimensions are currently redefinable
	 */
	public String getQueryUnderFocus();
	
	/**
	 * @return The number of axes in the view which correspond to a dimension in the underlying model
	 */
	public int numDimensionalAxes();
	
	public String axisName(int index);
	
	/**
	 * @param index
	 * @return The {@link Dimension} currently displayed in the view for the axis at index.
	 */
	public Dimension<T, ?> currentAxisDimension(int index);
	
}

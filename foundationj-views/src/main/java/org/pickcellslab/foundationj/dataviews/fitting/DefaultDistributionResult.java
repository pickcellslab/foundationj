package org.pickcellslab.foundationj.dataviews.fitting;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

public class DefaultDistributionResult<T extends Distribution<C>,C> extends DefaultFitResult<T> implements DistributionFitResult<T, C> {

	public DefaultDistributionResult(T fit, String description, String id, Dimension<?,?> dimensions) {
		super(fit, description, id, dimensions);
	}
	
	public DefaultDistributionResult(T fit, String description, String id, Dimension<?,?>[] dimensions) {
		super(fit, description, id, dimensions);
	}

	

}

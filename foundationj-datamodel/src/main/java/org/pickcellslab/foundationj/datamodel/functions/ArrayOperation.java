package org.pickcellslab.foundationj.datamodel.functions;

public enum ArrayOperation implements DoubleToDoubleBiFunction{
	
	ADD{

		@Override
		public double apply(double a, double b) {
			return a+b;
		}

	},
	SUBTRACT{

		@Override
		public double apply(double a, double b) {
			return a-b;
		}

	},
	MULTIPLY{

		@Override
		public double apply(double a, double b) {
			return a*b;
		}

	},
	DIVIDE{

		@Override
		public double apply(double a, double b) {
			if(b!=0)
				return a/b;
			else
				return Double.NaN;
		}

	}

}
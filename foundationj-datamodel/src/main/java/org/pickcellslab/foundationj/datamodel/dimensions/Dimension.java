package org.pickcellslab.foundationj.datamodel.dimensions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;

/**
 * Represents the concept of a dimension in a multidimensional dataset. Here a {@link Dimension} also implements 
 * {@link ExplicitFunction} allowing a {@link Dimension} object to be used to obtain the corresponding value from an input
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of the input supported by this {@link Dimension}
 * @param <E> The type of the return value.
 */
public interface Dimension<T,E> extends DataTyped, ExplicitFunction<T,E>{

	/**
	 * A Dimension allows to obtain either a single value, an entry in an array or a full array from a {@link WritableDataItem}. 
	 * The index will be -1 if the Dimension represent either a single value or an entire array, and will be the index in
	 * the array where the entry is located if this dimension points to an entry within an array.
	 * @return the index of this Dimension
	 */
	public abstract int index();

	/**
	 * @return The length of the array or -1 if the returned value is not an array. 
	 */
	public abstract int length();

	/**
	 * @return The name of this {@link Dimension}
	 */
	public abstract String name();

	/**
	 * @return Some information about this {@link Dimension}
	 */
	public abstract String info();

	@Override
	public default String description(){
		return info();
	}

	/**
	 * @return An HTML formatted description of this {@link Dimension}
	 */
	public default String toHTML(){
		return "<HTML> Dimension : \"" + name() + "\""
				+ "<br> Type [" + dataType() + "]"
				+ " <br> Description : "+ info()
				+ " <br> Index : "+index()
				+ "</HTML>";
	}


	public default AKey<?> createKey(){
		final String name = name();
		if(name==null)
			throw new RuntimeException(getClass().getSimpleName()+ "returns a null name");
		final Class<?> type = getReturnType();
		if(type==null)
			throw new RuntimeException(getClass().getSimpleName()+ "returns a null return type");
		return AKey.get(name, type);
	}



	public static <E> Dimension<DataItem, E> create(AKey<E> k, E prototype){
		if(prototype.getClass().isArray()) {
			return new KeyArrayDimension<>(k, Array.getLength(prototype), k.name);
		}
		else
			return new KeyDimension<>(k, k.name);		
	}



	public static <E> List<Dimension<DataItem, ?>> createDecomposed(AKey<E> k, E prototype){
		assert k != null : "key is null";
		assert prototype != null : "prototype is null";
		final List<Dimension<DataItem, ?>> list = new ArrayList<>();
		if(prototype.getClass().isArray()){
			final int length = Array.getLength(prototype);
			for(int i = 0; i<length; i++){
				list.add(new KeyDimension(k, i, k.name+" "+i));
			}
		}
		else{
			list.add(create(k, prototype));
		}
		return list;			
	}

}

package org.pickcellslab.foundationj.ui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.Comparator;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.renderers.DimensionRenderer;
import org.pickcellslab.foundationj.ui.wizard.PluggableSelection;

import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.GroupingType;

@SuppressWarnings("serial")
public class MultiDimensionChoice extends JPanel{//TODO change to ValidityTogglePanel

	private final PluggableSelection<Dimension<?,?>> choiceDims;
	@SuppressWarnings("unchecked")
	private final Comparator<Dimension<?,?>>[] comp = new Comparator[1];


	public <T,E> MultiDimensionChoice(List<Dimension<T,E>> dims, UITheme theme) { 

		comp[0] = new Comparator<Dimension<?,?>>(){

			@Override
			public int compare(Dimension<?, ?> o1, Dimension<?, ?> o2) {
				return o1.name().compareTo(o2.name());
			}

			@Override
			public String toString(){
				return "Natural Order";
			}
		};


		// Available Dimensions panel 
		final PluggableSelection<Dimension<?,?>> avDims = new PluggableSelection<>("Available", comp);
		avDims.setRenderer(new DimensionRenderer(theme));
		for(Dimension<?,?> d : dims){
			avDims.add(d);
		}

		// Chosen Dimensions panel 
		choiceDims = new PluggableSelection<>("Dimensions to export", comp);
		choiceDims.setRenderer(new DimensionRenderer(theme));

		//Button Panel
		final JButton addBtn = new JButton("Add");
		addBtn.addActionListener(l->avDims.getSelectedItems().forEach(sel->{
			choiceDims.add(sel);
			avDims.remove(sel);
		}));
		final JButton removeBtn = new JButton("Remove");
		removeBtn.addActionListener(l->choiceDims.getSelectedItems().forEach(sel->{
			choiceDims.remove(sel);
			avDims.add(sel);
		}));


		// Layout
		this.setLayout(new BorderLayout());
		this.add(new GroupPanel(10, avDims, 
				new GroupPanel(GroupingType.fillFirstAndLast, 10, false, Box.createVerticalGlue(), addBtn, removeBtn, Box.createVerticalGlue()),
				choiceDims), BorderLayout.CENTER);

	}



	public <T extends DataItem, E> List<Dimension<T,E>> get() {
		return (List)choiceDims.getAllItems();
	}




}

package org.pickcellslab.foundationj.dbm.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.MutableDouble;
import org.pickcellslab.foundationj.dbm.FjIdentityFactory;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest;
import org.pickcellslab.foundationj.dbm.db.DataStoreConnector;
import org.pickcellslab.foundationj.dbm.db.DataStoreManager;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.MarkedState;
import org.pickcellslab.foundationj.dbm.db.QInterpreter;
import org.pickcellslab.foundationj.dbm.events.ConnexionFailedException;
import org.pickcellslab.foundationj.dbm.events.MetaListenerNotifier;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.StorageBox;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class TestReadQueries {


	private DataAccess access;
	private DatabaseAccess<NodeItem,Link> db;


	@SuppressWarnings("unchecked")
	@BeforeAll
	public void setup() throws ConnexionFailedException {

		// Here we build a Session with necessary mocked objects within it

		// The object which returns Nodes and Links is the DatabaseAccess
		// We keep it as a field to allow each test to stub the methods appropriately
		db = Mockito.mock(DatabaseAccess.class);
		// 2 object needs to be provided : 
		// The FjAdapterFactory which is used normally to adapt objects from the backend to foundationj interfaces
		Mockito.when(db.createFactory()).thenReturn(new FjIdentityFactory());
		// And a MarkedState
		Mockito.when(db.newThread()).thenReturn(Mockito.mock(MarkedState.class));

		// The DatastoreManager is a factory for QInterpreter so we also need to create 2 of those
		// One for the MetaModel
		QInterpreter<MetaBox> mockedMetaInterpreter = Mockito.mock(QInterpreter.class);
		//And one for the actual Data
		QInterpreter<StorageBox> mockedStorageInterpreter = Mockito.mock(QInterpreter.class);
		// NB: As we only perform read queries here, we do not need to stub method calls for those.



		// Now we can create the DataStoreManager
		DataStoreManager<NodeItem,Link> mockedMgr = Mockito.mock(DataStoreManager.class);
		// Create a DataStoreConnector
		DataStoreConnector<NodeItem,Link> connector = Mockito.mock(DataStoreConnector.class);
		
		// Create A DataRegistry
		DataRegistry registry = Mockito.mock(DataRegistry.class);
		Mockito.when(registry.getHook(MetaInstanceDirector.class)).thenReturn(new MetaInstanceDirector(registry));
		
		// Create A ConnexionRequest
		ConnexionRequest request = Mockito.mock(ConnexionRequest.class);
		
		Mockito.when(connector.connect(request, registry)).thenReturn(mockedMgr);
		
		// And stub methods which will be checked in the constructor of Session and used during queries processing
		Mockito.when(mockedMgr.createMetaStorer(Mockito.any(MetaListenerNotifier.class))).thenReturn(mockedMetaInterpreter);
		Mockito.when(mockedMgr.createDataStorer(Mockito.any(MetaListenerNotifier.class))).thenReturn(mockedStorageInterpreter);
		Mockito.when(mockedMgr.newAccess()).thenReturn(db);


		// Now we can create the Session which should be properly configured
		access = new Session<>(connector, request, registry);

	}




	@Test
	public void testSimpleList() {


		// ---------------------------------------------------------//
		// Setup the answer from the database: 
		// An Iterator of NodeItem holding incrementing values
		// for a requested attribute
		// ---------------------------------------------------------//

		// The attribute to be read
		final AKey<Double> key = AKey.get("Test Key", double.class);

		// Create a mock NodeItem with a counter answer
		MutableDouble counter = new MutableDouble();
		NodeItem mockedNode = Mockito.mock(NodeItem.class);
		Mockito.when(mockedNode.getAttribute(key)).then((a)->{counter.increment(); return Optional.of(counter.doubleValue());});

		// Put this instance in a list
		List<NodeItem> input = new ArrayList<>();
		for(int i = 0; i<5; i++)
			input.add(mockedNode);

		// Now stub the relevant db method
		final String lbl = "Label";
		Mockito.when(db.getNodes(lbl)).thenAnswer((itr)->input.iterator()); // create a new iterator each time


		// ---------------------------------------------------------//
		// Perform the test without Grouping
		// ---------------------------------------------------------//
		try {
			List<Double> result1 = 
					access.queryFactory().read(lbl).makeList(key).inOneSet().getAll().run();

			Assertions.assertTrue(result1.size() == 5);
			MutableDouble check = new MutableDouble();
			for(int i = 0; i<5; i++) {
				check.increment();
				Assertions.assertTrue(result1.get(i) == check.doubleValue());
			}

		} catch (IllegalArgumentException | DataAccessException e) {			
			e.printStackTrace();
			Assertions.fail(e.getMessage());
		}


		// ---------------------------------------------------------//
		// Perform the test With a Split in 2 categories
		// ---------------------------------------------------------//

		// We should not forget to reset counter
		counter.set(0);

		try {
			OrganisedResult<Boolean, List<Double>> result2 = 
					access.queryFactory().read(lbl).makeList(key).inGroups().getAll().as2Groups(P.keyTest(key, Op.Logical.SUP_EQ, 3)).run();

			// We should have 2 categories (True and False)
			Assertions.assertTrue(result2.size() == 2);
			// The number of accepted
			Assertions.assertTrue(result2.get(true).size() == 3);
			// The number of rejected
			Assertions.assertTrue(result2.get(false).size() == 2);

		} catch (IllegalArgumentException | DataAccessException e) {			
			e.printStackTrace();
			Assertions.fail(e.getMessage()); 
		}



		// ---------------------------------------------------------//
		// Perform the test With a Split by key values
		// ---------------------------------------------------------//

		// We should not forget to reset counter
		counter.set(0);

		try {
			OrganisedResult<String, List<Double>> result3 = 
					access.queryFactory().read(lbl).makeList(key).inGroups().getAll().groupBy(F.select(key, Double.NaN).next(F.asString())).run();


			Assertions.assertTrue(result3.size() == 5);
			MutableDouble check = new MutableDouble(); 

			for(String entry : result3.entries()) {
				check.increment();
				Assertions.assertTrue(entry.equals(check.doubleValue()+""));
			}

		} catch (IllegalArgumentException | DataAccessException e) {			
			e.printStackTrace();
			Assertions.fail(e.getMessage());
		}


	}












}

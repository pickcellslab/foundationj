package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.util.Pair;

/**
 * Provides a cache for singleton {@link Pair} instances
 * 
 * @author Guillaume Blin
 *
 * @param <K>
 * @param <V>
 */
final class Pairs<K,V> {

	private final Map<K,Map<V,Pair<K,V>>> instances = new HashMap<>();

	public Pair<K,V> get(K k, V v){
		
		Pair<K,V> p = null;
		
		Map<V, Pair<K, V>> map = instances.get(k);
		if(null == map){
			p = new Pair<>(k,v);
			map = new HashMap<>();
			map.put(v, p);
		}else{
			p = map.get(v);
			if(null == p){
				p = new Pair<>(k,v);
				map = new HashMap<>();
				map.put(v, p);
			}
		}
		
		return p;
		
	}

}

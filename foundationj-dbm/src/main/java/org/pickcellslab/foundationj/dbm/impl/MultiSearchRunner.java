package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

class MultiSearchRunner<K,O,T> implements QueryRunner<OrganisedResult<K,O>>{

	private final MultiSearchDefinition<?, ?, T, K> definition;
	private final Action<? super T, O> action;
	private final TargetType<?, ?, T> target;

	MultiSearchRunner(TargetType<?,?,T> target, MultiSearchDefinition<?,?,T,K> definition, Action<? super T,O> action) {
		this.definition = definition;
		this.action = action;
		this.target = target;
	}
	
	

	@Override
	public OrganisedResult<K, O> run() throws DataAccessException {
		return target.session().runQuery(new MultiSearchQuery<>(definition, action));
	}

}

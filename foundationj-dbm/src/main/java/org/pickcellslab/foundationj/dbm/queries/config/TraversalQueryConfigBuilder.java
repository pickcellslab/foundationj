package org.pickcellslab.foundationj.dbm.queries.config;

import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;

public interface TraversalQueryConfigBuilder<T,D> extends TargetedQueryConfigBuilder<T,D> {


	public TraversalQueryConfigBuilder<T,D> setSynchronizeRoots(boolean synchronizeRoots);


	public TraversalQueryConfigBuilder<T,D> setSynchronizeTarget(boolean b);

	
	public TraversalQueryConfigBuilder<T,D> disablePathSPlitting();
	
	
	/**
	 * If set only the specified type will be available as a root for traversals
	 * @param fixedRootType null not permitted
	 * @return The update builder
	 */
	public TargetedQueryConfigBuilder<T,D> setFixedRootType(MetaClass fixedRootType);
	
	
	/**
	 * Sets the root of the traversal query to the specified {@link MetaReadable#owner()} type and limits the choice to instances that possess a
	 * given MetaReadable/Value.  
	 * @param fixedRootKey The MetaReadable to use for filtering root instances
	 * @param value the value that should be returned by instances that can be selected as root. If null, the user will have to choose among 
	 * the values available in the database for the given MetaReadable to determine 	 * the root (instances) of the traversal. For example,
	 * one may choose the name of Images as a MetaReadable: The user will have to choose one image based on its name and all the targets they
	 * may define will be reachable from this chosen image.
	 * @return The updated builder.
	 */
	public <E> TargetedQueryConfigBuilder<T,D> setFixedRootKey(MetaReadable<E> fixedRootKey, E value);
	
	
}

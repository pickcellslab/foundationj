package org.pickcellslab.foundationj.dbm.db;

import org.pickcellslab.foundationj.datamodel.AKey;

public interface UpdateInfo {

	boolean isAuthorized(String declaredType, AKey<?> k);

}
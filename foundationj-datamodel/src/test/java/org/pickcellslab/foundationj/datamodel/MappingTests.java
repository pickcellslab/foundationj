package org.pickcellslab.foundationj.datamodel;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

public class MappingTests {

	
	@Test
	public void testOperators() {
		assertTrue(MappingUtils.canBeMapped(Op.Bool.AND));
		assertTrue(MappingUtils.canBeMapped(Op.ElementOp.UNIQUE));
		assertTrue(MappingUtils.canBeMapped(Op.SetOp.ANY_OF));
		assertTrue(MappingUtils.canBeMapped(Op.Logical.SUP));
		assertTrue(MappingUtils.canBeMapped(Op.TextOp.CONTAINS));
	}
	
	
	@Test
	public void testPredicate() {		
		assertTrue(MappingUtils.canBeMapped(P.equalsTo(2d)));
		assertTrue(MappingUtils.canBeMapped(P.hasAny(new Integer[] {})));
		assertTrue(MappingUtils.canBeMapped(P.hasElement("Element")));
		assertTrue(MappingUtils.canBeMapped(P.hasKey(AKey.get("key", double.class))));
	}
	
	
}

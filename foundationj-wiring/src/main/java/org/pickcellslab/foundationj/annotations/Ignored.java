/**
 * 
 */
package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * 
 * Specifies that a field of a class annotated with {@link Mapping} does not need to be stored in the database.
 * 
 * @author Guillaume Blin
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Ignored {

}

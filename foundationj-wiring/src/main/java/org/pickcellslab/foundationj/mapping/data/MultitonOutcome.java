package org.pickcellslab.foundationj.mapping.data;

/**
 * 
 * An immutable container object for an instance which was either newly created 
 * or retrieved from cache.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The Type of object held by this MultitonOutcome instance
 */
public final class MultitonOutcome<T> {
	
	private final T instance;
	private final boolean wasCreated;

	public MultitonOutcome(T instance, boolean wasCreated){
		this.instance = instance;
		this.wasCreated = wasCreated;
	}
	
	/**
	 * @return The actual instance
	 */
	public T getInstance() {
		return instance;
	}

	/**
	 * @return {@code true} if the instance was created, {@code false} otherwise
	 */
	public boolean wasCreated() {
		return wasCreated;
	}
	
}

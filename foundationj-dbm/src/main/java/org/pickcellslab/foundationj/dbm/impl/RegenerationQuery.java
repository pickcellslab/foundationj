package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.queries.MonolithicResult;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A very special {@link SearchSeveralQueryImpl} which regenerate java objects from the database
 * 
 * @author Guillaume Blin
 * 
 */
class RegenerationQuery<V,E> extends SearchSeveralQueryImpl<V,E,V, String, RegeneratedItems, MonolithicResult<RegeneratedItems>>{

	
	private static Logger log = LoggerFactory.getLogger(RegenerationQuery.class);
	
	private static final String KEY = "Key";
	
	private MonolithicSearchDefinition<V, E, V> definition;

	protected RegenerationQuery(MonolithicSearchDefinition<V,E,V> search, ReconstructionAction<V,E> action) {
		super(action);
		this.definition = search;
	}

	@Override
	public String description() {
		return searchDescription()+" -> "+actionDescription();
	}

	@Override
	public String name() {
		//TODO 
		 throw new RuntimeException("Not implemented yet");
	}

	@Override
	public String shortDescription() {
		//TODO 
		 throw new RuntimeException("Not implemented yet");
	}

	@Override
	public String searchDescription() {
		return definition.description();
	}

	@Override
	public String actionDescription() {
		return action.description();
	}

	@Override
	public MonolithicResult<RegeneratedItems> createResult(DatabaseAccess<V, E> db) throws DataAccessException {
		
		Objects.requireNonNull(db, "The provided DatabaseAccess is null");
		
		long start = System.currentTimeMillis();

		//Initialise the ActionMechanism
		ReconstructionMechanism<V,E> m = (ReconstructionMechanism<V,E>) action.createCoordinate(KEY);

		m.setAccess(db);
		
		Iterator<V> it = definition.accessor().iterator(db);
		while(it.hasNext()){
			m.performAction(it.next());
		}		
		
		long endIt = System.currentTimeMillis();
		log.debug("Finding objects took "+(endIt-start)+" ms");
		
		
		m.lastAction();
		
		log.debug("Regeneration took "+(System.currentTimeMillis()-endIt)+" ms");
		
		
		//We only have one action (definition of Monolithic Search) so return now
		return new MonolithicResult<>(action.createOutput(KEY));		
		
		
	}





}


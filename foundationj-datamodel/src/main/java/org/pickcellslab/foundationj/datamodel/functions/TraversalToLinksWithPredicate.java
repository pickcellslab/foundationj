package org.pickcellslab.foundationj.datamodel.functions;

import java.util.Collections;
import java.util.Iterator;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.FilteredIterator;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "FilteredLinksFromTraversal")
public class TraversalToLinksWithPredicate implements ExplicitFunction<Traversal<NodeItem,Link>, Iterator<? extends DataItem>> {

	@Supported
	private final ExplicitPredicate<? super Link> predicate;
	
	public TraversalToLinksWithPredicate(ExplicitPredicate<? super Link> predicate) {
		MappingUtils.exceptionIfNullOrInvalid(predicate);
		this.predicate = predicate;
	}
	
	@Override
	public Iterator<Link> apply(Traversal<NodeItem, Link> t) {
		return t==null ? Collections.emptyIterator() : new FilteredIterator<>(t.edges().iterator(), predicate);
	}

	@Override
	public String description() {
		return predicate.description() + " links";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class<Iterator<? extends DataItem>> getReturnType() {
		return (Class)Iterator.class;
	}

}

package org.pickcellslab.foundationj.scope;

import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;

/**
 * 
 * Represents a runtime version of a {@link Scope}. Such an object has an additional id which will be unique per instance given a 
 * scope name.
 * <br><br>
 * To obtain references to {@link RuntimeScope}, one needs to extends {@link AppController}
 * <br><br> Do not implement yourself, instances are provided by the framework. 
 * 
 * @author Guillaume Blin
 *
 */
public interface RuntimeScope {

	/**
	 * Same as the {@link Scope#name()} that this {@link RuntimeScope} represents
	 * @return The name of this {@link RuntimeScope}
	 */
	public String name();

	/**
	 * A unique id for this {@link RuntimeScope}.
	 * @return The id of this {@link RuntimeScope}
	 */
	public String id();

	/**
	 * @return The parent.
	 */
	public RuntimeScope parent();
	
	/**
	 * @param name Same as {@link Scope#name()}
	 * @return The number of live {@link RuntimeScope} with the given name
	 */
	public int numberOfChildren(String name);

	/**
	 * @return The {@link ScopePolicy} of this {@link RuntimeScope}
	 */
	public ScopePolicy policy();
	
	/**
	 * Provides an instance for a desired type residing within this RuntimeScope
	 * <br> NB: Only root application controller objects should use this method. Instances should otherwise
	 * be obtained via constructor injection.
	 * @param clazz 
	 * @return An instance for the desired clazz
	 */
	public <T> T getComponentInstance(Class<T> clazz);

}
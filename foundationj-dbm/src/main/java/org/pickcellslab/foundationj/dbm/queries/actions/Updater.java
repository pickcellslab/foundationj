package org.pickcellslab.foundationj.dbm.queries.actions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.QueryFactory;
import org.pickcellslab.foundationj.dbm.db.Updatable;

/**
 * An {@link ActionMechanism} intended to be used during an update operation.
 * An {@link Updater} must explicitely define which properties will be modified or created via {@link #updateIntentions()}.
 * Any attempt to modify or create undefined AKey will result in a {@link DataAccessException}.
 * Do not delete properties from an update either, deletions are performed via the delete methods in {@link QueryFactory}
 * 
 * @author Guillaume Blin
 *
 */
public interface Updater extends VoidMechanism<Updatable>{
	
	/**
	 * @return A List of {@link AKeyInfo} which define the roperties which will be either modified or created.
	 */
	public List<AKeyInfo> updateIntentions();
	
	
}

package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;

class ToStringOrganisedResult<O> implements ResultAccess<String, O> {

	private OrganisedResult<Pair<?, ?>, O> origin;

	ToStringOrganisedResult(OrganisedResult<Pair<?,?>,O> origin) {
		this.origin = origin;
	}

	@Override
	public int size() {
		return origin.size();
	}

	@Override
	public String getKey(int i) {
		return origin.getKey(i).toString();
	}

	@Override
	public O getResult(int i) {
		return origin.getResult(i);
	}

	@Override
	public O getResult(String k) {
		
		for(Pair<?,?> p : origin.entries())
				if(p.toString().equals(k))
					return origin.get(p);
		
		return null;
	}

}

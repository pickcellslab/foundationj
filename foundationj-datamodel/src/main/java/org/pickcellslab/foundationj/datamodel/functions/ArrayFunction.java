package org.pickcellslab.foundationj.datamodel.functions;

import java.lang.reflect.Array;

import org.pickcellslab.foundationj.annotations.Mapping;

@Mapping(id = "ArrayFunction")
public class ArrayFunction implements ExplicitCombiner<Object,Object,double[]> {
	
	private final ArrayOperation operation;


	public ArrayFunction(ArrayOperation operation) {	
		this.operation = operation;
	}

	@Override
	public double[] combine(Object input1, Object input2) {
		if(input1==null || input2==null)
		return null;
		if(Array.getLength(input1)!=Array.getLength(input2))
			return null;
		double[] result = new double[Array.getLength(input1)];
		for (int i = 0; i < Array.getLength(input1); i++) {
			result[i] = operation.apply(Array.getDouble(input1,i), Array.getDouble(input2,i));
		}
		return result;
	}


	@Override
	public String description() {
		return operation.name();
	}

	@Override
	public Class<double[]> getReturnType() {
		return double[].class;
	}




}

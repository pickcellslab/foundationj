package org.pickcellslab.foundationj.dbm.queries.actions;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.reductions.CountOperation;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.dbm.queries.DataCollectorBuilder;
import org.pickcellslab.foundationj.dbm.queries.ReadTable;

/**
 * A Factory class to create {@link Action}s to be used in read queries.
 * 
 * @author Guillaume Blin
 *
 */
public final class Actions {

	private Actions(){/*Do not instantiate*/}

	
	
	/**
	 * Computes {@link SummaryStatistics} for a given {@link Dimension}
	 * @param dim The {@link DImension} on which to compute statistics
	 * @return The {@link Action} which will compute the summary statistics.
	 */
	public static <T extends DataItem, N extends Number> Action<T,SummaryStatistics> summaryStats(Dimension<T,N> dim){
		return new SimpleStatsAction<>(dim);
	}



	/**
	 * Generates a {@link Set} for a given {@link AKey} on queried objects
	 * @param key The {@link AKey} for the desired property.
	 * @param defaultValue A default value if a queried object does not possess the given property
	 * @return An {@link Action} which will create a {@link Set} holding al
	 */
	public static <T extends DataItem,D> Action<T,Set<D>> asSet(AKey<D> key, D defaultValue){
		return new SetAction<>(F.select(key, defaultValue));
	}



	/**
	 * @return An {@link Action} which will create a {@link ReadTable} which includes
	 * all properties of the queried objects
	 */
	public static <T extends DataItem> Action<T,ReadTable> fullTable(){
		return new TableMaker<>(new DataCollectorBuilder<T>().getAll());
	}





	/**
	 * Creates an {@link Action} which will generate a table where columns are defined by provided {@link Dimension}s.
	 * The table will have as many rows as there are queried objects
	 * @param dimensions A List of {@link Dimension} to include as column of the table
	 * @return An {@link Action} which will create a {@link ReadTable} as described above
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T extends DataItem> Action<T,ReadTable> table(List<Dimension<T, ?>> dimensions) {		
		DataCollectorBuilder<T> b = new DataCollectorBuilder<>();
		for(Dimension<T,?> d : dimensions)
			b.compute((Dimension)d, d.createKey());		
		return new TableMaker<>(b.build());
	}

	
	/**
	 * @param op The {@link ExplicitReduction} to apply
	 * @return An {@link Action} which will apply the given {@link ExplicitReduction} on queried objects
	 */
	public static <T,O> Action<T,O> reduction(ExplicitReduction<T,O> op){
		Objects.requireNonNull(op);
		return new ReductionAction<>(new ReductionMechanism<>(op));
	}
	
	
	/**
	 * @return An {@link Action} which will count the number of encountered objects
	 */
	public static <T> Action<T,Long> count(){
		return new ReductionAction<>(new ReductionMechanism<>(new CountOperation<>()));
	}
	
	
	
	
	
	

}

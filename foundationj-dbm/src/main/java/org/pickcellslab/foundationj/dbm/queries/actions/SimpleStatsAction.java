package org.pickcellslab.foundationj.dbm.queries.actions;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;

/**
 * An {@link Action} allowing to create {@link SummaryStatistics} for a given {@link Dimension}
 * @author Guillaume Blin
 *
 * @param <N>
 */
public class SimpleStatsAction<T extends DataItem, N extends Number> extends AbstractAction<T,SummaryStatistics>{

	private final Dimension<T,N> dim;

	public SimpleStatsAction(Dimension<T,N> dim) {
		super("Compute stats on "+dim);
		this.dim = dim;
	}




	@Override
	protected ActionMechanism<T, SummaryStatistics> create(String key) {
		return new StatsMecha(new SummaryStatistics());
	}




	private class StatsMecha implements ActionMechanism<T,SummaryStatistics>{

		private final SummaryStatistics s;

		public StatsMecha(SummaryStatistics s) {
			this.s = s;
		}

		@Override
		public void performAction(T i) throws DataAccessException {
			final Number n = dim.apply(i);
			if(n!=null)
				s.addValue(n.doubleValue());
		}

		@Override
		public SummaryStatistics get() {
			return s;
		}

	}


}
package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


public class Parcel {

	private static double adjustment = 0.2;
	
	private final String ware;
	private final int capacity;
	private final double price;
	private int currentQuantity;
	private double budget;
	
	public Parcel(String ware, double price, int capacity, int quantity, double budget){
		
		if(capacity<quantity)
			throw new IllegalArgumentException("The capacity cannot be lower than the assigned quantity");
		
		this.ware = ware;
		this.capacity = capacity;
		this.price = price;
		currentQuantity = quantity;
		this.budget = budget;
	}
	
	
	public String ware(){
		return ware;
	}
	
	public int capacity(){
		return capacity;
	} 
	
	public int currentQuantity(){
		return currentQuantity;
	}
	
	public int remainingCapacity(){
		return capacity-currentQuantity;
	}
	
	public double currentCost(){
		return ((double)currentQuantity)*getUnitPrice();
	}
	
	public double budget(){
		return budget;
	}
	
	
	public void transferTo(Parcel other){		
		if(!ware.equals(other.ware))
			return;
				
		//Possibilities:	
		//Budget of other is too low
		//Remaining capacity of other is too low
		//currentQuantity is lower than other capacity
		
		if(currentQuantity <= other.remainingCapacity()){
			if(currentCost() <= other.budget){
				//make transfer of everything
				makeTransfer(other, currentQuantity, currentCost());
				System.out.println("T1");
			}else{
				//make transfer according to other budget
				int quantity = (int) Math.floor(other.budget/getUnitPrice());
				makeTransfer(other, quantity, ((double)quantity)*getUnitPrice());
				System.out.println("T2");
			}
		}else{
			if(other.remainingCapacity()*getUnitPrice() <= other.budget){
				//make transfer according to other remaining capacity
				makeTransfer(other,other.remainingCapacity(),((double) other.remainingCapacity())*getUnitPrice());
				System.out.println("T3");
			}else{
				//make transfer according to other budget
				int quantity = (int) Math.floor(other.budget/getUnitPrice());
				makeTransfer(other, quantity, quantity*getUnitPrice());
				System.out.println("T4");
			}			
		}	
	}
	
	
	public Double virtualTransferTo(Parcel other){
		
		if(!ware.equals(other.ware))
			return null;
				
		//Possibilities:	
		//Budget of other is too low
		//Remaining capacity of other is too low
		//currentQuantity is lower than other capacity
		
		if(currentQuantity <= other.remainingCapacity()){
			if(currentCost() <= other.budget){
				//make transfer of everything
				return currentCost();
			}else{
				//make transfer according to other budget
				int quantity = (int) Math.floor(other.budget/getUnitPrice());
				return ((double)quantity)*getUnitPrice();
			}
		}else{
			if(other.remainingCapacity()*getUnitPrice() <= other.budget){
				//make transfer according to other remaining capacity
				return ((double)other.remainingCapacity())*getUnitPrice();
			}else{
				//make transfer according to other budget
				int quantity = (int) Math.floor(other.budget/getUnitPrice());
				return ((double)quantity)*getUnitPrice();
			}			
		}
	}
	
	private void makeTransfer(Parcel other, int quantity, double price){
		currentQuantity -= quantity;
		budget += price;
		other.currentQuantity += quantity;
		other.budget -= price;
	}
	
	private double getUnitPrice(){
		return (price/2)+(price*(double)remainingCapacity())*adjustment/(double)capacity;		
	}
	
	
	
	
}

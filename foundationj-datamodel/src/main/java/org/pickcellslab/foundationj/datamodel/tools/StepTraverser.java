package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * 
 * A {@link StepTraverser} is a {@link Traverser} which allows to traverse the graph in a stepwise manner.<br><br>
 * For example, a {@link StepTraverser} implementation may use a <a href="https://en.wikipedia.org/wiki/Breadth-first_search">
 * breadth-first search</a> method to traverse the graph and provide a {@link TraversalStep} at each depth via the {@link #nextStep()}
 * method.
 * 
 * @author Guillaume Blin
 *
 * @param <N> The type of Nodes handled by this Traverser
 * @param <E> The type of Edges handled by this Traverser
 */
public interface StepTraverser<N,E> extends Traverser<N, E> {

	/**
	 * Performs a step in the traversal process and returns a {@link TraversalStep} object.
	 * The whole Traversal can be completed as follows:
	 * <br><br>
	 * while((TraversalStep step = nextStep()) != null){<br>
	 *   ... do whatever with step;<br>
	 * }
	 * <br><br>
	 * 
	 * <b>NB :</b> Subsequent calls to {@link #traverse()} will resume from the last step obtained via
	 * this method and will return an empty {@link TraversalStep} if the full traversal was performed.
	 *
	 */
	public TraversalStep<N,E> nextStep();
	
	/**
	 * Performs the full traversal defined by this {@link StepTraverser}.
	 * <br><br>
	 * <b>NB: </b> If previous calls to {@link #nextStep()} were performed, the resulting {@link Traversal}
	 * will only contain the nodes and edges encountered while resuming the traversal from the last step visited.
	 */
	@Override
	public Traversal<N,E> traverse();
	
}

package org.pickcellslab.foundationj.optim.csp;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;


/**
 * A {@link Validator} is an object which holds the constraints of a problem. It is used by
 * {@link Solver} in a {@link GraphOptimization} 
 * 
 * @author Guillaume Blin
 *
 * @param <N>
 * @param <E>
 */
public interface Validator
<N extends Node<E>, E extends Edge<? extends N>>{
	
	/**
	 * Evaluates the specified coordinates to determine are satisfied locally. 
	 * @param source The source coordinate
	 * @param neighbours The selected solvers coordinate
	 * @return true if the specified coordinates are valid, {@code false} otherwise.
	 */
	boolean isValid(Solver<N,E> source, Collection<Solver<N,E>> neighbours);
	
	/**
	 * Identifies whether or not the specified neighbour may potentially be part of the solution in the future
	 * @param source The source coordinate
	 * @param neighbours The selected solvers coordinate
	 * @return true if the specified coordinates are valid, {@code false} otherwise.
	 */
	boolean potential(Solver<N,E> source, Solver<N,E> neighbour);	
	

}

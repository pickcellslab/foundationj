package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * An {@link Agent} is an object which has some specific capabilities and which declares them explicitly.
 * The methods to obtain the output of the capabilities are not defined in this interface as outputs may
 * arise as side effects.
 * 
 * @author Guillaume Blin
 *
 * @param <D> The definition of a capability
 */
public interface Agent<D> {
	
	/**
	 * @return The capabilities of this {@link Agent}
	 */
	public D capabilities();
	
	
}

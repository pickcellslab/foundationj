package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.BiFunction;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;

/** 
 * A step in a stepwise builder pattern to define the 'rules' for navigating a graph. This step defines how links should 
 * be traversed.
 *
 * @param <T> The type of the next builder
 */
public interface LinkDecisions<T> {

	/**
	 * Specifies that all links should be traversed
	 * @return The next step in the building process
	 */
	public EvaluationContainerInit<T> traverseAllLinks();
	
	/**
	 * Specifies that only the links accepted by the given filter should be traversed
	 * @param biPredicate A {@link BiFunction} testing if a link given a {@link Direction}
	 * should be traversed {@code true} and {@code false} otherwise.
	 * @return The next step in the building process
	 */
	public EvaluationContainerInit<T> acceptLinks(BiFunction<Link, Direction, Boolean> biPredicate);

	
	/**
	 * Specifies that only the links with the given {@link Direction} should be traversed
	 * @param dir The Direction to allow
	 * @return The next step in the building process
	 */
	public default EvaluationContainerInit<T> traverseOnly(Direction dir) {
		return acceptLinks((l,d)->d.equals(dir));
	}
	
	/**
	 * Specifies that Links should not be traversed unless they are of the defined type and {@link Direction}<br>
	 * <b>NB:</b> The next building step will allow to add more link types and direction if desired.
	 * @param type The type of links to be traversed
	 * @param d The {@link Direction} allowed to be traversed
	 * @return The next step in the building process (which will allow to include other links if desired)
	 */
	public IncludeLinksBased<T> traverseLink(String type, Direction d);

	/**
	 * Specifies that Links should all be traversed unless they are of the defined type and {@link Direction}<br>
	 * <b>NB:</b> The next building step will allow to exclude more link types and direction if desired.
	 * @param type The type of links to be excluded from the traversal
	 * @param d The {@link Direction} which is not allowed
	 * @return The next step in the building process
	 */
	public ExcludeLinksBased<T> doNotTraverseLink(String type, Direction d);
	
}

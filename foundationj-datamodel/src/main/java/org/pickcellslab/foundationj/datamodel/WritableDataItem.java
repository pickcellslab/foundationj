package org.pickcellslab.foundationj.datamodel;

/**
 *
 *A {@link DataItem} which can have its attributes modified
 *
 */
public interface WritableDataItem extends DataItem{

	/**
	 * Set the value for the given {@link AKey}
	 * 
	 * @param key
	 * @param v The value to store. Cannot be {@code null}
	 */
	public <T> void setAttribute( AKey<T> key,  T v);

	
	/**
	 * Remove the attribute mapped to the specified {@link AKey} object
	 * 
	 * @param key
	 */
	public void removeAttribute(AKey<?> key);




}

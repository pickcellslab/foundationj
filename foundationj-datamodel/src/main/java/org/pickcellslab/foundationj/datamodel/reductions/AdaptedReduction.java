package org.pickcellslab.foundationj.datamodel.reductions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="AdaptedReduction")
public class AdaptedReduction<I,T,R> implements ExplicitReduction<I,R>{

	@Supported
	private final ExplicitReduction<? super T,R> reduction;
	@Supported
	private final ExplicitFunction<I,T> function;
	
	public AdaptedReduction(ExplicitFunction<I,T> function, ExplicitReduction<? super T,R> reduction) {
		
		MappingUtils.exceptionIfNullOrInvalid(reduction);
		MappingUtils.exceptionIfNullOrInvalid(function);
		
		this.reduction = reduction;
		this.function = function;
		
	}
	
	
	@Override
	public ExplicitReduction<I, R> factor() {
		return new AdaptedReduction<>(function, reduction.factor());
	}

	@Override
	public void root(I root, boolean included) {
		reduction.root(function.apply(root), included);
	}

	@Override
	public void include(I t) {
		reduction.include(function.apply(t));
	}

	@Override
	public R getResult() {
		return reduction.getResult();
	}

	@Override
	public Class<R> getReturnType() {
		return reduction.getReturnType();
	}

	@Override
	public String description() {
		return reduction.description()+" "+function.description();
	}

}

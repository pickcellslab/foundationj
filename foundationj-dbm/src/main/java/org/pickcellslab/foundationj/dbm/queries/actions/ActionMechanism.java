package org.pickcellslab.foundationj.dbm.queries.actions;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;

/**
 * Implementing objects are objects which perform operations on database objects during a query.
 * They work as consumers of database objects which are provided to the mechanism in a sequential manner.
 * {@link ActionMechanisms} are created by {@link Action}s. 
 * 
 * @author Guillaume Blin
 *
 * @param <I> The type of database object the {@link ActionMechanism} can work on.
 * @param <O> the type of output this ActionMechanism creates
 */
public interface ActionMechanism<I,O> {

	/**
	 * Consumes the database object
	 * @param i 
	 * @throws DataAccessException
	 */
	public void performAction(I i) throws DataAccessException;
	
	/**
	 * Called once all objects have been consumed.
	 * @throws DataAccessException
	 */
	public default void lastAction() throws DataAccessException {/* Do nothing by default*/};
	
	public O get();
	
}

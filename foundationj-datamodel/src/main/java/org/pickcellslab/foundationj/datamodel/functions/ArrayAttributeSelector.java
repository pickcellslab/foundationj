package org.pickcellslab.foundationj.datamodel.functions;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;

/**
 * 
 * An {@link ExplicitFunction} allowing to select a value at a specific position in an attribute holding an array.
 * Can be used in {@link ExplicitPredicate}.
 * 
 * @author Guillaume Blin
 *
 * @param <R>
 */

@Mapping(id="ArraySelection")
@Scope
public class ArrayAttributeSelector<T extends WritableDataItem, R> implements ExplicitFunction<T,R> {

	
	@Supported
	private final ExplicitFunction<T,R[]> selector;
		
	private int index;
	
	

	public ArrayAttributeSelector(ExplicitFunction<T,R[]> selector, int index){		
		this.selector = selector;
		this.index = index;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public  R apply(T t) {
		
		Object array = selector.apply(t);//FIXME Not using generic as Object[] is returned in some situation, is this important?
				
		if(array == null || !array.getClass().isArray())
		return null;		
		else return (R) Array.get(array, index);
	}

	@Override
	public String description() {
		return selector.description()+"["+ index+"]";
	}
	
	@Override
	public String toString() {
		return selector.toString()+"["+ index+"]";
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public Class<R> getReturnType() {
		return (Class<R>) selector.getReturnType().getComponentType();
	}

	@Override
	public ExplicitOperator<?, ?>[] getOperatorCompatibility() {
		
		Class<?> clazz = getReturnType();
		
		if(Number.class.isAssignableFrom(clazz))
			return Op.Logical.values();
		else if(clazz == String.class)
			return Op.TextOp.values();
		else if(clazz == Boolean.class || clazz == boolean.class)
			return Op.Bool.values();
				
		return Op.Logical.values();
	}

}

package org.pickcellslab.foundationj.datamodel;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;

@TestInstance(Lifecycle.PER_CLASS)
public class TraversalTest {

	private DefaultNodeItem root;
	private final DefaultNodeItem n1 = new DefaultNodeItem("n1");
	private final DefaultNodeItem n2 = new DefaultNodeItem("n2");
	private final DefaultNodeItem n3 = new DefaultNodeItem("n3");
	private final DefaultNodeItem n4 = new DefaultNodeItem("n4");		
	private final DefaultNodeItem n5 = new DefaultNodeItem("n5");
	private final DefaultNodeItem n6 = new DefaultNodeItem("n6");
	private final DefaultNodeItem n7 = new DefaultNodeItem("n7");
	private final DefaultNodeItem n8 = new DefaultNodeItem("n8");
	private final DefaultNodeItem n9 = new DefaultNodeItem("n9");
	private final AKey<short[]> arrKey = AKey.get("Array", short[].class);

	@BeforeAll
	public void createGraph() {

		/* Create a graph of objects which we will query
		 * 
		 *           n5
		 *           |
		 *    n6 -- n2' - n7'
		 *    /     |   
		 *   n8'    n1 
		 *     \   /  \  
		 *      n3     n4'
		 *      |    
		 *      n9
		 *
		 * NB: depth 1 - OUTGOING, depth 2 - INCOMING All are IS_NEIGHBOUR_OF except n8->n6 which is SPECIAL
		 */

		System.out.println("setting up");

		// Add an array attribute
		n1.setAttribute(arrKey, new short[] {2,16,24});		n2.setAttribute(arrKey, new short[] {2,16,24});
		n3.setAttribute(arrKey, new short[] {2,16,24});		n4.setAttribute(arrKey, new short[] {2,16,24});
		n5.setAttribute(arrKey, new short[] {2,16,24});		n6.setAttribute(arrKey, new short[] {2,16,24});
		n7.setAttribute(arrKey, new short[] {2,16,24});		n8.setAttribute(arrKey, new short[] {2,16,24});
		n9.setAttribute(arrKey, new short[] {2,16,24});


		// Create the AKey
		final AKey<String> pass = AKey.get("pass", String.class);
		// Add the attribute. NB: In schematic above ' indicates that the node has the pass.
		n2.setAttribute(pass, "pass"); 		n4.setAttribute(pass, "pass");
		n7.setAttribute(pass, "pass");		n8.setAttribute(pass, "pass");


		final String neighbour = "IS_NEIGHBOUR_OF";
		new DataLink(neighbour, n1, n2,  true);		new DataLink(neighbour, n5, n2, true);
		new DataLink(neighbour, n6, n2, true);		new DataLink(neighbour, n7, n2, true);
		new DataLink(neighbour, n1, n3,  true);		new DataLink(neighbour, n1, n4,  true);
		new DataLink(neighbour, n9, n3,  true);		new DataLink(neighbour, n8, n3, true);
		new DataLink("SPECIAL", n8, n6,  true);

		root = n1;
		
		System.out.println("setup success");
	}


	@Test
	public void testBFNoConstraints() {

		final Set<NodeItem> found = Traversers.breadthFirst(root).traverse().nodes();
		Assertions.assertTrue(found.contains(n1));
		Assertions.assertTrue(found.contains(n2));
		Assertions.assertTrue(found.contains(n3));
		Assertions.assertTrue(found.contains(n4));
		Assertions.assertTrue(found.contains(n5));
		Assertions.assertTrue(found.contains(n6));
		Assertions.assertTrue(found.contains(n7));
		Assertions.assertTrue(found.contains(n8));
		Assertions.assertTrue(found.contains(n9));

	}


	@Test
	public void testBFAcceptLinks() {

		final Set<NodeItem> found = Traversers.breadthfirst(root,
				Traversers.newConstraints().fromMinDepth().toMaxDepth()
				.traverseOnly(Direction.OUTGOING)
				.includeAllNodes())
				.traverse().nodes();
		Assertions.assertTrue(found.contains(n1));
		Assertions.assertTrue(found.contains(n2));
		Assertions.assertTrue(found.contains(n3));
		Assertions.assertTrue(found.contains(n4));
		Assertions.assertTrue(!found.contains(n5));
		Assertions.assertTrue(!found.contains(n6));
		Assertions.assertTrue(!found.contains(n7));
		Assertions.assertTrue(!found.contains(n8));
		Assertions.assertTrue(!found.contains(n9));
	}

}

package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;

/**
 * An {@link ExplicitStream} which returns {@link Link} objects and offer a few additional convenience methods for mapping
 *
 * @param <T>
 */
public interface ExplicitLinkStream<T> extends ExplicitStream<T,Link> {
	
	
	/**
	 * @return The Stream mapped to the sources of the links
	 */
	public default StreamMap<T,Link, NodeItem> mapToSource(){
		return new StreamMap<>(this, LinkEnd.Source);
	}
	
	/**
	 * @return The Stream mapped to the targets of the links
	 */
	public default StreamMap<T,Link, NodeItem> mapToTarget(){
		return new StreamMap<>(this, LinkEnd.Target);
	}
	
	/**
	 * @return The Stream mapped to the desired key of the source
	 */
	public default <E> StreamMap<T,Link,E> mapToSourceKey(AKey<E> k) {
		return new StreamMap<>(this, F.fetchSourceKey(k));
	}
	
	/**
	 * @return The Stream mapped to the desired key of the target
	 */
	public default <E> StreamMap<T,Link,E> mapToTargetKey(AKey<E> k) {
		return new StreamMap<>(this, F.fetchTargetKey(k));
	}
	
}

package org.pickcellslab.foundationj.dbm.queries.builders;

import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public interface ReadOneBuilder<T> {

	/**
	 * Runs the query and uses the specified {@link ActionMechanism} when the object is encountered.
	 * NB: No call will be performed if the object is not found. 
	 * @param mecha The {@link ActionMechanism} to use.
	 * @throws DataAccessException
	 */
	<O> O using(Function<? super T,O> mecha) throws DataAccessException;
	
	
	/**
	 * Runs the query and returns the value for the specified {@link Dimension} found in the target object of the query.
	 * @param dim The {@link Dimension} to read
	 * @param defaultValue The default value to return if the object is not found or does not posess the provided Dimension
	 * @throws DataAccessException
	 */
	<N> N dimension(Dimension<T, N> dim, N defaultValue) throws DataAccessException;

}
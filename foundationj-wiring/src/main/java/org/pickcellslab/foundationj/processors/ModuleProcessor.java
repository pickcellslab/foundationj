package org.pickcellslab.foundationj.processors;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.annotations.Service;



public class ModuleProcessor extends AbstractProcessor {


	@Override
	public Set<String> getSupportedAnnotationTypes(){
		return Collections.singleton(Module.class.getName());
	};


	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;


		final Map<String, Set<String>> data = new HashMap<>();

		// Find Modules from the current compilation sources
		for (TypeElement type : (Collection<TypeElement>)roundEnv.getElementsAnnotatedWith(Module.class)) {

			// First check that contract is fulfilled
			String modularInterface = contractRespected(type);
			if(modularInterface != null) {
				Set<String> set = data.get(modularInterface);
				if(set == null) {
					set = new HashSet<>();
					data.put(modularInterface, set);
				}
				set.add(type.getQualifiedName().toString());
			}

		}

		final String metadataPath = "META-INF/foundationj/modules/";


		processingEnv.getMessager().printMessage(Kind.NOTE,"Writing "+metadataPath);
		Filer filer = processingEnv.getFiler();
		for (Map.Entry<String,Set<String>> e : data.entrySet()) {
			try {
				String modularInterface = e.getKey();
				FileObject f = filer.getResource(StandardLocation.CLASS_OUTPUT, "", metadataPath + modularInterface);
				BufferedReader r = new BufferedReader(new InputStreamReader(f.openInputStream(), "UTF-8"));
				String line;
				while((line=r.readLine())!=null)
					e.getValue().add(line);
				r.close();
			} catch (FileNotFoundException x) {
				// doesn't exist
			} catch (IOException x) {
				processingEnv.getMessager().printMessage(Kind.ERROR,"Failed to load existing modules definition files: "+x);
			}
		}

		// now write them back out
		for (Map.Entry<String,Set<String>> e : data.entrySet()) {
			try {
				String modularInterface = e.getKey();
				processingEnv.getMessager().printMessage(Kind.NOTE,"Writing "+metadataPath + modularInterface);
				FileObject f = filer.createResource(StandardLocation.CLASS_OUTPUT, "", metadataPath + modularInterface);
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(f.openOutputStream(), "UTF-8"));
				for (String value : e.getValue())
					pw.println(value);
				pw.close();
			} catch (IOException x) {
				processingEnv.getMessager().printMessage(Kind.ERROR,"Failed to write modules definition files: "+x);
			}
		}



		return false;
	}





	/**
	 * Perform any required checks and throw compilation errors if necessary
	 * @param type
	 * @return {@code true} if no errors, {@code false otherwise}
	 */
	protected String contractRespected(TypeElement type) {

		//--------------------------------------------------------------------//
		// First make sure that the annotation is on a concrete class
		//--------------------------------------------------------------------//

		ProcessorsUtils.checkConcrete(Module.class, type, processingEnv);


		//--------------------------------------------------------------------------//
		// Now, get all the implemented interfaces and check which ones are Modular
		//--------------------------------------------------------------------------//

		Set<TypeElement> modulars = ProcessorsUtils.findAllInterfacesWithAnnotation(Modular.class, type, processingEnv);

		if(modulars.isEmpty()) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "@Module must implement a @Modular interface",type);
			return null;
		}
		else if(modulars.size()>1) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "Only one @Modular interface can be implemented at once",type);
			return null;
		}


		//----------------------------------------------------------------------------------------------//
		// Finally check constructor arguments and ensure that all are either Modular or Core compliant
		//----------------------------------------------------------------------------------------------//

		//Get the type Mirror for Collection.class
		TypeMirror collectionMirror = processingEnv.getElementUtils().getTypeElement(Collection.class.getCanonicalName()).asType();
		Types types = processingEnv.getTypeUtils();

		for (ExecutableElement c: ElementFilter.constructorsIn(type.getEnclosedElements())) {
			for(VariableElement param : c.getParameters()) {

				TypeMirror tMirrorToCheck = null;

				//Check if array
				final TypeKind tk = param.asType().getKind();
				if (tk == TypeKind.ARRAY) {
					tMirrorToCheck = ((ArrayType)param.asType()).getComponentType();

					//Check that this is not a primitive
					if(tMirrorToCheck.getKind().isPrimitive()) {
						processingEnv.getMessager().printMessage(Kind.ERROR, "primitive arrays are unsupported "
								+ "in constructors of an @Module class", c);
						return null;
					}

				}

				// Check if we have a collection (with type parameter)
				else if(tk == TypeKind.DECLARED) {

					// Is this a Collection ?
					DeclaredType declared = ((DeclaredType)param.asType());
					if(types.isSubtype(declared, types.erasure(collectionMirror))) {

						// get the type parameter
						List<? extends TypeMirror> typeParams = declared.getTypeArguments();
						if(typeParams.size() == 0) {
							processingEnv.getMessager().printMessage(Kind.ERROR, "Collection arguments of @Module class constructors must "
									+ "be parameterised with @Modular interface as a type parameter", c);
							return null;
						}
						else {
							tMirrorToCheck = typeParams.get(0);
						}
					}//END of checking for Collection
					else {
						tMirrorToCheck = param.asType();
					}
				} // END of checking DECLARED Type
				else {			
					continue; //this must be an error such as non imported object.
				}

				//Now check if annotated with @Modular
				final TypeElement el = (TypeElement) types.asElement(tMirrorToCheck); 

				if(el.getAnnotation(Modular.class)==null
						&& el.getAnnotation(Core.class)==null
						&& el.getAnnotation(Service.class)==null) { // FIXME no need to check service
					
					// CheckHierarchy
					TypeElement modular = ProcessorsUtils.findOneInterfaceWithAnnotation(Modular.class, el, processingEnv);
					if(modular == null) {
						TypeElement core = ProcessorsUtils.findOneInterfaceWithAnnotation(Core.class, el, processingEnv);
						if(core == null) {
							processingEnv.getMessager().printMessage(Kind.ERROR, "The arguments of a @Module, must be either @Modular,"
									+ "@Core or @Service interfaces or arrays or collecions thereof", c);
							return null;
						}
					}
				}
				
			}
		}



		return modulars.iterator().next().toString();

	}






}
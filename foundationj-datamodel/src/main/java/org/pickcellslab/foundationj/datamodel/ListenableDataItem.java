package org.pickcellslab.foundationj.datamodel;

/**
 * Represents a {@link DataItem} which fire {@link AttributeChangeEvent} when one of its attribute is changed, added or deleted
 * 
 * @author Guillaume Blin
 *
 */
public interface ListenableDataItem extends WritableDataItem{

	void addAttributeChangeListener(AttributeChangeListener l);

	void removeAttributeChangeListener(AttributeChangeListener l);

	void removeAllAttributeChangeListeners();

}
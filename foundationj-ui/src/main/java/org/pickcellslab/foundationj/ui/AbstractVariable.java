package org.pickcellslab.foundationj.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractVariable<E> implements Variable<E> {

	protected List<VariableListener> varLstrs = new ArrayList<>();

	private final String name;
	private final String description;
	
	protected E currentValue;

	
	
	public AbstractVariable(String name, E defaultValue, String description) {
		Objects.requireNonNull(name);
		Objects.requireNonNull(defaultValue);
		this.name = name;
		this.currentValue = defaultValue;
		this.description = description == null ? "NA" : description;
	}
	
	
	@Override
	public String name() {
		return name;
	}
	
	@Override
	public String description() {
		return description;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<E> type() {
		return (Class<E>) currentValue.getClass();
	}

	@Override
	public E value() {
		return currentValue;
	}
		

	@Override
	public void setValue(E newValue) {
		if(newValue == null)	return;
		check(newValue);
		final E oldValue = currentValue;
		currentValue = newValue;
		varLstrs.forEach(l->l.valueChanged(this, oldValue, newValue));
	}
	
	
	protected abstract void check(E newValue) throws IllegalArgumentException;
	
	

	@Override
	public void addVariableListener(VariableListener l) {
		if(l!=null)
			varLstrs.add(l);
		
	}

	@Override
	public void removeVariableListener(VariableListener l) {
		varLstrs.remove(l);
	}

	
	public String toString() {
		return name + " Variable";
	}


}

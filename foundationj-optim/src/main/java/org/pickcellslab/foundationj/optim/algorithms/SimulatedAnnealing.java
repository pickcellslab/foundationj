package org.pickcellslab.foundationj.optim.algorithms;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.DoublePredicate;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

public class SimulatedAnnealing<T> {

	private static final Logger log = LoggerFactory.getLogger(SimulatedAnnealing.class);
	
	private final NeighbourFunction<T> neighbour;
	private final AcceptanceFunction probability;
	private final Function<T,Double> costFunction;
	private final SolutionAssignment<T> assignment;
	
	private Function<T,String> describer = t->t.toString();
	
	private T solution;
	
	
	//Monitoring of the algorithm
	public List<double[]> temperature = new ArrayList<>();
	public static final int time = 0, t = 1,  best = 2, steps = 3, accepted = 4;
	
	
	
	public SimulatedAnnealing(
			NeighbourFunction<T> neighbour,
			AcceptanceFunction probability,
			Function<T, Double> costFunction,
			SolutionAssignment<T> assignment
			){
		
		Objects.requireNonNull(neighbour,"Arguments of the SimulatedAnnealing cannot be null");
		Objects.requireNonNull(probability,"Arguments of the SimulatedAnnealing cannot be null");
		Objects.requireNonNull(costFunction,"Arguments of the SimulatedAnnealing cannot be null");
		Objects.requireNonNull(assignment,"Arguments of the SimulatedAnnealing cannot be null");
		
		this.neighbour = neighbour;
		this.probability = probability;
		this.costFunction = costFunction;
		this.assignment = assignment;
		
	}
	
		
	public void setTypeDescription(Function<T,String> describer){
		Objects.requireNonNull(describer, "Cannot set a null describer");
		this.describer = describer;
	};
	
	
	/*
	public void setLogLevel(Level level){
		log.setLevel(level);
	}
	*/
	
	
	public void solve(double temp, DoubleUnaryOperator regime, DoublePredicate stop){
		
		double initTemp = temp;
		
		long before = System.currentTimeMillis();
				
		// Initialize intial solution
		solution = neighbour.init();
		double bestCost = costFunction.apply(solution);
		
		log.info("Initial Cost: " + bestCost);
		
		
		T currentSolution = solution;
		double currentCost = bestCost;	
		

		// Loop until system has cooled
		while (stop.test(temp)) {
			
			double prevTemp = temp;
			
			// Create new neighbour solution
			T newSolution = neighbour.next(currentSolution, temp);
			//Calculate the new cost
			double newCost = costFunction.apply(newSolution);

			log.debug("Evaluating cost = "+newCost);

			// Decide if we should accept the neighbour
			if (probability.compute(currentCost, newCost, temp) > Math.random()) {
				currentCost = newCost;
				currentSolution = assignment.apply(newSolution);
				log.debug("New cost accepted");
			}

			// Keep track of the best solution found
			if (currentCost < bestCost) {				
				log.debug("New cost is best so far!");				
				bestCost = currentCost;
				solution = assignment.apply(currentSolution);
				log.trace(describer.apply(solution));
			}

			// Cool system
			temp = regime.applyAsDouble(temp);
			
			if(prevTemp<temp)
				currentSolution = solution;//TODO double check this
			
			double[] m = new double[5];
			m[time] = System.currentTimeMillis()-before;
			m[t] = temp;
			m[best] = bestCost;
			m[steps] = newCost;
			m[accepted] = currentCost;
			temperature.add(m);
		}
		
		
		
		long after = System.currentTimeMillis();		
		log.info("Processing time : "+(after - before)+"ms");
		log.info("Final Solution Cost -> "+bestCost);
		log.trace(describer.apply(solution));
	}
	
	public void solve(double temp, double coolingRate){
		solve(temp, d->d*(1-coolingRate), t->t>1);		
	}
	
	
	public Optional<T> getSolution(){
		return Optional.ofNullable(solution);
	}
	
	
	public static AcceptanceFunction defaultFunction(){
		return new AcceptanceFunction(){

			@Override
			public double compute(double energy, double newEnergy, double temperature) {
				// If the new solution is better, accept it
				if (newEnergy < energy) {
					return 1.0;
				}
				// If the new solution is worse, calculate an acceptance probability
				return Math.exp((energy - newEnergy) / temperature);
			}
			
		};
	}
	
	public void save(String fileName) throws FileNotFoundException {
	    PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));	    
	    pw.println("Temp \t Best \t Neighbour \t Accepted \t Time");
	    for (double[] t : temperature)
	        pw.println(t[SimulatedAnnealing.t]+"\t"+t[best]+"\t"+t[steps]+"\t"+t[accepted]+"\t"+t[time]);
	    pw.close();
	}
	
}

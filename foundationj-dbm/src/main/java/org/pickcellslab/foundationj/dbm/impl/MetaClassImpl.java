package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.access.RegenerableDataPointer;
import org.pickcellslab.foundationj.dbm.db.UpdatableNode;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.dbm.queries.ReadAnyBuilder;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.AllOrSome;
import org.pickcellslab.foundationj.dbm.queries.builders.DeletionConfig;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludeExcludeKeyInit;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadItemsBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.data.MultitonOutcome;


@Data(typeId = "MetaClass", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
class MetaClassImpl extends AbstractMetaQueryable implements MetaClass {



	protected MetaClassImpl(String uuid) {
		super(uuid);
	}



	private static final AKey<String> typeId = AKey.get("Class", String.class);




	static MetaClassImpl getOrCreate(MetaInstanceDirector director, Class<? extends DataItem> clazz, String declaredType, String creator, String description){

		try {

			final MultitonOutcome<MetaClassImpl> out = director.getOrCreateWithOutcome(MetaClassImpl.class, declaredType);

			if(out.wasCreated()) {
				final MetaClass mc = out.getInstance();
				mc.setAttribute(MetaModel.name, declaredType);
				if(creator!=null)
					mc.setAttribute(MetaModel.creator, creator);
				if(description!=null)
					mc.setAttribute(MetaModel.desc, description);

				mc.setAttribute(typeId, DataRegistry.typeIdFor(clazz));
			}

			return out.getInstance();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}



	@Override
	public String itemTypeId(){		
		return this.getAttribute(typeId).get();
	}


	@SuppressWarnings("unchecked")
	@Override
	public Class<? extends NodeItem> itemClass(DataRegistry registry) {
		Objects.requireNonNull(registry);
		return (Class<? extends NodeItem>) registry.classOf(itemTypeId());
	}


	@Override
	public String itemDeclaredType(){
		return name();
	}




	@Override
	public Stream<MetaLink> metaLinks(String linkType, Direction direction){
		Stream<Link> links = null;
		switch(direction){
		case BOTH : links = this.getLinks(Direction.INCOMING,new String[]{MetaModel.fromLink, MetaModel.toLink});
		break;
		case INCOMING : links = this.getLinks(Direction.INCOMING, MetaModel.toLink);
		break;
		case OUTGOING : links = this.getLinks(Direction.INCOMING, MetaModel.fromLink);
		break;      
		}
		if(links!=null)
			return links
					.map(l -> (MetaLink)l.source())
					.filter(ml -> ml.linkType().equals(linkType));
		else 
			return new ArrayList<MetaLink>().stream();
	}








	@Override
	public Stream<MetaLink> metaLinks(Direction direction){
		Stream<Link> links = null;
		switch(direction){
		case BOTH : links = this.getLinks(Direction.INCOMING,new String[]{MetaModel.fromLink, MetaModel.toLink});
		break;
		case INCOMING : links = this.getLinks(Direction.INCOMING, MetaModel.toLink);
		break;
		case OUTGOING : links = this.getLinks(Direction.INCOMING, MetaModel.fromLink);
		break;      
		}		
		return links.map(l->(MetaLink)l.source());		
	}



	@Override
	public Stream<MetaLink> metaLinks(){
		return 
				this.getLinks(Direction.INCOMING, new String[]{MetaModel.fromLink, MetaModel.toLink})
				.map(l->(MetaLink)l.source());
	}


	@Deprecated
	public <C> C metaLinks(Collector<MetaLink,?,C> collector, Predicate<? super MetaLink> predicate){
		return 
				this.getLinks(Direction.INCOMING, new String[]{MetaModel.fromLink, MetaModel.toLink})
				.map(l->(MetaLink)l.source())
				.filter(predicate)
				.collect(collector);
	}



	public void setTag(MetaTag tag) {
		new DataLink(MetaModel.tagLink, this, tag, true);
	}


	/**
	 * @return All the {@link MetaTag} describing the tags that may be found on the class of 
	 * {@link WritableDataItem} this MetaClass describes
	 */
	@Override
	public Stream<MetaTag> tags(){
		return this.getLinks(Direction.OUTGOING, MetaModel.tagLink)
				.map(l->(MetaTag)l.target());
	}

	@Override
	public int hashCode() {
		return itemDeclaredType().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		if(!MetaClass.class.isAssignableFrom(o.getClass()))
			return false;
		MetaClass other = (MetaClass) o;
		return this.itemDeclaredType().equals(other.itemDeclaredType());
	}







	@Override
	public String toHTML() {
		return "<HTML>"+
				"<p><strong>Data Type</strong>"+
				"<ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).orElse("Not Available")+
				"</li></p></HTML>";
	}


	@Override
	public String toString(){
		return name()+" (Node)";
	}





	@Override
	public TraversalDefinition deletionDefinition() {
		//Traverse nodes starting from the this MetaClass
		//delete all nodes connected until we reach other MetaClass
		//Special case for tags which should be included only if they have only one target
		return TraversalDefinition.newDefinition()
				.startFrom(typeId()).having(MetaModel.recognition, this.recognitionString())
				.fromDepth(0).toDepth(2)
				.traverseAllLinks()
				.withEvaluations(
						P.items().include(MetaClassImpl.class)
						.with(MetaModel.recognition, Op.TextOp.DISTINCT, this.recognitionString()).create(),
						Decision.EXCLUDE_AND_STOP,
						Decision.INCLUDE_AND_CONTINUE)
				.lastEvaluation(
						F.streamLinks().count().isSuperiorTo(1).and(P.isDeclaredType(DataRegistry.typeIdFor(MetaTag.class)))
						, Decision.EXCLUDE_AND_STOP)
				.build();
	}





	@Override
	public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
		return access.queryFactory().read(name(), limit).with(action).inOneSet().getAll().run(); 
		// we can use name() (declaredType of represented nodes)
	}



	@Override
	public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
		return access.queryFactory().readOneNode(name(), dbId).using(action);
	}


	@Override
	public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
		return access.queryFactory().readAnyNode(name()).using(action).getAll().run();
	}
	
	

	@Override
	public void updateData(DataAccess access, Updater action) throws DataAccessException {
		access.queryFactory().update(name(), action).getAll().run();
	}



	@Override
	public DeletionConfig<? extends WritableDataItem> initiateDeletion(DataAccess access) {
		return access.queryFactory().delete(name());
	}



	@Override
	public AccessChoice<UpdatableNode, QueryRunner<Void>> initiateUpdate(DataAccess access, Updater updater) {
		return access.queryFactory().update(name(), updater);
	}



	@Override
	public ReadItemsBuilder<? extends WritableDataItem> initiateRead(DataAccess access, long limit) {
		return access.queryFactory().read(name(), limit);
	}



	@Override
	public ReadAnyBuilder<? extends WritableDataItem> initiateFindOne(DataAccess access) {
		return access.queryFactory().readAnyNode(name());
	}



	@Override
	public List<MetaQueryable> queriedMetaObjects() {
		return Collections.singletonList(this);
	}




	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).orElse("Not Available");
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.creator, MetaModel.desc, MetaClassImpl.typeId);
	}



	@Override
	public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> initRegeneration(
			DataAccess access) {
		Objects.requireNonNull(access);
		return access.queryFactory().regenerate(this.itemDeclaredType());
	}



	@Override
	public RegeneratedItems finaliseRegeneration(AccessChoice<NodeItem, QueryRunner<RegeneratedItems>> choice)
			throws DataAccessException {
		Objects.requireNonNull(choice);
		return choice.getAll().run();
	}



	@Override
	public RegenerableDataPointer subset(ExplicitPredicate<DataItem> predicate) {
		
		Objects.requireNonNull(predicate);
		
		return new RegenerableDataPointer() {

			@Override
			public int deleteData(DataAccess access) throws DataAccessException {
				throw new RuntimeException("Forbidden for now");
			}

			@Override
			public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
				return access.queryFactory().read(name(), limit).with(action).inOneSet().useFilter(predicate).run(); 
			}



			@Override
			public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
				return access.queryFactory().readOneNode(name(), dbId).using(action);
			}
			

			@Override
			public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
				return access.queryFactory().readAnyNode(name()).using(action).useFilter(predicate).run();
			}



			@Override
			public void updateData(DataAccess access, Updater action) throws DataAccessException {
				access.queryFactory().update(name(), action).useFilter(predicate).run();
			}

			@Override
			public List<MetaQueryable> queriedMetaObjects() {
				return MetaClassImpl.this.queriedMetaObjects();
			}

			@Override
			public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> initRegeneration(
					DataAccess access) {
				Objects.requireNonNull(access);
				return access.queryFactory().regenerate(MetaClassImpl.this.itemDeclaredType());
			}



			@Override
			public RegeneratedItems finaliseRegeneration(AccessChoice<NodeItem, QueryRunner<RegeneratedItems>> choice)
					throws DataAccessException {
				Objects.requireNonNull(choice);
				return choice.useFilter(predicate).run();
			}

			@Override
			public RegenerableDataPointer subset(ExplicitPredicate<DataItem> predicate2) {
				Objects.requireNonNull(predicate2);
				return MetaClassImpl.this.subset(predicate.and(predicate2));
			}
			
		};
	}


}

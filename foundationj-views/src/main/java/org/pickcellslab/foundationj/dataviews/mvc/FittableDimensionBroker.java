package org.pickcellslab.foundationj.dataviews.mvc;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dataviews.events.FittingEvtListener;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.fitting.FittingMethod;

/**
 * @author Guillaume Blin
 *
 * @param <T> The type of objects the dimensions are defined for
 * @param <R> The type of coordinates required for fitting
 * @param <D> The type of dimensions outputs 
 * 
 */
public interface FittableDimensionBroker<T,R,D> extends DimensionBroker<T,D> {


	public int numFittableDimensions();

	public Dimension<T,D> fittableDimension(int i);

	
	
	public int numFittingMethods();

	public FittingMethod<?> getFittingMethod(int i);

	
	
	public void fitRequest(String prefix, int method, int dim, int... dims) throws Exception;

	public void deleteFit(String prefix);

		
	/**
	 * @param stringId The id of the fit (same as {@link FitResult#getId()})
	 * @return The fit with the specified Id
	 */
	public FitResult<? extends Distribution<R>> getDistributionFit(String stringId);

	//TODO add RegressionFitterFactory
	
	
	
	public void addFittingListener(FittingEvtListener l);

	public void removeFittingListener(FittingEvtListener l);

	
	
	
	

}

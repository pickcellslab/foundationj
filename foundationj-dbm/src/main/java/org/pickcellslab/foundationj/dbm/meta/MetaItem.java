package org.pickcellslab.foundationj.dbm.meta;

import java.util.UUID;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;

/**
 * Anyone wishing to extend the (non-transient) {@link MetaModel} should extend this abstract base class.
 * <br><br>
 * <b>NB: </b>Extending class must be annotated with @{@link Data}
 * 
 * @author Guillaume Blin
 *
 */
@Multiton(manager = MetaInstanceDirector.class)
public abstract class MetaItem extends DataNode implements MetaModel{

	
	protected static String randomUID(Class<? extends MetaItem> item) {
		return item.getSimpleName() + "_" + UUID.randomUUID().getMostSignificantBits();
	}
	
	protected MetaItem(String uuid){
		this.setAttribute(MetaModel.recognition, uuid);
	}
	
	

	@Override
	public int hashCode(){
		return 17 * recognitionString().hashCode();
	}

	@Override
	public boolean equals(Object o){
		if(o==null) return false;
		if(MetaItem.class.isAssignableFrom(o.getClass()))
			return this.recognitionString().equals(((MetaItem) o).recognitionString());
		else return false;
	}

	
	@Override
	public final String recognitionString() {
		return getAttribute(MetaModel.recognition).get();
	}
	

}

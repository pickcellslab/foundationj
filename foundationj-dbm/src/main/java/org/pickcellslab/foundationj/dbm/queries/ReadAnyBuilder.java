package org.pickcellslab.foundationj.dbm.queries;

import java.util.function.Function;

import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

public interface ReadAnyBuilder<T> {

	
	public <O> AccessChoice<T, QueryRunner<O>> using(Function<? super T,O> function);

	
}

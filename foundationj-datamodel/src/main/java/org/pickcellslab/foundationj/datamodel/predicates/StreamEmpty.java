package org.pickcellslab.foundationj.datamodel.predicates;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "Is Empty")
public class StreamEmpty<T, R> implements ExplicitPredicate<T> {

	
	@Supported
	private final ExplicitFunction<T,Stream<R>> stream; 
	
	public StreamEmpty(ExplicitFunction<T,Stream<R>> stream){
		MappingUtils.exceptionIfNullOrInvalid(stream);
		this.stream = stream;
	}
	
		
	

	@Override
	public boolean test(T t) {
		return stream.apply(t).findAny().isPresent();
	}

	@Override
	public String description() {
		return stream.description()+" is empty? ";
	}
	
}

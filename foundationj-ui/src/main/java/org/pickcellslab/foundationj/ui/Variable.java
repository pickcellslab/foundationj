package org.pickcellslab.foundationj.ui;

public interface Variable<E>{
	
	public String name();
	
	public String description();
	
	public Class<E> type();
	
	public E value();
	
	public void setValue(E newValue);
	
	public void addVariableListener(VariableListener l);
	
	public void removeVariableListener(VariableListener l);
}
package org.pickcellslab.foundationj.optim.csp;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.EdgeFactory;
import org.pickcellslab.foundationj.datamodel.graph.GraphListener;
import org.pickcellslab.foundationj.datamodel.graph.ListenableGraph;
import org.pickcellslab.foundationj.datamodel.graph.Node;
import org.pickcellslab.foundationj.optim.algorithms.NeighbourFunction;
import org.pickcellslab.foundationj.optim.algorithms.SimulatedAnnealing;
import org.pickcellslab.foundationj.optim.algorithms.SolutionAssignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <HTML>A {@link GraphOptimization} allows to solve assignment problems where several constraints must be met.
 * <br>It takes as input:
 * <br> 1- A directed pseudo graph where source nodes may be assigned to targets. This graph represents a model containing 
 * all possible solutions.
 * <br> 2- A Validation function determining when a specific assignment is valid (locally, taking a set of edges from one source as input)
 * <br> 3- A Cost function which computes the cost of assignments.
 * <br> 4- A "Local Neighbour" function which can, for a given node, return possible assignments combinations to be considered as
 * a potential solution. 
 * <br><br>
 * The above input are provided to the GraphOptimization as abstract factories which provides freedom to create "static" inputs for all
 * nodes or one instance per node, allowing for example to compute the cost based on individual assignments only or by taking into
 * consideration parameters of the current global solution state.
 * 
 * 
 * @author Guillaume Blin
 *
 * @param <N> The type of {@link Node}
 * @param <D> The type of {@link Edge}
 */
public class GraphOptimization<N extends Node<D>,D extends Edge<N>> implements GraphListener<N,D>{


	private static Logger log = LoggerFactory.getLogger(GraphOptimization.class);

	private final OptimizationPartsFactory<Validator<N,D>,N,D> vFactory;
	private final OptimizationPartsFactory<CostFunction<N,D>,N,D> cFactory;
	private final OptimizationPartsFactory<Enumerator<N,D>,N,D> eFactory;

	private List<OptimizationListener<? super N, ? super D>> lstrs = new ArrayList<>();

	//The list of all the solvers in this graphview
	private List<Solver<N,D>> solvers;
	//A map which references each node to its solver if the user wishes to listen to changes 
	//to specific validity or cost changes
	private Map<N,Solver<N,D>> map = new HashMap<>();	
	//The best solution found
	private Map<Solver<N,D>, Collection<Solver<N,D>>> solution = new HashMap<>();


	private boolean isUpToDate = false;


	private SimulatedAnnealing<Map<Solver<N,D>, Collection<Solver<N,D>>>> sa;

	//TODO builder

	public GraphOptimization(
			OptimizationPartsFactory<Validator<N,D>,N,D> vFactory,
			OptimizationPartsFactory<CostFunction<N,D>,N,D> cFactory,
			OptimizationPartsFactory<Enumerator<N,D>,N,D> eFactory,
			ListenableGraph<N,D> graph){

		this.vFactory = vFactory;
		this.cFactory = cFactory;
		this.eFactory = eFactory;

		log.debug("Number of solvers to create: "+graph.numberOfNodes());

		graph.addListener(this);

		solvers = new ArrayList<>(graph.numberOfNodes());		
		Set<N> nodes = graph.nodeSet();

		//Create a graph of solvers based on the provided graph
		//Create nodes (solvers)
		nodes.forEach(n-> {
			Solver<N,D> s = new Solver<>(n);
			solvers.add(s);
			map.put(n,s);
		});


		// Configure using the provided factories
		solvers.forEach(s-> {			
			s.setValidator(vFactory.getInstanceFor(s));
			s.setCostFunction(cFactory.getInstanceFor(s));
			s.setEnumerator(eFactory.getInstanceFor(s));
		});

		//Create solvers edges
		nodes.forEach(n-> {
			map.get(n).initialise(
					n.links().filter(l-> l.source() == n && l.target() != n)
					.map(e->map.get(e.target()))
					.collect(Collectors.toList()));
		});


		log.debug("Solvers created");
	}


	public boolean isUpToDate(){
		return isUpToDate;
	}

	/*
	public static void setLogLevel(Level l){
		log.setLevel(l);
		Solver.setLevel(l);		
	}
	*/

	public void setNamingConvention(Function<N,String> naming){
		solvers.forEach(s->s.setNaming(naming));
	}


	public Solver<N,D> solverForNode(N node){
		return map.get(node);
	}



	public Collection<N> getNodes(){
		return solvers.stream()
				.map(s->s.getNode()).collect(Collectors.toList());
	}



	public Collection<N> getNodes(boolean validity){
		return solvers.stream()
				.filter(s->s.currentValidity()==validity)
				.map(s->s.getNode()).collect(Collectors.toList());
	}

	public List<N> getSortedNodes(boolean validity){
		solvers.sort((i1,i2) -> Double.compare(i1.currentCost(),i2.currentCost())); 
		return solvers.stream()
				.filter(s->s.hasSolution()==validity)
				.map(s->s.getNode()).collect(Collectors.toList());
	}


	public Collection<Solver<N,D>> getSortedSolvers(){
		solvers.sort((i1,i2) -> Double.compare(i1.currentCost(),i2.currentCost()));
		return solvers;
	}



	/**
	 * Attempts to find the cheapest valid assignments in the graph using a Simmulated Annealing algorithm.
	 * If this method was previously called, then the previous solution is erased meaning that the assignments are 
	 * reset to an empty selection.
	 * This allows the user to externally modify the graph between 2 calls (adding, removing edges) and obtain an 
	 * updated solution while keeping the same solvers
	 */
	public  Map<Solver<N,D>, Collection<Solver<N,D>>> solve(double temp, double coolingRate){

		isUpToDate = false;
		
		sa = new SimulatedAnnealing<>(
				new Neighbour(),
				SimulatedAnnealing.defaultFunction(),
				new Cost(),
				new Assignment()
				);

		//sa.setLogLevel(log.getLevel());
		
		sa.solve(temp, coolingRate);

		solution = sa.getSolution().get();

		//Sort solvers
		getSortedSolvers().stream().forEach(s->s.setSelection(solution.get(s)));

		isUpToDate = true;

		fireUpToDateChanged();
		
		return solution;

	}


	public Map<Solver<N, D>, Collection<Solver<N, D>>> getSolution() {
		return solution;
	}


	public void save(String fileName) throws FileNotFoundException {
		if(sa!=null)
			sa.save(fileName);
	}



	/**
	 * Calling this method will cause the view to actually modify the graph.
	 * Any Edge passing the specified predicate not selected in the view will be deleted
	 */
	public void deleteUnselectedEdges(Predicate<D> p){
		getSortedSolvers().stream().forEach(s->s.deleteNonSelected(p));
	}







	//private classes for Simulated Annealing
	//-----------------------------------------------------------------------------------------------------------------

	//NeighbourFunction
	private class Neighbour implements NeighbourFunction<Map<Solver<N,D>, Collection<Solver<N,D>>>>{

		@Override
		public Map<Solver<N, D>, Collection<Solver<N, D>>> next(Map<Solver<N, D>, Collection<Solver<N, D>>> current, double temp) {

			//reassigning the provided selection
			//Step might not be accepted
			getSortedSolvers().stream().forEach(s->s.setSelection(current.get(s)));


			// Create new neighbour solution
			int index = (int) (current.size()*Math.random());
			//log.debug("------------------------------------------------------------------------------------------");
			//log.debug("Choosing index : "+ index);
			solvers.get(index).screenAsRoot();
			log.debug("Root screen done, initiate screen in solvers with no solution");
			solvers.stream().filter(s->!s.hasSolution()).forEach(s->s.screen());

			Map<Solver<N, D>, Collection<Solver<N, D>>> map = new HashMap<>();
			solvers.forEach(s->map.put(s, s.getSelection()));			
			return map;
		}

		@Override
		public Map<Solver<N, D>, Collection<Solver<N, D>>> init() {
			Map<Solver<N, D>, Collection<Solver<N, D>>> map = new HashMap<>();
			solvers.forEach(s->map.put(s, s.getSelection()));			
			return map;
		}

	}


	//Assignment operation
	private class Assignment implements SolutionAssignment<Map<Solver<N,D>, Collection<Solver<N,D>>>>{

		@Override
		public Map<Solver<N, D>, Collection<Solver<N, D>>> apply(Map<Solver<N, D>, Collection<Solver<N, D>>> t) {
			// TODO Test creating a new map
			return t;
		}

	}



	//Total cost computation
	private class Cost implements Function<Map<Solver<N,D>, Collection<Solver<N,D>>>, Double>{

		@Override
		public Double apply(Map<Solver<N, D>, Collection<Solver<N, D>>> t) {

			DoubleAdder globalCost = new DoubleAdder();
			LongAdder globalValidity = new LongAdder();

			t.keySet().stream().parallel().forEach(s->{
				globalCost.add(s.currentCost());
				if(s.currentValidity())
					globalValidity.increment();
			});

			if(globalValidity.longValue()!=0)
				return globalCost.doubleValue();//*globalValidity.longValue();
			else
				return globalCost.doubleValue()/2;
		}

	}









	//GraphListener methods 
	//------------------------------------------------------------------------------------------------------------


	@Override
	public void removed(N n) {		
		
		Solver<N,D> s = map.get(n);
		
		s.potentialTargets().forEach(t->t.removePotentialPickedBy(s));
		s.potentialPickers().forEach(p->p.removePotentialTarget(s));
				
		//Remove from map
		solvers.remove(s);
		map.remove(n);		
		
		isUpToDate = false;		
		fireUpToDateChanged();
	}


	@Override
	public void removed(D e) {
		Solver<N, D> s = map.get(e.source());		
		Solver<N, D> t = map.get(e.target());
		
		log.info("Removing: "+s.getNode()+" and "+t.getNode());
		
		s.removePotentialTarget(t);
		t.removePotentialPickedBy(s);
		
		isUpToDate = false;		
		fireUpToDateChanged();
	}



	@Override
	public void added(D e) {
		
		Solver<N, D> s = map.get(e.source());
		if(s==null){
			added(e.source());
			return;
		}			
		Solver<N, D> t = map.get(e.target());
		if(t==null){
			added(e.target());
			return;
		}
		
		s.addPotentialTarget(t);
		t.addPotentialPicker(s);
		
		isUpToDate = false;		
		fireUpToDateChanged();
	}

	
	

	@Override
	public void added(N n) {
		System.out.println("Receiving add ->"+n.getClass().getSimpleName());
		//Create a new solver for this new node
		Solver<N,D> s = new Solver<>(n);
		solvers.add(s);
		map.put(n,s);

		s.setValidator(vFactory.getInstanceFor(s));
		s.setCostFunction(cFactory.getInstanceFor(s));
		s.setEnumerator(eFactory.getInstanceFor(s));

		/*
		//reinit all of the connected
		List<Solver<N,D>> connected = 
				n.links(l-> l.source() == n && l.target() != n)
				.stream()
				.map(e->map.get(e.target()))
				.collect(Collectors.toList());

		connected.forEach(c->reinit(c));
		*/
		s.initialise(s.getNode().links().filter(l-> l.source() == n && l.target() != n)
				.map(l-> map.get(l.target()))
				.collect(Collectors.toList()));

		isUpToDate = false;	
		fireUpToDateChanged();

	}


	//Allow listeners registration
	//---------------------------------------------------------------------------------------------------------------------
	public void addListener(OptimizationListener<? super N, ? super D> l){
		lstrs.add(l);
	}
	



	private void fireUpToDateChanged() {
		lstrs.forEach(l->l.uptodateStateChanged(this, isUpToDate));
	}




	/**
	 * A utility method which can generate a model graph to use with {@link GraphOptimization}.
	 * A complete graph will be evaluated using the provided node and edges will be created if the provided BiFunction applied on
	 * the source and target returns true.
	 * @param nodes A Collection of nodes to be included in the graph.
	 * @param predicate A {@link BiFunction} determining when to assign an edge between a source node and a target node.
	 * @param factory An {@link EdgeFactory} to create new Edges
	 * @return A {@link ListenableGraph} that can be used for a GraphOptimization problem.
	 */
	public  static <N extends Node<D>,D extends Edge<N>> ListenableGraph<N,D> 
		createGraphModel(Collection<N> nodes, BiFunction<N,N,Boolean> predicate, EdgeFactory<N,D> factory){
		
		ListenableGraph<N,D> graph = new ListenableGraph<>(factory, nodes);
		
		ICombinatoricsVector<N> v = Factory.createVector(nodes);
		// Create the permutation generator by calling the appropriate method in the Factory class
		Generator<N> gen = Factory.createPermutationWithRepetitionGenerator(v,2);
		for(ICombinatoricsVector<N> combination : gen)
			if(combination.getValue(0) != combination.getValue(1)
			&& predicate.apply(combination.getValue(0), combination.getValue(1)))
				factory.create(combination.getValue(0), combination.getValue(1));
		
		return graph;
	}


}

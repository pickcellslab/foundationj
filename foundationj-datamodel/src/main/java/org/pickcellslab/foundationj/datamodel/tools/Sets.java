package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;

import org.pickcellslab.foundationj.datamodel.tools.SuppliedSetOperation.Builder;

/**
 * 
 * Static utility method for set operations
 *
 */
public final class Sets {


	private Sets(){/*Not instantiable*/}

	public static <T> Set<T> union(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>(setA);
		tmp.addAll(setB);
		return tmp;
	}

	public static SetOperation union(){
		return new SetOperation(){
			@Override
			public <T> Set<T> apply(Set<T> a, Set<T> b) {
				Set<T> tmp = new HashSet<T>(a);
				tmp.addAll(b);
				return tmp;
			}
		};
	}

	public static <T> Set<T> intersection(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>();
		for (T x : setA)
			if (setB.contains(x))
				tmp.add(x);
		return tmp;
	}

	public static SetOperation intersection(){
		return new SetOperation(){
			@Override
			public <T> Set<T> apply(Set<T> a, Set<T> b) {
				Set<T> tmp = new HashSet<T>();
				for (T x : a)
					if (b.contains(x))
						tmp.add(x);
				return tmp;
			}
		};
	}

	/**
	 * @param setA
	 * @param setB
	 * @return A Set containing all the elements of setA not present in setB
	 */
	public static <T> Set<T> difference(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>(setA);
		tmp.removeAll(setB);
		return tmp;
	}

	public static SetOperation difference(){
		return new SetOperation(){
			@Override
			public <T> Set<T> apply(Set<T> a, Set<T> b) {
				Set<T> tmp = new HashSet<T>(a);
				tmp.removeAll(b);
				return tmp;
			}			
		};
	}


	public static <T> Set<T> symDifference(Set<T> setA, Set<T> setB) {
		Set<T> tmpA;
		Set<T> tmpB;
		tmpA = union(setA, setB);
		tmpB = intersection(setA, setB);
		return difference(tmpA, tmpB);
	}

	public static SetOperation symDifference(){
		return new SetOperation(){
			@Override
			public <T> Set<T> apply(Set<T> a, Set<T> b) {
				//tmpA is union
				Set<T> tmpA = new HashSet<T>(a);
				tmpA.addAll(b);
				//tmpB is intersection
				Set<T> tmpB= new HashSet<T>();
				for (T x : a)
					if (b.contains(x))
						tmpB.add(x);
				//Result is the difference of tmpSets
				Set<T> tmp = new HashSet<T>(tmpA);
				tmp.removeAll(tmpB);
				return tmp;
			}
		};
	}


	/**
	 * Creates a new Builder of {@link SuppliedSetOperation}
	 * @param first The description of the first set
	 * @return a new Builder.
	 */
	public static <D> Builder<D> newBuilder(D first){
		return new Builder<>(first);
	}



}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.AbstractAction;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

class PathToStringAction extends AbstractAction<Path<NodeItem,Link>,List<String>>{

	public PathToStringAction() {
		super("Prints paths");
	}




	@Override
	protected ActionMechanism<Path<NodeItem, Link>, List<String>> create(String key) {
		return new PathToStringMechanism();
	}
	
	
	
	
	private class PathToStringMechanism implements ActionMechanism<Path<NodeItem, Link>,List<String>>{

		private final List<String> list = new ArrayList<>();
		
		@Override
		public void performAction(Path<NodeItem, Link> i) throws DataAccessException {
			System.out.println(i.toString());
			list.add(i.toString());
		}
		
		@Override
		public List<String> get(){
			return list;
		}
		
	}
	
	
	

}

package org.pickcellslab.foundationj.queryui.renderers;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Component;
import java.util.Objects;

import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.label.WebLabel;

/**
 * A {@link ListCellRenderer} which allows to display {@link MetaModel} in a concise form
 * 
 * @author Guillaume
 *
 */
public class MetaRenderer implements ListCellRenderer<MetaModel> {
	
	private final Logger log = LoggerFactory.getLogger(MetaRenderer.class);
	private final UITheme theme;
	private final DataRegistry registry;

	public MetaRenderer(UITheme theme, DataRegistry registry) {
		Objects.requireNonNull(theme);
		Objects.requireNonNull(registry);
		this.theme = theme;
		this.registry = registry;
	}


	@Override
	public Component getListCellRendererComponent(JList<? extends MetaModel> list, MetaModel value,
			int index, boolean isSelected, boolean cellHasFocus) {


		//Assign a different icon for each MetaItem type
		WebLabel label = new WebLabel();
		label.setOpaque(true);


		if(value == null){
			label.setText("   NONE");
		}
		else{

			try{

				label.setIcon(MetaToIcon.get(value, registry, theme, 16));

				//Add the name as text
				label.setText(value.toString());

				//Add the tooltip text
				//TooltipManager.setTooltip(label, value.toHTML(),TooltipWay.trailing,0);
				label.setToolTipText(value.toHTML());  
			
			}catch(Exception e){
				// deleted value probably
				log.error("MetaRenderer : value failed to returned valid toHTML code", e);
				label.setText("Error - check the log");
			}

		}

		//Create a small padding
		Border paddingBorder = BorderFactory.createEmptyBorder(3,3,3,3);
		label.setBorder(paddingBorder);


		//Handle hovering
		Color background;
		Color foreground;

		// check if this cell represents the current DnD drop location
		JList.DropLocation dropLocation = list.getDropLocation();
		if (dropLocation != null
				&& !dropLocation.isInsert()
				&& dropLocation.getIndex() == index) {

			background = Color.BLUE;
			foreground = Color.WHITE;

			// check if this cell is selected
		} else if (isSelected) {
			background = Color.GRAY;
			foreground = Color.BLACK;

			// unselected, and not the DnD drop location
		} else if (cellHasFocus) {
			background = Color.RED;
			foreground = Color.WHITE;

			// unselected, and not the DnD drop location
		} 
		else {
			background = Color.WHITE;
			foreground = Color.GRAY;
		};

		label.setBackground(background);
		label.setForeground(foreground);

		return label;
	}


}
package org.pickcellslab.foundationj.services.simplecharts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.services.simplecharts.SeriesChangeListener.SeriesChange;

public class DefaultSimpleChartModel<D> implements SimpleChartModel<D> {

	private final List<SimpleSeries<D>> series = new ArrayList<>();
	private final List<SeriesChangeListener<D>> lstrs = new ArrayList<>();

	@Override
	public void addSeries(SimpleSeries<D> series) {
		Objects.requireNonNull(series);
		this.series.add(series);
		this.fireSeriesChange(SeriesChange.ADDED,series);
	}

	@Override
	public void removeSeries(String Series) {
		Iterator<SimpleSeries<D>> itr = this.series.iterator();
		while(itr.hasNext()){
			SimpleSeries<D> s = itr.next();
			if(s.id().equals(Series)){
				itr.remove();
				this.fireSeriesChange(SeriesChange.DELETED, s);
			}
		}
	}

	@Override
	public List<SimpleSeries<D>> getSeries() {
		return Collections.unmodifiableList(series);
	}

	@Override
	public void clear() {
		series.forEach(s->fireSeriesChange(SeriesChange.DELETED,s));
		series.clear();
	}

	@Override
	public void addSeriesChangeListener(SeriesChangeListener<D> l) {
		if(l!=null)
			lstrs.add(l);
	}

	@Override
	public void removeSeriesChangeListener(SeriesChangeListener<D> l) {
		lstrs.remove(l);
	}


	protected void fireSeriesChange(SeriesChange c, SimpleSeries<D> s){
		lstrs.forEach(l->l.seriesChanged(c, s));
	}

}

package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import org.pickcellslab.foundationj.optim.algorithms.NeighbourFunction;

public class CityTour implements NeighbourFunction<List<Point2D>> {

	
	private List<Point2D> list;
	
	public CityTour(List<Point2D> list){
		this.list = list;
	}
	
	@Override
	public List<Point2D> next(List<Point2D> current, double temp) {
		list = new ArrayList<>(current);
		swap();
		return list;
	}

	
	@Override
	public List<Point2D> init() {
		return list;
	}
	
	private void swap(){
		
		 // Get a random positions in the tour
        int tourPos1 = (int) (list.size() * Math.random());
        int tourPos2 = (int) (list.size() * Math.random());

        // Swap them
        Collections.swap(list, tourPos1, tourPos2);
        
	}

	
	public static Function<List<Point2D>, Double> costFunction(){
		return (points)->{
			double d = 0;
			for(int i = 0; i<points.size(); i++){
				Point2D p1 = points.get(i);
				Point2D p2 = null;
				if(i==points.size()-1){
					p2 = points.get(0);
				}else{
					p2 = points.get(i+1);
				}								
				d+=p1.distance(p2);
			}
			return d;
		};
	}
	
	
}

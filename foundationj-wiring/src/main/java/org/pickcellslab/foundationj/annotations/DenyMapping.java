package org.pickcellslab.foundationj.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Non-data types which need to be stored in the database can be annotated with @{@link Mapping}. This ensures that 
 * the instance tree to be stored remains well controlled since all fields need also to be from class which are annotated with @Mapping.
 * <br>
 * <br> However by using @{@link DenyMapping}, it is also possible to apply an extra layer of control by preventing that
 * the annotated class becomes part of a stored instance graph (This is still possible if a {@link CustomNonDataConverter} is employed)
 *  
 * @author Guillaume Blin
 *
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface DenyMapping {

	
}

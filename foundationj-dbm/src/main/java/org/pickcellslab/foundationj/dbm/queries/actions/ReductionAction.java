package org.pickcellslab.foundationj.dbm.queries.actions;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;

public class ReductionAction<I,O> implements Action<I, O> {

	private final ReductionMechanism<I,O> template;
	private final Map<Object, ReductionMechanism<I,O>> map = new HashMap<>();
	
	public ReductionAction(ReductionMechanism<I,O> template) {
		this.template = template;
	}
	
	@Override
	public ActionMechanism<I,O> createCoordinate(Object key) throws DataAccessException {
		final ReductionMechanism<I,O> novel = template.factor();
		map.put(key, novel);
		return novel;
	}

	@Override
	public Set<Object> coordinates() {
		return map.keySet();
	}

	@Override
	public O createOutput(Object key) throws IllegalArgumentException {
		return map.get(key).get();
	}

	@Override
	public String description() {
		return template.description();
	}

}

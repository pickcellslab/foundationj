package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;

/**
 * Allows an object from the {@link MetaModel} to be hidden, either to remove clutter from a GUI or
 * to prevent an object from being used at a wrong time while still being available at a later more
 * appropriate time.
 * 
 * @author Guillaume Blin
 *
 */
public interface Hideable extends MetaModel{
	
	static final AKey<Boolean> hideKey = AKey.get("Hidden", Boolean.class);
	
	public default void setHidden(boolean hidden){
		this.setAttribute(hideKey, hidden);
	};
	
	public default boolean isHidden(){
		return this.getAttribute(hideKey).orElse(false);
	}
	
}

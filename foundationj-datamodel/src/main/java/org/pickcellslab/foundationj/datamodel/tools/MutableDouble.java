package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

@SuppressWarnings("serial")
public class MutableDouble extends Number{

	private double i;

	public MutableDouble() {
		this(0);
	}

	public MutableDouble(double i) {
		this.i = i;
	}


	public void increment(){
		i++;
	}


	public void decrement(){
		i--;
	}

	public void set(double toAdd){
		i=+toAdd;
	}


	@Override
	public double doubleValue(){
		return i;
	}

	@Override
	public long longValue(){
		return (long)i;
	}

	@Override
	public int intValue(){
		return (int)i;
	}

	@Override
	public float floatValue() {
		return (float)i;
	}



}

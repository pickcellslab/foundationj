package org.pickcellslab.foundationj.ui.wizard;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;

@SuppressWarnings("serial")
public class UserDefinedList<T extends UserDefinable<T>> extends ValidityTogglePanel implements ListSelectionListener, KeyListener {


	private final UITheme theme;
	private final JList<UserDefinable<?>> list;
	private final DefaultListModel<UserDefinable<?>> model;
	private final List<UserDefinedListListener<T>> lstrs = new ArrayList<>();
	private final Supplier<T> factory;
	private final Set<String> names = new HashSet<>();
	private boolean notifyChanges = true;


	public UserDefinedList(UITheme theme, Supplier<T> factory, String newTabsName, boolean allowMorePanes) {

		Objects.requireNonNull(theme,"theme is null");
		this.theme = theme;
		Objects.requireNonNull(factory,"factory is null");
		this.factory = factory;


		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		list = new JList<>();
		model = new DefaultListModel<>();
		list.setModel(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(this);
		list.setCellRenderer(new LabelRenderer());
		list.setToolTipText("Double click to add a dataset, press delete to remove.");
		scrollPane.setViewportView(list);


		model.addElement(Mock.ADD);
		Mock.ADD.setEnabled(allowMorePanes);


		list.addKeyListener(this);
		list.addMouseListener(new MouseAdapter(){



			@Override
			public void mouseClicked(MouseEvent e){

				if(allowMorePanes)

					if(Mock.ADD == list.getSelectedValue() && e.getClickCount()==2){
						create(newTabsName);
					}


				if ( SwingUtilities.isRightMouseButton(e) ){



					int row = list.locationToIndex(e.getPoint());
					list.setSelectedIndex(row);
					UserDefinable<?> selected = list.getSelectedValue();
					if(Mock.ADD!=selected){

						if(allowMorePanes){
							//Create a popup menu
							JPopupMenu menu = new JPopupMenu();
							JMenuItem rnItem = new JMenuItem("Rename...");
							rnItem.addActionListener(l->{
								rename(selected);								
							});
							menu.add(rnItem);

							JMenuItem dupItem = new JMenuItem("Duplicate");
							dupItem.addActionListener(l->{
								create(newTabsName, (T) list.getSelectedValue());
							});
							menu.add(dupItem);
							menu.show(UserDefinedList.this, e.getX(),e.getY());
						}
						else
							rename(selected);

					}
				}
			}

		});

	}


	private void create(String newTabsName, T selectedValue) {

		list.clearSelection();

		String name = null;
		if(newTabsName == null){
			name = JOptionPane.showInputDialog("Give me a name: ", "new...");
			if(name == null)
				return;
			if(name.isEmpty()){
				JOptionPane.showMessageDialog(null, "The name cannot be null!");
				return;
			}
			if(!names.add(name)){
				JOptionPane.showMessageDialog(null, "This name already exists!");
				return;
			}
		}else{
			name = newTabsName + " " + (numberOfElements()+1);
		}

		T t = factory.get();
		t.setName(name);		
		t.copyState(selectedValue);
		t.addFinishListener((s,isDone)->{
			list.repaint();
			fireNextValidityChanged();
		});					
		model.add(0, t);
		lstrs.forEach(l->l.added(t));
		fireNextValidityChanged();
	}


	private void create(String newTabsName){

		list.clearSelection();

		String name = null;
		if(newTabsName == null){
			name = JOptionPane.showInputDialog("Give me a name: ", "new...");
			if(name == null)
				return;
			if(name.isEmpty()){
				JOptionPane.showMessageDialog(null, "The name cannot be null!");
				return;
			}
			if(!names.add(name)){
				JOptionPane.showMessageDialog(null, "This name already exists!");
				return;
			}
		}else{
			name = newTabsName + " " + (numberOfElements()+1);
		}

		T t = factory.get();
		t.setName(name);					
		t.addFinishListener((s,isDone)->{
			list.repaint();
			fireNextValidityChanged();
		});					
		model.add(0, t);
		lstrs.forEach(l->l.added(t));
		fireNextValidityChanged();
	}





	private void rename(UserDefinable selected){
		String name = JOptionPane.showInputDialog("New Name : ", selected.getName());
		names.remove(selected.getName());
		if(name == null)
			return;
		if(name.isEmpty()){
			JOptionPane.showMessageDialog(null, "The name cannot be empty!");
			names.add(selected.getName());
			return;
		}
		if(!names.add(name)){
			JOptionPane.showMessageDialog(null, "This name already exists!");
			names.add(selected.getName());
			return;
		}
		selected.setName(name);
		lstrs.forEach(l->l.renamed((T) selected));
	}




	public void addItem(String name){
		T t = factory.get();
		t.setName(name);					
		t.addFinishListener((s,isDone)->{
			list.repaint();
			fireNextValidityChanged();
		});					
		model.add(0, t);
		lstrs.forEach(l->l.added(t));
	}


	@Override
	public boolean validity() {
		Enumeration<UserDefinable<?>> e = model.elements();
		while(e.hasMoreElements()){
			if(!e.nextElement().isFinished())
				return false;

		}
		return true;
	}


	public int numberOfElements(){
		return model.size()-1;
	}

	@SuppressWarnings("unchecked")
	public void getAll(Collection<T> c){
		Objects.requireNonNull(c);
		Enumeration<UserDefinable<?>> e = model.elements();
		while(e.hasMoreElements()){
			UserDefinable<?> d = e.nextElement();
			if(d!=Mock.ADD)
				c.add((T) d);
		}
	}


	public void addSelectionListener(UserDefinedListListener<T> lst){
		lstrs.add(lst);
	}

	public void removeSelectionListener(UserDefinedListListener<T> lst){
		lstrs.remove(lst);
	}



	public void seSelected(T selected) {
		notifyChanges = false;
		list.setSelectedValue(selected, true);
		notifyChanges = true;
	}



	@SuppressWarnings("unchecked")
	@Override
	public void valueChanged(ListSelectionEvent e) {

		if(!e.getValueIsAdjusting() && notifyChanges){

			UserDefinable ud = list.getSelectedValue();
			if (Mock.ADD != ud && null!=ud){//this is the add label
				// Dispatch selected event
				lstrs.forEach(l->l.selected((T) list.getSelectedValue()));

			}
		}
	}



	@Override
	public void keyTyped(KeyEvent e) {}
	@Override
	public void keyPressed(KeyEvent e) {}


	@SuppressWarnings("unchecked")
	@Override
	public void keyReleased(KeyEvent e) {

		if(e.getKeyCode()== KeyEvent.VK_DELETE){			
			T ud = (T) list.getSelectedValue();
			if(ud!=null && ud != Mock.ADD){
				model.removeElement(ud);
				names.remove(ud.getName());
				lstrs.forEach(l->l.removed(ud));
				fireNextValidityChanged();
			}
		}
	}






	private class LabelRenderer implements ListCellRenderer<UserDefinable>{

		@Override
		public Component getListCellRendererComponent(JList<? extends UserDefinable> list, UserDefinable value, int index,
				boolean isSelected, boolean cellHasFocus) {

			JLabel head = new JLabel();
			head.setOpaque(true);
			head.setHorizontalAlignment(SwingConstants.LEFT);

			if(value!=Mock.ADD){

				head.setText(value.getName());
				if(value.isFinished())
					head.setIcon(theme.icon(IconID.Misc.VALID, 16));
				else
					head.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16));

				if(isSelected)
					head.setBackground(Color.GRAY);
				else
					head.setBackground(Color.WHITE);

			}else{
				head.setText("Add...");				
				head.setBackground(Color.WHITE);
				head.setIcon(theme.icon(IconID.Misc.PLUS_SIGN, 16));	
				head.setEnabled(Mock.ADD.isEnabled());
			}

			return head;
		}
	}


	private enum Mock implements UserDefinable<Mock>{

		ADD;

		private boolean enabled = true;

		@Override
		public void addFinishListener(FinishListener l) {}

		@Override
		public void removeFinishListener(FinishListener l) {}

		@Override
		public String getName() {return null;}

		@Override
		public void setName(String name) {}

		@Override
		public boolean isFinished() {return true;}

		public void setEnabled(boolean b){
			enabled = b;
		}

		public boolean isEnabled(){
			return enabled;
		}

		@Override
		public void copyState(Mock other) {}

	}




}

package org.pickcellslab.foundationj.dbm.queries.builders;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Deletable;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;

public interface DeletionConfig<T extends WritableDataItem & Deletable> {

	
	public AccessChoice<T,QueryRunner<Integer>> completely();
	
	public AccessChoice<T,QueryRunner<Integer>> key(AKey<?> k);

	public AccessChoice<T,QueryRunner<Integer>> keys(Collection<AKey<?>> keys);
	
}

package org.pickcellslab.foundationj.mapping.data;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Constructor;

import org.pickcellslab.foundationj.mapping.exceptions.DataTypeException;


/**
 * 
 * The default {@link InstantiationHook}. To use this for instantiation of your {@link Data} objects,
 * objects must possess a constructor with no arguments (private constructor is fine)
 * 
 * @author Guillaume Blin
 *
 */
public final class DefaultInstantiationHook  implements InstantiationHook{

	private final DataRegistry types;
	
	public DefaultInstantiationHook(DataRegistry types){
		this.types = types;
	}

	@Override
	public Object getDataInstance(DbElement dbObject) throws DataTypeException {
		try{
			Class<?> c = types.classOf(dbObject);
			Constructor<?> constr = c.getDeclaredConstructor();
			constr.setAccessible(true);	
			return constr.newInstance();
		}
		catch(Exception e){
			throw new DataTypeException("Exception while instantiating the DataElement with type "+dbObject.typeId(), e);
		}
	}

}

package org.pickcellslab.foundationj.datamodel.predicates;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.FilteringBehaviour.Behaviour;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;



/**
 * An {@link ExplicitPredicate}  aggregating multiple other ExplicitPredicate
 */
@Mapping(id = "Filter")
@Scope
public final class SimpleFilter<T extends DataItem> implements ExplicitPredicate<T>{

	private final HashSet<String> concernedTypes;
	private final Behaviour behaviour;
	@Supported
	private ExplicitPredicate<? super T> constraints;

	/**
	 * Creates a new SimpleFilter to include or exclude the specified {@link NodeItem#typeId()}s.
	 * Note: if empty, all the classes of DataItem will be included or excluded.
	 * Inclusion or exclusion is determined by the specified Behaviour
	 * 
	 */
	public SimpleFilter(Behaviour behaviour, String... typeIds) { 
		concernedTypes = new HashSet<>();
		for (String s : typeIds) 
			this.concernedTypes.add(s);
		this.behaviour = behaviour;
	}


	Behaviour getBehaviour(){
		return behaviour;
	}

	/**
	 * Adds constraints on the {@link WritableDataItem}s to be included or excluded by this SimpleFilter
	 * @param o The boolean operator determining how the constraints should be added to the existing constraints.
	 * if this is the first time this method is called, then the provided operator may be {@code null} and has no effect
	 * @param p The {@link ExplicitPredicate} imposing the constraint
	 * @return This SimpleFilter
	 */
	public SimpleFilter<T> addConstrainsts(ExplicitOperator<Boolean,Boolean> o, ExplicitPredicate<? super T> p){
		Objects.requireNonNull(p,"predicate is null");
		MappingUtils.exceptionIfInvalid(p);		
		if(constraints != null)
			constraints = constraints.merge(o, (ExplicitPredicate)p);
		else constraints = p;
		return this;
	}

	@Override
	public boolean test(T t) {
				
		boolean typeCheck = concernedTypes.isEmpty() 
				|| Link.class.isAssignableFrom(t.getClass()) && concernedTypes.contains(t.declaredType())
				|| NodeItem.class.isAssignableFrom(t.getClass()) && concernedTypes.contains(t.typeId());
		
		if (typeCheck){				
			// If handled, then test the attributes if an AttributeFilter was set
			if (constraints != null)
				return behaviour.behave(constraints.test(t));
			//If no AttributeFilter was set then return the behaviour to true
			else return behaviour.behave(true);
		}
		//If not in the concernedTypes, return the behaviour to false
		return behaviour.behave(false);
	}


	@Override
	public String description() { 

		String description = "{" + behaviour.name()+" -> ";
		if(concernedTypes.isEmpty())
			description = description + "All Types";
		else {
			int counter = 0;
			for (String s : concernedTypes) {
				if(counter == 0)
					description = description+ "\"" +trimToSimpleName(s)+"\"";
				else
					description = description+"; \""+trimToSimpleName(s)+"\"";
				counter++;
			}
		}

		if(constraints != null)
			description = description + " WHERE " + constraints.description(); 


		return description+"}";
	}
	
	
	public String toString(){
		return description();
	}
	

	private String trimToSimpleName(String s){
		String trimmed = null;
		int i = s.lastIndexOf(".");
		if(i!=-1)
		trimmed = s.substring(i+1, s.length())+"s";	
		else trimmed = s;
		return trimmed;
	}


}

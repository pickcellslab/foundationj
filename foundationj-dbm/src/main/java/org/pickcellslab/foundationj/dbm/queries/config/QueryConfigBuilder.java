package org.pickcellslab.foundationj.dbm.queries.config;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public interface QueryConfigBuilder<T,D> {

	/**
	 * Determines the numbers of queries the user can create, the default is only one
	 * @param min
	 * @param max
	 * @return The updated Builder
	 * @throws IllegalArgumentException if min or max <= 0 and if max<min
	 */
	public QueryConfigBuilder<T,D> numberOfQueriesAllowed(int min, int max);

	/**
	 * Regardless of the number of queries the user creates each query will only return one group
	 * @return The updated builder
	 */
	public QueryConfigBuilder<T,D> disableGrouping();
	

	/**
	 * Builds the config
	 * @return The {@link QueryWizardConfig}
	 */
	public QueryWizardConfig<T,D> build();




	
}

package org.pickcellslab.foundationj.datamodel.reductions;

import org.apache.commons.math3.stat.descriptive.StorelessUnivariateStatistic;
import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;


@Mapping(id="SingleStatistics")
@Scope
public class SingleStatistics<T extends Number> implements ExplicitReduction<T, Double>{

	private final StatsType statsType;
	
	@Ignored
	private StorelessUnivariateStatistic stats;

	public SingleStatistics(StatsType stats) {
		this.statsType = stats;
	}
	
	
	@Override
	public void root(T root, boolean included) {
		if(included)
			include(root);		
	}

	

	@Override
	public void include(T data) {
		if(stats==null)
			stats=statsType.get();
		if(data != null) {
			double d = data.doubleValue();
			if(!Double.isNaN(d))
				stats.increment(d);
		}
	}

	@Override
	public Double getResult() {
		if(stats==null)
			stats=statsType.get();
		return stats.getResult();
	}

	

	@Override
	public SingleStatistics<T> factor() {
		return new SingleStatistics<>(statsType);
	}


	@Override
	public Class<Double> getReturnType() {
		return double.class;
	}

	@Override
	public String description() {
		return statsType.name();
	}

	
	public static <I,N extends Number> ExplicitReduction<I, Double> of(ExplicitFunction<I,N> function, StatsType stats){
		return new AdaptedReduction<>(function, new SingleStatistics<>(stats));
	}
	
}

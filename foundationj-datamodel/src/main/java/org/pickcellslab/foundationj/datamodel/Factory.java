package org.pickcellslab.foundationj.datamodel;

/**
 * Implementing classes can create new instances of their own type
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 */
public interface Factory<T extends Factory<T>> {

	/**
	 * @return A new instance with the same class as this object.
	 */
	public T factor();
	
}

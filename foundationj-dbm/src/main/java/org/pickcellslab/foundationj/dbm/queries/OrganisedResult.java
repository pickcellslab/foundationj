package org.pickcellslab.foundationj.dbm.queries;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class OrganisedResult<K,O> implements ResultAccess<K,O> {

	private final Map<K, O> content;
	private List<K> list;
	
	public OrganisedResult(Map<K,O> content){
		this.content = content;
	}
	
	
	public Set<K> entries(){
		return content.keySet();
	}
	
	
	public O get(K entry){
		return content.get(entry);
	}


	@Override
	public int size() {
		list = new ArrayList<>(content.keySet());
		return content.size();
	}


	@Override
	public K getKey(int i) {
		return list.get(i);
	}


	@Override
	public O getResult(int i) {
		return content.get(list.get(i));
	}


	@Override
	public O getResult(K k) {
		return content.get(k);
	}
	
}

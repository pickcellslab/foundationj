package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

/**
 * 
 * Defines elements of the {@link MetaModel} which are not automatically created when data are written to the database.
 * Basically every MetaModel object which are not either {@link MetaClass}, {@link MetaLink} or {@link MetaKey}.
 * <br><br>
 * NB: Implementing classes must use outgoing links with type {@link MetaModel#requiresLink} to declare their
 * dependencies to other {@link ManuallyStored} objects and <u>outgoing</u> links with any type for any other purpose.
 * 
 * @author Guillaume Blin
 *
 */
@Tag(creator = "MetaModel",
description = "MetaModel object which are not automatically created when data are written to the database",
		tag = "ManuallyStored")
public interface ManuallyStored extends MetaModel {

	
	/**
	 * @return A {@link TraverserConstraints} defining the graph unit to be considered to properly store this {@link MetaModel} object
	 */
	public default TraverserConstraints storageConstraints() {
		return Traversers.newConstraints().fromMinDepth().toMaxDepth()
				.traverseOnly(Direction.OUTGOING)
				.withOneEvaluation(P.isSubType(ManuallyStored.class),
						Decision.INCLUDE_AND_CONTINUE, Decision.INCLUDE_AND_STOP);
	}
	

	@Override
	public default TraversalDefinition deletionDefinition() {
		
		final ExplicitPredicate<NodeItem> checkIfThisIsSource = 
				F.streamLinks(Direction.INCOMING,MetaModel.requiresLink)
				.filter(
						P.sourcePasses(
								F.select(MetaModel.recognition)
								.equalsTo(getAttribute(MetaModel.recognition).get())))
				.count()
				.equalsTo(1l);
		
		return TraversalDefinition.newDefinition()
				.startFrom(typeId())
				.having(MetaModel.recognition, getAttribute(MetaModel.recognition).get())
				.fromMinDepth().toMaxDepth()
				.traverseLink(MetaModel.requiresLink, Direction.BOTH)
				.withEvaluations(
						P.isSubType(ManuallyStored.class).negate(),			// Exclude anything that is not Manually Stored			
						Decision.EXCLUDE_AND_STOP,
						Decision.INCLUDE_AND_CONTINUE)
				.addEvaluation(				
						P.isSubType(MetaAutoDeletable.class).negate()	    // Exclude downstream objects which are not autodeletable
						.and(checkIfThisIsSource)
						, Decision.EXCLUDE_AND_STOP)
				.addEvaluation(				
						P.isSubType(MetaAutoDeletable.class)
						.and(checkIfThisIsSource)
						.and(
								F.streamLinks(Direction.INCOMING,MetaModel.requiresLink)
								.count()
								.isInferiorOrEqualTo(1)
								)
						,
						Decision.INCLUDE_AND_STOP)
				.lastEvaluation(											// Delete encountered autodeletable only if one object depends on it
						P.isSubType(MetaAutoDeletable.class)
						.and(
								F.streamLinks(Direction.INCOMING,MetaModel.requiresLink)
								.count()
								.isSuperiorTo(1)
								)
						,
						Decision.EXCLUDE_AND_STOP)
				.done()
				.build();		
	};
	
}

package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;



/**
 * This parameterised class is designed to force a convention regarding the data
 * that can be included in the Attributes of {@link DataItem} objects. It allows
 * type safe handling of attributes and also forbids the storage of arbitrary
 * objects. The reason for this is to ensure that the Persistence Module can safely
 * handle the attribute whatever the persistence system it uses. Furthermore,
 * it ensures that the data remain in a standardised form which can be retrieved
 * without the need of custom java classes, meaning that data should be able to
 * outlive the application.
 * 
 * @author Guillaume Blin
 * 
 * @param <T>
 */

@Mapping(id = "Key", converter=AKeyConverter.class)
public final class AKey<T> {

	//private static final Map<String, Map<String, AKey<?>>> instances = new HashMap<>();//new ConcurrentHashMap<>(16, 0.9f, 1);
	private static final Map<Class<?>,Class<?>> boxing = new HashMap<>(7);

	static{
		boxing.put(byte.class, Byte.class);
		boxing.put(short.class, Short.class);
		boxing.put(int.class, Integer.class);
		boxing.put(long.class, Long.class);
		boxing.put(float.class, Float.class);
		boxing.put(double.class, Double.class);
		boxing.put(boolean.class, Boolean.class);

	}

	private static final Map<String,Class<?>> aliases = new HashMap<>();

	static{
		aliases.put(byte.class.getSimpleName(), byte.class);
		aliases.put(short.class.getSimpleName(), short.class);
		aliases.put(int.class.getSimpleName(), int.class);
		aliases.put(long.class.getSimpleName(), long.class);
		aliases.put(float.class.getSimpleName(), float.class);
		aliases.put(double.class.getSimpleName(), double.class);
		aliases.put(boolean.class.getSimpleName(), boolean.class);
		aliases.put(char.class.getSimpleName(), char.class);
		aliases.put(String.class.getSimpleName(), String.class);

		aliases.put(Byte.class.getSimpleName(), byte.class);
		aliases.put(Short.class.getSimpleName(), short.class);
		aliases.put(Integer.class.getSimpleName(), int.class);
		aliases.put(Long.class.getSimpleName(), long.class);
		aliases.put(Float.class.getSimpleName(), float.class);
		aliases.put(Double.class.getSimpleName(), double.class);
		aliases.put(Boolean.class.getSimpleName(), boolean.class);


		aliases.put(byte[].class.getSimpleName(), byte[].class);
		aliases.put(short[].class.getSimpleName(), short[].class);
		aliases.put(int[].class.getSimpleName(), int[].class);
		aliases.put(long[].class.getSimpleName(), long[].class);
		aliases.put(float[].class.getSimpleName(), float[].class);
		aliases.put(double[].class.getSimpleName(), double[].class);
		aliases.put(boolean[].class.getSimpleName(), boolean[].class);
		aliases.put(String[].class.getSimpleName(), String[].class);

	}



	/**
	 * The name of the attribute this key is associated with
	 */
	public final String name;
	public final String clazz;



	/**
	 * This enum is a convenience to determine a sort of "generic" type of the data associated
	 * with an AKey object
	 */
	public enum dType{
		NUMERIC, BOOLEAN, TEXT, MIXED;

		public static dType valueOf(Class<?> clazz){

			if(clazz.isArray())
				clazz = clazz.getComponentType();

			if(clazz.isPrimitive())
				clazz = boxing.get(clazz);

			if(Number.class.isAssignableFrom(clazz))
				return dType.NUMERIC;
			if(clazz == boolean.class || Boolean.class.isAssignableFrom(clazz))
				return dType.BOOLEAN;
			else
				return dType.TEXT;  
		}		
	}







	private AKey(  String name,   Class<T> clazz) {
		this.name = name;
		this.clazz = clazz.getSimpleName();
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AKey other = (AKey) obj;
		if (clazz == null) {
			if (other.clazz != null)
				return false;
		} else if (!clazz.equals(other.clazz))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return name + ":" + clazz;
	}


	/**
	 * A factory method for AKey objects 
	 * @return The instance of the AKey for the given name and type. 
	 */
	/**
	 * @param name The name of the desired AKey. NB: ':' in the name is forbidden
	 * @param type The Class of of the attribute the AKey to be returned points to.
	 * @return The desired AKey instance.
	 * @throws IllegalArgumentException if the name contains ':' or if the type is not supported.
	 */
	public final static <E> AKey<E> get(String name, Class<E> type) throws IllegalArgumentException {

		Objects.requireNonNull(type);
		
		@SuppressWarnings("unchecked")
		final Class<E> prim = (Class<E>) aliases.get(type.getSimpleName());

		if(prim == null)
			throw new IllegalArgumentException("The type " + type.getName()
			+ " cannot be used, Primitives, Primitive arrays, String and String arrays or boxed types are allowed");

		if(name.contains(":"))
			throw new IllegalArgumentException("\":\" is forbidden in the name of AKey - name is "+name);

		return new AKey<E>(name, prim);

	}

	/**
	 * This is a factory method that returns an AKey instance. 
	 * The specified keyDefinition must follow the following rule:
	 * "name:alias" where name can be any String devoid of ':' character and alias 
	 * id the {@link Class#getSimpleName()} of the class (primitive class) when
	 * a primitive class is available.
	 * 
	 * @return The instance of the AKey for the given keyDefinition
	 */
	public static AKey<?> get(String keyDefinition) throws IllegalArgumentException{
		String[] s = keyDefinition.split("[:]");
		try {
			return get(s[0],s[1]);
		}catch(IndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Bad key definition "+keyDefinition);
		}
	}




	public static   AKey<?> get(String name, String classAlias) throws IllegalArgumentException{


		try {
			final Class<?> alias = aliases.get(classAlias);
			//System.out.println("Alias = "+alias.getSimpleName());
			if(alias!=null)
				return get(name, alias);
			else //Legacy -> Important for MetaKeys
				return get(name, Class.forName(classAlias));

		}
		catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("The class defined by "+classAlias+" does no exist.");
		}
		catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalArgumentException(name+":"+classAlias+" does not follow the convention.");
		}


	}




	/**
	 * @return The dType of the attribute associated with this key
	 */
	public dType dType(){
		return dType.valueOf(type());
	}



	/**
	 * @return The class of the attribute this key points at
	 */
	@SuppressWarnings("unchecked")
	public Class<T> type(){
		return (Class<T>) aliases.get(clazz);
	}



	/**
	 * @param clazz The class to assess
	 * @return The boxed class of the given primitive class or the class itself if it is not a primitive
	 */
	public static Class<?> noPrimitiveClass(Class<?> clazz){
		if(clazz.isPrimitive())
			return boxing.get(clazz);
		else
			return clazz;
	}



	public String asString(WritableDataItem i){

		if (null == i)
			return "null";

		T o = i.getAttribute(this).get();

		return asString(o);
	}






	public String asString(T o){

		if(null == o)
			if(dType() == dType.NUMERIC)
				return "NaN";
			else
				return "N/A";

		if(type().isArray()){			

			int l = Array.getLength(o)-1;
			if(l==-1)
				return "Empty";
			String s = "[";
			for(int x = 0; x<l; x++)
				s+=Array.get(o,x)+", ";
			s+=Array.get(o,l)+"]";
			return s;
		}
		else
			return o.toString();

	}






	public static <T extends DataItem> String asString(T i, ExplicitFunction<T,?> f){

		if (null == i)
			return "null";

		Object o = f.apply(i);

		if(null == o)
			if(dType.valueOf(f.getReturnType()) == dType.NUMERIC)
				return "NaN";
			else
				return "N/A";

		if(o.getClass().isArray()){		
			int l = Array.getLength(o)-1;
			if(l==-1)
				return "Empty";	
			String s = "[";
			for(int x = 0; x<l; x++)
				s+=Array.get(o,x)+", ";
			s+=Array.get(o,l)+"]";
			return s;
		}
		else
			return o.toString();

	}











	/**
	 * A Helper method to deal with 2d matrices to be stored in an attribute.
	 * Attributes only accept 1d array of primitive types. This method will "unfold" a matrix
	 * into a 1d array.
	 * @param t The matrix to store
	 * @return The resulting 1d array
	 * @throws IllegalArgumentException If the matrix is not 2d or the component type is not a primitive type
	 * 
	 * @see #restoreMatrix(Object, int)
	 */
	public static <A> A to1dArray(A[] t) throws IllegalArgumentException{

		Class<?> comp = t.getClass().getComponentType().getComponentType();
		if(!comp.isPrimitive())
			throw new IllegalArgumentException("The argument must be an array of primitive arrays");

		int l = Array.getLength(t[0]);

		@SuppressWarnings("unchecked")
		A concat = (A) Array.newInstance(comp,t.length*l);
		for(int i = 0; i<t.length; i++){
			for(int j = 0; j<l; j++){
				Array.set(concat, i*l + j, Array.get(t[i],j));
			}
		}	

		return concat;
	}

	/**
	 * A Helper method to deal with 2d matrices which were stored in an attribute as a 1d array.
	 * This method restores the matrix from a 1d array
	 * TODO generalise to higher dimensions
	 * @param concat The 1d array to restore into a matrix
	 * @param d The number of dimensions (columns)
	 * @return the corresponding matrix
	 * @throws IllegalArgumentException if the provided class is not a 1d array of primitive
	 * 
	 * @see #to1dArray(Object[])
	 */
	public static <A> A[] restoreMatrix(A concat, int d) throws IllegalArgumentException{

		Class<?> comp = concat.getClass().getComponentType();
		if(!comp.isPrimitive())
			throw new IllegalArgumentException("The argument must be an array of primitives");

		int l = Array.getLength(concat)/d;

		@SuppressWarnings("unchecked")
		A[] t = (A[]) Array.newInstance(comp,d,l);
		for(int i = 0; i<d; i++){
			@SuppressWarnings("unchecked")
			A ins = (A) Array.newInstance(comp,l);
			for(int j = 0; j<l; j++){
				Array.set(ins,j,Array.get(concat,  i*l + j));
			}
			Array.set(t, i, ins);
		}	

		return t;
	}




}

package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * A {@link Fitter} capable of generating a {@link Distribution} fitting a sample dataset
 * 
 * @author Guillaume Blin
 *
 * @param <D> The type of the data to be fitted
 * @param <O> The type of options required to configure the fitting process using this fitter
 * @param <R> The type of {@link Distribution}  this fitter can generate
 */

public interface DistributionFitter<D,O,R extends Distribution<D>> extends Fitter<List<D>,O,R>{

	
	/**
	 * Generates a random sample from the provided distribution
	 * @param size The size of the sample to generate
	 * @return The generated sample
	 */
	public D[] sample(R distribution, int size);
	
	
}

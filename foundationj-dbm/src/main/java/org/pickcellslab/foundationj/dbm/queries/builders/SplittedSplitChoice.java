package org.pickcellslab.foundationj.dbm.queries.builders;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;

public interface SplittedSplitChoice<T,K,O> {

public QueryRunner<OrganisedResult<Pair<K,String>,O>> furtherGroupBy(ExplicitFunction<T,String> categorical);
	
	public QueryRunner<OrganisedResult<Pair<K,Integer>,O>> furtherGroupUsing(ExplicitFunction<T,Integer> categorical);
	
	public CategoryAdder<QueryRunner<OrganisedResult<Pair<K,ExplicitPredicate<? super T>>,O>>,T> createFurtherCategories(ExplicitPredicate<? super T> filter);
	
	public QueryRunner<OrganisedResult<Pair<K,Boolean>,O>> furtherSplitInto2Groups(ExplicitPredicate<? super T> test);

	public <J> QueryRunner<OrganisedResult<Pair<K,J>,O>> splitFurtherUsing(ExplicitSplit<T,J> splitter);
	
	
}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;

class StatsActionBuilder<T extends WritableDataItem> extends ActionBuilderImpl<T, Map<AKey<?>,SummaryStatistics>>{


	private final TargetType<?,?,T> target;
	private final Map<AKey<?>, StatsWrap<? super T,?>> todoList = new HashMap<>();

	StatsActionBuilder(TargetType<?,?,T> target){
		this.target = target;
	}


	public <N extends Number> StatsActionBuilder<T> withEntry(AKey<N> key){
		Objects.requireNonNull(key);
		if(todoList.containsKey(key))
			throw new IllegalStateException("The given key is already registered");
		todoList.put(key, new StatsWrap<>(F.select(key, null)));
		return this;
	}

	public <N extends Number> StatsActionBuilder<T> withEntry(AKey<N> key, ExplicitFunction<? super T,N> f){
		Objects.requireNonNull(key, "The given AKey cannot be null");
		Objects.requireNonNull(f, "The given DataFunction cannot be null");
		if(todoList.containsKey(key))
			throw new IllegalStateException("The given key is already registered");
		todoList.put(key,new StatsWrap<>(f));
		return this;
	}


	@Override
	protected TargetType<?,?,T> getType() {
		return target;
	}

	@Override
	protected Action<T, Map<AKey<?>,SummaryStatistics>> getAction() {
		return new StatsAction<T>(todoList);
	}
	
	
	
}

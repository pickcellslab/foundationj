package org.pickcellslab.foundationj.dbm.queries;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "MultiPredicateGrouping")
@WithService(DataAccess.class)
public class ToStringFilterSetSplitter<E> implements ExplicitSplit<E,String> {

	private final String d;
	@Supported
	private final ExplicitPredicate<? super E>[] predicates;
	private final String[] groups;

	@Ignored
	private Map<ExplicitPredicate<? super E>, String> map;


	public ToStringFilterSetSplitter(String description, ExplicitPredicate<? super E>[] predicates, String[] names) {
		Objects.requireNonNull(predicates);
		for(ExplicitPredicate<? super E> p : predicates)
			MappingUtils.exceptionIfNullOrInvalid(p);
		this.d = description;
		this.predicates = predicates;
		this.groups = names;
	}


	@Override
	public String description() {
		return d;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public String getKeyFor(E e) {

		if(map==null) {
			map = new HashMap<>();
			for(int i = 0; i<predicates.length; i++)
				map.put(predicates[i], groups[i]);
		}

		for(Predicate p : predicates){
			if(p.test(e))
				return map.get(p);
		}

		return ExplicitSplit.OTHER;
	}

}

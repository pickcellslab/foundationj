package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.ManuallyStored;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.queryui.utils.Comparators;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.HighlightablePanel;
import org.pickcellslab.foundationj.ui.wizard.PluggableSelection;
import org.pickcellslab.foundationj.ui.wizard.Wizard;
import org.pickcellslab.foundationj.ui.wizard.WizardFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Panel which displays selectable {@link MetaModel} Objects and automatically updates itself when the MetaModel is modified.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of MetaModel object handled by the panel
 * @see ChoicePanels
 */
@SuppressWarnings("serial")
public class ChoicePanel<T extends MetaModel> extends HighlightablePanel implements MetaModelListener, Supplier<T>{

	private static Logger log = LoggerFactory.getLogger(ChoicePanel.class);

	private static final Color normalColor = Color.BLACK;
	private static final Color highlightColor = Color.GREEN;

	private final PluggableSelection<T> selectionPanel;
	private final Predicate<MetaModel> p;



	/**
	 * Creates a new ChoicePanel with the following options:
	 * @param access The DataAccess which may emit updates
	 * @param clazz The Class of MetaModel which can be chosen from this PanelChoice
	 * @param alreadyRegenerated A list of MetaModel objects to display in the choices. The panel will trust this list as up to date
	 * and will not attempt to regenerate further objects. (if null, then The ChoicePanel will regenerate all the objects passing the given
	 * predicate which are present in the database and offer them as available choices).
	 * @param p A {@link Predicate} on the objects which can be chosen (maybe null)
	 * @param factory A Factory for Wizards which can allow the user to create object of type clazz
	 * When the wizard returns successfully, the object will be saved to the database (if null, the function will be disabled). 
	 * NB, this wizard should only be used with {@link ManuallyStored} objects.
	 * @param title The title to be displayed on the panel
	 * @throws Exception If anything happens while attempting to read the database
	 */
	@SuppressWarnings("unchecked")
	public ChoicePanel(UITheme theme, DataAccess access, Class<T> clazz, List<T> alreadyRegenerated, Predicate<? super T> p, WizardFactory<T> factory, String title) throws Exception {

		Objects.requireNonNull(access,"Access is null");
		Objects.requireNonNull(clazz,"Clazz is null");
		Objects.requireNonNull(title,"Title is null");

		//Check that the clazz is ManuallyStored if factory is not null
		/*
		if(factory!=null && ManuallyStored.class.isAssignableFrom(clazz))
			throw new IllegalArgumentException("factory is not null and "+clazz.getSimpleName()+" does not implement ManuallyStored (which is the only type of object of the MetaModel which can be stored "
					+ "into the database independently of data creation)");
		 */
		

		access.addMetaListener(this);



		if(p!=null){
			this.p = (i)-> clazz.isAssignableFrom(i.getClass()) && p.test((T) i);
		}
		else
			this.p = (i)-> clazz.isAssignableFrom(i.getClass());



			if(alreadyRegenerated == null){
				alreadyRegenerated = access.metaModel().getMetaObjects(clazz).filter(this.p).collect(Collectors.toList());
				System.out.println("Number of objects in ChoicePanels : "+alreadyRegenerated.size());
			}




			selectionPanel = new PluggableSelection<>(title, Comparators.createOptions());
			selectionPanel.setRenderer(new MetaRenderer(theme, access.dataRegistry()));		

			log.debug("Number of MetaModel objects to display: "+alreadyRegenerated.size());

			alreadyRegenerated.forEach(i->selectionPanel.add(i));
			setLayout(new BorderLayout(0, 0));

			this.add(selectionPanel,BorderLayout.CENTER);

			if(factory!=null){
				JButton btnNew = new JButton("New...");
				btnNew.addActionListener(l-> {
					Wizard.showWizardDialog(factory.create()).ifPresent(t->{
						//Save with constraints to avoid saving the dataset along with filters etc
						new Thread(()->{
							try {
								access.queryFactory().meta((ManuallyStored) t).run();
							} catch (Exception e) {
								log.error("Unable to save the MetaModel",e);							
							}
						}).start();
					});
				});
				add(btnNew, BorderLayout.SOUTH);
			}

			if(!alreadyRegenerated.isEmpty())
				setSelected(alreadyRegenerated.get(0));

	}

	
	
	

	public void setSelected(T data) {
		selectionPanel.setSelected(data);		
	}



	/**
	 * Sets the selection Mode of this ChoicePanel
	 * @param selectionMode One of {@link ListSelectionModel#SINGLE_SELECTION} or {@link ListSelectionModel#MULTIPLE_INTERVAL_SELECTION}
	 */
	public void setSelectionMode(int selectionMode){
		selectionPanel.setSelectionMode(selectionMode);
	}

	public Optional<T> getSelected(){
		return Optional.ofNullable(selectionPanel.getSelectedItem());
	}


	public Optional<List<T>> getAllSelected(){
		return Optional.ofNullable(selectionPanel.getSelectedItems());
	}


	public void removeSelectionListener(ListSelectionListener l){
		selectionPanel.removeSelectionListener(l);
	}

	public void addSelectionListener(ListSelectionListener l){
		selectionPanel.addSelectionListener(l);
	}


	public void addFilter(Predicate<? super T> predicate){
		selectionPanel.addFilter(predicate);
	}

	public void removeFilters(){
		selectionPanel.removeFilters();
	}


	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {
		SwingUtilities.invokeLater(
				()->{
					meta.getAllTargets().stream()
					.forEach(m->{
						if(evt == MetaChange.CREATED && ((Predicate)p).test(m)){
							selectionPanel.add((T) m);
							setSelected((T) m);
						}
						else if(evt == MetaChange.DELETED){
							selectionPanel.remove((T)m);
							if(selectionPanel.numElements()!=0)
								setSelected(selectionPanel.getElementAt(0));
						}
					});
					/*
					meta.getAllDependencies().stream()
					.forEach(m->{
						if(evt == MetaChange.CREATED && ((Predicate)p).test(m)){
							selectionPanel.add((T) m);
							setSelected((T) m);
						}
						else if(evt == MetaChange.DELETED){
							selectionPanel.remove((T)m);
							if(selectionPanel.numElements()!=0)
								setSelected(selectionPanel.getElementAt(0));
						}
					});
					*/
				}
				);
	}


	@Override
	public T get() {
		return selectionPanel.getSelectedItem();
	}



	private void highlight() {
		new Thread(()->{	
			for (int i = 0; i<3; i++){
				try {
					selectionPanel.setBorderColor(highlightColor);				
					Thread.sleep(200);				
					selectionPanel.setBorderColor(normalColor);
					Thread.sleep(200);	
					selectionPanel.setBorderColor(highlightColor);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}


	@Override
	public void setHighlighted(boolean highlighted) {
		if(highlighted)
			highlight();
		else
			selectionPanel.setBorderColor(normalColor);

		this.fireHighlighted(highlighted);
	}




}
package org.pickcellslab.foundationj.datamodel.functions;

import java.util.Collections;
import java.util.Iterator;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;

@Mapping(id = "NodesFromTraversal")
@Scope
public class TraversalToNodesIterator implements ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> {

	@Override
	public Iterator<NodeItem> apply(Traversal<NodeItem, Link> t) {
		return t==null ? Collections.emptyIterator() : t.nodes().iterator();
	}

	@Override
	public String description() {
		return "nodes";
	}

	@Override
	public Class<Iterator<? extends DataItem>> getReturnType() {
		return (Class)Iterator.class;
	}

}

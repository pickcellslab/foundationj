package org.pickcellslab.foundationj.dbm.queries;

import java.util.Arrays;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "BinningByKey")
@WithService(DataAccess.class)
public class ToStringKeyBinsSplitter<E> implements ExplicitSplit<E,String>{

	private final double[] values;
	@Supported
	private final ExplicitFunction<E, ? extends Number> f;
	
	
	public ToStringKeyBinsSplitter(final double[] values, final ExplicitFunction<E,? extends Number> f) {
		MappingUtils.exceptionIfNullOrInvalid(f);
		this.values = values;
		this.f = f;
	}
	

	@Override
	public String description() {
		return "Group objects by bins on "+f.toString()+" , bins: "+Arrays.toString(values);
	}


	@Override
	public String getKeyFor(E i) {

		Number n = f.apply(i);
		if(null == n)
			return "NA";

		double m = n.doubleValue();
		double low = Double.NaN; 
		for(double d : values){
			if(m<d)
				break;
			else
				low = d;				
		}
		if(Double.isNaN(low))
			return "<"+values[0];
		else
			return ">="+low;

	}

}

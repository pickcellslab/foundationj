package org.pickcellslab.foundationj.processors;

import java.lang.annotation.Annotation;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic.Kind;


final class ProcessorsUtils {


	private ProcessorsUtils(){}


	static boolean checkConcrete(Class<? extends Annotation> annotation, TypeElement type, ProcessingEnvironment processingEnv) {

		if(!type.getKind().isClass()) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "@"+annotation.getSimpleName()+" can only be used on a concrete class",type);
			return false;
		}
		// Create another error if it is used on an anonymous class
		else if(type.getSimpleName().length()==0) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "@"+annotation.getSimpleName()+" cannot be used on anonymous class",type);
			return false;
		}
		// Create another error if the class is abstract
		else if(type.getModifiers().contains(Modifier.ABSTRACT)) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "@"+annotation.getSimpleName()+" cannot be used on an abstract class",type);
			return false;
		}

		return true;
	}



	static boolean checkAbstract(Class<? extends Annotation> annotation, TypeElement type, ProcessingEnvironment processingEnv) {

		if(type.getKind().isClass()) {
			// Is it abstract
			if(!type.getModifiers().contains(Modifier.ABSTRACT)) {
				processingEnv.getMessager().printMessage(Kind.ERROR, "@"+annotation.getSimpleName()+" can only be used on abstract class or interface",type);
				return false;
			}
		}
		// Is it an interface
		else if(!type.getKind().isInterface()) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "@"+annotation.getSimpleName()+" can only be used on abstract class or interface",type);
			return false;
		}

		return true;
	}




	/**
	 * @param annotation The annotation to search for
	 * @param type The {@link TypeElement} to start from
	 * @param processingEnv
	 * @return An interface found either directly implemented of in the supertype hierarchy
	 * which is annotated with the specified annotation. If none was found, null is returned.
	 */
	static TypeElement findOneInterfaceWithAnnotation(Class<? extends Annotation> annotation, TypeElement type, ProcessingEnvironment processingEnv) {

		TypeElement found = null;

		// Queue to go up the tree if necessary
		final Deque<TypeElement> queue = new LinkedList<>();


		//Check directly implemented interfaces
		for(TypeMirror itrfc : type.getInterfaces()){	
			final TypeElement itrElement = (TypeElement) processingEnv.getTypeUtils().asElement(itrfc);			
			if(itrElement.getAnnotation(annotation)!=null) {
				found = itrElement;
				break;				
			}
			queue.push(itrElement);
		}

		// Add the superclass to the queue if it exists
		TypeMirror tm = type.getSuperclass();
		if(tm.getKind() != TypeKind.NONE) {
			queue.push( (TypeElement) processingEnv.getTypeUtils().asElement(tm) );
		}


		if(found == null) { // If not directly implemented climb up the tree

			while(!queue.isEmpty()){

				TypeElement parent = queue.poll(); // this can be either class or interface



				for(TypeMirror itrfc : parent.getInterfaces()){
					final TypeElement itrElement = (TypeElement) processingEnv.getTypeUtils().asElement(itrfc);					
					if(itrElement.getAnnotation(annotation)!=null) {
						found = itrElement;
						break;
					}
					queue.push(itrElement);
				}
				if(found != null)
					break;

				// Add the superclass to the queue if it exists
				tm = parent.getSuperclass();
				if(tm.getKind() != TypeKind.NONE) {
					queue.push( (TypeElement) processingEnv.getTypeUtils().asElement(tm) );
				}

			}

		}

		return found;
	}



	/**
	 * @param annotation The annotation to search for
	 * @param type The {@link TypeElement} to start from
	 * @param processingEnv
	 * @return A Set containing all the interfaces (or abstract super types) found either directly implemented or in the supertype hierarchy
	 * which are annotated with the specified annotation. If none was found, an empty set is returned.
	 */
	static Set<TypeElement> findAllInterfacesWithAnnotation(Class<? extends Annotation> annotation, TypeElement type, ProcessingEnvironment processingEnv) {

		final Set<TypeElement> found = new HashSet<>();

		// Queue to go up the tree if necessary
		final Deque<TypeElement> queue = new LinkedList<>();
		queue.add(type);
		
		do {
			TypeElement parent = queue.poll(); // this can be either class or interface
			
			//Check directly implemented interfaces
			for(TypeMirror itrfc : parent.getInterfaces()){
				final TypeElement itrElement = (TypeElement) processingEnv.getTypeUtils().asElement(itrfc);					
				if(itrElement.getAnnotation(annotation)!=null) {
					found.add(itrElement);
				}
				queue.push(itrElement);
			}

			// Add the superclass to the queue if it exists
			final TypeMirror abstractTm = parent.getSuperclass();
			if(abstractTm.getKind() != TypeKind.NONE) {
				final TypeElement itrElement = (TypeElement) processingEnv.getTypeUtils().asElement(abstractTm);					
				if(itrElement.getAnnotation(annotation)!=null) {
					found.add(itrElement);
				}
				queue.push( (TypeElement) processingEnv.getTypeUtils().asElement(abstractTm) );
			}
		}
		while(!queue.isEmpty());

		return found;
	}


	public static void main(String[] args) {
		System.out.println(String.class.getCanonicalName());
	}




}

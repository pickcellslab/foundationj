package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="PathGroupingByValue")
class PathSplitterCategorical<V,E,K> implements ExplicitPathSplit<V,E,K> {


	private final AKey<K> k;
	@Ignored
	private FjAdaptersFactory<V, E> f;
	@Supported
	private final ExplicitPredicate<? super NodeItem> predicate;

	public PathSplitterCategorical( ExplicitPredicate<? super NodeItem> p, AKey<K> k) {
		Objects.requireNonNull(k,"The provided Key is null");
		MappingUtils.exceptionIfNullOrInvalid(p);
		this.k = k;
		this.predicate = p;
		//this.target = target;
	}




	@Override
	public String description() {
		return "group paths by "+k.name+" on "+predicate.description();
	}


	@Override
	public void setFactory(FjAdaptersFactory<V,E> factory){
		this.f = factory;
	}


	
/*



	@Override
	public <T> Splitter<PathTraverser<V, E>, T, K> toSplitter(Function<Path<V, E>, T> pathTranslation) {
		return new Splitter<PathTraverser<V,E>,T,K>(){

			@Override
			public String description() {
				return "Sort targets according to "+k.name+" in the path";
			}

			@Override
			public Map<K, Iterable<T>> split(PathTraverser<V, E> it) {
				
				Map<K, List<Path<V,E>>> map = new HashMap<>();

				// Iterate over Paths
				Iterator<Path<V,E>> itr = it.paths();
				while(itr.hasNext()){
					
					Path<V,E> p = itr.next();
					
					// Sort target according to categoricalkey
					Iterator<V> ns = p.nodes();
					while(ns.hasNext()){
						NodeItem i = f.getNodeReadOnly(ns.next());
						if(predicate.test(i)){
							K o = i.getAttribute(k).orElse(null);
							List<Path<V,E>> list = map.get(o);
							if(null == list){
								list = new ArrayList<>();
								map.put(o, list);
							}
							list.add(p);
						}
					}
				}
				return (Map)map;				
			}
			
		};

		
		
	}




	@Override
	public <T, S> Splitter<PathTraverser<V, E>, T, Pair<K, S>> toSplitter(Function<Path<V, E>, T> pathTranslation,
			Splitter<T, T, S> furtherSplit) {
		
		return new Splitter<PathTraverser<V,E>,T,Pair<K,S>>(){

			@Override
			public String description() {
				// TODO Auto-generated method stub
				return null;
			}
			

			@Override
			public Map<Pair<K, S>, Iterable<T>> split(PathTraverser<V, E> it) {
				
				Map<Pair<K,S>, List<Path<V,E>>> map = new HashMap<>();

				// Iterate over Paths
				Iterator<Path<V,E>> itr = it.paths();
				while(itr.hasNext()){
					
					Path<V,E> p = itr.next();
					
					// Sort target according to categoricalkey
					Iterator<V> ns = p.nodes();
					while(ns.hasNext()){
						NodeItem i = f.getNodeReadOnly(ns.next());
						if(predicate.test(i)){
							K o = i.getAttribute(k).orElse(null);
							List<Path<V,E>> list = map.get(o);
							if(null == list){
								list = new ArrayList<>();
								map.put(o, list);
							}
							list.add(p);
						}
					}
				}
				return (Map)map;				
			
				
				
			}
			
		};
	}

*/


	@Override
	public K getKeyFor(Path<V, E> p) {
		
		Iterator<V> ns = p.nodes();
		while(ns.hasNext()){
			NodeItem i = f.getNodeReadOnly(ns.next());
			if(predicate.test(i)){
				return i.getAttribute(k).orElse(null);
			}
		}
		return null;
	}



}

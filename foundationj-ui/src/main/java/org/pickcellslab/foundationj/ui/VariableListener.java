package org.pickcellslab.foundationj.ui;

public interface VariableListener {

	public void valueChanged(Variable<?> source, Object oldValue, Object newValue );
	
}

package org.pickcellslab.foundationj.datamodel.functions;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;

import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.annotations.Mapping;

@Mapping(id = "Distance")
public class DistanceMaker implements ExplicitCombiner<Object,Object,Double>{


	@Override
	public Double combine(Object va, Object vb) {
		if(va == null)
			return Double.NaN;
		
		if(vb == null)
			return Double.NaN;
		
		int vaLength = Array.getLength(va);
		if(vaLength<2)
			return Double.NaN;
		
		if( vaLength != Array.getLength(vb))
			return Double.NaN;
				
		double sum = 0;
        for (int i = 0; i < vaLength; i++) {
            final double dp = ((Number)Array.get(va, i)).doubleValue() - ((Number)Array.get(vb, i)).doubleValue();
            sum += dp * dp;
        }
        return FastMath.sqrt(sum);	
	}


	@Override
	public String description() {
		return "Distance";
	}

	@Override
	public Class<Double> getReturnType() {
		return double.class;
	}


	

}

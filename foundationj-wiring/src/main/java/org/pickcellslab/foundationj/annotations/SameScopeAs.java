package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * Allows the annotated type to inherit the {@link Scope} of other types themselves annotated with @{@link Scope}
 * 
 * @author Guillaume Blin
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface SameScopeAs {

	/**
	 * @return The Interface annotated with {@link Scope}
	 */
	Class<?> value();
	
}

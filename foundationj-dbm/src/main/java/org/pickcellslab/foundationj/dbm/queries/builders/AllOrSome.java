package org.pickcellslab.foundationj.dbm.queries.builders;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.RegenerableDataPointer;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public interface AllOrSome<A, S> {

	/**
	 * Indicates that all objects of the type specified earlier should be considered
	 * @return The next building step
	 * @throws DataAccessException
	 */
	public A getAll() throws DataAccessException;

	/**
	 * Indicates that NOT all objects should be considered. This will then give options
	 * to specify which criteria should be used to select the objects to consider
	 * @return The next building step
	 * @throws DataAccessException
	 */
	public S doNotGetAll();

	/**
	 * Indicates that the objects to select is defined by a provided {@link RegenerableDataPointer}.
	 * @param pointer
	 * @throws DataAccessException 
	 */
	public A followPointer(RegenerableDataPointer pointer) throws DataAccessException;

}

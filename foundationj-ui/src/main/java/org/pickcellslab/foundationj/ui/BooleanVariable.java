package org.pickcellslab.foundationj.ui;

public class BooleanVariable extends AbstractVariable<Boolean> {

	public BooleanVariable(String name, Boolean defaultValue, String description) {
		super(name, defaultValue, description);
	}

	@Override
	protected void check(Boolean newValue) throws IllegalArgumentException {
		// Nothing to do		
	}

}

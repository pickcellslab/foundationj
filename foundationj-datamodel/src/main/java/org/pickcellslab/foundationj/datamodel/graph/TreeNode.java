package org.pickcellslab.foundationj.datamodel.graph;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.math3.util.Pair;

/**
 * 
 * Represents a node in an n-ary tree.
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 */
public interface TreeNode<T extends TreeNode<T>> {

	/**
	 * @return true if this TreeNode is a leaf, false otherwise
	 */
	public default boolean isLeaf(){
		return numChildren() == 0;
	};

	/**
	 * @return true if this TreeNode is a root, false otherwise
	 */
	public default boolean isRoot(){
		return getParent() == null;
	};

	/**
	 * @return The parent of this node
	 */
	public T getParent();

	/**
	 * @return The number of children this node possesses
	 */
	public int numChildren();

	/**
	 * Obtain the child of this node at index i.
	 * @param i The index of the desired child
	 * @return The child at position i
	 */
	public T get(int i);	

	/**
	 * Here ancestor means a node within the upper hierarchy of this node which possesses at least 2 children.
	 * @param node The node to be tested
	 * @return true if the given node is an ancestor of this node, false otherwise.
	 */
	public boolean hasAncestor(T node);

	/**
	 * @return An {@link Iterator} which provide access to the ancestors 
	 * (a node within the upper hierarchy of this node which possesses at least 2 children)
	 * The order of iteration is from lowest to uppest.
	 */
	public default Iterator<T> ancestorIterator(){
		
		@SuppressWarnings("unchecked")
		final T start = (T) this;
		
		return new Iterator<T>(){

			
			private T current = start;

			@Override
			public boolean hasNext() {

				while(current.getParent()!=null)
					if(current.getParent().numChildren()<2)
						current = current.getParent();
					else
						break;
				return current.getParent() != null;			

			}

			@Override
			public T next() {
				current = current.getParent();
				return current;
			}

		};
	};


	/**
	 * @param parent A {@link TreeNode} within the upper hierarchy of the node.
	 * @return The list of {@link TreeNode} to traverse to reach the given parent (including this node and excluding the parent).
	 * If the parent was not found, return an empty list.
	 */
	public default List<T> pathToParent(T parent){
		List<T> path = new ArrayList<>();
		boolean found = false;
		@SuppressWarnings("unchecked")
		T current = (T) this;
		while(current != null){
			path.add(current);
			current = current.getParent();
			if(current == parent){
				found = true;
				break;
			}
		}

		if(found)
			return path;
		else 
			return Collections.emptyList();
	}




	public static <T extends TreeNode<T>> Optional<T> commonAncestor(T n1, T n2){
		//Find LCA
		T ancestor = null;
		Iterator<T> it1 = n1.ancestorIterator();
		while(it1.hasNext()){
			T t = it1.next();
			if(n2.hasAncestor(t)){
				ancestor = t;
				break;
			}
		}
		return Optional.ofNullable(ancestor);
	}



	/**
	 * Computes the Path from the given nodes to the least common ancestor
	 * @param n1 A node in the Tree
	 * @param n2 A Node in the same tree as n1
	 * @return An Empty Optional if a common ancestor is not found, otherwise an Optional which contains 
	 * A Pair with the path from n1 on the left and the path from n2 on the right. The last node in both path will correspond
	 * to the common ancestor.
	 */
	public static <T extends TreeNode<T>> Optional<Pair<List<T>,List<T>>> pathsToCommonAncestor(T n1, T n2){

		//Find LCA
		T ancestor = null;
		Iterator<T> it1 = n1.ancestorIterator();
		while(it1.hasNext()){
			T t = it1.next();
			if(n2.hasAncestor(t)){
				ancestor = t;
				break;
			}
		}

		// If it exists then build the paths
		if(ancestor == null)
			return Optional.empty();

		return Optional.of(new Pair<>(n1.pathToParent(ancestor),n2.pathToParent(ancestor)));
	}




}

package org.pickcellslab.foundationj.bootstrap;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.ScopePolicy;

public class AtScopeSibling extends AbstractAtScope{

	private final AbstractAtScope parentAtScope;
	private final List<ScopeImpl> active = new ArrayList<>();

	public AtScopeSibling(String scope, AbstractAtScope parent) {
		super(scope, parent.scope(), ScopePolicy.SIBLING);
		parentAtScope = parent;
	}

	

	@Override
	public ScopeImpl createAndRegister(final String id, final String parentId) {
		System.out.println("***********");
		System.out.println("AtScopeSibling: Scope "+id+" will be created with parent "+parentId);
		System.out.println("***********");
		// Check that parent is active
		final ScopeImpl parentScope = parentAtScope.activeWithId(parentId).orElse(null);
		
		if(parentScope==null) 
			throw new RuntimeException("parent is inactive: "+parentAtScope.scope());
		
		// Check that this id does not already exists
		if(this.activeWithId(id).isPresent())
			throw new RuntimeException("The id "+id+" already exists for scope "+scope());
		
		System.out.println("AtScopeSibling: Scope "+id+" created with parent "+parentId);		
		
		final ScopeImpl novel = new ScopeImpl(scope, id, parentScope, policy, comps);
		active.add(novel);
		
		return novel;		
	}


	
	
	@Override
	public ScopeImpl remove(String id) {
		final ScopeImpl toRm = activeWithId(id).orElse(null);
		
		if(!active.remove(toRm)) throw new RuntimeException("scope with id "+id+" was never activated and cannot be removed");
		
		System.out.println("AtScopeSibling: Scope "+id+" removed ! ");
		
		return toRm;
	}

	@Override
	public Stream<ScopeImpl> active() {
		return active.stream();
	}

	@Override
	public Optional<ScopeImpl> activeWithId(String id) {
		return active().filter(s->s.id().equals(id)).findAny();
	}


	@Override
	boolean hierarchyHasComponent(Class<?> comp) {
		// First check if we already contain the serviceImpl
		if(comps.contains(comp))
			return true;
		final AbstractAtScope parent = parentAtScope;
		if(parent != null)
			return parent.hierarchyHasComponent(comp);
		return false;
	}

}

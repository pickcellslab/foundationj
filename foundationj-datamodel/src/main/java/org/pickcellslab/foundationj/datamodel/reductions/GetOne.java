package org.pickcellslab.foundationj.datamodel.reductions;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "GetOne")
public class GetOne<T,R> implements ExplicitReduction<T,R> {

	@Supported
	private final ExplicitFunction<T,R> getter;
	
	@Ignored
	private R value;
		
	public GetOne(ExplicitFunction<T,R> getter) {
		MappingUtils.exceptionIfNullOrInvalid(getter);
		this.getter = getter;
	}
	
	
	@Override
	public ExplicitReduction<T, R> factor() {
		return new GetOne<>(getter);
	}

	@Override
	public void root(T root, boolean included) {
		if(included)
			value = getter.apply(root);
		
	}

	@Override
	public void include(T t) {
		if(value==null)
			value = getter.apply(t);
		
	}

	@Override
	public R getResult() {
		return value;
	}

	@Override
	public Class<R> getReturnType() {
		return (Class<R>) getter.getReturnType();
	}

	@Override
	public String description() {
		return getter.toString();
	}

}

package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * Models a statistical distribution.
 * 
 * @author Guillaume Blin
 *
 */
public interface Distribution<T>{

		
	/**
	 * @return The variables in this distribution
	 */
	int dimensions();
	
	/**
	 * @param coordinates
	 * @return The density of probability at the provided coordinates
	 */
	double density(T coordinates);
	
	
	/**
	 * @return A Summary of the distribution (HTML recommended)
	 */
	public String summary();
	
}

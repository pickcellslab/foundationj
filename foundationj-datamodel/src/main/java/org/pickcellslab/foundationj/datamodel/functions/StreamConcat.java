package org.pickcellslab.foundationj.datamodel.functions;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "Concatenation")
@Scope
public class StreamConcat<I,T> implements ExplicitFunction<I, Stream<T>> {

	@Supported
	private final ExplicitFunction<I,Stream<T>> first; 
	@Supported
	private final ExplicitFunction<I,Stream<T>> second;
	
	
	public StreamConcat(ExplicitFunction<I,Stream<T>> first, ExplicitFunction<I,Stream<T>> second) {
		MappingUtils.exceptionIfNullOrInvalid(first);
		MappingUtils.exceptionIfNullOrInvalid(second);
		this.first = first;
		this.second = second;
	}
	
	
	@Override
	public Stream<T> apply(I t) {
		return Stream.concat(first.apply(t), second.apply(t));
	}

	@Override
	public String description() {
		return " concat ";
	}


	@Override
	public String toString() {
		return " concat ";
	}
	
	@Override
	public Class<Stream<T>> getReturnType() {
		return (Class)Stream.class;
	}

}

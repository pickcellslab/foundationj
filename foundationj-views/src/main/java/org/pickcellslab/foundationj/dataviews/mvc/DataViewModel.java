package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dataviews.events.DataSetChangedListener;

/**
 * The root interface of Models of DataViews. A {@link DataViewModel} contains the actual data to be displayed by the views.
 * Modifications of the data are performed on the model which can then dispatch updates to any registered listener.
 * 
 * @author Guillaume
 * 
 * @param <T> The type of data exchanged between this model and the view
 *
 */
public interface DataViewModel<T> {


	public DataSet<T> getDataSet();
	
	
	public void setDataSet(DataSet<T> dataset);

	/**
	 * Registers the specified listeners into this model
	 * @param lstr The {@link DataSetChangedListener} to register
	 */
	public void addDataEvtListener(DataSetChangedListener<T> lstr);

	/**
	 * Deregisters the specified listeners from this model
	 * @param lstr The {@link DataSetChangedListener} to deregister
	 */
	public void removeDataEvtListener(DataSetChangedListener<T> lstr);



}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.queries.MonolithicResult;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

final class MonolithicSearchQuery<V,E,T,O> extends SearchSeveralQueryImpl<V,E,T,String, O, MonolithicResult<O>> {

	private String Key;


	private final MonolithicSearchDefinition<V,E,T> definition;

	MonolithicSearchQuery(MonolithicSearchDefinition<V,E,T> search, Action<? super T, O> action, String name) {
		super(action);
		this.definition = search;
		if(null == name)
			Key = "Key";
		else 
			Key = name;
	}



	@Override
	public String description() {
		return searchDescription()+" -> "+actionDescription();
	}

	@Override
	public String name() {
		//TODO 
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public String shortDescription() {
		//TODO 
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public String searchDescription() {
		return definition.description();
	}

	@Override
	public String actionDescription() {
		return action.description();
	}


	@Override
	public MonolithicResult<O> createResult(DatabaseAccess<V,E> db) throws DataAccessException {		

		Objects.requireNonNull(db, "The provided DatabaseAccess is null");

		//Initialise the ActionMechanism
		ActionMechanism<? super T,O> m = action.createCoordinate(Key);

		Iterator<T> it = definition.accessor().iterator(db);
		while(it.hasNext()){
			m.performAction(it.next());
		}

		m.lastAction();

		//We only have one action (definition of Monolithic Search) so return now
		return new MonolithicResult<>(action.createOutput(Key));
	}





}

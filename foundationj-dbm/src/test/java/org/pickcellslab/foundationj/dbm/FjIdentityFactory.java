package org.pickcellslab.foundationj.dbm;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.DatabaseNode;
import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.db.UpdatableLink;
import org.pickcellslab.foundationj.dbm.db.UpdatableNode;
import org.pickcellslab.foundationj.dbm.db.UpdateInfo;

/**
 * Used in mocks of {@link DatabaseAccess}
 *
 */
public class FjIdentityFactory implements FjAdaptersFactory<NodeItem, Link> {

	@Override
	public DatabaseNode getNodeReadOnly(NodeItem v) {
		return new TestNodeDeletable(v);
	}

	@Override
	public DatabaseNodeDeletable getNodeDeletable(NodeItem v) {
		return new TestNodeDeletable(v);
	}

	@Override
	public Link getLinkReadOnly(Link e) {
		return e;
	}

	@Override
	public Link getLinkDeletable(Link v) {
		return v;
	}

	@Override
	public UpdatableLink getUpdatableLink(Link v, UpdateInfo ui) {
		throw new RuntimeException("Not yet implemented");
	}

	@Override
	public UpdatableNode getUpdatableNode(NodeItem v, UpdateInfo ui) {
		throw new RuntimeException("Not yet implemented");
	}

	

	private class TestNodeDeletable implements DatabaseNodeDeletable{

		private final NodeItem node;
		private final Set<String> tags;
		
		private TestNodeDeletable(NodeItem n) {
			this(n, new HashSet<>());
		}

		private TestNodeDeletable(NodeItem n, Set<String> tags) {
			this.node = n;
			this.tags = tags;
		}

		@Override
		public Iterator<String> getTags() {
			return tags.iterator();
		}

		@Override
		public String typeId() {
			return node.typeId();
		}

		@Override
		public int getDegree(Direction direction, String linkType) {
			return node.getDegree(direction, linkType);
		}

		@Override
		public Stream<Link> getLinks(Direction direction, String... linkTypes) {
			return node.getLinks(direction, linkTypes);
		}

		@Override
		public boolean addLink(Link link) {
			return node.addLink(link);
		}

		@Override
		public boolean removeLink(Link link, boolean updateNeighbour) {
			return node.removeLink(link, updateNeighbour);
		}

		@Override
		public Collection<Link> removeLink(Predicate<Link> predicate, boolean updateNeighbour) {
			return node.removeLink(predicate, updateNeighbour);
		}

		@Override
		public Link addOutgoing(String type, NodeItem target) {
			return node.addOutgoing(type, target);
		}

		@Override
		public Link addIncoming(String type, NodeItem target) {
			return node.addIncoming(type, target);
		}

		@Override
		public <T> void setAttribute(AKey<T> key, T v) {
			node.setAttribute(key, v);
		}

		@Override
		public void removeAttribute(AKey<?> key) {
			node.removeAttribute(key);
		}

		@Override
		public Stream<AKey<?>> minimal() {
			return node.minimal();
		}

		@Override
		public String declaredType() {
			return node.declaredType();
		}

		@Override
		public Stream<AKey<?>> getValidAttributeKeys() {
			return node.getValidAttributeKeys();
		}

		@Override
		public <T> Optional<T> getAttribute(AKey<T> key) {
			return node.getAttribute(key);
		}

		@Override
		public Stream<Link> links() {
			return node.links();
		}

		@Override
		public void delete() {
			System.out.println(node.toString()+" has been deleted");
		}

		@Override
		public void addLabel(String label) {
			tags.add(label);
		}

		@Override
		public boolean hasTag(String tag) {
			return tags.contains(tag);
		}

	}

}

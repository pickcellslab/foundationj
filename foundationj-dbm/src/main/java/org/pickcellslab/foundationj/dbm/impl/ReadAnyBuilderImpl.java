package org.pickcellslab.foundationj.dbm.impl;

import java.util.function.Function;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ReadAnyBuilder;
import org.pickcellslab.foundationj.dbm.queries.actions.AbstractAction;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

public class ReadAnyBuilderImpl<V,E,T> implements ReadAnyBuilder<T> {

	
	private final TargetType<V,E,T> target;

	ReadAnyBuilderImpl(TargetType<V,E,T> target) {
		this.target = target;
	}


	@Override
	public <O> AccessChoice<T, QueryRunner<O>> using(Function<? super T, O> function) {
		return new MonolithicQBuilder<V, E,T,O>(target, new FunctionAction<>(function), "NA");
	}

	
	

	private class FunctionAction<O> extends AbstractAction<T,O>{

		private final Function<? super T,O> f;

		public FunctionAction(Function<? super T,O> f) {
			super("NA");
			this.f = f;
		}

		@Override
		protected ActionMechanism<T, O> create(String key) {
			return new ActionMechanism<T, O>() {

				private O result;

				@Override
				public void performAction(T i) throws DataAccessException {
					result = f.apply(i);
				}

				@Override
				public O get() {
					return result;
				}

			};
		}

	}


}

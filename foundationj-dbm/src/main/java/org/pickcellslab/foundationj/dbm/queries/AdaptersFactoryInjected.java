package org.pickcellslab.foundationj.dbm.queries;

import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;

public interface AdaptersFactoryInjected<V,E> {

	void setFactory(FjAdaptersFactory<V,E> f);
	
	
}

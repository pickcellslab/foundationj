package org.pickcellslab.foundationj.dbm.queries;

import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "DimensionGrouping")
@WithService(DataAccess.class)
public class ToStringDimensionSplitter<E> implements ExplicitSplit<E,String>{

	@Supported
	private final Dimension<E,?> dim;
	
	public ToStringDimensionSplitter(Dimension<E,?> dim) {
		MappingUtils.exceptionIfNullOrInvalid(dim);
		this.dim = dim;
	}
	
	
	@Override
	public String description() {
		return "Group by "+dim.name();
	}



	@Override
	public String getKeyFor(E i) {
		return Objects.toString(dim.apply(i));
	}
	
	
	
}

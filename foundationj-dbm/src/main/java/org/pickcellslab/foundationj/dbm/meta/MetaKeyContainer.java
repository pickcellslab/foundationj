package org.pickcellslab.foundationj.dbm.meta;

import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;

public interface MetaKeyContainer extends MetaModel{

	/**
	 * @param k
	 * @param <K> The type of value the provided AKey points at
	 * @return An {@link Optional} holding the {@link MetaKey} describing the specified AKey object.
	 * If this {@link MetaKeyContainer> does not possess the specified AKey, then an empty optional is returned.
	 */
	public <K> Optional<MetaKey<K>> metaKey(AKey<K> k);

	/**
	 * @return a {@link Stream} with all the {@link MetaKey} this MetaKeyContainer owns
	 */
	public Stream<MetaKey<?>> keys();


	public boolean hasKey(AKey<?> centroid);
	
	
	
}

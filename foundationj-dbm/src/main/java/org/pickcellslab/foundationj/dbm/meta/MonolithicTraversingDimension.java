package org.pickcellslab.foundationj.dbm.meta;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "GraphNavigatingFunction")
@WithService(DataAccess.class)
class MonolithicTraversingDimension<T> implements CachingDimension<T>{


	private final boolean targetLinks;		
	private final TraversalDefinition traversalDescription;
	@Supported
	private ExplicitReduction<DataItem,?> reductionOperation;
	@Supported
	private final ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> traversalToIteration;
	private final String name, description;

	private final AKey<?> generatedKey;
	private final int lengthOfReturnedValue;

	// Caching ----------------------
	@Ignored
	private int useCaching = 0;
	@Ignored
	private Map<String,Object> cache;
	@Ignored
	private TraverserConstraints tc;



	MonolithicTraversingDimension(
			String name, String description,
			TraversalDefinition td, boolean targetLinks,
			MetaReductionOperation fctry,
			ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> traversalToIterator) {
		
		
		MappingUtils.exceptionIfNullOrInvalid(traversalToIterator);
		
		this.traversalDescription = td;
		this.targetLinks = targetLinks;
		this.reductionOperation = fctry.toReductionOperation();
		this.name = name;
		this.description = description;
		this.traversalToIteration = traversalToIterator;
		this.lengthOfReturnedValue = fctry.lengthOfReturnedValue();
		this.generatedKey = fctry.generatedKey();
	}

	@Override
	public dType dataType() {
		return generatedKey.dType();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class getReturnType() {
		return generatedKey.type();
	}

	@Override
	public Object apply(DataItem t) {	

		if(null==cache)
			cache = new HashMap<>();

		Object result = cache.get(t.toString());
		//System.out.println(t.toString());
		if(result!=null)
			return result;

		if(!(t instanceof NodeItem))
			return null;		

		if(tc==null)
			tc=traversalDescription.asConstraints(targetLinks);

		if(!tc.accept(0))
			reductionOperation.root(t, false);
		traversalToIteration.apply(Traversers.breadthfirst((NodeItem)t,tc).traverse()).forEachRemaining(i->reductionOperation.include(i));

		result = reductionOperation.getResult();			
		if(useCaching>1)
			cache.put(t.toString(), result);

		reductionOperation = reductionOperation.factor();

		return result;
	}


	@Override
	public int index() {
		return -1;
	}

	@Override
	public int length() {
		return getReturnType().isArray() ? -1 : numDimensions();
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String getName(int index) {
		if(lengthOfReturnedValue == 1)
			return name;
		return name+" "+index;
	}

	@Override
	public String info() {
		return description();
	}

	@Override
	public String description() {
		return description;
	}
	
	@Override
	public String description(int index) {
		if(lengthOfReturnedValue == 1)
			return description;
		return description+" "+index;
	};

	@Override
	public String toString(){
		return name()+" (Function)";
	}

	@Override
	public Dimension<DataItem, ?> getRaw() {
		return this;
	}

	@Override
	public int numDimensions() {
		return lengthOfReturnedValue;
	}

	@Override
	public Dimension<DataItem, T> getDimension(int index) throws ArrayIndexOutOfBoundsException {

		int nd = numDimensions();
		if(nd <= index)
			throw new ArrayIndexOutOfBoundsException("Provided Index "+index+" ; num Dims : "+nd);

		if(nd != 1)
			useCaching++;

		return new CachedDimension<>(this, index);

	}


}
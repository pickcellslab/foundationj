package org.pickcellslab.foundationj.services.theme;

/**
 * Represents an id for an icon obtained from {@link UITheme}
 * 
 * @author Guillaume Blin
 * 
 * @see App
 * @see Arrows
 * @see Charts
 * @see Data
 * @see Files
 * @see Levels
 * @see Misc
 * @see Queries
 *
 */
public interface IconID {
	
	
	/**
	 * Application IconIDs (App icon, users, database...)
	 */
	public enum App implements IconID{
		APP, DATABASE, ADMIN, USER, SHARE, TEAM
	}
	
	/**
	 * IconIDs for data types
	 */
	public enum Data implements IconID{
		NODE, LINK,
		INCOMING, OUTGOING, BOTH,
		INCLUDE_STOP, INCLUDE_CONTINUE, EXCLUDE_STOP, EXCLUDE_CONTINUE,
		NUMERIC, BOOLEAN, TEXT, MISC
	}
	
	/**
	 * IconIDs for arrows
	 */
	public enum Arrows implements IconID{
		ARROW_DOWN, ARROW_UP, ARROW_LEFT, ARROW_RIGHT
	}
	
	/**
	 * IconIDs for queries (reduction, filtering, grouping...)
	 */
	public enum Queries implements IconID{
		FILTER, GROUPING, SORT_ASC, SORT_DESC, NO_SPLIT, SPLIT, ONE_GROUP, MULTIPLE_GROUPS, PATH
	}
	
	/**
	 * IconIDs for files
	 */
	public enum Files implements IconID{
		EXPORT, IMPORT, SAVE, DELETE, DOWNLOAD, UPLOAD
	}
	
	public enum Levels implements IconID{
		DEBUG, ERROR, INFO, WARN,
	}

	/**
	 * IconIDs for charts
	 */
	public enum Charts implements IconID{
		CHART_AREA, CHART_BAR, CHART_LINE, CHART_PIE, AXES, FIT, HISTOGRAM
	}

	/**
	 * Misc. IconIDs
	 */
	public enum Misc implements IconID{
		AT,
		BINOCULARS, BOOK, 
		CALENDAR, CAMERA, CHECK, CLIPBOARD,
		CLOUD, CLOCK, CLONE, CANCEL, COG, COGS, 
		ENVELOPE, ENVELOPE_OPEN, ENVELOPE_SQUARE, ERASER,  EXPAND, 
		START_FLAG, FINISH_FLAG, FLASK,		
		PAINT, PICK_ONE, PICK_SEVERAL, PIPETTE,		
		DATA_TABLE, THUMBS_UP, TAG,
		UNDO,
		QUESTION,
		VALID, IN_PROGRESS, PLUS_SIGN, MAGIC, BATTERY_HALF, CIRCLE,					
	}

}

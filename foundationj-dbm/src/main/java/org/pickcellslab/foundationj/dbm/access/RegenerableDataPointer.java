package org.pickcellslab.foundationj.dbm.access;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.AllOrSome;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludeExcludeKeyInit;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

/**
 * 
 * A {@link DataPointer} which also offers the possibility to regenerate items it points at
 * 
 * @author Guillaume Blin
 *
 */
public interface RegenerableDataPointer extends DataPointer {

	
	/**
	 * Initialise the building of a 'regenerate' query for objects this pointer points at
	 * @param access The {@link DataAccess} providing access to the database
	 * @return The next step in the query building process
	 */
	public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>>
	initRegeneration(DataAccess access);
	
	/**
	 * Configures the last steps in the building process of a regenerate query.
	 * <br><br>
	 * For simplicity, use instead {@link AllOrSome#followPointer(RegenerableDataPointer)}.
	 * @param choice
	 * @return The result of the query
	 * @throws DataAccessException 
	 */
	public RegeneratedItems finaliseRegeneration(AccessChoice<NodeItem, QueryRunner<RegeneratedItems>> choice) throws DataAccessException;
	
	
	
	@Override
	public RegenerableDataPointer subset(ExplicitPredicate<DataItem> predicate);
	
}

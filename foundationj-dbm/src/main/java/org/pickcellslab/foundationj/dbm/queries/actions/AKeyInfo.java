package org.pickcellslab.foundationj.dbm.queries.actions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;

/**
 * 
 * An immutable object storing the description and the length of the returned array for a given {@link AKey} (-1 if the return type
 * is not an array).
 * 
 * @author Guillaume Blin
 *
 */
//@Mapping(id="AKeyInfo")
public final class AKeyInfo {

	private final AKey<?> k;
	private final String description;
	private final int length;
	private final MetaQueryable ownerTypeId;
	
	/**
	 * Constructs a new {@link AKeyInfo} for the given {@link AKey} and description, assuming that the returned type for the property
	 * is not an array.
	 * @param k
	 * @param description
	 */
	public AKeyInfo(MetaQueryable ownerTypeId, AKey<?> k, String description){
		this(ownerTypeId, k,description,-1);
	}
	
	/**
	 * Constructs a new {@link AKeyInfo} for the given {@link AKey} with the given description and length.
	 * @param k
	 * @param description
	 */
	public AKeyInfo(MetaQueryable ownerTypeId, AKey<?> k, String description, int length){
		
		Objects.requireNonNull(k, "AKey is null");
		Objects.requireNonNull(description, "Description is null");
		Objects.requireNonNull(ownerTypeId, "owner id is null");
		
		//Check if k is array
		if(k.type().isArray()){
			if(length <=0 )
					throw new IllegalStateException("The AKey defines array attribute you must specify a valid length");
			this.length = length;
		}
		else
			this.length = -1;
		
		this.k = k;
		this.description = description;		
		this.ownerTypeId = ownerTypeId;
	}
	
	
	public MetaQueryable owner() {
		return ownerTypeId;
	}
	
	public AKey<?> getKey(){
		return k;
	}
	
	public String getDescription(){
		return description;
	}
	
	public int getLength(){
		return length;
	}
}

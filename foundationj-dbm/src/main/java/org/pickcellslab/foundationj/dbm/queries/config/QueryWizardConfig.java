package org.pickcellslab.foundationj.dbm.queries.config;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;

/**
 *
 *<HTML> Useful to define constraints on queries.
 * <br>The default configuration is:</br>
 * <ul>
 *<li>{@link #isConsiderAllEnabled()} : true</li>
 *<li>{@link #isPathEnabled()} : true</li>
 *<li>{@link #isGroupingEnabled()} : true</li>
 *<li>{@link #setAllowedQueryables(Predicate)} : null </li>
 *<li>{@link PathMode} : Target</li>
 *<li>{@link #setFixedRootType(MetaClass)} : null</li>
 *<li>{@link #setFixedRootKey(MetaReadable)} : null</li>
 *<li>{@link #isSynchronizeRoots()} : false</li>
 * </ul></HTML
 *
 * @author Guillaume Blin
 *
 */
public class QueryWizardConfig<T,D> {


	//--------------------------------------------------------------------------------------------------------------
	// GENERAL CONFIGURATION
	private String textOrHTML;

	private boolean considerAllEnabled = true;

	private boolean pathEnabled = true;


	private int minQueries = 1;

	private int maxQueries = 2;

	private boolean GroupingEnabled = true;
	private boolean pathSplitAllowed = true;;

	private Predicate<MetaQueryable> allowedQueryables = null;


	//--------------------------------------------------------------------------------------------------------------
	// CONSIDER ALL CONFIGURATION






	//---------------------------------------------------------------------------------------------------------------
	// PATH CONFIGURATION


	/**
	 * When objects from the database are read using a traversal strategy, the result may include either
	 * objects of the target type only or multiple types of objects encountered during the traversal.
	 *
	 * @author Guillaume Blin
	 *
	 */
	public enum PathMode{
		/**
		 * Include target type only
		 */
		TARGET,

		/**
		 * Allow multiple types of objects to be included
		 */
		PATH
	}


	private PathMode pathMode = PathMode.TARGET;

	/**
	 * If non null, Only the given type of objects will be possible as root of a path
	 */
	private MetaClass fixedRootType = null;

	/**
	 * If not null, then only one instance of the root type will be allowed, this instance will be found using the given {@link MetaReadable}
	 */
	private MetaReadable<?> fixedRootKey = null;
	private Object fixedRootKeyValue;


	private boolean synchronizeRoots = false;

	private boolean synchronizeTargets = false;









	QueryWizardConfig(String textOrHTML) {
		if(textOrHTML != null)
			this.textOrHTML = textOrHTML;
		else this.textOrHTML = "";
		this.textOrHTML = "To view available Filters and Attributes, select a Type";
	}






	/**
	 * @return true if searching by considering all elements with a given type is enabled, false otherwise
	 */
	public boolean isConsiderAllEnabled() {
		return considerAllEnabled;
	}

	/**
	 * @return true if searching using a traversal is enabled, false otherwise
	 */
	public boolean isPathEnabled() {
		return pathEnabled;
	}




	public boolean isGroupingEnabled() {
		return GroupingEnabled;
	}

	public boolean isPathSplittingEnabled() {
		return pathSplitAllowed;
	}


	public Predicate<MetaQueryable> getAllowedQueryables() {
		return allowedQueryables;
	}



	public PathMode getPathMode() {
		return pathMode;
	}



	public MetaClass getFixedRootType() {
		return fixedRootType;
	}


	public MetaReadable<?> getFixedRootKey() {
		return fixedRootKey;
	}

	public Object getFixedRootKeyValue() {
		return fixedRootKeyValue;
	}



	public boolean isSynchronizeRoots() {
		return synchronizeRoots;
	}




	public boolean isSynchronizeTargets(){
		return this.synchronizeTargets;
	}


	public int minNumberOfQueries(){
		return minQueries;
	}

	public int maxNumberOfQueries(){
		return maxQueries;
	}


	public String toHTML(){
		return textOrHTML;



	}



	public QueryInfo create(String id){

		Map<Class<?>,Object> map = new HashMap<>();
		map.put(QueryInfo.TARGET, null);
		if(this.fixedRootType!=null)
			map.put(QueryInfo.FIXED_ROOT_TYPE, this.fixedRootType);
		if(this.fixedRootKey!=null)
			map.put(QueryInfo.FIXED_ROOT, null);
		if(pathMode == PathMode.PATH)
			map.put(QueryInfo.INCLUDED,null);

		return new QueryInfo(id,map);
	}







	void considerAllEnabled(boolean considerAllEnabled) {
		this.considerAllEnabled = considerAllEnabled;
	}





	void setPathEnabled(boolean pathEnabled) {
		this.pathEnabled = pathEnabled;
	}




	void setGroupingAllowed(boolean multiSetAllowed) {
		GroupingEnabled = multiSetAllowed;
	}



	void setSplittingAllowed(boolean b) {
		pathSplitAllowed = b;
	}


	void setAllowedQueryables(Predicate<MetaQueryable> allowedQueryables) {
		this.allowedQueryables = allowedQueryables;
	}


	void setPathMode(PathMode pathMode) {
		this.pathMode = pathMode;
	}




	void setFixedRootType(MetaClass fixedRootType) {
		this.fixedRootType = fixedRootType;
	}





	<E> void setFixedRootKey(MetaReadable<E> fixedRootKey, E value) {
		this.setFixedRootType((MetaClass) fixedRootKey.owner());
		this.fixedRootKey = fixedRootKey;
		this.fixedRootKeyValue = value;
	}




	void setSynchronizeRoots(boolean synchronizeRoots) {
		this.synchronizeRoots = synchronizeRoots;
	}


	void setSynchronizeTarget(boolean b) {
		this.synchronizeTargets = b;
	}



	void numberOfQueriesAllowed(int min, int max) {
		minQueries = min;
		maxQueries = max;
	}







}

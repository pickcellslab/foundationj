package org.pickcellslab.foundationj.datamodel.functions;

import java.util.Collections;
import java.util.Iterator;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;

@Mapping(id = "LinksFromTraversal")
public class TraversalToLinksIterator implements ExplicitFunction<Traversal<NodeItem,Link>, Iterator<? extends DataItem>> {

	@Override
	public Iterator<Link> apply(Traversal<NodeItem, Link> t) {
		return t==null ? Collections.emptyIterator() : t.edges().iterator();
	}

	@Override
	public String description() {
		return "links";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class<Iterator<? extends DataItem>> getReturnType() {
		return (Class)Iterator.class;
	}

}
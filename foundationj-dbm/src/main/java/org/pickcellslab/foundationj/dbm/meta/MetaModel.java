package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.annotations.Tag;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

/**
 * The root interface for elements of the MetaModel. The MetaModel is the dynamic representation of self constructed schema within the database.
 * Do not implement directly, if you wish to extend the MetaModel, see {@link MetaItem}.
 * 
 * @author Guillaume Blin
 *
 */
@Tag(tag = "MetaModel", creator = "FoundationJ MetaModel", description = "A tag to group items belonging to the MetaModel")
public interface MetaModel extends NodeItem {

	
	//public static final String TARGET_OF = "IS_TARGET_OF";
	
	//public static final String APPLICABLE_TO = "APPLICABLE_TO";
	
	public final static String keyLink = "HAS_KEY", tagLink = "HAS_TAG", fromLink = "FROM", toLink = "TO", requiresLink = "REQUIRES";

	/**
	 * The key to access the Recognition String of this {@link MetaItem}
	 * @see #recognitionString()
	 */
	public static final AKey<String> recognition = AKey.get("Recognition Sequence", String.class);
	/**
	 * The key to access the name of this DataItem
	 */
	public static AKey<String> name = AKey.get("Name", String.class);
	
	
	public static AKey<String> desc = AKey.get("Description", String.class);
	
	public static AKey<String> creator = AKey.get("Created By", String.class);
	
	
	
	/**
	 * @return A unique ID.
	 * Two MetaItem describing the same object must share the same Recognition String and two MetaItem 
	 * describing two different objects must have distinct recognition strings. This String must not change.
	 */
	public String recognitionString();
	
	/**
	 * @return A description of this MetaModel in HTML format.
	 * 
	 */
	public String toHTML();

	/**
	 * @return The name of this {@link MetaModel} object
	 */
	public String name();
	
	/**
	 * @return The description of this {@link MetaModel} object
	 */
	public String description();

	
	/**
	 * @return A {@link TraversalDefinition} defining which nodes and links should be deleted when deleting this MetaModel from the database
	 */
	public TraversalDefinition deletionDefinition();
	
}

package org.pickcellslab.foundationj.dbm.meta;

import java.util.Collection;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;

/**
 * Provide information regarding MetaModel objects which concerned by a database write operation. 
 * 
 * @author Guillaume Blin
 *
 */
public interface MetaInfo {

	/**
	 * @return A List of the MetaModel concerned by the write operation
	 */
	public List<MetaModel> items();

	/**
	 * @return A Collection of subgraph of the MetaModel concerned by the write operation.
	 */
	public Collection<StepTraverser<NodeItem, Link>> traversers();

	/**
	 * @return A summary of the MetaModel concerned by the write operation
	 */
	public String summarize();

}
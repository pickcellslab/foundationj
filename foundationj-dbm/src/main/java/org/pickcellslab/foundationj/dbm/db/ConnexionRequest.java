package org.pickcellslab.foundationj.dbm.db;

import java.util.Objects;

public class ConnexionRequest {

	private final String dbPath, dbName, userName, password;
	private final Permission permission;

	public enum Permission{
		READ_ONLY, READ_WRITE
	}
	
	
	public ConnexionRequest(String dbPath, String dbName, String userName, String password, Permission permission) {
		Objects.requireNonNull(dbPath, "dbPath is null");
		Objects.requireNonNull(dbName, "dbName is null");
		Objects.requireNonNull(userName, "userName is null");
		Objects.requireNonNull(password, "password is null");
		Objects.requireNonNull(permission, "permission is null");
		if(userName.isEmpty())
			throw new IllegalArgumentException("Please set a user name");
		if(password.isEmpty())
			throw new IllegalArgumentException("Please set a valid password");
		this.dbPath = dbPath;
		this.dbName = dbName;
		this.userName = userName;
		this.password = password;
		this.permission = permission;
	}

	/**
	 * @return The path to the database
	 */
	public String dbPath() {
		return dbPath;
	};
	
	/**
	 * @return The name of the database
	 */
	public String dbName() {
		return dbName;
	};

	/**
	 * @return The user name
	 */
	public String userName() {
		return userName;
	};

	/**
	 * @return The password
	 */
	public String userPassword() {
		return password;
	};

	
	public Permission permission() {
		return permission;
	};
	

}

package org.pickcellslab.foundationj.dbm.queries.config;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;

/**
 * Instances of this class hold information about a query.
 * <br> For example whether a specific Node was used as a root for a traversal or 
 * what types of objects are expected to be included in the query.
 * <br> This information is provided in the form of {@link MetaItem}
 *
 */
public class QueryInfo {

	
	/**
	 * Defines the list of NodeItem instances used as a root in a path
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static final Class<ExplicitPredicate<? super NodeItem>> FIXED_ROOT = (Class)ExplicitPredicate.class;
	
	/**
	 * Defines the MetaClass representing the type of the node chosen as a root of the query
	 */
	public static final Class<MetaClass> FIXED_ROOT_TYPE = MetaClass.class;
	
	/**
	 * Defines the list of {@link MetaClass} expected to be included in the result of the query
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static final Class<List<MetaQueryable>> INCLUDED = (Class)List.class;
	
	/**
	 * Defines the {@link MetaQueryable} targeted by the query.
	 */
	public static final Class<MetaQueryable> TARGET = MetaQueryable.class;
	
	
	private final Map<Class<?>, Object> valid;
	private final String queryId;


	/**
	 * Creates a new {@link QueryInfo} instance with the given queryId and valid entries
	 * @param queryId An identifier for this QueryInfo
	 * @param valid A Map holding the query information. Keys should be 
	 * {@link #TARGET}, {@link #FIXED_ROOT_TYPE}, {@link #FIXED_ROOT} or {@link #INCLUDED}.
	 */
	public QueryInfo(String queryId, Map<Class<?>,Object> valid) {
		this.queryId = queryId;
		this.valid = valid;
	}
	
	public String id(){
		return queryId;
	}
	

	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> i){
		return (T) valid.get(i);
	}
	
	/**
	 * @param k One of {@link #TARGET}, {@link #FIXED_ROOT_TYPE}, {@link #FIXED_ROOT} or {@link #INCLUDED}. 
	 * @param object
	 */
	public <T> void set(Class<T> k, T object){
		if(valid.containsKey(k))
			valid.put(k, object);
		else
			throw new IllegalArgumentException();
	}

	public MetaQueryable getIncludedForClass(String type) {
		MetaQueryable mc = null;
		List<MetaQueryable> list = get(INCLUDED);
		for(MetaQueryable pot : list)
			if(pot.itemDeclaredType().equals(type)){
				mc = pot;
				break;
			}
		
		return mc;
	}
	

}

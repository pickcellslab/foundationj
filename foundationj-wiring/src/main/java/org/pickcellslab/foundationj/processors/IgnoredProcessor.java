package org.pickcellslab.foundationj.processors;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;



public class IgnoredProcessor extends AbstractProcessor {

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		return Collections.singleton(Ignored.class.getName());
	}


	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;


		// Find elements annotated with ignored
		for (VariableElement type : (Collection<VariableElement>)roundEnv.getElementsAnnotatedWith(Ignored.class)) {

			// Simply check that the field is within a class which is annotated with @Mapping
			if(type.getEnclosingElement().getAnnotationsByType(Mapping.class).length == 0)
				processingEnv.getMessager().printMessage(Kind.ERROR, "@Ignored can only be used inside a class which"
						+ " is itself annotated with @Mapping",type);

		}


		return false;
	}




}
package org.pickcellslab.foundationj.datamodel.predicates;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/**
 * An {@link ExplicitPredicate} built upon a given {@link DataDunction} and an existing ExplicitPredicate acting
 * on the result of the DataFunction
 *
 * @param <T>
 */

@Mapping(id = "Predicate")
@Scope
public class FunctionPredicate<T,E> implements ExplicitPredicate<T>{

	@Supported
	private final ExplicitFunction<T,E> function;
	@Supported
	private final ExplicitPredicate<? super E> predicate;

	public FunctionPredicate(ExplicitFunction<T,E> df, ExplicitPredicate<? super E> ep) {
		MappingUtils.exceptionIfNullOrInvalid(df);
		MappingUtils.exceptionIfNullOrInvalid(ep);
		this.function = df;
		this.predicate = ep;
	}
	
	@Override
	public boolean test(T t) {
		return predicate.test(function.apply(t));
	}

	@Override
	public String description() {
		return function.description()+" "+predicate.description();
	}

	@Override
	public String toString() {
		return function.toString()+" "+predicate.toString();
	}
}

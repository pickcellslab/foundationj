package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * 
 * Performs some kind of operation on objects supplied by an {@link OperationSuplier} and returns a result.
 * Definitions of the objects to operate on are held in this {@link SuppliedOperation} and provided to the
 * {@link OperationSupplier}  
 * 
 * @author Guillaume Blin
 *
 * @param <D> Definitions of operands to be supplied by the {@link OperationSuplier}
 * @param <S> The type of the operands
 * @param <R> The type of the output of the operation
 */
public interface SuppliedOperation<D,S,R> {

	
	/**
	 * Performs the operation on operands provided by the given {@link OperationSuplier}
	 * @param s The {@link OperationSuplier} responsible for supplying the operands to the operation
	 * @return The result of the operation
	 * @throws SuppliedOperationException
	 */
	public R operate(OperationSuplier<S,D> s) throws SuppliedOperationException;
	
}

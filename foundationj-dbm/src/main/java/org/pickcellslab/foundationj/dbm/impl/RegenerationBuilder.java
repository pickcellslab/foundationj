package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.EvaluationContainerBuilder;
import org.pickcellslab.foundationj.datamodel.tools.EvaluationContainerInit;
import org.pickcellslab.foundationj.datamodel.tools.ExcludeLinksBased;
import org.pickcellslab.foundationj.datamodel.tools.IncludeLinksBased;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.RegenerableDataPointer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.ReconstructionRule.Policy;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.AllOrSome;
import org.pickcellslab.foundationj.dbm.queries.builders.ExcludeKeyBased;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludeExcludeKeyInit;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludedKeyBased;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

class RegenerationBuilder<V,E> implements 
ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>>>>,
LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>>>, 
ExcludeLinksBased<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>>>, 
IncludeLinksBased<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>>>,
IncludeExcludeKeyInit<AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>>,
AllOrSome<RegeneratedItems,AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>>,
EvaluationContainerBuilder<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>,
IncludedKeyBased<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>, 
ExcludeKeyBased<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>
{



	private final TargetType<V, E, V> target; 

	String description = "";

	int maxDepth = Integer.MAX_VALUE;

	final Map<String, Direction> included = new HashMap<>();

	final Map<ExplicitPredicate<? super NodeItem>, Decision> filters = new HashMap<>();

	boolean includeAllKeys = true;

	final Set<AKey<?>> includedKeys = new HashSet<>();

	Policy linkPolicy;

	boolean includeAllLinks;

	boolean includeAllNodes;

	Policy keyPolicy;

    Decision defaultDecision;



	/**
	 * Initialise a builder for a {@link RegenerationQuery} designed to obtain DataItem of the specified class
	 * @param itemClazz
	 */
	RegenerationBuilder(TargetType<V,E,V> target){
		this.target = target;
	}






	//-------------------------------------------------------------------------------------------------------------------------------
	// Reconstruction rule building methods


	// Until which depth do we regenerate linked objects

	@Override
	public LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> toMaxDepth() {
		return toDepth(Integer.MAX_VALUE);
	}


	@Override
	public LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> toDepth(int max) {
		if(max<0)
			throw new IllegalArgumentException("Negative depth!");
		description+=" Regenerate to depth "+max;
		this.maxDepth = max;
		return this;
	}









	//--------------------------------------
	// Which links do we traverse while regenerating connected objects?





	@Override
	public EvaluationContainerInit<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> traverseAllLinks() {
		linkPolicy = Policy.INCLUDED;
		description+= " traverse all links";
		includeAllLinks = true;
		return this;
	}




	@Override
	public IncludeLinksBased<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> traverseLink(String type,
			Direction d) {		
		Objects.requireNonNull(type,"Null link type!");
		Objects.requireNonNull(d,"Null link Direction!");
		description+= " traverse "+type+" "+d;
		included.put(type, d);
		linkPolicy = Policy.INCLUDED;
		return this;
	}




	@Override
	public ExcludeLinksBased<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> doNotTraverseLink(String type,
			Direction d) {
		Objects.requireNonNull(type,"Null link type!");
		Objects.requireNonNull(d,"Null link Direction!");
		description+= " traverse all except "+type+" "+d;
		included.put(type, d);
		linkPolicy = Policy.EXCLUDED;
		return this;

	}


	@Override
	public IncludeLinksBased<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> and(String type,
			Direction d) {
		Objects.requireNonNull(type,"Null link type!");
		Objects.requireNonNull(d,"Null link Direction!");
		description+= " and "+type+" "+d;
		included.put(type, d);
		return this;
	}






	@Override
	public ExcludeLinksBased<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> asWellAs(String type,
			Direction d) {
		Objects.requireNonNull(type,"Null link type!");
		Objects.requireNonNull(d,"Null link Direction!");
		description+= " and "+type+" "+d;
		included.put(type, d);
		return this;
	}


	
	
	@Override
	public EvaluationContainerInit<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> 
	acceptLinks(BiFunction<Link, Direction, Boolean> biPredicate) {
		throw new RuntimeException("Not implemented yet");
	}
	
	



	//----------------------------------------
	// Which connected objects do we regenerate alongside?



	@Override
	public IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> includeAllNodes() {
		description+= " include all nodes";
		includeAllNodes = true;
		return this;
	}






	@Override
	public EvaluationContainerBuilder<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> withEvaluations(
			ExplicitPredicate<? super NodeItem> explicitPredicate, Decision ifYes, Decision defaultDecision) {		
		Objects.requireNonNull(explicitPredicate,"Null predicate!");
		Objects.requireNonNull(ifYes,"Null if yes decision!");
		Objects.requireNonNull(defaultDecision,"Null if default decision!");
		filters.put(explicitPredicate, ifYes);		
		this.defaultDecision = defaultDecision;
		description+=" "+ifYes+" "+explicitPredicate.description();
		return this;
	}





	@Override
	public IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> withOneEvaluation(
			ExplicitPredicate<? super NodeItem> explicitPredicate, Decision ifYes, Decision ifNot) {
		withEvaluations(explicitPredicate,ifYes, ifNot);
		return this;
	}



	@Override
	public EvaluationContainerBuilder<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>> addEvaluation(
			ExplicitPredicate<? super NodeItem> explicitPredicate, Decision d) {
		withEvaluations(explicitPredicate,d, defaultDecision);
		return this;
	}






	@Override
	public IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> lastEvaluation(
			ExplicitPredicate<? super NodeItem> explicitPredicate, Decision d) {
		withEvaluations(explicitPredicate,d, defaultDecision);
		return this;
	}


	@Override
	public IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> done() {
		return this;
	}










	//--------------------------------------
	// Which keys do we regenerate?




	@Override
	public AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>> regenerateAllKeys() {
		includeAllKeys = true;
		description+=" include all keys";
		return this;
	}






	@Override
	public AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>> includeOneKey(AKey<?> k) {
		Objects.requireNonNull(k,"Null Key!");		
		includeAllKeys = false;
		keyPolicy = Policy.INCLUDED;
		includedKeys.add(k);
		description+=" include "+k.name;
		return this;
	}



	@Override
	public AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>> includeKeys(
			Collection<AKey<?>> keys) {
		
		Objects.requireNonNull(keys,"Null Keys!");		
		includeAllKeys = false;
		keyPolicy = Policy.INCLUDED;
		includedKeys.addAll(keys);
		description+=" include "+keys.size()+" keys";
		return this;
	}
	
	



	@Override
	public IncludedKeyBased<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> includeKeys(AKey<?> k) {
		includeOneKey(k);
		return this;
	}






	@Override
	public AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>> excludeOneKey(AKey<?> k) {
		Objects.requireNonNull(k,"Null Key!");
		if(k == WritableDataItem.idKey)
			throw new IllegalArgumentException("It is forbidden to exclude the database id key");
		includeAllKeys = false;
		keyPolicy = Policy.EXCLUDED;
		includedKeys.add(k);
		description+=" exclude "+k.name;
		return this;
	}





	@Override
	public ExcludeKeyBased<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> excludeKeys(AKey<?> k) {
		excludeOneKey(k);
		return this;
	}
	

	
	
	@Override
	public IncludedKeyBased<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> andKey(AKey<?> k) {
		Objects.requireNonNull(k,"Null Key!");	
		includedKeys.add(k);
		description+=" and "+k.name;
		return this;
	}

	@Override
	public ExcludeKeyBased<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>> asWellAsKey(AKey<?> k) {
		Objects.requireNonNull(k,"Null Key!");	
		includedKeys.add(k);
		description+=" and "+k.name;
		return this;
	}
	
	
	
	
	


	//----------------------------------------------------------------------------------------------------------------------------
	// -> To Search definition


	@Override
	public RegeneratedItems getAll() throws DataAccessException{
		return new RegenerateQBuilder<V,E>(target, build()).run();
	}


	@Override
	public AccessChoice<NodeItem,QueryRunner<RegeneratedItems>> doNotGetAll(){
		return new RegenerateQBuilder<V,E>(target, build());
	}







	private ReconstructionRuleImpl build(){		
		return new ReconstructionRuleImpl(this);
	}






	@Override
	public AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>> doneWithKeys() {
		return this;
	}






	@Override
	public RegeneratedItems followPointer(RegenerableDataPointer pointer) throws DataAccessException {
		return pointer.finaliseRegeneration(new RegenerateQBuilder<V,E>(target, build()));		
	}












	





	














}
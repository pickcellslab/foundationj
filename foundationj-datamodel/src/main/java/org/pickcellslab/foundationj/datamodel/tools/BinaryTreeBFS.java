package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.graph.BinaryTreeNode;

public final class BinaryTreeBFS<T extends BinaryTreeNode<T>> implements Iterable<T>{

	private final T root;

	public BinaryTreeBFS(T root){
		this.root = root;
	}

	
	public List<T> getAllLeaves(){
		final List<T> leaves = new ArrayList<>();
		this.forEach(t->{
			if(t.isLeaf())
				leaves.add(t);
		});
		return leaves;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new BTDFSIterator();
	}

	public Iterator<List<T>> depthAsListIterator(){
		return new ListBFSIterator();
	}






	public class BTDFSIterator implements Iterator<T>{

		private final Deque<T> queue = new LinkedList<>();

		private BTDFSIterator(){
			queue.add(root);
		}

		@Override
		public boolean hasNext() {
			return !queue.isEmpty();
		}

		@Override
		public T next() {			
			T current = queue.removeFirst();
			if(!current.isLeaf()){
				T l = current.getLeft();
				T r = current.getRight();
				if(null != l)
					queue.add(l);
				if(null != r)
					queue.add(r);	
			}
			return current;
		}

	}








	public class ListBFSIterator implements Iterator<List<T>>{

		private final Deque<T> queue = new LinkedList<>();
		private final List<T> next = new ArrayList<>();


		private ListBFSIterator(){
			next.add(root);
		}

		@Override
		public boolean hasNext() {
			return !next.isEmpty();
		}

		@Override
		public List<T> next() {
			queue.addAll(next);
			next.clear();
			while(!queue.isEmpty()){
				T current = queue.removeFirst();
				T l = current.getLeft();
				T r = current.getRight();
				if(null != l)
					next.add(l);
				if(null != r)
					next.add(r);
			}
			return new ArrayList<>(next);
		}

	}





}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.datamodel.tools.Traverser;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.impl.MetaInfoImpl.MetaBuilder;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;

/**
 * A {@link Traverser}  capable of generating the metamodel of the data it traverses
 * 
 * @author Guillaume
 *
 */
class MetaTrackingTraverser implements StepTraverser<NodeItem,Link> {

  private final StepTraverser<NodeItem,Link> traverser;
  private final MetaTracker tracker;

  public MetaTrackingTraverser(NodeItem node, MetaBuilder builder){
    traverser = Traversers.breadthFirst(node);
    tracker = new MetaTracker(builder);
  }
  public MetaTrackingTraverser(NodeItem node, TraverserConstraints constraints, MetaBuilder builder){
    traverser = Traversers.breadthfirst(node, constraints);
    tracker = new MetaTracker(builder);
  }

  @Override
  public TraversalStep<NodeItem,Link> nextStep() {
    TraversalStep<NodeItem,Link> ts = traverser.nextStep();
    if(ts!=null){
      tracker.track(ts.nodes);
      tracker.track(ts.edges);
    }
    return ts;
  }

  @Override
  public DefaultTraversal<NodeItem,Link> traverse() {
    DefaultTraversal<NodeItem,Link> t = (DefaultTraversal<NodeItem, Link>) traverser.traverse();
    tracker.track(t.nodes);
    tracker.track(t.edges);
    return t;
  }


  public MetaInfo getMetaInfo(){
    return tracker.getMetaInfo();
  }
 

}

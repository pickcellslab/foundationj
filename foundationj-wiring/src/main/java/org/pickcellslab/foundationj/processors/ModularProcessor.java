package org.pickcellslab.foundationj.processors;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.annotations.Scope;


public class ModularProcessor extends AbstractProcessor {


	@Override
	public Set<String> getSupportedAnnotationTypes(){
		return Collections.singleton(annotation().getName());
	};



	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;

		final Set<String> data = new HashSet<String>();

		// Find Data elements from the current compilation sources
		for (TypeElement type : (Collection<TypeElement>)roundEnv.getElementsAnnotatedWith(annotation())) {

			// First check that contract is fulfilled
			if(contractRespected(type)) {// Will generate errors if not
				data.add(type.getQualifiedName().toString()); 
				processingEnv.getMessager().printMessage(Kind.NOTE,"FoundationJProcessor: "+type.getSimpleName()+" has been processed by "+getClass().getSimpleName());
			}

		}

		return false;
	}



	protected boolean contractRespected(TypeElement type) {

		//--------------------------------------------------------------------//
		// First make sure that the annotation is on an interface
		//--------------------------------------------------------------------//

		if(!ProcessorsUtils.checkAbstract(Modular.class, type, processingEnv))
			return false;



		//--------------------------------------------------------------------//
		// Ensure that @Scope or @SameScopeAs or @WithService is present
		//--------------------------------------------------------------------//
		if(type.getAnnotation(Scope.class)==null
				&& type.getAnnotation(SameScopeAs.class)==null){
			processingEnv.getMessager().printMessage(Kind.ERROR, "@Modular must also be annotated with either @Scope or @SameScopeAs", type);
			return false;
		}
		

		return true;

	}




	protected Class<Modular> annotation() {
		return Modular.class;
	}




}

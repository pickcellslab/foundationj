package org.pickcellslab.foundationj.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.pickcellslab.foundationj.mapping.data.DefaultInstantiationHook;
import org.pickcellslab.foundationj.mapping.data.InstantiationHook;

/**
 * Specifies that a class defines a data type, i.e. that it is meant to be stored in a database. All such objects
 * also need to be annotated with @{@link Scope}, @{@link SameScopeAs} or @{@link WithService} .
 * <br> <b>NB1:</b> This annotation needs to be used on concrete, non-anonymous classes only.
 * <br> <b>NB2:</b> The annotated class must possess an empty constructor (which can be private) unless a 
 * custom {@link InstantiationHook} is defined
 * <br><b>NB3:</b> @{@link Data} is reserved for <u>data</u> objects. Other elements which are not data per se but still
 * need to be stored in the database need to use {@link Mapping}. 
 *  
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Data {

	/**
	 * In order to increase flexibility and reuse of the database across applications, the specific class of the object
	 * is not written to the database. Instead an id which should be self-explanatory is stored 
	 * (i.e Experiment, User, Image ...), this is the typeId. 
	 */
	String typeId();

	/**
	 * @return The type of {@link InstantiationHook} required to instantiate this type Data object. <b>Must be a concrete implementation</b>
	 */
	Class<? extends InstantiationHook> hook() default DefaultInstantiationHook.class;
	
	
	/**
	 * Defines the relative path to a custom icon to be used to represent this particular data type.
	 * Example: "/icons/custom.png" if the icon is stored in an 'icons' folder within the 'resources' folder.
	 * @return The relative path to the icon to be used for this type of data object
	 */
	String icon() default "";
}

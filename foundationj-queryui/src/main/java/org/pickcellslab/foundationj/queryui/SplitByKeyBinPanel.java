package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class SplitByKeyBinPanel<T extends Supplier<MetaReadable<?>> & Highlightable> extends SplitterUI<SplitByKeyBinPanel<T>> {


	private final UITheme theme;
	private final DataRegistry registry;
	
	private MetaReadable<?> mr;
	private double[] bins;
	private JLabel lblNone;
	private JTextField binField;
	private JButton btnValidate;

	public SplitByKeyBinPanel(UITheme theme, DataRegistry registry, T supplier) {

		this.theme = theme;
		this.registry = registry;
		
		JLabel lblSelectedAttribute = new JLabel("Selected Attribute : ");

		lblNone = new JLabel("None");

		JLabel lblBins = new JLabel("Bins (comas separated)");



		JButton btnChange = new JButton("Change");
		btnChange.addActionListener(l->{
			MetaReadable<?> mr = supplier.get();
			if(null == mr){
				JOptionPane.showMessageDialog(null,"No attribute selected.");
				supplier.setHighlighted(true);
				return;
			}
			if(mr.dataType() != dType.NUMERIC){
				JOptionPane.showMessageDialog(null,"Only numerical attributes are compatible");
				return;
			}			
			if(mr.numDimensions()>1){
				
				JOptionPane.showMessageDialog(null, "Array attributes are not supported");
				return; //TODO propose a selector
			}
			lblNone.setText(mr.toString());
			lblNone.setIcon(MetaToIcon.get(mr, registry, theme, 16));
			this.mr = mr;
			this.fireNextValidityChanged();
		});


		binField = new JTextField();

		JLabel lblNumberOfBins = new JLabel("Number of bins :");


		btnValidate = new JButton("Validate");
		btnValidate.addActionListener(l->{
			System.out.println("Action evt");
			//if(l.getPropertyName() == "value"){

			String[] sBins = binField.getText().split(",");

			if(sBins.length < 2){
				JOptionPane.showMessageDialog(null, "Please add more bins");
				bins = null;
				lblNumberOfBins.setText("Number of bins : invalid");
				fireNextValidityChanged();				
				return;
			}

			try{

				Set<Double> set = new HashSet<>(sBins.length);
				boolean duplicate = false;
				for(int i = 0;i<sBins.length; i++){
					if(!set.add(Double.parseDouble(sBins[i])))
						duplicate = true;
				}
				if(duplicate)
					JOptionPane.showMessageDialog(null, "Duplicates will be ommitted.");

				int c = 0;
				bins = new double[set.size()];
				for(double d : set)
					bins[c++] = d;

			}catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null, "Invalid numbers");
				bins = null;
				lblNumberOfBins.setText("Number of bins : invalid");
				fireNextValidityChanged();
				return;
			}
			Arrays.sort(bins);
			lblNumberOfBins.setText("Number of bins : "+bins.length);
			fireNextValidityChanged();
		});





		//Layout
		//------------------------------------------------------------------------------------------------

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblSelectedAttribute)
								.addComponent(lblBins)
								.addComponent(btnValidate))
						.addGap(27)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(lblNone)
												.addGap(92)
												.addComponent(btnChange))
										.addComponent(binField))
								.addComponent(lblNumberOfBins))
						.addContainerGap(39, Short.MAX_VALUE))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGap(28)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblSelectedAttribute)
								.addComponent(lblNone)
								.addComponent(btnChange))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(binField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
										.addGap(14)
										.addComponent(lblBins)))
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addGap(18)
										.addComponent(btnValidate))
								.addGroup(groupLayout.createSequentialGroup()
										.addGap(23)
										.addComponent(lblNumberOfBins)))
						.addContainerGap(24, Short.MAX_VALUE))
				);
		setLayout(groupLayout);
		//Objects.requireNonNull(supplier);

	}


	@Override
	public boolean validity() {
		return mr!=null && bins!=null;
	}

	@Override
	MetaSplit getSplitter(Meta meta) {
		try {
			return KeyBinsMetaSplit.getOrCreate(meta.getInstanceManager(), mr,bins);
		} catch (InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}


	@Override
	public void copyState(SplitByKeyBinPanel<T> other) {
		this.mr = other.mr;
		if(mr!=null){
			lblNone.setText(mr.toString());
			lblNone.setIcon(MetaToIcon.get(mr, registry, theme, 16));			
			this.binField.setText(other.binField.getText());
			this.btnValidate.doClick();
		}
	}
}

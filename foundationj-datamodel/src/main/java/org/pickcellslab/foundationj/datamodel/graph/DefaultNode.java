package org.pickcellslab.foundationj.datamodel.graph;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class DefaultNode<E extends Edge<? extends Node<E>>> implements Node<E> {

	private Set<E> edges = new HashSet<>();
	
	
	@Override
	public Stream<E> links() {
		return edges.stream();
	}


	@Override
	public boolean addLink(E link) {
		return edges.add(link);
	}

	@Override
	public boolean removeLink(E link, boolean updateNeighbour) {				
		boolean success = edges.contains(link);
		if(!success)
			return false;
		if(updateNeighbour){
			//Note that the edge maybe outgoing, incoming or a self-ref
			if(link.source() != this)
				link.source().removeLink(link, false);
			if(link.target() != this)
				link.target().removeLink(link, false);
		}
		edges.remove(link);
		return success;
	}

	
	


	
	
}

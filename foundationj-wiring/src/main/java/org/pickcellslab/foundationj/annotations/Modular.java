package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Specifies that the annotated interface supports the {@link Module} annotation for its implementing classes.
 * <br><br>
 * <b>NB:</b> @{@link Modular} Interfaces must also be annotated with either {@link SameScopeAs} or {@link Scope}
 * @see Module
 * @see SameScopeAs
 */
@Documented
@Retention(CLASS)
@Target(TYPE)
public @interface Modular {

}

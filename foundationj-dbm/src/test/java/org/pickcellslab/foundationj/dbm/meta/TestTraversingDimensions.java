package org.pickcellslab.foundationj.dbm.meta;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DefaultNodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.functions.KeySelector;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToLinksIterator;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToNodesIterator;
import org.pickcellslab.foundationj.datamodel.reductions.CountOperation;
import org.pickcellslab.foundationj.datamodel.reductions.GetOne;
import org.pickcellslab.foundationj.dbm.NonDataMappingMock;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.queries.ToStringBinarySplitter;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;


@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class TestTraversingDimensions {

	// Key to the name property of our test data
	private final AKey<String> name = AKey.get("Name", String.class);

	// Key to the name property of our test data
	private final AKey<short[]> arrKey = AKey.get("Array", short[].class);

	private final DataRegistry registry = Mockito.mock(DataRegistry.class);

	private final MetaInstanceDirector director = new MetaInstanceDirector(registry);

	//-----------------------------------------------------------------------------------------------//
	// To test our 2 implementations of Traversing Dimensions, we will need: 
	//		- A MetaReductionOperation
	//		- A TraversalDefinition 
	//		- A PredictableMetaSplit for the SplittedTraversingDimension -> MetaBinarySplit
	//-----------------------------------------------------------------------------------------------//


	// Defines the classes that are supported for string conversion
	private final NonDataMappingMock ndm = new NonDataMappingMock(
			new HashSet<>(
					Arrays.asList(CountOperation.class, GetOne.class, KeySelector.class)));

	// TraversalDefinitions
	private final TraversalDefinition depth_1 = 
			TraversalDefinition.newDefinition()
			.startFrom("Label").acceptedBy(P.none()) // root definition does not matter here
			.fromDepth(1).toDepth(1)
			.traverseAllLinks()
			.includeAllNodes()
			.build();

	private final TraversalDefinition depth_2 = 
			TraversalDefinition.newDefinition()
			.startFrom("Label").acceptedBy(P.none()) // root definition does not matter here
			.fromDepth(1).toDepth(2)
			.traverseAllLinks()
			.includeAllNodes()
			.build();


	// 3- PredictableMetaSplitter
	private PredictableMetaSplit split; // we don't have an implementation so we will mock it


	// 4- Data Nodes (see setup())
	final DefaultNodeItem n1 = new DefaultNodeItem();
	final DefaultNodeItem n2 = new DefaultNodeItem();
	final DefaultNodeItem n3 = new DefaultNodeItem();
	final DefaultNodeItem n4 = new DefaultNodeItem();		
	final DefaultNodeItem n5 = new DefaultNodeItem();
	final DefaultNodeItem n6 = new DefaultNodeItem();
	final DefaultNodeItem n7 = new DefaultNodeItem();
	final DefaultNodeItem n8 = new DefaultNodeItem();
	final DefaultNodeItem n9 = new DefaultNodeItem();






	@BeforeAll
	public void setup() {


		/* Create a graph of objects which we will query
		 * 
		 *           n5
		 *           |
		 *    n6 -- n2' - n7'
		 *    /     |   
		 *   n8'    n1 
		 *     \   /  \  
		 *      n3     n4'
		 *      |    
		 *      n9
		 */


		// Add the name
		n1.setAttribute(name, "n1");		n2.setAttribute(name, "n2");
		n3.setAttribute(name, "n3");		n4.setAttribute(name, "n4");
		n5.setAttribute(name, "n5");		n6.setAttribute(name, "n6");
		n7.setAttribute(name, "n7");		n8.setAttribute(name, "n8");
		n9.setAttribute(name, "n9");

		// Add an array attribute
		n1.setAttribute(arrKey, new short[] {2,16,24});		n2.setAttribute(arrKey, new short[] {2,16,24});
		n3.setAttribute(arrKey, new short[] {2,16,24});		n4.setAttribute(arrKey, new short[] {2,16,24});
		n5.setAttribute(arrKey, new short[] {2,16,24});		n6.setAttribute(arrKey, new short[] {2,16,24});
		n7.setAttribute(arrKey, new short[] {2,16,24});		n8.setAttribute(arrKey, new short[] {2,16,24});
		n9.setAttribute(arrKey, new short[] {2,16,24});


		// While testing Splitted versions, we will split based on the presence of an attribute
		// Create the AKey
		final AKey<String> pass = AKey.get("pass", String.class);
		// Add the attribute. NB: In schematic above ' indicates that the node has the pass.
		n2.setAttribute(pass, "pass"); 		n4.setAttribute(pass, "pass");
		n7.setAttribute(pass, "pass");		n8.setAttribute(pass, "pass");


		final String neighbour = "IS_NEIGHBOUR_OF";
		new DataLink(neighbour, n1, n2,  true);		new DataLink(neighbour, n2, n5,  true);
		new DataLink(neighbour, n2, n6,  true);		new DataLink(neighbour, n2, n7,  true);
		new DataLink(neighbour, n1, n3,  true);		new DataLink(neighbour, n1, n4,  true);
		new DataLink(neighbour, n3, n9,  true);		new DataLink(neighbour, n3, n8,  true);
		new DataLink(neighbour, n8, n6,  true);


		// Mock the Splitter
		split = Mockito.mock(PredictableMetaSplit.class);
		Mockito.when(split.toSplitter()).thenReturn(new ToStringBinarySplitter(P.hasKey(pass), "Passed", "Rejected"));
		Mockito.when(split.getPrediction()).thenReturn(new String[] {"Passed", "Rejected"});
		//System.out.println(split.getClass());
	}


	@Test
	public void testMonolithicTraversingDimensionNodes() throws NonDataMappingException, InstantiationHookException {

		final CountOperation<DataItem> counter = new CountOperation<>();

		// Configure data mapping (required by MetaReductionOperation) 
		final String id = "Counter";
		Mockito.when(ndm.convertToString(counter)).thenReturn(id);
		Mockito.when(ndm.restoreFromString(id)).thenReturn(counter);

		MetaReductionOperation mecha = MetaReductionOperation.getOrCreate(director, "Counter", counter);


		MonolithicTraversingDimension<Long> dim = 
				new MonolithicTraversingDimension<>(
						"mono",
						"tested mono dim",
						depth_1,
						false,
						mecha,
						new TraversalToNodesIterator());

		System.out.println((long)dim.apply(n1));
		Assertions.assertTrue((long)dim.apply(n1) == 3);

		System.out.println(dim.getReturnType());
		Assertions.assertTrue(dim.getReturnType() == long.class);

	}


	@Test
	public void testMonolithicTraversingDimensionLinks() throws NonDataMappingException, InstantiationHookException {

		final CountOperation<DataItem> counter = new CountOperation<>();

		// Configure data mapping (required by MetaReductionOperation) 
		final String id = "Counter";
		Mockito.when(ndm.convertToString(counter)).thenReturn(id);
		Mockito.when(ndm.restoreFromString(id)).thenReturn(counter);

		MetaReductionOperation mecha = MetaReductionOperation.getOrCreate(director, "Counter", counter);

		MonolithicTraversingDimension<Long> dim = 
				new MonolithicTraversingDimension<>(
						"mono",
						"tested mono dim",
						depth_1,
						true,
						mecha,
						new TraversalToLinksIterator());

		Assertions.assertTrue((long)dim.apply(n1)==3);

	}


	@Test
	public void testMonolithicTraversingDimensionArrayGet() throws InstantiationHookException, NonDataMappingException {


		// Here we test for the proper return type in the case of a reduction which creates an array attribute

		// We will need a MetaReadable
		MetaReadable mockedMR = mock(MetaReadable.class);
		// with a name
		when(mockedMR.name()).thenReturn("Mocked MetaReadable");

		// And a MetaQueryable (owner of MetaReadable)
		MetaQueryable mockedOwner = mock(MetaQueryable.class);
		// with a name
		when(mockedOwner.name()).thenReturn("Mocked Owner");
		// So when owner() is called we return mockedOwner
		when(mockedMR.owner()).thenReturn(mockedOwner);

		//Finally setup the dimension returned by our MetaReadable
		when(mockedMR.getRaw()).thenReturn(Dimension.create(arrKey, new short[]{0,0,0}));
		// Data Class
		when(mockedMR.dataClass()).thenReturn(short[].class);

		// Configure data mapping (required by MetaReductionOperation)
		final KeySelector<DataItem, short[]> selector = F.select(arrKey);
		final String idSel = "sel";
		Mockito.when(ndm.convertToString(selector)).thenReturn(idSel);
		Mockito.when(ndm.restoreFromString(idSel)).thenReturn(selector);
		
		final GetOne<DataItem, short[]> getOne = new GetOne<>(selector);
		final String idOne = "one";
		Mockito.when(ndm.convertToString(getOne)).thenReturn(idOne);
		Mockito.when(ndm.restoreFromString(idOne)).thenReturn(getOne);


		MetaReductionOperation mecha = MetaReductionOperation.getOrCreate(director, "test", getOne, 3,  mockedMR);


		MonolithicTraversingDimension<Short> dim = 
				new MonolithicTraversingDimension<>(
						"mono",
						"tested mono dim",
						depth_1,
						false,
						mecha,
						new TraversalToNodesIterator());

		final short[] result = (short[]) dim.apply(n1);
		Assertions.assertTrue(result.length==3);
		Assertions.assertTrue(Arrays.equals(result, new short[]{2,16,24}));		
		Assertions.assertTrue(dim.getReturnType() == short[].class);


	}


	/*
//FIXME for the test below we need a Mockito extension so that we can create a mock with @Mapping
	@Test
	public void testSplittedTraversingDimension() {

		SplittedTraversingDimension<Long> dim = 
				new SplittedTraversingDimension<>(
						"mono",
						"tested mono dim",
						depth_1,
						false,
						mecha,
						new TraversalToNodesIterator(), 
						split);

		// We expect 2 passed and 1 rejected
		Long[] result = dim.apply(n1);
		Assertions.assertTrue(result.length == 2);
		Assertions.assertTrue(result[0] == 2);
		Assertions.assertTrue(result[1] == 1);

		Assertions.assertTrue(dim.getReturnType() == long[].class);
		System.out.println(dim.getReturnType());

	}

	 */
}

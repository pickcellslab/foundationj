package org.pickcellslab.foundationj.services.theme;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Service;

@Core
@Service
public interface UITheme {

		
		public Icon icon(IconID choice, int size);
				
		public Icon icon(IconID choice, int size, Color color);
				
		public static Image toImage(Icon icon) {
			if(icon instanceof ImageIcon)
				return ((ImageIcon) icon).getImage();
			final BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
			icon.paintIcon(null, image.getGraphics(), 0, 0);
			return image;
		}
		
		public static ImageIcon toImageIcon(Icon icon) {
			if(icon instanceof ImageIcon)
				return ((ImageIcon) icon);
			return new ImageIcon( toImage(icon) );
		}
		
		public static Icon resize(Icon icon, int w, int h) {
			
			if(icon.getIconHeight() == h && icon.getIconWidth() == w)
				return icon;
			
			final Image img = toImage(icon);
			final Image newimg = img.getScaledInstance( w, h,  java.awt.Image.SCALE_SMOOTH ) ;  
			return new ImageIcon( newimg );
		}
		
				
		
		public Color[] defaultPalette();

				
		
}
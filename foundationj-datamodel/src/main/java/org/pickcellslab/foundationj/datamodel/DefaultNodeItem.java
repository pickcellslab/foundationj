package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Scope;

@Data(typeId = "DefaultNodeItem")
@Scope
public class DefaultNodeItem extends DataNode implements NodeItem{

	private static final AKey<String> nameKey = AKey.get("name",String.class);
	
	
	public DefaultNodeItem() {
		this(null);
	}
	
	public DefaultNodeItem(String name) {
		if(name == null)
			name = "No Name";
		setAttribute(nameKey, name);
	}

	public String name() {
		return (String) attributes.get(nameKey);
	}
	
	@Override
	public Stream<AKey<?>> minimal(){
		return Stream.of(idKey);
	}
	
}

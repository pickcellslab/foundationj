package org.pickcellslab.foundationj.bootstrap;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractAtScope implements AtScope {


	private static final Logger log = LoggerFactory.getLogger(AbstractAtScope.class);

	protected final List<Object> comps = new ArrayList<>();
	protected final String scope, parent;
	protected final ScopePolicy policy;


	public AbstractAtScope(String scope, String parent, ScopePolicy policy) {
		this.scope = scope;
		this.parent = parent;
		this.policy = policy;
		log.debug("AtScope "+scope+" with parent "+Objects.toString(parent)+" and policy "+policy+" created");
	}

	void addComponent(Object comp){
		comps.add(comp);
	}

	void addComponents(List<Object> list) {
		comps.addAll(list);		
	}

	boolean containsComponent(Object comp) {
		return comps.contains(comp) || comps.contains(comp.getClass());
	}

	void takeServices(Map<Class<?>, Class<?>> services) {
		
		// 1- make the list of services that are required by our components (and not already in parents)
		final Map<Class<?>, Class<?>> required = new HashMap<>();
		final LinkedList<Class<?>> queue = new LinkedList<>();
		
		for(Entry<Class<?>, Class<?>> e : services.entrySet()) {
			if(this.requireService(e.getKey(), e.getValue())) {
				required.put(e.getKey(),e.getValue());
				queue.add(e.getValue());// keep the list of serviceImpl whose constructor need to be checked
			}
		}
		
		// 2- Now check dependencies of services to other services		
		while(!queue.isEmpty()) {
			final Class<?> requiredImpl = queue.poll();
			for(Constructor<?> c : requiredImpl.getDeclaredConstructors()) {
				for(Class<?> param : c.getParameterTypes()) {// (Only service interface in constructor)
					Class<?> dependencyImpl = services.get(param);
					if(dependencyImpl!=null && !required.containsKey(param)) { // required dependency probably
						// last check:
						if(!hierarchyHasComponent(dependencyImpl)) {
							required.put(param, dependencyImpl);
							queue.add(dependencyImpl);
						}
					}
				}
			};
		}
		
		// 3- Finally we can add the services as components
		comps.addAll(required.values());
		
	}
	
	
	
	abstract boolean hierarchyHasComponent(Class<?> comp);

	boolean requireService(Class<?> service, Class<?> serviceImpl) {

		// First check if we already contain the serviceImpl
		if(hierarchyHasComponent(serviceImpl))
			return false;

		// If not, then check if any of the components declares the service as dependency
		return comps.stream().anyMatch((comp)->{
			final Class<?> clazz = comp instanceof Class ? (Class<?>)comp : comp.getClass();
			for(Constructor<?> c : clazz.getDeclaredConstructors()) {
				for(Class<?> param : c.getParameterTypes()) {
					if(param == service)
						return true;
				}
			};
			return false;
		});
	};

	
	/**
	 * @param itfc
	 * @return true if this AtScope is configured to use the given service
	 */
	boolean isServiceConsumer(Class<?> itfc) {
		return comps.stream().anyMatch(comp->{
			final Class<?> clazz = comp instanceof Class ? (Class<?>)comp : comp.getClass();
			return itfc.isAssignableFrom(clazz);
		});
	}
	
	//TODO isActive() // throw exception if adding while active ?

	@Override
	public String scope() {
		return scope;
	}


	@Override
	public String parent() {
		return parent;
	}


	@Override
	public ScopePolicy policy() {
		return policy;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result + ((policy == null) ? 0 : policy.hashCode());
		result = prime * result + ((scope == null) ? 0 : scope.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAtScope other = (AbstractAtScope) obj;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		if (policy != other.policy)
			return false;
		if (scope == null) {
			if (other.scope != null)
				return false;
		} else if (!scope.equals(other.scope))
			return false;
		return true;
	}




}

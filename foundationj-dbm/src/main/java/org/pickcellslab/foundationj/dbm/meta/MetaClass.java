package org.pickcellslab.foundationj.dbm.meta;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.dbm.access.RegenerableDataPointer;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

/**
 * A {@link MetaModel} object which represents a type of {@link NodeItem} in the data model
 * 
 * @author Guillaume Blin
 *
 */
public interface MetaClass extends MetaQueryable, RegenerableDataPointer{

	
	public Class<? extends NodeItem> itemClass(DataRegistry registry);
	
	public String itemTypeId();

	public Stream<MetaLink> metaLinks();
	
	public Stream<MetaLink> metaLinks(Direction dir);
	
	public Stream<MetaLink> metaLinks(String linkType, Direction outgoing);

	public Stream<MetaTag> tags();

	

 

}

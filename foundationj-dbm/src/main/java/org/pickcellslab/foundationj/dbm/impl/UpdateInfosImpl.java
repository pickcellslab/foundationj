package org.pickcellslab.foundationj.dbm.impl;

import java.util.HashMap;
import java.util.HashSet;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.UpdateInfo;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.actions.AKeyInfo;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;

final class UpdateInfosImpl implements UpdateInfo {

	private final Map<String,Set<AKey<?>>> keyMap;
	private final Map<MetaQueryable,Set<AKeyInfo>> infoMap;

	UpdateInfosImpl(Updater action) {
		final List<AKeyInfo> infos = action.updateIntentions();
		keyMap = new HashMap<>();
		infoMap = new HashMap<>();
		infos.stream().forEach(i-> {
			keyMap.put(i.owner().itemDeclaredType(), new HashSet<>());	infoMap.put(i.owner(), new HashSet<>());	
		});
		infos.stream().forEach(i-> {
			keyMap.get(i.owner().itemDeclaredType()).add(i.getKey());	infoMap.get(i.owner()).add(i);
		});
	}


	@Override
	public boolean isAuthorized(String declaredType, AKey<?> k){
		final Set<AKey<?>> authorised = keyMap.get(declaredType);
		if(authorised!=null)
			return authorised.contains(k);
		else
			return false;
	}


	public void updateMetas(Session<?,?> access) throws DataAccessException{

		final MetaInstanceDirector instanceManager = access.metaModel().getInstanceManager();
		
		for(MetaQueryable id : infoMap.keySet()){
			Set<AKeyInfo> keys = infoMap.get(id);
			keys.forEach(k->MetaKeyImpl.getOrCreate(instanceManager, k.getKey(), k.getLength(), (AbstractMetaQueryable) id, "Action update", k.getDescription()));
			access.queryFactory().protectedMeta(access, id).run();
		}
		
	}


}

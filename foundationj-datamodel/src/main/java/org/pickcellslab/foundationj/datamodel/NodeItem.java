package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.graph.Node;

/**
 * Objects implementing this interface are both {@link Node} and {@link DataItem}.
 * 
 */
public interface NodeItem extends WritableDataItem, Node<Link> {


	public boolean hasTag(String tag);
	
	/**
	 * 
	 * @param direction The desired {@link Direction}
	 * @param linkType The desired link type
	 * @return The <a  href="https://en.wikipedia.org/wiki/Degree_(graph_theory)"> degree </a> of this node for the given 
	 * link type and direction.
	 */
	public int getDegree(Direction direction, String linkType);

	/**
	 * @param linkTypes
	 * @return all the link objects of type specified by linkTypes. Note: if none
	 *         is provided, then all the links will be returned
	 */
	public Stream<Link> getLinks(Direction direction, String... linkTypes);


	/**
	 * @param link
	 * @return true if the {@link Link} was successfully added, false if it was already contained into
	 * this item or if the specified link does not point to this item (either source or target)
	 */
	public boolean addLink(Link link);

	/**
	 * @param link The Link to remove
	 * @param updateNeighbour a flag indicating whether this link reference should also be removed from the adjacent node 
	 * @return true if the {@link Link} was successfully removed, false otherwise
	 */
	public boolean removeLink(Link link, boolean updateNeighbour);

	/**
	 * @param predicate A {@link Predicate} to filter the links that need to be removed. If a link passes the specified predicate
	 * then it is removed
	 * @param updateNeighbour a flag indicating whether this link reference should also be removed from the adjacent node
	 * @return The {@link Link} that were removed from this node
	 */
	public Collection<Link> removeLink(Predicate<Link> predicate, boolean updateNeighbour);

	/**
	 * Creates and adds a new {@link Link} to this node. This node will be set as the source of the new Link. 
	 * @param type The type of the new Link
	 * @param target The target of the Link
	 * @return The newly created Link
	 */
	public Link addOutgoing(String type, NodeItem target);

	/**
	 * Creates and adds a new {@link Link} to this node. This node will be set as the <u>target</u> of the new Link.
	 * @param type The type of the new Link
	 * @param source The source to attach to this Node
	 * @return The newly created Link
	 */
	public Link addIncoming(String type, NodeItem source);



}

package org.pickcellslab.foundationj.dbm.queries.actions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;

/**
 * 
 * An {@link Action} is a factory for {@link ActionMechanism}. The {@link Action} interface is the interface to implement in order to
 * perform custom database operations. 
 * 
 * @author Guillaume Blin
 *
 * @param <I> The type of input required by this Action
 * @param <O> The type of the output of this Action
 */
public interface Action<I,O> {

	/**
	 * Creates a new {@link ActionMechanism} which is expected to be able 
	 * to work concurrently with other ActionMechanisms created with this method.<br>
	 * <b>NB:</b> A call to this method needs to create an entry in the set returned by {@link #coordinates()}.
	 * 
	 * @param key An object used as the identifier for the returned ActionMechanism
	 * @return A new {@link ActionMechanism}
	 * @throws DataAccessException 
	 */
	public ActionMechanism<I,O> createCoordinate(Object key) throws DataAccessException;	
	
	/**
	 * @return A Set holding the currently created coordinates via {@link #createCoordinate(Object)}
	 */
	public Set<Object> coordinates();	
	
	/**
	 * Provides an output for the given coordinate.
	 * @param key A coordinate (previously registered via {@link #createCoordinate(Object)}) 
	 * @return The output for the given coordinate
	 * @throws IllegalArgumentException
	 */
	public O createOutput(Object key) throws IllegalArgumentException;	
	
	/**
	 * @return A description of this {'link Action}
	 */
	public String description();
}

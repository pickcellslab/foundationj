package org.pickcellslab.foundationj.datamodel.functions;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.ConnectionSelectorBuilder;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;

/**
 * 
 * A {@link ConnectionSelector} is a {@link ExplicitFunction} which is capable of selecting {@link Link} objects
 * attached to a {@link NodeItem}.
 * 
 * @see ConnectionSelectorBuilder
 * 
 * @author Guillaume Blin
 *
 */
@Mapping(id = "LinkSelection")
@Scope
public class ConnectionSelector implements ExplicitStream<NodeItem, Link>{

	@Supported
	private final ExplicitPredicate<? super Link> filterOnLink;
	@Supported
	private final ExplicitPredicate<NodeItem> filterOnAdjacent;
	
	private final Direction direction;

	public ConnectionSelector(Direction direction , ExplicitPredicate<? super Link> filterOnLink, ExplicitPredicate<NodeItem> filterOnAdjacent){

		Objects.requireNonNull(direction);
		Objects.requireNonNull(filterOnAdjacent);
		Objects.requireNonNull(filterOnLink);

		this.direction = direction;
		this.filterOnLink = filterOnLink;
		this.filterOnAdjacent = filterOnAdjacent;
	}

	@Override
	public Stream<Link> apply(NodeItem t) {

		if(NodeItem.class.isAssignableFrom(t.getClass())){

			return t.getLinks(direction).filter(l->{
				NodeItem adj = null;

				if(t == l.source())
					adj = l.target();
				else if(t == l.target())
					adj = l.source();	
				
				if(adj==null) {
					System.out.println("t : "+ t.getClass().getSimpleName()+" - "+System.identityHashCode(t));
					System.out.println("source : "+l.source().getClass().getSimpleName()+" - "+System.identityHashCode(l.source()));
					System.out.println("target : "+l.target().getClass().getSimpleName()+" - "+System.identityHashCode(l.target()));
					throw new RuntimeException("Unable to find adjacent for Node ["+ t + "] and link ["+l+"]");
				}
				
				return filterOnAdjacent.test(adj) && filterOnLink.test(l);
			});


		}
		else
			return Stream.empty();

	}

	@Override
	public String description() {
		String description = direction+" "+filterOnLink.description()
		+ " with " + filterOnAdjacent.description() + "Adjacent";			
		return description;
	}

	@Override
	public String toString() {
		String description = direction+" "+filterOnLink.toString()
		+ " with adjacent accepted by " + filterOnAdjacent.toString() ;			
		return description;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class getReturnType() {
		return Collection.class;
	}

	@Override
	public ExplicitOperator<?, ?>[] getOperatorCompatibility() {
		return new ExplicitOperator<?,?>[0];
	}

}

package org.pickcellslab.foundationj.dbm.queries.actions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;

public class ListAction<T extends DataItem, E> extends AbstractAction<T, List<E>> {

	
	private final ExplicitFunction<? super T, E> f;


	public ListAction(ExplicitFunction<? super T,E> f){
		super("Create a list for "+f.description());
		this.f = f;
	}
	
	
	@Override
	protected ActionMechanism<T, List<E>> create(String key) {
		return new ListMechanism();
	}
	
	
	
	
	private class ListMechanism implements ActionMechanism<T,List<E>>{

		private final List<E> list = new ArrayList<>();
		
		@Override
		public void performAction(T i) throws DataAccessException {
			list.add(f.apply(i));
		}

		@Override
		public List<E> get() {
			return list;
		}

			
		
	}




	

}

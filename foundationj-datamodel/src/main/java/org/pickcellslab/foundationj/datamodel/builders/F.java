package org.pickcellslab.foundationj.datamodel.builders;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.functions.ConnectionSelector;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitLinkStream;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitStream;
import org.pickcellslab.foundationj.datamodel.functions.KeySelector;
import org.pickcellslab.foundationj.datamodel.functions.LinkEnd;
import org.pickcellslab.foundationj.datamodel.functions.StreamConcat;
import org.pickcellslab.foundationj.datamodel.functions.StreamFindOne;
import org.pickcellslab.foundationj.datamodel.functions.StreamKeys;
import org.pickcellslab.foundationj.datamodel.functions.StreamLinks;
import org.pickcellslab.foundationj.datamodel.functions.StreamSpecificLinks;

/**
 * 
 * A Factory class to obtain commonly used {@link ExplicitFunction}s
 * 
 * @author Guillaume Blin
 *
 */
public final class F {

	/**
	 * Creates a {@link ExplicitFunction} which returns value for the specified {@link AKey}
	 * in a {@link Link} source. 
	 * @param k The desired {@link AKey}
	 * @return The value for the specified {@link AKey} in the link source. Will return null if
	 * the encountered item is not a link or if the key does not exist. 
	 */
	public static <E> ExplicitFunction<Link,E> fetchSourceKey(AKey<E> k){		
		return LinkEnd.Source.next(new KeySelector<>(k,null));		
	}

	
	/**
	 * Creates a {@link ExplicitFunction} which returns value for the specified {@link AKey}
	 * in a {@link Link} source. 
	 * @param k The desired {@link AKey}
	 * @param defaultValue The desired defaultValue to be returned in case the NodeItem encountered does not possess the specified AKey
	 * @return The value for the specified {@link AKey} in the link source.
	 */
	public static <E> ExplicitFunction<Link,E> fetchSourceKey(AKey<E> k, E defaultValue){		
		return LinkEnd.Source.next(new KeySelector<>(k,defaultValue));		
	}

	/**
	 * Creates a {@link ExplicitFunction} which returns value for the specified {@link AKey}
	 * in a {@link Link} target. 
	 * @param k The desired {@link AKey}
	 * @return The value for the specified {@link AKey} in the link source. Will return null if
	 * the encountered item is not a link or if the key does not exist. 
	 */
	public static <E> ExplicitFunction<Link,E> fetchTargetKey(AKey<E> k){		
		return LinkEnd.Target.next(new KeySelector<>(k,null));		
	}

	
	/**
	 * Creates a {@link ExplicitFunction} which returns value for the specified {@link AKey}
	 * in a {@link Link} target. 
	 * @param k The desired {@link AKey}
	 * @param defaultValue The desired defaultValue to be returned in case the NodeItem encountered does not possess the specified AKey
	 * @return The value for the specified {@link AKey} in the link source.
	 */
	public static <E> ExplicitFunction<Link,E> fetchTargetKey(AKey<E> k, E defaultValue){		
		return LinkEnd.Target.next(new KeySelector<>(k,defaultValue));		
	}
	
	
	/**
	 * @return A {@link ExplicitFunction} which will return {@link DataItem#getValidAttributeKeys()}
	 */
	public static ExplicitStream<DataItem,AKey<?>> streamKeys(){
		return new StreamKeys<>();
	}
	

	/**
	 * Creates an {@link ExplicitFunction} which returns the value associated to the provided AKey.
	 * @param k The {@link AKey} to be selected
	 * @param defaultValue The default value if the AKey is not present in the encountered item
	 * @return A new KeySelector
	 */
	public static <T extends DataItem, E> KeySelector<T,E> select(AKey<E> k, E defaultValue){
		return new KeySelector<>(k, defaultValue);
	}

	/**
	 * @param k The {@link AKey} to be selected
	 * @return A new {@link KeySelector}
	 */
	public static <T extends DataItem, E> KeySelector<T,E> select(AKey<E> k){
		return new KeySelector<>(k);
	}




	/**
	 * Initializes a Builder to create a {@link ConnectionSelector}
	 * @see ConnectionSelectorBuilder
	 */
	public static ConnectionSelectorBuilder connections(){
		return new ConnectionSelectorBuilder();
	}



	/**
	 * Creates a {@link ExplicitFunction} which returns the value of the provided {@link ExplicitFunction}
	 * on the source of encountered {@link Link} objects. 
	 * @param f The desired {@link ExplicitFunction}
	 * @return The value for the specified {@link AKey} in the link source. Will return null if
	 * the encountered item is not a link or if the key does not exist. 
	 */
	public static <E> ExplicitFunction<Link,E> applyOnSource(ExplicitFunction<NodeItem,E> f){
		return LinkEnd.Source.next(f);
	}



	/**
	 * Creates a {@link ExplicitFunction} which returns the value of the provided {@link ExplicitFunction}
	 * on the target of encountered {@link Link} objects. 
	 * @param f The desired {@link ExplicitFunction}
	 * @return The value for the specified {@link AKey} in the link target. Will return null if
	 * the encountered item is not a link or if the key does not exist. 
	 */
	public static <E> ExplicitFunction<Link,E> applyOnTarget(ExplicitFunction<NodeItem,E> f){
		return LinkEnd.Target.next(f);
	}


	public static <E extends NodeItem> ExplicitLinkStream<E> streamLinks(){
		return new StreamLinks<>();
	}
	
	
	public static <E extends NodeItem> ExplicitLinkStream<E> streamLinks(Direction dir, String... types){
		return new StreamSpecificLinks<>(dir,types);
	}

	public static <D extends DataItem,T> ExplicitFunction<D,Stream<T>> concat(ExplicitFunction<D,Stream<T>> first, ExplicitFunction<D,Stream<T>> second){
		return new StreamConcat<>(first,second);

	}



	public static <T extends DataItem,E> ExplicitFunction<NodeItem,E> getKeyFromOne(ExplicitFunction<NodeItem, Stream<T>> streamProducer, AKey<E> k){
		return new StreamFindOne<>(streamProducer, DataItem.class).next(new KeySelector<>(k, null)); 

	}



	public static <T> ExplicitFunction<T,String> asString() {
		return new AsString<>();
	}
	
	
	
	
	

}

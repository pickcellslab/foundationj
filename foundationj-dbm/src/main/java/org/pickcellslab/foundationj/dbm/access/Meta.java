package org.pickcellslab.foundationj.dbm.access;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.Traverser;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaPath;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

/**
 * 
 * Provides convenience methods to navigate the MetaModel.
 * A {@link Meta} instance can be obtained via {@link DataAccess#metaModel()}
 * 
 * @author Guillaume Blin
 *
 */
public interface Meta {

	
	public MetaInstanceDirector getInstanceManager();
	
	/**
	 * Finds one of the shortest paths between a MetaClass and any other MetaQueryable in the MetaModel
	 * and returns an ordered map listing the link types and corresponding direction which must
	 * be included in a {@link TraversalDefinition} in order to be able to reach the target from the specified
	 * root.
	 * NB: Links in the MetaModel are actually nodes, therefore if you wish to exclude links in the data model
	 * you should exclude the nodes of the metamodel with a name equals to the type of the link in the datamodel.
	 * @param session The Session where the MetaModel resides
	 * @param root The root to start the search from
	 * @param target The target of the search
	 */
	public MetaPath shortestPath(MetaClass root, MetaQueryable target, TraverserConstraints constraints);

	/**
	 * Finds the MetaModel object which corresponds to the provided data instance
	 * @param dataItem An instance of an item stored in the database. Must not be null and must not be
	 * an instance of the MetaModel.
	 * @param session The session providing access to the database in wich the datamodel and metamodel are stored
	 * @return The {@link MetaQueryable} which corresponds to the provided data instance. Maybe null if not found.
	 */

	public MetaQueryable getMetaModel(DataItem dataItem);

	/**
	 * Finds the MetaModel object which corresponds to the provided data instance
	 * @param declaredType The type of the item stored in the database. Must not be null as returned by {@link WritableDataItem#declaredType()}
	 * @param session The session providing access to the database in wich the datamodel and metamodel are stored
	 * @return The {@link MetaClass} which corresponds to the provided type. Maybe null if not found.
	 */
	public MetaClass getMetaModel(String declaredType);

	public List<MetaLink> getMetaLinks(String linkType);

	/**
	 * @param linkType The {@link MetaLink#linkType()}
	 * @param srcType The {@link MetaClass#itemDeclaredType()} of the source
	 * @param tgType The {@link MetaClass#itemDeclaredType()} of the target
	 * @return The MetaLink given the provided types or null if it does not exists.
	 */
	public MetaLink getMetaLinks(String linkType, String srcType, String tgType);

	public boolean hasSeveralSourceTargetCombination(String linkType);

	public List<MetaClass> classesExtending(Class<?> itrfc);

	/**
	 * @param predicate
	 * @return A List of {@link MetaQueryable} that meets the provided {@link Predicate}
	 */
	public List<MetaQueryable> getQueryable(Predicate<? super MetaQueryable> predicate);

	/**
	 * @param predicate
	 * @return A List of {@link MetaReadable} that meets the provided {@link Predicate}
	 */
	public List<MetaReadable<?>> getReadable(Predicate<? super MetaReadable> predicate);

	/**
	 * A utility method to retrieve links likely to be present at a specific depth in the data graph. All Link types are included in
	 * the traversal.
	 * @param root The root {@link MetaClass}
	 * @param depth The desired depth
	 * @return A {@link RegeneratedItemsImpl} holding {@link MetaLink} as targets representing the possible {@link Link}
	 * that can be encountered at the specified depth
	 * Note that the reconstruction of each MetaLink goes to depth 2, therefore the full MetaModel is umlikely
	 * to be retrieved. However, the {@link MetaKey} for both MetaLink and the source and target MetaClass will be present
	 */
	public List<MetaLink> linksAtDepth(MetaClass root, int depth);

	/**
	 * @param root The root {@link MetaClass}
	 * @param depth The desired depth
	 * @return A Set of {@link MetaClass} representing the possible classes of {@link WritableDataItem}
	 * that can be encountered at the specified depth
	 * Note that the reconstruction of each MetaClass goes to depth 1, therefore the {@link MetaLink} that are attached
	 * to the MetaClass are not complete (do not call toString() on MetaLinks for instance)
	 */
	public Set<MetaClass> classesAtDepth(MetaClass root, int depth);

	/**
	 * @return All the {@link MetaClass} found in the MetaModel in a Set. Here the MetaClass are reconstructed
	 * entirely, including, MetaKey, MetaTag and MetaLink. Note, this set can easily be
	 * transformed into a Collection of {@link Traverser} via {@link #toTraversers(Collection)}
	 */
	public Set<MetaClass> getAll();
	

	public <T extends MetaModel> Stream<T> getMetaObjects(Class<T> clazz);

	public void performIntegrityCheck();

}
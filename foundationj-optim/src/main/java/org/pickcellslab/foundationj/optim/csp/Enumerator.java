package org.pickcellslab.foundationj.optim.csp;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;

/**
 * An {@link Enumerator} is an object capable of creating iterable objects allowing to go through each 
 * possible assignment state for a specific solver.
 * 
 * @author Guillaume Blin
 *
 * @param <N>
 * @param <D>
 */
public interface Enumerator<N extends Node<D>,D extends Edge<? extends N>> extends Iterable<Collection<Solver<N,D>>>{
	
	
	/**
	 * This method is called when a graph of solvers is properly configured, not before.
	 * It allows the Enumerator to perform any preconfiguration such as caching before its
	 * {@link #iterator()} method is called during the solving process.
	 */
	public void initialise();
	
}

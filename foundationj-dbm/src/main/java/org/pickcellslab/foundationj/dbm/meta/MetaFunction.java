package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.FunctionDimension;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.AKeyInfo;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapping;

/**
 * 
 * A {@link MetaReadable} which applies a {@link ExplicitFunction} on its target.
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 */


@Data(typeId = "MetaFunction", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaFunction<T> extends MetaItem implements MetaCastable<T> {


	private static AKey<String> generated = AKey.get("Generated Key",String.class);
	private static AKey<Integer> length = AKey.get("length",Integer.class);


	private MetaFunction(String uid){
		super(uid);
	}

	/**
	 * Creates a new {@link MetaFunction} with the given properties.
	 * NB the length must be equal to -1 if the return type is not an array!
	 * @param Name
	 * @param creator
	 * @param description
	 * @param function
	 * @param key
	 * @param length must be equal to -1 if the return type is not an array!
	 * @param usedOn
	 * @throws InstantiationHookException 
	 * @throws ModelExtensionException 
	 */
	public static <T> MetaFunction<T> getOrCreate(
			MetaInstanceDirector director,
			String name, 
			String description,
			ExplicitFunction<? extends DataItem,T> function,
			AKey<T> key,
			int length,
			MetaQueryable usedOn
			) throws NonDataMappingException, InstantiationHookException {



		Objects.requireNonNull(usedOn);
		Objects.requireNonNull(function);
		Objects.requireNonNull(key);

		final String xml = NonDataMapping.convertToString(function); // perform this first in case exception occurs while marshalling function


		@SuppressWarnings("unchecked")
		final MetaFunction<T> mf = director.getOrCreate(MetaFunction.class, MetaItem.randomUID(MetaFunction.class));

		if(!mf.getAttribute(MetaModel.name).isPresent()){// Check if it was retrieved or if it already existed

			mf.setAttribute(MetaItem.name, name);
			mf.setAttribute(MetaItem.desc, description);


			//Store the generated key
			mf.setAttribute(generated,key.toString());

			//and the length
			mf.setAttribute(MetaFunction.length,length);


			//Attach to the queryable that use this function
			new DataLink(MetaReadable.APPLICABLE_TO, mf, usedOn, true);

			//To XML
			mf.setAttribute(AKey.get("Function",String.class), xml);

		}

		return mf;


	}


	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.creator, MetaModel.desc, AKey.get("Function",String.class), generated, length);
	}
	
	
	


	@SuppressWarnings("unchecked")
	public AKey<T> generatedKey(){
		return (AKey<T>) AKey.get(getAttribute(generated).get());
	}


	@Override
	public String toString(){
		return name()+" (Function)";
	}

	@SuppressWarnings("unchecked")
	public ExplicitFunction<DataItem, T> toFunction(){
		ExplicitFunction<DataItem, T> f;
		try {
			f = (ExplicitFunction<DataItem,T>) 
					NonDataMapping.restoreFromString(getAttribute(AKey.get("Function", String.class)).get());
		} catch (NonDataMappingException e) {
			throw new RuntimeException("The "+AKey.get("Function", String.class).toString()+
					" property of this object has been modified, Unable to rebuild the function", e);
		}		
		return f;
	}

	@Override
	public dType dataType() {
		return generatedKey().dType();
	}

	@Override
	public MetaQueryable owner() {
		return (MetaQueryable) typeMap.get(MetaReadable.APPLICABLE_TO).iterator().next().target();
	}

	@Override
	public int numDimensions() {
		int l = getAttribute(MetaFunction.length).get();
		return l == -1 ? 1 : l;
	}


	@Override
	public Dimension<DataItem,T> getDimension(int index) {
		if(index<0 || index>=numDimensions())
			throw new ArrayIndexOutOfBoundsException(index);
		if(!isArray())
			return new FunctionDimension<>(generatedKey(), toFunction(), this.description());
		else
			return new FunctionDimension<>(generatedKey(), index, getAttribute(MetaFunction.length).get(), toFunction(), this.description());
	}

	@Override
	public Dimension<DataItem, T> getRaw() {
		return new FunctionDimension<>(generatedKey(), -1, getAttribute(MetaFunction.length).get(), toFunction(), this.description());
	}




	@Override
	public String toHTML() {
		return "<HTML>"+
				"<p><strong>MetaFunction</strong>"+
				"<ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"<li>Creates: "+getDimension(0).toString()+
				"</li></p></HTML>";
	}



	@Override
	public Class<T> dataClass() {
		return generatedKey().type();
	}

	@Override
	public void cast(DataAccess access) throws DataAccessException {

		Objects.requireNonNull(access, "Access is null");

		final Updater u = createUpdater();
		final MetaQueryable owner = owner();

		owner.updateData(access, u);


		// Now delete this MetaFunction		
		access.queryFactory().deleteAnnotation(this);

		//this.setHidden(true);
		//access.queryFactory().meta(this).run();

	}





	@Override
	public void cast(DataAccess access, ExplicitPredicate<WritableDataItem> p) throws DataAccessException {

		Objects.requireNonNull(access, "Access is null");
		Objects.requireNonNull(p, "p is null");

		final Updater u = createUpdater();
		final MetaQueryable owner = owner();

		owner.initiateUpdate(access, u).useFilter(p).run();

		// Now delete this MetaFunction		
		//access.queryFactory().deleteAnnotation(this);

		this.setHidden(true);
		access.queryFactory().meta(this).run();

	}





	@Override
	public void cast(DataAccess access, TraversalDefinition td) throws DataAccessException, IllegalArgumentException {

		Objects.requireNonNull(access, "Access is null");
		Objects.requireNonNull(td, "td is null");


		final Updater u = createUpdater();
		final MetaQueryable owner = owner();

		owner.initiateUpdate(access, u).useTraversal(td).run();

		// Now delete this MetaFunction		
		//access.queryFactory().deleteAnnotation(this);

		this.setHidden(true);
		access.queryFactory().meta(this).run();


	}



	private Updater createUpdater(){

		// First perform the update operation

		final AKey<T> k = generatedKey();
		final ExplicitFunction<DataItem,T> f = toFunction();
		final List<AKeyInfo> updates = new ArrayList<>(1);
		updates.add(new AKeyInfo(owner(), generatedKey(), description(), getAttribute(MetaFunction.length).get()));

		return new Updater(){

			@Override
			public void performAction(Updatable i) throws DataAccessException {
				i.setAttribute(k, f.apply(i));
			}

			@Override
			public List<AKeyInfo> updateIntentions() {
				return updates;
			}

		};

	}



}

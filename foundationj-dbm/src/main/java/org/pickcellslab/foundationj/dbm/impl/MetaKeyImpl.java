package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyArrayDimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyDimension;
import org.pickcellslab.foundationj.datamodel.functions.IndexNamingFunction;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.queries.IllegalDeletionException;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

/**
 * A {@link MetaReadable} which correspond to a specific {@link AKey} in the data graph
 * 
 *
 */
@Data(typeId = "MetaKey", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
class MetaKeyImpl<E> extends MetaItem implements MetaKey<E>{


	public static AKey<Boolean> optional = AKey.get("Is Optional", Boolean.class);
	public static AKey<Integer> length = AKey.get("Length", Integer.class);
	public static AKey<String> type = AKey.get("ObjectType", String.class);
	public static AKey<String[]> indiceNames = AKey.get("Indice Names", String[].class);


	private MetaKeyImpl(String uid) {
		super(uid);
	};


	static <E> MetaKeyImpl<E> getOrCreate(MetaInstanceDirector metaMgr, AKey<?> aKey, int lengthIfArray, AbstractMetaQueryable mq, String creator, String description){

		try{

			@SuppressWarnings("unchecked")
			final MetaKeyImpl<E> mk = (MetaKeyImpl<E>) metaMgr.getOrCreate(MetaKeyImpl.class, aKey.toString() + mq.recognitionString());

			if(!mk.getAttribute(name).isPresent()){// Check if it was retrieved or if it already existed

				mk.setAttribute(MetaModel.creator, creator);
				mk.setAttribute(MetaModel.desc, description);
				mk.setAttribute(MetaModel.name, aKey.name);
				mk.setAttribute(type, aKey.clazz);    
				mk.setAttribute(optional, false);
				mk.setAttribute(length, lengthIfArray);

				Link l = new DataLink(MetaModel.keyLink, mq, mk, false);

				Set<Link> typeSet  = new LinkedHashSet<>();
				mk.typeMap.put(l.declaredType(), typeSet);

				mk.directionMap.get(Direction.INCOMING).add(l);
				typeSet.add(l);

				mq.addKey(mk);

			}

			return mk;

		}
		catch(Exception e){
			throw new RuntimeException(e);
		}

	}


	void setLength(int length){
		this.setAttribute(MetaKeyImpl.length,length);
	}




	void setIsOptional(boolean optional){
		this.setAttribute(MetaKeyImpl.optional, optional);
	}


	
	public void setIndicesNames(String[] names) {
		Objects.requireNonNull(names);
		if(names.length<this.getAttribute(length).get())
			throw new IllegalArgumentException("provided names length ("+names.length+") inadequate - required: "+this.getAttribute(length).get());
		this.setAttribute(indiceNames, names);
	};
	

	@Override
	public Class<E> dataClass(){
		return toAKey().type();    
	}




	@Override
	public String name(){
		return getAttribute(name).get();
	}



	@Override
	public int length(){
		return getAttribute(length).get();
	}



	@Override
	public MetaQueryable owner(){
		return (MetaQueryable) this.typeMap.get(MetaModel.keyLink).iterator().next().source();
	}




	@Override
	public boolean isOptional(){
		return getAttribute(optional).get();
	}


	@Override
	@SuppressWarnings("unchecked")
	public AKey<E> toAKey(){
		return (AKey<E>) AKey.get(getAttribute(name).get()+":"+getAttribute(type).get());
	}







	@Override
	public int numDimensions() {
		int l = length();
		if(l==-1)
			return 1;
		else
			return l;
	}



	@Override
	public Dimension<DataItem,E> getDimension(int index) {

		if(index <0 || index >= numDimensions())
			throw new ArrayIndexOutOfBoundsException(index);

		int l = length();
		if(l==-1)
			return new KeyDimension<>(toAKey(), this.description());
		else if(l==1)
			return new KeyDimension(toAKey(), index, this.description(), (i)->"");
		else {
			final String[] names = getAttribute(indiceNames).orElse(null);
			if(names!=null)
				return new KeyDimension(toAKey(), index, this.description(), new IndexNamingFunction(names));
			else
				return new KeyDimension(toAKey(), index, this.description());
		}

	}







	@Override
	public String toHTML() {
		return "<HTML> Key : \"" + name() + "\" and type [" + dataType() + "]"
				+ " <br> Found in " + owner().itemDeclaredType()
				+ " <br> Description : "+ description()
				+ " <br> length : "+length()
				+ " <br> IsOptional " + isOptional()
				+ "</HTML>";
	}



	@Override
	public dType dataType() {
		return this.toAKey().dType();
	}



	@Override
	public Dimension<DataItem, ?> getRaw() {
		int l = length();
		if(l==-1)
			return new KeyDimension<>(toAKey(), this.description());
		else
			return new KeyArrayDimension<>(toAKey(), l, this.description());
	}



	@Override
	public String toString(){
		if(isOptional())
			return getAttribute(name).get()+ " (Optional)";
		else
			return getAttribute(name).get();

	}



	@Override
	public TraversalDefinition deletionDefinition() {
		return TraversalDefinition.newDefinition().startFrom(DataRegistry.typeIdFor(MetaKeyImpl.class))
				.having(MetaModel.recognition, this.getAttribute(MetaModel.recognition).get())
				.fromDepth(0).toDepth(0).traverseAllLinks().includeAllNodes().build();
	}




	@Override
	public String description() {
		return (String) attributes.get(desc);
	}




	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc, MetaModel.recognition, length, optional, type);
	}



	@Override
	public int deleteThisAttribute(DataAccess access) throws DataAccessException, IllegalDeletionException {
		return owner().initiateDeletion(access).key(toAKey()).getAll().run();
	}



	@Override
	public int deleteThisAttributeUsingAPredicate(DataAccess access, ExplicitPredicate<DataItem> predicate)
			throws DataAccessException, IllegalDeletionException {		
		return owner().initiateDeletion(access).key(toAKey()).useFilter(predicate).run();
	}

}

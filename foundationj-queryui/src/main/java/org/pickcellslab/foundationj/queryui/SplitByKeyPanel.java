package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Supplier;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class SplitByKeyPanel<T extends Supplier<MetaReadable<?>> & Highlightable>  extends SplitterUI<SplitByKeyPanel<T>> {

	private final UITheme theme;
	private final DataRegistry registry;
	
	private MetaReadable<?> key;
	private JLabel lblNone;

	public SplitByKeyPanel(UITheme theme, DataRegistry registry, T supplier) {

		Objects.requireNonNull(theme);
		Objects.requireNonNull(supplier);
		
		this.theme = theme;
		this.registry = registry;

		JLabel lblSelectedAttribute = new JLabel("Selected Attribute : ");
		lblSelectedAttribute.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblSelectedAttribute);

		lblNone = new JLabel("None");
		lblNone.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblNone);

		JButton btnChange = new JButton("Change");
		add(btnChange);

		btnChange.addActionListener(l->{
			MetaReadable<?> mr = supplier.get();
			if(mr == null){
				JOptionPane.showMessageDialog(null, "No attributes selected");
				supplier.setHighlighted(true);
				return;
			}
			key = mr;
			lblNone.setText(mr.toString());
			lblNone.setIcon(MetaToIcon.get(mr, registry, theme, 16));
			this.fireNextValidityChanged();
		});

	}

	@Override
	public boolean validity() {
		return key!=null;
	}

	
	MetaReadable<?> getChosenReadable(){
		return key;
	}
	
	
	@Override
	MetaSplit getSplitter(Meta meta) {
		try {
			return MetaReadableSplitter.getOrCreate(meta.getInstanceManager(), key);
		} catch (InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}

	@Override
	public void copyState(SplitByKeyPanel<T> other) {
		key = other.key;
		if(key!=null){
			lblNone.setText(key.toString());
			lblNone.setIcon(MetaToIcon.get(key, registry, theme, 16));
			this.fireNextValidityChanged();
		}
	}



}

package org.pickcellslab.foundationj.datamodel.reductions;

import java.util.function.Supplier;

import org.apache.commons.math3.stat.descriptive.StorelessUnivariateStatistic;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.apache.commons.math3.stat.descriptive.rank.Max;
import org.apache.commons.math3.stat.descriptive.rank.Min;
import org.apache.commons.math3.stat.descriptive.summary.Sum;

public enum StatsType implements Supplier<StorelessUnivariateStatistic>{
	MEAN{
		@Override
		public StorelessUnivariateStatistic get() {
			return new Mean();
		}
	}, 
	STD{
		@Override
		public StorelessUnivariateStatistic get() {
			return new Variance();
		}
	},
	SUM{
		@Override
		public StorelessUnivariateStatistic get() {
			return new Sum();
		}
	},
	MIN{
		@Override
		public StorelessUnivariateStatistic get() {
			return new Min();
		}
	},
	MAX{
		@Override
		public StorelessUnivariateStatistic get() {
			return new Max();
		}
	}
	
}
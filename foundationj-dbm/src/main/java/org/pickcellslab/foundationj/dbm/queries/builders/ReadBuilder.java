package org.pickcellslab.foundationj.dbm.queries.builders;


import java.util.function.Consumer;

import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;

public interface ReadBuilder<T> {

	/**
	 * Specifies the {@link Action} to use when reading the database
	 * @param action The {@link Action} which will be used during the query
	 * @return The next step in the building process
	 * 
	 * @see {@link Actions}
	 */
	<O> ActionBuilder<T, O> with(Action<? super T, O> action);
	
	
	<O> AccessChoice<T, QueryRunner<O>> consume(Consumer<? super T> consumer); 

}
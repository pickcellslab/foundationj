package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.SplitTraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

class SplitTraversalRunner<V,E,T,K,O> implements QueryRunner<OrganisedResult<K, O>>{

	private final TargetType<V, E, T> target;
	private final SplitTraversalDefinition<V, E, T, K> definition;
	private final Action<? super T, O> action;

	SplitTraversalRunner(TargetType<V,E,T> target, SplitTraversalDefinition<V,E,T,K> definition, Action<? super T,O> action){
		this.target = target;
		this.definition = definition;
		this.action = action;
	}
	
	@Override
	public OrganisedResult<K, O> run() throws DataAccessException {
		return target.session().runQuery(new SplitTraversalQuery<>(definition, action));
	}

}

+++

title ="The Query Factory"
weight = 2

date = "2018-07-23"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2018-07-31"

repo = "foundationj"
fileUrl = "https://framagit.org/pickcellslab/foundationj/blob/docs/docs/content/developers/architecture/foundationj/query_factory.md"

tags = ["concepts", "develop"]

+++ 



{{% panel theme="warning" header="TODO" %}}

This page is just a place holder for now. It will contain descriptions and code example to learn about how the graph database can be queried using the foundationj API.
An important part will also be dedicated to interrogating the MetaModel. 

{{% /panel %}}

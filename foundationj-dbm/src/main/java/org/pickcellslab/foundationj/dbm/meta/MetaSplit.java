package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;

public abstract class MetaSplit extends MetaItem implements ManuallyStored, MetaAutoDeletable {

	
	protected MetaSplit(String uuid) {
		super(uuid);
	}

	
	
	public abstract boolean pathSplitSupported();
	
	public abstract boolean targetSplitSupported();
	
	public abstract <E> ExplicitSplit<E, String> toSplitter();	
	
	public abstract <V,E> ExplicitPathSplit<V,E,String> toPathSplitter();
	


}

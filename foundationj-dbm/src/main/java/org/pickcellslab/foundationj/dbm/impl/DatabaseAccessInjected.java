package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;

interface DatabaseAccessInjected<V,E> {

	void setAccess(DatabaseAccess<V,E> access);
	
}

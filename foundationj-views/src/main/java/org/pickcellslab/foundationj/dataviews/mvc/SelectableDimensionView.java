package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;

/**
 * A {@link DataView} which displays all or a subset of all the dimensions available in a DataViewModel.
 * 
 * @author Guillaume Blin
 *
 */
public interface SelectableDimensionView<M extends DimensionalModel<T,D>,T,D> extends DataView<M,T>{  

	
	public int numViewDimensions();
	
	public String viewDimensionName(int i) throws IndexOutOfBoundsException;
	
	/**
	 * @return A {@link Predicate} which dictates which dimensions (from the model) can be displayed in this view
	 */
	public Predicate<Dimension<T,?>> dimensionsPredicate();	
	
	/**
	 * Sets the variable to be displayed in the specified dimension in the view
	 * @param dim The name of the dimension in which the variable should be displayed
	 * @param key The {@link Dimension} describing the variable
	 * @param index The index in the array if the MetaKey refers to an array
	 * @throws IllegalArgumentException If the provided dimension does not exist in the view
	 */
	public void select(String dim, Dimension<T,?> key, int index) throws IllegalArgumentException;

	/**
	 * @param dim The name of the dimension in the view
	 * @return The {@link MetaKey} associated with the currently selected variable in the specified view dimension
	 * @throws IllegalArgumentException If the provided dimension does not exist in the view
	 */
	public Dimension<T,?> selectionFor(String dim);

	/**
	 * The current variable held in one dimension of the view may be an array itself. This method
	 * returns the currently selected index in this array, or -1 if the variable is not
	 * an array.
	 * @param dim The name of the dimension in the view
	 */
	public int indexFor(String dim);

}

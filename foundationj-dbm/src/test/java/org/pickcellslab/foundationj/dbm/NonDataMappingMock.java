package org.pickcellslab.foundationj.dbm;

import java.util.Collections;
import java.util.Set;

import org.mockito.Mockito;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapper;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapping;

public class NonDataMappingMock extends NonDataMapping{
	
	
	public NonDataMappingMock(Set<Class<?>> mappedClass) {
		super(Mockito.mock(NonDataMapper.class), mappedClass, Collections.emptyList());
		
	}

}

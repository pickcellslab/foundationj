package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.queries.DataCollectorBuilder;
import org.pickcellslab.foundationj.dbm.queries.ReadTable;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.TableMaker;
import org.pickcellslab.foundationj.dbm.queries.builders.ActionBuilder;


class TableMakerBuilder<T extends WritableDataItem> extends ActionBuilderImpl<T,ReadTable>{

	private DataCollectorBuilder<T> collectorBuilder = new DataCollectorBuilder<>();
	private final TargetType<?,?,T> target;
	private boolean getAll = false;


	public TableMakerBuilder(TargetType<?,?,T> target){
		this.target = target;
	}


	public TableMakerBuilder<T> get(AKey<?> key){
		collectorBuilder.get(key);			
		return this;
	}

	public TableMakerBuilder<T> get(Collection<AKey<?>> keys){
		collectorBuilder.get(keys);			
		return this;
	}



	public <E> TableMakerBuilder<T> compute(ExplicitFunction<? super T,E> f, AKey<E> key){
		collectorBuilder.compute(f,key);
		return this;
	}


	public ActionBuilder<T, ReadTable> getAllKeys() {
		getAll = true;
		return this;
	}


	@Override
	protected TargetType<?,?,T> getType() {
		return target;
	}


	@Override
	protected Action<T, ReadTable> getAction() {
		if(getAll)
			return new TableMaker<>(collectorBuilder.getAll());
		else
			return new TableMaker<>(collectorBuilder.build());
	}










}

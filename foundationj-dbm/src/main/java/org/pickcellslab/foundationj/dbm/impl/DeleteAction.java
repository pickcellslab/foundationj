package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Deletable;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.AbstractAction;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

class DeleteAction<T extends WritableDataItem & Deletable> extends AbstractAction<T,Integer> {

	private final DeleteConfiguration<T> config;
	private DataDeletionListener lstr;

	DeleteAction(DeleteConfiguration<T> config, DataDeletionListener lstr) {
		super(config.description());
		this.config = config;
		this.lstr = lstr;
	}




	@Override
	protected ActionMechanism<T, Integer> create(String key) {
		return new DeleteMechanism();
	}




	private class DeleteMechanism implements ActionMechanism<T,Integer>{

		private int count = 0;

		@Override
		public void performAction(T i) {

			if(config.deleteItem())
				i.delete();
			else
				for(AKey<?> k : config.deleteKeys())
					i.removeAttribute(k);

			count++;
		}

		@Override
		public void lastAction() throws DataAccessException{
			
			//System.out.println();
			//System.out.println("Delete Mechanism last Action called");
			
			if(lstr!=null){
				//System.out.println("listener is not null");
				
				if(config.deleteItem())
					lstr.completeDeletion();
				else
					lstr.keysDeletion(config.deleteKeys());
			}
		}


		public Integer get() {
			return count;
		}

	}




}

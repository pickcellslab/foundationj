package org.pickcellslab.foundationj.bootstrap;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

import org.picocontainer.Behavior;
import org.picocontainer.ComponentAdapter;
import org.picocontainer.ComponentMonitor;
import org.picocontainer.Injector;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ComponentLogger implements ComponentMonitor {

	private final Logger log = LoggerFactory.getLogger(ComponentLogger.class);

	private final ScopeImpl scope;
	
	public ComponentLogger(ScopeImpl scope) {
		this.scope = scope;
	}
	
	@Override
	public <T> Constructor<T> instantiating(PicoContainer container, ComponentAdapter<T> componentAdapter,
			Constructor<T> constructor) {
		log.trace(scope.name()+" instantiating "+componentAdapter.getComponentKey());
		return constructor;
	}

	@Override
	public <T> void instantiated(PicoContainer container, ComponentAdapter<T> componentAdapter,
			Constructor<T> constructor, Object instantiated, Object[] injected, long duration) {
		log.debug(scope.name()+" instantiated "+componentAdapter.getComponentKey());
	}

	@Override
	public <T> void instantiationFailed(PicoContainer container, ComponentAdapter<T> componentAdapter,
			Constructor<T> constructor, Exception cause) {
		log.error(scope.name()+" instantiation Failed "+componentAdapter.getComponentKey(), cause);
		
	}

	@Override
	public Object invoking(PicoContainer container, ComponentAdapter<?> componentAdapter, Member member,
			Object instance, Object[] args) {

		 return KEEP;
	}

	@Override
	public void invoked(PicoContainer container, ComponentAdapter<?> componentAdapter, Member member, Object instance,
			long duration, Object[] args, Object retVal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void invocationFailed(Member member, Object instance, Exception cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void lifecycleInvocationFailed(MutablePicoContainer container, ComponentAdapter<?> componentAdapter,
			Method method, Object instance, RuntimeException cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object noComponentFound(MutablePicoContainer container, Object componentKey) {
		log.debug(scope.name()+" has no " + componentKey);
		
		return null;//scope.getOption(componentKey);
	}

	@Override
	public Injector newInjector(Injector injector) {
		// TODO Auto-generated method stub
		return injector;
	}

	@Override
	public Behavior newBehavior(Behavior behavior) {
		// TODO Auto-generated method stub
		return behavior;
	}

	
	
}

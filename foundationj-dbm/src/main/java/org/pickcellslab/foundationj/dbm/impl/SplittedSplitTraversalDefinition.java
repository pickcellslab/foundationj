package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.PathTraverser;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.SplitTraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

/**
 * 
 * 
 * @author Guillaume Blin
 *
 * @param <V>
 * @param <E>
 * @param <T>
 * @param <K>
 * @param <L>
 */
class SplittedSplitTraversalDefinition<V,E,T,K,L> implements SplitTraversalDefinition<V,E,T,Pair<L,K>>{ 	

	//private final Logger log = Logger.getLogger(SplittedSplitTraversalDefinition.class);

	private final TraversalDefinition td;
	private final ExplicitSplit<T,K> splitter;
	private final ExplicitPathSplit<V,E,L> pathSplitter;

	private final TargetType<V, E, T> target;

	SplittedSplitTraversalDefinition(TargetType<V,E,T> target, TraversalDefinition td, ExplicitPathSplit<V,E,L> pathSplitter, ExplicitSplit<T,K> splitter){			
		this.td = td;
		this.target = target;
		this.splitter = splitter;
		this.pathSplitter = pathSplitter;
	}


	public String descrition(){
		String d = td.description()+" "+pathSplitter.description();
		if(null != splitter)
			d +=" then "+splitter.description();
		return d;
	}



	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<Pair<L,K>, Iterable<T>> split(DatabaseAccess<V, E> db) throws DataAccessException {


		Pairs<L,K> pairs = new Pairs<>();

		Map<Pair<L,K>, List<T>>  finalSplit = new HashMap<>();


		FjAdaptersFactory<V,E> f = db.createFactory();		
		PathTraverser<V,E> tr = db.traverser(td);

		pathSplitter.setFactory(f);


		// Iterate over Paths
		Iterator<Path<V,E>> itr = tr.paths();
		while(itr.hasNext()){			

			Path<V,E> p = itr.next();

			//Get the grouping for path
			L l = pathSplitter.getKeyFor(p);

			//Translate path to target
			T t = target.last(p,f);

			if(null!=t){

				//Get the grouping for target			
				K k = splitter.getKeyFor(t);

				//null check TODO

				Pair<L,K> pr = pairs.get(l,k);

				List<T> list = finalSplit.get(pr);
				if(list == null){
					list = new ArrayList<>();
					finalSplit.put(pr, list);
				}
				list.add(t);	
			}
		}



		return (Map)finalSplit;

	}



}













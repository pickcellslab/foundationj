package org.pickcellslab.foundationj.datamodel.dimensions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.Objects;
import java.util.function.Function;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;

@Mapping(id = "Property")
public class KeyDimension<E> implements Dimension<DataItem,E> {

	@SuppressWarnings("rawtypes")
	private final AKey key;
	private final String name;
	private final int index;
	private final String description;


	public KeyDimension(AKey<E> k, String description){

		Objects.requireNonNull(k,"the provided AKey cannot be null");
		if(k.type().isArray()) {
			throw new IllegalArgumentException("A length must be specified if the provided AKey points to an array value");
		}
		this.key = k;
		this.index = -1;
		this.name = key.name;

		if(null == description)
			description = "NaN";
		this.description = description;
	}



	public KeyDimension(AKey<E[]> k, int index, String description){
		this(k, index, description, i-> index == -1 ? "" : " "+i);
	}



	public KeyDimension(AKey<E[]> k, int index, String description, Function<Integer,String> indexNames){

		Objects.requireNonNull(k,"the provided AKey cannot be null");
		Objects.requireNonNull(indexNames,"the provided indexNames cannot be null");

		if(!k.type().isArray())
			throw new IllegalArgumentException("The provided AKey does not point to an array type");
		if(index<0)
			throw new IllegalArgumentException("The provided index is negative, use KeyArrayDimension instead if you need a dimension"
					+ " which retrieves the entire array the AKey points at");
		if(index<0)
			throw new IllegalArgumentException("The lengt");

		this.key = k;
		this.index = index;
		this.name = key.name+indexNames.apply(index);

		if(null == description)
			description = "Not Available";
		this.description = description;
	}



	@Override
	public int index() {
		return index;
	}

	@Override
	public String name() {
		return name;
	}



	@Override
	public dType dataType() {
		dType t = key.dType();
		return t;
	}


	@Override
	public String toString() {
		return name;
	}


	@Override
	public boolean equals(Object o){
		if(o instanceof KeyDimension)
			return key.equals(((KeyDimension<?>)o).key) && index == ((Dimension<?,?>)o).index();
		else return false;
	}


	@Override
	public int hashCode(){
		return key.hashCode() + Integer.hashCode(index);
	}





	@SuppressWarnings("unchecked")
	@Override
	public E apply(DataItem i) {
		if(i==null)	return null;
		final Object r = i.getAttribute(key).orElse(null);
		if(index==-1)			
			return (E) r;
		else
			if(r == null)
				return null;
		final int length = Array.getLength(r);
		if(index>=length)
			return null;
		return (E) Array.get(r, index);
	}





	@SuppressWarnings("unchecked")
	@Override
	public Class<E> getReturnType() {
		// Here we may extract one value out of an array so check for this
		if(index!=-1)
			return key.type().getComponentType();
		return (Class<E>) key.type();
	}



	@Override
	public String info() {
		return description;
	}



	@Override
	public int length() {
		return -1;
	}



}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.queries.ReconstructionRule;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;


class MetaDeletion<V,E> implements ReconstructionMechanism<V,E> {

	
	private final ReconstructionRule rule;
	private final List<V> list = new ArrayList<>();
	
	private DatabaseAccess<V, E> access;
	private RegeneratedItems result;

	MetaDeletion(ReconstructionRule rule){
		this.rule = rule;
	}
	
	
	@Override
	public void performAction(V i) throws DataAccessException {
			list.add(i);
	}

	@Override
	public void setAccess(DatabaseAccess<V, E> access) {
		this.access = access;
	}
	
	public void lastAction() throws DataAccessException{
		result = access.regenerate(list, rule, true);
	}

	@Override
	public RegeneratedItems get() {
		return result;
	}
	
}

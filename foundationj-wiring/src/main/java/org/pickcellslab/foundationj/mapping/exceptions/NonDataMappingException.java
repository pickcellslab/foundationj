package org.pickcellslab.foundationj.mapping.exceptions;

import org.pickcellslab.foundationj.mapping.extra.NonDataMapping;

/**
 * 
 * A checked Exception thrown when an issue occurs when performing a {@link NonDataMapping}
 * 
 * @author Guillaume Blin
 *
 */
public class NonDataMappingException extends Exception {

	public NonDataMappingException(String message, Throwable cause) {
		super(message, cause);
	}

}

package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.function.Function;

public class MorphedIterator<I,T> implements Iterator<T>{

	private final Function<I, T> f;
	private Iterator<I> it;

	public MorphedIterator(Iterator<I> it, Function<I,T> f){
		this.it = it;
		this.f = f;
	}
	
	@Override
	public boolean hasNext() {
		return it.hasNext();
	}

	@Override
	public T next() {
		return f.apply(it.next());
	}
	
}
package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;

class StatsWrap<T, N extends Number>{

	private final ExplicitFunction<T,N> f;
	private final SummaryStatistics stats = new SummaryStatistics();


	StatsWrap(ExplicitFunction<T,N> f){
		this.f = f;
	}

	void performAction(T i) {
		stats.addValue(f.apply(i).doubleValue());
	}

	
	 SummaryStatistics getStats(){
		 return stats;
	 }
	
	StatsWrap<T,N> duplicate(){
		return new StatsWrap<>(f); //No need to duplicate the function
	}

	public String description() {
		return f.description();
	}

}
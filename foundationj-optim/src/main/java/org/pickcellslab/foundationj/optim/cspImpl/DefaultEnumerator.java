package org.pickcellslab.foundationj.optim.cspImpl;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;
import org.pickcellslab.foundationj.optim.csp.Enumerator;
import org.pickcellslab.foundationj.optim.csp.Solver;

public class DefaultEnumerator <N extends Node<L>, L extends Edge<? extends N>> implements Enumerator<N, L> {


	private final SpanPolicy policy;
	private final Solver<N,L> node;
	
	private final List<Collection<Solver<N,L>>> solvers =  new ArrayList<>();


	public DefaultEnumerator(SpanPolicy policy, Solver<N,L> node){
		this.policy = policy;
		this.node = node;
	}

	@Override
	public void initialise() {	

		solvers.clear();
		
		ICombinatoricsVector<Solver<N,L>> initialVector = 
				Factory.createVector(node.potentialTargets());

		// Generate for each spanning options
		Generator<Solver<N,L>> gen = null;
		if(policy.ordering()==ordering.ASCENDING)
			for(int i = policy.min(); i<=policy.max();i++ ){
				gen = Factory.createSimpleCombinationGenerator(initialVector, i);
				gen.forEach(v-> solvers.add(v.getVector()));

			}
		else
			for(int i = policy.max(); i>=policy.min();i-- ){
				gen = Factory.createSimpleCombinationGenerator(initialVector, i);				
				gen.forEach(v-> solvers.add(v.getVector()));
			}
		
	}



	public SpanPolicy spanning() {
		return policy;
	}



	@Override
	public Iterator<Collection<Solver<N, L>>> iterator() {
		if(policy.ordering() == ordering.RANDOM)
			Collections.shuffle(solvers);
		return solvers.iterator();
	}



	public enum ordering{
		ASCENDING,DESCENDING,RANDOM
	}


	public static class SpanPolicy{

		private final int min;
		private final int max;
		private final ordering o;

		public SpanPolicy(int min, int max, ordering o){
			this.min = min;
			this.max = max;
			this.o = o;
		}

		public int min(){
			return min;
		}
		public int max(){
			return max;
		}
		public ordering ordering(){
			return o;
		}

	}

}

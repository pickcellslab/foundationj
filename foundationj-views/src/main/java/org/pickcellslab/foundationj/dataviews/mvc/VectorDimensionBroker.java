package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.events.Modif;
import org.pickcellslab.foundationj.dataviews.fitting.AbstractSelectableFittableBroker;
import org.pickcellslab.foundationj.dataviews.fitting.DefaultFitResult;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.DistributionFitter;
import org.pickcellslab.foundationj.dataviews.fitting.DistributionFitterFactory;
import org.pickcellslab.foundationj.dataviews.fitting.FitConfig;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.fitting.FittingMethod;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;




public final class VectorDimensionBroker<T> extends AbstractSelectableFittableBroker<T,double[], double[]> 
implements FittableDimensionBroker<T,double[], double[]>, TransformableDimensionBroker<T,double[]>{

	private final DimensionalDataSet<T,double[]> dataset;

	private final List<DistributionFitterFactory<?,double[],?,?>> distriFactories;	
	private final Map<String, FitResult<? extends Distribution<double[]>>> distributions = new HashMap<>();

	private final TransformFactory<T, double[]> transFactory;

	private final List<Dimension<T,double[]>> transforms;



	public VectorDimensionBroker(List<Dimension<T,double[]>> containers,
			DimensionalDataSet<T,double[]> meta,
			TransformFactory<T,double[]> transFactory,
			List<DistributionFitterFactory<?,double[],?,?>> fitters
			) {
		super(containers);		
		Objects.requireNonNull(meta,"Meta is null");
		Objects.requireNonNull(transFactory,"transFactory is null");
		this.dataset = meta;
		transforms = new ArrayList<>(containers.size());
		for(int i = 0; i<containers.size();i++)
			transforms.add(null);
		this.transFactory = transFactory;
		this.transFactory.setDataSet(meta);
		distriFactories = fitters;
	}


	



	@Override
	public int numFittableDimensions() {
		return numDimensions();
	}


	@Override
	public Dimension<T, double[]> fittableDimension(int i) {
		return dimension(i);
	}


	@Override
	public FittingMethod<?> getFittingMethod(int i) {
		return distriFactories.get(i).method();
	}


	@Override
	public int numFittingMethods() {
		return distriFactories.size();
	}


	@Override
	public void fitRequest(String query, int method, int dim, int... dims) throws DataAccessException {

		Optional<?> config = distriFactories.get(method).method().config();
		if(!config.isPresent())
			return;

		if(dims.length!=0)
			throw new IllegalArgumentException("More than one vector dim is unsupported");



			ResultAccess<String,List<double[]>> result = dataset.loadQuery(dataset.queryIndex(query), new VectorAction<>(dimension(dim)));
			DistributionFitter<double[], ?, ?> fitter = distriFactories.get(method).createFitter();

			for(int r = 0; r<result.size(); r++){

				String fitId = "Fit : "+result.getKey(r);

				@SuppressWarnings({ "rawtypes", "unchecked" })
				FitResult<? extends Distribution<double[]>> fr =  new DefaultFitResult(
						fitter.fit(result.getResult(r), (FitConfig) config.get()),
						fitter.lastFitInfos(),
						fitId,
						dimension(dim)
						);

				distributions.put(fitId, fr);

				this.fireFittingEvent(new FittingEvent(this, Modif.ADDED, fitId,r));
			}

		


	}


	@Override
	public void deleteFit(String prefix) {

		for(String s : new ArrayList<>(distributions.keySet())){
			if(s.startsWith(prefix)){
				distributions.remove(s);
				this.fireFittingEvent(new FittingEvent(this,Modif.DELETED,s,0));
			}
		}

	}



	@Override
	public FitResult<? extends Distribution<double[]>> getDistributionFit(String id) {
		return distributions.get(id);
	}


	@Override//Override the method to apply the transform if necessary
	public Dimension<T,double[]> dimension(int i) {	
		Dimension<T,double[]> d = transforms.get(i);
		if(null == d)
			d = containers.get(i);
		return d;
	}




	@Override
	public int numAvailableTransforms(int dimension) {
		return transFactory.numAvailableTransforms();
	}

	@Override
	public void removeTransform(int dimension) {
		transforms.set(dimension,null);
		this.fireTransformChanged();
	}


	@Override
	public void setTransform(int dimension, int transform) {
		transFactory.create(this.containers.get(dimension), transform)
		.ifPresent(d->{
			//	System.out.println("VectorDimensionBroker: Setting transform ->"+d.description());
			transforms.set(dimension, d);
			this.fireTransformChanged();
		});
	}


	@Override
	public String transformName(int t) {
		return transFactory.transformName(t);
	}


	@Override
	public String transformDescription(int t) {
		return transFactory.transformDescription(t);
	}





	protected List<TransformChangeListener<T,double[]>> lstrs = new ArrayList<>();


	@Override
	public void addTransformChangeListener(TransformChangeListener<T,double[]> lst) {
		lstrs.add(lst);
	}

	@Override
	public void removeTransformChangeListener(TransformChangeListener<T,double[]> lst) {
		lstrs.remove(lst);
	}


	protected void fireTransformChanged(){
		lstrs.forEach(l->l.transformChanged(this));
	}






}

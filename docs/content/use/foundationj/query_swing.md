+++

title ="Understanding Queries"
weight = 2


date = "2018-07-23"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2018-07-23"

repo = "foundationj"
fileUrl = "https://framagit.org/pickcellslab/foundationj/tree/feature/refactoring"

tags = ["user interface"]

+++



{{% panel theme="warning" header="Draft" %}}

This page is just a place holder for now. It will contain descriptions and examples to learn how to use the graphical query builders provided by foundationj. 

{{% /panel %}}

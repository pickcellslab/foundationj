package org.pickcellslab.foundationj.datamodel.reductions;

import java.lang.reflect.Array;

import org.apache.commons.math3.stat.descriptive.StorelessUnivariateStatistic;
import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="SingleStatisticsArray")
public class SingleStatisticsArray implements ExplicitReduction<DataItem, double[]> {

	@Supported
	private final ExplicitFunction<DataItem, ?> dim;
	private final StatsType statsType;

	@Ignored
	private StorelessUnivariateStatistic[] stats;


	public SingleStatisticsArray(ExplicitFunction<DataItem, ?> dim, StatsType st) {
		MappingUtils.exceptionIfNullOrInvalid(dim);
		// Ensure that the return type if numeric
		final Class<?> compType = dim.getReturnType().getComponentType();
		if(compType==null)
			throw new IllegalArgumentException("The return type of the provided function is not an array");
		if(!Number.class.isAssignableFrom(AKey.noPrimitiveClass(compType)))
			throw new IllegalArgumentException("The provided DataFunction return an unsupported type : "+compType);

		this.dim = dim;
		this.statsType = st;
	}


	@Override
	public void root(DataItem root, boolean included) {
		if(included)
			include(root);		
	}


	@Override
	public void include(DataItem data) {
		final Object o = dim.apply(data);
		if(o!=null) {
			if(stats==null)
				initMeanArray(Array.getLength(o));

			//Check for NaN
			final double[] d = new double[stats.length];
			for(int i = 0; i<stats.length; i++) {
				d[i] = Array.getDouble(o,i);
				if(Double.isNaN(d[i]))
					return;
			}
			for(int i = 0; i<stats.length; i++)
				stats[i].increment(d[i]);
		}
	}

	@Override
	public double[] getResult() {
		if(stats==null)
			return new double[] {};
		final double[] gk = new double[stats.length];
		for(int j = 0; j<stats.length; j++)		
			gk[j] = stats[j].getResult();
		return gk;
	}





	private void initMeanArray(int length) {		
		stats = new StorelessUnivariateStatistic[length];
		for(int i = 0; i<stats.length; i++)
			stats[i] = statsType.get();
	}


	@Override
	public SingleStatisticsArray factor() {
		return new SingleStatisticsArray(dim, statsType);
	}



	@Override
	public Class<double[]> getReturnType() {
		return double[].class;
	}


	@Override
	public String description() {
		return statsType.name()+" of "+dim.toString();
	}

}

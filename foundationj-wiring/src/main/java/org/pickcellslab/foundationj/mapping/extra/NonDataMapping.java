package org.pickcellslab.foundationj.mapping.extra;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;


/**
 * Provides static methods to convert objects into a human readable String form and to restore java
 * objects from the String form.
 * <br>
 * <br><b>NB:</b> Only Objects whose class is annotated with @{@link Mapping} are supported.
 * 
 * 
 * @author Guillaume Blin
 *
 */
public abstract class NonDataMapping {

	

	protected static NonDataMapper mapperImpl;
	private static Set<Class<?>> supported;

	
	protected NonDataMapping(NonDataMapper mapperImpl, Set<Class<?>> mappedClass, Collection<Class<?>> mappingDenied) {
		
		// The presence of this abstract class is to enable static access to mapping of non data objects
		// Bootstrapper (in foundationj-boot) sets this up and clients should not have to worry about the initialisation
		// of this class (they just need to use @Mapping.
		// Not pretty but check that not already instantiated
		if(NonDataMapping.mapperImpl!=null || supported!=null) {
			throw new RuntimeException("Only one NonDataMapping object allowed - BootStrapper already provides this instance"); 
		}
		// Null checks
		Objects.requireNonNull(mapperImpl, "NonDataMapper must not be null");
		NonDataMapping.mapperImpl = mapperImpl;
		Objects.requireNonNull(mappedClass, "mappedClass cannot be null");
		supported = mappedClass;	
		
		// Ensure that there are no duplicate ids
		final Set<String> set = new HashSet<>();
		mappedClass.stream().map(m->m.getAnnotation(Mapping.class).id()).forEach(c->{
			if(!set.add(c))
				throw new RuntimeException("Assembly defines a class path with classes having duplicate mappingIds: "+c);
		});
		
		// Perform configuration
		for(Class<?> mappable : mappedClass) {
			
			// Get the annotation
			Mapping an = mappable.getAnnotation(Mapping.class);
			
			// Does the class require a custom converter?
			if(an.converter() != Mapping.Default.class) {
				mapperImpl.setMappingWithCustomConverter(mappable, an.id(), an.converter());
			}
			else
				mapperImpl.setMapping(mappable, an.id());
			
			// Are there ignored fields
			for(Field f : mappable.getDeclaredFields()) {
				if(f.getAnnotation(Ignored.class) != null) {
					mapperImpl.ignoreField(mappable, f.getName());
				}
			}
			
		}
		
		for(Class<?> toDeny : mappingDenied)
			mapperImpl.denyMappingFor(toDeny);
		
	}

	

	/**
	 * @param object The object to represent as a String
	 * @return A String representation of the object which can be used with {@link #frestoreFromString(String)} 
	 * to recreate the java object instance.
	 * @throws NonDataMappingException if the object is not supported
	 */
	public static String convertToString(Object object) throws NonDataMappingException{
		if(supported.contains(object.getClass()))
			return mapperImpl.convertToString(object);
		else
			throw new NonDataMappingException(object.getClass() +"is unsupported. Is this class annotated with Mapping?", null);
	};

	/**
	 * @param str A String (obtained with {@link #convertToString(Object)}) representing a java object to recreate
	 * @return The java object represented by the given String
	 * @throws NonDataMappingException if the string is not valid
	 */
	public static Object restoreFromString(String str) throws NonDataMappingException{
		return mapperImpl.restoreFromString(str);
	}




}
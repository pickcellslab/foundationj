package org.pickcellslab.foundationj.mapping.exceptions;

import org.pickcellslab.foundationj.mapping.data.InstantiationHook;

public class InstantiationHookException extends Exception {

	public InstantiationHookException(Class<? extends InstantiationHook> hookClass, Class<?> typeToInstantiate, Exception e) {
		super("The MetaInstantiationManager was unable to create a new instance of "+typeToInstantiate, e);
	}

}

package org.pickcellslab.foundationj.bootstrap;

import java.util.List;
import java.util.stream.Stream;

/**
 * Container for Class discovered to be annotated with @{@link Data}
 * 
 * @author Guillaume Blin
 *
 */
public class DiscoveredData {

	private final List<Class<?>> dataTypes;
	
	public DiscoveredData(List<Class<?>> dataTypes) {
		this.dataTypes = dataTypes;
	}
	
	Stream<Class<?>> discovered(){
		return dataTypes.stream();
	};
	
}

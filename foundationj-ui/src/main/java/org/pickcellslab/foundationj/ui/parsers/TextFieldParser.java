package org.pickcellslab.foundationj.ui.parsers;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.text.ParseException;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextFieldParser {

	private static Logger log = LoggerFactory.getLogger(TextFieldParser.class);
	
	
	private interface SupportedType{		

		/**
		 * 
		 * @return An instance of this SupportedType class as described by the specified value
		 */
		public Object giveMeInstance(String value);

		/**
		 * @return the class this SupportedType represents. This avoid the use of Class.forName(String)
		 */
		public Class<?> giveMeClass();



	}


	public enum Supported implements SupportedType{
		CLASS(Class.class.getTypeName()){
			@Override
			public Object giveMeInstance(String value) {
				try {
					return Class.forName(value);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			public Class<?> giveMeClass() {
				return Class.class;
			}
		},
		CLASS_ARRAY(Class[].class.getTypeName()){
			@Override
			public Object giveMeInstance(String value) {
				try {
					String[] names = value.split(",");				

					Class<?>[] instance = new Class<?>[names.length];		
					for(int i = 0; i<names.length; i++){
						log.debug(names[i]);
					}

					for(int i = 0; i<names.length; i++){

						instance[i] = Class.forName(names[i].substring(6, names[i].length()));
					}
					return instance;
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			public Class<?> giveMeClass() {
				return Class[].class;
			}
		},
		AKEY(AKey.class.getTypeName())
		{
			@Override
			public Object giveMeInstance(String value) {
				return AKey.get(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return AKey.class;
			}
		},
		DIRECTION(Direction.class.getTypeName())
		{
			@Override
			public Object giveMeInstance(String value) {				
				return valueOf(Direction.class,value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Direction.class;
			}
		},
		STRING(String.class.getTypeName())
		{
			@Override
			public Object giveMeInstance(String value) {
				return value;
			}

			@Override
			public Class<?> giveMeClass() {
				return String.class;
			}
		},
		STRING_ARRAY(String.class.getTypeName()){
			@Override
			public Object giveMeInstance(String value) {
				return value.split(",");
			}

			@Override
			public Class<?> giveMeClass() {
				return String[].class;
			}
		},
		NUMBER(Number.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(String value) {				
				return Double.parseDouble(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Number.class;
			}
			
		},
		DOUBLE(double.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(String value) {				
				return Double.parseDouble(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Double.class;
			}
			
		},
		DOUBLE_ARRAY(double[].class.getTypeName())
		{

			@Override
			public Object giveMeInstance(String value) {
				String[] str = value.split(",");
				double[] v = new double[str.length];
				for(int i = 0; i<str.length; i++)
					 v[i] = Double.parseDouble(str[i]);
				return v;
			}

			@Override
			public Class<?> giveMeClass() {
				return double[].class;
			}
			
		},
		FLOAT(float.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(java.lang.String value) {				
				return Float.parseFloat(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Float.class;
			}
			
		},
		FLOAT_ARRAY(float[].class.getTypeName())
		{

			@Override
			public Object giveMeInstance(String value) {
				String[] str = value.split(",");
				float[] v = new float[str.length];
				for(int i = 0; i<str.length; i++)
					 v[i] = Float.parseFloat(str[i]);
				return v;
			}

			@Override
			public Class<?> giveMeClass() {
				return float[].class;
			}
			
		},
		LONG(long.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(java.lang.String value) {				
				return Long.parseLong(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Long.class;
			}
			
		},
		LONG_ARRAY(long[].class.getTypeName())
		{

			@Override
			public Object giveMeInstance(String value) {
				String[] str = value.split(",");
				long[] v = new long[str.length];
				for(int i = 0; i<str.length; i++)
					 v[i] = Long.parseLong(str[i]);
				return v;
			}

			@Override
			public Class<?> giveMeClass() {
				return long[].class;
			}
			
		},
		INTEGER(int.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(java.lang.String value) {				
				return Integer.parseInt(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Integer.class;
			}
			
		},
		SHORT(short.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(java.lang.String value) {				
				return Short.parseShort(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Short.class;
			}
			
		},
		BYTE(byte.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(java.lang.String value) {				
				return Byte.parseByte(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Byte.class;
			}
			
		},
		BOOLEAN(boolean.class.getTypeName())
		{

			@Override
			public Object giveMeInstance(java.lang.String value) {				
				return Boolean.parseBoolean(value);
			}

			@Override
			public Class<?> giveMeClass() {
				return Boolean.class;
			}
			
		};


		private String s;

		Supported(String s){
			this.s = s;
		}


		public String definition(){
			return s;
		}



	}


	private static class GenericSupport implements SupportedType{

		private final Class<?> generic;
		private Supported actual;

		GenericSupport(String generic, String actual) throws Exception{
			
			log.debug("generic = "+generic);
			log.debug("actual = "+actual);		
			
			
			this.generic = Class.forName(generic);

			//Get the Supported enum associated with the actual type
			boolean isSupported = false;

			for(Supported s : Supported.values()){
				if(actual.equals(s.definition())){
					log.debug("Captured Supported Type : " + s.definition());
					this.actual = s;
					isSupported = true;
					break;
				}
			}
			if(!isSupported)
				throw new Exception();
		}

		@Override
		public Object giveMeInstance(String value) {			
			return actual.giveMeInstance(value);
		}

		@Override
		public Class<?> giveMeClass() {
			return generic;
		}



	}
	
	
	public static <E>  E parse(String text, Class<E> clazz) throws ParseException{
					
				
			String typeName = clazz.getTypeName(); 
			
			for(Supported s : Supported.values()){
				if(typeName.equals(s.definition())){
					log.debug("Captured Supported Type : " + s.definition());
					try{
					return (E) s.giveMeInstance(text);			
					}catch(Exception e){
						throw new ParseException("The provided text cannot be parsed to "+ clazz.getTypeName(), 0);
					}
				}
			}
				throw new UnsupportedOperationException("Parsing class: "+typeName);
		
		
	}


	public <E> ExplicitPredicate<E> parsePredicate(String text, Class<E> clazz) throws ParseException{
		throw new ParseException("Not yet implemented", 0);
		// TODO 
		
	}
	
	
}

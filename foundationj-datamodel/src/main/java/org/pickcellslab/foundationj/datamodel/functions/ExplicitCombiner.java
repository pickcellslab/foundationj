package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.ExplicitOperation;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/**
 *	A {@link ExplicitCombiner} combines 2 inputs in order to return a result. Like {@link ExplicitPredicate},
 * {@link ExplicitFunction} and {@link ExplicitReduction}, {@link ExplicitCombiner} are meant to be {@link Mapping}
 * compatible
 * <br> 
 * <b>NB:</b> Implementing class MUST be annotated with @{@link Mapping}.
 * 
 * @param <I1> The type of the first input accepted by the {@link ExplicitCombiner}
 * @param <I2> The type of the second input accepted by the {@link ExplicitCombiner}
 * @param <O> The type of result returned by the {@link ExplicitCombiner}
 * 
 * @author Guillaume Blin
 */
public interface ExplicitCombiner<I1,I2,O> extends ExplicitOperation {

	/**
	 * Combines input1 and input2 into an output
	 * @param input1
	 * @param input2
	 * @return The output of the combine operation
	 */
	public O combine(I1 input1, I2 input2);
	
	
	public Class<O> getReturnType();
	
	
	/**
	 * Adds a transformation defined by the given {@link ExplicitFunction} on the result of this {@link ExplicitCombiner}
	 * @param function A {@link ExplicitFunction} defining the transformation to apply on the result of this {@link ExplicitCombiner}
	 * @return The transformed {@link ExplicitCombiner}
	 */
	public default <R> ExplicitCombiner<I1,I2,R> andThen(ExplicitFunction<? super O, R> function){
		MappingUtils.exceptionIfNullOrInvalid(function);
		return new TransformedCombiner<I1,I2,O,R>(this, function);
	}
	
	
	
	
	/**
	 * @return A description for this DataCombiner
	 */
	public String description();
	
	@Mapping(id="TransformedCombiner")
	public static class TransformedCombiner<I1, I2, O, R> implements ExplicitCombiner<I1,I2,R>{

		@Supported
		private final ExplicitCombiner<I1, I2, O> combiner;
		@Supported
		private final ExplicitFunction<? super O, R> function;
		
		public TransformedCombiner(ExplicitCombiner<I1, I2, O> combiner, ExplicitFunction<? super O, R> function) {
			this.combiner = combiner;
			this.function = function;
		}
		

		@Override
		public R combine(I1 input1, I2 input2) {
			return function.apply(combiner.combine(input1, input2));
		}

		@Override
		public String description() {
			return function.description()+" "+combiner.description();
		}


		@Override
		public Class<R> getReturnType() {
			return function.getReturnType();
		}
		
	}
	
}

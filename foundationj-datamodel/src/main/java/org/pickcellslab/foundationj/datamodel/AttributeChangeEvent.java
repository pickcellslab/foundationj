package org.pickcellslab.foundationj.datamodel;


/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * An {@link AttributeChangeEvent} may be thrown by {@link ListenableDataItem}s when one of its property is changed or deleted. 
 *
 */
public final class AttributeChangeEvent {

  private final WritableDataItem source;
  private final MODE mode;
  private final AKey<?> key;
  private Object oldValue, newValue;
  
  public enum MODE{
    UPDATED, ADDED, DELETED
  }
  
  private AttributeChangeEvent(AEventBuilder builder) {
    this.source = builder.source;
    this.mode = builder.mode;
    this.key = builder.key;
    this.oldValue = builder.oldValue;
    this.newValue = builder.newValue;
  }

  public WritableDataItem getSource() {
    return source;
  }
  public MODE getMode() {
    return mode;
  }

  public Object getOldValue() {
    return oldValue;
  }
  public Object getNewValue() {
    return newValue;
  }
  public AKey<?> getAKey() {
    return key;
  }
  
  
  public static class AEventBuilder {

    private final WritableDataItem source;
    private final MODE mode;
    private final AKey<?> key;
    private Object oldValue, newValue;
    
    private boolean newIsSet = false;
    private boolean oldIsSet = false;

    public AEventBuilder(WritableDataItem source, AKey<?> key, MODE mode) {
      this.source = source;
      this.mode = mode;
      this.key = key;
    }

    public AEventBuilder setOldValue(Object oldValue) {
      this.oldValue = oldValue;
      oldIsSet = true;
      return this;
    }
    
    public AEventBuilder setNewValue(Object newValue) {
      this.newValue = newValue;
      newIsSet = true;
      return this;
    }
    
    public AttributeChangeEvent build() {
      if (mode == MODE.ADDED) {
        if (!newIsSet)
          throw new IllegalStateException(
              "a new value must be spcified when creating AEvent with mode ADDED");
      }
      else if (mode == MODE.UPDATED) {
        if (!(newIsSet && oldIsSet))
          throw new IllegalStateException(
              "a new value and an old value must be specified when creating AEvent with mode UPDATED");
      }
      return new AttributeChangeEvent(this);
    }
  }
  
}

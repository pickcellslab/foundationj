package org.pickcellslab.foundationj.datamodel.functions;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Function;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.ExplicitOperation;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.predicates.BiFunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.FunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.datamodel.predicates.ValuePredicate;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;



/**
 * ExplicitFunctions are {@link Function}s designed to be storable as part of the user-defined description of the data property graph.
 * To fulfill this role, ExplicitFunction objects build and store a human readable description of the operation they perform.
 * <br><br>
 * <b>NB:</b> To facilitate storage of the object extending this interface, one must avoid the use of anonymous class, to prevent the storage
 * of reference to the outer class. Implementing classes should be annotated with {@link Mapping}
 * 
 * @param <D> The type of the input value.
 * @param <R> the type of the returned value.
 * 
 * @see F 
 */
public interface ExplicitFunction<D,R> extends Function<D, R>, ExplicitOperation{

	
	
	/**
	 * @return The type of the object returned by this function
	 */
	public Class<R> getReturnType();
	
			
	/**
	 * @return An array of {@link ExplicitOperator} which may be used on the returned type of
	 * this function when the result of this function is on the left hand side of the operator
	 */
	public default ExplicitOperator<?,?>[] getOperatorCompatibility(){
		return Op.findLeftValid(getReturnType());
	}
	
	
	public default <E> ExplicitFunction<D,E> next(ExplicitFunction<? super R, E> other){
		return new Composed<>(this,other);
	}
	
	
	/**
	 * @param value A value to evaluate
	 * @return An {@link ExplicitPredicate} which evaluates whether the value returned by this function
	 * is equal to the provided value.
	 */
	public default ExplicitPredicate<D> equalsTo(R value){
		return new FunctionPredicate<>(this, new ValuePredicate<>(Op.ObjectOp.EQUALS, value));
	}
	
	/**
	 * @param value A value to evaluate
	 * @return An {@link ExplicitPredicate} which evaluates whether the value returned by this function
	 * is equal to the provided value.
	 */
	public default ExplicitPredicate<D> distinctFrom(R value){
		return new FunctionPredicate<>(this, new ValuePredicate<>(Op.ObjectOp.DISTINCT, value));
	}
	
	
	/**
	 * @param o The {@link ExplicitOperator} to use for the evaluation
	 * @param value A value to evaluate
	 * @return An {@link ExplicitPredicate} which evaluates whether the value returned by this {@link ExplicitFunction}
	 * satisfies the provided operator / value combination.
	 */
	public default <O> ExplicitPredicate<D> checkIf(ExplicitOperator<R,O> o, O value){
		return new BiFunctionPredicate<>(
				this
				, new Constant<>(value), 
				o);
	}
	
	/**
	 * @param predicate An {@link ExplicitPredicate} used to evaluate the value by this {@link ExplicitFunction}
	 * @return An {@link ExplicitPredicate} which evaluates whether the value returned by this {@link ExplicitFunction}
	 * satisfies the provided {@link ExplicitPredicate}.
	 */
	public default ExplicitPredicate<D> satisfies(ExplicitPredicate<? super R> predicate){
		return new FunctionPredicate<>(this, predicate);
	}

	
	
	@Mapping(id = "Composition")
	static class Composed<I,B,O> implements ExplicitFunction<I,O>{

		@Supported
		private final ExplicitFunction<I,B> first;
		@Supported
		private final ExplicitFunction<? super B,O> second;
		
		
		Composed(ExplicitFunction<I,B> first, ExplicitFunction<? super B,O> second){
			MappingUtils.exceptionIfNullOrInvalid(first);
			MappingUtils.exceptionIfNullOrInvalid(second);
			this.first = first;
			this.second = second;
		}
		
		@Override
		public O apply(I t) {
			return (O) second.apply(first.apply(t));
		}

		@Override
		public String description() {
			return first.description()+" and then "+second.description();
		}
		
		@Override
		public String toString() {
			return first.toString()+" and then "+second.toString();
		}

		@Override
		public Class<O> getReturnType() {
			return second.getReturnType();
		}
		
	}
	
	
}

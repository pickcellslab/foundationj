package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;
import java.util.Objects;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequenceHints;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.DataToIconID;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

@SuppressWarnings("serial")
public class GraphSequenceFunctionUI extends WizardNode {

	private final ConsiderAllPanel pathPanel;
	private final MetaClass generator;
	private final DataAccess access;
	
	public GraphSequenceFunctionUI(WizardNode parent, UITheme theme, DataAccess access, MetaClass generator) {

		super(parent);

		Objects.requireNonNull(access);
		Objects.requireNonNull(generator);

		this.access = access;
		this.generator = generator;
		
		setPreferredSize(new Dimension(650,550));
		setLayout(new BorderLayout());


		List<MetaQueryable> choices = GraphSequenceHints.getHints((Class<? extends GraphSequencePointer>) generator.itemClass(access.dataRegistry())).encounteredQueryable(access, generator.itemDeclaredType());
		//Choice view				
		ChoiceSet choiceView = null;
		try {
			choiceView = new ChoiceSet(theme, access, mq->choices.contains(mq));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}


		// In the West add MetaModel types, filters and existing Attributes.

		JPanel westPanel = new JPanel(new BorderLayout());


		//Available choices ScrollPane
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.WEST);		
		scrollPane.setViewportView(choiceView);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JLabel choiceLabel = new JLabel("MetaModel Browsing :");
		choiceLabel.setBorder(new EmptyBorder(10,10,10,10));
		Font lFont = choiceLabel.getFont();
		choiceLabel.setFont( new Font(lFont.getFontName(), Font.BOLD, lFont.getSize()));
		choiceLabel.setIcon(theme.icon(DataToIconID.get(dType.MIXED), 16));
		scrollPane.setColumnHeaderView(choiceLabel);
		westPanel.add(scrollPane,BorderLayout.CENTER);

		add(westPanel,BorderLayout.WEST);



		// In the Center : Add path definition, filter combination, Target grouping ad Reduction Operation.

		pathPanel = new ConsiderAllPanel(theme, access.dataRegistry(), choiceView,  QueryConfigs.newNoTraversalConfig("").disableGrouping().build(), true);				

		add(pathPanel, BorderLayout.CENTER);

		pathPanel.addValidityListener(l->this.fireNextValidityChanged());




	}





	@Override
	public boolean validity() {
		return pathPanel.validity();
	}

	@Override
	protected WizardNode getNextStep() {		
		
		return new MetaDescriptionPanel<>(
				this, 
				access.metaModel().getInstanceManager(),
				generator,
				pathPanel.getMetaFilter(access.metaModel()).orElse(null),
				pathPanel.getMechanism(access.metaModel()).get(),
				pathPanel.getTarget());
		
	}


	@Override
	public int numberOfForks() {
		return 1;
	}

}

package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;

/**
 * 
 * A simple dialog for the user to choose a MetaQueryable among the one currently existing in the database
 * 
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("serial")
public class QueryableChoiceDialog<T extends MetaQueryable> extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private boolean wasCancelled = true;


	private final JComboBox<T> comboBox = new JComboBox<>();
	private final JComboBox<MetaFilter> filterBox = new JComboBox<>();

	private final String message;
	private final List<T> list;

	private final boolean allowFiltering;

	private final DataRegistry registry;
	private final UITheme theme;



	public QueryableChoiceDialog(UITheme theme, DataRegistry registry, T mq, String message, boolean allowFiltering) {
		Objects.requireNonNull(mq, "The provided MetaQueryable must not be null");
		Objects.requireNonNull(theme);
		Objects.requireNonNull(registry);		
		this.registry = registry;
		this.theme = theme;
		this.list = new ArrayList<>(1);
		list.add(mq);
		this.allowFiltering = allowFiltering;
		this.message = message;
		buildDialog();
	}

	public QueryableChoiceDialog(UITheme theme, DataRegistry registry, List<T> list, String message, boolean allowFiltering) {
		Objects.requireNonNull(list, "The provided list must not be null");
		Objects.requireNonNull(theme);
		Objects.requireNonNull(registry);		
		this.registry = registry;
		this.theme = theme;
		this.list = list;
		this.allowFiltering = allowFiltering;
		this.message = message;
		buildDialog();
	}

	/**
	 * Creates the dialog
	 * @param session The session allowing to access the data
	 * @param message The message indicating to the user what the choice is about
	 * @param predicate A {@link Predicate} to filter non allowed choices
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public QueryableChoiceDialog(UITheme theme, DataAccess session, String message, Predicate<MetaQueryable> predicate, boolean allowFiltering) {

		Objects.requireNonNull(theme);
		Objects.requireNonNull(session, "The provided Session must not be null");

		this.theme = theme;
		this.registry = session.dataRegistry();
		this.allowFiltering = allowFiltering;
		this.message = message;

		if(predicate == null){
			predicate = p->true;
		}

		list = (List)session.metaModel().getQueryable(predicate);
		if(list.isEmpty()) {
			JOptionPane.showMessageDialog(null, "The database does not yet contain objects compatible with this analysis");
			this.dispose();
			return;
		}	


		buildDialog();
	}







	@SuppressWarnings("unchecked")
	private void buildDialog() {

		for(T m : list)
			comboBox.addItem(m);


		JLabel lblNewLabel = new JLabel(message);		
		lblNewLabel.setAlignmentX(SwingConstants.LEFT);
		lblNewLabel.setBorder(new EmptyBorder(10,10,10,10));
		comboBox.setRenderer(new MetaRenderer(theme, registry));

		JLabel lblFilter = new JLabel("Use Filter");
		lblFilter.setAlignmentX(SwingConstants.LEFT);
		lblFilter.setBorder(new EmptyBorder(10,10,10,10));
		filterBox.setRenderer(new MetaRenderer(theme, registry));


		JButton okButton = new JButton("OK");
		okButton.addActionListener(l->{			
			wasCancelled = false;
			this.dispose();
		});



		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(l->this.dispose());




		//Layout
		//-----------------------------------------------------------------------------------------------------------

		
		setTitle("Queryable Type choice");
		//setBounds(100, 100,320, 250);
		getContentPane().setLayout(new BorderLayout());
		
		BoxLayout bl = new BoxLayout(contentPanel,BoxLayout.Y_AXIS);
		contentPanel.setLayout(bl);
		contentPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		contentPanel.add(lblNewLabel);
		contentPanel.add(comboBox);

		if(allowFiltering){
			contentPanel.add(lblFilter);
			contentPanel.add(filterBox);
			filterBox.addItem(null);
			((T)comboBox.getSelectedItem()).filters().forEach(mf->filterBox.addItem(mf));
			comboBox.addActionListener(l->{
				filterBox.removeAllItems();
				filterBox.addItem(null);
				((T)comboBox.getSelectedItem()).filters().forEach(mf->filterBox.addItem(mf));
			});
		}
		
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);

			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
			buttonPane.add(cancelButton);

		}
	}

	/**
	 * @return {@code true} if the dialog was cancelled or closed directly
	 */
	public boolean wasCancelled(){
		return wasCancelled;
	}

	/**
	 * @return The Queryable selected by the user, will be empty if cancelled
	 */
	@SuppressWarnings("unchecked")
	public Optional<T> getChoice(){
		return Optional.ofNullable((T)comboBox.getSelectedItem());
	}


	/**
	 * @return The {@link MetaFilter} selected by the user, will be empty if cancelled
	 */
	public Optional<MetaFilter> getFilter(){
		return Optional.ofNullable((MetaFilter)filterBox.getSelectedItem());
	}
	



}


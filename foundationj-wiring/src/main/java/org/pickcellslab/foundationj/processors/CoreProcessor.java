package org.pickcellslab.foundationj.processors;

import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Service;


public class CoreProcessor extends FoundationJProcessor {



	protected boolean contractRespected(TypeElement type) {

		//--------------------------------------------------------------------//
		// First make sure that the annotation is on an interface
		//--------------------------------------------------------------------//

		if(!ProcessorsUtils.checkAbstract(Core.class, type, processingEnv))
			return false;

		//--------------------------------------------------------------------//
		// Ensure that @Scope or @SameScopeAs or @Service is present
		//--------------------------------------------------------------------//
		if(type.getAnnotation(Scope.class)==null
				&& type.getAnnotation(SameScopeAs.class)==null
				&& type.getAnnotation(Service.class)==null){
			processingEnv.getMessager().printMessage(Kind.ERROR, "@Core must also be annotated with either @Scope, "
					+ "@SameScopeAs or @Service", type);
			return false;
		}



		return true;

	}




	@Override
	protected Class<Core> annotation() {
		return Core.class;
	}


	@Override
	protected String metadatapath() {
		return "META-INF/foundationj/core";
	}



}

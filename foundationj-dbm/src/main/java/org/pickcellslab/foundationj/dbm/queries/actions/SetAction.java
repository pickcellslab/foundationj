package org.pickcellslab.foundationj.dbm.queries.actions;

import java.util.HashSet;
import java.util.Set;

import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;

class SetAction<T, E> extends AbstractAction<T, Set<E>> {

	
	private final ExplicitFunction<T, E> f;


	public SetAction(ExplicitFunction<T,E> f){
		super("Create a Set for "+f.description());
		this.f = f;
	}




	@Override
	protected ActionMechanism<T, Set<E>> create(String key) {
		return new SetMechanism();
	}
	
	
	
	private class SetMechanism implements ActionMechanism<T,Set<E>>{

		private final Set<E> set = new HashSet<>();
		
		@Override
		public void performAction(T i) throws DataAccessException {
			set.add(f.apply(i));
		}

		@Override
		public Set<E> get() {
			return set;
		}
		
		
	}


}
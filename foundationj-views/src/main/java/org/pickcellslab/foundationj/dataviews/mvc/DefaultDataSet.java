package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaDataSet;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultDataSet<T,D> implements DimensionalDataSet<T,D>{

	private static final Logger log = LoggerFactory.getLogger(DefaultDataSet.class);


	private final List<QueryProgressListener> lstrs = new ArrayList<>();
	
	protected final List<MetaDataSet<T,D>> metas = new ArrayList<>();
	protected final List<QueryInfo> infos;
	
	private final List<ResultAccess<String,?>> loaded = new ArrayList<>();

	private final DataAccess access;



	public DefaultDataSet(DataAccess access, List<MetaDataSet<T,D>> metas, List<QueryInfo> infos){
		Objects.requireNonNull(access, "Access is null");
		Objects.requireNonNull(metas, "Meta is null");
		if(metas.isEmpty())
			throw new IllegalArgumentException("Empty dataset");
		this.metas.addAll(metas);
		this.metas.forEach(m->loaded.add(null));
		this.infos = infos;
		this.access = access;
	}




	@Override
	public int numQueries() {
		return metas.size();
	}

	@Override
	public String queryID(int query) {
		final String id = metas.get(query).name();
		log.trace("Requested queryID for index "+query+" = "+id);
		return id;
	}

	@Override
	public int queryIndex(String queryId) {
		for(int i = 0; i<numQueries(); i++)
			if(metas.get(i).name().equals(queryId))
				return i;
		return -1;
	}

	
	@Override
	public int firstSeriesIndex(int query) {
		int count = 0;
		for(int i = 0; i<query; i++){
			count+=loaded.get(i).size();
		}		
		return count;
	}

	@Override
	public int firstSeriesIndex(String query) {
		return firstSeriesIndex(queryIndex(query));
	}
	
	

	

	@Override
	public <O> ResultAccess<String,O> loadQuery(int query, Action<T,O> action) throws DataAccessException {
		setProgress(metas.get(query).name(),0);
		final ResultAccess<String,O> r = metas.get(query).loadDataSet(access, action);
		loaded.set(query, r);
		setProgress(metas.get(query).name(), 100);
		return r;
	}

	
	
	@Override
	public int numSeries(int query) {
		final ResultAccess<String,?> r = loaded.get(query);
		return r != null ? r.size() : 0;
	}

	@Override
	public String seriesID(int query, int i) {
		final ResultAccess<String,?> r = loaded.get(query);
		return r != null ? r.getKey(i) : null;
	}



	protected void setProgress(String query, int progress){
		lstrs.forEach(l->l.setProgress(query, progress));
	}



	@Override
	public void addQueryProgressListener(QueryProgressListener l) {
		lstrs.add(l);
	}

	@Override
	public void removeQueryProgressListener(QueryProgressListener l) {
		lstrs.remove(l);
	}

	@Override
	public QueryInfo queryInfo(int query) {
		return infos.get(query);
	}

	@Override
	public int queryIndexOf(String seriesID) {
		for(int q = 0; q<this.numQueries(); q++)
			for(int s = 0 ; s< this.numSeries(q); s++){
				if(seriesID.equals(this.seriesID(q, s)))
					return q;
			}
		return -1;
	}

	@Override
	public String queryIDOf(String seriesID) {
		for(int q = 0; q<this.numQueries(); q++)
			for(int s = 0 ; s< this.numSeries(q); s++){
				if(seriesID.equals(this.seriesID(q, s)))
					return this.queryID(q);
			}
		return null;
	}

	@Override
	public int seriesIndex(String seriesID) {
		int index = 0;
		for(int q = 0; q<this.numQueries(); q++)
			for(int s = 0 ; s< this.numSeries(q); s++){
				if(seriesID.equals(this.seriesID(q, s)))
					return index;
				index++;
			}
		return -1;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<DimensionContainer<T, D>> possibleDimensions(int query, Predicate<DimensionContainer<T, ?>> predicate) {
		return (List)metas.get(query).possibleDimensions((Predicate)predicate);
	}

	
	


}

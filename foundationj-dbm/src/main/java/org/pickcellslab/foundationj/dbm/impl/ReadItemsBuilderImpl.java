package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.queries.builders.ActionBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadItemsBuilder;

class ReadItemsBuilderImpl<T extends WritableDataItem> extends ReadBuilderImpl<T> implements ReadItemsBuilder<T> {

	
	ReadItemsBuilderImpl(TargetType<?,?,T> target){
		super(target);	
	}
	
	
	@Override
	public ActionBuilder<T, Long> count() {
		return new CounterBuilder<>(target);
	}
	
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.ReadItemsBuilder#makeList(org.pickcellslab.foundationj.datamodel.AKey)
	 */
	@Override
	public <E> ListMakerBuilder<T,E> makeList(AKey<E> k){
		return new ListMakerBuilder<>(target,k);
	}
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.ReadItemsBuilder#makeList(org.pickcellslab.foundationj.datamodel.functions.DataFunction)
	 */
	@Override
	public <E> ListMakerBuilder<T,E> makeList(ExplicitFunction<? super T,E> f){
		return new ListMakerBuilder<>(target,f);
	}
	
		
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.ReadItemsBuilder#makeTable()
	 */
	@Override
	public TableMakerBuilder<T> makeTable(){
		return new TableMakerBuilder<>(target);		
	}
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.ReadItemsBuilder#makeStats()
	 */
	@Override
	public StatsActionBuilder<T> makeStats(){
		return new StatsActionBuilder<>(target);
	}



	
}

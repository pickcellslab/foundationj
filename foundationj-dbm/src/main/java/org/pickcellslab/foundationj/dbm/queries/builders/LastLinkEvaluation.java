package org.pickcellslab.foundationj.dbm.queries.builders;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.EvaluationContainerBuilder;

/** 
 * The final step of the stepwise builder pattern which defines the 'rules' for navigating a graph. 
 * This step allows to add an evaluation on the last link encountered in the traversal before building
 * the object holding the 'rules' for navigating the graph.
 *
 * @param <T> The type of the 'rules' container
 */
public interface LastLinkEvaluation<T> extends EvaluationContainerBuilder<LastLinkEvaluation<T>> {

	
	/**
	 * Adds an evaluation on the last link encountered in the traversal before building the object holding 
	 * the 'rules' for navigating the graph.
	 * @param type The type of link to consider
	 * @param explicitPredicate An {@link ExplicitPredicate} to categorise the last link
	 * @param ifYes The {@link Decision} if {@code explicitPredicate} returns {@code true}
	 * @param ifNot The {@link Decision} if {@code explicitPredicate} returns {@code false}
	 * @return An object holding the rules for navigating the graph.
	 */
	public T finallyEvaluateLastLink(String type, ExplicitPredicate<? super Link> explicitPredicate, Decision ifYes, Decision ifNot);
	
	/**
	 * Builds the graph navigation rules. 
	 * @return An object holding the rules for navigating the graph.
	 */
	public T build();
	
}

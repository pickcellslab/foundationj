package org.pickcellslab.foundationj.dbm.access;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;

/**
 *
 * {@link DataPointer} objects provide methods to perform operations such as read, delete or update on the set of objects
 * they point to in the database.
 * By hiding the details of how the data are searched they provide client code with a simple agnostic manner
 * to perform a database operation.
 * 
 * @author Guillaume Blin
 *
 */
public interface DataPointer {
	
	/**
	 * Deletes all the data this {@link DataPointer} points at
	 * @param access Access to the database where the operation should be performed
	 * @return The number of deletions
	 * @throws DataAccessException
	 */
	public int deleteData(DataAccess access) throws DataAccessException;	
	
	/**
	 * Reads all the data object this {@link DataPointer} points at
	 * @param access Access to the database where the operation should be performed
	 * @param action An {@link Action} on the data which are going to be read
	 * @param limit A limit on the number of data object to be read, use -1 if you want no limit
	 * @return The result of the Action
	 * @throws DataAccessException
	 * 
	 * @see Actions
	 */
	public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException;
	
	
	/**
	 * Read a database Object with the given database id if the dataset defined by this {@link DataPointer} 
	 * contains it.
	 * @param access The {@link DataAccess} providing access to the database
	 * @param dbId The database id of the desired object
	 * @param action The {@link Function} to use on the encountered object.
	 * 
	 * @see Actions
	 */
	public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action) throws DataAccessException;
	
	
	public default void consumeOne(DataAccess access, int dbId, Consumer<DataItem> action) throws DataAccessException{
		readOne(access, dbId, (item)-> {action.accept(item); return null;}); 
	};
	
	
	/**
	 * Read any of the database Object this {@link DataPointer} points at 
	 * @param access The {@link DataAccess} providing access to the database
	 * @param action The {@link Function} to use on the encountered object.
	 * 
	 * @see Actions
	 */
	public <O> O readAny(DataAccess access, Function<DataItem,O> action) throws DataAccessException;
	
	public default void consumeAny(DataAccess access, int dbId, Consumer<DataItem> action) throws DataAccessException{
		readAny(access, (item)-> {action.accept(item); return null;}); 
	};
	
	
	/**
	 * Perform an Update operation on all the data this {@link DataPointer} points at.
	 * @param access Access to the database where the operation should be performed
	 * @param action The {@link Updater} which defines which update are to be performed
	 * @throws DataAccessException
	 */
	public void updateData(DataAccess access, Updater action) throws DataAccessException;	
	
	
	/**
	 * @return The list of {@link MetaQueryable} objects which hold information about the data this {@link DataPointer} points at.
	 */
	public List<MetaQueryable> queriedMetaObjects();
	
	
	/**
	 * Creates a {@link DataPointer} which points at a subset of the objects that the current instance is pointing at 
	 * @param predicate The {@link ExplicitPredicate} defining the subset
	 * @return A new {@link DataPointer} instance
	 */
	public DataPointer subset(ExplicitPredicate<DataItem> predicate);

	
}

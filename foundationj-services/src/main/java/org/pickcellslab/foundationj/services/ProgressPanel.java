package org.pickcellslab.foundationj.services;

/**
 * 
 * The UI repsonsible for displaying {@link TaskProgress}s
 * 
 * @author Guillaume Blin
 *
 * @param <V>
 */
public interface ProgressPanel<V> {

	public void setTitle(String title);
	
	public void setVisible(boolean isVisible);
	
	public void dispose();

	/**
	 * @return The {@link ProgressControl} used as model by this UI.
	 */
	public ProgressControl<V> getControl();
	
}

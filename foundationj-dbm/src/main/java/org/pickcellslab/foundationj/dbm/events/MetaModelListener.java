package org.pickcellslab.foundationj.dbm.events;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;

/**
 * Objects implementing this interface can listen for changes occurring in the MetaModel
 * of a specific {@link DataAccess} session
 * 
 * @see #metaEvent(Collection, MetaChange)
 * 
 * @see DataUpdateListener
 * 
 * @author Guillaume Blin
 *
 */
public interface MetaModelListener {

  /**
   * Describes which type of change occurred for a specific MetaItem
   *
   */
  public enum MetaChange{
    CREATED, DELETED, MODIFIED
  }

  
  /**
   * This method is called whenever a new {@link MetaModel} is inserted or deleted.
   * WARNING: DO NOT query the database from this method! (possible Deadlock) 
   * @param meta A Collection holding all the {@link MetaModel} objects concerned by the event
   * @param evt The type of change
   */
  public void metaEvent(RegeneratedItems meta, MetaChange evt);
  
  
}

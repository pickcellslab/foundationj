package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.IncludeLinksBased;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition.TraversalBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.LastLinkEvaluation;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;

/**
 * This class holds information regarding a  path in the MetaModel (A list of MetaQueryable).
 * It provides access to the order of the MetaQueryable in the path and methods to obtain
 * {@link MetaTraversal} objects from the path.
 * This object can only be obtained via {@link MetaImpl} factory method.
 * 
 * @author Guillaume Blin
 *
 */
public class MetaPath {

	private final LinkedList<Link> metaPath;
	private final LinkedHashMap<String, Direction> toDataPath = new LinkedHashMap<>();;
	private final MetaClass root;
	private final MetaQueryable target;

	public MetaPath(LinkedList<Link> metaPath, MetaClass root, MetaQueryable target){
		this.metaPath = metaPath;
		this.root = root;
		this.target = target;
	}


	public MetaClass rootType(){
		return root;

	}

	public MetaQueryable targetType(){
		return target;

	}

	public boolean isEmpty(){
		return metaPath.isEmpty();
	}

	/**
	 * @return An ordered map listing the link types and corresponding direction which must
	 * be included in a {@link TraversalDefinition} in order to be able to reach the target from a root
	 * of type given by {@link #rootType()}
	 */
	public LinkedHashMap<String, Direction> toDataPath(){
		if(!metaPath.isEmpty() && toDataPath.isEmpty()){
			AtomicBoolean even = new AtomicBoolean();
			even.set(true);
			metaPath.forEach(link ->{
				if(even.get()){
					String linkType = ((MetaLink)link.source()).linkType();
					Direction dir = toDataPath.get(linkType);
					if(link.declaredType().equals(MetaModel.fromLink)){
						if(dir == null)
							toDataPath.put(linkType, Direction.OUTGOING);
						else if(dir !=  Direction.OUTGOING)
							toDataPath.put(linkType, Direction.BOTH);
					}					
					else{
						if(dir == null)
							toDataPath.put(linkType, Direction.INCOMING);
						else if(dir !=  Direction.INCOMING)
							toDataPath.put(linkType, Direction.BOTH);
					}	
					even.set(false);
				}
				else
					even.set(true);	
			});
		}
		return toDataPath;
	}


	public List<MetaQueryable> metaPath(){
		//The list of traversed queryable in the correct order
		if(metaPath.isEmpty())
			return Collections.emptyList();

		List<MetaQueryable> path = new ArrayList<>(metaPath.size()+1);
		path.add(root);
		MetaQueryable current = root;
		for(int i = 0; i<metaPath.size(); i++){
			Link l = metaPath.get(i);
			if(l.source() == current){
				current = (MetaQueryable) l.target();
				path.add((MetaQueryable) l.target());				
			}else{
				current = (MetaQueryable) l.source();
				path.add((MetaQueryable) l.source());
			}			
		}		
		return path;		
	}


	public Collection<MetaTraversal> createTraversalForEachInstance(DataAccess session, String creator){
		if(metaPath.isEmpty())
			throw new IllegalStateException("Cannot create a MetaTraversal from an empty MetaPath");

		Collection<? extends DataItem> roots = null;
		try {
			roots = session.queryFactory()
					.regenerate(root.itemClass(session.dataRegistry()))
					.toDepth(0)
					.traverseAllLinks()
					.includeAllNodes()
					.includeOneKey(WritableDataItem.idKey)
					.getAll()
					.getAllTargets();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Collection<MetaTraversal> travs = new ArrayList<>();
		for(DataItem item : roots){
			String name = target.name()+" from "+item.toString();
			travs.add(createTraversal(session, name, creator, name,item.getAttribute(WritableDataItem.idKey).get()));
		}

		return travs;
	}

	public MetaTraversal createTraversal(DataAccess session, String name, String creator, String description, int dbID){
		if(metaPath.isEmpty())
			throw new IllegalStateException("Cannot create a MetaTraversal from an empty MetaPath");

		LinkDecisions<LastLinkEvaluation<TraversalDefinition>> builder = TraversalDefinition.newDefinition()
		.startFrom(root.itemDeclaredType())
		.having(WritableDataItem.idKey, dbID)
		.fromMinDepth().toMaxDepth();
		
		IncludeLinksBased<LastLinkEvaluation<TraversalDefinition>> ib = null;
		LinkedHashMap<String, Direction> included = toDataPath();
		for(Entry<String, Direction> e : included.entrySet()){
			ib = builder.traverseLink(e.getKey(), e.getValue());
		}		
		try {
			return MetaTraversal.getOrCreate(session.dataRegistry().getHook(MetaInstanceDirector.class), name, description, ib.includeAllNodes().build(), target);
		} catch (NonDataMappingException e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * Creates a {@link TraversalBuilder} based on this {@link MetaPath}
	 * @param roots an {@link ExplicitPredicate} used to filter instances of the root type of this {@link MetaPath}, maybe null.
	 * @return a {@link TraversalBuilder} built from this {@link MetaPath} using the specified {@link ExplicitPredicate}
	 * to filter the starting roots.
	 */
	public IncludeLinksBased<LastLinkEvaluation<TraversalDefinition>> toBuilder(ExplicitPredicate<NodeItem> roots){
		
		LinkDecisions<LastLinkEvaluation<TraversalDefinition>> builder = TraversalDefinition.newDefinition()
		.startFrom(root.itemDeclaredType())
		.acceptedBy(roots)
		.fromMinDepth().toMaxDepth();
		
		IncludeLinksBased<LastLinkEvaluation<TraversalDefinition>> ib = null;
		LinkedHashMap<String, Direction> included = toDataPath();
		for(Entry<String, Direction> e : included.entrySet()){
			ib = builder.traverseLink(e.getKey(), e.getValue());
		}	
		return ib;
	}


}

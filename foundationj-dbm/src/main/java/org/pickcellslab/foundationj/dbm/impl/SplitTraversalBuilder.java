package org.pickcellslab.foundationj.dbm.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.CategoryAdder;
import org.pickcellslab.foundationj.dbm.queries.builders.SplittedSplitChoiceNoneAllowed;
import org.pickcellslab.foundationj.dbm.queries.builders.TraversalSplitChoice;

class SplitTraversalBuilder<V,E,T,O> implements TraversalSplitChoice<V, E, T, O>,
CategoryAdder<SplittedSplitChoiceNoneAllowed<T, ExplicitPredicate<? super NodeItem>, O>, NodeItem>{


	private final TraversalDefinition td;
	private final TargetType<V, E, T> target;
	private final Action<? super T, O> action;
	private List<ExplicitPredicate<? super NodeItem>> filterSet;

	SplitTraversalBuilder(TargetType<V,E,T> target, TraversalDefinition td, Action<? super T,O> action){

		Objects.requireNonNull(td,"The TraversalDefinition is null!");		
		this.td = td;		
		this.target = target;
		this.action = action;

	}



	@Override
	public CategoryAdder<SplittedSplitChoiceNoneAllowed<T, ExplicitPredicate<? super NodeItem>, O>, NodeItem> basedOnFilters(
			ExplicitPredicate<? super NodeItem> p) {
		filterSet = new ArrayList<>();
		return addCategory(p);
	}

	@Override
	public SplittedSplitChoiceNoneAllowed<T, Integer, O> basedOnDepth() {
		return new SplitTraversalRunnerBuilder<>(
				target,
				action,
				new PathSplitterByDepth<>(),
				td				
				);
	}

	@Override
	public <K> SplittedSplitChoiceNoneAllowed<T, K, O> basedOnCustom(ExplicitPathSplit<V, E, K> splitter) {
		return new SplitTraversalRunnerBuilder<>(
				target,
				action,
				splitter,
				td				
				);
	}

	@Override
	public CategoryAdder<SplittedSplitChoiceNoneAllowed<T, ExplicitPredicate<? super NodeItem>, O>, NodeItem> addCategory(
			ExplicitPredicate<? super NodeItem> filter) {
		Objects.requireNonNull(filter,"Filter is null");
		filterSet.add(filter);
		return this;
	}

	@Override
	public SplittedSplitChoiceNoneAllowed<T, ExplicitPredicate<? super NodeItem>, O> lastCategory(
			ExplicitPredicate<? super NodeItem> filter) {
		filterSet.add(filter);
		return new SplitTraversalRunnerBuilder<>(
				target,
				action,
				new PathSplitterWithFilterSet<>(filterSet),
				td				
				);
	}

	@Override
	public <K> SplittedSplitChoiceNoneAllowed<T, K, O> basedOnACategoricalKey(ExplicitPredicate<? super NodeItem> p,
			AKey<K> k) {
		return new SplitTraversalRunnerBuilder<>(
				target,
				action,
				new PathSplitterCategorical<>(p, k),
				td				
				);
	}

	@Override
	public <K> SplittedSplitChoiceNoneAllowed<T, Boolean, O> basedOnBinary(
			ExplicitPredicate<? super NodeItem> p) {
		return new SplitTraversalRunnerBuilder<>(
				target,
				action,
				new PathBinarySplitter<>(p),
				td				
				);
	}




}

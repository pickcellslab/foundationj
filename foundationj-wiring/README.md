# Foundationj Wiring


This project contains annotations and interfaces used for the following purposes:

* To enable and configure the discovery and wiring of components of the foundationj application at boot time (see also [foundationj-boot](https://framagit.org/pickcellslab/foundationj/tree/feature/refactoring/foundationj-boot)).
* To enable the discovery and mapping of graph data objects to database entities.
* To enable the mapping of non-data java objects into a serialised form.

More information can be found on our [website](https://pickcellslab.frama.io/docs/architecture/foundationj/dependency_injection/)


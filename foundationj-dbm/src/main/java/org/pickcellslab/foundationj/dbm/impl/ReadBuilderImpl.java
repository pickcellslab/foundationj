package org.pickcellslab.foundationj.dbm.impl;

import java.util.function.Consumer;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.AbstractAction;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.VoidMechanism;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadBuilder;

class ReadBuilderImpl<T> implements ReadBuilder<T> {

	protected final TargetType<?,?,T> target;

	ReadBuilderImpl(TargetType<?,?,T> target){
		this.target = target;	
	}
	
	
	@Override
	public final <O> ItemsSearchBuilderFork<T,O> with(Action<? super T,O> action){
		return new ItemsSearchBuilderFork<>(target,action);		
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <O> AccessChoice<T, QueryRunner<O>> consume(Consumer<? super T> consumer) {
		return new MonolithicQBuilder(target, new ConsumeAction(consumer), null);
	}
		
	
	private class ConsumeAction extends AbstractAction<T,Void>{

		private final Consumer<? super T> consumer;
		
		public ConsumeAction(Consumer<? super T> consumer) {
			super("NA");
			this.consumer = consumer;
		}

		@Override
		protected VoidMechanism<T> create(String key) {
			return new VoidMechanism<T>() {

				@Override
				public void performAction(T i) throws DataAccessException {
					consumer.accept(i);
					
				}
			};
		}
		
		
	}
	
	
	
}

package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Component;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Supplier;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.ui.EvaluatorUI;
import org.pickcellslab.foundationj.ui.parsers.ExplicitPredicateParser;
import org.pickcellslab.foundationj.ui.wizard.Copyable;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

import com.alee.extended.button.WebSwitch;
import com.alee.managers.language.data.TooltipWay;
import com.fathzer.soft.javaluator.StaticVariableSet;

@SuppressWarnings("serial")
public class CombineFilterPanel<E> extends ValidityTogglePanel  implements Copyable<CombineFilterPanel<E>>{

	//EvaluatorUI
	private final EvaluatorUI<ExplicitPredicateParser<E>, ExplicitPredicate<E>> evaluation;
	private final LinkedList<String> expression; 

	private final WebSwitch ws;
	private JTextField nameField;
	private int last = 0;



	public <T extends Supplier<MetaFilter> & Highlightable> CombineFilterPanel(T supplier) {

		//Objects.requireNonNull(supplier, "Supplier is Null!");

		//Defining variables important for the creation of the expression


		//The map storing the variables must be the same for the display and the evaluator
		//TODO create an abstract subclass of abstract evaluator in order to ensure this
		StaticVariableSet<ExplicitPredicate<E>> var = new StaticVariableSet<>();

		//Create a parser for an attribute filter to inject into the EvaluatorUI
		ExplicitPredicateParser<E> parser = new ExplicitPredicateParser<>(var);

		//Create the display to show the expression
		evaluation = new EvaluatorUI<>(parser,var);

		//A list to keep track of expression tokens that were inserted
		expression = new LinkedList<>();




		// Filter config panel

		// Controllers
		//------------------------------------------------------------------------------------------

		JPanel configPanel = new JPanel();

		JScrollPane display = evaluation.getDisplay();
		//Component display = new JPanel();


		JButton rightPBtn = new JButton("(");
		rightPBtn.addActionListener(l->{
			last = nameField.getText().length();
			nameField.setText(nameField.getText()+" ( ");
			evaluation.insert("{", EvaluatorUI.parenthesis);
			expression.add("{");
			this.fireNextValidityChanged();
		});

		JButton leftPBtn = new JButton(")");
		leftPBtn.addActionListener(l->{
			last = nameField.getText().length();
			nameField.setText(nameField.getText()+" ) ");
			evaluation.insert("}", EvaluatorUI.parenthesis);
			expression.add("}");
			this.fireNextValidityChanged();
		});

		JButton btnAnd = new JButton("AND");
		btnAnd.addActionListener(l->{
			last = nameField.getText().length();
			nameField.setText(nameField.getText()+" AND ");
			evaluation.insert(" AND ", EvaluatorUI.operator);
			expression.add(" AND ");
			this.fireNextValidityChanged();
		});

		JButton btnOr = new JButton("OR");
		btnOr.addActionListener(l->{
			last = nameField.getText().length();
			nameField.setText(nameField.getText()+" OR ");
			evaluation.insert(" OR ", EvaluatorUI.operator);
			expression.add(" OR ");
		});

		JButton btnXor = new JButton(" XOR ");
		btnXor.addActionListener(l->{
			last = nameField.getText().length();
			nameField.setText(nameField.getText()+" XOR ");
			evaluation.insert(" XOR ", EvaluatorUI.operator);
			expression.add(" XOR ");
			this.fireNextValidityChanged();
		});

		JButton btnNot = new JButton(" NOT ");
		btnNot.addActionListener(l->{
			last = nameField.getText().length();
			nameField.setText(nameField.getText()+" NOT ");
			evaluation.insert(" NOT ", EvaluatorUI.operator);
			expression.add(" NOT ");
			this.fireNextValidityChanged();
		});

		JButton btnC = new JButton("C");
		btnC.addActionListener(l->{			
			nameField.setText(nameField.getText().substring(0, last));
			last = nameField.getText().length();
			evaluation.delete(expression.pollLast());
			this.fireNextValidityChanged();
		});


		JButton btnInsFilter = new JButton("INS");
		btnInsFilter.addActionListener(l->{
			MetaFilter mf = supplier.get();
			if(mf == null){
				JOptionPane.showMessageDialog(this,"There are no filters selected");
				return;
			}
			last = nameField.getText().length();
			nameField.setText(nameField.getText()+" "+mf.name());			
			ExplicitPredicate<E> p = mf.toFilter();
			expression.add(mf.name());
			evaluation.insert(mf.name(), p, EvaluatorUI.funct);	
			this.fireNextValidityChanged();
		});






		//layout
		
		configPanel.add(rightPBtn);
		configPanel.add(leftPBtn);
		configPanel.add(btnAnd);
		configPanel.add(btnOr);
		configPanel.add(btnXor);
		configPanel.add(btnNot);
		configPanel.add(btnInsFilter);
		configPanel.add(btnC);
		configPanel.setEnabled(false);
		for(Component c : configPanel.getComponents())
			c.setEnabled(false);


		//RadioButton panel
		JPanel radioPanel = new JPanel();

		// Simple switch
        ws = new WebSwitch ();
        ws.addToolTip(null, "Enable filtering",TooltipWay.down,0);
        ws.addActionListener(l->{
        	if(ws.isSelected()){
        		configPanel.setEnabled(true);
    			for(Component c : configPanel.getComponents())
    				c.setEnabled(true);
    			supplier.setHighlighted(true);
    			this.fireNextValidityChanged();
        	}else{
        		configPanel.setEnabled(false);
    			for(Component c : configPanel.getComponents())
    				c.setEnabled(false);
    			this.fireNextValidityChanged();
        	}
        });
		
        radioPanel.add(ws);

		

		JLabel nameLabel = new JLabel("Filter Name : ");
		radioPanel.add(nameLabel);
		
		nameField = new JTextField(5);		
		radioPanel.add(nameField);
		
				
		
		//Layout
		//-------------------------------------------------------------------------------
		

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(65)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(display, GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
							.addGap(65))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(radioPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
								.addComponent(configPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(67))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(5)
					.addComponent(radioPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(configPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(display, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		setLayout(groupLayout);

	}






	public Optional<ExplicitPredicate<E>> getPredicate(){

		if(!ws.isSelected())
			return Optional.empty();

		ExplicitPredicate<E> current = null;
		try{
			current = evaluation.getFunction();
		}catch(IllegalStateException e){
			return Optional.empty();
		}				
		return Optional.of(current);
	}


	public String name(){
		return nameField.getText();
	}


	public String expression(){
		return evaluation.getExpression();
	}




	@Override
	public boolean validity() {
		if(!ws.isSelected())
			return true;
		return evaluation.isValid() && !nameField.getText().isEmpty();
	}






	@Override
	public void copyState(CombineFilterPanel<E> other) {
		this.expression.addAll(other.expression);
		this.evaluation.copyState(other.evaluation);
		this.nameField.setText(other.nameField.getText());
		this.ws.setSelected(other.ws.isSelected());
		this.fireNextValidityChanged();
	}






	
}

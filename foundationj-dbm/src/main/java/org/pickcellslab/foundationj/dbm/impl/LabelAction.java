package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.VoidMechanism;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;


class LabelAction implements Action<DatabaseNodeDeletable, Void> {

	private static final String KEY = "Key"; 

	private final String label;
	private final LabelMechanism m;

	private TargetType<?, ?, DatabaseNodeDeletable> target;

	LabelAction(TargetType<?, ?, DatabaseNodeDeletable> target, String label){
		this.label = label;
		this.target = target;
		m = new LabelMechanism();
	}


	@Override
	public VoidMechanism<DatabaseNodeDeletable> createCoordinate(Object key) {
		return m;
	}

	@Override
	public Set<Object> coordinates() {
		Set<Object> coords = new HashSet<>(1);
		coords.add(KEY);
		return coords;
	}

	@Override
	public Void createOutput(Object key) throws IllegalArgumentException {
		return null;
	}

	@Override
	public String description() {
		return "Create label : "+label;
	}


	private class LabelMechanism implements VoidMechanism<DatabaseNodeDeletable>{

		private int count = 0;

		@Override
		public void performAction(DatabaseNodeDeletable i) throws DataAccessException {
			i.addLabel(label);
			count++;
		}

		@Override
		public void lastAction() throws DataAccessException{


			if(count != 0){

				//Create the MetaTag object and assign to affected objects
				try {

					MetaTag mt = MetaTag.getOrCreate(target.session().dataRegistry().getHook(MetaInstanceDirector.class), label, "Tag: "+label);

					// Queried MetaObjects can only be MetaClass or MetaTag (internal API)
					target.metaTypes().get().forEach(pointer -> pointer.queriedMetaObjects().forEach( m -> ((MetaClassImpl)m).setTag(mt))); 				
					target.session().queryFactory().protectedMeta(target.session(),mt).run();

				} catch (InstantiationHookException e) {
					throw new DataAccessException("Unable to create MetaTag for "+label, e);
				}

			}

		}


	}


}

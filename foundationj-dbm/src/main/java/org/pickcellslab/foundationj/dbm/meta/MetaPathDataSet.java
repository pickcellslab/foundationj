package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.queries.MonolithicResult;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.ActionBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

/**
 * 
 * A {@link MetaDataSet} which uses a {@link Path} to reach nodes in the graph which needs to be collected.
 * This implies that the actual node types encountered and collected are unknown a priori and that multiple types of objects can be collected. 
 * 
 * @author Guillaume Blin
 *
 *@see MetaTargetedDataSet
 *
 * @param <D>
 */


@Data(typeId = "MetaPathDataSet", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaPathDataSet<D> extends MetaDataSet<Path<NodeItem,Link>,D> {

	
	private MetaPathDataSet(String uid){
		super(uid);
	};

	public static MetaPathDataSet getOrCreate(MetaInstanceDirector director, String name, String description, MetaStrategy strategy, MetaSplit split) throws InstantiationHookException {
		
		
		final MetaPathDataSet d = director.getOrCreate(MetaPathDataSet.class, MetaItem.randomUID(MetaPathDataSet.class));
		
		d.setAttribute(MetaItem.name, name);
		d.setAttribute(MetaItem.desc, description);

		if(strategy != null) {
			new DataLink(STRATEGY, d, strategy, true);
			new DataLink(MetaModel.requiresLink, d, strategy, true);
		}

		if(split!=null) {
			new DataLink(GROUPING, d, split, true);
			new DataLink(MetaModel.requiresLink, d, split, true);
		}
		
		return d;
	}


	
	@Override
	public DataPointer getDataPointer() {
		// FIXME 
		throw new RuntimeException("Not supported");
	}
	
	
	


	@Override
	public String name() {
		return getAttribute(name).orElse("No Name");
	}


	@Override
	public String description() {
		return getAttribute(desc).orElse("Unavailable");
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}
	
	
	
	
	
	
	
	

	public MetaStrategy getStrategy(){
		if(!typeMap.containsKey(STRATEGY))
			return null;
		return (MetaStrategy) typeMap.get(STRATEGY).iterator().next().target();
	}

	public MetaSplit getSplitter(){
		if(!typeMap.containsKey(GROUPING))
			return null;
		return (MetaSplit) typeMap.get(GROUPING).iterator().next().target();
	}


	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>DataBase Query</strong>"+
				"<p><ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"</li></p></HTML>";
	}




	//@SuppressWarnings("unchecked")
	@Override
	protected <O> ResultAccess<String, O> load(DataAccess access, Action<Path<NodeItem,Link>, O> action) throws DataAccessException {


		boolean monolithic = false;

		ActionBuilder<?, O> fork = access.queryFactory().readPaths(getAttribute(MetaItem.name).get()).with(action);


		MetaStrategy strategy = getStrategy();
		MetaSplit split = getSplitter();
		QueryRunner<?> runner = null;
		if(null == strategy){			
			if(split == null){
				monolithic = true;
				runner = fork.inOneSet(name()).getAll();
			}				
			else
				runner = fork.inGroups().getAll().withSplitter(split.toSplitter());
		}		
		else if(strategy instanceof MetaTraversal){			
			if(split == null){
				monolithic = true;
				runner = fork.inOneSet(name()).useTraversal( ((MetaTraversal) strategy).toTraversal());
			}
			else
				runner = fork.inGroups().splitTraversal( ((MetaTraversal) strategy).toTraversal())
				.basedOnCustom(split.toPathSplitter())
				.noFurtherSplit();
		}
		else if(strategy instanceof MetaSplitTraversal){
			if(split == null)
				runner = fork.inGroups()
				.splitTraversal(((MetaSplitTraversal) strategy).metaTraversal().toTraversal())
				.basedOnCustom(((MetaSplitTraversal) strategy).metaSplit().toPathSplitter())
				.noFurtherSplit();
			else{
				runner = fork.inGroups()
						.splitTraversal(((MetaSplitTraversal) strategy).metaTraversal().toTraversal())
						.basedOnCustom(((MetaSplitTraversal) strategy).metaSplit().toPathSplitter())
						.splitFurtherUsing(split.toSplitter());

				//FIXME cannot have 2 pathsplitter in the same definition

				return new ToStringOrganisedResult((OrganisedResult) runner.run());
			}
		}


		Object result = runner.run();

		if(monolithic)
			return new MonolithicResult<O>((O) result, name());

		return (ResultAccess<String,O>) result;

	}




	@Override
	public List<? extends DimensionContainer<Path<NodeItem, Link>, D>> possibleDimensions(
			Predicate<DimensionContainer<Path<NodeItem, Link>, ?>> p) {
		throw new RuntimeException("Not implemented yet");
	}

	
	
	@Override
	public TraversalDefinition deletionDefinition() {
		return TraversalDefinition.newDefinition().startFrom("MetaPathDataSet").having(MetaModel.recognition, recognitionString())
				.fromDepth(0).toDepth(0)
				.traverseAllLinks().includeAllNodes().build();
	}


	@Override
	public TraverserConstraints storageConstraints() {
		return Traversers.newConstraints().fromDepth(0).toDepth(4)
				.traverseAllLinks()
				.withEvaluations(P.isNodeWithDeclaredType(DataRegistry.typeIdFor(MetaClass.class)), Decision.INCLUDE_AND_STOP, Decision.INCLUDE_AND_CONTINUE)
				.lastEvaluation(P.isNodeWithDeclaredType(DataRegistry.typeIdFor(MetaLink.class)), Decision.INCLUDE_AND_STOP);
	}

	

}

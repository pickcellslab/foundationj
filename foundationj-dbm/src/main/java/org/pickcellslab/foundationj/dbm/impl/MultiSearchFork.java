package org.pickcellslab.foundationj.dbm.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.impl.MonolithicSearchDefinition.Builder;
import org.pickcellslab.foundationj.dbm.queries.CombinedBinary;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.BinaryCombinationAdder;
import org.pickcellslab.foundationj.dbm.queries.builders.CategoryAdder;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.SplitChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.SplittableAccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.TraversalSplitChoice;


class MultiSearchFork<V,E,T , O> implements 
SplittableAccessChoice<V,E,T,O,SplitChoice<T,O>>,
SplitChoice<T,O>,
CategoryAdder<QueryRunner<OrganisedResult<ExplicitPredicate<? super T>, O>>, T>,
BinaryCombinationAdder<QueryRunner<OrganisedResult<CombinedBinary<T>, O>>, T>
{



	private final Builder<V,E,T> delegate;
	private final Action<? super T,O> action;
	private final TargetType<V, E, T> target;
	private List<ExplicitPredicate<? super T> > filterSet;




	public MultiSearchFork(TargetType<V,E,T> target, Action<? super T,O> action) {
		this.target = target;
		this.action = action;
		this.delegate = new MonolithicSearchDefinition.Builder<>(target);
	}



	@Override
	public SplitChoice<T,O> getAll() {
		return this;
	}

	@Override
	public SplitChoice<T,O> useFilter(ExplicitPredicate<? super T> test) {
		delegate.setFilter(test);
		return this;
	}

	@Override
	public SplitChoice<T,O> useTraversal(TraversalDefinition definition) {
		assert definition != null;
		target.checkTraversalDefinition(definition);
		delegate.setTraversal(definition);
		return this;
	}

	@Override
	public TraversalSplitChoice<V, E, T, O> splitTraversal(TraversalDefinition td) {
		return new SplitTraversalBuilder<>(target, td, action);
	}


	@Override
	public SplitChoice<T,O> useTraversals(SuppliedTraversalOperation operations) {
		delegate.setTraversalOperation(operations);
		return this;
	}



	@Override
	public QueryRunner<OrganisedResult<String,O>> groupBy(ExplicitFunction<? super T,String> categorical) {
		return new MultiSearchRunner<>(
				target,
				new MultiSearchDefinition<>(
						delegate.build(),
						new CategoricalKeySplitter<>(categorical,"value")
						),
				action);
	}




	@Override
	public QueryRunner<OrganisedResult<Integer, O>> asCategories(ExplicitFunction<? super T,Integer> categorical) {
		return new MultiSearchRunner<>(
				target,
				new MultiSearchDefinition<>(
						delegate.build(),
						new CategoricalKeySplitter<>(categorical,"value")
						),
				action);
	}


	@Override
	public QueryRunner<OrganisedResult<Boolean, O>> as2Groups(ExplicitPredicate<? super T> test) {
		return new MultiSearchRunner<>(
				target,
				new MultiSearchDefinition<>(
						delegate.build(),
						new BinarySplitter<>(test)
						),
				action);
	}





	@Override
	public <K> QueryRunner<OrganisedResult<K, O>> withSplitter(ExplicitSplit<? super T, K> splitter) {
		Objects.requireNonNull(splitter,"The provided Splitter is null");
		return new MultiSearchRunner<>(
				target,
				new MultiSearchDefinition<>(
						delegate.build(),
						splitter
						),
				action);
	}



	@Override
	public CategoryAdder<QueryRunner<OrganisedResult<ExplicitPredicate<? super T>, O>>, T> createCategory(
			ExplicitPredicate<? super T> filter) {
		filterSet = new ArrayList<>();
		return addCategory(filter);		
	}



	@Override
	public CategoryAdder<QueryRunner<OrganisedResult<ExplicitPredicate<? super T>, O>>, T> addCategory(
			ExplicitPredicate<? super T> filter) {
		Objects.requireNonNull(filter, "The provided filter is null");
		filterSet.add(filter);
		return this;
	}



	@Override
	public QueryRunner<OrganisedResult<ExplicitPredicate<? super T>, O>> lastCategory(
			ExplicitPredicate<? super T> filter) {
		return new MultiSearchRunner<>(
				target,
				new MultiSearchDefinition<>(
						delegate.build(),
						new FilterSetSplitter<>(filterSet)
						),
				action);
	}


	@Override
	public BinaryCombinationAdder<QueryRunner<OrganisedResult<CombinedBinary<T>, O>>, T> withBinaryCategories(
			ExplicitPredicate<? super T> filter) {
		filterSet = new ArrayList<>();
		return combineWith(filter);	
	}
	

	@Override
	public BinaryCombinationAdder<QueryRunner<OrganisedResult<CombinedBinary<T>, O>>, T> combineWith(
			ExplicitPredicate<? super T> filter) {
		Objects.requireNonNull(filter,"filter is Null");
		filterSet.add(filter);
		return this;
	}



	@Override
	public QueryRunner<OrganisedResult<CombinedBinary<T>, O>> lastBinary(ExplicitPredicate<? super T> filter) {
		filterSet.add(filter);
		return new MultiSearchRunner<>(
				target,
				new MultiSearchDefinition<>(
						delegate.build(),
						new CombinedBinarySplitter<T>(filterSet)
						),
				action);
	}



	



	





}

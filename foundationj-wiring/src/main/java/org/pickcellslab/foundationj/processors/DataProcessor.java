package org.pickcellslab.foundationj.processors;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.mapping.data.DbElement;
import org.pickcellslab.foundationj.mapping.data.DefaultInstantiationHook;


public class DataProcessor extends FoundationJProcessor {



	protected boolean contractRespected(TypeElement type) {

		//--------------------------------------------------------------------//
		// First make sure that the annotation is on a concrete class
		//--------------------------------------------------------------------//

		if(!ProcessorsUtils.checkConcrete(Data.class, type, processingEnv)) return false;


		//--------------------------------------------------------------------//
		// Check that it does NOT implement DbElement
		//--------------------------------------------------------------------//

		//Get the type Mirror for DataElement.class
		TypeMirror deMirror = processingEnv.getElementUtils().getTypeElement(DbElement.class.getCanonicalName()).asType();

		//Check that this is not a Map
		if(processingEnv.getTypeUtils().isSubtype(type.asType(),deMirror)) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "@Data implementations must NOT implement DbElement", type);
			return false;
		}

		//--------------------------------------------------------------------//
		// Ensure that @Scope or @SameScopeAs or @WithService is present
		//--------------------------------------------------------------------//
		if(type.getAnnotation(Scope.class)==null
				&& type.getAnnotation(SameScopeAs.class)==null
				&& type.getAnnotation(WithService.class)==null){
			processingEnv.getMessager().printMessage(Kind.ERROR, "@Data must also be annotated with eith @Scope, "
					+ "@SameScopeAs or @WithService", type);
			return false;
		}
		
		

		//---------------------------------------------------------------------------//
		// Check that an empty constructor is present if the hook is the default one
		//---------------------------------------------------------------------------//

		final TypeElement defaultHook = processingEnv.getElementUtils().getTypeElement(DefaultInstantiationHook.class.getCanonicalName());
		TypeMirror tm = null;
		try {
			type.getAnnotation(Data.class).hook();
		}catch(MirroredTypeException e) {
			tm = e.getTypeMirror();
		}

		if(processingEnv.getTypeUtils().asElement(tm).toString().equals(defaultHook.toString())){

			boolean hasEmptyConstructor = false;
			for (ExecutableElement c : ElementFilter.constructorsIn(type.getEnclosedElements())) {
				if (c.getParameters().isEmpty()) {
					hasEmptyConstructor = true;
					break;
				}
			}

			if(!hasEmptyConstructor)
				processingEnv.getMessager().printMessage(Kind.ERROR, "An @Data class must possess an empty constructor"
						+ "unless a custom nstantiationHook is specified. (a private empty constructor is allowed)",type);

			return hasEmptyConstructor;

		}


		return true;

	}




	@Override
	protected Class<Data> annotation() {
		return Data.class;
	}


	@Override
	protected String metadatapath() {
		return "META-INF/foundationj/data";
	}



}

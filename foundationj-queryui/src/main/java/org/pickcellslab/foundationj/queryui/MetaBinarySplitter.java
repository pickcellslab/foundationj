package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.PredictableMetaSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.ToStringBinarySplitter;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

@Data(typeId = "MetaBinarySplitter", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaBinarySplitter extends PredictableMetaSplit {


	private static final AKey<String> acceptKey = AKey.get("Accept",String.class);
	private static final AKey<String> rejectKey = AKey.get("Reject",String.class);

	private MetaBinarySplitter(String uid) {
		super(uid);
	}

	public static MetaBinarySplitter getOrCreate(MetaInstanceDirector director, MetaFilter mf, String accept, String reject) throws InstantiationHookException {		

		Objects.requireNonNull(accept,"accept is null");
		Objects.requireNonNull(reject,"reject is null");

		final MetaBinarySplitter split = director.getOrCreate(MetaBinarySplitter.class, "Group"+mf.recognitionString()+accept+reject);

		if(!split.getAttribute(MetaModel.name).isPresent()){// Check if it was retrieved or if it already existed


			split.setAttribute(MetaItem.name, "Split in 2 : "+mf.name());
			split.setAttribute(MetaItem.desc, "Split in 2 categories based on "+mf.description());
			
			split.setAttribute(acceptKey,accept);
			split.setAttribute(rejectKey,reject);

			new DataLink("BASED_ON", split, mf, true);
			new DataLink(MetaModel.requiresLink, split, mf, true);

		}

		return split;
	}
	
	
	

	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc, acceptKey, rejectKey);
	}
	
	



	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>Grouping:</strong>"+
				"<p><ul><li>Name:"+name()+"</li>"+
				"<li>Description:"+description()+
				"</li></p></HTML>";
	}




	@Override
	public boolean pathSplitSupported() {
		return true;
	}



	@Override
	public boolean targetSplitSupported() {
		return true;
	}






	@Override
	public <E> ExplicitSplit<E, String> toSplitter() {

		final ExplicitPredicate<E> p = ((MetaFilter) typeMap.get("BASED_ON").iterator().next().target()).toFilter();
		final String accept = getAttribute(AKey.get("Accept",String.class)).get();
		final String reject = getAttribute(AKey.get("Reject",String.class)).get();

		return new ToStringBinarySplitter<>(p, accept, reject);
		
	}






	@Override
	public <V, E> ExplicitPathSplit<V, E, String> toPathSplitter() {



		return new ExplicitPathSplit<V,E,String>(){

			private final MetaFilter mf = ((MetaFilter) typeMap.get("BASED_ON").iterator().next().target());
			private final ExplicitPredicate<DataItem> p = mf.toFilter();
			private final Predicate<DataItem> predicate = P.isDeclaredType(mf.getTarget().itemDeclaredType());
			private final String accept = getAttribute(AKey.get("Accept",String.class)).get();
			private final String reject = getAttribute(AKey.get("Reject",String.class)).get();
			private final boolean isLink = mf.getTarget() instanceof MetaLink;

			private FjAdaptersFactory<V, E> fctry;

			@Override
			public String description() {
				return "Split path in 2 categories based on "+p.description()+" for "+mf.getTarget().name();
			}

			@Override
			public String getKeyFor(Path<V, E> i) {
				if(isLink){
					Iterator<E> it = i.edges();
					while(it.hasNext()){
						DataItem t = fctry.getLinkReadOnly(it.next());
						if(predicate.test(t)){
							if(p.test(t))
								return accept;
							else
								return reject;
						}
					}
				}
				else{				
					Iterator<V> it = i.nodes();
					while(it.hasNext()){
						DataItem t = fctry.getNodeReadOnly(it.next());
						if(predicate.test(t)){
							if(p.test(t))
								return accept;
							else
								return reject;
						}
					}
				}
				return OTHER;
			}

			@Override
			public void setFactory(FjAdaptersFactory<V, E> f) {
				this.fctry = f;
			}

		};
	}

	@Override
	public String[] getPrediction() {
		return new String[]{getAttribute(AKey.get("Accept",String.class)).get(),  getAttribute(AKey.get("Reject",String.class)).get(),"Other"};
	}


	@Override
	public TraverserConstraints storageConstraints() {
		return Traversers.newConstraints().fromDepth(0).toDepth(1)
				.traverseLink("BASED_ON", Direction.OUTGOING)
				.includeAllNodes();
	}



}

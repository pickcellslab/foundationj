package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="GroupingByKey")
class CategoricalKeySplitter<T, K> implements ExplicitSplit<T,K> {

	@Supported
	private final ExplicitFunction<T,K> f;
	private final String id;

	CategoricalKeySplitter(ExplicitFunction<T,K> f, String id){
		MappingUtils.exceptionIfNullOrInvalid(f);
		this.f = f;
		this.id = id;
	}


	@Override
	public String description() {
		return "group by "+id;
	}


	@Override
	public K getKeyFor(T t) {
		return f.apply(t);
	}

}

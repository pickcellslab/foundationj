package org.pickcellslab.foundationj.datamodel.builders;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.annotations.Tagged;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.functions.Constant;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.KeySelector;
import org.pickcellslab.foundationj.datamodel.functions.LinkEnd;
import org.pickcellslab.foundationj.datamodel.predicates.BiFunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.FunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.LinkOrNode;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.datamodel.predicates.ValuePredicate;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.data.DbElement;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/**
 * A Factory class to create common {@link ExplicitPredicate}
 * 
 * @author Guillaume Blin
 *
 */
public final class P {


	private P(){
		//Not instantiable
	};


	public static SimpleFilterBuilder items(){
		return new SimpleFilterBuilder();
	}


	/**
	 * @return an {@link ExplicitPredicate} which always returns true
	 */
	public static <T> ExplicitPredicate<T> none(){
		return new None<>();
	}
	
	/**
	 * 
	 * Creates n {@link ExplicitPredicate} which returns {@code true} if the {@link DataItem#declaredType()} of the tested 
	 * DataItem equals the provided string and {@code false} otherwise.
	 * @param declaredType The string to test against
	 * @return An {@link ExplicitPredicate} which returns {@code true} if the {@link DataItem#declaredType()} of the tested 
	 * DataItem equals the provided string and {@code false} otherwise.
	 * @param <T> The type parameter of the returned ExplicitPredicate
	 * @see #isNodeWithDeclaredType(String)
	 * @see #isLinkWithDeclaredType(String)
	 */
	public static <T extends DataItem> ExplicitPredicate<T> isDeclaredType(String declaredType){
		assert declaredType != null : "declaredType cannot be null";
		return new IsDeclaredType<T>(declaredType);
	}
	
	/**
	 * Same as {@link #isDeclaredType(String)} but also tests first whether the tested {@link DataItem} implements {@link NodeItem}
	 * @param declaredType The string to test against
	 * @return An {@link ExplicitPredicate} which returns {@code true} if the tested object is a {@link NodeItem} and if its {@link DataItem#declaredType()}
	 *  equals to the provided string, {@code false} otherwise.
	 */
	public static <T> ExplicitPredicate<T> isNodeWithDeclaredType(String declaredType){
		assert declaredType != null : "declaredType cannot be null";
		return new IsLinkOrNodeDeclaredType<T>(declaredType, LinkOrNode.NODE);
	}
	
	/**
	 * Same as {@link #isDeclaredType(String)} but also tests first whether the tested {@link DataItem} implements {@link Link}
	 * @param declaredType The string to test against
	 * @return An {@link ExplicitPredicate} which returns {@code true} if the tested object is a {@link Link} and if its {@link DataItem#declaredType()}
	 *  equals to the provided string, {@code false} otherwise.
	 */
	public static <T> ExplicitPredicate<T> isLinkWithDeclaredType(String declaredType){
		assert declaredType != null : "declaredType cannot be null";
		return new IsLinkOrNodeDeclaredType<T>(declaredType, LinkOrNode.LINK);
	}
		
	
	/**
	 * Creates an {@link ExplicitPredicate} which returns {@code true} if a given class is the same as, or a superclass or superinterface 
	 * of the class the {@link DataItem} object tested by the predicate. Returns	{@code false} otherwise.
	 * @param clazz The class to test if it is assignable from the class of the tested DataElement. <b>Note that the class must be
	 * annotated with {@link Data} or {@link Tag}</b>
	 * @param <T> The type parameter of the returned ExplicitPredicate
	 * @return An {@link ExplicitPredicate} which returns {@code true} if the tested {@link DbElement} 
	 * is an instance of the given class or of a sub-type of the given class, {@code false} otherwise.
	 * @see {@link Class#isAssignableFrom(Class)}
	 */
	public static <T extends NodeItem> ExplicitPredicate<T> isSubType(@Tagged Class<? extends DataItem> clazz){
		assert clazz != null : "clazz cannot be null";
		return new IsSubType<>(clazz, null);
	}

	
	/**
	 * Same as {@link #isSubType(Class)} except that the type parameter of the returned {@link ExplicitPredicate} is specified
	 * as an argument
	 * @param clazz
	 * @param predicateType The class of the type parameter of the returned {@link ExplicitPredicate}
	 * @return An {@link ExplicitPredicate} which returns {@code true} if the tested {@link DbElement} 
	 * is a subtype of the given class and {@code false} otherwise.
	 * @see DataRegistry
	 */
	public static <T extends NodeItem> ExplicitPredicate<T> isSubType(@Tagged Class<? extends NodeItem> clazz, Class<T> predicateType){
		assert clazz != null : "clazz cannot be null";
		return new IsSubType<>(clazz, predicateType);
	}
	
	
	
	public static <T extends Link> ExplicitPredicate<T> sourcePasses(ExplicitPredicate<? super NodeItem> p){
		assert p != null : "p cannot be null";
		return new LinkEndPredicate<>(p, LinkEnd.Source);
	}
	
	
	public static <T extends Link> ExplicitPredicate<T> targetPasses(ExplicitPredicate<NodeItem> p) {
		assert p != null : "p cannot be null";
		return new LinkEndPredicate<>(p, LinkEnd.Target);
	}


	
		

	@Deprecated
	public static <T extends DataItem,L,R> ExplicitPredicate<T> keyTest(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
		return new BiFunctionPredicate<T,L,R>(
				new KeySelector<>(k, null)
				, new Constant<>(value), 
				o);
	}

	@Deprecated
	public static <T extends DataItem, L> ExplicitPredicate<T> keyTest(AKey<L> k, ExplicitPredicate<L> test){		
		Objects.requireNonNull(k);
		Objects.requireNonNull(test);
		return new FunctionPredicate<>(F.select(k, null), test);		
	}


	/**
	 * @return An {@link ExplicitPredicate} testing if a given object is equal to the provided value
	 */
	@Deprecated
	public static <E extends Number> ExplicitPredicate<E> equalsTo(E value){
		return new ValuePredicate<>(Op.Logical.EQUALS, value);		
	}
	/**
	 * @return An {@link ExplicitPredicate} testing if a given string is equal to the provided value
	 */
	@Deprecated
	public static ExplicitPredicate<String> stringEquals(String value){
		return new ValuePredicate<>(Op.TextOp.EQUALS, value);
	}
	
	/**
	 * @return An {@link ExplicitPredicate} testing if a given object is equal to the provided value
	 */
	@Deprecated
	public static <T> ExplicitPredicate<T> objectEquals(T value){
		return new ValuePredicate<>(Op.ObjectOp.EQUALS, value);	
	}

	/**
	 * @return An {@link ExplicitPredicate} testing if a given object is equal to the provided value
	 */
	public static ExplicitPredicate<String> stringContains(String value){
		return new ValuePredicate<>(Op.TextOp.CONTAINS, value);	
	}

	/**
	 * @return An {@link ExplicitPredicate} testing if an Array of type E contains the specified element
	 */
	public static <E> ExplicitPredicate<E[]> hasElement(E element){
		return new ValuePredicate<>(Op.ElementOp.ELEMENT_OF, element);	
	}

	/**
	 * @return An {@link ExplicitPredicate} testing if an Array of type E contains any of the elements of the provided array
	 */
	public static <E> ExplicitPredicate<E[]> hasAny(E[] element){
		return new ValuePredicate<>(Op.SetOp.ANY_OF, element);		
	}


	/**
	 * @param o The desired {@link ExplicitOperator}
	 * @param value A value to test against.
	 * @return An {@link ExplicitPredicate} testing if the given {@link ExplicitOperator} returns true
	 * when using the argument in {@link Predicate#test(Object)} as the left operand and
	 * the given value as right operand. 
	 * @param <T> Type of the left operand
	 * @param <E> Type of the right operand
	 * 
	 */
	@Deprecated
	public static <T,E> ExplicitPredicate<T> valueTest(ExplicitOperator<? super T,? super E> o, E value){
		return new ValuePredicate<>(o, value);	
	}


	/**
	 * @return An {@link ExplicitPredicate} testing if a Number is within the specified range
	 */
	public static <E extends Number> ExplicitPredicate<E> inRange(E min, E max){
		return new RangePredicate<>(min, max);
	}

	
	
	/**
	 * Creates an {@link ExplicitPredicate} that returns {@code true} if the tested 
	 * {@link DataItem} has the specified {@link AKey}
	 * @param k the AKey object the tested DataItem must have
	 * @return {@code true} if the AKey is present, {@code false} otherwise
	 */
	@Deprecated
	public static ExplicitPredicate<DataItem> hasKey(AKey<?> k){
		return F.streamKeys().contains(k);
	}


	

	/**
	 * Creates an {@link ExplicitPredicate} which tests if a DataItem has one of the values hold by the provided Set
	 * @param k The key to property to be tested (null not permitted)
	 * @param set The set containing the valid values (null not permitted)
	 * @return The {@link ExplicitPredicate}
	 */
	public static <E> ExplicitPredicate<DataItem> setContains(AKey<E> k, Set<E> set){

		Objects.requireNonNull(k, "The provided AKey must not be null");
		Objects.requireNonNull(set, "The provided Set must not be null");

		return new SetContains<>(k, set);
	}
	
	
	@Deprecated
	public static <I,O> ExplicitPredicate<I> test(ExplicitFunction<I,O> f, O value){
		return new FunctionPredicate<>(f, P.objectEquals(value));
	}
	
	
	
	

	@Mapping(id = "All")
	@Scope
	public static class None<T> implements ExplicitPredicate<T>{
		@Override
		public boolean test(T t) {
			return true;
		}
	
		@Override
		public String description() {
			return "No filter";
		}
	
	}

	
	@Mapping(id = "CheckType")
	@Scope
	public static class IsDeclaredType<T extends DataItem> implements ExplicitPredicate<T>{
	
		private final String type;
	
		IsDeclaredType(String type){
			this.type = type;	
		}
	
		@Override
		public boolean test(T t) {			
			return type.equals(t.declaredType());
		}
	
		@Override
		public String description() {
			return "Tests if declared type is : "+type;
		}
	
		@Override
		public String toString() {
			return " has declared type "+type;
		}
	}
	
	
	
	
	
	
	
	
	@Mapping(id = "CheckLinkOrNodeType")
	@Scope
	public static class IsLinkOrNodeDeclaredType<T> implements ExplicitPredicate<T>{
			
		private final LinkOrNode linkOrNode;
		private final String type;
	
		IsLinkOrNodeDeclaredType(String type, LinkOrNode linkOrNode){
			this.type = type;	
			this.linkOrNode = linkOrNode;
		}
	
		@Override
		public boolean test(T t) {
			return linkOrNode.test(t) && type.equals(((DataItem) t).declaredType());
		}
	
		@Override
		public String description() {
			return "Tests if object is a "+linkOrNode.name()+" and has a declared type equals to "+ type;
		}
	
		@Override
		public String toString() {
			return " object is a "+linkOrNode.name()+" and has a declared type equals to "+ type;
		}
	}
	
	
	@Mapping(id = "Is_SubType_Of")
	@Scope
	public static class IsSubType<E extends NodeItem> implements ExplicitPredicate<E>{
	
		private final String tag;
			
		IsSubType(Class<?> type, Class<E> predicateType){
			final Tag t = type.getAnnotation(Tag.class);
			if(t!=null)
				tag = t.tag();
			else {
				final Data d = type.getAnnotation(Data.class);
				if(d!=null)
					tag = d.typeId();
				else throw new RuntimeException(type+" is not supported for testing subtypes");
			}
		}
	
		@Override
		public boolean test(E t) {
			return t.hasTag(tag);
		}
	
		@Override
		public String description() {
			return "Tests if is subtype of " + tag;
		}
	
		@Override
		public String toString() {
			return " Is Subtype of " + tag;
		}
	}
	
	
	

	@Mapping(id = "LinkTest")
	@Scope
	public static class LinkEndPredicate<T extends Link> implements ExplicitPredicate<T>{
	
		@Supported
		private final ExplicitPredicate<? super NodeItem> predicate;
		private final LinkEnd linkEnd;
	
		LinkEndPredicate(ExplicitPredicate<? super NodeItem> predicate, LinkEnd end){
			MappingUtils.exceptionIfNullOrInvalid(predicate);
			this.predicate = predicate;	
			this.linkEnd = end;
		}
	
		@Override
		public boolean test(T t) {			
			return predicate.test(linkEnd.apply(t));
		}
	
		@Override
		public String description() {
			return linkEnd + " passes : " + predicate.description();
		}
	
		@Override
		public String toString() {
			return linkEnd + " passes : " + predicate.toString();
		}
	}

	@Mapping(id = "Range")
	@Scope
	public static class RangePredicate<T extends Number> implements ExplicitPredicate<T>{
	
		private final double min;
		private final double max;
	
		RangePredicate(T min, T max){
			this.min = min.doubleValue();	
			this.max = max.doubleValue();
		}
		
		@Override
		public boolean test(T t) {
			double d = t.doubleValue();
			return min <= d && d <= max;
		}
	
		@Override
		public String description() {
			return "in Range {"+min+" "+max+"}";
		}
	
		@Override
		public String toString() {
			return "between "+min+" and "+max;
		}
	}

	@Mapping(id = "SetChoice")
	@Scope
	public static class SetContains<E> implements ExplicitPredicate<DataItem>{

		private final AKey<E> key;
		@Supported
		private final Set<E> values;

		SetContains(AKey<E> k, Set<E> set){
			MappingUtils.exceptionIfNullOrInvalid(set);
			this.key = k;	
			this.values = set;
		}
		
		@Override
		public boolean test(DataItem t) {
			return values.contains(t.getAttribute(key).orElse(null));
		}

		@Override
		public String description() {
			return "values for "+key.name+" contained in a specific set";
		}

		@Override
		public String toString() {
			return key.name + " contained in " + values.toString();
		}
	}


}

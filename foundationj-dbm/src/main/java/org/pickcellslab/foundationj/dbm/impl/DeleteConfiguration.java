package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;

class DeleteConfiguration<T> {

	private final List<AKey<?>> toDelete;
	private final boolean deleteItem;
	private final String description;

	private DeleteConfiguration(Builder<T> b) {
		toDelete = b.toDelete;
		deleteItem = b.deleteItem;
		description = b.desc;
	}

	public boolean deleteItem(){
		return deleteItem;
	}

	public List<AKey<?>> deleteKeys(){
		return toDelete;
	}

	public String description(){
		return description;
	}


	public static class Builder<T>{

		private List<AKey<?>> toDelete = new ArrayList<>();
		private boolean deleteItem = true;
		private String desc;

		
		Builder(){};
		


		public Builder<T> deleteKey(AKey<?> k){
			toDelete.add(k);
			deleteItem = false;
			return this;
		}

		public Builder<T> deleteKeys(Collection<AKey<?>> keys){
			toDelete.addAll(keys);
			deleteItem = false;
			return this;
		}


		/**
		 * Build a {@link DeleteQuery} with all the options specified in the builder.
		 * Note that if no AKey objects to be deleted were specified, then the entire
		 * {@link WritableDataItem} found to be the target of the query will be deleted from the database 
		 * @return the DeleteQuery
		 */
		public DeleteConfiguration<T> build(){
			desc = "Delete";
			if(deleteItem)	
				desc += " completely";
			else{
				desc += " keys: ";
				for(AKey<?> k : toDelete)
					desc += "|"+k.name;
			}	
			return new DeleteConfiguration<>(this);
		}



	}

}

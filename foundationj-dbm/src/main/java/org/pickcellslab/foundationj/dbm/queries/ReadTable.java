package org.pickcellslab.foundationj.dbm.queries;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.queries.DataCollectorBuilder.DataCollector;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

/**
 * A {@link TableModel} implementation with some extra convenience method.
 * <br>
 * This object is often obtained from a read query.
 *
 */
public class ReadTable extends AbstractTableModel{



	//TODO export to file


	private final List<AKey<?>> keys;
	private final List<Map<AKey<?>, Object>> result;
	private final boolean isEmpty;


	public ReadTable(List<Map<AKey<?>, Object>> result, List<AKey<?>> keys) {
		this.keys = keys;
		this.result = result;
		boolean emptyTest = true;
		for(Map<AKey<?>,Object> map : result){
			if(!map.isEmpty()){
				emptyTest = false;
				break;
			}			
		}
		isEmpty = emptyTest;
	}


	public List<AKey<?>> keys(){
		return Collections.unmodifiableList(keys);
	}


	public boolean isEmpty(){
		return isEmpty;
	}

	public void sortColumns(Comparator<AKey<?>> comp) {
		Collections.sort(keys, comp);
		this.fireTableStructureChanged();
	}


	/**
	 * Returns all the value of a column in the table of the specified item type. Values are in the same order as in the table
	 * at the moment of the call. However sort modifications are not replicated in this ReadResult nor ReadResult sort 
	 * modification are in the returned List. 
	 */
	@SuppressWarnings("unchecked")
	public <E> List<E> getColumn(AKey<E> key){		
		List<E> column = new ArrayList<E>(result.size());		
		result.forEach(m -> column.add((E) m.get(key)));		
		return column;		 
	}


	/**
	 * Return all the values for the given AKey in a collection defined by the provided supplier
	 * @param k The AKey defining the column of interest (null not permitted)
	 * @param s The {@link Suplier} for the desired type of collection
	 * @return The collection holding the values for the desired column, if the supplier
	 * provides an insertion ordered collection, then the order will be the same as the order stored in this ReadResult
	 */
	@SuppressWarnings("unchecked")
	public <C extends Collection<E>,E> C getColumn(AKey<E> k, Supplier<C> s){
		C r = s.get();
		result.forEach(m -> r.add((E) m.get(k)));		
		return r;	
	}



	/**
	 * @return A read only map representing the row at index
	 */
	public Map<AKey<?>,Object> getRow(int index){		
		return Collections.unmodifiableMap(result.get(index));
	}


	/**
	 * @return the full table or reads. Note that the returned table is not duplicated therefore
	 * changes made to the table will be created in the ReadResult as well
	 */
	public List<Map<AKey<?>,Object>> getTable(){
		return result;
	}


	/**
	 * Returns the number of rows in the table of the specified item type
	 */
	public int rowCount(){		
		return result.size();
	}


	@SuppressWarnings("unchecked")
	public <E> E getValue(AKey<E> key, int rowIndex){
		return (E) result.get(rowIndex).get(key);
	}


	/**
	 * Sorts the table of the specified item type based on the value corresponding to key and in the specified order
	 */
	public void sort(AKey<?> key, Order d) {
		if(!isEmpty)
			Collections.sort(result, d.comparator(key));
	}



	/**
	 * Searches the table corresponding to the specified type for all the rows where the values
	 * matches the rules defined in the specified {@link Predicate}
	 * @return the list of the corresponding row indices
	 *TODO: change MultiKeyTester for Matcher
	 */
	public List<Integer> indicesOfMatch(Predicate<Map<AKey<?>, Object>> p){

		List<Integer> matches = new ArrayList<>();

		for (int i = 0; i<result.size(); i++)
			if (p.test(result.get(i)))
				matches.add(i);

		return matches;
	}



	/**
	 *Returns true if this ReadResult contains a match as defined by the specified {@link Predicate}, false otherwise
	 */
	public boolean contains(Predicate<Map<AKey<?>, Object>> p) {
		return result.stream().anyMatch(p);
	}



	private interface ComparatorFactory {
		Comparator<Map<AKey<?>, Object>> comparator(AKey<?> key);
	}

	public enum Order implements ComparatorFactory {

		ASCENDING {
			@Override
			public Comparator<Map<AKey<?>, Object>> comparator(final AKey<?> key) {

				return new Comparator<Map<AKey<?>, Object>>() {

					private AKey<?> attribute = key;

					@SuppressWarnings("unchecked")
					@Override
					public int compare(Map<AKey<?>, Object> m1, Map<AKey<?>, Object> m2) {

						Object v1 = m1.get(attribute);
						if(v1==null){
							Object v2 = m2.get(attribute);
							if(v2 == null)
								return 0;
							else return -1;
						}
						if (!Comparable.class.isAssignableFrom(v1.getClass()))
							return 0;
						else
							return ((Comparable<Object>) m1.get(attribute)).compareTo(m2.get(attribute)); 
					}
				};
			} 
		}
		,
		DESCENDING {
			@Override
			public Comparator<Map<AKey<?>, Object>> comparator(final AKey<?> property) {

				return new Comparator<Map<AKey<?>, Object>>() {

					private AKey<?> attribute = property;

					@SuppressWarnings("unchecked")
					@Override
					public int compare(Map<AKey<?>, Object> m1, Map<AKey<?>, Object> m2) {

						Object v1 = m1.get(attribute);
						if(v1==null){
							Object v2 = m2.get(attribute);
							if(v2 == null)
								return 0;
							else return 1;
						}
						if (!Comparable.class.isAssignableFrom(v1.getClass()))
							return 0;
						else
							return ((Comparable<Object>) m2.get(attribute)).compareTo(m1.get(attribute));
					}
				};
			}
		};

	}


	public static ReadTable combine(ReadTable r1, ReadTable r2){
		//TODO
		throw new RuntimeException("Not implemented yet!");
	}


	//===========================================================================================================================
	//TableModel implementation


	@Override
	public int getColumnCount() {
		return keys.size();
	}


	@Override
	public int getRowCount() {
		return rowCount();
	}


	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object o = result.get(rowIndex).get(keys.get(columnIndex));
		if(null == o)
			return "Null";
		if(o.getClass().isArray()){
			String s = "[";
			int l = Array.getLength(o)-1;
			if(l==-1)
				return "Empty";
			for(int i = 0; i<l; i++)
				s+=Objects.toString(Array.get(o,i))+", ";
			s+=Objects.toString(Array.get(o,l))+"]";
			return s;
		}
		else 
			return result.get(rowIndex).get(keys.get(columnIndex));
	}


	 @Override
	 public String getColumnName(int columnIndex) {
	  return keys.get(columnIndex).name;
	 }











	/**
	 * <HTML>
	 * A builder for a {@link ReadTable}. The strategy to build the ReadResult object is incremental: 
	 *  
	 * <pre>
	 *
	 *ReadResultBuilder builder = new ReadResultBuilder();
	 *builder.forType(chosenType);
	 *while(moreItemsToRead){
	 *	builder
	 *	.nextItem()
	 *	.add(AKey.get(someKeyDefinition), someValue);				
	 *}
	 *ReadResult result = builder.build();
	 *}
	 * </pre> 

	 * </HTML>
	 */
	public static class ReadResultBuilder<I extends WritableDataItem> implements ActionMechanism<I, ReadTable>{


		private DataCollector<I> collector;
		private List<Map<AKey<?>,Object>> result;

		//Used by ReadQuery
		public ReadResultBuilder(DataCollector<I> collector) {
			Objects.requireNonNull(collector);
			this.collector = collector;
			result = collector.supplier().get();
		}



		@Override
		public void performAction(I i) {
			collector.accumulator().accept(result, i);			
		}

		/**
		 * Builds and return a ReadResult object
		 */
		@Override
		public ReadTable get(){
			return new ReadTable(result,collector.accumulator().keys());
		}

	}




}

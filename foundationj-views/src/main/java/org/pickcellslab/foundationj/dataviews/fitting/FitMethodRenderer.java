package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

public class FitMethodRenderer implements ListCellRenderer<FittingMethod<?>> {

	@Override
	public Component getListCellRendererComponent(JList<? extends FittingMethod<?>> list, FittingMethod<?> value,
			int index, boolean isSelected, boolean cellHasFocus) {

		JLabel label = new JLabel();
		
		//Create a small padding
				Border paddingBorder = BorderFactory.createEmptyBorder(3,3,3,3);
				label.setBorder(paddingBorder);
		
		if(value == null){
			label.setText("  NONE   ");
			return label;			
		}
		
		label.setText(value.getName());
		label.setToolTipText(value.getDescription());

		return label;
	}

}

package org.pickcellslab.foundationj.mapping.data;

import org.pickcellslab.foundationj.annotations.Data;

/**
 * 
 * Listens for Instantiation events for a given type of {@link Data} objects when such objects are mapped back
 * from {@link DbElement}
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of {@link Data} object the implementing class listens
 */
public interface InstantiationListener<T> {

	
	/**
	 * Notifies this listener that the given instance was created from a {@link DbElement}
	 * @param instance
	 */
	public void instanceWasCreated(T instance);
	
}

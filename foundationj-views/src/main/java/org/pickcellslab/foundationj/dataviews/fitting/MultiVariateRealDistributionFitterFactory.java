package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * A marker interface for {@link NumericFitterFactory} which return probability distributions
 * 
 * @author Guillaume Blin
 *
 * @param <M> The type of {@link FittingMethod} available in this factory
 * @param <O> The type of the configuration options
 * @param <F> The type of fit created by the Fitter this factory can create
 */
public interface MultiVariateRealDistributionFitterFactory<F extends MultiVariateRealDistributionFitter<O,R>, O, R extends MultiVariateRealDistribution> 
extends DistributionFitterFactory<F, double[], O, R>  {

}

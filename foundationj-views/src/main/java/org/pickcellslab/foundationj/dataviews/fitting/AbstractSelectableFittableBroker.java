package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.mvc.DimensionSelectionListener;
import org.pickcellslab.foundationj.dataviews.mvc.SelectableDimensionBroker;

public abstract class AbstractSelectableFittableBroker<T,R,D> extends AbstractFittableBroker<T,R,D>	implements SelectableDimensionBroker<T,D> {

	protected final List<Integer> selection = new ArrayList<>();
	private final List<DimensionSelectionListener> sLstrs = new ArrayList<>();

	public AbstractSelectableFittableBroker(List<Dimension<T,D>> containers) {
		super(containers);
	}


	@Override
	public int numSelectedDimensions() {
		return selection.size();
	}


	@Override
	public Dimension<T,D> getSelectedDimension(int i) {
		return this.dimension(selection.get(i));
	}


	@Override
	public void setSelectedDimension(int i, boolean selected) {		
		
		if(selected){
			//Check not already selected
			if(!selection.contains(i)){
				selection.add(i);
				sLstrs.forEach(l->l.selectionChanged(this, this.dimension(i), selected));
			}
		}
		else{
			//Check that the dimension was indeed selected
			if(selection.remove((Integer)i)){
				sLstrs.forEach(l->l.selectionChanged(this, this.dimension(i), selected));				
			}			
		}
		
	}


	public void addDimensionSelectionListener(DimensionSelectionListener l) {
		sLstrs.add(l);
	}

	public void removeDimensionSelectionListener(DimensionSelectionListener l) {
		sLstrs.remove(l);
	}




}

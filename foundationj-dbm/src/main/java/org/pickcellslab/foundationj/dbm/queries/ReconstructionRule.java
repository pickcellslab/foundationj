package org.pickcellslab.foundationj.dbm.queries;

import java.util.Map;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;


public interface ReconstructionRule{

	public enum Policy{

		INCLUDED, EXCLUDED;


	}





	public String description();


	public Policy linkPolicy();


	public Policy keysPolicy();



	/**
	 * @return {@code true} if there are no filters defined for the links to retrieve. In other words, return true
	 * if all the links are to be included during the reconstruction process, false otherwise.
	 */
	public boolean noLinkConstraints();


	public boolean noNodeConstraints();


	/**
	 * @return {@code true} if all the existing properties are to be included in the new instance of the DataItem
	 */
	public boolean allKeysIncluded();



	/**
	 * @return The maximum depth of the graph linked to the NodeItem to retrieve
	 */
	public int getMaxDepth();





	public Map<String, Direction> getDecisionsOnLinks();


	public Decision defaultDecision();


	public Map<ExplicitPredicate<? super NodeItem>, Decision> getDecisionsOnNodes();




	public Stream<String> getKeysToRetrieve(DataItem item, Stream<String> availableDefinitions);




}

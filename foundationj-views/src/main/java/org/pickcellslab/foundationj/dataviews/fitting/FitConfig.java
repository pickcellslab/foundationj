package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

public class FitConfig<T> {

	
	private final FittingMethod<T> origin;
	private final T options;

	/**
	 * Creates a new FitConfig with the specified  origin and options. 
	 * @param origin The {@link FittingMethod} this config is appropriate for. Cannot be null
	 * @param options The actual configuration options.
	 */
	public FitConfig(FittingMethod<T> origin, T options){
		
		Objects.requireNonNull(origin);
		
		this.origin = origin;
		this.options = options;
	}
	
	public T options() {
		return options;
	}
	
	public FittingMethod<T> origin() {
		return origin;
	}
	
}

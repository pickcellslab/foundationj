package org.pickcellslab.foundationj.dbm.db;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.IllegalDeletionException;

public abstract class Updatable implements WritableDataItem {

	
	protected final UpdateInfo ui;



	protected Updatable(UpdateInfo ui) {
		this.ui = ui;
	}



	@Override
	public final <T> void setAttribute(AKey<T> key, T v) {
		if(ui.isAuthorized(this.declaredType(), key))
			set(key,v);
		else throw new RuntimeException(
				new DataAccessException("An update action must explicitely define its update intentions : "+key+" was undefined for " + this.declaredType()));
			
	}

	
	
	protected abstract <T> void set(AKey<T> key, T v);
	
	

	@Override
	public final void removeAttribute(AKey<?> key) {
		if(ui.isAuthorized(this.declaredType(), key))
			remove(key);
		else throw new RuntimeException(
				new IllegalDeletionException("A delete action must explicitely define its intentions : "+key+" was undefined for " + this.declaredType()));
	}
	
	
	protected abstract <T> void remove(AKey<T> key);

	
	
}

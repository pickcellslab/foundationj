package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaDataSet;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.DataToIconID;
import org.pickcellslab.foundationj.ui.wizard.UserDefinableDocument;
import org.pickcellslab.foundationj.ui.wizard.UserDefinedList;
import org.pickcellslab.foundationj.ui.wizard.UserDefinedListListener;

import com.alee.extended.tab.WebDocumentPane;

@SuppressWarnings("serial")
public class QueryWizard<T,D> extends JDialog{

	private final UserDefinedList<UserDefinableDocument<StrategyPanel<T,D>>> datasetList;
	private boolean wasCancelled = true;

	public QueryWizard(UITheme theme, DataAccess access, QueryWizardConfig<T,D> config) throws Exception {

		Objects.requireNonNull(config,"Configuration is null");

		//Choice view				
		ChoiceSet choiceView = new ChoiceSet(theme, access, null);


		//Tabbed pane
		WebDocumentPane<UserDefinableDocument<StrategyPanel<T,D>>> tabsPane = new WebDocumentPane<>();		
		tabsPane.setCloseable(false);
		tabsPane.setSplitEnabled(false);


		LongAdder ids = new LongAdder();
		datasetList = 
				new UserDefinedList<>(theme, ()-> {
					ids.increment();
					UserDefinableDocument<StrategyPanel<T,D>> udd = new UserDefinableDocument<>(""+ids.intValue(), theme.icon(IconID.Misc.DATA_TABLE, 16),
							new StrategyPanel<>(theme, access, choiceView, config));
					return udd;
				},null, config.isGroupingEnabled());

		datasetList.addSelectionListener(new UserDefinedListListener<UserDefinableDocument<StrategyPanel<T,D>>>(){
			@Override
			public void removed(UserDefinableDocument<StrategyPanel<T,D>> t) {
				tabsPane.closeDocument(t);
			}
			@Override
			public void added(UserDefinableDocument<StrategyPanel<T,D>> t) {
				//System.out.println("Added recieved");
				tabsPane.openDocument(t);
				choiceView.highlightMaster();
			}
			@Override
			public void selected(UserDefinableDocument<StrategyPanel<T,D>> t) {
				tabsPane.setSelected(t);
				choiceView.highlightMaster();
			}
			@Override
			public void renamed(UserDefinableDocument<StrategyPanel<T,D>> t) {
				//System.out.println("Renamed!");
				tabsPane.closeDocument(t);
				tabsPane.openDocument(t);
				tabsPane.setSelected(t);
			}			
		});

		//Propagate selection changes to the UserDefinedList
		tabsPane.getActivePane().getTabbedPane().addChangeListener(l->{
			datasetList.seSelected(tabsPane.getSelectedDocument());
			choiceView.highlightMaster();
		});


		datasetList.addItem("Rename Me");





		//-----------------------------------------------------------------------------------------------------------
		//Layout


		getContentPane().setLayout(new BorderLayout(0, 0));
		setPreferredSize(new Dimension(1200, 750));





		// NORTH -> some useful info
		//A Panel to add some information
		JPanel panel = new JPanel();
		JTextPane configDescription = new JTextPane();
		configDescription.setText(config.toHTML());
		panel.add(configDescription,BorderLayout.CENTER);
		getContentPane().add(panel, BorderLayout.NORTH);




		// WEST -> choiceset on top of dataset
		JPanel westPanel = new JPanel(new BorderLayout());


		//Available choices ScrollPane
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.WEST);		
		scrollPane.setViewportView(choiceView);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JLabel choiceLabel = new JLabel("MetaModel Browsing :");
		choiceLabel.setBorder(new EmptyBorder(10,10,10,10));
		Font lFont = choiceLabel.getFont();
		choiceLabel.setFont( new Font(lFont.getFontName(), Font.BOLD, lFont.getSize()));
		choiceLabel.setIcon(theme.icon(DataToIconID.get(dType.MIXED), 24));
		scrollPane.setColumnHeaderView(choiceLabel);
		westPanel.add(scrollPane,BorderLayout.CENTER);


		//Panel for the dataset list
		JPanel listPanel = new JPanel();
		JLabel title = new JLabel("My DataSet");
		title.setIcon(theme.icon(IconID.Misc.DATA_TABLE, 16));
		title.setFont( new Font(lFont.getFontName(), Font.BOLD, lFont.getSize()));
		title.setBorder(new EmptyBorder(10,10,10,10));

		listPanel.setLayout(new BorderLayout());
		listPanel.add(title, BorderLayout.NORTH);
		listPanel.add(datasetList, BorderLayout.CENTER);
		westPanel.add(listPanel,BorderLayout.SOUTH);

		getContentPane().add(westPanel,BorderLayout.WEST);



		// CENTER -> Tabbed Pane containing wizards
		getContentPane().add(tabsPane, BorderLayout.CENTER);		





		// SOUTH -> Done / Cancel
		JPanel btnPanel = new JPanel();
		JButton okBtn = new JButton("Done");
		okBtn.setEnabled(false);
		okBtn.addActionListener(l->{
			wasCancelled = false;
			this.dispose();
		});
		btnPanel.add(okBtn);

		JButton cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(l->{
			wasCancelled = true;
			this.dispose();
		});
		btnPanel.add(cancelBtn);

		getContentPane().add(btnPanel, BorderLayout.SOUTH);

		datasetList.addValidityListener(l->	{
			okBtn.setEnabled(datasetList.validity());
		});
	}





	public List<MetaDataSet<T,D>> getDataset(){
		//TODO exception if cancelled

		List<UserDefinableDocument<StrategyPanel<T,D>>> data = new ArrayList<>();
		datasetList.getAll(data);

		return data.stream()
				.map(d->d.getData().getDataSet(d.getName()))
				.collect(Collectors.toList());
	}


	
	public List<QueryInfo> getInfos() {
		List<UserDefinableDocument<StrategyPanel<T,D>>> data = new ArrayList<>();
		datasetList.getAll(data);

		return data.stream()
				.map(d->d.getData().getInfo(d.getName()))
				.collect(Collectors.toList());
	}
	



	public boolean wasCancelled() {
		return wasCancelled;
	}





	




}

package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer.Mode;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequenceHints;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;

public class GraphSequenceViewModel<T,D> extends AbstractModel<DataItem> implements RedefinableDimensionsModel<DataItem,SeriesGeneratorBroker<D>,D>, 
RedefinableBrokerContainer<DataItem, SeriesGeneratorBroker<D>, D>{


	private final DataAccess access;

	@SuppressWarnings("rawtypes")
	private final List<BrokerChangedListener> brokerLstrs = new ArrayList<>();
	private final Map<String, SeriesGeneratorBroker<D>> brokers = new HashMap<>();

	private final Map<Integer,Map<String,List<GraphSequencePointer>>>datasets = new HashMap<>();

	private final Mode mode;

	private final Predicate<DimensionContainer<DataItem, ?>> p;


	public GraphSequenceViewModel(DataAccess access, Mode mode, Predicate<DimensionContainer<DataItem, ?>> p) {
		this.access = access;
		this.mode = mode;
		this.p = p;
	}

	
	@Override
	public void setDataSet(DimensionalDataSet<DataItem, D> dataset) {
		super.setDataSet(dataset);
	}

	
	@Override
	public DimensionalDataSet<DataItem,D> getDataSet(){
		return (DimensionalDataSet<DataItem, D>) dataset;
	}


	@Override
	protected void dataSetHasChanged() {
		// For each dataset ensure that the target is a SeriesGenerator type
		for(int i = 0; i<dataset.numQueries(); i++){
			final MetaQueryable mq  = dataset.queryInfo(i).get(QueryInfo.TARGET);
			if(mq instanceof MetaLink)
				throw new RuntimeException("SeriesGeneratorViewModel cannot handle Links as target only, SeriesGenerators");
			if(!GraphSequencePointer.class.isAssignableFrom(((MetaClass) mq).itemClass(access.dataRegistry())))
				throw new RuntimeException("SeriesGeneratorViewModel can only handle SeriesGenerator as targets");
		}			
		datasets.clear();
	}

	
	
	public Dimension<DataItem,D> getGeneratedDimension(int queryIndex){
		final MetaClass mc  = (MetaClass) dataset.queryInfo(queryIndex).get(QueryInfo.TARGET);
		return (Dimension<DataItem, D>) GraphSequenceHints.getHints((Class<? extends GraphSequencePointer>) mc.itemClass(access.dataRegistry())).generatedDimension();
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DimensionContainer<DataItem, D>> possibleDimensions(int i) {
		final MetaClass mc  = (MetaClass) dataset.queryInfo(i).get(QueryInfo.TARGET);
		return (List) GraphSequenceHints.getHints((Class<? extends GraphSequencePointer>) mc.itemClass(access.dataRegistry())).encounteredQueryable(access, mc.itemDeclaredType())
		.stream().flatMap(mq->mq.getReadables().filter(dimensionPredicate())).collect(Collectors.toList());
	}
	
	
	
	


	public void consumeQuery(int query, GraphSequenceConsumerFactory factory) throws DataAccessException{

		if(factory==null)
			return;

		//Regenerate the SeriesGenerator if not already done
		Map<String, List<GraphSequencePointer>> series = datasets.get(query);
		if(series == null){
			
			series = new HashMap<>();
			datasets.put(query, series);
			
			final ResultAccess<String, Set<Integer>> result = dataset.loadQuery(query, Actions.asSet(DataItem.idKey, -1));

			final MetaClass mc = (MetaClass) dataset.queryInfo(query).get(QueryInfo.TARGET);
			for(int i = 0; i<result.size(); i++){//TODO parallel intstream
				final Set<Integer> ids = result.getResult(i);
				final List list = 
						access.queryFactory().regenerate(mc.itemDeclaredType())
						.toDepth(0).traverseAllLinks().includeAllNodes().regenerateAllKeys()
						.doNotGetAll().useFilter(P.setContains(DataItem.idKey, ids)).run().getAllTargets();
				series.put(result.getKey(i), list);
			}
		}
		
		// Now that we have the SeriesGenerators, make transaction
		series.forEach((s,l)->{
			try {
				
				for(GraphSequencePointer sg : l){
					final Consumer<Path<NodeItem,Link>> c = factory.createConsumer(s, sg.getAttribute(DataItem.idKey).get());
					sg.consume(access, c);
				}
				
			
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		

	}



	@Override
	public SeriesGeneratorBroker<D> dimensionBroker(String series) {
		return brokers.get(series);
	}



	@SuppressWarnings("unchecked")
	@Override
	public void setBroker(String series, SeriesGeneratorBroker<D> broker) {
		SeriesGeneratorBroker<D> old = brokers.put(series, broker);
		brokerLstrs.forEach(l->l.brokerChanged(this, series, old, broker));
	}

	
	
	
	@Override
	public SeriesGeneratorBroker<D> createBroker(List<Dimension<DataItem, D>> list) {
		return new SeriesGeneratorBroker<>(list);
	}


	@Override
	public Predicate<DimensionContainer<DataItem, ?>> dimensionPredicate() {
		return p;
	}


	@Override
	public Mode getDimensionMode() {
		return mode;
	}
	
	
	
	


	@Override
	public void addBrokerChangedListener(BrokerChangedListener<DataItem, SeriesGeneratorBroker<D>, D> l) {
		brokerLstrs.add(l);
	}



	@Override
	public void removeBrokerChangedListener(BrokerChangedListener<DataItem, SeriesGeneratorBroker<D>, D> l) {
		brokerLstrs.remove(l);
	}




}

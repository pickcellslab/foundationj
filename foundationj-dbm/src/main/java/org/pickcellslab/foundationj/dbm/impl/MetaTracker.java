package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.impl.MetaInfoImpl.MetaBuilder;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class takes {@link Collection} of {@link WritableDataItem} and creates the associated {@link MetaImpl}.
 * 
 * @author Guillaume
 *
 */
class MetaTracker {



	private final static Logger log = LoggerFactory.getLogger(MetaTracker.class);


	private final MetaBuilder builder;

	private final ExecutorService executor;


	private volatile int activeFeeds = 0;




	/**
	 * Creates a new {@link MetaTracker} initialized with the provided {@link MetaBuilder}
	 * Information previously entered in the builder will be used as the template of the {@link MetaInfo}
	 * building process. If the provided information are incomplete (in comparison to the types encountered
	 * during the tracking) then objects with "unknown" creator and "not available" description will be created
	 * @param builder
	 */
	MetaTracker(MetaBuilder builder){
		this.builder = builder;
		executor = Executors.newCachedThreadPool();
	}


	void track(Collection<? extends WritableDataItem> items){
		activeFeeds++;
		executor.execute(new DataFeed(this,items));
	}

	MetaInfo getMetaInfo(){
		try {
			//System.out.println("MetaTracker building MetaInfo");

			//Wait all the DataFeed are off
			//while(activeFeeds!= 0){}

			executor.shutdown();

			//System.out.println("MetaTacker : ActiveFeeds depleted");

			//Kill the MetaMaker
			//queue.add(new Killer());

			//System.out.println("MetaTacker : Killer sent");

			executor.awaitTermination(10, TimeUnit.SECONDS);

			//System.out.println("MetaTacker : executor done!");

		}
		catch (InterruptedException e) {
			log.error(e.getMessage());
		}
		return builder.buildInfo();
	}


	private void feedDone() {
		activeFeeds--;    
	}


	private class DataFeed implements Runnable {
		private final Collection<? extends WritableDataItem> items;
		private final MetaTracker tracker;

		DataFeed(MetaTracker tracker, Collection<? extends WritableDataItem> items) {
			log.trace("new DataFeed!");
			this.items = items; 
			this.tracker = tracker;
		}
		@Override
		public void run() {  
			try {
				final Iterator<? extends WritableDataItem> it = items.iterator();
				while(it.hasNext()){					
					builder.feed(it.next());
				}
			} catch (InstantiationHookException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("MetaModel  Instantiation failed", e);
			}
			finally {
				tracker.feedDone();
			}
		}


	}





}


package org.pickcellslab.foundationj.datamodel.functions;

import java.util.Collections;
import java.util.Iterator;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.FilteredIterator;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "FilteredNodesFromTraversal")
public class TraversalToNodesWithPredicate implements ExplicitFunction<Traversal<NodeItem,Link>, Iterator<? extends DataItem>>{

	@Supported
	private final ExplicitPredicate<? super NodeItem> predicate;
	
	public TraversalToNodesWithPredicate(ExplicitPredicate<? super NodeItem> predicate) {
		MappingUtils.exceptionIfNullOrInvalid(predicate);
		this.predicate = predicate;
	}
	

	@Override
	public Iterator<NodeItem> apply(Traversal<NodeItem, Link> t) {
		return t==null ? Collections.emptyIterator() : new FilteredIterator<>(t.nodes().iterator(), predicate);
	}

	@Override
	public String description() {
		return predicate.description() + " nodes";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class<Iterator<? extends DataItem>> getReturnType() {
		return (Class)Iterator.class;
	}
	
}

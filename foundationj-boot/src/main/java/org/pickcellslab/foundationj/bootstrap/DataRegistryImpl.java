package org.pickcellslab.foundationj.bootstrap;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.data.DbElement;
import org.pickcellslab.foundationj.mapping.data.DefaultInstantiationHook;
import org.pickcellslab.foundationj.mapping.data.InstantiationHook;
import org.pickcellslab.foundationj.mapping.data.InstantiationListener;
import org.pickcellslab.foundationj.mapping.data.NotifyingInstantiationHook;
import org.pickcellslab.foundationj.mapping.exceptions.DataTypeException;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Instance Factory for {@link Data} objects using {@link DbElement} as input.
 * 
 * @author Guillaume Blin
 *
 */
@CoreImpl
public class DataRegistryImpl implements DataRegistry{

	private static final Logger log = LoggerFactory.getLogger(DataRegistryImpl.class);
	
	private final Map<String, Class<?>> types = new HashMap<>();
	private final Map<Class<?>, InstantiationHook> hooks = new HashMap<>();


	public DataRegistryImpl(Collection<DiscoveredData> dataClasses) {

		final Map<Class<?>, InstantiationHook> hookInstances = new HashMap<>();
		// add the default one first
		hookInstances.put(DefaultInstantiationHook.class, new DefaultInstantiationHook(this));
		
		dataClasses.stream().flatMap(dc->dc.discovered()).forEach(c->{

			try {
				Data d= c.getAnnotation(Data.class);
				if(d!=null) {
					types.put(d.typeId(), c);
					InstantiationHook instance = hookInstances.get(d.hook());
					if(instance==null) {
						instance = createInstance(d.hook());
						hookInstances.put(d.hook(), instance);
					}
					hooks.put(c, instance);
				}


			}catch(Exception e) {
				throw new RuntimeException("An InstantiationHook implementation does not"
						+ " have a constructor which takes a DataRegistry as argument",e);
			}
		});		
		
		
	}  

	private InstantiationHook createInstance(Class<? extends InstantiationHook> hook) throws Exception {
		final Constructor<? extends InstantiationHook> c = hook.getDeclaredConstructor(DataRegistry.class);
		c.setAccessible(true);
		return c.newInstance(this);
	}



	/**
	 * @return a Stream of data types defined in the data model, including classes which have no stored instance
	 * in the database and therefore not yet available via the {@link MetaModel}
	 */
	public Stream<Class<?>> dataTypes(){
		return types.values().stream();
	}


	public Class<?> classOf(DbElement data){
		return classOf(data.typeId()); //TODO check is known
	}

	/**
	 * @param typeId
	 * @return The java Class corresponding to the specified typeId or null if the given typeId is not known.
	 */
	public Class<?> classOf(String typeId){
		return types.get(typeId);
	}


	/**
	 * @param proxy The persisted DataElement with the generic backend implementation to be rebuilt into the corresponding data model implementation.
	 * @return A new DataElement with the data model implementation
	 * @throws DataTypeException
	 * @throws InstantiationHookException 
	 */
	public Object getDataInstance(DbElement proxy) throws DataTypeException, InstantiationHookException{		
		Objects.requireNonNull(proxy, "The given DataElement proxy is null");
		Class clazz = classOf(proxy);
		if(clazz==null) {
			log.warn("Unknown type for dbElement: "+proxy.typeId());
			throw new DataTypeException("Unknown type for dbElement: "+proxy.typeId());
		}
		return hooks.get(clazz).getDataInstance(proxy);		
	}


	/**
	 * @param itfc
	 * @return A {@link Stream} of the known {@link Data} types implementing the given interface
	 */
	public <T> Stream<Class<? extends T>> knownTypes(Class<T> itfc){
		return types.values().stream().filter(c->itfc.isAssignableFrom(c)).map(c->(Class<? extends T>)c);
	}


	/**
	 * Registers the given {@link InstantiationListener} with the {@link NotifyingInstantiationHook} instance maintained by this 
	 * {@link DataRegistryImpl}
	 * @param l
	 * @param hookClass
	 */
	public <T> void addInstatiationListener(InstantiationListener<T> l, Class<? extends NotifyingInstantiationHook<T>> hookClass) {

		Optional<NotifyingInstantiationHook<T>> found =
				hooks.values().stream().filter(h->h.getClass()==hookClass)
				.map(h->(NotifyingInstantiationHook<T>) h).findFirst();

		if(!found.isPresent()) throw new IllegalArgumentException("the provided hookClass ("+hookClass.getSimpleName()+
				") is unknown to this DataRegistry");

		found.get().addInstantiationListener(l);
	};


	public <H extends InstantiationHook> H getHook(Class<H> hookClass) {
		Optional<H> found =
				hooks.values().stream().filter(h->h.getClass()==hookClass)
				.map(h->(H)h).findFirst();

		if(!found.isPresent()) throw new IllegalArgumentException("the provided hookClass ("+hookClass.getSimpleName()+
				") is unknown to this DataRegistry");

		return found.get();
	}


}

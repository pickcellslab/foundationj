package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="MetaArrayReduction")
class MetaDelegatingArrayReduction<D> implements ExplicitReduction<DataItem, D>{


	@Supported
	private ExplicitReduction<DataItem, D> op;

	public MetaDelegatingArrayReduction(ExplicitReduction<DataItem, D> op) {
		MappingUtils.exceptionIfNullOrInvalid(op);
		this.op = op;
	}


	@Override
	public void root(DataItem root, boolean included) {
		op.root(root, included);
	}


	@Override
	public void include(DataItem t) {
		op.include(t);		
	}

	@Override
	public D getResult() {
		return op.getResult();
	}

	@Override
	public ExplicitReduction<DataItem, D> factor() {
		return new MetaDelegatingArrayReduction<>(op.factor());
	}

	@Override
	public Class<D> getReturnType() {
		return op.getReturnType();
	}

	@Override
	public String description() {
		return op.description();
	}


}

package org.pickcellslab.foundationj.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Specifies that the annotated class implements the <a href="http://gen5.info/q/2008/07/25/the-multiton-design-pattern/"> Multiton <a/> pattern.
 * <br><br>
 * Implementing classes must fulfill the following requirements:
 * <ul>
    <li>A static getOrCreate() method must be present</li>
    <li>The first argument of the getOrCreate() method must be typed with the Class returned by {@link Multiton#manager()}</li>
    <li>The constructors must not be public</li>
</ul>
 * 
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface Multiton {

	/**
	 * @return The Class responsible for managing instances of the annotated class
	 */
	Class<?> manager(); 
	
}

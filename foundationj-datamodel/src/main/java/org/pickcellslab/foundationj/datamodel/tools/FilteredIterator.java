package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * An {@link Iterator} filtered by a given {@link Predicate}
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type object held by this Iterator
 */
public class FilteredIterator<T> implements Iterator<T> {

	private final Iterator<T> origin;
	private final Predicate<? super T> p;
	private T next;
	

	public FilteredIterator(Iterator<T> origin, Predicate<? super T> p){
		Objects.requireNonNull(origin,"The original Iterator cannot be null");
		Objects.requireNonNull(p,"The Predicate cannot be null");
		this.origin = origin;
		this.p = p;
		while(next == null && origin.hasNext()){
			T t = origin.next();
			if(p.test(t))
				next = t;
		}
	}

	@Override
	public boolean hasNext() {
		return next != null;
	}

	@Override
	public T next() {
		T toReturn = next;
		next = null;
		while(next == null && origin.hasNext()){
			T t = origin.next();
			if(p.test(t))
				next = t;
		}
		return toReturn;
	}
	
}

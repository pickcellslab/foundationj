package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Represents a Mixture of distributions which is also a {@link Distribution} itself
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of the variables in the distributions
 * @param <D> The type of Distribution mixed in this model
 */
public interface MixtureModel<T,D extends Distribution<T>> extends Distribution<T> {

	/**
	 * @return The number of components in this mixture model
	 */
	public int numDistributions();
	/**
	 * @return The weights of each component in this mixture model. (Same order as {@link #distributions()})
	 */
	public double[] weights();
	/**
	 * @return The components of this mixture model
	 */
	public Distribution<T>[] distributions();
	/**
	 * Predicts the class of the provided points based on this mixture model
	 * @param coordinates
	 * @return An array of integer where the indices are the same as indices in coordinates and where
	 * values correspond to indices in {@link #distributions()}
	 */
	public int[] predictions(T[] coordinates);
	/**
	 * Predicts the class of the provided point based on this mixture model
	 * @param coordinate
	 * @return The indice in {@link #distributions()} giving the predicted class of the provided coordinate
	 */
	public int predictions(T coordinate);
	/**
	 * Computes the "membership probability" to the components of this mixture model
	 * @param coordinates
	 * @return A matrix of double M[coords][numD] where coords corresponds to indices in coordinates and numD
	 * correspond to indices in {@link #distributions()}. The values indicate the membership probability
	 */
	public double[][] memberships(T[] coordinates);
	/**
	 * Computes the "membership probability" to the components of this mixture model
	 * @param coordinate
	 * @return An array of double indicating the membership probability for each component of the mixture
	 */
	public double[] memberships(T coordinate);
	
	/**
	 * Compute the probability density function estimate at each coordinate in the provided array
	 * @param coordinates
	 * @return A double array holding the densities at each coordinate
	 */
	public double[] densities(T[] coordinates);
	/**
	 * Compute the probability density function estimate at the specified coordinate
	 * @param coordinate
	 * @return The probability density function estimate at the specified coordinate
	 */
	public double density(T coordinate);
}

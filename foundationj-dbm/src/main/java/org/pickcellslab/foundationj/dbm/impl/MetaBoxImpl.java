package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.ManuallyStored;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;


final class MetaBoxImpl implements MetaBox {

	
	private final StepTraverser<NodeItem,Link> meta;
	private String description = "MetaStore -> ";
	private final Session<?, ?> session;
	
	// This TraverserConstraints ensures that no Manually stored objects are stored when automatically creating a MetaQueryable
	private static final TraverserConstraints tc = Traversers.newConstraints().fromMinDepth().toMaxDepth()
			.traverseLink(MetaClass.fromLink, Direction.BOTH)
			.and(MetaClass.toLink, Direction.BOTH)
			.and(MetaKey.keyLink, Direction.BOTH)
			.and(MetaTag.tagLink, Direction.BOTH)
			.includeAllNodes();

		
	MetaBoxImpl(MetaModel item, Session<?,?> session) {		
		Objects.requireNonNull(item);
		this.meta = Traversers.breadthfirst(item,tc);
		description += item.description();
		this.session = session;
	}
	
	MetaBoxImpl(ManuallyStored item, Session<?,?> session) {		
		Objects.requireNonNull(item);
		this.meta = Traversers.breadthfirst(item, item.storageConstraints());
		description += item.description();
		this.session = session;
	}


	@Override
	public StepTraverser<NodeItem,Link> get(){
		return meta;
	}
	
	@Override
	public String description() {
		return description;
	}
	
	@Override
	public Void run() throws DataAccessException{
		return session.runQuery(this);
	}

}

package org.pickcellslab.foundationj.dbm.impl;

import java.util.List;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.CombinedBinary;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;

@Mapping(id="BinaryTreeGrouping")
class CombinedBinarySplitter<T> implements ExplicitSplit<T, CombinedBinary<T>> {

	@Supported
	private final List<ExplicitPredicate<? super T>> predicates;
	private final CombinedBinary<T> root;

	public CombinedBinarySplitter(List<ExplicitPredicate<? super T>> predicates) {
		this.predicates = predicates;
		root = CombinedBinary.createTree(predicates);
	}
	
	
	@Override
	public String description() {
		return "Filter tree grouping";
	}	
	


	@Override
	public CombinedBinary<T> getKeyFor(T t) {
		return CombinedBinary.get(root, t);
	}

}

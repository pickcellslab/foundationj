package org.pickcellslab.foundationj.services;

import java.io.File;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Service;

@Core
@Service
public interface PreferencesAccess {
	
	
	/**
	 * @return The path of the last directory set via {@link #setLastDirectory(File)}
	 */
	public String getLastDirectory();


	/**
	 * @param directory The path of the last directory
	 */
	public void setLastDirectory(File directory);
	
	
}

package org.pickcellslab.foundationj.services;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * 
 * Handles {@link Exception}s which may be thrown by {@link TaskProgress}s executed by a {@link ProgressControl}
 * 
 * @author Guillaume Blin
 *
 * @param <V> The type of result created by TaskProgress objects 
 */
public interface ProgressExceptionHandler<V> {

	
	/**
	 * Handles a {@link CancellationException} thrown by a given task from within the given {@link ProgressControl}
	 * @param source The source {@link ProgressControl}
	 * @param task The {@link TaskProgress} which generated the exception
	 * @param e The exception thrown by the task
	 */
	public void handle(ProgressControl<V> source, TaskProgress<V> task, CancellationException e);
	
	/**
	 * Handles a {@link InterruptedException} thrown by a given task from within the given {@link ProgressControl}
	 * @param source The source {@link ProgressControl}
	 * @param task The {@link TaskProgress} which generated the exception
	 * @param e The exception thrown by the task
	 */
	public void handle(ProgressControl<V> source, TaskProgress<V> task, InterruptedException e);
	
	/**
	 * Handles a {@link ExecutionException} thrown by a given task from within the given {@link ProgressControl}
	 * @param source The source {@link ProgressControl}
	 * @param task The {@link TaskProgress} which generated the exception
	 * @param e The exception thrown by the task
	 */
	public void handle(ProgressControl<V> source, TaskProgress<V> task, ExecutionException e);
}

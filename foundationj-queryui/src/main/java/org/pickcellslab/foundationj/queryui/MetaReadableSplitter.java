package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.ToStringDimensionSplitter;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

@Data(typeId = "MetaGroupingByProperty", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaReadableSplitter extends MetaSplit {

	private MetaReadableSplitter(String uid) {
		super(uid);
	}

	static MetaReadableSplitter getOrCreate(MetaInstanceDirector director, MetaReadable key) throws InstantiationHookException {
		
		Objects.requireNonNull(key);
		
		final MetaReadableSplitter split = director.getOrCreate(MetaReadableSplitter.class, "SplitBy_"+key.recognitionString());
		if(!split.getAttribute(MetaModel.name).isPresent()){// Check if it was retrieved or if it already existed
			
			split.setAttribute(MetaItem.name, "Categories "+key.name());
			split.setAttribute(MetaItem.desc, "Group"+key.owner().name()+" by "+key.name());
			
			new DataLink(MetaModel.requiresLink, split, key, true);
		}
		return split;
	}

	
	
	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}
	
	


	@Override
	public String toHTML() {
		return "<HTML> GROUPING: " + name() 
		+ " <br> Description : "+ description()
		+ "</HTML>";
	}




	@Override
	public boolean pathSplitSupported() {
		return true;
	}


	@Override
	public boolean targetSplitSupported() {
		return true;
	}





	@Override
	public <E> ExplicitSplit< E, String> toSplitter() {
		final MetaReadable key = ((MetaReadable) getLinks(Direction.OUTGOING, MetaModel.requiresLink).iterator().next().target());
		final Dimension d = key.getDimension(0);
		return new ToStringDimensionSplitter(d);		
	}




	@Override
	public <V, E> ExplicitPathSplit<V, E, String> toPathSplitter() {

		return new ExplicitPathSplit<V,E,String>(){

			private final MetaReadable key = ((MetaReadable) getLinks(Direction.OUTGOING, MetaModel.requiresLink).iterator().next().target());
			private final Dimension d = key.getDimension(0);
			private final Predicate<DataItem> predicate = P.isDeclaredType(key.owner().itemDeclaredType());
			private final boolean isLink = key.owner() instanceof MetaLink;

			private FjAdaptersFactory<V, E> fctry;


			@Override
			public String description() {
				return "Group by "+key.name();
			}



			@Override
			public void setFactory(FjAdaptersFactory<V, E> f) {
				this.fctry = f;
			}

			@Override
			public String getKeyFor(Path<V, E> p) {
				if(isLink){

					//System.out.println("MetaReadableSplit is Link!");

					Iterator<E> ns = p.edges();
					while(ns.hasNext()){
						Link i = fctry.getLinkReadOnly(ns.next());
						if(predicate.test(i)){
							return Objects.toString(d.apply(i));
						}
					}
					return null;
				}else{

					//System.out.println("MetaReadableSplit is Node!");

					Iterator<V> ns = p.nodes();
					while(ns.hasNext()){
						NodeItem i = fctry.getNodeReadOnly(ns.next());
						if(predicate.test(i)){
							return Objects.toString(d.apply(i));
						}
					}
					return null;
				}
			}

		};




	}



}

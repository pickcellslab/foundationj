package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * Marks a class which implements one of the {@link Core} interfaces of the framework
 * <br> This annotation is required to allow the annotated class to be discovered at runtime during the boot process.
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(SOURCE)
@Target(TYPE)
public @interface CoreImpl {

	
}

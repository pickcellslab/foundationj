package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.ShortestPath;
import org.pickcellslab.foundationj.datamodel.tools.Traverser;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaKeyContainer;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaPath;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.data.InstantiationListener;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class handles the MetaModel (the metadata about the data graph) in memory and provides convenience static method 
 * to navigate it.
 * 
 * @author Guillaume Blin
 *
 */
final class MetaImpl implements Meta, MetaModelListener, InstantiationListener<MetaItem>{


	private static final Logger log = LoggerFactory.getLogger(MetaImpl.class);

	private final Session<?,?> access;
	private final MetaInstanceDirector metaInstanceMgr;

	private final List<MetaClass> classes = new ArrayList<>();
	private final List<MetaLink> links = new ArrayList<>();



	MetaImpl(Session<?,?> access, MetaInstanceDirector metaInstanceMgr){		
		this.access = access;
		access.addMetaListener(this);
		this.metaInstanceMgr = metaInstanceMgr;
		metaInstanceMgr.addInstantiationListener(this);

		try {
			access.queryFactory().regenerate("MetaModel").toMaxDepth()
			.traverseAllLinks().includeAllNodes().regenerateAllKeys()
			.getAll();
		} catch (DataAccessException e) {
			log.error("MetaModel could not be retrieved", e);
		}

	}



	@Override
	public MetaInstanceDirector getInstanceManager() {
		return metaInstanceMgr;
	}





	@Override
	public MetaPath shortestPath(MetaClass root, MetaQueryable target, TraverserConstraints constraints){

		//TODO ensure MetaLink.toLink and fromLink are the links that are traversed

		//Find the shortestpath between the 2 nodes
		//and translate the encountered link of the metamodel into link/direction pairs into the data graph
		ShortestPath sp = new ShortestPath(root, target, constraints);
		Optional<LinkedList<Link>> p = sp.call();
		if(p.isPresent())
			return new MetaPath(p.get(), root, target);	
		else return new MetaPath(new LinkedList<>(), root, target);
	}





	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#getMetaModel(org.pickcellslab.foundationj.datamodel.DataItem)
	 */

	@Override
	public MetaQueryable getMetaModel(DataItem dataItem){

		//Null check
		Objects.requireNonNull(dataItem,"The provided NodeItem cannot be null");


		//Check that this is not a node from the MetaModel
		if(MetaModel.class.isAssignableFrom(dataItem.getClass()))
			throw new IllegalArgumentException("The provided NodeItem cannot be part of the MetaModel already");


		//Look the database for a MetaQueryable with this type		
		if(Link.class.isAssignableFrom(dataItem.getClass())){
			//If this is a link we need to find the metaclass of the source and target to identify the accurate MetaLink
			Link l = (Link) dataItem;
			MetaClass source = (MetaClass) getMetaModel(l.source());
			if(source == null)
				return null;
			MetaClass target = (MetaClass) getMetaModel(l.target());
			if(target == null)
				return null;

			return source.metaLinks(l.declaredType(), Direction.OUTGOING).filter(ml->ml.target().equals(target)).findFirst().orElse(null);

		}			
		else{
			Predicate<MetaQueryable> p = mq -> mq.itemDeclaredType().equals(dataItem.declaredType());
			Collection<MetaQueryable> c = getQueryable(p);
			if(c.isEmpty())
				return null;
			else
				return c.iterator().next();
		}		
	}


	@Override
	public MetaClassImpl getMetaModel(String declaredType){

		//Null check
		Objects.requireNonNull(declaredType,"The provided NodeItem cannot be null");

		Predicate<DataItem> p = F.select(MetaItem.name).equalsTo(declaredType);
		Collection<MetaQueryable> c = getQueryable(p);
		if(c.isEmpty())
			return null;
		else
			return (MetaClassImpl) c.iterator().next();

	}


	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#getMetaLinks(java.lang.String)
	 */
	@Override
	public List<MetaLink> getMetaLinks(String linkType){
		//Null check
		Objects.requireNonNull(linkType,"The provided link type cannot be null");

		Predicate<DataItem> p = F.select(MetaLink.type).equalsTo(linkType);
		return (List)getQueryable(p);
	}


	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#getMetaLinks(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public MetaLink getMetaLinks(String linkType, String srcType, String tgType){
		//Null check
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		Objects.requireNonNull(srcType,"The provided source type cannot be null");
		Objects.requireNonNull(tgType,"The provided target type cannot be null");

		return links.stream().filter(
				ml->ml.linkType().equals(linkType)
				&& ml.source().itemDeclaredType().equals(srcType)
				&& ml.target().itemDeclaredType().equals(tgType))
				.findAny().orElse(null);
	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#hasSeveralSourceTargetCombination(java.lang.String)
	 */
	@Override
	public boolean hasSeveralSourceTargetCombination(String linkType) {
		return getMetaLinks(linkType).size()>1;
	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#classesExtending(java.lang.Class)
	 */
	@Override
	public List<MetaClass> classesExtending(Class<?> itrfc){
		//Null check
		Objects.requireNonNull(itrfc,"The provided class cannot be null");

		Predicate<MetaQueryable> p = mq->{
			if(mq instanceof MetaClass){
				return itrfc.isAssignableFrom(((MetaClass) mq).itemClass(access.dataRegistry()));
			}
			else
				return false;
		};

		List c = getQueryable(p);

		if(c.isEmpty())
			return Collections.emptyList();
		else
			return c;

	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#getQueryable(java.util.function.Predicate)
	 */
	@Override
	public List<MetaQueryable> getQueryable(Predicate<? super MetaQueryable> predicate){

		List<MetaQueryable> mq = new ArrayList<>(classes);
		mq.addAll(links);

		return mq.stream().filter(predicate).collect(Collectors.toList());
	}


	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#getReadable(java.util.function.Predicate)
	 */
	@Override
	public List<MetaReadable<?>> getReadable(Predicate<? super MetaReadable> predicate){

		List<MetaReadable<?>> mr = new ArrayList<>();
		classes.forEach(mc ->mr.addAll(mc.getReadables().filter(predicate).collect(Collectors.toList())));
		links.forEach(mc ->mr.addAll(mc.getReadables().filter(predicate).collect(Collectors.toList())));

		return mr;

	}










	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#linksAtDepth(org.pickcellslab.foundationj.dbm.meta.MetaClass, int)
	 */  
	@Override
	public List<MetaLink> linksAtDepth(MetaClass root, int depth){

		return 	Traversers.breadthfirst(root,
				Traversers.newConstraints()
				.fromDepth(depth*2)
				.toDepth(depth*2)// *2 because links are nodes in the MetaModel
				.traverseAllLinks().includeAllNodes())
				.traverse().nodes().stream()
				.filter(n->n instanceof MetaLink)
				.map(n->(MetaLink)n).collect(Collectors.toList());

	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#classesAtDepth(org.pickcellslab.foundationj.dbm.meta.MetaClass, int)
	 */
	@Override
	public Set<MetaClass> classesAtDepth(MetaClass root, int depth){

		return 	Traversers.breadthfirst(root,
				Traversers.newConstraints()
				.fromDepth(depth*2)
				.toDepth(depth*2)// *2 because links are nodes in the MetaModel
				.traverseAllLinks().includeAllNodes())
				.traverse().nodes().stream()
				.filter(n->n instanceof MetaClass)
				.map(n->(MetaClass)n).collect(Collectors.toSet());
	}






	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#getAll()
	 */
	@Override
	public Set<MetaClass> getAll(){
		return new HashSet<>(classes);
	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.meta.Meta#getMetaObjects(java.lang.Class)
	 */
	@Override
	public <T extends MetaModel> Stream<T> getMetaObjects(Class<T> clazz) {

		if(classes.isEmpty())
			return new ArrayList<T>().stream();

		return Traversers.breadthfirst(classes.get(0), Traversers.doNotConstrain()).traverse().nodes().stream().filter(n->clazz.isAssignableFrom(n.getClass())).map(n ->(T)n);

	}





	/**
	 * @param clazz The Class of {@link WritableDataItem} for which the AKey meta information
	 * must be retrieved
	 * @return A Collection of {@link MetaKey} holding information about each 
	 * {@link AKey} object found in the specified class
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Collection<MetaKey<?>> getKeys(DataAccess session, Class<? extends WritableDataItem> clazz){
		RegeneratedItems set = null;
		try {      
			set = session.queryFactory()
					.regenerate(MetaKey.class)
					.toDepth(0)
					.traverseAllLinks()
					.includeAllNodes()
					.regenerateAllKeys()
					.doNotGetAll()
					.useTraversal(TraversalDefinition.newDefinition().startFrom(DataRegistry.typeIdFor(MetaClass.class))
							.having(MetaModel.recognition, clazz.getTypeName())
							.fromMinDepth().toDepth(1).traverseAllLinks().includeAllNodes().build())
					.run();
		}
		catch (DataAccessException e) {
			log.error("Unable to read the database ; cause -> "+ e.getMessage());
			return Collections.emptySet();
		}
		return (Collection) set.getTargets(MetaKey.class).collect(Collectors.toList());
	}





	/**
	 * @param key The AKey object meta information are required for
	 * @param clazz The class of {@link WritableDataItem} the key belongs to
	 * @return A {@link MetaKey} holding information about the desired {@link AKey}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Optional<MetaKey<?>> get(DataAccess session, AKey<?> key, Class<? extends WritableDataItem> clazz){

		RegeneratedItems set = null;
		try {      
			set = session.queryFactory()
					.regenerate(MetaKey.class) 
					.toDepth(0)
					.traverseAllLinks().includeAllNodes().regenerateAllKeys()
					.doNotGetAll()
					.useTraversal(TraversalDefinition.newDefinition()
							.startFrom(DataRegistry.typeIdFor(MetaClass.class))
							.having(MetaModel.recognition, clazz.getTypeName())
							.fromMinDepth().toDepth(1).traverseAllLinks()
							.withOneEvaluation(
									P.keyTest(MetaKey.name, Op.TextOp.EQUALS, key.name),
									Decision.INCLUDE_AND_CONTINUE,
									Decision.EXCLUDE_AND_CONTINUE).build())
					.run();
		}
		catch (DataAccessException e) {
			log.error("Unable to read the database ; cause -> "+ e.getMessage());
			return Optional.empty();
		}
		return (Optional)set.getOneTarget(MetaKey.class);
	}


	public static Set<String> getTags(Class<?> clazz){    
		Set<String> tagSet= new HashSet<>();    
		//Get all superclasses
		Class<?> superClass = clazz.getSuperclass();
		while(superClass != null){
			//CheckTag
			for(Tag t : superClass.getDeclaredAnnotationsByType(Tag.class)){
				tagSet.add(t.tag()); 
			} 
			superClass = superClass.getSuperclass();
		}
		//Get all interfaces
		for(Class<?> itrfc : clazz.getInterfaces()){
			for(Tag t : itrfc.getDeclaredAnnotationsByType(Tag.class)){
				tagSet.add(t.tag()); 
			}      
		}    
		return tagSet;    
	}









	public static Set<MetaTag> getMetaTags(Class<?> clazz, MetaInstanceDirector director) throws InstantiationHookException{

		Set<MetaTag> tagSet= new HashSet<>();

		//Get all superclasses
		Class<?> superClass = clazz.getSuperclass();
		while(superClass != null){
			//CheckTag
			for(Tag t : superClass.getDeclaredAnnotationsByType(Tag.class)){
				tagSet.add(MetaTag.getOrCreate(director, t.tag(),t.description())); 
			} 
			superClass = superClass.getSuperclass();
		}
		//Get all interfaces
		for(Class<?> itrfc : clazz.getInterfaces()){
			for(Tag t : itrfc.getDeclaredAnnotationsByType(Tag.class)){
				tagSet.add(MetaTag.getOrCreate(director,t.tag(),t.description())); 
			}      
		}    
		return tagSet;    
	}


	/**
	 * @return Each subgraph of the MetaModel contained in the provided collection
	 * of {@link MetaClass}
	 */
	public static Collection<Traverser<NodeItem,Link>> toTraversers(Collection<MetaClass> meta){

		LinkedList<MetaClass> list = new LinkedList<>();
		list.addAll(meta);    
		List<Traverser<NodeItem,Link>> result = new ArrayList<>();

		while(!list.isEmpty()){
			DefaultTraversal<NodeItem,Link> t = Traversers.breadthFirst(list.peekFirst()).traverse();
			result.add(Traversers.breadthFirst(list.pollFirst()));
			list.removeAll(t.nodes);
		}

		return result;
	}





	@Override
	public void instanceWasCreated(MetaItem instance) {
		if(instance instanceof MetaClass)
			classes.add((MetaClass) instance);
		else if(instance instanceof MetaLink)
			links.add((MetaLink) instance);		
	}



	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {

		// Handle deletions
		// No need to delete links connecting these items,
		// This is taken care of by the MetaInstanceDirector
		//System.out.println("Meta received MetaEvent");
		if(evt == MetaChange.DELETED){
			meta.getTargets(MetaModel.class).sequential().forEach(m->{
				if(m instanceof MetaLink)
					this.links.remove(m);
				else if( m instanceof MetaClass)
					classes.remove(m);
			});		 
		}
	}



	@Override
	public void performIntegrityCheck() {
		// Check for MetaKey duplicates due to rewrite over old database form

		//FIXME create a Map with name() / MetaKey

		List<MetaKey<?>> toDel = new ArrayList<>();
		classes.forEach(mc->check(mc,toDel));
		links.forEach(ml->check(ml,toDel));

		try {
			for(MetaKey<?> mk : toDel)
				access.notify(access.queryFactory().deleteMeta().run(mk.deletionDefinition(),-1),	MetaChange.DELETED);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	private void check(MetaKeyContainer mc, List<MetaKey<?>> toDel) {
		final Map<String,MetaKey<?>> map = new HashMap<>();
		mc.keys().forEach(mk->{
			final MetaKey<?> duplicate = map.get(mk.name());
			if(duplicate!=null) {
				// Check which metakey is the old one
				if(!mk.recognitionString().contains(mk.toAKey().toString()))
					toDel.add(mk);
				else
					toDel.add(duplicate);
			}
			else
				map.put(mk.name(), mk);
		});
		
	}










}

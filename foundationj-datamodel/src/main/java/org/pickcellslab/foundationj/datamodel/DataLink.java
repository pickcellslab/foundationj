package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Scope;



/**
 * This class is a default implementation of {@link Link}, 
 * using the default implementation of {@link DefaultData}
 * 
 * @see WritableDataItem
 * 
 * @author Guillaume
 * 
 */
@Data(typeId = "DataLink")
@Scope
public class DataLink extends DefaultData implements Link {


	private String type;
	private NodeItem source;
	private NodeItem target;

	
	@SuppressWarnings("unused")
	private DataLink() {/*database only*/}
	

	/**
	 * Creates a new Silent Link with the specified type, source and target.
	 * @param type
	 * @param source
	 * @param target
	 * @parma a flag to determine if the link should automatically be registered in the source and target during instantiation
	 */
	public DataLink(String type, NodeItem source, NodeItem target, boolean addAutomatically) {

		Objects.requireNonNull(source);
		Objects.requireNonNull(target);

		this.type = type;
		this.source = source;
		this.target = target;
		if(addAutomatically){
			source.addLink(this);
			target.addLink(this);
		}
	}
	
	
	@Override
	public String declaredType() {
		return type;
	}

	@Override
	public NodeItem source() {
		return source;
	}

	@Override
	public NodeItem target() {
		return target;
	}

	@Override
	public void delete() {	
		if(source!=null){ //already deleted
			source.removeLink(this, true);
			this.attributes = null;
			target = null;
			source = null;
			type = null;	
		}
	}

	@Override
	public String toString() {
		return source.toString()+"--["+declaredType()+"]--> "+target.toString();
	}

	


}

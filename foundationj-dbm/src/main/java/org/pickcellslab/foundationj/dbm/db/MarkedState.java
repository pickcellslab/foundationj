package org.pickcellslab.foundationj.dbm.db;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * 
 * Defines the state of a database transaction
 * 
 * @author Guillaume Blin
 *
 */
public interface MarkedState extends AutoCloseable {

	/**
	 * Mark this transaction as successful and commit any change that may have occurred
	 */
	public void success();
	
	/**
	 * Marks this {@link DatabaseAccess} as failed and rolls back any change when the Transaction closes.
	 */
	public void fail();
	
}

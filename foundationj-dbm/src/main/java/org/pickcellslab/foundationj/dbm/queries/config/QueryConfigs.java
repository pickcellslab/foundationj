package org.pickcellslab.foundationj.dbm.queries.config;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig.PathMode;

public final class QueryConfigs<T,D> implements TargetedQueryConfigBuilder<T,D>, TraversalQueryConfigBuilder<T,D> {

	private final QueryWizardConfig<T,D> config;
	
	public static String DIRECTIONAL = "DIRECTIONAL";


	private QueryConfigs(QueryWizardConfig<T,D> config){				
		this.config = config;
	}


	
	
	/**
	 * Initialises the building process for a {@link QueryWizardConfig} which do not allow the creation of
	 * queries which use traversals
	 * @param textOrHTML
	 * @return A dedicated builder
	 */
	public static TargetedQueryConfigBuilder<DataItem,?> newNoTraversalConfig(String textOrHTM){
		QueryWizardConfig<DataItem,?> conf = new QueryWizardConfig<>(textOrHTM);
		conf.setPathEnabled(false);
		return new QueryConfigs<>(conf);
	}

	



	/**
	 * Initialises the building process for a {@link QueryWizardConfig} which do not allow the creation of
	 * queries which use traversals
	 * @param textOrHTML
	 * @return A dedicated builder
	 */
	public static TraversalQueryConfigBuilder<DataItem,?> newTargetedOnlyConfig(String textOrHTML){
		QueryWizardConfig<DataItem,?> conf = new QueryWizardConfig<>(textOrHTML);
		return new QueryConfigs<>(conf);
	}
	
	
	
	
	
	/**
	 * Initialises the building process for a {@link QueryWizardConfig} which do not allow the creation of
	 * queries which use traversals
	 * @param textOrHTML
	 * @return A dedicated builder
	 */
	public static TraversalQueryConfigBuilder<DataItem,Number> newTargetedOnlyToNumber(String textOrHTML){
		QueryWizardConfig<DataItem,Number> conf = new QueryWizardConfig<>(textOrHTML);
		return new QueryConfigs<>(conf);
	}
	
	
	
	/**
	 * Initialises the building process for a {@link QueryWizardConfig} which do not allow the creation of
	 * queries which use traversals
	 * @param textOrHTML
	 * @return A dedicated builder
	 */
	public static TargetedQueryConfigBuilder<DataItem,Number> newNoTraversalToNumber(String textOrHTM){
		QueryWizardConfig<DataItem,Number> conf = new QueryWizardConfig<>(textOrHTM);
		conf.setPathEnabled(false);
		return new QueryConfigs<>(conf);
	}
	
	
	
	
	
	/**
	 * Initialises the building process for a {@link QueryWizardConfig} which does not allow the creation of
	 * queries which use traversals. In addition only Queryable objects which possess directional data will be available
	 * to the user as targets of the query
	 * @param textOrHTML
	 * @return A dedicated builder
	 */
	public static TraversalQueryConfigBuilder<DataItem,double[]> newTargetedOnlyToVector(String textOrHTML){
		QueryWizardConfig<DataItem,double[]> conf = new QueryWizardConfig<>(textOrHTML);
		conf.setAllowedQueryables(
				(mq) -> mq.getReadables().anyMatch((mr)->mr.dataType() == dType.NUMERIC && mr.description().startsWith(DIRECTIONAL)));
		return new QueryConfigs<>(conf);
	}
	
	
	
	/**
	 * Initialises the building process for a {@link QueryWizardConfig} which do not allow the creation of
	 * queries which use traversals
	 * @param textOrHTML
	 * @return A dedicated builder
	 */
	public static TargetedQueryConfigBuilder<DataItem,double[]> newNoTraversalToVecor(String textOrHTM){
		QueryWizardConfig<DataItem,double[]> conf = new QueryWizardConfig<>(textOrHTM);
		conf.setPathEnabled(false);
		conf.setAllowedQueryables(
				(mq) -> mq.getReadables().anyMatch((mr)->mr.dataType() == dType.NUMERIC && mr.description().startsWith(DIRECTIONAL)));				
		return new QueryConfigs<>(conf);
	}
	
	
	
	
	
	



	/**
	 * Initialises the building process for a {@link QueryWizardConfig} which allows only {@link DataSet} holding 
	 * {@link Path} to be created
	 * @param textOrHTML
	 * @return A dedicated builder
	 */
	public static TraversalQueryConfigBuilder<Path<NodeItem,Link>,?> newPathOnlyConfig(String textOrHTML){
		QueryWizardConfig<Path<NodeItem,Link>,?> conf = new QueryWizardConfig<>(textOrHTML);
		conf.setPathMode(PathMode.PATH);
		conf.considerAllEnabled(false);
		return new QueryConfigs<>(conf);
	}

	
	
	
	
	

	@Override
	public TargetedQueryConfigBuilder<T,D> setAllowedQueryables(Predicate<MetaQueryable> allowedQueryables) {
		config.setAllowedQueryables(allowedQueryables);
		return this;
	}
	


	@Override
	public QueryConfigBuilder<T,D> numberOfQueriesAllowed(int min, int max) {
		config.numberOfQueriesAllowed(min, max);
		return this;
	}




	@Override
	public QueryConfigBuilder<T,D> disableGrouping() {
		config.setGroupingAllowed(false);
		return this;
	}

	
	@Override
	public TraversalQueryConfigBuilder<T, D> disablePathSPlitting() {
		config.setSplittingAllowed(false);
		return this;
	}
	

	@Override
	public TraversalQueryConfigBuilder<T,D> setSynchronizeRoots(boolean synchronizeRoots) {
		config.setSynchronizeRoots(synchronizeRoots);
		return this;
	}




	@Override
	public TraversalQueryConfigBuilder<T,D> setSynchronizeTarget(boolean b) {
		config.setSynchronizeTarget(b);
		return this;
	}




	@Override
	public TargetedQueryConfigBuilder<T,D> setFixedRootType(MetaClass fixedRootType) {
		config.setFixedRootType(fixedRootType);
		return this;
	}




	@Override
	public <E> TargetedQueryConfigBuilder<T,D> setFixedRootKey(MetaReadable<E> fixedRootKey, E value) {
		config.setFixedRootKey(fixedRootKey, value);
		return this;
	}



	@Override
	public QueryWizardConfig<T,D> build() {
		return config;
	}




	

	



}

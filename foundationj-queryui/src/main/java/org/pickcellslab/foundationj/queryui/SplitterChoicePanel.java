package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Copyable;
import org.pickcellslab.foundationj.ui.wizard.ValidityListener;
import org.pickcellslab.foundationj.ui.wizard.ValidityToggle;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class SplitterChoicePanel extends SplitterUI<SplitterChoicePanel> implements ValidityListener{

	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final Map<SplitID,SplitterUI> mapping = new HashMap<>();


	private SplitterUI current;
	private List<GroupingChangeListener> groupingLstrs = new ArrayList<>();

	public SplitterChoicePanel(UITheme theme, DataRegistry registry, ChoiceSet set, MetaQueryable target, boolean paths, boolean allowNonPredictable) {


		Objects.requireNonNull(set,"ChoiceSet is null");
		Objects.requireNonNull(target,"Target is null");

		
		setLayout(new BorderLayout(0, 0));


		JPanel radioPanel = new JPanel();
		radioPanel.setBorder(new TitledBorder(null, "Grouping Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(radioPanel, BorderLayout.NORTH);
		radioPanel.setLayout(new GridLayout(3, 2, 0, 2));

		JPanel configPanel = new JPanel();
		CardLayout card = new CardLayout(0, 0);
		configPanel.setLayout(card);


		if(paths){

			JRadioButton rdbtnGroupByDepth = new JRadioButton("Group by depth");
			radioPanel.add(rdbtnGroupByDepth);
			buttonGroup.add(rdbtnGroupByDepth);
			rdbtnGroupByDepth.addActionListener(l->{
				SplitterUI ui = mapping.get(SplitID.Depth);				
				current = ui;
				card.show(configPanel, SplitID.Depth.name());
				set.highlightFilter(target);	
				groupingLstrs.forEach(gl->gl.groupingMethodSelected(SplitID.Depth));
				this.fireNextValidityChanged();			
			});



			@SuppressWarnings("rawtypes")
			SplitterUI ui = new SplitterUI(){

				@Override
				public boolean validity() {
					return true;
				}

				@Override
				MetaSplit getSplitter(Meta meta) {
					try {
						return MetaDepthSplitter.getOrCreate(meta.getInstanceManager());
					} catch (InstantiationHookException e) {
						LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
						JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
						return null;
					}
				}

				@Override
				public void copyState(Copyable other) {}

			};
			mapping.put(SplitID.Depth, ui);
			current = ui;





			rdbtnGroupByDepth.setSelected(true);


		}else{
			JRadioButton rdbtnOneGroup = new JRadioButton("One Group");
			rdbtnOneGroup.setToolTipText("Pool all the objects into one group");
			buttonGroup.add(rdbtnOneGroup);
			radioPanel.add(rdbtnOneGroup);

			//Add by default the no split panel
			configPanel.add(new JLabel("All in One Group"), "One");

			rdbtnOneGroup.addActionListener(l->{
				if(rdbtnOneGroup.isSelected()){
					card.show(configPanel, "One");
					current = null;
					groupingLstrs.forEach(gl->gl.groupingMethodSelected(null));
					this.fireNextValidityChanged();
				}
			});
			rdbtnOneGroup.setSelected(true);
		}








		JRadioButton rdbtnSplitIn = new JRadioButton("Split in 2");
		rdbtnSplitIn.setToolTipText("<HTML><p align=\"justify\"><p width=\"500\">Select a filter which will be used to split your dataset into 2 groups: Accepted / Rejected by the filter</p></HTML>");
		buttonGroup.add(rdbtnSplitIn);
		radioPanel.add(rdbtnSplitIn);

		JRadioButton rdbtnGroupByFilter = new JRadioButton("Group by Filter");
		rdbtnGroupByFilter.setToolTipText("<HTML><p align=\"justify\"><p width=\"500\">Each filter you choose will represent one group. Note that the order of filters matters."
				+ "<br> If an object could be accepted by multiple filters only the group defined by the first filter which accepts it will contain this object. </p></HTML>");
		buttonGroup.add(rdbtnGroupByFilter);
		radioPanel.add(rdbtnGroupByFilter);

		JRadioButton rdbtnBinaryTree = new JRadioButton("Binary Tree");
		rdbtnBinaryTree.setToolTipText("<HTML><p align=\"justify\"><p width=\"500\">This is a \"multi-filter\" version of the \"Split in 2\" grouping type: The query will create [2^filter number] groups and objects can only fall into one group.\n<br>For example, if you have 2 filters A and B, the groups will be </br>\n<ul>\n<li>Accepted by A and B</li>\n<li>Rejected by A and B</li>\n<li>Accepted by A, Rejected by B</li>\n<li>Rejected by A, Accepted by B</li>\n</ul>\n</p></HTML> ");
		buttonGroup.add(rdbtnBinaryTree);
		radioPanel.add(rdbtnBinaryTree);

		JRadioButton rdbtnGroupByKey = new JRadioButton("Group by key value");
		rdbtnGroupByKey.setToolTipText("<HTML><p align=\"justify\"><p width=\"500\">\nSelect one attribute for your object. The query will create as many groups as there are values found for the selected attribute.</p></HTML>");
		buttonGroup.add(rdbtnGroupByKey);
		radioPanel.add(rdbtnGroupByKey);
		rdbtnGroupByKey.setEnabled(allowNonPredictable);
			

		JRadioButton rdbtnGroupByBins = new JRadioButton("Group by bins on key value");
		rdbtnGroupByBins.setToolTipText("<HTML><p align=\"justify\"><p width=\"500\">Select a numerical attribute and define multiple thresholds, the number of group will be : number of thresholds + 1</p></HTML>");
		buttonGroup.add(rdbtnGroupByBins);
		radioPanel.add(rdbtnGroupByBins);





		add(configPanel, BorderLayout.CENTER);



		rdbtnSplitIn.addActionListener(l->{
			if(rdbtnSplitIn.isSelected()){
				SplitterUI ui = mapping.get(SplitID.In2);
				if(ui == null){
					ui = new SplitIn2Panel<>(theme, set.getFilterChoice(target));
					ui.addValidityListener(this);
					configPanel.add(new JScrollPane(ui), SplitID.In2.name());
					mapping.put(SplitID.In2, ui);
				}
				current = ui;
				card.show(configPanel, SplitID.In2.name());
				set.highlightFilter(target);	
				groupingLstrs.forEach(gl->gl.groupingMethodSelected(SplitID.In2));
				this.fireNextValidityChanged();
			}
		});

		
		
		rdbtnGroupByFilter.addActionListener(l->{
			if(rdbtnGroupByFilter.isSelected()){
				SplitterUI ui = mapping.get(SplitID.FilterSet);
				if(ui == null){
					ui = new SplitByFilterPanel<>(theme, registry, set.getCurrentQueryable().get(), set.getFilterChoice(target));
					ui.addValidityListener(this);
					configPanel.add(ui, SplitID.FilterSet.name());
					mapping.put(SplitID.FilterSet, ui);
				}
				current = ui;
				card.show(configPanel, SplitID.FilterSet.name());
				set.highlightFilter(target);	
				groupingLstrs.forEach(gl->gl.groupingMethodSelected(SplitID.FilterSet));
				this.fireNextValidityChanged();
			}
		});
		
		
		

		rdbtnGroupByKey.addActionListener(l->{
			SplitterUI ui = mapping.get(SplitID.Key);
			if(ui == null){
				ui = new SplitByKeyPanel<>(theme, registry, set.getReadableChoice(target));
				ui.addValidityListener(this);
				configPanel.add(ui, SplitID.Key.name());
				mapping.put(SplitID.Key, ui);
			}
			current = ui;
			card.show(configPanel, SplitID.Key.name());
			set.highlightReads(target);	
			groupingLstrs.forEach(gl->gl.groupingMethodSelected(SplitID.Key));
			this.fireNextValidityChanged();			
		});


		rdbtnGroupByBins.addActionListener(l->{
			SplitterUI ui = mapping.get(SplitID.KeyBins);
			if(ui == null){
				ui = new SplitByKeyBinPanel<>(theme, registry, set.getReadableChoice(target));
				ui.addValidityListener(this);
				configPanel.add(ui, SplitID.KeyBins.name());
				mapping.put(SplitID.KeyBins, ui);
			}
			current = ui;
			card.show(configPanel, SplitID.KeyBins.name());
			set.highlightReads(target);	
			groupingLstrs.forEach(gl->gl.groupingMethodSelected(SplitID.KeyBins));
			this.fireNextValidityChanged();			
		});


		rdbtnBinaryTree.addActionListener(l->{
			SplitterUI ui = mapping.get(SplitID.BTree);
			if(ui == null){
				ui = new BinaryTreeSplitPanel<>(theme, set.getFilterChoice(target));
				ui.addValidityListener(this);
				configPanel.add(new JScrollPane(ui), SplitID.BTree.name());
				mapping.put(SplitID.BTree, ui);
			}
			current = ui;
			card.show(configPanel, SplitID.BTree.name());
			set.highlightFilter(target);	
			groupingLstrs.forEach(gl->gl.groupingMethodSelected(SplitID.BTree));
			this.fireNextValidityChanged();			
		});






	}


	
	
	@Override
	public void copyState(SplitterChoicePanel other) {
		
		Enumeration<AbstractButton> otherE =  other.buttonGroup.getElements();
		Enumeration<AbstractButton> thisE =  this.buttonGroup.getElements();
		while(otherE.hasMoreElements()){
			AbstractButton otherAB = otherE.nextElement();
			AbstractButton thisAB = thisE.nextElement();
			if(otherAB.isSelected()){
				thisAB.doClick();
			}
		}
		
		if(current!=null){
			current.copyState(other.current);
		}
	}
	
	
	
	
	
	


	@Override
	public boolean validity() {		
		return current == null || current.validity();
	}


	@Override
	public MetaSplit getSplitter(Meta meta){
		if(current!=null && current.validity())
			return current.getSplitter(meta);
		return null;
	}


	public SplitID selectedId(){
		for(Entry<SplitID, SplitterUI> e : mapping.entrySet())
			if(e.getValue() == current)
				return e.getKey();
		return null;
	}
	

	@Override
	public void nextValidityChanged(ValidityToggle wizardNode) {
		this.fireNextValidityChanged();
	}




	public void addGroupingMethodListener(GroupingChangeListener l) {
		groupingLstrs.add(l);
	}


	public void removeGroupingMethodListener(GroupingChangeListener l) {
		groupingLstrs.remove(l);
	}




}

package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Represents the output of a <a href="https://en.wikipedia.org/wiki/Graph_traversal"> graph traversal</a>. 
 * These objects are generated by {@link Traverser}s.
 * 
 * @author Guillaume Blin
 *
 * @param <S1> The type of the nodes of the traversed graph
 * @param <S2> The type of the edges of the traversed graph
 */
public interface Traversal<S1, S2> {

	/**
	 * @return An {@link Iterable} for the nodes encountered during the traversal
	 */
	public Iterable<S1> nodes();
	
	/**
	 * @return An {@link Iterable} for the edges encountered during the traversal
	 */
	public Iterable<S2> edges();
	
}

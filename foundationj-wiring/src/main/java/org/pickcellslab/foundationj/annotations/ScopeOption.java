package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * Provides a form of communication between Scopes with no parent/child relationships among them.
 * 
 * Used to annotate constructor arguments of Module or CoreImpl classes. {@link ScopeOption} specifies that the annotated
 * argument need not be a Modular or Core interface but that it should be provided as a stateful instance by the system
 * controlling the scopes lifecycles of the application.
 * <br><br>
 * For example a database session may need credentials before starting. An API can provide a
 * Core interface Session with scope "Session" as well as an interface Credentials (neither Modular nor Core). 
 * <br> The session implementation can then declare a constructor with the following arguments: 
 * {@code SessionImpl(Database db, @ScopeOption Credentials)}. This way when starting the Scope "Session" a controller
 * will know that a specific Credential instance is required.
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(SOURCE)
@Target(PARAMETER)
public @interface ScopeOption {

}

package org.pickcellslab.foundationj.dataviews.mvc;

import org.pickcellslab.foundationj.dbm.access.DataAccess;


@FunctionalInterface
public interface DataSetFactory<T> {

	
	public DataSet<T> create(DataAccess session) throws BuildCancelledException;
	
}

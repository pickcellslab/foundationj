package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener.MetaChange;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;

class DeletionListenerComplete implements DataDeletionListener {

	private final TargetType<?, ?, ?> target;
	private RegeneratedItems set;

	DeletionListenerComplete(TargetType<?,?,?> target){
		this.target = target;
	}




	@Override
	public void completeDeletion() throws DataAccessException {

		for(DataPointer pointer : target.metaTypes().get()){

			//Special case when we have a tag as pointer
			if(pointer instanceof MetaTag) {

				final AtomicBoolean bool = new AtomicBoolean(false);
				pointer.readAny(target.session(), i->{bool.set(true); return null;});
				if(bool.get() == false) {
					set = target.session().queryFactory().deleteMeta().run(((MetaTag) pointer).deletionDefinition(), 1);
				}

				// Now check the labeled classes
				for(MetaQueryable mc : pointer.queriedMetaObjects()){
					if(mc.initiateRead(target.session(), 1).makeList(DataItem.idKey).inOneSet().getAll().run().isEmpty()) {
						// if yes delete
						final RegeneratedItems tmpSet = target.session().queryFactory().deleteMeta().run(mc.deletionDefinition(), 1);
						if(set== null)
							set = tmpSet;
						else
							set.merge(tmpSet); 
					}
				}
			}
			else { // if not MetaTag

				for(MetaQueryable mq : pointer.queriedMetaObjects()) {
					final RegeneratedItems tmpSet = target.session().queryFactory().deleteMeta().run(mq.deletionDefinition(), 1);
					if(set== null)
						set = tmpSet;
					else
						set.merge(tmpSet);
				}

			}
		}

		if(set!=null)
			validate();

	}



	@Override
	public void keysDeletion(Collection<AKey<?>> deleted) throws DataAccessException {

		for(DataPointer pointer : target.metaTypes().get()){

			for(MetaQueryable ml : pointer.queriedMetaObjects()){

				for(AKey<?> k : deleted) {
					final Optional<MetaKey<?>> opt = (Optional)ml.metaKey(k);
					if(opt.isPresent()) {

						// if not, delete the MetaKey
						final RegeneratedItems tmpSet = target.session().queryFactory().deleteMeta().run(opt.get().deletionDefinition(), 1);
						if(set== null)
							set = tmpSet;
						else
							set.merge(tmpSet);

					}
				}
				
			}
			
		}

		if(set!=null)
			validate();

	}



	private void validate() {
		target.session().notify(set, MetaChange.DELETED);
	}

}

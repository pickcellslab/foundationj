package org.pickcellslab.foundationj.datamodel.graph;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;

/**
 * A graph which can be listened for changes in its structure (addition/deletion of nodes and edges)
 * Note that changes must be explicitly made via the graph methods {@link #edge(Node, Node)}, {@link #remove(Node)}
 * or {@link #remove(Edge)} or via the provided {@link EdgeFactory}. If edges are created via the implementation
 * constructor, the graph will not be able to intercept the change to dispatch to listeners.
 * 
 * @author Guillaume Blin
 *
 * @param <N> The type of Node
 * @param <E> The type of Edge
 */
public class ListenableGraph<N extends Node<E>, E extends Edge<N>> implements EdgeSetListener<E> {

	protected final EdgeFactory<N,E> eFactory;
	
	protected final Set<N> nodes = new LinkedHashSet<>();
	protected final Set<E> edges = new LinkedHashSet<>();
	
	protected final List<GraphListener<? super N,E>> lstrs = new ArrayList<>();
	
	
	public ListenableGraph(EdgeFactory<N,E> eFactory){
		Objects.requireNonNull(eFactory, "The provided EdgeFactory is null");
		this.eFactory = eFactory;
		eFactory.addListener(this);
	}
	
	public ListenableGraph(EdgeFactory<N,E> eFactory, Collection<N> nodes){
		Objects.requireNonNull(eFactory, "The provided EdgeFactory is null");
		this.eFactory = eFactory;
		this.nodes.addAll(nodes);
		eFactory.addListener(this);
	}
	
	/**
	 * Constructs a new ListenableGraph using the provided factories and root. If the root already has edges, the
	 * full graph will be reconstructed and registered into this ListenableGraph. 
	 * 
	 * @param eFactory
	 * @param root
	 */
	public ListenableGraph(EdgeFactory<N,E> eFactory, N root){
		Objects.requireNonNull(eFactory, "The provided EdgeFactory is null");		
		this.eFactory = eFactory;
		eFactory.addListener(this);
		DefaultTraversal<N,E> t = Traversers.breadthFirst(root).traverse();
		t.edges.forEach(e->edges.add(e));
		t.nodes.forEach(n->nodes.add(n));
	}
	
	
	public int numberOfNodes(){
		return nodes.size();
	}
	
	public int numberOfEdges(){
		return edges.size();
	}
	
	/**
	 * @return An unmodifiable view of the nodes contained in this graph
	 */
	public Set<N> nodeSet(){
		return Collections.unmodifiableSet(nodes);
	}
	
	/**
	 * @return An unmodifiable view of the edges contained in this graph
	 */
	public Set<E> edgeSet(){
		return Collections.unmodifiableSet(edges);
	}
	
	/**
	 * @param source The source Node
	 * @param target The target Node
	 * @return An edge linking the specified source and target
	 */
	public E edge(N source, N target){
		//will call the added method as we listen to the factory
		return eFactory.create(source,target);
	};
	
	
	/**
	 * Removes the specified edge from this graph. The source and target will no longer reference the
	 * edge, however the edge will not be deleted as specified by {@link Edge#delete()}.
	 * @param e The Edge to remove
	 */
	public void remove(E e){
		if(edges.remove(e)){
			e.source().removeLink(e, true);
			lstrs.forEach(l->l.removed(e));									
		}
	}	
	
	
	/**
	 * Removes the specified Node from this graph
	 * @param n The node to remove
	 */
	public void remove(N n){
		if(nodes.remove(n))
			lstrs.forEach(l->l.removed(n));
	}
	
	
	/**
	 * Registers the specified GraphListener to this Graph.
	 */
	public void addListener(GraphListener<? super N,E> l){
		lstrs.add(l);
	}
	
	/**
	 * Removes the specified GraphListener from this Graph.
	 */
	public void removeListener(GraphListener<? super N,E> l){
		lstrs.remove(l);
	}

	
	@Override
	public void removed(E e) {
		remove(e);		
	}

	
	@Override
	public void added(E e) {		
		if(edges.add(e)){
			lstrs.forEach(l->l.added(e));
			nodes.add(e.source());
			nodes.add(e.target());
		}		
	}
	
	
	
	
	
}

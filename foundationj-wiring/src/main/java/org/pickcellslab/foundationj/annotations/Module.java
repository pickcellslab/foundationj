package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Collection;

/**
 * 
 * Marks classes implementing a {@link Modular} interface. This annotation is required to enable the class to be discovered 
 * and registered in the container at boot time.
 * <br>
 * <br> <b>NB:</b> A class annotated with @{@link Module} must possess a constructor which is either empty, or with
 * arguments which are @{@link Modular} interfaces or arrays or {@link Collection} of @{@link Modular} interfaces.
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(SOURCE)
@Target(TYPE)
public @interface Module {

}

package org.pickcellslab.foundationj.dbm.queries;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;


	/**
	 * A Builder to obtain a {@link DataCollector}
	 * 
	 * @author Guillaume Blin
	 *
	 */
	public class DataCollectorBuilder<T extends DataItem>{

		//The list of BiConsumers which will update the map for each encountered DataItem
		private List<BiConsumer<Map<AKey<?>, Object>,? super T>> bics = new ArrayList<>();
		//All the keys registered in this gatherer including the ones hold by functions
		private Set<AKey<?>> registered = new HashSet<>();
		//The keys to accumulate
		private List<AKey<?>> keys = new ArrayList<>();

		

		/**
		 * Adds the specified {@link AKey} into the list of attributes to collect
		 * @param k The AKey object specifying the attribute to collect
		 * @throws IllegalStateException if this AKey is already in use in the builder
		 * @return The updated DataCollectorBuilder
		 */
		public DataCollectorBuilder<T> get(AKey<?> k){
			if(!registered.add(k))
				throw new IllegalStateException("This Gatherer already contains a key "
						+ "with the following description : "+k.toString());
			keys.add(k);
			return this;
		}

		/**
		 * Adds the specified {@link AKey} objects into the list of attributes to collect
		 * @param keys The collection of AKey object specifying the attribute to collect
		 * @throws IllegalStateException if one of the provided AKey is already in use in the builder
		 * @return The updated DataCollectorBuilder
		 */
		public DataCollectorBuilder<T> get(Collection<AKey<?>> keys){
			for(AKey<?> k : keys)
				if(!registered.add(k))
					throw new IllegalStateException("This Gatherer already contains a key "
							+ "with the following description : "+k.toString());
			this.keys.addAll(keys);
			return this;
		}

		/**
		 * Adds a function to be computed on the DataItem. The result of the function will collected
		 * and mapped to the provided AKey object
		 * @param f The {@link ExplicitFunction} to be computed
		 * @throws IllegalStateException if the provided AKey is already in use in the builder
		 * @return The updated DataCollectorBuilder
		 */
		public <E> DataCollectorBuilder<T> compute(ExplicitFunction<? super T, E> f, AKey<E> k){

			Objects.requireNonNull(f, "The provided function cannot be null");
			Objects.requireNonNull(k, "The provided key cannot be null");

			if(!registered.add(k))
				throw new IllegalStateException("This Gatherer already contains a key "
						+ "with the following description : "+k.toString());

			bics.add((m,d)->m.put(k, f.apply(d)));
			return this;
		}

	
		
		public DataCollector<T> getAll() {
			return new DataCollector<>(new GetAllGatherer<>());
		}


		/**
		 * @return The {@link DataCollector}
		 */
		public DataCollector<T> build(){

			if(!keys.isEmpty()){
				BiConsumer<Map<AKey<?>, Object>, T> attributes =
						(m,d)->{
							for(AKey<?> k : keys)
								m.put(k, d.getAttribute(k).orElse(null));
						};
						bics.add(attributes);
			}

			return new DataCollector<>(new ConfiguredGatherer<>(bics, registered));
		}

		
		
		public static interface Gatherer<T> extends BiConsumer<List<Map<AKey<?>, Object>>,T>{
			
			public Gatherer<T> duplicate();
			public List<AKey<?>> keys();
		
		}
		
		
		
		
		
		private static class GetAllGatherer<T extends DataItem> implements Gatherer<T>{

			Collection<AKey<?>> keys = Collections.emptyList();
			
			@Override
			public void accept(List<Map<AKey<?>, Object>> t, T u) {
				final HashMap<AKey<?>, Object> map = new HashMap<>();
				u.getValidAttributeKeys().forEach(k -> map.put(k, u.getAttribute(k).orElse(null)));
				t.add(map);
			}

			@Override
			public Gatherer<T> duplicate() {
				return new GetAllGatherer<>();
			}

			@Override
			public List<AKey<?>> keys() {
				return new ArrayList<>(keys);
			}
			
		}
		
		
		
		
		/**
		 * A {@link ConfiguredGatherer} is a {@link BiConsumer} used as an accumulator in a {@link DataCollector}.
		 * The {@link AKey} objects used as address of values in the maps it creates can be obtained
		 * as well as a full description of the accumulation process
		 * @author Guillaume Blin
		 *
		 */
		private static class ConfiguredGatherer<T> implements Gatherer<T>{



			private List<BiConsumer<Map<AKey<?>, Object>,? super T>> bics;
			private List<AKey<?>> keys;


			private ConfiguredGatherer(
					List<BiConsumer<Map<AKey<?>, Object>,? super T>> bics, 
					Collection<AKey<?>> keys){
				this.bics = bics;
				this.keys = new ArrayList<>(keys);
			}


			@Override
			public void accept(List<Map<AKey<?>, Object>> t, T u) {
				HashMap<AKey<?>, Object> map = new HashMap<>();
				t.add(map);
				for(BiConsumer<Map<AKey<?>, Object>,? super T> bic : bics)
					bic.accept(map, u);

			}


			public Gatherer<T> duplicate(){
				return new ConfiguredGatherer<>(bics,keys);
			}


			@Override
			public List<AKey<?>> keys() {
				return keys;
			}
			
		}
	
	
	
	
	



	/**
	 * A {@link Collector} implementation useful to collect the values of {@link DataItem}s attributes
	 * into a list of maps
	 * 
	 * @author Guillaume Blin
	 *
	 */
	public static class DataCollector<T extends DataItem>
	implements Collector<T, List<Map<AKey<?>, Object>>, List<Map<AKey<?>, Object>>>{


		private Gatherer<T> accumulator;

		DataCollector(Gatherer<T> accumulator) {
			this.accumulator = accumulator;
		}
		
		public DataCollector<T> duplicate(){
			return new DataCollector<>(accumulator.duplicate());
		}
		

		@Override
		public Supplier<List<Map<AKey<?>, Object>>> supplier() {

			return new Supplier<List<Map<AKey<?>, Object>>>(){

				@Override
				public List<Map<AKey<?>, Object>> get() {
					//return new ArrayList<>();
					return Collections.synchronizedList(new ArrayList<>());
				}

			};

		}

		@Override
		public Gatherer<T> accumulator() {
			return accumulator;
		}

		@Override
		public BinaryOperator<List<Map<AKey<?>, Object>>> combiner() {
			return (m1,m2)->{
				m1.addAll(m2);
				return m1;
			};
		}

		@Override
		public Function<List<Map<AKey<?>, Object>>, List<Map<AKey<?>, Object>>> finisher() {
			return (map)-> map;
		}

		@Override
		public Set<java.util.stream.Collector.Characteristics> characteristics() {
			return Collections.unmodifiableSet(EnumSet.of(
					Collector.Characteristics.CONCURRENT,
					//Collector.Characteristics.UNORDERED,
					Collector.Characteristics.IDENTITY_FINISH));
		}


	}



}

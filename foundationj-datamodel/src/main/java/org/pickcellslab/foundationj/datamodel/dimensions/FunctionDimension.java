package org.pickcellslab.foundationj.datamodel.dimensions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.function.Function;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "Function")
public class FunctionDimension<D extends DataItem, T> implements Dimension<D, T> {

	private final AKey<T> key;
	@Supported
	private final Function<D,T> function;
	private final String description;
	private final int index;
	private final int length;

	public FunctionDimension(AKey<T> generatedKey, Function<D, T> function, String description) {
		MappingUtils.exceptionIfNullOrInvalid(function);
		this.key = generatedKey;
		this.function = function;
		this.description = description;
		this.index = -1;
		this.length = -1;
	}

	public FunctionDimension(AKey<T> generatedKey, int index, int length, Function<D, T> function, String description) {
		MappingUtils.exceptionIfNullOrInvalid(function);
		this.key = generatedKey;
		this.function = function;
		this.description = description;
		this.index = index;
		this.length = length;
	}

	@Override
	public int index() {
		return index;
	}

	@Override
	public String name() {
		String name = index == -1 ? key.name : key.name +" ["+index+"]";
		return name;
	}



	@Override
	public dType dataType() {
		dType t = key.dType();
		return t;
	}


	@Override
	public String toString() {
		return key.name;
	}


	@Override
	public boolean equals(Object o){
		if(o instanceof FunctionDimension)
			return key == ((FunctionDimension<?,?>)o).key && index == ((Dimension<?,?>)o).index();
		else return false;
	}


	@Override
	public int hashCode(){
		return key.hashCode() + Integer.hashCode(index);
	}





	@SuppressWarnings("unchecked")
	@Override
	public T apply(D i) {
		Object r = function.apply(i);
		if(index==-1)			
			return (T) r;
		else
			if(r == null)
				return null;
		return (T) Array.get(r, index);
	}






	@SuppressWarnings("unchecked")
	@Override
	public Class<T> getReturnType() {
		// Here we may extract one value out of an array so check for this
		if(index!=-1)
			return (Class<T>) key.type().getComponentType();
		return (Class<T>) key.type();
	}



	@Override
	public String info() {
		return description;
	}

	@Override
	public int length() {
		return length;
	}



}

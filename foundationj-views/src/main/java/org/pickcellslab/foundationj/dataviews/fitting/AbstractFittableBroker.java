package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.events.FittingEvtListener;
import org.pickcellslab.foundationj.dataviews.mvc.FittableDimensionBroker;

public abstract class AbstractFittableBroker<T,R,D> extends SimpleDimensionBroker<T,D> implements FittableDimensionBroker<T,R,D> {
	
	public AbstractFittableBroker(List<Dimension<T,D>> containers) {
		super(containers);
	}


	/**
	 * The List of {@link FittingEvtListener}s
	 */
	protected final List<FittingEvtListener> fittingLstrs = new ArrayList<>();


	//Listeners management
	//-----------------------------------------------------------------------------------------------

	
	@Override
	public void addFittingListener(FittingEvtListener l) {
		fittingLstrs.add(l);
	}

	@Override
	public void removeFittingListener(FittingEvtListener l) {
		fittingLstrs.remove(l);
	}


	protected void fireFittingEvent(FittingEvent evt){
		fittingLstrs.forEach(l->l.fittingUpdated(evt));
	}


}

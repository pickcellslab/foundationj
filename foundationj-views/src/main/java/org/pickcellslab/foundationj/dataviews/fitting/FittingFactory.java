package org.pickcellslab.foundationj.dataviews.fitting;

import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.Scope;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * A {@link FittingFactory} contains {@link FittingMethod} describing the {@link Fitter} objects it can create.
 * It also declares the name of the group of fitting methods.
 * Note that in the context of an application, this factory may be provided by modules and injected in the constructor 
 * of consumer classes, see {@link InjectedSingleSerieFittingModel} for example.
 * 
 * @author Guillaume Blin
 *
 * @param <F> The type of {@link Fitter} this factory creates
 * @param <M> The type of {@link FittingMethod} available in this factory
 * @param <O> The type of the configuration options
 */
@Modular
@Scope
public interface FittingFactory<F extends Fitter<D,O,R>, D,O,R> {

	/**
	 * @return The FittingMethod associated with this Factory. NB: the returned method
	 * must always be the same instance.
	 */
	public FittingMethod<O> method();
	
	public F createFitter();
	
	public String group();
}

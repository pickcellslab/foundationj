package org.pickcellslab.foundationj.mapping.extra;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Scope;

/**
 * Defines the procedure to store and restore non data objects to and from a String form.
 * <br> The string form must be as human readable as possible to allow for the data to be 
 * usable in other contexts than the application itself.
 *  
 * @author Guillaume Blin
 * 
 * @see NonDataMapping
 *
 */
@Core
@Scope
public interface NonDataMapper {

	public void setMapping(Class<?> toMap, String mappingId); 
	
	public void ignoreField(Class<?> fromClass, String fieldName);
		
	public void denyMappingFor(Class<?> deniedClass);

	public String convertToString(Object object);

	public Object restoreFromString(String str);

	public void setMappingWithCustomConverter(Class<?> mappable, String value,
			Class<? extends CustomNonDataConverter> converter);
	
		
	
}

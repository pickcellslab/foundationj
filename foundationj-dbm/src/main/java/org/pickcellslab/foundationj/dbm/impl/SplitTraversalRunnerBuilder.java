package org.pickcellslab.foundationj.dbm.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.CategoryAdder;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.SplittedSplitChoiceNoneAllowed;

class SplitTraversalRunnerBuilder<V,E,T,K,O> implements SplittedSplitChoiceNoneAllowed<T, K, O>,
CategoryAdder<QueryRunner<OrganisedResult<Pair<K, ExplicitPredicate<? super T>>, O>>, T>{

	private final TargetType<V, E, T> target;
	private final Action<? super T, O> action;
	private final ExplicitPathSplit<V, E, K> splitter;
	private final TraversalDefinition td;
	private List<ExplicitPredicate<? super T>> filterSet;

	SplitTraversalRunnerBuilder(TargetType<V,E,T> target, Action<? super T,O> action, ExplicitPathSplit<V,E,K> splitter, TraversalDefinition td){
		this.target = target;
		this.action = action;
		this.splitter = splitter;
		this.td = td;
	}
	
	
	@Override
	public QueryRunner<OrganisedResult<Pair<K, String>, O>> furtherGroupBy(ExplicitFunction<T,String> categorical) {
		return new SplitTraversalRunner<>(
				target,
				new SplittedSplitTraversalDefinition<>(
						target,
						td,
						splitter,
						new CategoricalKeySplitter<>(categorical,"value")),
				action);
	}

	@Override
	public QueryRunner<OrganisedResult<Pair<K, Integer>, O>> furtherGroupUsing(ExplicitFunction<T,Integer> categorical) {
		return new SplitTraversalRunner<>(
				target,
				new SplittedSplitTraversalDefinition<>(
						target,
						td,
						splitter,
						new CategoricalKeySplitter<>(categorical,"value")),
				action);
	}

	
	@Override
	public QueryRunner<OrganisedResult<Pair<K, Boolean>, O>> furtherSplitInto2Groups(ExplicitPredicate<? super T> test) {
		return new SplitTraversalRunner<>(
				target,
				new SplittedSplitTraversalDefinition<>(
						target,
						td,
						this.splitter,
						new BinarySplitter<>(test)),
				action);
	}

	@Override
	public <J> QueryRunner<OrganisedResult<Pair<K, J>, O>> splitFurtherUsing(ExplicitSplit<T,J> splitter) {
		return new SplitTraversalRunner<>(
				target,
				new SplittedSplitTraversalDefinition<>(
						target,
						td,
						this.splitter,
						splitter),
				action);
	}

	@Override
	public QueryRunner<OrganisedResult<K, O>> noFurtherSplit() {
		return new SplitTraversalRunner<>(
				target,
				new SingleSplitTraversalDefinition<>(
						target,
						td,
						splitter
						),
				action);
	}
	
	
	@Override
	public CategoryAdder<QueryRunner<OrganisedResult<Pair<K, ExplicitPredicate<? super T>>, O>>, T> createFurtherCategories(
			ExplicitPredicate<? super T> filter) {
		filterSet = new ArrayList<>();
		return addCategory(filter);		
	}
	
	

	@Override
	public CategoryAdder<QueryRunner<OrganisedResult<Pair<K, ExplicitPredicate<? super T>>, O>>, T> addCategory(
			ExplicitPredicate<? super T> filter) {
		Objects.requireNonNull(filter, "The provided filter is null");
		filterSet.add(filter);
		return this;
	}

	@Override
	public QueryRunner<OrganisedResult<Pair<K, ExplicitPredicate<? super T>>, O>> lastCategory(
			ExplicitPredicate<? super T> filter) {
		return new SplitTraversalRunner<>(
				target,
				new SplittedSplitTraversalDefinition<>(
						target,
						td,
						splitter,
						new FilterSetSplitter<>(filterSet)
						),
				action);
	}

}

package org.pickcellslab.foundationj.datamodel.predicates;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "Check")
@Scope
public class ValuePredicate<T,E> implements ExplicitPredicate<T> {

	@Supported
	private final ExplicitOperator<? super T,? super E> o;
	@Supported
	private final E value;
	
	public ValuePredicate(ExplicitOperator<? super T,? super E> o, E value) {
		MappingUtils.exceptionIfNullOrInvalid(o);
		MappingUtils.exceptionIfNullOrInvalid(value);
		this.o = o;
		this.value = value;
	}
	
	@Override
	public boolean test(T t) {
		return o.apply(t,value);
	}

	@Override
	public String description() {
		return o+" "+value;
	}
	
	@Override
	public String toString() {
		return o+" "+value;
	}

}

package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShortestPath implements Callable<Optional<LinkedList<Link>>> {

	private static Logger log = LoggerFactory.getLogger(ShortestPath.class);


	private TraverserConstraints constraints;

	private final Deque<NodeItem> queue = new LinkedList<>();

	private final Set<NodeItem> visitedNodes = new LinkedHashSet<>();  
	private final Set<Link> visitedLinks = new LinkedHashSet<>();

	private final Map<NodeItem, Link> predecessors = new HashMap<>();


	private int nextDepthAt = 1;
	private int nextDepthFinder = 0;
	private int currentDepth = 0;


	private final NodeItem target;


	public ShortestPath(NodeItem source, NodeItem target, TraverserConstraints constraints){      
		Objects.requireNonNull(constraints);
		this.constraints = constraints;
		this.target = target;
		visitedNodes.add(source); 
		queue.add(source);   
	}    




	/**
	 * Computes and return the shortest path
	 * @return An {@link Optional} holding the shortest path between the source and target node
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Optional<LinkedList<Link>> call() {


		while (!queue.isEmpty()) {

			NodeItem node = queue.remove();

			//log.debug("Checking "+node.toString());


			if(handleItem(node)){

				Link out = null;
				Link in = null;

				while ((out = getUnvisitedOutgoingLink(node)) != null
						|| (in = getUnvisitedIncomingLink(node)) != null) {
					if (out != null) {
						queue.add(out.target());
						predecessors.put(out.target(), out);
						nextDepthFinder++;
					}
					if (in != null) {
						predecessors.put(in.source(), in);
						queue.add(in.source());
						nextDepthFinder++;
					}
				}
			};

			//Monitoring depth
			nextDepthAt--;
			if(nextDepthAt == 0){
				nextDepthAt = nextDepthFinder;
				nextDepthFinder = 0;
				currentDepth++;
				if(!constraints.accept(currentDepth-1))
					return Optional.ofNullable(backtrack());

			}
		}

		return Optional.ofNullable(backtrack());
	}





	/**
	 * Return true if we should continue to screen the edges from this node
	 */
	private boolean handleItem(NodeItem node) {      

		switch(constraints.decide(node)){
		case EXCLUDE_AND_CONTINUE : 
			visitedNodes.add(node); 
			return true;
		case EXCLUDE_AND_STOP : 
			visitedNodes.add(node); 
			return false;
		case INCLUDE_AND_CONTINUE : 
			visitedNodes.add(node); 
			return true;
		case INCLUDE_AND_STOP : 
			visitedNodes.add(node); 
			return false;         
		}
		System.out.println("Default case in Traversers handle Item");
		return true;
	}




	private Link getUnvisitedOutgoingLink(NodeItem node) {
		return node.getLinks(Direction.OUTGOING).filter(out->{
			if(visitedLinks.add(out))
				//Test if link passes the constraints
				if(constraints.accept(out, Direction.OUTGOING))
					//Test if target is not already visited
					if(visitedNodes.add(out.target()))
						return true;			
			return false;
		}).findFirst().orElse(null);
	}

	private Link getUnvisitedIncomingLink(NodeItem node) {

		return node.getLinks(Direction.INCOMING).filter(in->{
			if(visitedLinks.add(in))
				//Test if link passes the constraints
				if(constraints.accept(in, Direction.INCOMING))
					//Test if source is not already visited
					if(visitedNodes.add(in.source()))
						return true;
				
			return false;
		}).findFirst().orElse(null);
	}




	private LinkedList<Link> backtrack(){

		log.debug("Starting backtracking");

		NodeItem current = target;
		Link l = predecessors.get(target);
		if(null == l){
			log.debug("The target was not found in the graph using pointer");
			log.debug("Attempting to find using equals method");
			//Try to find an instance which is equal to the target
			for(NodeItem i : predecessors.keySet())
				if(i.equals(target)){
					current = i;					
					break;
				}
			l = predecessors.get(current);
			if(null == l){
				log.debug("Finding the target failed");	
				return null;				
			}
		}


		LinkedList<Link> path = new LinkedList<>();
		while (null != l){
			path.addFirst(l);
			if(l.target() == current)
				current = l.source();
			else
				current = l.target();

			l = predecessors.get(current); 
		}
		return path;

	}





}

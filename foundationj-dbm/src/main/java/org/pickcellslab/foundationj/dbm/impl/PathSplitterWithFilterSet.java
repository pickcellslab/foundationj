package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="MultiPredicatePathGrouping")
class PathSplitterWithFilterSet<V,E> implements ExplicitPathSplit<V,E, ExplicitPredicate<? super NodeItem>> {

	//private final Logger log = Logger.getLogger(PathSplitterWithFilterSet.class);
	
	@Supported
	private final List<ExplicitPredicate<? super NodeItem>> nodeSplit;

	@Ignored
	private FjAdaptersFactory<V, E> f;



	PathSplitterWithFilterSet(List<ExplicitPredicate<? super NodeItem>> nodeSplit){
		Objects.requireNonNull(nodeSplit, "the list of filters is null");
		if(nodeSplit.isEmpty())
			throw new IllegalArgumentException("The list of filters is empty");
		
		nodeSplit.forEach(p->MappingUtils.exceptionIfNullOrInvalid(p));
		this.nodeSplit = nodeSplit;
		
	}


	@Override
	public void setFactory(FjAdaptersFactory<V,E> factory){
		this.f = factory;
	}


	@Override
	public String description() {
		return "group path using multiple predicates";
	}

	
	@Override
	public <I> Collection<I> accumulator(){
		return new HashSet<>();
	}


	@Override
	public ExplicitPredicate<? super NodeItem> getKeyFor(Path<V, E> p) {
		
		
		for(ExplicitPredicate<? super NodeItem> test : nodeSplit){
			Iterator<V> ns = p.nodes();
			while(ns.hasNext())
				if(test.test(f.getNodeReadOnly(ns.next())))
						return(test);		
		}
		return P.none();
	}





}

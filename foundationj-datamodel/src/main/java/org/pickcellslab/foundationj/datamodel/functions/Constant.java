package org.pickcellslab.foundationj.datamodel.functions;

import java.lang.reflect.Array;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 *A {@link ExplicitFunction} always returning the object passed in the constructor
 *
 * @param <R>
 * @author Guillaume Blin
 */
@Mapping(id = "Constant")
@Scope
public class Constant<T, R> implements ExplicitFunction<T,R> {


	@Supported
	private final R constant;


	public Constant(final R constant){
		MappingUtils.exceptionIfInvalid(constant);
		this.constant = constant;
	}

	@Override
	public R apply(T t) {
		return constant;
	}

	@Override
	public String description() {
		if (constant == null)
			return "null";
		if(constant.getClass().isArray()) {			
			int iMax = Array.getLength(constant) - 1;
			if (iMax == -1)
				return "[]";
			StringBuilder b = new StringBuilder();
			b.append('[');
			for (int i = 0; ; i++) {
				b.append(Array.get(constant,i));
				if (i == iMax)
					return b.append(']').toString();
				b.append(", ");
			}
		}
		else
			return constant.toString();
	}

	@Override
	public String toString() {
		return description();
	}


	R getConstant(){
		return constant;
	}

	/*
	@Override
	public Operator<?,?>[] getOperatorCompatibility() {
		if(constant.getClass().isArray())
			return Op.ElementOp.values();
		else if(Number.class.isAssignableFrom(constant.getClass()))
			return Op.Logical.values();
		else if(constant.getClass() == String.class)
			return Op.TextOp.values();
		else if(constant.getClass() == Boolean.class || constant.getClass() == boolean.class)
			return Op.Bool.values();
		return new Operator[0];
	}
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Class<R> getReturnType() {
		return (Class<R>) constant.getClass();
	}





}

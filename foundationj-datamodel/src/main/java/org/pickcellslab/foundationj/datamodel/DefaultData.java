package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;


public abstract class DefaultData implements WritableDataItem {



	protected Map<AKey<?>, Object> attributes = new ConcurrentHashMap<>(); 

	@Override
	public String declaredType(){
		return typeId();
	}
	
	
	@Override
	public Stream<AKey<?>> getValidAttributeKeys() {
		return attributes.keySet().stream();
	}

	@Override
	public <T> void setAttribute( AKey<T> key, T v) {
		Objects.requireNonNull(v);
		attributes.put(key, v);
	}

	
	@SuppressWarnings({ "unchecked"})
	@Override
	public <T> Optional<T> getAttribute(AKey<T> key) {
		return Optional.ofNullable((T) attributes.get(key));
	}

	@Override
	public void removeAttribute(AKey<?> key) {
		attributes.remove(key);
	}

	@Override
	public abstract String toString();

}

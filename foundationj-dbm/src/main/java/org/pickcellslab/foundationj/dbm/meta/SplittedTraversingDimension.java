package org.pickcellslab.foundationj.dbm.meta;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "SplittedGraphNavigatingFunction")
@WithService(DataAccess.class)
class SplittedTraversingDimension<T> implements CachingDimension<T>{

	@Supported
	private final ExplicitSplit<DataItem, ?> split;
	private final String[] groups;


	private final boolean targetLinks;		
	private final TraversalDefinition traversalDescription;
	@Supported
	private final ExplicitReduction<DataItem, ?> reductionOperation ;
	@Supported
	private final ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> traversalToIteration;


	private final AKey<?> generatedKey;
	private final int lengthOfReturnedValue;

	private final String name, description;

	// Caching ----------------------
	@Ignored
	private Map<String,Object> cache;
	@Ignored
	private TraverserConstraints tc;

	@Ignored
	private Class returnedType;


	SplittedTraversingDimension(
			String name, String description,
			TraversalDefinition td, boolean targetLinks,
			MetaReductionOperation fctry,
			ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> traversalToIterator,
			PredictableMetaSplit split) {

		this.traversalDescription = td;
		this.targetLinks = targetLinks;
		this.reductionOperation = fctry.toReductionOperation();
		this.name = name;
		this.description = description;
		this.traversalToIteration = traversalToIterator;
		this.lengthOfReturnedValue = fctry.lengthOfReturnedValue();
		this.generatedKey = fctry.generatedKey();

		this.split = split.toSplitter();
		MappingUtils.exceptionIfNullOrInvalid(this.split);
		this.groups = split.getPrediction();
	}



	@Override
	public dType dataType() {
		return generatedKey.dType();
	}

	@Override
	public Class getReturnType() {
		if(returnedType==null)
			returnedType = reductionOperation.getReturnType().isArray() ? 
					reductionOperation.getReturnType() : 
						Array.newInstance(reductionOperation.getReturnType(), 0).getClass();
					return returnedType;
	}


	
	@Override
	public Object apply(DataItem t) {	


		if(null==cache)
			cache = new HashMap<>();

		Object result = cache.get(t.toString());
		//System.out.println(t.toString());
		if(result!=null)
			return result;

		if(!(t instanceof NodeItem))
			return null;			

		if(tc==null)
			tc=traversalDescription.asConstraints(targetLinks);


		final Map<String, ExplicitReduction<DataItem,?>> map = new HashMap<>(groups.length);

		for(int i = 0; i<groups.length; i++)
			map.put(groups[i], reductionOperation.factor());

		traversalToIteration.apply(Traversers.breadthfirst((NodeItem)t,tc).traverse())
		.forEachRemaining(i->{
			map.get(split.getKeyFor(i)).include(i);
		});

		final int length = numDimensions();
		result = Array.newInstance(getReturnType().getComponentType(), length);

		int c = 0;
		for(int i = 0; i<groups.length; i++){
			ExplicitReduction<DataItem,?> mecha = map.get(groups[i]);
			Object r = mecha.getResult();
			if(r.getClass().isArray()) {
				for(int j = 0; j<Array.getLength(r); j++)
					Array.set(result, c++, Array.get(r, j));
			}
			else
				Array.set(result, c++, r);
		}

		cache.put(t.toString(), result);

		return result;
	}


	@Override
	public int index() {
		return -1;
	}

	@Override
	public int length() {
		return numDimensions();
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String getName(int index) {
		// Order of dimensions --> Group 1 (mechanism dims) / Group 2 (mechanism dims) / ...
		String gr = groups[(index/lengthOfReturnedValue) % groups.length];
		//System.out.println("groups "+gr);
		String k = name+"["+index % lengthOfReturnedValue+"]";
		return k+"_"+gr;
	}

	@Override
	public String info() {
		return description();
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public String description(int index) {
		if(lengthOfReturnedValue == 1)
			return description;
		return description+" "+index;
	};

	@Override
	public String toString(){
		return name()+" (Function)";
	}

	@Override
	public Dimension<DataItem, ?> getRaw() {
		return this;
	}

	@Override
	public int numDimensions() {
		return lengthOfReturnedValue * groups.length;
	}

	@Override
	public Dimension<DataItem, T> getDimension(int index) throws ArrayIndexOutOfBoundsException {

		int nd = numDimensions();
		if(nd <= index)
			throw new ArrayIndexOutOfBoundsException("Provided Index "+index+" ; num Dims : "+nd);

		return new CachedDimension<>(this, index);

	}



}
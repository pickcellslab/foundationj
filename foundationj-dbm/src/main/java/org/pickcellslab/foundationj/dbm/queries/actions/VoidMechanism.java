package org.pickcellslab.foundationj.dbm.queries.actions;

/**
 * An {@link ActionMechanism} that produces no result
 *
 * @param <I>
 */
@FunctionalInterface
public interface VoidMechanism<I> extends ActionMechanism<I,Void> {

	@Override
	public default Void get(){
		return null;
	}
	
}

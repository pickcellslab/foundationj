package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.dataviews.events.DataEvent;
import org.pickcellslab.foundationj.dataviews.events.DataSetChangedListener;

/**
 * An abstract {@link DataViewModel} which handles {@link DataSetChangedListener} registrations and DataSet updates
 * Notice also the {@link #fireDataEvt(DataEvent)} which dispatch the event to all the registered 
 * listeners
 * 
 * @author Guillaume Blin
 *
 * @param <D> The type of {@link DataSet} accepted by this Model.
 * @param <B> The type of parameter required by the {@link ViewActionFactory} 
 */
public abstract class AbstractModel<T> implements DataViewModel<T> {

	/**
	 * The list of {@link DataEvtListeners}
	 */
	protected final List<DataSetChangedListener<T>> dataEvtLstrs = new ArrayList<>();


	protected DataSet<T> dataset;

		
	@Override
	public DataSet<T> getDataSet(){
		return dataset;
	}

	@Override
	public void setDataSet(DataSet<T> ds){		
		DataSet<T> oldDataSet = dataset;
		dataset = ds;
		dataSetHasChanged();
		this.fireDataEvt(oldDataSet, dataset);
	}
		

	protected abstract void dataSetHasChanged();
	

	@Override
	public void addDataEvtListener(DataSetChangedListener<T> lstr) {
		dataEvtLstrs.add(lstr);
	}

	@Override
	public void removeDataEvtListener(DataSetChangedListener<T> lstr) {
		dataEvtLstrs.remove(lstr);
	}


	/**
	 * Dispatch the event to all registered listeners
	 * @param evt
	 */
	protected void fireDataEvt(DataSet<T> old, DataSet<T> newDataSet){
		dataEvtLstrs.forEach(l->l.dataSetChanged(old, newDataSet));
	};

}

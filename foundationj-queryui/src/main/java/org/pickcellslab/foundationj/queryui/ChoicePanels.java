package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Wizard;
import org.pickcellslab.foundationj.ui.wizard.WizardFactory;


/**
 * A Factory for {@link ChoicePanel}s
 * 
 * @author Guillaume Blin
 *
 */
public final class ChoicePanels{

	
	
	public static ChoicePanel<MetaQueryable> allQueryable(UITheme theme, DataAccess access, String title) throws Exception{		
		return new ChoicePanel<>(theme, access, MetaQueryable.class, null, null, null, title);
	}


	public static ChoicePanel<MetaQueryable> queryables(UITheme theme, DataAccess access, Predicate<? super MetaQueryable> p, String title) throws Exception{
		return new ChoicePanel<>(theme, access, MetaQueryable.class, null, p, null, title);
	}

	
	
	public static ChoicePanel<MetaFilter> allFilters(UITheme theme, DataAccess access, String title, MetaQueryable container) throws Exception{
		
		Objects.requireNonNull(container, "The MetaKeyContainer is null");		
				
		List<MetaFilter> list = container.filters().collect(Collectors.toList());
				
		return new ChoicePanel<>(theme, access, MetaFilter.class, list, 
				(t)->t.getTarget()==container,
				customFilter(theme, access, container),
				title);
	}
	
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ChoicePanel<MetaReadable<?>> allReadables(UITheme theme, DataAccess access, String title, MetaQueryable container) throws Exception{
		
		Objects.requireNonNull(container, "The MetaKeyContainer is null");		
				
		List<MetaReadable<?>> list = container.getReadables().collect(Collectors.toList());		
		
		return new ChoicePanel(theme, access, MetaReadable.class, list, 
				(t)->((MetaReadable<?>) t).owner() == container,
				customFeatureFactory(theme, access, ()->container), title);
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static ChoicePanel<MetaReadable<?>> readables(UITheme theme, DataAccess access, String title, MetaQueryable container, Predicate<? super MetaReadable> p) throws Exception{
		
		Objects.requireNonNull(container, "The MetaKeyContainer is null");			
		
		List<MetaReadable<?>> list = container.getReadables().filter(p).collect(Collectors.toList());		
		
		return new ChoicePanel(theme, access, MetaReadable.class, list, p, customFeatureFactory(theme, access, ()->container), title);
	}
	
	
	
	public static WizardFactory<MetaReadable<?>> customFeatureFactory(UITheme theme, DataAccess access, Supplier<MetaQueryable> supplier){
		return ()-> new Wizard<>(new AccumulatorChoice(null, theme, access, supplier));
	}
	
	
	public static WizardFactory<MetaReadable<?>> customFeatureFactory(UITheme theme, DataAccess access, ChoicePanel<MetaQueryable> panel){
		return ()-> new Wizard<>(new AccumulatorChoice(null, theme, access, () -> panel.getSelected().orElse(null)));
	}
	
	
	public static WizardFactory<MetaFilter> customFilter(UITheme theme, DataAccess access, MetaQueryable mq){
		return ()-> new Wizard<>(new FilterChoiceUI(null, mq, theme, access));
	}
	
	
	
	
}

package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

/**
 * A wrapper class holding the result of a Traversal performed by a {@link Traverser}
 * @author Guillaume
 *
 */
public class DefaultTraversal<N,E> implements Traversal<N,E> {

  /**
   * The set of nodes in order
   */
  public Set<N> nodes;
  /**
   * The set of edges (Links) in order
   */
  public Set<E> edges;

  DefaultTraversal(Set<N> nodes, Set<E> edges){
    this.nodes = nodes;
    this.edges = edges;
  }

@Override
public Set<N> nodes() {
	return nodes;
}

@Override
public Set<E> edges() {
	return edges;
}
  
  
  
}

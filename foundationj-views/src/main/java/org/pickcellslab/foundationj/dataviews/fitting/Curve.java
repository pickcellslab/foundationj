package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;

public interface Curve extends NumericFit{

	/**
	 * @return The dimensions this curve is defined for
	 */
	AKey<? extends Number>[] dimensions();
	
	/**
	 * Specifies the new coordinate for a specific dimension. By default, the current coordinate is
	 * the origin.
	 * @param axis The dimension to specify the coordinate for
	 * @param value The desired coordinate
	 */
	public <E extends Number> void setCoord(AKey<E> axis, E value);
	
	public double[] currentCoordinates();
	
	double value();
	
}

package org.pickcellslab.foundationj.bootstrap;

import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.ScopePolicy;

interface AtScope {

	String scope();

	String parent();

	ScopePolicy policy();
	
	ScopeImpl createAndRegister(String id, String parentId);
	
	ScopeImpl remove(String id);
	
	Stream<ScopeImpl> active();

	Optional<ScopeImpl> activeWithId(String id);
}
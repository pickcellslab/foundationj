package org.pickcellslab.foundationj.mapping.extra;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.foundationj.mapping.exceptions.UnsupportedMappingException;


public class MappingUtilsTests {

	
	
	@Test
	public void testSupportedClass() {
		assertTrue(MappingUtils.canBeMapped(MappableObject.class));
	}
	
	
	@Test
	public void testClass() {
		assertFalse(MappingUtils.canBeMapped(TestInterface.class));
	}
	
	
	@Test
	public void testPrimitive() {
		final int one = 1;
		final float two = 2f;
		final double three = 3d;
		assertTrue(MappingUtils.canBeMapped(one));
		assertTrue(MappingUtils.canBeMapped(two));
		assertTrue(MappingUtils.canBeMapped(three));
	}
	
	@Test
	public void testPrimitiveArray() {
		assertTrue(MappingUtils.canBeMapped(new int[] {1,2,3}));
		assertTrue(MappingUtils.canBeMapped(new float[] {1,2,3}));
		assertTrue(MappingUtils.canBeMapped(new double[] {1,2,3}));
	}
	
	@Test
	public void testString() {
		assertTrue(MappingUtils.canBeMapped(""));
	}
	
	@Test
	public void testStringArray() {
		assertTrue(MappingUtils.canBeMapped(new String[]{"", ""}));
	}
	
	@Test
	public void testBoxed() {
		assertTrue(MappingUtils.canBeMapped(new Float(0)));
		assertTrue(MappingUtils.canBeMapped(new Double(0)));
	}
	
	@Test
	public void testBoxedArray() {
		assertTrue(MappingUtils.canBeMapped(new Float[] {0f,2f}));
		assertTrue(MappingUtils.canBeMapped(new Double[]{}));
	}
	
	@Test
	public void testSimpleEnum() {
		assertTrue(MappingUtils.canBeMapped(SimpleEnum.E1));
	}
	
	@Test
	public void testComplexEnum() {
		assertTrue(MappingUtils.canBeMapped(ComplexEnum.C1));
	}
	
	
	@Test
	public void testEmptyCollection() {
		assertTrue(MappingUtils.canBeMapped(Collections.emptyList()));
		assertTrue(MappingUtils.canBeMapped(Collections.emptySet()));
	}
	
	@Test
	public void testNonEmptyCollection() {
		final List<String> list = new ArrayList<>();
		list.add("");
		assertTrue(MappingUtils.canBeMapped(list));
	}
	
	@Test
	public void testEmptyMap() {
		assertTrue(MappingUtils.canBeMapped(Collections.emptyMap()));
	}
	
	@Test
	public void testNonEmptyMap() {
		final Map<String,double[]> map = new HashMap<>();
		map.put("", new double[] {0,1});
		assertTrue(MappingUtils.canBeMapped(map));
	}
	
	
	@Test
	public void testMappingAnnotatedObject() {
		assertTrue(MappingUtils.canBeMapped(new MappableObject()));
	}
	
	
	@Test
	public void testUnsupported() {
		assertFalse(MappingUtils.canBeMapped(getClass()));
	}
	
	@Test
	public void testUnsupportedCollection() {
		assertFalse(MappingUtils.canBeMapped(Collections.singletonList(getClass())));
	}
	
	@Test
	public void testUnsupportedMap() {
		final Map<String,Object> map = new HashMap<>();
		map.put("", getClass());
		assertFalse(MappingUtils.canBeMapped(map));
	}
	
	
	@Test
	public void testNull() {
		assertThrows(NullPointerException.class, ()->MappingUtils.canBeMapped(null));
	}
	
	
	@Test
	public void testThrows() {
		assertThrows(NullPointerException.class, ()->MappingUtils.exceptionIfInvalid(null));
		assertThrows(NullPointerException.class, ()->MappingUtils.exceptionIfNullOrInvalid(null));
		assertThrows(UnsupportedMappingException.class, ()->MappingUtils.exceptionIfInvalid(getClass()));
	}
	
	
	
	
	
	@Mapping(id = "Mapping Test")
	@Scope(name="Test", parent="", policy=ScopePolicy.SINGLETON)
	private class MappableObject{}
	
	private enum SimpleEnum{E1,E2,E3}
	
	private interface TestInterface{}
	
	private enum ComplexEnum implements TestInterface{C1,C2,C3}
	
	
	
}

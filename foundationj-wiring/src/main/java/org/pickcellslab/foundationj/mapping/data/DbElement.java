package org.pickcellslab.foundationj.mapping.data;

import org.pickcellslab.foundationj.annotations.Data;

/**
 * <u>Database entities</u> are expected to implement this interface in order to allow the framework to map the entity back to 
 * its application level java object.
 * 
 * @see {@link Data}
 * 
 * @author Guillaume Blin
 * 
 */
public interface DbElement {
	
	/** 	 
	 * @return  The typeId which matches the {@link Data#typeId()} of the data object it represents.
	 */
	public String typeId();
	
	
	/**
	 * @param otherType Either the typeId of a {@link DbElement} or a String defined in a {@link Tag} from an interface
	 * @return {@code true} if this  {@link DbElement} possesses the given tag
	 */
	public boolean isTagged(String tag);
}

package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.events.Modif;
import org.pickcellslab.foundationj.dataviews.fitting.AbstractSelectableFittableBroker;
import org.pickcellslab.foundationj.dataviews.fitting.DefaultFitResult;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.DistributionFitter;
import org.pickcellslab.foundationj.dataviews.fitting.DistributionFitterFactory;
import org.pickcellslab.foundationj.dataviews.fitting.FitConfig;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.fitting.FittingMethod;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.dbm.queries.actions.AbstractAction;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public final class ScalarDimensionBroker<T,D> extends AbstractSelectableFittableBroker<T,D,Number> implements FittableDimensionBroker<T,D,Number>{



	private final List<DistributionFitterFactory<?,D,?,?>> factories;

	private final Map<String, FitResult<? extends Distribution<D>>> distributions = new HashMap<>();

	private final DataSet<T> dataset;




	public <F extends DistributionFitterFactory<?,D,?,?>> ScalarDimensionBroker(List<Dimension<T,Number>> containers, DataSet<T> meta, List<DistributionFitterFactory<?,D,?,?>> fitters) {
		super(containers);		
		Objects.requireNonNull(meta,"Meta is null");
		Objects.requireNonNull(fitters, "fitter definition is null");
		this.dataset = meta;
		this.factories = fitters;
	}




	@Override
	public int numFittableDimensions() {
		int count = 0;
		for(int i = 0; i<numDimensions(); i++)
			if(dimension(i).dataType() == dType.NUMERIC)
				count++;
		return count;
	}

	
	@Override
	public Dimension<T,Number> fittableDimension(int i) {
		int count = 0;
		for(int d = 0; d<numDimensions(); d++){
			Dimension<T,Number> dim = dimension(d);
			if(dim.dataType() == dType.NUMERIC)
				if(count++ == i)
					return dim;		
		}
		throw new IndexOutOfBoundsException("Index : "+i);
	}


	@Override
	public FittingMethod<?> getFittingMethod(int i) {
		return factories.get(i).method();
	}


	@Override
	public int numFittingMethods() {
		return factories.size();
	}


	@Override
	public void fitRequest(String query, int method, int dim, int... dims) throws DataAccessException {

		Optional<?> config = factories.get(method).method().config();
		if(!config.isPresent())
			return;

		//Get the functions which will fetch the values in the database
		Dimension<T,Number>[] ds = new Dimension[dims.length+1];
		ds[0] = fittableDimension(dim);
		for(int i = 0; i<dims.length; i++){
			ds[i+1] = fittableDimension(dims[i]);
			if(ds[i+1] == null)
				System.out.println("ScalarDimensionBroker: null dim identified!");
		}

		ResultAccess<String,List<D>> result = dataset.loadQuery(dataset.queryIndex(query), getAction(ds));
		 DistributionFitter<D, ?, ?> fitter = factories.get(method).createFitter();

		//TODO parallel
		for(int r = 0; r<result.size(); r++){
			
			String fitId = "Fit : "+result.getKey(r);
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			FitResult<? extends Distribution<D>> fr =  new DefaultFitResult(
					fitter.fit(result.getResult(r), (FitConfig) config.get()),
					fitter.lastFitInfos(),
					fitId,
					ds
					);

			distributions.put(fitId, fr);

			this.fireFittingEvent(new FittingEvent(this, Modif.ADDED, fitId,r));
		}


	}


	@Override
	public void deleteFit(String prefix) {

		for(String s : new ArrayList<>(distributions.keySet())){
			if(s.startsWith(prefix)){
				distributions.remove(s);
				this.fireFittingEvent(new FittingEvent(this,Modif.DELETED,s,0));
			}
		}

	}



	@Override
	public FitResult<? extends Distribution<D>> getDistributionFit(String id) {
		return distributions.get(id);
	}


	
	private Action<T,List<D>> getAction(Dimension<T,Number>[] ds){
		if(ds.length == 1)
			return (Action)new UnivariateAction(ds[0]);
		else
			return (Action) new MultiVariateAction(ds);
	}


	private class UnivariateAction extends AbstractAction<T,List<Double>>{

		private final Function<T, Number> functions;

		UnivariateAction(Function<T, Number> functions){
			super("Obtain data for fitting");
			this.functions = functions;
		}

		@Override
		protected ActionMechanism<T, List<Double>> create(String key) {
			return new UnivariateMechanism(functions);
		}

		

	}

	private class UnivariateMechanism implements ActionMechanism<T,List<Double>>{

		private final Function<T, Number> functions;
		private final List<Double> data = new ArrayList<>();

		UnivariateMechanism(Function<T,Number> functions){
			this.functions = functions;
		}

		@Override
		public void performAction(T t) throws DataAccessException {

			Number n = functions.apply(t);
			if(null == n)
				return;
			data.add( n.doubleValue());

		}

		public List<Double> get(){
			return data;
		}
	}
	
	

	private class MultiVariateAction extends AbstractAction<T,List<double[]>>{

		private final Map<Object,MultiVariateMechanism> mechanisms = new HashMap<>();
		private final Function<T, Number>[] functions;

		MultiVariateAction(Function<T, Number>[] functions){
			super("Obtain data for fitting");
			this.functions = functions;
		}

		@Override
		protected ActionMechanism<T, List<double[]>> create(String key) {
			return new MultiVariateMechanism(functions);
		}

	}

	private class MultiVariateMechanism implements ActionMechanism<T,List<double[]>>{

		private final Function<T, Number>[] functions;
		private final List<double[]> data = new ArrayList<>();

		MultiVariateMechanism(Function<T,Number>[] functions){
			this.functions = functions;
		}

		@Override
		public void performAction(T t) throws DataAccessException {
			double[] coords = new double[functions.length];
			for(int i = 0; i<functions.length; i++){
				Number n = functions[i].apply(t);
				if(null == n)
					return;
				coords[i] = n.doubleValue();
			}
			data.add(coords);
		}

		@Override
		public List<double[]> get(){
			return data;
		}
	}
	

}

package org.pickcellslab.foundationj.dbm.access;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.data.DbElement;
import org.pickcellslab.foundationj.mapping.data.InstantiationListener;
import org.pickcellslab.foundationj.mapping.data.MultitonDirector;
import org.pickcellslab.foundationj.mapping.data.MultitonOutcome;
import org.pickcellslab.foundationj.mapping.data.NotifyingInstantiationHook;
import org.pickcellslab.foundationj.mapping.exceptions.DataTypeException;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Multiton Director for live instances of the MetaModel within a given {@link DataAccess} instance.
 * 
 * @author Guillaume Blin
 *
 */
public final class MetaInstanceDirector implements MultitonDirector<MetaItem>, NotifyingInstantiationHook<MetaItem>, MetaModelListener{


	private static final Logger log = LoggerFactory.getLogger(MetaInstanceDirector.class);
	
	protected final DataRegistry registry;
	protected final List<InstantiationListener<? super MetaItem>> instantiationListeners = new ArrayList<>();
	
	private final Map<String, MetaItem> instances = new HashMap<>();


	/**
	 * Public only for testing purpose, do not instantiate out of tests
	 * @param registry
	 */
	public MetaInstanceDirector(DataRegistry registry) {
		Objects.requireNonNull(registry, "The provided DataRegistry cannot be null");
		this.registry = registry;
	}



	@Override
	public MetaItem getOrCreate(DataRegistry registry, DbElement dbObject) throws InstantiationHookException {
		return getOrCreate((Class<? extends MetaItem>)registry.classOf(dbObject), ((DataItem) dbObject).getAttribute(MetaModel.recognition).get());
	}



	@Override
	public MultitonOutcome<MetaItem> getOrCreateWithOutcome(DataRegistry registry, DbElement dbObject) throws InstantiationHookException {
		return (MultitonOutcome<MetaItem>) getOrCreateWithOutcome((Class<? extends MetaItem>)registry.classOf(dbObject), ((DataItem) dbObject).getAttribute(MetaModel.recognition).get());
	}


	//TODO annotation checking call is of type MetaModel
	@Override
	public <E extends MetaItem> E getOrCreate(Class<E> clazz, String uid) throws InstantiationHookException {
		synchronized(instances) {
			E m = (E) instances.get(uid);
			if(m==null)
				m = construct(clazz,uid);
			return m;
		}
	}


	@Override
	public <E extends MetaItem> MultitonOutcome<E> getOrCreateWithOutcome(Class<E> c, String uid) throws InstantiationHookException {
		synchronized(instances) {
			E m = (E) instances.get(uid);
			if(m==null)
				return new MultitonOutcome<>(construct(c,uid), true);
			else
				return new MultitonOutcome<>(m, false);
		}
	}


	private <E extends MetaItem> E construct(Class<E> c, String uid) throws InstantiationHookException {

		Constructor<E> constr;
		try {

			constr = c.getDeclaredConstructor(String.class);			
			constr.setAccessible(true);	
			final E m = constr.newInstance(uid);
			instances.put(uid, m);
			log.debug(c.getSimpleName()+" created with uid "+uid);
			instantiationListeners.forEach(l->l.instanceWasCreated(m));			
			return m;

		} catch (Exception e) {
			throw new InstantiationHookException(getClass(), c, e);
		}

	}



	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {
		// Listen only for deletions
		if(evt == MetaChange.DELETED) {
			synchronized(instances){
				meta.getTargets(MetaItem.class).forEach(m->{
					final MetaItem metaItem = instances.remove(m.recognitionString());
					if(metaItem!=null) {
						metaItem.links().collect(Collectors.toList()).forEach(link->link.delete());
					}
				});
			}
		}

	}


	public DataRegistry registry() {
		return registry;
	}



	@Override
	public Object getDataInstance(DbElement dbObject) throws DataTypeException, InstantiationHookException {
		return getOrCreate(registry, dbObject);
	}



	@Override
	public void addInstantiationListener(InstantiationListener<? super MetaItem> l) {
		if(!instantiationListeners.contains(l))
			instantiationListeners.add(l);		
	}


}

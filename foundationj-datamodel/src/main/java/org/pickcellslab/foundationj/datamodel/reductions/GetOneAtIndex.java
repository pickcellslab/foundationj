package org.pickcellslab.foundationj.datamodel.reductions;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="GetOneAtIndex")
public class GetOneAtIndex<T,R> implements ExplicitReduction<T,R>{

	@Supported
	private final ExplicitFunction<T,R[]> getter;
	private final int index;
	@Ignored
	private R value;
	
	public GetOneAtIndex(ExplicitFunction<T,R[]> getter, int index) {
		MappingUtils.exceptionIfNullOrInvalid(getter);
		if(!getter.getReturnType().isArray())
			throw new IllegalArgumentException("The provided DataFunction does not return arrays");
		if(index<0)
			throw new IllegalArgumentException("Negative index provided");
		this.getter = getter;
		this.index = index;
	}
	
	@Override
	public ExplicitReduction<T, R> factor() {
		return new GetOneAtIndex<>(getter, index);
	}

	@Override
	public void root(T root, boolean included) {
		if(included) {
			R[] arr = getter.apply(root);
			if(arr!=null && arr.length>index)
				value = arr[index];
		}
		
	}

	@Override
	public void include(T t) {
		if(value == null) {
			R[] arr = getter.apply(t);
			if(arr!=null && arr.length>index)
				value = arr[index];
		}
	}

	@Override
	public R getResult() {
		return value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<R> getReturnType() {
		return (Class<R>) getter.getReturnType().getComponentType();
	}

	@Override
	public String description() {
		return getter.toString()+" ["+index+"]";
	}

}

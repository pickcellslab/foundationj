package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.pickcellslab.foundationj.datamodel.tools.Decision.EXCLUDE_AND_CONTINUE;
import static org.pickcellslab.foundationj.datamodel.tools.Decision.EXCLUDE_AND_STOP;
import static org.pickcellslab.foundationj.datamodel.tools.Decision.INCLUDE_AND_CONTINUE;
import static org.pickcellslab.foundationj.datamodel.tools.Decision.INCLUDE_AND_STOP;

import java.util.Objects;

public final class Decisions {

	
	private final Decision ifYes;
	private final Decision ifNot;
	
	public Decisions(Decision ifYes, Decision ifNot){
		Objects.requireNonNull(ifYes,"if Yes decision is null");
		Objects.requireNonNull(ifNot,"if Not decision is null");
		this.ifYes = ifYes;
		this.ifNot = ifNot;
	}
	
	
	public Decision ifYes(){
		return ifYes;
	}
	
	public Decision ifNot(){
		return ifNot;
	}
	
	
	/**
	 * Creates a {@link Decisions} which include and continues when a predicate is met and exclude and continue otherwise
	 * @return The defined {@link Decisions}
	 */
	public static Decisions include(){
		return new Decisions(INCLUDE_AND_CONTINUE,EXCLUDE_AND_CONTINUE);
	}
	
	/**
	 * Creates a {@link Decisions} which exclude and continues when a predicate is met and exclude and continues otherwise
	 * @return The defined {@link Decisions}
	 */
	public static Decisions exclude(){
		return new Decisions(EXCLUDE_AND_CONTINUE, INCLUDE_AND_CONTINUE);
	}
	
	/**
	 * Creates a {@link Decisions} which exclude and continues when a predicate is met and exclude and continues otherwise
	 * @return The defined {@link Decisions}
	 */
	public static Decisions excludeAndStop(){
		return new Decisions(EXCLUDE_AND_STOP, INCLUDE_AND_CONTINUE);
	}
	
	
	/**
	 * Creates a {@link Decisions} which include and stop when a predicate is met and exclude and continue otherwise
	 * @return The defined {@link Decisions}
	 */
	public static Decisions includeAndStop(){
		return new Decisions(INCLUDE_AND_STOP, EXCLUDE_AND_CONTINUE);
	}
	
}

package org.pickcellslab.foundationj.bootstrap;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class AppScopes {

	private static final Logger log = LoggerFactory.getLogger(AppScopes.class);
	
	private final List<AbstractAtScope> atScopes = new ArrayList<>();



	// This is where the @Score configuration is checked for consistency
	AbstractAtScope getOrCreate(String scope, String parent, ScopePolicy policy){
		
		// Is it already registered (not efficient but that's ok)
		AbstractAtScope s = atScopes.stream().filter(as->as.scope().equals(scope)).findAny().orElse(null);

		if(s!=null) {
			// check that parent and policy are identical
			if(!s.parent().equals(parent)) throw new RuntimeException("Same scope with different parent have been found : "+scope);
			if(s.policy() != policy) throw new RuntimeException("Same scope with different ScopePolicy have been found : "+scope);
			return s;
		}
		else {// not already known

			// Find out which subtype to instantiate and register
			switch(policy) {

			case MULTIPLE: throw new RuntimeException("Not implemented");

			case SIBLING: {
				// find parent
				AbstractAtScope parentAtScope = atScopes.stream().filter(as->as.scope().equals(parent)).findAny().orElse(null);
				if(parentAtScope!=null)
					s = new AtScopeSibling(scope, parentAtScope); 
				else
					throw new RuntimeException("Cannot create a Sibling scope with no parent "
							+ "(parent with scope "+parent+"not found)");


			}break;

			case SINGLETON: {

				if(parent == null || parent.isEmpty())
					s = new AtScopeSingletonOrphan(scope); 
				else {

					AbstractAtScope parentAtScope = atScopes.stream().filter(as->as.scope().equals(parent)).findAny().orElse(null);
					if(parentAtScope!=null)
						s = new AtScopeSingletonWithParent(scope, parentAtScope); 
					else
						throw new RuntimeException("Parent with name "+parent+" requested but not found");				
				}			

			}break;

			default: throw new RuntimeException("Unknown policy");

			}

			atScopes.add(s);			
			return s;
		}
	}
	
	
	Optional<AbstractAtScope> parentAtScope(AtScope as){
		return atScopes.stream().filter(known->known.scope().equals(as.parent())).findAny();
	}


	Optional<AbstractAtScope> getAtScope(String scope){
		return atScopes.stream().filter(as->as.scope().equals(scope)).findAny();
	}


	Stream<AbstractAtScope> getAll() {
		return atScopes.stream();
	}




	/*

	 // Does not visit parent	
	static void parentToChildTraversal(ScopeImpl parent, Consumer<Scope> visitor) {

		final LinkedList<ScopeImpl> toCheck = new LinkedList<>();
		toCheck.addAll(parent.children());
		while(!toCheck.isEmpty()) {
			final ScopeImpl s = toCheck.poll();			
			final List<ScopeImpl> children = s.children();			
			toCheck.addAll(children);
			visitor.accept(s);
		}

	}


	 // Does not visit parent	 
	static void childrenToParentTraversal(ScopeImpl parent, Consumer<Scope> visitor) {

		//First pass get all leaves
		final LinkedList<ScopeImpl> leaves = new LinkedList<>();

		final LinkedList<ScopeImpl> toCheck = new LinkedList<>();
		toCheck.addAll(parent.children());
		while(!toCheck.isEmpty()) {
			final ScopeImpl s = toCheck.poll();
			final List<ScopeImpl> children = s.children();
			if(!children.isEmpty()) {
				toCheck.addAll(children);
			}
			else {
				leaves.add(s);
			}
		}

		//Now visit climbing up the tree
		while(!leaves.isEmpty()) {
			final ScopeImpl s = leaves.removeFirst();
			if(s!=parent) {
				leaves.offerLast(s.parent());
				visitor.accept(s);
			}
		}
	}
	 */

}

package org.pickcellslab.foundationj.datamodel.tools;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.DataItem;

/**
 * A container for {@link DataItem} objects.
 * Objects in this container can either be categorised as 'Target' or as 'Dependency'.
 * The meaning of these terms may vary depending on context but an example might be that 'Target' objects are
 * objects returned by a {@link Traversal} and 'Dependencies' are objects that were not returned but are still part of the same graph.
 *
 */
public interface DataItemBuild {

	
	
	/**
	 * @return A List of all the 'target' {@link DataItem}s
	 */
	public List<? extends DataItem> getAllTargets();
	
	/**
	 * @return A List of all {@link DataItem}s contained in this build
	 */
	public List<? extends DataItem> getAllDependencies();
	
	
	/**
	 * @param clazz The class of the desired targets
	 * @return A {@link Stream} for all the {@link DataItem} of the specified class that are categorised as 'Target'
	 */
	public <E extends DataItem> Stream<E> getTargets(Class<E> clazz);
	
	
	/**
	 * @param clazz The class of the desired dependencies
	 * @return A {@link Stream} for all the {@link DataItem} of the specified class that are categorised as 'Dependency'
	 */
	public <E extends DataItem> Stream<E> getDependencies(Class<E> clazz);
	
	
	/**
	 * @param <E>
	 * @param clazz The class of the desired objects
	 * @return A {@link Stream} for all the {@link DataItem} of the specified class regardless of category
	 */
	public <E extends DataItem> Stream<E> getAllItemsFor(Class<E> clazz);
	
	

	/**
	 * @param clazz The type of the desired target
	 * @return An {@link Optional} holding a target of the specified type
	 */
	public <E> Optional<E> getOneTarget(Class<E> clazz);
	
	/**
	 * @param clazz The type of the desired dependency
	 * @return An {@link Optional} holding a dependency of the specified type
	 */
	public <E> Optional<E> getOneDependency(Class<E> clazz);
	
}

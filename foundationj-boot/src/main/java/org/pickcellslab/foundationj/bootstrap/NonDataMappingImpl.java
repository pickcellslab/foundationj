package org.pickcellslab.foundationj.bootstrap;

import java.util.Collection;
import java.util.Set;

import org.pickcellslab.foundationj.mapping.extra.NonDataMapper;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapping;

class NonDataMappingImpl extends NonDataMapping {

	protected NonDataMappingImpl(NonDataMapper mapperImpl, Set<Class<?>> mappedClass, Collection<Class<?>> mappingDenied) {
		super(mapperImpl, mappedClass, mappingDenied);
	}

}

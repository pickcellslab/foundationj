package org.pickcellslab.foundationj.dataviews.mvc;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;


/**
 * Defines the {@link Dimension} which can be obtained in a dataset
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of objects the dimensions are defined for (generally DataItem or Path)
 */
public interface DimensionBroker<T,D> {

	
	public int numDimensions();
	
	public Dimension<T,D> dimension(int i);
	


}

package org.pickcellslab.foundationj.datamodel.tools;


/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * 
 * A step in a stepwise builder pattern to define the 'rules' for navigating a graph. This step defines the starting depth.
 *
 * @param <T> The type of the next builder
 */
public interface FromDepth<T> {

	/**
	 * Specify that the <a href="https://en.wikipedia.org/wiki/Graph_traversal">traversal</a> 
	 * should start from the root(s) and include the root(s)
	 * @return The next building step
	 */
	public T fromMinDepth();
	
	/**
	 * Specify that the <a href="https://en.wikipedia.org/wiki/Graph_traversal">traversal</a> 
	 * should start including nodes and links from the given depth only
	 * @param min The depth from which to start including nodes and links 
	 * @return The next building step
	 */
	public T fromDepth(int min);
	
}

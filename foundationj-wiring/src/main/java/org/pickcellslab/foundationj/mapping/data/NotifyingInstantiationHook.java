package org.pickcellslab.foundationj.mapping.data;

/**
 * An {@link InstantiationHook} which notifies {@link InstantiationListener}s when a new instance has been created.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of the instance this {@link InstantiationHook} creates
 */
public interface NotifyingInstantiationHook<T> extends InstantiationHook {

		public void addInstantiationListener(InstantiationListener<? super T> l);
	
}

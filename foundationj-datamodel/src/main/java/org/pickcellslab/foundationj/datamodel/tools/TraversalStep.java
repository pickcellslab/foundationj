package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
/**
 * A wrapper class for nodes and edges that a {@link StepTraverser} returns at each step
 * 
 * @author Guillaume Blin
 *
 */
public class TraversalStep<N,E> {

		/**
		 * The count of the round (For example, for a breadth First Traversal
		 *  this count corresponds to the depth)
		 */
		public int round;
		/**
		 * The list of nodes in order
		 */
		public List<N> nodes;
		/**
		 * The list of edges (Links) in order
		 */
		public List<E> edges;

		TraversalStep(int depth, List<N> nodes, List<E> edges){
			this.round = depth;
			this.nodes = nodes;
			this.edges = edges;
		}
	}
	

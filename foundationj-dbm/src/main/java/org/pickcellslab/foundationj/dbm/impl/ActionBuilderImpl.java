package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.ActionBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.SplitChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.SplittableAccessChoice;

abstract class ActionBuilderImpl<T,O> implements ActionBuilder<T, O> {


	protected abstract TargetType<?,?,T> getType();

	protected abstract Action<? super T,O> getAction();

	
	@Override
	public final AccessChoice<T,QueryRunner<O>> inOneSet(){
		return new MonolithicQBuilder<>(getType(), getAction(), null);
	};
	
	
	@Override
	public final AccessChoice<T,QueryRunner<O>> inOneSet(String name){
		return new MonolithicQBuilder<>(getType(), getAction(),name);
	};



	
	@Override
	public SplittableAccessChoice<?,?,T,O,SplitChoice<T,O>> inGroups(){
		return new MultiSearchFork<>(getType(), getAction());
	};
}

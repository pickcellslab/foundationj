package org.pickcellslab.foundationj.ui.renderers;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.DataToIconID;

public class DimensionRenderer implements ListCellRenderer<Dimension<?,?>> {

	private final UITheme theme;
	
	public DimensionRenderer(UITheme theme) {
		this.theme = theme;
	}
	
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Dimension<?,?>> list, Dimension<?,?> value, int index,
			boolean isSelected, boolean cellHasFocus) {
		
		//Assign a different icon for each MetaItem type
				JLabel label = new JLabel();

				if(value == null){
					label.setText("   NONE");
					return label;			
				}
				
				
				label.setIcon(theme.icon(DataToIconID.get(value), 16));

				//Add the name as text
				label.setText(value.name());

				//Add the tooltip text
				label.setToolTipText(value.toHTML());  

				//Create a small padding
				Border paddingBorder = BorderFactory.createEmptyBorder(3,3,3,3);
				label.setBorder(paddingBorder);


				//Handle hovering
				Color background;
				Color foreground;

				// check if this cell represents the current DnD drop location
				JList.DropLocation dropLocation = list.getDropLocation();
				if (dropLocation != null
						&& !dropLocation.isInsert()
						&& dropLocation.getIndex() == index) {

					background = Color.BLUE;
					foreground = Color.WHITE;

					// check if this cell is selected
				} else if (isSelected) {
					background = Color.WHITE;
					foreground = Color.BLACK;

					// unselected, and not the DnD drop location
				} else if (cellHasFocus) {
					background = Color.RED;
					foreground = Color.WHITE;

					// unselected, and not the DnD drop location
				} 
				else {
					background = Color.WHITE;
					foreground = Color.GRAY;
				};

				label.setBackground(background);
				label.setForeground(foreground);

				return label;
		
		
	}

}

package org.pickcellslab.foundationj.dbm.queries;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.tools.SuppliedSetOperation;

/**
 * A {@link SuppliedSetOperation} working on the outputs of a TraversalDescription. 
 * This object also wraps the description of the operation
 * 
 * @author Guillaume Blin
 *
 */
public class SuppliedTraversalOperation extends SuppliedSetOperation<TraversalDefinition> {


	private final String description;


	SuppliedTraversalOperation(Builder b){
		super(b);
		this.description = b.description;
	}

	public static Builder newBuilder(TraversalDefinition td){
		return new Builder(td);
	}
	
	public String description(){
		return description;
	}
	

	public static class Builder extends SuppliedSetOperation.Builder<TraversalDefinition>{

		private String description;
		
		
		Builder(TraversalDefinition first) {
			super(first);
			description = first.description();
		}
		
		public Builder union(TraversalDefinition next){
			super.union(next);
			description += " UNION "+next.description();
			return this;
		}
		
		public Builder intersection(TraversalDefinition next){
			super.intersection(next);
			description += " INTERSECT "+next.description();
			return this;
		}
		
		public Builder difference(TraversalDefinition next){
			super.difference(next);
			description += " DIFFERENCE "+next.description();
			return this;
		}
		
		
		public Builder symetric(TraversalDefinition next){
			super.symetric(next);
			description += " SYMETRIC DIF "+next.description();
			return this;
		}
		
		
		
		public SuppliedTraversalOperation build(){
			return new SuppliedTraversalOperation(this);
		};
		
		
		
		
	}
	
}
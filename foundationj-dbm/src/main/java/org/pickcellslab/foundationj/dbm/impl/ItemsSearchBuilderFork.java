package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dbm.queries.actions.Action;


final class ItemsSearchBuilderFork<T,O> extends ActionBuilderImpl<T,O>{

	
	private final TargetType<?,?,T> type;
	private final Action<? super T,O> action;

	ItemsSearchBuilderFork(TargetType<?,?,T> type, Action<? super T,O> action){
		this.type = type;
		this.action = action;
	}

	@Override
	protected TargetType<?,?,T> getType() {
		return type;
	}

	@Override
	protected Action<? super T, O> getAction() {
		return action;
	}
	
	
	
	
}

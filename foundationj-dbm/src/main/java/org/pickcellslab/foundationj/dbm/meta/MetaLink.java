package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;

/**
 * A {@link MetaModel} object which represents a type of {@link Link} in the data model.
 * <br>
 * <b>NB:</b> There are as many MetaLinks as there are link type / source / target type combinations.
 * 
 * @author Guillaume Blin
 *
 */
public interface MetaLink extends MetaQueryable{

	public static AKey<String> type = AKey.get("Type", String.class);
	
	
	public MetaClass source();
	
	public MetaClass target();

	
	public <E> DimensionContainer<Link,E> readOnSource(AKey<E> sk);
	
	public <E> DimensionContainer<Link, E> readOnTarget(AKey<E> tk);

	/**
	 * @return The declaredType of the link that this MetaLink represents
	 */
	public String linkType();
}

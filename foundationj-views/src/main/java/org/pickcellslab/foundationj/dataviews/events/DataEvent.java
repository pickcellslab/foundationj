package org.pickcellslab.foundationj.dataviews.events;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.dataviews.mvc.DataViewModel;


/**
 * An event which gets fired when a modification changes on the data of model
 * 
 * @author Guillaume Blin
 *
 * @param <M> The type of the source
 * 
 * @see Modif
 * 
 */
public class DataEvent {

	private final DataViewModel<?> source;
	private final Modif type;
	private final String series;

	public DataEvent(DataViewModel<?> source, Modif type, String series) {
		
		Objects.requireNonNull(source);
		Objects.requireNonNull(type);
		Objects.requireNonNull(series);
		
		this.source = source;	
		this.type = type;
		this.series = series;
	}

	public DataViewModel<?> getSource() {
		return source;
	}

	public Modif getType() {
		return type;
	}

	public String getSeries() {
		return series;
	}

}

package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.annotations.Mapping;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;

@Mapping(id="GroupingByDepth")
class PathSplitterByDepth<V,E> implements ExplicitPathSplit<V,E,Integer>{

	
	
	@Override
	public String description() {
		return "Group targets based on their position in the tarversal";
	}


	@Override
	public void setFactory(FjAdaptersFactory<V, E> f) {
		// Not required
	}


	@Override
	public Integer getKeyFor(Path<V, E> p) {
		return p.edgesCount();
	}

}

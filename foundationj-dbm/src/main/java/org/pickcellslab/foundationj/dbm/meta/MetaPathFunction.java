package org.pickcellslab.foundationj.dbm.meta;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToLinksIterator;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToLinksWithPredicate;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToNodesIterator;
import org.pickcellslab.foundationj.datamodel.functions.TraversalToNodesWithPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.AKeyInfo;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequence;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequenceHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Data(typeId = "MetaPathFunction", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaPathFunction<T> extends MetaItem implements MetaCastable<T> {

	private static final Logger log = LoggerFactory.getLogger(MetaPathFunction.class);

	public static final String SEQUENCED = "SEQUENCED", REDUCTION = "REDUCTION", FILTER = "FILTER";

	private static AKey<String> hintsType = AKey.get("Hints Type", String.class);


	private GraphSequenceDimension raw;




	private MetaPathFunction(String uid){
		super(uid);
	}




	public static <T> MetaPathFunction<T> getOrCreate(
			MetaInstanceDirector director,
			String name,
			String description,
			MetaClass generator,
			MetaReductionOperation mechanism,
			MetaQueryable target) {

		return getOrCreate(director, name, description, generator, null, mechanism, target);

	}



	public static <T> MetaPathFunction<T> getOrCreate(
			MetaInstanceDirector director,
			String name,
			String description,
			MetaClass generator,
			MetaFilter filter,
			MetaReductionOperation mechanism,
			MetaQueryable target) {

		try{

			final MetaPathFunction<T> mpf = director.getOrCreate(MetaPathFunction.class, "Path Function : "+name+"/"+generator.name()+"/"+target.name() +"/"+mechanism.name());

			if(!mpf.getAttribute(MetaItem.name).isPresent()){// Check if it was retrieved or if it already existed

				mpf.setAttribute(MetaItem.name, name);
				mpf.setAttribute(MetaItem.desc, description);

				// No choice we have to write down the class for the hints in order to be able to generate a dimension without the DataRegistry
				final GraphSequence gs = generator.itemClass(director.registry()).getAnnotation(GraphSequence.class);
				if(gs==null)
					throw new IllegalArgumentException("The provided MetaClass does not represent a GraphSequencePointer annotated with GraphSequence");

				mpf.setAttribute(hintsType, gs.value().getName());


				// Transform the name of the mechanism to be more informative to the user
				mechanism.setAttribute(MetaItem.name, name + " " + mechanism.getAttribute(MetaItem.name).get());


				new DataLink(SEQUENCED, mpf, target, true);
				new DataLink(MetaModel.requiresLink, mpf, target, true);

				new DataLink(REDUCTION, mpf, mechanism, true);
				new DataLink(MetaModel.requiresLink, mpf, mechanism, true);

				//Attach to the queryable that uses this function
				new DataLink(MetaReadable.APPLICABLE_TO, mpf, generator, true);
				new DataLink(MetaModel.requiresLink, mpf, generator, true);

				if(filter!=null) {
					new DataLink(FILTER, mpf, filter, true);
					new DataLink(MetaModel.requiresLink, mpf, filter, true);
				}

			}

			return mpf;

		}catch (Exception e){
			throw new RuntimeException(e);
		}

	}





	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}











	@Override
	public String toString(){
		return name()+" (Path Function)";
	}



	@Override
	public dType dataType() {
		return getMetaReduction().generatedKey().dType();
	}

	@Override
	public MetaClass owner() {
		return (MetaClass) typeMap.get(MetaReadable.APPLICABLE_TO).iterator().next().target();
	}

	public MetaQueryable getSequenced() {
		return (MetaQueryable) typeMap.get(SEQUENCED).iterator().next().target();
	}

	public MetaFilter getFilter() {
		final Set<Link> set = typeMap.get(FILTER);
		if(set==null)
			return null;
		else
			return (MetaFilter) set.iterator().next().target();
	}


	@Override
	public int numDimensions() {
		return getRaw().numDimensions();
	}


	@Override
	public Dimension<DataItem,T> getDimension(int index) {
		if(index<0 || index>=numDimensions())
			throw new ArrayIndexOutOfBoundsException(index);
		return getRaw().getDimension(index);
	}



	@Override
	public GraphSequenceDimension getRaw() {
		if(raw == null)
			raw = createDimension();
		return raw;
	}





	private GraphSequenceDimension createDimension() {

		final MetaQueryable mq = getSequenced();
		final MetaFilter mf = getFilter();

		try {

			final GraphSequenceHints hints = (GraphSequenceHints) Class.forName(getAttribute(hintsType).get()).newInstance();
			ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> traversalToIteration;

			if(mq instanceof MetaLink) {
				traversalToIteration = mf==null ? new TraversalToLinksIterator() : new TraversalToLinksWithPredicate(mf.toFilter());
			}
			else {
				traversalToIteration = mf==null ?  new TraversalToNodesIterator() : new TraversalToNodesWithPredicate(mf.toFilter());
			}


			return new GraphSequenceDimension(hints, this.getMetaReduction(), traversalToIteration);

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new RuntimeException(getAttribute(hintsType).get()+ "is no longer a valid GraphSequenceHints type, this may be the result of a software update");
		}
	}




	@Override
	public String toHTML() {
		return "<HTML>"+
				"<p><strong>MetaPathFunction</strong>"+
				"<ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"<li>Number of Dimensions:"+numDimensions()+
				"</li></p></HTML>";
	}



	@Override
	public Class<?> dataClass() {
		return getRaw().getReturnType();
	}


	@Override
	public void cast(DataAccess access) throws DataAccessException {
		//throw new DataAccessException("MetaPathFunction - Cast : Not implemented yet");

		Objects.requireNonNull(access, "Access is null");

		final MetaQueryable owner = owner();
		final Updater u = createUpdater(access);

		owner.initiateUpdate(access, u).getAll().run();

		// Now delete this
		access.queryFactory().deleteAnnotation(this);
	}


	@Override
	public void cast(DataAccess access, ExplicitPredicate<WritableDataItem> p) throws DataAccessException {
		//throw new DataAccessException("MetaPathFunction - Cast : Not implemented yet");

		Objects.requireNonNull(access, "Access is null");

		final MetaQueryable owner = owner();
		final Updater u = createUpdater(access);

		owner.initiateUpdate(access, u).useFilter(p).run();

		// Now delete this
		access.queryFactory().deleteAnnotation(this);
	}


	@Override
	public void cast(DataAccess access, TraversalDefinition td) throws DataAccessException, IllegalArgumentException {

		//throw new DataAccessException("MetaPathFunction - Cast : Not implemented yet");

		Objects.requireNonNull(access, "Access is null");

		final MetaQueryable owner = owner();
		final Updater u = createUpdater(access);

		owner.initiateUpdate(access, u).useTraversal(td).run();

		// Now delete this
		access.queryFactory().deleteAnnotation(this);


	}



	private MetaReductionOperation getMetaReduction(){
		return ((MetaReductionOperation) this.typeMap.get(REDUCTION).iterator().next().target());
	}




	private Updater createUpdater(DataAccess access){

		final GraphSequenceDimension dim = getRaw();
		final AKey aKey = AKey.get(name(), dim.generatedKey.type());

		return new Updater(){


			final AKeyInfo k = new AKeyInfo(owner(), aKey, description(), dim.lengthOfReturnedValue);

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void performAction(Updatable i) throws DataAccessException {
				Object o = dim.apply(i);
				if(o!=null)
					i.setAttribute((AKey)k.getKey(), o);
			}

			@Override
			public List<AKeyInfo> updateIntentions() {
				return Collections.singletonList(k);
			}

		};

	}






	@Mapping(id="GraphSequenceDimension")
	class GraphSequenceDimension implements CachingDimension<T>{

		@Supported
		private ExplicitReduction<DataItem, ?> reduction;
		@Supported
		private final ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> traversalToIteration;

		private final AKey<?> generatedKey;
		private final int lengthOfReturnedValue;
		private final String hintsClazz;

		@Ignored
		private GraphSequenceHints hints;		
		@Ignored
		private Map<String,Object> cache;
		@Ignored
		private int useCaching;
		@Ignored
		private Consumer<StepTraverser<NodeItem,Link>> consumer;


		public GraphSequenceDimension(GraphSequenceHints hints, MetaReductionOperation fctry, ExplicitFunction<Traversal<NodeItem,Link>,Iterator<? extends DataItem>> traversalToIteration) {

			this.lengthOfReturnedValue = fctry.lengthOfReturnedValue();
			this.generatedKey = fctry.generatedKey();
			this.reduction = fctry.toReductionOperation();
			this.traversalToIteration = traversalToIteration;

			this.hints = hints;
			this.hintsClazz = hints.getClass().getName();


			this.consumer = createConsumer();

		}


		private Consumer<StepTraverser<NodeItem, Link>> createConsumer() {
			return p->{
				final Iterator<? extends DataItem> item = traversalToIteration.apply(p.traverse());
				if(item!=null)
					item.forEachRemaining(i->reduction.include(i));
			};
		}


		@Override
		public dType dataType() {
			return generatedKey.dType();
		}

		@SuppressWarnings("unchecked")
		@Override
		public Class getReturnType() {
			return reduction.getReturnType();
		}

		@Override
		public Object apply(DataItem t) {	

			//The provided NodeItem should only be a database object representing a GraphSequencePointer of our clazz (should check this?)

			if(null==cache)
				initIgnored();

			Object result = cache.get(t.toString());
			//System.out.println(t.toString());
			if(result!=null)
				return result;

			if(t instanceof Link)
				return null;


			hints.consumeFromDatabaseItem((NodeItem) t, consumer);

			result = reduction.getResult();
			reduction = reduction.factor();
			if(useCaching>1)
				cache.put(t.toString(), result);

			return result;
		}


		private void initIgnored() {

			try {
				cache = new HashMap<>();
				hints=(GraphSequenceHints) Class.forName(hintsClazz).newInstance();
				this.consumer = createConsumer();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				throw new RuntimeException("Unable to initialise the GraphSequenceDimension, Obsolete GraphSequenceHints?", e);
			}

		}


		@Override
		public int index() {
			return -1;
		}

		@Override
		public int length() {
			return getReturnType().isArray() ? -1 : numDimensions();
		}

		@Override
		public String name() {
			return MetaPathFunction.this.name();
		}

		@Override
		public String getName(int index) {
			return numDimensions() < 2 ? name() : name()+" ["+index+"]";
		}


		@Override
		public String info() {
			return description();
		}

		@Override
		public String description() {
			return MetaPathFunction.this.description();
		}

		@Override
		public String description(int index) {
			return numDimensions() < 2 ? name() : description()+" ["+index+"]";
		}

		@Override
		public Dimension<DataItem, ?> getRaw() {
			return this;
		}

		@Override
		public int numDimensions() {
			return lengthOfReturnedValue;
		}

		@Override
		public Dimension<DataItem, T> getDimension(int index) throws ArrayIndexOutOfBoundsException {

			int nd = numDimensions();
			if(nd <= index)
				throw new ArrayIndexOutOfBoundsException("Provided Index "+index+" ; num Dims : "+nd);

			if(nd != 1)
				useCaching++;

			return new CachedDimension<>(this, index);

		}




	}



}

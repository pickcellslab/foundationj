package org.pickcellslab.foundationj.dbm.queries.builders;

import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;

public interface SchemaQueryBuilder extends AccessChoice<DatabaseNodeDeletable, QueryRunner<Void>>, QueryRunner<Void> {

	/**
	 * @param label A label to add to the Nodes targeted by the query.
	 * @return The configured SchemaQueryBuilder.
	 */
	SchemaQueryBuilder addLabel(String label);



}
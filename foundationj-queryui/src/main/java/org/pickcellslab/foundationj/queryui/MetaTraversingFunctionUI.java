package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.Objects;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.PredictableMetaSplit;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.DataToIconID;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

@SuppressWarnings("serial")
public class MetaTraversingFunctionUI extends WizardNode{


	private CreatePathPanel<?> pathPanel;
	private final DataAccess access;

	public MetaTraversingFunctionUI(WizardNode parent, UITheme theme, DataAccess access, MetaClass mc) {

		super(parent);
		
		Objects.requireNonNull(access);

		this.access = access;
		
		setLayout(new BorderLayout());


		//Choice view				
		ChoiceSet choiceView = null;
		try {
			choiceView = new ChoiceSet(theme, access, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}



		// In the West add MetaModel types, filters and existing Attributes.

		JPanel westPanel = new JPanel(new BorderLayout());


		//Available choices ScrollPane
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.WEST);		
		scrollPane.setViewportView(choiceView);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JLabel choiceLabel = new JLabel("MetaModel Browsing :");
		choiceLabel.setBorder(new EmptyBorder(10,10,10,10));
		Font lFont = choiceLabel.getFont();
		choiceLabel.setFont( new Font(lFont.getFontName(), Font.BOLD, lFont.getSize()));
		choiceLabel.setIcon(theme.icon(DataToIconID.get(dType.MIXED), 24));
		scrollPane.setColumnHeaderView(choiceLabel);
		westPanel.add(scrollPane,BorderLayout.CENTER);

		add(westPanel,BorderLayout.WEST);



		// In the Center : Add path definition, filter combination, Target grouping ad Reduction Operation.
		
		try {
			pathPanel = new CreatePathPanel<>(theme, access, choiceView,  QueryConfigs.newTargetedOnlyToNumber("").disablePathSPlitting().setFixedRootType(mc).build(), true);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		add(pathPanel, BorderLayout.CENTER);

		pathPanel.addValidityListener(l->this.fireNextValidityChanged());

	}





	@Override
	public boolean validity() {
		return pathPanel.validity();
	}


	@Override
	protected WizardNode getNextStep() {		
		
		return new MetaDescriptionPanel<>(
				this, 
				access.metaModel().getInstanceManager(),
				pathPanel.getMetaTraversal(access.metaModel()),
				(PredictableMetaSplit)pathPanel.getGrouping(access.metaModel()).orElse(null),
				pathPanel.getMechanism().get(),
				pathPanel.getRoot());
		
	}


	@Override
	public int numberOfForks() {
		return 1;
	}



}

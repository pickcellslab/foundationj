package org.pickcellslab.foundationj.bootstrap;

import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.ScopePolicy;

public class AtScopeSingletonOrphan extends AbstractAtScope {

	private ScopeImpl active;
	


	public AtScopeSingletonOrphan(String scope) {
		super(scope, "", ScopePolicy.SINGLETON);
	}
	
	

		

	@Override
	public ScopeImpl createAndRegister(String id, String parentId) {
		if(active != null)
			throw new RuntimeException(scope.toString() + "is already active");
		
		else {
			active = new ScopeImpl(scope, id, null, policy, comps);
					
			System.out.println("AtScopeSingletonOrphan: Scope "+super.scope+" with id "+id+ " and parent "+parentId + "registered");
			
			return active;
		}		
	}


	@Override
	public ScopeImpl remove(String id) {
		if(active==null)
			throw new RuntimeException("There are no active Scope with "+id);
		else {
			ScopeImpl toReturn = active;
			active = null;
			System.out.println("AtScopeSingletonOrphan: Scope "+id+" removed ! ");
			return toReturn;
		}
	}

	@Override
	public Stream<ScopeImpl> active() {
		if(active==null)
			return Stream.empty();
		else
			return Stream.of(active);
	}


	@Override
	public Optional<ScopeImpl> activeWithId(String id) {
		return active().findAny();
	}

	
	@Override
	boolean hierarchyHasComponent(Class<?> comp) {
		return comps.contains(comp);	
	}

}

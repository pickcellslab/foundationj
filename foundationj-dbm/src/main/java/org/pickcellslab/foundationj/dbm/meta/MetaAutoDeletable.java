package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.annotations.Tag;

@Tag(creator = "MetaModel",
description = "MetaModel object which will be automatically deleted when "
		+ "other MetaModel object depending on this object are deleted",
		tag = "MetaAutoDeletable")
public interface MetaAutoDeletable extends ManuallyStored {


}

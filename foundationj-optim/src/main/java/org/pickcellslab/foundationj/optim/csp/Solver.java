package org.pickcellslab.foundationj.optim.csp;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

/**
 * @author Guillaume Blin
 *
 * @param <N> The type of {@link Node}
 * @param <D> The type of {@link Edge}
 * TODO DOCME
 * TODO listener for validity or costchange
 */
public class Solver
<
N extends Node<D>,
D extends Edge<? extends N>
>

{ 

	//Configuration
	private N node;
	private Enumerator<N,D> enumerator;
	private Validator<N,D> validator;
	private CostFunction<N,D> cost;
	private Collection<Solver<N,D>> potentialTargets;

	//CurrentState
	private boolean solved = false;
	private boolean currentValidity;
	private double currentCost;	
	
	private Collection<Solver<N, D>> selection = Collections.emptyList();
	
	private Set<Solver<N,D>> potentiallyPickedBy = new HashSet<>();

	//Loop detection
	private Set<Solver<N, D>> loopTest = new HashSet<>();


	//Logging
	private static Logger log = LoggerFactory.getLogger(Solver.class);
	private Function<N,String> naming = n -> n.toString();


	//Instantiation and configuration elements used by GraphOptimization

	Solver(N node) {
		this.node = node;
	}




	void setValidator(Validator<N,D> v){
		this.validator = v;
	}

	void setEnumerator(Enumerator<N,D> e){
		this.enumerator = e;
		//enumerator.initialise();
	}

	void setCostFunction(CostFunction<N,D> c){
		this.cost = c;
	}

	/*
	static void setLevel(Level level){
		log.setLevel(level);
	}
	*/

	void setNaming(Function<N,String> naming){
		this.naming = naming;
	}



	

	//-------------------------------------------------------------------------------------------------
	//Accessed by the GraphOptimization to initialise the solver
	/**
	 * Causes this Solver to initialise its components using the specified solvers as potential targets
	 */
	void initialise(Collection<Solver<N,D>> newTargets){	


		//Set potential targets
		potentialTargets = newTargets;

		//initialise enumerator
		enumerator.initialise();		

		//Register as potentialPickedBy to all potential match
		potentialTargets
		.stream()
		.forEach(l->l.addPotentiallyPickedBy(this));	

		currentValidity = validator.isValid(this, selection);
		currentCost = cost.compute(this, selection);

		if(currentCost <= cost.minCost() && currentValidity == true)
			solved = true;
		
	}

	Collection<Solver<N,D>> potentialPickers(){
		return new ArrayList<>(potentiallyPickedBy);
	}
	
	void addPotentialPicker(Solver<N,D> s){
		potentiallyPickedBy.add(s);
	}
	
	void removePotentialPickedBy(Solver<N,D> s){
		potentiallyPickedBy.remove(s);
	}
			
	
	void addPotentialTarget(Solver<N,D> s){
		potentialTargets.add(s);
		enumerator.initialise();
	}
	
	void removePotentialTarget(Solver<N,D> s){
		potentialTargets.remove(s);
		if(selection.contains(s)){
			double newCost = cost.compute(this, Collections.emptyList());
			boolean newValidity = validator.isValid(this,Collections.emptyList());
			makeAssignement(Collections.emptyList(), newCost, newValidity);
		}
		enumerator.initialise();
	}


	
	//Called by the GraphOptimization instance to define a specific selection
	void setSelection(Collection<Solver<N, D>> sel){
		boolean newValidity = validator.isValid(this, sel);
		double newCost = cost.compute(this, sel);
		makeAssignement(sel, newCost,  newValidity);

	}






	void deleteNonSelected(Predicate<D> p){

		//log.debug("-------------------------------------------------------------------------------------------");
		//log.debug("source = "+getNode().toString());
		log.debug("Selection size = "+getSelection().size());		


		LongAdder counter = new LongAdder();

		getNode().links().filter(p).filter(l->l.source()==getNode()).forEach(l->{

			//log.debug("target = "+l.target().toString());

			boolean delete = true;
			for(Solver<N,D> s : getSelection()){

				//log.debug("node = "+s.getNode().toString());
				if(s.getNode() == l.target()){
					delete = false;
					break;
				}
			}
			if(delete){
				l.delete();
				counter.increment();
			}

		});

		log.debug("Number of deletes = "+counter);

	}




	/**
	 * Screens for the cheapest valid solution among the possible solutions
	 */
	void screen(){
	
		log.debug("-------------------------------------------------------------------------------");
		log.debug("Solver for "+naming.apply(node)+" is starting to screen");
	
		if((currentCost>cost.minCost() || solved == false)){
	
			log.debug("Lock passed");
	
			//Map keeping track of the best solution among available
			Map<Double, Collection<Solver<N, D>>> allCosts = new HashMap<>();
	
			log.debug("Screening neighbours :");
	
			for(Collection<Solver<N, D>> neighbours : enumerator){
	
	
				neighbours.forEach(sel->
				log.debug("\t-> "+((Solver<N,D>) sel).naming.apply(sel.getNode())));
	
				//First compute and rank costs independently of the collective state
				//TODO replace by collective check
				boolean check = validator.isValid(this,neighbours);
	
				if(check)					
					allCosts.put(cost.compute(this, neighbours),neighbours);
	
				log.debug("   - NEXT -  ");
			}
	
			log.debug("Computing for each local solution, now evaluating");
	
	
			//If the map is empty then check the cost for an empty selection and validity
			//Make sure the map is not empty. and make the assignment
			if(allCosts.isEmpty()){
				log.debug("There is no neighbours combination that satisfies this source");
				double newCost = cost.compute(this, Collections.emptyList());
				boolean newValidity = validator.isValid(this,Collections.emptyList());
				makeAssignement(Collections.emptyList(), newCost, newValidity);
				return;				
			}
	
	
			//If not empty, then assign the lowest cost
			//Rank
			List<Double> egoRank = allCosts.keySet().stream().sorted().collect(Collectors.toList());
			double newCost = egoRank.get(0);
			//Assign
			makeAssignement(allCosts.get(newCost), newCost, true);
	
		}
	
	}




	/**
	 * Screens for the cheapest valid solution among the possible solutions
	 */
	void screenAsRoot(){
		//TODO check whats best
	
		log.debug("-------------------------------------------------------------------------------");
		log.debug("Solver for  "+naming.apply(node)+" is starting to screen as root");
	
		selection.forEach(s->s.resetSelection());
		//pickedBy().forEach(s->s.resetSelection());
		screen();
	}




	private void resetSelection() {
		if(!selection.isEmpty())
			setSelection(Collections.emptyList());		
	}




	//----------------------------------------------------------------------------------------
	//The following method will be called when the cost found a change
	
	private void costChange(Solver<N,D> source, double oldValue, double newValue) {
	
	
		log.debug("Cost change called in "+naming.apply(getNode()));
	
		if(selection.contains(source)){
	
			loopTest.add(this);
	
			//PreventLoop
			if(loopTest.contains(this)){
				pickedBy().forEach(s->s.resetSelection());
				loopTest.clear();
			}
	
	
			double prev = currentCost;
			double novel = cost.compute(this, selection);
			if(prev != novel){
				log.debug("previous cost "+prev);
				log.debug("new cost "+novel);
				currentCost = novel;
				pickedBy().forEach(l->l.costChange(this, prev, novel));
			}
			//else screen();
		}
		//TODO else if (oldValue>newValue && cost.minCost()< currentCost) screen();
	
	}




	//----------------------------------------------------------------------------------------
	//The following method will be called when the validator found a change
	
	private void validityChanged(
			Solver<N,D> source,
			boolean oldValue,
			boolean newValue) {
	
		//log.trace(this.getNode()+ " : validityChange Received");
		//Check if source is in current selection
		if(selection.contains(source))		
			makeAssignement(selection, cost.compute(this, selection), validator.isValid(this, selection));
	
		//Otherwise if one of the potential match has become available, then screen again
		else if(newValue == true && currentValidity == false)		
			;//screen();
	
	}




	private void addPotentiallyPickedBy(Solver<N,D> lst){
		potentiallyPickedBy.add(lst);
	}




	private void makeAssignement(Collection<Solver<N, D>> newSelection, double newCost, boolean newValidity) {
	
	
	
		//log.debug("-----------------------------------------------------------------");
		log.debug(naming.apply(getNode())+" : Making assignement : ");
	
		selection = newSelection;
	
		if(selection.isEmpty())
			log.debug("empty selection");
		else{
			log.debug("Selection : ");
			selection.forEach(sel->
			log.debug("\t\t-> "+((Solver<N,D>) sel).naming.apply(sel.getNode())));
		}
	
	
		if(newValidity)
			solved = true;			
		else
			solved = false;
	
	
		if(currentValidity != newValidity){			
	
			log.debug(naming.apply(getNode())+" : Firing new Validity! ("+newValidity+")");
	
			currentCost = newCost;
			//copy lstrs for iteration to prevent concurrent modif exception
			//if one of the listeners decides to deregister itself
			Set<Solver<N,D>> listeners = new HashSet<>(pickedBy());
	
	
			currentValidity = newValidity;	
			for(Solver<N,D> l : listeners)
				l.validityChanged(this, !newValidity, currentValidity);				
			log.debug("["+naming.apply(getNode())+" : Firing new Validity Done! ]");
	
		}
		else if(currentCost != newCost){
			log.debug(naming.apply(getNode())+" : Firing new Cost! ");
			double oldCost = currentCost;
			currentCost = newCost;
	
			Set<Solver<N,D>> listeners = new HashSet<>(pickedBy());
			listeners.forEach(l->l.costChange(this,oldCost, currentCost));
			log.debug("["+naming.apply(getNode())+" : Firing new Cost Done! ]");
		}
	
	}




	/**
	 * @return The {@link Node} this {@link Solver} is associated to. Note that in combination with 
	 * {@link #getSelection()}, it is possible to obtain the current coordinates of this solver
	 * into the Graph
	 */
	public N getNode(){
		return node;
	}




	public Collection<Solver<N,D>> potentialTargets(){
		return Collections.unmodifiableCollection(potentialTargets);
	}




	/**
	 * @return A {@link Collection} holding the neighbouring {@link Node}s currently selected by this solver
	 * @see #getNode()
	 */
	public Collection<Solver<N, D>> getSelection(){
		return selection;
	}




	public CostFunction<N,D> getCostFunction(){
		return cost;
	}




	public Enumerator<N,D> getEnumerator(){
		return enumerator;
	}




	public Validator<N,D> getValidator(){
		return validator;
	}




	public double currentCost(){
		return currentCost;
	}




	public boolean currentValidity(){
		return currentValidity;
	}




	/**
	 * @return {@code true} if this Solver has identified a solution, {@code false}
	 * otherwise
	 */
	public boolean hasSolution(){
		log.debug(naming.apply(node)+" is returning solved = "+solved);
		return solved;
	}




	public List<Solver<N,D>> pickedBy(){
		return potentiallyPickedBy
				.stream()
				.filter(l -> l.getSelection().contains(this))
				.collect(Collectors.toList());
	}






}

package org.pickcellslab.foundationj.dbm.queries.actions;

import org.pickcellslab.foundationj.datamodel.Factory;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;

public class ReductionMechanism<I, O> implements ActionMechanism<I,O>, Factory<ReductionMechanism<I,O>>{

	
	private final ExplicitReduction<I, O> op;

	public ReductionMechanism(ExplicitReduction<I,O> op) {
		this.op = op;
	}
	
	@Override
	public ReductionMechanism<I,O> factor() {
		return new ReductionMechanism<>(op.factor());
	}

	@Override
	public void performAction(I i) throws DataAccessException {
		op.include(i);
	}

	@Override
	public O get() {
		return op.getResult();
	};
	
	public String description() {
		return op.description();
	}
	
}

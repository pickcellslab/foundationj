package org.pickcellslab.foundationj.datamodel.reductions;

import org.pickcellslab.foundationj.datamodel.ExplicitOperation;
import org.pickcellslab.foundationj.datamodel.Factory;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;

/**
 * An {@link ExplicitReduction} is an {@link ExplicitOperation} which accepts objects sequentially
 * and returns a value once all objects have been processed.
 * <br><br>
 * For example, a sum is a form of reduction.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of input of this {@link ExplicitReduction}
 * @param <R> The type of result created by this {@link ExplicitReduction}
 */
public interface ExplicitReduction<T,R> extends Factory<ExplicitReduction<T,R>>, ExplicitOperation {


	/**
	 * Used when the {@link ExplicitReduction} is used to reduce data obtained from a {@link Traversal}
	 * @param root The root of the traversal
	 * @param included whether or not the root should be included in the result
	 */
	public void root(T root, boolean included);
	
	/**
	 * Specifies that the result that this {@link ExplicitReduction} will produce needs to take
	 * into consideration the provided object
	 * @param t an object to be included
	 */
	public void include(T t);
	
	/**
	 * @return The result of this {@link ExplicitReduction}
	 */
	public R getResult();
	
	/**
	 * @return The Class of the results produced by this {@link ExplicitReduction}
	 */
	public Class<R> getReturnType();
	

}

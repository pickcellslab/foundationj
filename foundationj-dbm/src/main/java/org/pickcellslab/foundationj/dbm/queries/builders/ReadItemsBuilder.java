package org.pickcellslab.foundationj.dbm.queries.builders;

import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.queries.ReadTable;

public interface ReadItemsBuilder<T extends WritableDataItem> extends ReadBuilder<T> {

	ActionBuilder<T,Long> count();
	
	<E> ActionBuilder<T, List<E>> makeList(AKey<E> k);

	<E> ActionBuilder<T, List<E>> makeList(ExplicitFunction<? super T, E> f);

	ActionBuilder<T,ReadTable> makeTable();

	ActionBuilder<T, Map<AKey<?>,SummaryStatistics>> makeStats();

	

}
package org.pickcellslab.foundationj.scope;
/**
 * Listens for {@link RuntimeScope} life cycle events.
 * 
 * @author Guillaume Blin
 *
 */
public interface ScopeEventListener {


	// TODO scope starting issue

	// TODO componentInitialisationFailed(Scope scope, Class<? extends SwingSupplier> viewClazz, Throwable t)

	/**
	 * Notifies this {@link ScopeEventListener} that the given {@link RuntimeScope} is about to stop.
	 * @param willStop The {@link RuntimeScope} which is about to stop.
	 */
	public void beforeStop(RuntimeScope willStop);

	/**
	 * Notifies this {@link ScopeEventListener} that the given {@link RuntimeScope} has stopped.
	 * @param hasStopped The {@link RuntimeScope} which has stopped.
	 */
	public void afterStop(RuntimeScope hasStopped);

	/**
	 * Notifies this {@link ScopeEventListener} that the given {@link RuntimeScope} is about to star.
	 * @param willStart The {@link RuntimeScope} which is about to start
	 */
	public void beforeStart(RuntimeScope willStart);

	/**
	 * Notifies this {@link ScopeEventListener} that the given {@link RuntimeScope} has just started.
	 * @param hasStarted The {@link RuntimeScope} which has just started.
	 */
	public void afterStart(RuntimeScope hasStarted);

}

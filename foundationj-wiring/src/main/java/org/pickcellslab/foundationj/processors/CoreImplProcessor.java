package org.pickcellslab.foundationj.processors;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.CoreImpl;



public class CoreImplProcessor extends AbstractProcessor {


	@Override
	public Set<String> getSupportedAnnotationTypes(){
		return Collections.singleton(CoreImpl.class.getName());
	};


	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;

		final Map<String,String> data = new HashMap<>();

		// Find CoreImpl elements from the current compilation sources
		for (TypeElement type : (Collection<TypeElement>)roundEnv.getElementsAnnotatedWith(CoreImpl.class)) {

			// First check that contract is fulfilled
			String coreInterface = contractRespected(type);
			if(coreInterface != null) {
				String prev = data.put(coreInterface, type.getQualifiedName().toString());
				// Check if there already exist an implementation for this interface
				if(prev != null) {
					processingEnv.getMessager().printMessage(Kind.ERROR,"Multiple Core implementations for the same"
							+ " core interface are not allowed");
					// do not process this coreinterface
					data.remove(coreInterface);					
				}
			}

		}

		final String metadataPath = "META-INF/foundationj/coreImpl/";

		// Write the files
		try {
			processingEnv.getMessager().printMessage(Kind.NOTE,"Writing "+metadataPath);
			Filer filer = processingEnv.getFiler();
			for(Entry<String, String> kv : data.entrySet()) {				
				FileObject f = filer.createResource(StandardLocation.CLASS_OUTPUT, "", metadataPath+kv.getKey());
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(f.openOutputStream(), "UTF-8"));
				pw.println(kv.getValue());
				pw.close();
			}
		} catch (IOException x) {
			processingEnv.getMessager().printMessage(Kind.ERROR,"Failed to write service definition files: "+x);
		}


		return false;
	}





	/**
	 * Perform any required checks and throw compilation errors if necessary
	 * @param type
	 * @return {@code true} if no errors, {@code false otherwise}
	 */
	protected String contractRespected(TypeElement type) {

		//--------------------------------------------------------------------//
		// First make sure that the annotation is on a concrete class
		//--------------------------------------------------------------------//

		ProcessorsUtils.checkConcrete(CoreImpl.class, type, processingEnv);


		//--------------------------------------------------------------------//
		// Now, get all the implemented interfaces and check which one is core
		//--------------------------------------------------------------------//

		Set<TypeElement> cores = ProcessorsUtils.findAllInterfacesWithAnnotation(Core.class, type, processingEnv);

		if(cores.isEmpty()) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "@CoreImpl must implement a @Core interface",type);
			return null;
		}
		else if(cores.size()>1) {
			processingEnv.getMessager().printMessage(Kind.ERROR, "Only one @Core interface can be implemented at once",type);
			return null;
		}


		return cores.iterator().next().toString();

	};



}
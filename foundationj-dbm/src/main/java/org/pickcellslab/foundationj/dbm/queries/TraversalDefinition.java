package org.pickcellslab.foundationj.dbm.queries;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.EvaluationContainerBuilder;
import org.pickcellslab.foundationj.datamodel.tools.EvaluationContainerInit;
import org.pickcellslab.foundationj.datamodel.tools.ExcludeLinksBased;
import org.pickcellslab.foundationj.datamodel.tools.FromDepth;
import org.pickcellslab.foundationj.datamodel.tools.IncludeLinksBased;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.LinkEvaluation;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints.LinksLogic;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.queries.builders.LastLinkEvaluation;
import org.pickcellslab.foundationj.dbm.queries.builders.RootDefinition;
import org.pickcellslab.foundationj.dbm.queries.builders.StartWith;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/**
 * This object stores the rules to navigate the data property graph.
 * It defines a root to start the search as well as link types and nodes that
 * can be traversed or that are excluded.
 * 
 * @author Guillaume
 *
 */
@Mapping(id = "TraversalDefinition")
public class TraversalDefinition {

	/**
	 * Determines if nodes or links can be traversed more than once during
	 * the Traversal.
	 */
	public enum TraversalPolicy{
		NODES_ONCE,
		LINKS_ONCE,
		NONE
	}
	
	// Root definition
	private final boolean usesIndexedProperty;
	private final String rootDeclaredType;
	@Supported
	private final ExplicitPredicate<? super NodeItem> rootPredicate;
	private final AKey<?> rootKey;
	
	@Supported
	private final Object rootValue;

	private TraversalPolicy policy = TraversalPolicy.NODES_ONCE;

	// Default Evaluation
	private final Decision defaultDecision;


	// Evaluations on Links
	private final boolean noLinkConstraints;
	private final LinksLogic linkLogic;
	private final Map<String, Direction> directions;
	
	private final LinkEvaluation targetLinksFilter;

	// Evaluation on Nodes
	private final boolean noNodeConstraint;
	
	@Supported
	private final Map<ExplicitPredicate<? super NodeItem>, Decision> nodeFilters;


	// Depth restrictions
	private final int minDepth;
	private final int maxDepth;


	private final String desc;


	
	



	private TraversalDefinition(TraversalBuilder builder){
		this.rootDeclaredType = builder.clazz;
		this.rootPredicate = builder.rootPredicate;
		this.rootKey = builder.key;
		this.rootValue = builder.value;
		this.directions = builder.typeDirections;
		this.nodeFilters = builder.filters;
		this.defaultDecision = builder.defaultEvaluation;
		this.targetLinksFilter = builder.finalLink;
		this.noLinkConstraints = builder.noLinkConstraints;
		this.linkLogic = builder.logic;
		this.noNodeConstraint = builder.noNodeConstraint;
		this.minDepth = builder.minDepth;
		this.maxDepth = builder.maxDepth;
		this.desc = builder.desc;
		this.usesIndexedProperty = builder.usesIndexedProperty;
	}
	
	private TraversalDefinition(TraversalDefinition other, ExplicitPredicate predicate){
		this.rootDeclaredType = other.rootDeclaredType;
		this.rootPredicate = other.rootPredicate.and(predicate);
		this.rootKey = other.rootKey;
		this.rootValue = other.rootValue;
		this.directions = other.directions;
		this.nodeFilters = other.nodeFilters;
		this.defaultDecision = other.defaultDecision;
		this.targetLinksFilter = other.targetLinksFilter;
		this.noLinkConstraints = other.noLinkConstraints;
		this.linkLogic = other.linkLogic;
		this.noNodeConstraint = other.noNodeConstraint;
		this.minDepth = other.minDepth;
		this.maxDepth = other.maxDepth;
		this.desc = other.desc;
		this.usesIndexedProperty = other.usesIndexedProperty;
	}
	

	
	public TraversalDefinition subsetRoots(ExplicitPredicate<? super NodeItem> predicate) {
		Objects.requireNonNull(predicate);
		return new TraversalDefinition(this, predicate);
	}
	

	public TraversalPolicy traversalPolicy(){
		return policy;
	}


	public TraversalDefinition setTraversalPolicy(TraversalPolicy policy){
		assert policy != null;
		this.policy = policy;
		return this;
	}


	/**
	 * @return The class of the root nodes
	 */
	public String getStartNodeClass(){
		return rootDeclaredType;
	}


	/**
	 * @return true if the roots can be identified by a specific value for a specific property
	 */
	public boolean usesIndexedProperty(){
		return usesIndexedProperty;	
	}

	/**
	 * @return an {@link ExplicitPredicate} used to filter the root nodes or null if
	 * this description should use an indexed property.
	 * @see #usesIndexedProperty()
	 */
	public ExplicitPredicate<? super NodeItem> rootPredicate(){
		return rootPredicate;
	}

	/**
	 * @return The AKey defining the property to look for if {@link #usesIndexedProperty()} return true, null otherwise
	 */
	public AKey<?> getStartKey(){
		return rootKey;
	}

	/**
	 * @return The value of the property to look for if {@link #usesIndexedProperty()} return true, null otherwise
	 */
	public Object getStartValue(){
		return rootValue;
	}

	/**
	 * @return a flag indicating whether all the links encountered should be included in the traversal
	 */
	public boolean traverseAllLinks(){
		return noLinkConstraints;
	}

	/**
	 * @return a flag indicating whether all the nodes encountered should be included in the traversal
	 */
	public boolean allNodesIncluded(){
		return noNodeConstraint;
	}


	public int minDepth(){
		return minDepth;
	}

	public int maxDepth(){
		return maxDepth;
	}


	public LinksLogic logicOnLinks() {
		return linkLogic;
	}


	/**
	 * @return the types of the links to be included in the traversal and the associated Direction restriction.
	 * Note that this method will return an empty map if {@link #traverseAllLinks()} is true or if 
	 * {@link #getExcluded()} is not empty.
	 */
	public final Map<String, Direction> getLinkConstraints() {
		return directions;
	}



	/**
	 * @return An evaluation on links which maybe target of the traversal (maybe null)
	 */
	public final LinkEvaluation getLinkTargetEvaluation() {
		return targetLinksFilter;
	}



	/**
	 * @return Filters and the associated Decision in charge of determining how the encountered node should be regarded during the traversal
	 */
	public final Map<ExplicitPredicate<? super NodeItem>, Decision> getEvaluations() {
		return nodeFilters;
	}


	public Decision defaultDecision() {
		return defaultDecision;
	}


	/**
	 * @return An explicit description of This traversalDescription
	 */
	public String description(){
		return desc; 
	}
	
	/**
	 * Converts this {@link TraversalDefinition} into a {@link TraverserConstraints}.
	 * @param targetLinks true if the target of the traversal are links false otherwise
	 * @return A TraverserConstraints built based on this {@link TraversalDefinition}
	 */
	public TraverserConstraints asConstraints(boolean targetLinks) {
		
		int minD = minDepth;
		if(targetLinks && minDepth == maxDepth)
			minD--;
		
		
		// Depth constraints
		final LinkDecisions<TraverserConstraints> ld = Traversers.newConstraints().fromDepth(minD).toDepth(maxDepth);
		EvaluationContainerInit<TraverserConstraints> eci = null;
		
		
		// Constraints on links
		if(this.noLinkConstraints)
			eci = ld.traverseAllLinks();
		else if(linkLogic == LinksLogic.INCLUSION){
			Iterator<Entry<String, Direction>> it = directions.entrySet().iterator();
			Entry<String, Direction> first = it.next();
			IncludeLinksBased<TraverserConstraints> ilb = ld.traverseLink(first.getKey(), first.getValue());
			while(it.hasNext()){
				Entry<String, Direction> next = it.next();
				ilb = ilb.and(next.getKey(), next.getValue());
			}	
			eci = ilb;
		}
		else{
			Iterator<Entry<String, Direction>> it = directions.entrySet().iterator();
			Entry<String, Direction> first = it.next();
			ExcludeLinksBased<TraverserConstraints> elb = ld.doNotTraverseLink(first.getKey(), first.getValue());
			while(it.hasNext()){
				Entry<String, Direction> next = it.next();
				elb = elb.asWellAs(next.getKey(), next.getValue());
			}	
			eci = elb;
		}
		
		// Constraints on nodes
		TraverserConstraints tc = null;
		if(this.allNodesIncluded())
			tc = eci.includeAllNodes();
		else {
			EvaluationContainerBuilder<TraverserConstraints> ecb = null;
			Iterator<Entry<ExplicitPredicate<? super NodeItem>, Decision>> it = nodeFilters.entrySet().iterator();
			Entry<ExplicitPredicate<? super NodeItem>, Decision> first = it.next();
			ecb = eci.withEvaluations(first.getKey(), first.getValue(), defaultDecision);
			while(it.hasNext()){
				Entry<ExplicitPredicate<? super NodeItem>, Decision> next = it.next();
				ecb = eci.withEvaluations(next.getKey(), next.getValue(), defaultDecision);
			}
			tc = ecb.done();
		}
			
				
		return tc;
	}
	
	

	/**
	 * Initiates the building process to create a {@link TraversalDefinition}
	 * @return A dedicated builder
	 */
	public static StartWith<RootDefinition<FromDepth<ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>>,NodeItem>,String> newDefinition(){
		return new TraversalBuilder();
	}	
	
	
	
	
	/**
	 * 
	 * A Builder for {@link LastLinkEvaluation<TraversalDefinition>}
	 * 
	 * @author Guillaume
	 *
	 */
	public static class TraversalBuilder
	implements
	StartWith<RootDefinition<FromDepth<ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>>,NodeItem>,String>,
	RootDefinition<FromDepth<ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>>,NodeItem>,
	FromDepth<ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>>,
	ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>,
	LinkDecisions<LastLinkEvaluation<TraversalDefinition>>,
	IncludeLinksBased<LastLinkEvaluation<TraversalDefinition>>,
	ExcludeLinksBased<LastLinkEvaluation<TraversalDefinition>>,
	EvaluationContainerBuilder<LastLinkEvaluation<TraversalDefinition>>,
	LastLinkEvaluation<TraversalDefinition>

	{

		private Decision defaultEvaluation = Decision.INCLUDE_AND_CONTINUE;

		private LinksLogic logic;
		private final Map<String, Direction> typeDirections = new HashMap<>();
		private LinkEvaluation finalLink;


		private Map<ExplicitPredicate<? super NodeItem>, Decision> filters = new HashMap<>();
		private boolean noLinkConstraints;
		private boolean noNodeConstraint;
		private int minDepth = 0;
		private int maxDepth = Integer.MAX_VALUE;
		private boolean usesIndexedProperty;
		private String clazz;
		private ExplicitPredicate<? super NodeItem> rootPredicate;
		private AKey<?> key;
		private Object value;

		private String desc;



		TraversalBuilder(){/*Package protected*/}




		/**
		 * Sets the maximum depth up to which the resolution will search for linked Nodes
		 */
		@Override
		public ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>> fromDepth(int min){    
			minDepth = min;
			desc = desc+"\n from depth "+min;
			return this;
		}

		/**
		 * Sets the maximum depth up to which the resolution will search for linked Nodes
		 */
		@Override
		public LinkDecisions<LastLinkEvaluation<TraversalDefinition>> toDepth(int max){    
			maxDepth = max;
			desc = desc+"\n to depth "+max;
			return this;
		}




		@Override
		public ExcludeLinksBased<LastLinkEvaluation<TraversalDefinition>> asWellAs(String type, Direction d) {
			and(type,d);
			return this;
		}


		@Override
		public IncludeLinksBased<LastLinkEvaluation<TraversalDefinition>> and(String type, Direction d) {
			Objects.requireNonNull(type, "The link type cannot be null");
			Objects.requireNonNull(d, "The Direction cannot be null");
			typeDirections.put(type,d);
			desc+=" and "+type+" "+d;
			return this;
		}


		@Override
		public IncludeLinksBased<LastLinkEvaluation<TraversalDefinition>> traverseLink(String type, Direction d) {
			Objects.requireNonNull(type, "The link type cannot be null");
			Objects.requireNonNull(d, "The Direction cannot be null");
			typeDirections.put(type,d);
			logic = LinksLogic.INCLUSION;
			desc+=" traverse "+type+" "+d;
			return this;
		}




		@Override
		public ExcludeLinksBased<LastLinkEvaluation<TraversalDefinition>> doNotTraverseLink(String type, Direction d) {
			Objects.requireNonNull(type, "The link type cannot be null");
			Objects.requireNonNull(d, "The Direction cannot be null");
			typeDirections.put(type,d);
			logic = LinksLogic.EXCLUSION;
			desc+=" traverse all except "+type+" "+d;
			return this;
		}


		
		@Override
		public EvaluationContainerInit<LastLinkEvaluation<TraversalDefinition>> acceptLinks(
				BiFunction<Link, Direction, Boolean> biPredicate) {
			throw new RuntimeException("Not implemented yet");
		}

		
		
		
		



		@Override
		public LastLinkEvaluation<TraversalDefinition> includeAllNodes() {
			this.noNodeConstraint = true;
			desc+=" include all nodes";
			return this;
		}


		@Override
		public EvaluationContainerBuilder<LastLinkEvaluation<TraversalDefinition>> withEvaluations(
				ExplicitPredicate<? super NodeItem> test, Decision ifYes, Decision defaultDecision) {
			MappingUtils.exceptionIfNullOrInvalid(test);
			filters.put(test, ifYes); 
			this.defaultEvaluation = defaultDecision;
			desc += " "+ifYes+" "+"nodes ["+test.description()+"]";
			return this;
		}


		@Override
		public EvaluationContainerInit<LastLinkEvaluation<TraversalDefinition>> traverseAllLinks() {
			this.noLinkConstraints = true;
			desc+=" traverse all links";
			return this;
		}


		@Override
		public <E> FromDepth<ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>> having(AKey<E> k, E value) {
			Objects.requireNonNull(k,"The given key cannot be null");
			this.key = k;
			this.value = value;
			this.usesIndexedProperty = true;
			desc+=" having "+k.name+" = "+value;
			return this;
		}


		@Override
		public FromDepth<ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>> acceptedBy(ExplicitPredicate<? super NodeItem> test) {
			MappingUtils.exceptionIfNullOrInvalid(test);
			this.rootPredicate = test;
			desc+=" accepted by ["+test.description()+"]";
			return this;
		}




		@Override
		public RootDefinition<FromDepth<ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>>>, NodeItem> startFrom(String declaredType) {
			Objects.requireNonNull(declaredType,"The given type cannot be null");
			this.clazz = declaredType;
			desc = "Traversal: start from "+declaredType;			
			return this;
		}



		/**
		 * @return The TraversalDescription with all the options set previously. Note that if neither
		 * {@link #traverseLink(String, Direction)} nor {@link #doNotTraverseLink(String, Direction)} have been called,
		 * all the links will be included by default
		 */
		@Override
		public TraversalDefinition build(){
			if(!noNodeConstraint)
				desc+= "(Default -> "+defaultEvaluation+")";
			return new TraversalDefinition(this);
		}




		@Override
		public LastLinkEvaluation<TraversalDefinition> withOneEvaluation(ExplicitPredicate<? super NodeItem> explicitPredicate,
				Decision ifYes, Decision ifNot) {
			withEvaluations(explicitPredicate, ifYes, ifNot);
			return this;
		}




		@Override
		public EvaluationContainerBuilder<LastLinkEvaluation<TraversalDefinition>> addEvaluation(
				ExplicitPredicate<? super NodeItem> explicitPredicate, Decision d) {
			withEvaluations(explicitPredicate,d, this.defaultEvaluation);
			return this;
		}




		@Override
		public LastLinkEvaluation<TraversalDefinition> lastEvaluation(ExplicitPredicate<? super NodeItem> explicitPredicate, Decision d) {
			withEvaluations(explicitPredicate,d, this.defaultEvaluation);
			return this;
		}

	



		@Override
		public LinkDecisions<LastLinkEvaluation<TraversalDefinition>> toMaxDepth() {
			maxDepth = Integer.MAX_VALUE;
			desc = desc+"\n to max depth";
			return this;
		}




		@Override
		public ToDepth<LinkDecisions<LastLinkEvaluation<TraversalDefinition>>> fromMinDepth() {
			return fromDepth(0);
		}







		@Override
		public LastLinkEvaluation<TraversalDefinition> done() {
			return this;
		}




		@Override
		public TraversalDefinition finallyEvaluateLastLink(String type, ExplicitPredicate<? super Link> p,
				Decision ifYes, Decision ifNot) {
			Objects.requireNonNull(p,"predicate is null");
			Objects.requireNonNull(ifYes,"ifYes is null");
			Objects.requireNonNull(ifNot,"ifNot is null");
			desc+="(Filter target links : "+p.description()+")";
			finalLink = new LinkEvaluation(type, p, ifNot, ifNot);
			return new TraversalDefinition(this);
		}



	}



}


package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.queries.IllegalDeletionException;

/** 
 *  A {@link MetaReadable} which corresponds to a specific {@link AKey} in the data graph
 * 
 * @author Guillaume Blin
 *
 * @param <E>
 */
public interface MetaKey<E> extends MetaReadable<E> {

	/**
	 * @return The type of data the {@link AKey} objects points to. Same as {@link AKey#type()}
	 */
	@Override
	Class<E> dataClass();

	/**
	 * @return The name of the {@link AKey} object described by this MetaKey, which as a consequence is also the name
	 * of this {@link MetaKey}
	 */
	@Override
	String name();

	/**
	 * @return the length of the array the {@link AKey} object points at or -1 if the object is not an array
	 */
	int length();


	/**
	 * @return {@code true} if the {@link AKey} object is not present in all the {@link DataItem}
	 * instance belonging to the class this MetaKey belongs to, {@code false} otherwise
	 */
	boolean isOptional();

	/**
	 * @return The {@link AKey} object described by this {@link MetaKey}
	 */
	AKey<E> toAKey();
	
	
	void setIndicesNames(String[] names);


	/**
	 * Deletes the attributes this {@link MetaKey} represents in the actual datagraph.
	 * @param access
	 * @return The number of deletions
	 */
	int deleteThisAttribute(DataAccess access) throws DataAccessException, IllegalDeletionException;
	
	
	/**
	 * @param access
	 * @param predicate An {@link ExplicitPredicate} performing test on an {@link Updatable} to determine if the attribute should be deleted or not.
	 * Returning {@code true} will delete the attribute, 
	 * @return
	 * @throws DataAccessException
	 * @throws IllegalDeletionException
	 */
	int deleteThisAttributeUsingAPredicate(DataAccess access, ExplicitPredicate<DataItem> predicate) throws DataAccessException, IllegalDeletionException;
	
	
}

package org.pickcellslab.foundationj.dbm.meta;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.queries.ReadAnyBuilder;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.DeletionConfig;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadItemsBuilder;



/**
 * {@link MetaQueryable} are elements of the {@link MetaModel} which represent {@link DataItem}s in the data graph.
 * They conveniently implement {@link DataPointer} and provide additional methods to initiate customisable queries.
 */
@Tag(creator = "FoundationJ", description = "Tags nodes and links belonging to the MetaModel", tag = "MetaQueryable")
public interface MetaQueryable extends MetaModel, MetaKeyContainer, DataPointer{

	/**
	 * @return The {@link DataItem#declaredType()} that this {@link MetaQueryable} represents
	 */
	public String itemDeclaredType();
	
	/**
	 * @return A {@link Stream} of {@link MetaReadable} available of this {@link MetaQueryable}
	 */
	public Stream<MetaReadable<?>> getReadables();
	
	public Stream<MetaFilter> filters();
	
	public Stream<MetaTraversal> traversals();

	public DeletionConfig<? extends WritableDataItem> initiateDeletion(DataAccess access);

	public AccessChoice<? extends Updatable, QueryRunner<Void>> initiateUpdate(DataAccess access, Updater updater);

	public ReadItemsBuilder<? extends WritableDataItem> initiateRead(DataAccess access, long limit);
	
	public ReadAnyBuilder<? extends WritableDataItem> initiateFindOne(DataAccess access);

}

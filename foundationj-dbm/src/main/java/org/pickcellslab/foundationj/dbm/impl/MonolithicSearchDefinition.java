package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.FilteredIterator;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

class MonolithicSearchDefinition<V,E,T> {

	
	private final String description;
	private final DatabaseAccessor<V,E,T> accessor;

	
	
	MonolithicSearchDefinition(DatabaseAccessor<V,E,T> accessor, String description){			
		this.accessor = accessor;
		this.description = description;
	}





	public DatabaseAccessor<V,E,T> accessor(){
		return accessor;
	}


	/**
	 * @return A description of this search
	 */
	public String description(){
		return description;
	}






	public static class Builder<V,E,T> {


		//Tags test
		private	ExplicitPredicate<String[]> tagTester;
		//Filter
		private ExplicitPredicate<? super T> filter;
		//Traversals
		private TraversalDefinition traversal;
		private SuppliedTraversalOperation trOps;
		private String description;
		private final TargetType<V,E,T> target;
		



		public Builder(TargetType<V,E,T> target){
			Objects.requireNonNull(target);
			this.target = target;
		}


		
		

		/**
		 * Adds a filter on the target which tests the tags of the targets of the search 
		 * Note that if the targets are links, the provided predicate will be ignored (links have no tags)
		 * @param tagTester
		 * @return The updated Builder
		 */
		@Deprecated
		public Builder<V,E,T> setTagTester(ExplicitPredicate<String[]> tagTester){
			if(this.tagTester != null)
				throw new IllegalStateException("The Tag tester on target was already set for this search");
			this.tagTester = tagTester;
			return this;
		}


		/**
		 * Convenience method to create a search using one {@link TraversalDefinition} only. Note that
		 * after this call, it will be illegal to set other TraversalDescription for this search
		 * @param traversal null not permitted
		 * @return The updated Builder
		 * @throws IllegalStateException if TraversalDescription were already set in this search
		 */
		public  Builder<V,E,T> setTraversal(TraversalDefinition traversal) throws IllegalStateException {
			if(trOps!=null)
				throw new IllegalStateException("Traversals were already set for this search");
			this.traversal = traversal;
			return this;
		}


		public  Builder<V,E,T> setTraversalOperation(SuppliedTraversalOperation operations) throws IllegalStateException {
			Objects.requireNonNull(operations," Operations cannot be null!");
			if(trOps!=null)
				throw new IllegalStateException("Traversals were already set for this search");
			this.trOps = operations;
			return this;
		}


		/**
		 * @param f An {@link ExplicitPredicate} to filter the targets of the database search
		 * @return The updated Builder
		 */
		public  Builder<V,E,T> setFilter(ExplicitPredicate<? super T> f) {
			if(this.filter != null)
				throw new IllegalStateException("The filter on target was already set for this search");
			this.filter = f;
			return this;
		}


		public MonolithicSearchDefinition<V,E,T> build(){

			//Create the description of the search and the Iterator
						
			description = "Search for "+target.toString();
			DatabaseAccessor<V,E,T> it = null;
			if(trOps != null){
				description += " using traversal Operation: " + trOps.description();
				it = target.accessor(trOps);
			}
			else if(traversal != null){
				description += " using traversal: " + traversal.description();
				it = target.accessor(traversal);
			}
			else
				it = target.accessor();
			
			if(filter != null){
				description += " with filter : "+filter.description();
				final DatabaseAccessor<V,E,T> accessor = it;
				it = new DatabaseAccessor<V,E,T>(){					
					@Override
					public Iterator<T> iterator(DatabaseAccess<V, E> access) {
						return new FilteredIterator<T>(accessor.iterator(access), filter);
					}					
				};
			}


			return new MonolithicSearchDefinition<>(it, description);
		}


	}




}

package org.pickcellslab.foundationj.services;

import java.util.logging.Level;

public interface ProgressLogListener {

	
	public void log(TaskProgress<?> source, Level level, String msg);
	
}

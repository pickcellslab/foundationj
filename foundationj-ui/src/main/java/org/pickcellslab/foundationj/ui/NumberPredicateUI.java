package org.pickcellslab.foundationj.ui;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import java.awt.Component;
import java.text.ParseException;
import java.util.LinkedList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.ValuePredicate;
import org.pickcellslab.foundationj.ui.parsers.ExplicitPredicateParser;
import org.pickcellslab.foundationj.ui.parsers.TextFieldParser;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

import com.fathzer.soft.javaluator.StaticVariableSet;

@SuppressWarnings("serial")
public class NumberPredicateUI<E extends Number> extends ValidityTogglePanel {

		
	//EvaluatorUI
	private final EvaluatorUI<ExplicitPredicateParser<E>, ExplicitPredicate<E>> evaluation;
	
	
	//A list to keep track of expression tokens that were inserted
			LinkedList<String> expression = new LinkedList<>();
	

	public NumberPredicateUI(Class<E> type) {

		//Defining variables important for the creation of the expression
		//check if the target is an item or a link


		//The map storing the variables must be the same for the display and the evaluator
		//TODO create an abstract subclass of abstract evaluator in order to ensure this
		StaticVariableSet<ExplicitPredicate<E>> var = new StaticVariableSet<>();

		//Create a parser for an attribute filter to inject into the EvaluatorUI
		ExplicitPredicateParser<E> parser = new ExplicitPredicateParser<E>(var);

		//Create the display to show the expression
		evaluation = new EvaluatorUI<>(parser,var);

		


		//Controllers
		//------------------------------------------------------------------------------------------
		Component display = evaluation.getDisplay();
		//Component display = new JPanel();


		JButton btnLeftP = new JButton("(");
		btnLeftP.addActionListener(l->{
			evaluation.insert("{", EvaluatorUI.parenthesis);
			expression.add("{");
			this.fireNextValidityChanged();
		});

		JButton btnRightP = new JButton(")");
		btnRightP.addActionListener(l->{
			evaluation.insert("}", EvaluatorUI.parenthesis);
			expression.add("}");
			this.fireNextValidityChanged();
		});

		JButton btnAnd = new JButton("AND");
		btnAnd.addActionListener(l->{
			evaluation.insert(" AND ", EvaluatorUI.operator);
			expression.add(" AND ");
			this.fireNextValidityChanged();
		});

		JButton btnOr = new JButton("OR");
		btnOr.addActionListener(l->{
			evaluation.insert(" OR ", EvaluatorUI.operator);
			expression.add(" OR ");
		});

		JButton btnXor = new JButton(" XOR ");
		btnXor.addActionListener(l->{
			evaluation.insert(" XOR ", EvaluatorUI.operator);
			expression.add(" XOR ");
			this.fireNextValidityChanged();
		});

		JButton btnNot = new JButton(" NOT ");
		btnNot.addActionListener(l->{
			evaluation.insert(" NOT ", EvaluatorUI.operator);
			expression.add(" NOT ");
			this.fireNextValidityChanged();
		});

		JButton btnC = new JButton("C");
		btnC.addActionListener(l->{
			evaluation.delete(expression.pollLast());
			this.fireNextValidityChanged();
		});

		JComboBox<Op.Logical> opBox = new JComboBox<>(Op.Logical.values());

		JFormattedTextField entryBox = new JFormattedTextField();
		entryBox.setText("1");

		
		
		JButton btnIns = new JButton("INS");
		btnIns.addActionListener(l->{
			
			//Try to parse the number in the textfield
			E right;
			try {
				right = TextFieldParser.parse(entryBox.getText(), type);
			} catch (ParseException e) {
				JOptionPane.showMessageDialog(this,"Options are not well defined, please enter an integer in the text field");
				return;
			}
			
			Op.Logical op = (Op.Logical) opBox.getSelectedItem();
			ExplicitPredicate<E> current = new ValuePredicate<>(op, right);
			expression.add(current.description());
			evaluation.insert(current.description(), current, EvaluatorUI.funct);			
			this.fireNextValidityChanged();
		});




		//Layout
		//-------------------------------------------------------------------------------------------------



		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(display, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
												.addComponent(opBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(btnNot, GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE))
												.addGap(12)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(btnAnd)
																.addPreferredGap(ComponentPlacement.UNRELATED)
																.addComponent(btnOr))
																.addComponent(entryBox, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(ComponentPlacement.UNRELATED)
																.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																		.addComponent(btnIns, GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
																		.addComponent(btnXor, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
																				.addGroup(groupLayout.createSequentialGroup()
																						.addComponent(btnLeftP, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(ComponentPlacement.RELATED)
																						.addComponent(btnRightP))
																						.addComponent(btnC, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
																						.addContainerGap(67, Short.MAX_VALUE))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(display, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNot)
								.addComponent(btnC)
								.addComponent(btnAnd)
								.addComponent(btnOr)
								.addComponent(btnXor))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(opBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(btnRightP)
										.addComponent(btnIns)
										.addComponent(btnLeftP)
										.addComponent(entryBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnLeftP, btnRightP});
		groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnNot, btnAnd, btnOr, btnXor, btnC});
		setLayout(groupLayout);
		// TODO Auto-generated constructor stub
	}




	@Override
	public boolean validity() {
		return !expression.isEmpty() && evaluation.isValid();
	}
	
	
	public ExplicitPredicate<E> getPredicate(){
		return evaluation.getFunction();
	}

	

}

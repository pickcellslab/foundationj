package org.pickcellslab.foundationj.ui.wizard;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import javax.swing.DefaultListModel;
import javax.swing.DefaultRowSorter;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.RowFilter;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.swingx.JXList;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;


@SuppressWarnings("serial")
public class PluggableSelection<T> extends PluggableWizardNode {

	private final JXList list;
	private final DefaultListModel<T> model;
	private JTextField textField;

	public PluggableSelection(String title, Comparator<? super T>[] comp) {
		this(null,title,comp);
	}


	@SuppressWarnings("rawtypes")
	public PluggableSelection(WizardNode parent, String title, Comparator<? super T>[] comp) {
		super(parent);

		Objects.requireNonNull(comp,"Comparators are null");
		if(comp.length == 0)
			throw new IllegalArgumentException("No comparator provided");


		//Setup controllers

		//List and model
		list = new JXList(true);
		model = new DefaultListModel<>();
		list.setModel(model);

		//Sorting
		JComboBox<Comparator<? super T>> compBox = new JComboBox<>(comp);

		compBox.addActionListener(l->{
			list.setComparator(comp[compBox.getSelectedIndex()]);
			list.toggleSortOrder();
			list.toggleSortOrder();
			((DefaultRowSorter) list.getRowSorter()).sort();	
		});
		list.setComparator(comp[0]);


		//Filtering
		textField = new JTextField();		
		textField.setColumns(5);
		textField.addActionListener(l->{
			String s = textField.getText();
			if(s.isEmpty())
				list.setRowFilter(null);
			else{
				list.setRowFilter(RowFilter.regexFilter(s));
			}
		});






		//Layout
		
		this.setLayout(new VerticalFlowLayout());
		this.add(new GroupPanel(10,  new JLabel("Filter"), textField,  new JLabel("Sort "), compBox));

		this.add(new JScrollPane(list));
		
		setBorder(new TitledBorder(null, title, TitledBorder.LEADING, TitledBorder.TOP, null, null));
		

	}

	public void setBorderColor(Color color){
		((TitledBorder)getBorder()).setTitleColor(color);
		repaint();
	}





	public void setRenderer(ListCellRenderer<? super T> renderer){
		list.setCellRenderer(renderer);
	}

	public void setSelectionMode(int mode){
		list.setSelectionMode(mode);
	}
	
	
	public void setSelected(T data) {
		list.setSelectedValue(data, true);
	}

	public void add(T item){
		if(!model.contains(item)){
			model.addElement(item);
			//list.toggleSortOrder();
			list.toggleSortOrder();
		}
	}

	public void addAll(Collection<T> items){
		items.forEach(i -> add(i));
	}

	public void remove(T item){
		model.removeElement(item);
	}

	public void removeAllItems(){
		model.removeAllElements();
	}


	@SuppressWarnings("unchecked")
	public T getSelectedItem(){
		return (T) list.getSelectedValue();
	}


	@SuppressWarnings("unchecked")
	public List<T> getSelectedItems(){
		List<T> values = new ArrayList<>();
		for(Object o : list.getSelectedValues())
			values.add((T)o);
		return values;
	}

		
	
	public int numElements(){
		return model.size();
	}
	
	
	public T getElementAt(int i){
		return model.getElementAt(i);
	}
	
	
	public List<T> getAllItems(){
		List<T> all = new ArrayList<T>(model.size());
		for(int i = 0; i<model.size(); i++)
			all.add(model.getElementAt(i));
		return all;
	}

	@Override
	public boolean validity() {
		return true;
	}


	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		Comparator<String> c1 = (s1,s2)->s1.compareTo(s2);
		Comparator<String> c2 = c1.reversed();

		Comparator<String>[] comps = new Comparator[]{c1,c2};

		PluggableSelection<String> selection = new PluggableSelection<>("Test",comps);

		selection.add("Test 1");
		selection.add("Miam");
		selection.add("Fusee");
		selection.add("Avion");
		selection.add("Test 02");
		selection.add("To Be sorted");
		selection.add("Selected");

		JFrame f = new JFrame("Test");
		f.setContentPane(selection);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}


	public void removeSelectionListener(ListSelectionListener l){
		list.removeListSelectionListener(l);
	}
	
	public void addSelectionListener(ListSelectionListener l){
		list.addListSelectionListener(l);
	}


	public void addFilter(Predicate<? super T> predicate) {
		RowFilter rf = list.getRowFilter();
		if(rf!=null){
			List<RowFilter> rfList = new ArrayList<>();
			rfList.add(new RowPredicate(predicate));
			rf.andFilter((List)rfList);
		}else{
			list.setRowFilter(new RowPredicate(predicate));
		}
	}
	
	public void removeFilters() {			
		String s = textField.getText();
		if(s.isEmpty())
			list.setRowFilter(null);
		else{
			list.setRowFilter(RowFilter.regexFilter(s));
		}		
	}


	private class RowPredicate extends RowFilter<DefaultListModel<T>,Integer>{

		private Predicate<? super T> predicate;


		RowPredicate(Predicate<? super T> predicate){
			this.predicate = predicate;
		}
		

		@Override
		public boolean include(javax.swing.RowFilter.Entry<? extends DefaultListModel<T>, ? extends Integer> entry) {			
			return predicate.test(entry.getModel().getElementAt(entry.getIdentifier()));
		}
		
	}


	

	
	

}

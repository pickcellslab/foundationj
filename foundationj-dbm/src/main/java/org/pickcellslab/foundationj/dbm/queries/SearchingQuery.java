package org.pickcellslab.foundationj.dbm.queries;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;

public interface SearchingQuery<V, E, R> extends Query<R> {

	public R createResult(DatabaseAccess<V,E> db) throws DataAccessException;
	
}

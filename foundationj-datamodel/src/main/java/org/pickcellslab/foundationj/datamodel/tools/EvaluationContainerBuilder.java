package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;

/** 
 * The last step of the stepwise builder pattern to define the 'rules' for navigating a graph. 
 * This step allows to add evaluations on nodes before building
 * the object holding the 'rules' for navigating the graph.
 *
 * @param <T> The type of the next builder
 */
public interface EvaluationContainerBuilder<T> {

	public EvaluationContainerBuilder<T> addEvaluation(ExplicitPredicate<? super NodeItem> explicitPredicate, Decision d);
	
	/**
	 * Adds an evaluations on nodes before building the object holding the 'rules' for navigating the graph.
	 * @param explicitPredicate An {@link ExplicitPredicate} to categorise nodes encountered during the traversal
	 * @param d The {@link Decision} if {@code explicitPredicate} returns {@code true}. Please note that the Decision taken if
	 * {@code explicitPredicate} was defined in previous steps.
	 * @return An object holding the rules for navigating the graph.
	 */
	public T lastEvaluation(ExplicitPredicate<? super NodeItem> explicitPredicate, Decision d);
	
		
	/**
	 * Builds the graph navigation rules. 
	 * @return An object holding the rules for navigating the graph.
	 */
	public T done();
	
}

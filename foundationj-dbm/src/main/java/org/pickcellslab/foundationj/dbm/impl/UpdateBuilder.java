package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.actions.VoidMechanism;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

class UpdateBuilder<V,E, U extends Updatable> implements AccessChoice<U,QueryRunner<Void>>{

	
	private final TargetType<V, E, U> target;
	private final Updater updater;
	private final UpdateInfosImpl ui;

	public UpdateBuilder(TargetType<V,E,U> target, UpdateInfosImpl ui, Updater updater) {
		this.target = target;
		this.updater = updater;
		this.ui = ui;
	}
	
	
	@Override
	public QueryRunner<Void> getAll() {
		return new MonolithicQBuilder<>(
				target,
				new UpdateAction(),null
				);
	}

	@Override
	public QueryRunner<Void> useFilter(ExplicitPredicate<? super U> test) {
		return new MonolithicQBuilder<>(
				target,
				new UpdateAction(),null
				).useFilter(test);
	}

	@Override
	public QueryRunner<Void> useTraversal(TraversalDefinition definition) {
		assert definition != null;
		target.checkTraversalDefinition(definition);
		return new MonolithicQBuilder<>(
				target,
				new UpdateAction(),null
				).useTraversal(definition);
	}

	@Override
	public QueryRunner<Void> useTraversals(SuppliedTraversalOperation operations) {
		return new MonolithicQBuilder<>(
				target,
				new UpdateAction(),null
				).useTraversals(operations);
	}

	
	
	private class UpdateAction implements Action<Updatable,Void>{
		
		
		

		@Override
		public VoidMechanism<Updatable> createCoordinate(Object key) {
			return updater;
		}

		@Override
		public Set<Object> coordinates() {
			//TODO
			return null;
		}

		@Override
		public Void createOutput(Object key) throws IllegalArgumentException {
			try {
				ui.updateMetas(target.session());
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public String description() {
			return "Update Action";
		}
		
	}
	



	
	
	
}

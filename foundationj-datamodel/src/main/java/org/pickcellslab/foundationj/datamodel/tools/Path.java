package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;

/**
 * Root interface for <a href="https://en.wikipedia.org/wiki/Path_(graph_theory)"> paths </a>.
 *
 * @param <N> The type of the Nodes (Vertices)
 * @param <E> The type of the Links (Edges)
 */
public interface Path<N,E> {

		
	/**
	 * @return The first Node
	 */
	public N first();

	/**
	 * @return The Last Node
	 */
	public N last();
	
	/**
	 * @return The first Edge
	 */
	public E firstEdge();

	/**
	 * @return The last Edge
	 */
	public E lastEdge();
	
	/**
	 * @return An {@link Iterator} for the Nodes included in this path
	 */
	public Iterator<N> nodes();
		
	/**
	 * @return An {@link Iterator} for the Edges included in this path
	 */
	public Iterator<E> edges();

	/**
	 * @return The number of nodes
	 */
	public int nodesCount();
	
	/**
	 * @return The number of edges
	 */
	public int edgesCount();

	/*
	
	public boolean atLeastOneNode(Predicate<? super N> p);
	
	public boolean atLeastOneEdge(Predicate<? super E> p);
	
	public int nodes(Predicate<? super N> p);
	
	public int edges(Predicate<? super E> p);
	
	*/
	
}

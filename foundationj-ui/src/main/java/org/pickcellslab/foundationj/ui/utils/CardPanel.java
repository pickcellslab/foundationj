package org.pickcellslab.foundationj.ui.utils;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.CardLayout;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CardPanel<E,T extends Component> extends JPanel{
	
	private final Function<E,T> factory;
	private final Map<E,T> mapping = new HashMap<>();
	private final CardLayout layout;
	private E current;
	
	public CardPanel(Function<E,T> factory) {
		Objects.requireNonNull(factory);
		
		this.factory = factory;
		
		layout = new CardLayout();		
		setLayout(layout);
	}
	
	public E currentKey(){
		return current;
	}
	
	public T current(){
		return mapping.get(current);
	}
	
	public boolean show(E id){
		T t = mapping.get(id);
		if(null == t){
			t = factory.apply(id);
			if(null == t)
				return false;
			add(id.toString(),t);
			mapping.put(id, t);
		}
		current = id;
		layout.show(this, id.toString());
		return true;
	}
	
	
	
}

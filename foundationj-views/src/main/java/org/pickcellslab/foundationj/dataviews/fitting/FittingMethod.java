package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;

/**
 * Describes a method to fit some data
 * 
 * @author Guillaume Blin
 *
 * @param <O> The type of options required by this method
 *
 * @see Fitter
 * @see FittingFactory
 * 
 */
public interface FittingMethod<O> {
	
	/**
	 * @return A short Name of the Fitter
	 */
	public String getName();
	
	/**
	 * @return A description of the fitting methodology
	 */
	public String getDescription();
				
	/**
	 * @return The minimum number of dimensions required by the fitting process
	 */
	public int minNumberOfDimensions();
	
	/**
	 * @return The maximum number of dimensions handled
	 */
	public int maxNumberOfDimensions();
	
	/**
	 * Shows an input dialog to allow the user to create an appropriate {@link FitConfig} for this {@link FittingMethod}
	 * 
	 * @return An {@link Optional} holding an appropriate FittingDimenions object for the specified {@link BuildableFittingMethod}
	 * The Optional will be empty if the process was cancelled by the user.
	 */
	public Optional<FitConfig<O>> config();
	
}

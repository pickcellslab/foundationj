package org.pickcellslab.foundationj.bootstrap;

import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.behaviors.Caching;
import org.picocontainer.lifecycle.ReflectionLifecycleStrategy;
import org.picocontainer.monitors.LifecycleComponentMonitor;
import org.picocontainer.monitors.NullComponentMonitor;

@SuppressWarnings("serial")
public class FoundationJContainer extends DefaultPicoContainer {

	public FoundationJContainer(ScopeImpl scope){
		super(new Caching(), 
				new ReflectionLifecycleStrategy(new LifecycleComponentMonitor()), 
				null, 
				new NullComponentMonitor());
				//new ComponentLogger(scope));
	}
	
	public FoundationJContainer(ScopeImpl scope, FoundationJContainer parent) {
		super(parent.componentFactory, parent.lifecycleStrategy, parent, new NullComponentMonitor());//ComponentLogger(scope));
		parent.addChildContainer(this);
	}
	
	
}

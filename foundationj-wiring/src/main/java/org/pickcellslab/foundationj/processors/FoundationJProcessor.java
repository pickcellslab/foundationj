package org.pickcellslab.foundationj.processors;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

abstract class FoundationJProcessor extends AbstractProcessor {

	@Override
	public Set<String> getSupportedAnnotationTypes(){
		return Collections.singleton(annotation().getName());
	};


	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;

		final Set<String> data = new HashSet<String>();

		// Find Data elements from the current compilation sources
		for (TypeElement type : (Collection<TypeElement>)roundEnv.getElementsAnnotatedWith(annotation())) {

			// First check that contract is fulfilled
			if(contractRespected(type)) {// Will generate errors if not
				final String qualifiedName = type.getQualifiedName().toString();
				//If nested then we need to replace the last dot with a $
				if(type.getNestingKind().isNested()) {
					String className = qualifiedName.replaceAll("\\.(?!.*\\.)","\\$");
					data.add(className);
				}
				else
					data.add(qualifiedName);
				processingEnv.getMessager().printMessage(Kind.NOTE,"FoundationJProcessor: "+type.getSimpleName()+" has been processed by "+getClass().getSimpleName());
			}

		}

		final String metadataPath = metadatapath();

		// Load any existing values, since this compilation may be partial
		Filer filer = processingEnv.getFiler();
		try {
			FileObject f = filer.getResource(StandardLocation.CLASS_OUTPUT, "", metadataPath);
			BufferedReader r = new BufferedReader(new InputStreamReader(f.openInputStream(), "UTF-8"));
			String line;
			while((line=r.readLine())!=null)
				data.add(line);
			r.close();
		} catch (FileNotFoundException x) {
			// doesn't exist
		} catch (IOException x) {
			processingEnv.getMessager().printMessage(Kind.ERROR,"Failed to load existing data definition files: "+x);
		}


		// now write them back out
		try {
			processingEnv.getMessager().printMessage(Kind.NOTE,"Writing "+metadataPath);
			FileObject f = filer.createResource(StandardLocation.CLASS_OUTPUT, "", metadataPath);
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(f.openOutputStream(), "UTF-8"));
			for (String value : data)
				pw.println(value);
			pw.close();
		} catch (IOException x) {
			processingEnv.getMessager().printMessage(Kind.ERROR,"Failed to write service definition files: "+x);
		}


		return false;
	}





	/**
	 * Perform any required checks and throw compilation errors if necessary
	 * @param type
	 * @return {@code true} if no errors, {@code false otherwise}
	 */
	protected abstract boolean contractRespected(TypeElement type);

	/**
	 * @return The annotation this Processor needs to process
	 */
	protected abstract Class<? extends Annotation> annotation();

	/**
	 * @return The path to the metadata which need to be generated (for example: META-INF/services)
	 */
	protected abstract String metadatapath();


}

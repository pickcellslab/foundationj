package org.pickcellslab.foundationj.dbm.meta;

import org.pickcellslab.foundationj.annotations.Tag;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.DataTyped;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.DataItem;


/**
 * {@link MetaReadable}s are elements of the {@link MetaModel} which define methods to collect or to compute properties
 * from {@link DataItem} objects. A {@link MetaReadable} may create one or more of these properties ({@link Dimension}s)
 * and declare them explicitly.
 *  
 * @param <D> The output type of the {@link Dimension} this {@link DimensionContainer} can generate
 *  
 * @author Guillaume Blin
 *
 */
@Tag(creator = "MetaModel", 
description = "Items in the metamodel which describes properties that can be collected or computed from a DataItem", 
tag = "MetaReadable")
public interface MetaReadable<D> extends MetaModel, DataTyped, Hideable, DimensionContainer<DataItem,D>{

	public static String APPLICABLE_TO = "APPLICABLE_TO";
	
	/**
	 * A tag which can be included within the description of Dimensions of MetaReadable to indicate that it describes
	 * a categorical variable.
	 */
	public static final String CATEGORICAL = "CATEGORICAL";
	

	/**
	 * @return The {@link MetaItem} this {@link MetaReadable} can be used on.
	 */
	public MetaQueryable owner();


	
	public default boolean isArray(){
		return numDimensions()>1;		
	}
	
	
	/**
	 * @return The type of data the {@link AKey} objects points to.
	 */
	public Class<?> dataClass();// The reason why not Class<D>: could be an array class != from Dimension at index class

	
	@Override
	public default dType dataType(){
		return dType.valueOf(dataClass());
	}

	
}

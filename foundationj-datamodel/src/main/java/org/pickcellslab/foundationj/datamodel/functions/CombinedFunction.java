package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/**
 * A {@link ExplicitFunction} which combines the results of 2 {@link ExplicitFunction}s into one result using 
 * a delegate {@link ExplicitCombiner} 
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of input accepted by this {@link ExplicitFunction}
 * @param <I1> The type of the first intermediary result generated by this {@link CombinedFunction}
 * @param <I2> The type of the second intermediary result generated by this {@link CombinedFunction}
 * @param <R> The type of result returned by this {@link ExplicitFunction}
 */
@Mapping(id="CombinedFunctions")
public class CombinedFunction<T,I1,I2,R> implements ExplicitFunction<T,R> {

	@Supported
	private final ExplicitFunction<T,I1> f1;
	@Supported
	private final ExplicitFunction<T,I2> f2;
	@Supported
	private final ExplicitCombiner<? super I1,? super I2, R> combiner;
	
	public CombinedFunction(ExplicitFunction<T,I1> f1, ExplicitFunction<T,I2> f2, ExplicitCombiner<? super I1,? super I2, R> combiner ) {
		
		MappingUtils.exceptionIfNullOrInvalid(f1);
		MappingUtils.exceptionIfNullOrInvalid(f2);
		MappingUtils.exceptionIfNullOrInvalid(combiner);
		
		this.f1 = f1;
		this.f2 = f2;
		this.combiner = combiner;
		
	}
	
	@Override
	public R apply(T t) {
		final I1 i1 = f1.apply(t);
		final I2 i2 = f2.apply(t);
		return combiner.combine(i1, i2);
	}

	@Override
	public String description() {
		return combiner.description()+" "+f1.description()+" "+f2.description();
	}

	@Override
	public Class<R> getReturnType() {
		return combiner.getReturnType();
	}

}

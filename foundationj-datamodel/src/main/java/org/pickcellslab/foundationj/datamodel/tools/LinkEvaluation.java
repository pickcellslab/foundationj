package org.pickcellslab.foundationj.datamodel.tools;

import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/**
 * 
 * Defines an evaluation on {@link Link}s during a <a href="https://en.wikipedia.org/wiki/Graph_traversal">graph traversal</a>
 * 
 * @author Guillaume Blin
 *
 */
@Mapping(id="LinkEvaluation")
@Scope
public class LinkEvaluation {

	private final String type;
	
	@Supported
	private final ExplicitPredicate<? super Link> predicate;
	
	private final Decision ifYes, ifNo;

	/**
	 * @param type as in {@link DataItem#declaredType}
	 * @param predicate An {@link ExplicitPredicate} to test encountered object along a graph Traversal
	 * @param ifYes
	 * @param ifNo
	 */
	public LinkEvaluation(String type, ExplicitPredicate<? super Link> predicate, Decision ifYes, Decision ifNo) {
		MappingUtils.exceptionIfNullOrInvalid(predicate);
		Objects.requireNonNull(ifYes);
		Objects.requireNonNull(ifNo);
		this.type = type;
		this.predicate = predicate;
		this.ifYes = ifYes;
		this.ifNo = ifNo;
	}
	
	
	/**
	 * @return The {@link DataItem#declaredType()} for which this {@link LinkEvaluation}
	 * is defined
	 */
	public String evaluatedType() {
		return type;
	}
	
	/**
	 * @return The {@link ExplicitPredicate} used by this {@link LinkEvaluation}
	 */
	public ExplicitPredicate<? super Link> predicate(){
		return predicate;
	}
	
	
	/**
	 * @return The decision used by this {@link LinkEvaluation} if its {@link #predicate()} returns {@code true}
	 */
	public Decision ifYes() {
		return ifYes;
	}
	
	/**
	 * @return The decision used by this {@link LinkEvaluation} if its {@link #predicate()} returns {@code false}
	 */
	public Decision ifNo() {
		return ifNo;
	}
	
	
	/**
	 * @param object
	 * @return {@code true} if this {@link LinkEvaluation} can evaluate the given {@link DataItem}
	 */
	public boolean canEvaluate(Link object) {
		return object.declaredType().equals(type);
	}
	
	public Decision evaluate(Link item) {
		if(!canEvaluate(item))
			return ifNo;
		return predicate.test(item) ? ifYes : ifNo;
	}
	
	
}

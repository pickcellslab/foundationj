package org.pickcellslab.foundationj.queryui;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.meta.MetaAutoDeletable;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

@Data(typeId = "MetaDepthSplitter", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaDepthSplitter extends MetaSplit {

	
	private MetaDepthSplitter(String uid){
		super(uid);
	}
	
	static MetaDepthSplitter getOrCreate(MetaInstanceDirector director) throws InstantiationHookException {
		
		final MetaDepthSplitter split = director.getOrCreate(MetaDepthSplitter.class, "MetaDepthSplitter");	

		split.setAttribute(MetaItem.name, "Depth splitting");
		split.setAttribute(MetaItem.desc, "Organise path by depth");
		
		return split;
	}
	
	
	
	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}
	
	

	@Override
	public String toHTML() {
		return "<HTML>"+
				"<p><strong>Grouping by bins</strong>"+
				"<ul><li>Name:"+name()+"</li>"+
				"<li>Description:"+description()+
				"</li></p></HTML>";
	}

	@Override
	public boolean pathSplitSupported() {
		return true;
	}

	@Override
	public boolean targetSplitSupported() {
		return false;
	}

	@Override
	public <E> ExplicitSplit<E, String> toSplitter() {
		throw new UnsupportedOperationException();
	}

	@Override
	public <V, E> ExplicitPathSplit<V, E, String> toPathSplitter() {
		return new ExplicitPathSplit<V,E,String>(){
			@Override
			public String description() {
				return "Group targets based on their position in the tarversal";
			}


			@Override
			public void setFactory(FjAdaptersFactory<V, E> f) {
				// Not required
			}


			@Override
			public String getKeyFor(Path<V, E> p) {
				return ""+p.edgesCount();
			}
		};
	}

}

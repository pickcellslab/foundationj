package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.annotations.ScopeOption;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest;
import org.pickcellslab.foundationj.dbm.db.DataStoreConnector;
import org.pickcellslab.foundationj.dbm.db.DataStoreManager;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.QInterpreter;
import org.pickcellslab.foundationj.dbm.events.ConnexionFailedException;
import org.pickcellslab.foundationj.dbm.events.DataUpdateListener;
import org.pickcellslab.foundationj.dbm.events.MetaListenerNotifier;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener.MetaChange;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;
import org.pickcellslab.foundationj.dbm.queries.Query;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.SearchingQuery;
import org.pickcellslab.foundationj.dbm.queries.StorageBox;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@CoreImpl
public final class Session<V,E> implements DataAccess, MetaListenerNotifier{

	private final Logger log = LoggerFactory.getLogger(Session.class);

	
	private final DataStoreManager<V,E> datastore;

	private List<MetaModelListener> metaLstrs = new ArrayList<>();

	private final QInterpreter<StorageBox> dataStorer;
	private final QInterpreter<MetaBox> metaStorer;


	private final MetaImpl meta;
	
	private final DataRegistry registry;

	
	public Session(DataStoreConnector<V,E> connector, @ScopeOption ConnexionRequest request, DataRegistry registry) throws ConnexionFailedException {

		Objects.requireNonNull(connector, "The connector is null");
		Objects.requireNonNull(request, "The connexion request is null");
		Objects.requireNonNull(registry, "The registry is null");

		this.datastore = connector.connect(request, registry);
		
		dataStorer = datastore.createDataStorer(this);
		metaStorer = datastore.createMetaStorer(this);
		
		Objects.requireNonNull(dataStorer, "DataStoreManager failed to produce a QInterpreter for data storage");
		Objects.requireNonNull(metaStorer, "DataStoreManager failed to produce a QInterpreter for meta storage");
		
		this.registry = registry;
		
		// Get the MultitonDirector for MetaModel objects		
		final MetaInstanceDirector director = registry.getHook(MetaInstanceDirector.class);
		// and register this director as a MetaModelListener
		metaLstrs.add(director);
		
		// Now instantiate the Meta
		meta = new MetaImpl(this, director);
		
	}

	
	
	@Override
	public MetaImpl metaModel() {
		return meta;
	}



	@Override
	public DataRegistry dataRegistry() {
		return registry;
	}



	@Override
	public QueryFactoryImpl<V,E> queryFactory() {
		return new QueryFactoryImpl<V,E>(this);
	}
	

	@Override
	public void addUpdateListener(DataUpdateListener lstr){	  
		datastore.addUpdateListener(lstr);
	}

	@Override
	public boolean removeUpdateListener(DataUpdateListener lstr){	 
		return datastore.removeUpdateListener(lstr);
	}

	@Override
	public void addMetaListener(MetaModelListener lstr){    
		metaLstrs.add(lstr);
	}

	@Override
	public boolean removeMetaListener(MetaModelListener lstr){  
		return metaLstrs.remove(lstr);
	}

	@Override
	public void notify(RegeneratedItems meta, MetaChange evt) {		
		new Thread(()->	new ArrayList<>(metaLstrs).forEach(l->{
			log.trace("notifying "+l.toString());
			l.metaEvent(meta, evt);
			log.trace(l.toString()+" done!");
		})).start();		
		log.trace("MetaEvents dispatched!");		
	}





	/**
	 * Runs the specified {@link Query}
	 * 
	 * @param query
	 * @return a {@link QResult}
	 * @throws DataAccessException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <R> R runQuery(Query<R> query) throws DataAccessException {

		Objects.requireNonNull(query,"null query!");

		R result = null;


		if(SearchingQuery.class.isAssignableFrom(query.getClass())){

			try(DatabaseAccess<V, E> access = datastore.newAccess()){
				result = (R)((SearchingQuery) query).createResult(access);
				access.success();
			} catch (Exception e) {	
				throw new DataAccessException("An error occured while searching the database. Check the log",e);
			} 
			
		}else{


			if(query instanceof StorageBoxImpl){	
				dataStorer.run((StorageBoxImpl) query);
			}
			else{
				metaStorer.run((MetaBoxImpl) query);
			}
		}

		return result;
	}


	
	public void stop() {
		log.debug("Closing Session");
		datastore.close();
	}



}

package org.pickcellslab.foundationj.dbm.events;

import org.pickcellslab.foundationj.datamodel.DataItem;

/**
 * Listens for modifications occurring in the persisted data graph. When a database operation is performed and that at least one DataUpdateListener is associated,
 * modifications are recorded to a {@link DataModificationEvent} which is provided to the listeners at the end of the operation.
 * <br><br>
 * <b>NB: </b> There are 2 possibilities to listen for data modifications. 1) At the (high) level of the MetaModel by implementing
 * {@link MetaModelListener} or 2) at a (lower) level by implementing {@link DataUpdateListener} which receives info about changes to individual 
 * instances of {@link DataItem}s.  
 * 
 * @see MetaModelListener
 * 
 * @author Guillaume Blin
 *
 */
public interface DataUpdateListener {
			
 
	/**
	 * @param typeId
	 * @return {@code true} if this {@link DataUpdateListener} is interested in listening to this typeId, {@code false} otherwise
	 */
	public boolean listens(String typeId);
		
	/**
	 * @param evt A {@link DataModificationEvent} providing info about the modification
	 */
	public void aDataModificationHasOccured(DataModificationEvent evt);

	
	
	
}

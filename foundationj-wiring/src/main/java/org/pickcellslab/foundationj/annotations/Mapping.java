package org.pickcellslab.foundationj.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.mapping.extra.CustomNonDataConverter;

/**
 * 
 * Non-data types which need to be stored in the database must be annotated with @{@link Mapping}.
 * <br>
 * <br>A unique String id which should be human readable is required. This id is used to store the
 * annotated object in the database and map the database entity back to the annotated class when the object
 * needs to be restored. 
 * <br><br>
 * A specific {@link CustomNonDataConverter} can be defined for the annotated class.
 * 
 * <br><br><b>NB:</b> If a {@link CustomNonDataConverter} is not specified, all fields must be either objects whose class
 * is annotated with {@link Mapping} or fields annotated with @{@link Ignored} or @{@link Supported}, 
 * unless they are enum constants, primitive, boxed types, String or Maps / Collections / Arrays of supported objects
 * 
 * @author Guillaume Blin
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Mapping {

	/**
	 * @return A human readable id which gives an idea of what the annotated class is about 
	 */
	String id();
	
	/**
	 * @return A concrete {@link CustomNonDataConverter} which will handle the annotated class
	 */
	Class<? extends CustomNonDataConverter> converter() default Default.class;
	
	
	
	/**
	 * Internal, do not use.
	 */
	static class Default implements CustomNonDataConverter{

		@Override
		public String toString(Object object) throws NonDataMappingException {
			throw new RuntimeException("NonDataMapping Implementation is not checking for default converter");
		}

		@Override
		public Object fromString(String xml) throws NonDataMappingException {
			throw new RuntimeException("NonDataMapping Implementation is not checking for default converter");
		}
		
	}
	
}

package org.pickcellslab.foundationj.dbm.queries;

import java.util.List;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "PredicateTreeGrouping")
@WithService(DataAccess.class)
public class ToStringBinaryTreeSplitter<E> implements ExplicitSplit<E, String> {

	private final CombinedBinary<E> root;
	
	
	public ToStringBinaryTreeSplitter(List<ExplicitPredicate<?super E>> predicates) {
		assert predicates.size()>2 : "At least 2 pedicates are required";
		MappingUtils.exceptionIfNullOrInvalid(predicates);
		root = CombinedBinary.createTree(predicates);
	}
	
	
	@Override
	public String description() {
		return "Group using a binary tree of predicate ";
	}


	
	@Override
	public String getKeyFor(E i) {
		return CombinedBinary.get(root, i).toString();		
	}

}

package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Component;
import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.functions.Constant;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.BiFunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.EvaluatorUI;
import org.pickcellslab.foundationj.ui.ValueSelector;
import org.pickcellslab.foundationj.ui.parsers.ExplicitPredicateParser;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

import com.fathzer.soft.javaluator.StaticVariableSet;

@SuppressWarnings("serial")
public class AttributeFilterUI extends WizardNode {

	private final UITheme theme;
	private final MetaQueryable target;
	private final MetaInstanceDirector director;
	
	private static final String ITEM = "Item attribute", SOURCE = "Source Attribute",
			LINK = "Link Attribute", TARGET = "Target Attribute", CONSTANT = "Constant";

	//EvaluatorUI
	private final EvaluatorUI<ExplicitPredicateParser<WritableDataItem>, ExplicitPredicate<WritableDataItem>> evaluation;

	//Variables for a new attribute restriction
	private	ExplicitFunction<?,?> left = null;
	private	ExplicitFunction<?,?> right = null;
	private	ExplicitOperator<?,?> op = null;

	//UI field
	private final JComboBox<ExplicitOperator<?,?>> opBox;
	private MetaDescriptionPanel<?> mdp;


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public AttributeFilterUI(WizardNode parent, UITheme theme, MetaInstanceDirector director, MetaQueryable target) {
		super(parent);

		this.theme = theme;
		this.target = target;
		this.director = director;

		//Defining variables important for the creation of the expression
		//check if the target is an item or a link
		boolean isClass = false;
		if(MetaClass.class.isAssignableFrom(target.getClass()))
			isClass = true;

		//The map storing the variables must be the same for the display and the evaluator
		//TODO create an abstract subclass of abstract evaluator in order to ensure this
		StaticVariableSet<ExplicitPredicate<?>> var = new StaticVariableSet<>();

		//Create a parser for an attribute filter to inject into the EvaluatorUI
		ExplicitPredicateParser<?> parser = new ExplicitPredicateParser(var);

		//Create the display to show the expression
		evaluation = new EvaluatorUI(parser,var);

		//A list to keep track of expression tokens that were inserted
		LinkedList<String> expression = new LinkedList<>();


		//Controllers
		//------------------------------------------------------------------------------------------
		Component display = evaluation.getDisplay();
		//Component display = new JPanel();


		JButton rightPBtn = new JButton("(");
		rightPBtn.addActionListener(l->{
			evaluation.insert("{", EvaluatorUI.parenthesis);
			expression.add("{");
			this.fireNextValidityChanged();
		});

		JButton leftPBtn = new JButton(")");
		leftPBtn.addActionListener(l->{
			evaluation.insert("}", EvaluatorUI.parenthesis);
			expression.add("}");
			this.fireNextValidityChanged();
		});

		JButton btnAnd = new JButton("AND");
		btnAnd.addActionListener(l->{
			evaluation.insert(" AND ", EvaluatorUI.operator);
			expression.add(" AND ");
			this.fireNextValidityChanged();
		});

		JButton btnOr = new JButton("OR");
		btnOr.addActionListener(l->{
			evaluation.insert(" OR ", EvaluatorUI.operator);
			expression.add(" OR ");
		});

		JButton btnXor = new JButton(" XOR ");
		btnXor.addActionListener(l->{
			evaluation.insert(" XOR ", EvaluatorUI.operator);
			expression.add(" XOR ");
			this.fireNextValidityChanged();
		});

		JButton btnNot = new JButton(" NOT ");
		btnNot.addActionListener(l->{
			evaluation.insert(" NOT ", EvaluatorUI.operator);
			expression.add(" NOT ");
			this.fireNextValidityChanged();
		});

		JButton btnC = new JButton("C");
		btnC.addActionListener(l->{
			evaluation.delete(expression.pollLast());
			this.fireNextValidityChanged();
		});

		JComboBox<String> leftKeyBox = new JComboBox<>();
		leftKeyBox.addItem(" ");
		if(isClass)
			leftKeyBox.addItem(ITEM);
		else{
			leftKeyBox.addItem(SOURCE);
			leftKeyBox.addItem(LINK);
			leftKeyBox.addItem(TARGET);
		}

		opBox = new JComboBox<>();
		opBox.setPrototypeDisplayValue(Op.Bool.AND);
		opBox.setToolTipText("Select an operator");

		JComboBox<String> rightBox = new JComboBox<>();
		rightBox.addItem(" ");
		rightBox.addItem(CONSTANT);
		if(isClass)
			rightBox.addItem(ITEM);
		else{
			rightBox.addItem(SOURCE);
			rightBox.addItem(LINK);
			rightBox.addItem(TARGET);
		}
		rightBox.setEnabled(false);


		leftKeyBox.addActionListener(l->{
			if(leftKeyBox.getSelectedIndex() != 0){
				if(updateLeft((String) leftKeyBox.getSelectedItem())){
					leftKeyBox.insertItemAt(left.toString(), 0);
					leftKeyBox.removeItemAt(1);	
					rightBox.setEnabled(true);					

					checkOperator();
					rightBox.insertItemAt(" ", 0);
					rightBox.removeItemAt(1);
				}
				leftKeyBox.setSelectedIndex(0);								
			}
		});


		rightBox.addActionListener(l->{
			if(rightBox.getSelectedIndex() != 0){
				if(updateRight((String) rightBox.getSelectedItem())){
					rightBox.insertItemAt(right.toString(), 0);
					rightBox.removeItemAt(1);				
				}else{
					rightBox.insertItemAt(" ", 0);
					rightBox.removeItemAt(1);
				}
				rightBox.setSelectedIndex(0);
			}
		});

		JButton btnInsPredicate = new JButton("INS");
		btnInsPredicate.addActionListener(l->{
			if(right == null){
				JOptionPane.showMessageDialog(this,"Options are not well defined yet");
				return;
			}
			op = (ExplicitOperator<?, ?>) opBox.getSelectedItem();
			ExplicitPredicate<WritableDataItem> current = new BiFunctionPredicate(left, right, op);
			expression.add(current.toString());
			evaluation.insert(current.toString(), current, EvaluatorUI.funct);			
			this.fireNextValidityChanged();
		});

		JLabel lblKnownFilters = new JLabel("Known Filters");

		JComboBox<MetaFilter> filtersBox = new JComboBox<>();
		filtersBox.setRenderer(new MetaRenderer(theme, director.registry()));
		//Load filters for the specific target ()
		target.filters()
		//.stream().filter(f->f.subtype() == Subtype.ATTRIBUTES)
		.forEach(item -> filtersBox.addItem(item));

		String[] fChoice = null;
		if(MetaLink.class.isAssignableFrom(target.getClass()))
			fChoice = new String[]{"On Link", "on Source", "on Target"};
		else
			fChoice = new String[]{"On Item"};

		JComboBox<String> fChoiceBox = new JComboBox<>(fChoice);
		fChoiceBox.addActionListener(l->{
			filtersBox.removeAllItems();
			switch(fChoiceBox.getSelectedIndex()){
			case 0: 
				target.filters()
				.forEach(item -> filtersBox.addItem(item));
				break;
			case 1: 
				((MetaLink) target).source().filters()
				.forEach(item -> filtersBox.addItem(item));
				break;
			case 2: 
				((MetaLink) target).target().filters()
				.forEach(item -> filtersBox.addItem(item));
				break;
			}
		});


		JButton btnInsFilter = new JButton("INS");
		btnInsFilter.addActionListener(l->{
			MetaFilter mf = (MetaFilter) filtersBox.getSelectedItem();
			if(mf == null){
				JOptionPane.showMessageDialog(this,"There are no filters selected");
				return;
			}
			//if this is a MetaLink allow to use source or target filters
			//which means creating a new predicate to apply on source or target 
			ExplicitPredicate f = mf.toFilter();
			String st = "";
			switch(fChoiceBox.getSelectedIndex()){
			case 0: break;
			case 1: f = P.sourcePasses(f); st = "source:"; break;
			case 2: f = P.targetPasses(f); st = "target:"; break;
			}
			expression.add(st+mf.toString());
			evaluation.insert(st+mf.toString(), f, EvaluatorUI.funct);	
			this.fireNextValidityChanged();
		});





		//Layout
		//----------------------------------------------------------------------------------



		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(display, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblKnownFilters)
												.addGroup(groupLayout.createSequentialGroup()
														.addComponent(btnNot, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
														.addGap(18)
														.addComponent(btnAnd, GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE))
												.addComponent(leftKeyBox, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE))
										.addGap(12)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
														.addComponent(rightPBtn, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(ComponentPlacement.UNRELATED)
														.addComponent(leftPBtn, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
												.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
														.addComponent(fChoiceBox, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(opBox, Alignment.LEADING, 0, 85, Short.MAX_VALUE)))
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
														.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																.addGroup(groupLayout.createSequentialGroup()
																		.addGap(12)
																		.addComponent(btnOr, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
																		.addGap(18)
																		.addComponent(btnXor, GroupLayout.PREFERRED_SIZE, 64, Short.MAX_VALUE))
																.addGroup(groupLayout.createSequentialGroup()
																		.addGap(10)
																		.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
																				.addComponent(filtersBox, Alignment.TRAILING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																				.addComponent(rightBox, Alignment.TRAILING, 0, 136, Short.MAX_VALUE))))
														.addPreferredGap(ComponentPlacement.UNRELATED)
														.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																.addComponent(btnC, GroupLayout.PREFERRED_SIZE, 33, Short.MAX_VALUE)
																.addComponent(btnInsPredicate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
												.addGroup(groupLayout.createSequentialGroup()
														.addPreferredGap(ComponentPlacement.RELATED, 217, Short.MAX_VALUE)
														.addComponent(btnInsFilter)))))
						.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(display, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnC)
								.addComponent(rightPBtn)
								.addComponent(leftPBtn)
								.addComponent(btnNot)
								.addComponent(btnOr)
								.addComponent(btnAnd)
								.addComponent(btnXor))
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(leftKeyBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(rightBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(opBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnInsPredicate))
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnInsFilter)
								.addComponent(lblKnownFilters)
								.addComponent(filtersBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(fChoiceBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(23, Short.MAX_VALUE))
				);
		groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnAnd, btnOr, btnXor, btnNot});
		groupLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {leftKeyBox, rightBox});
		setLayout(groupLayout);



	}

	private void checkOperator() {	
		right = null;
		opBox.removeAllItems();		
		for(ExplicitOperator<?,?> o : left.getOperatorCompatibility()){
			opBox.addItem(o);
		}
	}

	private boolean updateLeft(String owner) {

		AtomicBoolean choice = new AtomicBoolean(false);

		//Get the owner of the MetaKeys to select
		MetaQueryable mq = null;
		switch(owner){
		case ITEM : mq = target; break;
		case LINK : mq = target; break;
		case SOURCE : mq = ((MetaLink) target).source(); break;
		case TARGET : mq = ((MetaLink) target).target(); break;
		}



		ReadableSelection ui = new ReadableSelection(theme, director.registry(), mq.getReadables().filter(p->true).collect(Collectors.toList()), true);
		ui.setModal(true);
		ui.pack();
		ui.setLocationRelativeTo(this);		
		ui.setVisible(true);

		ui.getSelected().ifPresent(chosen->{
			switch(owner){
			case ITEM : left = chosen; break;
			case LINK : left = chosen; break;
			case SOURCE : left = F.applyOnSource((ExplicitFunction)chosen); break;
			case TARGET : left = F.applyOnTarget((ExplicitFunction)chosen); break;
			}
			choice.set(true);
		});

		return choice.get();		
	}

	private boolean updateRight(String owner) {

		right = null;
		
		if(owner == CONSTANT){
			//Create a fake key to obtain a ValueSelector
			//get the left operand return type
			Class<?> clazz = left.getReturnType();
			//If it is an array then get the actual type
			if(clazz.isArray())
				clazz = clazz.getComponentType();
			
			//fakeKey for a ValueSelector
			AKey<?> fake = AKey.get("fake", AKey.noPrimitiveClass(clazz));
			@SuppressWarnings({ "rawtypes", "unchecked" })
			ValueSelector<?> vs = new ValueSelector(fake);
			vs.setModal(true);
			vs.setVisible(true);

			Object value = vs.getValue();
			if(null == value){
				System.out.println("Null!");
				right = null;
				return false;
			}
			right = new Constant<>(value);
			return true;
		}


		//Get the owner of the MetaKeys to select
		MetaQueryable mq = null;
		switch(owner){
		case ITEM : mq = target; break;
		case LINK : mq = target; break;
		case SOURCE : mq = ((MetaLink) target).source(); break;
		case TARGET : mq = ((MetaLink) target).target(); break;
		}

		//Get the MetaKeys it possesses and filter to obtain only the compatible types 
		//with the current operator
		op = (ExplicitOperator<?, ?>) opBox.getSelectedItem();


		ReadableSelection ui = new ReadableSelection(theme, director.registry(), mq.getReadables().filter(mk -> op.isRightValid(mk.dataClass())).collect(Collectors.toList()), true);

		ui.setModal(true);
		ui.pack();
		ui.setLocationRelativeTo(this);		
		ui.setVisible(true);
		
		AtomicBoolean b = new AtomicBoolean(false);

		ui.getSelected().ifPresent(chosen->{

			switch(owner){
			case ITEM : right = chosen; break;
			case LINK : right = chosen; break;
			case SOURCE : right = F.applyOnSource((ExplicitFunction)chosen); break;
			case TARGET : right = F.applyOnTarget((ExplicitFunction)chosen); break;
			}
			
			b.set(true);
		});
		return b.get();	
	}

	@Override
	protected WizardNode getNextStep() {		
		if(mdp == null)
			mdp = new MetaDescriptionPanel<>(this, director, evaluation.getFunction(), target);
		else
			mdp.setPredicate(evaluation.getFunction());
		return mdp;
	}

	@Override
	public int numberOfForks() {
		return 1;
	}

	@Override
	public boolean validity() {
		return evaluation.isValid();
	}

	public Optional<ExplicitPredicate<?>> getPredicate(){
		ExplicitPredicate<?> current = null;
		try{
			current = evaluation.getFunction();
		}catch(IllegalStateException e){
			return Optional.empty();
		}
		return Optional.of(current);
	}

	public String expression(){
		return evaluation.getExpression();
	};

}

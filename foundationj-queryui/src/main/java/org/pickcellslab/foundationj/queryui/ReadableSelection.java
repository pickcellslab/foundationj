package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;

/**
 * A UI to create a {@link ExplicitFunction} from a list of {@link MetaReadable}'s {@link Dimension}s.
 * 
 * @author Guillaume Blin
 *
 */
public class ReadableSelection extends JDialog {

	private static final long serialVersionUID = -1116254179306797622L;
	private final JPanel contentPanel = new JPanel();

	private List<ChangeListener> lstrs = new ArrayList<>();

	private MetaReadable<?> selectedKey;
	private boolean chooseArray = false;
	private int index = 0;
	private final JSlider slider;
	private final JCheckBox arrayBox;
	private final boolean arraySelectable, forceArray;


	/**
	 * Creates a new {@link ReadableSelection} which will create a {@link ExplicitFunction} transforming a
	 * {@link WritableDataItem} into a value. The DataFunction is configured by the user
	 * who is given the possibility to choose a {@link MetaKey} object to select the attribute and to select
	 * an index if the attribute is an array. Note that any {@link MetaReadable} type is supported meaning that
	 * for example, if a {@link MetaMultiFunction} is contained in the provided collection, then all the generated
	 * keys will be proposed and the created {@link ExplicitFunction} will be composed of the MultiFunction which 
	 * generates the selected MetaKey + a function which selects which computed feature to obtain
	 * 
	 * @param readables The List of {@link MetaReadable} that can be used to construct the DataFunction
	 * @param arraySelectable If {@code true}, then the user can create a DataFunction returning an array
	 * @throws IllegalArgumentException if keys is empty or null
	 */
	public ReadableSelection(UITheme theme, DataRegistry registry, Collection<MetaReadable<?>> readables, boolean arraySelectable){
		this(theme, registry, readables, arraySelectable, false);
	}
	
	/**
	 * Creates a new {@link ReadableSelection} which will create a {@link ExplicitFunction} transforming a
	 * {@link WritableDataItem} into a value. The DataFunction is configured by the user
	 * who is given the possibility to choose a {@link MetaKey} object to select the attribute and to select
	 * an index if the attribute is an array. Note that any {@link MetaReadable} type is supported meaning that
	 * for example, if a {@link MetaMultiFunction} is contained in the provided collection, then all the generated
	 * keys will be proposed and the created {@link ExplicitFunction} will be composed of the MultiFunction which 
	 * generates the selected MetaKey + a function which selects which computed feature to obtain
	 * 
	 * @param readables The List of {@link MetaReadable} that can be used to construct the DataFunction
	 * @param arraySelectable If {@code true}, then the user can create a DataFunction returning an array
	 * @param forceArray If {@code true}, then the user cann only choose an attribute which is an array.
	 * @throws IllegalArgumentException if keys is empty or null
	 */
	@SuppressWarnings("rawtypes")
	public ReadableSelection(UITheme theme, DataRegistry registry, Collection<MetaReadable<?>> readables, boolean arraySelectable, boolean forceArray) {

		if(readables == null || readables.isEmpty())
			throw new IllegalArgumentException("readables cannot be empty or null");

		this.arraySelectable = arraySelectable;
		this.forceArray = forceArray;

		//ComboBox to display available MetaKey
		JComboBox<MetaReadable<?>> comboBox = new JComboBox<>(readables.toArray(new MetaReadable[readables.size()]));
		comboBox.setRenderer(new MetaRenderer(theme, registry));
		selectedKey =  (MetaReadable<?>) comboBox.getSelectedItem();

		//Slider in case the selected key is an array
		slider = new JSlider();
		slider.setToolTipText(""+slider.getValue());
		slider.setValue(5);
		slider.setMaximum(10);
		slider.setSnapToTicks(true);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setMinimum(0);

		slider.addChangeListener(l->{
			slider.setToolTipText(""+slider.getValue());
			this.fireChange();
		});

		updateSlider();


		arrayBox = new JCheckBox("Get the entire array");
		updateArrayBox();

		arrayBox.addActionListener(e->{
			chooseArray = arrayBox.isSelected();
			updateSlider();
		});

		//Update slider and arrayBox when chosen MetaKey changes
		comboBox.addItemListener( 
				e->{
					if(e.getStateChange() == ItemEvent.SELECTED){
						selectedKey = (MetaReadable) comboBox.getSelectedItem();
						updateArrayBox();
						updateSlider();
					};
				});






		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));

		{
			JButton okButton = new JButton("OK");
			okButton.addActionListener(e-> {
				index = slider.getValue();
				this.dispose();					
			});
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(e-> {
				selectedKey = null;
				this.dispose();
			});
			buttonPane.add(cancelButton);
		}










		//Layout
		//--------------------------------------------------------------------------------


		setBounds(100, 100, 324, 190);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);



		JLabel lblPleaseSelectA = new JLabel("Please select a key");
		lblPleaseSelectA.setHorizontalAlignment(SwingConstants.RIGHT);

		JLabel lblIndex = new JLabel("index");
		lblIndex.setHorizontalAlignment(SwingConstants.RIGHT);



		JLabel lblIfTheKey = new JLabel("If key contains array");
		lblIfTheKey.setHorizontalAlignment(SwingConstants.RIGHT);


		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(lblPleaseSelectA, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
								.addGroup(Alignment.LEADING, gl_contentPanel.createSequentialGroup()
										.addComponent(lblIfTheKey, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGroup(gl_contentPanel.createSequentialGroup()
												.addComponent(lblIndex, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
												.addGap(18)))
												.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
														.addGroup(gl_contentPanel.createSequentialGroup()
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
																.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
																		.addComponent(arrayBox, GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
																		.addComponent(slider, 0, 0, Short.MAX_VALUE)))
																		.addGap(63))
				);
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblPleaseSelectA)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(arrayBox)
										.addComponent(lblIfTheKey))
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
												.addComponent(slider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblIndex))
												.addContainerGap())
				);
		gl_contentPanel.linkSize(SwingConstants.HORIZONTAL, new Component[] {lblPleaseSelectA, lblIndex, lblIfTheKey});
		contentPanel.setLayout(gl_contentPanel);

		getContentPane().add(buttonPane, BorderLayout.SOUTH);
	}


	
	//Listeners
	//---------------------------------------------------------------------------------------------------------

	public void addChangeListener(ChangeListener lst){
		lstrs.add(lst);
	}

	public void removeSliderListener(ChangeListener lst){
		lstrs.remove(lst);
	}

	public void fireChange(){
		lstrs.forEach(l->l.stateChanged(new ChangeEvent(this)));
	}

	public int currentIndex(){
		return slider.getValue();
	}
	


	/**
	 * @return An {@link Optional} holding the {@link ExplicitFunction} specified by the user (Extract a value from a DataItem)
	 */
	public Optional<Dimension<DataItem,?>> getSelected(){

		if(selectedKey == null)
			return Optional.empty();
		
		//Create the function
		Dimension<DataItem,?> function = null;		
		if(index==-1)//Is a single value or take the whole array
			function = selectedKey.getRaw();
		else{
			function = selectedKey.getDimension(index);
		}

		return Optional.of(function);
	}





	private void updateArrayBox() {
		if(forceArray){
			arrayBox.setSelected(true);
			arrayBox.setEnabled(false);
		}
		else if(!selectedKey.isArray() || chooseArray == true){//not array
			arrayBox.setSelected(false);
			arrayBox.setEnabled(false);
		}else 			
			arrayBox.setEnabled(arraySelectable);
	}




	private void updateSlider(){
		if(!selectedKey.isArray() || chooseArray == true || forceArray){//not array
			slider.setEnabled(false);
			slider.setMinimum(-1);
			slider.setMaximum(-1);
			slider.setValue(-1);
		}
		else{
			slider.setMinimum(0);
			slider.setMaximum(selectedKey.numDimensions()-1);
			slider.setValue(0);
			slider.setEnabled(true);
		}
		this.fireChange();
	}


	




}

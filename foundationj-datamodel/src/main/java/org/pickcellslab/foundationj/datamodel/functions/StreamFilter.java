package org.pickcellslab.foundationj.datamodel.functions;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "stream filter")
public class StreamFilter<I,O> implements ExplicitStream<I, O> {

	@Supported
	private final ExplicitFunction<I, Stream<O>> stream;
	
	@Supported
	private final ExplicitPredicate<? super O> predicate;
	
	public StreamFilter(ExplicitFunction<I, Stream<O>> stream, ExplicitPredicate<? super O> predicate) {
		MappingUtils.exceptionIfNullOrInvalid(stream);
		MappingUtils.exceptionIfNullOrInvalid(predicate);
		this.stream = stream;
		this.predicate = predicate;
	}
	
	@Override
	public Stream<O> apply(I i) {
		return stream.apply(i).filter(o->predicate.test(o));
	}

	@Override
	public String description() {
		return stream.description()+" -> "+predicate.description();
	}


	@Override
	public String toString() {
		return stream.toString()+" -> "+predicate.toString();
	}


}

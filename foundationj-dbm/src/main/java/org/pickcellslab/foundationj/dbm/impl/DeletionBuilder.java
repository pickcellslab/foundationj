package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Deletable;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.impl.DeleteConfiguration.Builder;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.DeletionConfig;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;


class DeletionBuilder<V,E,T extends WritableDataItem & Deletable> implements AccessChoice<T,QueryRunner<Integer>>, DeletionConfig<T>{

	private final TargetType<V,E,T> target;
	private final Builder<T> delegate;

	DeletionBuilder(TargetType<V,E,T> target){
		this.target = target;
		delegate = new DeleteConfiguration.Builder();
	}


	@Override
	public AccessChoice<T,QueryRunner<Integer>> completely() {
		return this;
	}




	@Override
	public AccessChoice<T,QueryRunner<Integer>> key(AKey<?> k) {
		Objects.requireNonNull(k,"The provided key cannot be null");
		delegate.deleteKey(k);
		return this;
	}




	@Override
	public AccessChoice<T,QueryRunner<Integer>> keys(Collection<AKey<?>> keys) {
		Objects.requireNonNull(keys, "The provided collection cannot be null");
		if(keys.isEmpty())
			throw new IllegalArgumentException("The provided Collection is empty");		
		delegate.deleteKeys(keys);
		return this;
	}
	


	
	public Integer all() throws DataAccessException{
		return getAll().run();
	}
	
	


	@Override
	public QueryRunner<Integer> getAll() {
		return new MonolithicQBuilder<>(
				target,
				new DeleteAction<>(delegate.build(), new DeletionListenerComplete(target)),null
				);
	}


	@Override
	public QueryRunner<Integer> useFilter(ExplicitPredicate<? super T> test) {
		return new MonolithicQBuilder<>(
				target,
				new DeleteAction<>(delegate.build(), new DeletionListener(target)),null
				).useFilter(test);
	}


	@Override
	public QueryRunner<Integer> useTraversal(TraversalDefinition definition) {
		target.checkTraversalDefinition(definition);
		return new MonolithicQBuilder<>(
				target,
				new DeleteAction<>(delegate.build(), new DeletionListener(target)),null
				).useTraversal(definition);
	}


	@Override
	public QueryRunner<Integer> useTraversals(SuppliedTraversalOperation operations) {
		return new MonolithicQBuilder<>(
				target,
				new DeleteAction<>(delegate.build(), new DeletionListener(target)),null
				).useTraversals(operations);
	}



	

	

}

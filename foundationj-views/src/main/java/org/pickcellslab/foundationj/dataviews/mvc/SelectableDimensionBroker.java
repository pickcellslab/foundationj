package org.pickcellslab.foundationj.dataviews.mvc;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

public interface SelectableDimensionBroker<T,D> extends DimensionBroker<T,D> {

	/**
	 * @return The number of selected {@link Dimension}(s)
	 */
	public int numSelectedDimensions();
	
	/**
	 * @param i
	 * @return The selected Dimension at index i (i must be < {@link #numSelectedDimensions()})
	 */
	public Dimension<T,D> getSelectedDimension(int i);
	
	public void setSelectedDimension(int i, boolean selected);
	
	public void addDimensionSelectionListener(DimensionSelectionListener l);
	
	public void removeDimensionSelectionListener(DimensionSelectionListener l);
	
}

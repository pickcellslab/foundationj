package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.functions.ConnectionSelector;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.NumberPredicateUI;
import org.pickcellslab.foundationj.ui.wizard.ValidityListener;
import org.pickcellslab.foundationj.ui.wizard.ValidityToggle;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.GroupingType;

@SuppressWarnings("serial")
public class ConnectionFilterUI extends WizardNode implements ValidityListener {

	private final JComboBox<Direction> dirBox;
	private final NumberPredicateUI<Long> numberPredicateUI;

	private final MetaClass target;
	private final MetaInstanceDirector director;

	private MetaLink ml;
	private ExplicitPredicate<WritableDataItem> lPredicate;
	private ExplicitPredicate<NodeItem> aPredicate;

	private boolean isValid = false;

	private JLabel ltChoice;
	private JLabel linkFilter;
	private JLabel adjFilter;
	private ChoiceSet choiceView;



	public ConnectionFilterUI(WizardNode parent, MetaClass mq, DataAccess access, UITheme theme ) throws Exception {
		super(parent);



		Objects.requireNonNull(mq, "The MetaClass is null");
		Objects.requireNonNull(access, "The DataAccess is null");

		this.director = access.metaModel().getInstanceManager();

		this.target = mq;

		// Get the links and adjacent object of our target
		Set<MetaQueryable> allowed = new HashSet<>();
		mq.metaLinks().forEach(ml->{
			allowed.add(ml);
			if(ml.source().equals(ml.target()))
				allowed.add(ml.source());
			else if(ml.source().equals(mq)){
				allowed.add(ml.target());
			}
			else{
				allowed.add(ml.source());
			}
		});

		//Choice view				
		choiceView = new ChoiceSet(theme, access, m -> allowed.contains(m));


		// Link Choice

		JLabel lblLinkType = new JLabel("Link Type : ");	
		ltChoice = new JLabel("   ...   ");

		JButton chooseBtn = new JButton("Select Type"); 
		chooseBtn.addActionListener(l->{

			Optional<MetaQueryable> opt = choiceView.getCurrentQueryable();
			if(!opt.isPresent()){
				JOptionPane.showMessageDialog(null, "There are no link selected in the choice view (left panel)");
				return;
			}
			if(opt.get() instanceof MetaClass){
				JOptionPane.showMessageDialog(null, "You should select a Link, not a class");
				return;
			}
						
			updateChoice((MetaLink)opt.get());

		});


		dirBox = new JComboBox<>(Direction.values());


		GroupPanel ltPanel = new GroupPanel(GroupingType.fillMiddle, 10, lblLinkType, ltChoice, chooseBtn, dirBox);


		// Filter on Link

		JLabel lblLinkFilter = new JLabel("Filter on Link");
		linkFilter = new JLabel("    None    ");
		JButton lFBtn = new JButton("Select Filter"); 		
		lFBtn.addActionListener(l->{

			if(ml == null){
				JOptionPane.showMessageDialog(null, "Please select a link type first (left panel)");
				return;
			}

			Optional<MetaFilter> opt = choiceView.getCurrentFilter(ml);
			if(!opt.isPresent()){
				JOptionPane.showMessageDialog(null, "There are no filter selected in the choice view (left panel)");
				return;
			}

			linkFilter.setText(opt.get().name());
			lPredicate = opt.get().toFilter();

		});
		
		JButton lFRmBtn = new JButton("Remove Filter"); 
		lFRmBtn.addActionListener(l->{
			if(lPredicate != null){
				lPredicate = null;
				linkFilter.setText("    None    ");
			}
		});
		
		GroupPanel lfPanel = new GroupPanel(GroupingType.fillMiddle, 10, lblLinkFilter, linkFilter, lFBtn, lFRmBtn);


		JLabel lblAdjFilter = new JLabel("Filter on Adjacent");
		adjFilter = new JLabel("    None    ");
		JButton aFBtn = new JButton("Select Filter"); 
		aFBtn.addActionListener(l->{

			if(ml == null){
				JOptionPane.showMessageDialog(null, "Please select a link type first (left panel)");
				return;
			}
			
			// Get the adjacent

			Optional<MetaFilter> opt = choiceView.getCurrentFilter(getCurrentAdjacent());
			if(!opt.isPresent()){
				JOptionPane.showMessageDialog(null,
						"There are no filter selected in the choice view -> select adjacent in types first "+getCurrentAdjacent()+")");
				return;
			}

			adjFilter.setText(opt.get().name());
			aPredicate = opt.get().toFilter();

		});
		
		JButton aFRmBtn = new JButton("Remove Filter"); 
		aFRmBtn.addActionListener(l->{
			if(aPredicate != null){
				aPredicate = null;
				adjFilter.setText("    None    ");
			}
		});
		
		
		GroupPanel afPanel = new GroupPanel(GroupingType.fillMiddle, 10, lblAdjFilter, adjFilter, aFBtn, aFRmBtn);


		GroupPanel linkPanel = new GroupPanel(10, false, ltPanel, lfPanel, afPanel);



		// Number Predicate Panel

		numberPredicateUI = new NumberPredicateUI<>(long.class);
		numberPredicateUI.addValidityListener(this);



		// ---------------------------  Layout  ---------------------------------------

		linkPanel.setBorder(new TitledBorder(null, "Links Definition", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		numberPredicateUI.setBorder(new TitledBorder(null, "Number of such connections", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		setLayout(new BorderLayout());	
		add(choiceView, BorderLayout.WEST);
		add(new GroupPanel(10, false, linkPanel, numberPredicateUI), BorderLayout.CENTER);



	}

	private MetaQueryable getCurrentAdjacent() {
		MetaQueryable adj = null;
		if(ml.source().equals(ml.target()))
			adj = ml.source();
		else if(ml.source().equals(target))
			adj = ml.target();
		else
			adj = ml.source();
		choiceView.highlightFilter(adj);
		return adj;
	}

	private void updateChoice(MetaLink ml) {

		//Set label text with link type
		ltChoice.setText(ml.itemDeclaredType());
		this.ml = ml;
		// Reset filters
		linkFilter.setText("   None   ");
		lPredicate = null;
		adjFilter.setText("   None   ");
		aPredicate = null;
		// update dir
		if(ml.source().equals(ml.target())){
			dirBox.setEnabled(true);
			dirBox.setSelectedIndex(2);
		}
		else if(ml.source().equals(target)){
			dirBox.setEnabled(false);
			dirBox.setSelectedIndex(1);
		}
		else {
			dirBox.setEnabled(false);
			dirBox.setSelectedIndex(0);
		}

	}





	@Override
	public boolean validity() {
		return isValid;
	}

	@Override
	public void nextValidityChanged(ValidityToggle wizardNode) {
		isValid = wizardNode.validity();
		this.fireNextValidityChanged();
	}



	@Override
	protected WizardNode getNextStep() {

		// Build the ConnectionSelector

		if(lPredicate == null)
			lPredicate = P.isDeclaredType(ml.itemDeclaredType());
		if(aPredicate == null)
			aPredicate = P.none();

		ExplicitPredicate<NodeItem> predicate = 
				new ConnectionSelector(
						(Direction)dirBox.getSelectedItem(),
						lPredicate,
						aPredicate)
				.count()
				.satisfies(numberPredicateUI.getPredicate());

		return new MetaDescriptionPanel<>(this, director, predicate, target);
	}

	@Override
	public int numberOfForks() {
		return 1;
	}
}

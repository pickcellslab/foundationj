package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.functions.AngleMaker;
import org.pickcellslab.foundationj.datamodel.functions.CombinedFunction;
import org.pickcellslab.foundationj.datamodel.functions.Constant;
import org.pickcellslab.foundationj.datamodel.functions.DistanceMaker;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.ValueSelector;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

import com.alee.extended.panel.GroupPanel;

public class VectorFunctionBuilderUI<T extends DataItem, N extends Number> extends WizardNode {

	private final UITheme theme;
	private final MetaQueryable target;

	private static final String ITEM = "Item Vector", SOURCE = "Source Vector",
			LINK = "Link Vector", TARGET = "Target Vector", CONSTANT = "Manual Entry";

	private	ExplicitFunction<T,N[]> left = null;
	private	ExplicitFunction<T,N[]> right = null;
	private int length;

	private JComboBox opBox;

	private final MetaInstanceDirector director;

	public VectorFunctionBuilderUI(WizardNode parent, UITheme theme, MetaInstanceDirector director, MetaQueryable target) {
		super(parent);

		this.theme = theme;
		this.target = target;
		this.director = director;
		

		//check if the target is an item or a link
		boolean isClass = false;
		if(MetaClass.class.isAssignableFrom(target.getClass()))
			isClass = true;


		JComboBox<String> leftKeyBox = new JComboBox<>();
		leftKeyBox.addItem(" ");
		if(isClass)
			leftKeyBox.addItem(ITEM);
		else{
			leftKeyBox.addItem(SOURCE);
			leftKeyBox.addItem(LINK);
			leftKeyBox.addItem(TARGET);
		}

		// JLabel angleLabel = new JLabel(getAngleIcon());

		JComboBox<String> rightBox = new JComboBox<>();
		rightBox.addItem(" ");
		rightBox.addItem(CONSTANT);
		if(isClass)
			rightBox.addItem(ITEM);
		else{
			rightBox.addItem(SOURCE);
			rightBox.addItem(LINK);
			rightBox.addItem(TARGET);
		}


		opBox = new JComboBox<>(new String[]{"Angle", "Distance"});
		opBox.setToolTipText("Select an operator");



		leftKeyBox.addActionListener(l->{
			if(leftKeyBox.getSelectedIndex() != 0){
				ExplicitFunction df = updateLeft((String) leftKeyBox.getSelectedItem());
				if(df!=null){
					left = df;	
					leftKeyBox.insertItemAt(left.toString(), 0);
					leftKeyBox.removeItemAt(1);								
				}
				else{
					leftKeyBox.insertItemAt(" ", 0);
					leftKeyBox.removeItemAt(1);
				}
				leftKeyBox.setSelectedIndex(0);								
			}
			this.fireNextValidityChanged();
		});


		rightBox.addActionListener(l->{
			if(rightBox.getSelectedIndex() != 0){
				ExplicitFunction df = updateLeft((String) rightBox.getSelectedItem());
				if(df!=null){
					right = df;
					rightBox.insertItemAt(right.toString(), 0);
					rightBox.removeItemAt(1);					
				}else{
					rightBox.insertItemAt(" ", 0);
					rightBox.removeItemAt(1);
				}
				rightBox.setSelectedIndex(0);
			}
			this.fireNextValidityChanged();
		});



		// 	Layout
		//--------------------------------------------------------------------------------------------------------------

		GroupPanel gp = new GroupPanel(10, leftKeyBox, opBox, rightBox);
		gp.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.add(gp, BorderLayout.CENTER);



	}







	private ExplicitFunction updateLeft(String owner) {

		AtomicBoolean choice = new AtomicBoolean(false);

		//Get the owner of the MetaKeys to select
		MetaQueryable mq = null;
		switch(owner){
		case ITEM : mq = target; break;
		case LINK : mq = target; break;
		case SOURCE : mq = ((MetaLink) target).source(); break;
		case TARGET : mq = ((MetaLink) target).target(); break;
		case CONSTANT : 
			
			//fakeKey for a ValueSelector
			AKey<?> fake = AKey.get("fake", double[].class);
			@SuppressWarnings({ "rawtypes", "unchecked" })
			ValueSelector<?> vs = new ValueSelector(fake);
			vs.setModal(true);
			vs.setVisible(true);

			Object value = vs.getValue();
			if(null == value){
				return null;
			}
			return new Constant<>(value);	
		}



		ReadableSelection ui = new ReadableSelection(theme, director.registry(), 
				mq.getReadables().filter(p->p.isArray() && p.dataType() == dType.NUMERIC).collect(Collectors.toList()), true, true);
		ui.setModal(true);
		ui.pack();
		ui.setLocationRelativeTo(this);		
		ui.setVisible(true);

		ExplicitFunction df = null;

		Optional<Dimension> opt = (Optional)ui.getSelected();

		System.out.println("Optional selected = "+opt);

		if(opt.isPresent()){

			length = opt.get().length();

			switch(owner){
			case ITEM : df = opt.get(); break;
			case LINK : df = opt.get(); break;
			case SOURCE : df = F.applyOnSource((ExplicitFunction)opt.get()); break;
			case TARGET : df = F.applyOnTarget((ExplicitFunction)opt.get()); break;			
			}
			
			System.out.println("df = "+df.description());
		}

		return df;		
	}





	@Override
	public boolean validity() {
		return left != null && right != null;
	}

	@Override
	protected WizardNode getNextStep() {

		if(opBox.getSelectedIndex() == 0)		
			return new MetaDescriptionPanel<>(this, director, new CombinedFunction<>(left,right, new AngleMaker()), -1, target);
		else
			return new MetaDescriptionPanel<>(this, director, new CombinedFunction<>(left,right, new DistanceMaker()), -1, target);
	}

	@Override
	public int numberOfForks() {
		return 1;
	}

}

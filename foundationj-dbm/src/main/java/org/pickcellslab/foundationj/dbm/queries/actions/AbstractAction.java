package org.pickcellslab.foundationj.dbm.queries.actions;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;

public abstract class AbstractAction<I, O> implements Action<I, O> {

	private final String description;
	private final Map<Object, ActionMechanism<I,O>> coords = new HashMap<>();

	public AbstractAction(String description) {
		this.description = description;
	}
	
	
	@Override
	public final synchronized ActionMechanism<I,O> createCoordinate(Object key) throws DataAccessException {
		ActionMechanism<I,O> action = create(Objects.toString(key));
		coords.put(key, action);
		return action;
	}
	
	protected abstract ActionMechanism<I,O> create(String key);
	

	@Override
	public final Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public final O createOutput(Object key) throws IllegalArgumentException {
		final ActionMechanism<I,O> mecha = coords.get(key);
		if(mecha == null)	return null;
		return mecha.get();
	}

	@Override
	public String description() {
		return description;
	}

}

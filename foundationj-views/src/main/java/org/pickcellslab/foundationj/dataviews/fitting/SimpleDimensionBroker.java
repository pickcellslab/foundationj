package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.mvc.DimensionBroker;

public class SimpleDimensionBroker<T,D> implements DimensionBroker<T,D> {

	protected final List<Dimension<T,D>> containers;
	
	public SimpleDimensionBroker(List<Dimension<T,D>> containers) {
		Objects.requireNonNull(containers);
		if(containers.isEmpty())
			throw new IllegalArgumentException("An empty list of containers was provided");
		this.containers = containers;
	}
	
	
	@Override
	public int numDimensions() {
		return containers.size();
	}

	@Override
	public Dimension<T,D> dimension(int i) {				
		return containers.get(i);
	}

	
}

package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Supplier;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class SplitIn2Panel<T extends Supplier<MetaFilter> & Highlightable> extends SplitterUI<SplitIn2Panel<T>> {


	private final UITheme theme;
	
	private final JTextField acceptedField;
	private final JTextField rejectedField;

	
	private MetaFilter mf;
	private JLabel lblName;
	private JTextPane textPane;



	public  SplitIn2Panel(UITheme theme, T filterSupplier) {

		Objects.requireNonNull(filterSupplier);

		this.theme = theme;
		
		//Controllers		

		textPane = new JTextPane();
		textPane.setContentType("text/html");

		JLabel lblAccepted = new JLabel("Accepted");

		acceptedField = new JTextField();
		acceptedField.setColumns(10);
		//TODO listener to detect empty string and equal to accepted and rejected

		JLabel lblRejected = new JLabel("Rejected");

		rejectedField = new JTextField();
		rejectedField.setColumns(10);


		lblName = new JLabel("None");

		JButton btnChange = new JButton("Change");
		btnChange.addActionListener(l->{
			MetaFilter mf = filterSupplier.get();
			if(mf == null){
				JOptionPane.showMessageDialog(null, "No filter selected");
				filterSupplier.setHighlighted(true);
				lblName.setIcon(null);
				lblName.setText("None");
				textPane.setText("");
				acceptedField.setText("");
				rejectedField.setText("");
				mf = null;
				this.fireNextValidityChanged();
			}
			else{
				this.mf = mf;
				lblName.setIcon(theme.icon(IconID.Queries.FILTER, 16));
				lblName.setText(mf.getAttribute(MetaFilter.name).get());
				acceptedField.setText(mf.getAttribute(MetaFilter.name).get()+" +");
				rejectedField.setText(mf.getAttribute(MetaFilter.name).get()+" -");
				textPane.setText(mf.toHTML());
				this.fireNextValidityChanged();
			}
		});





		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(lblName)
										.addPreferredGap(ComponentPlacement.RELATED, 185, Short.MAX_VALUE)
										.addComponent(btnChange))
								.addComponent(textPane, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(lblAccepted)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(acceptedField, GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(lblRejected)
										.addGap(15)
										.addComponent(rejectedField, GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)))
						.addGap(24))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblName)
								.addComponent(btnChange))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(textPane, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblAccepted)
								.addComponent(acceptedField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblRejected)
								.addComponent(rejectedField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(29, Short.MAX_VALUE))
				);
		setLayout(groupLayout);
	}

	@Override
	public boolean validity() {
		return mf!=null && !acceptedField.getText().isEmpty() && acceptedField.getText()!= rejectedField.getText();
	}

	@Override
	MetaSplit getSplitter(Meta meta) {
		if(mf==null)
			throw new IllegalStateException("Validity is false");
		try {
			return MetaBinarySplitter.getOrCreate(meta.getInstanceManager(), mf, acceptedField.getText(), rejectedField.getText());
		} catch (InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}


	public String getAcceptedName(){
		return acceptedField.getText();
	}

	public String getRejectedName(){
		return rejectedField.getText();
	}
	
	public MetaFilter getPredicate(){
		return mf;
	}

	@Override
	public void copyState(SplitIn2Panel<T> other) {
		
		this.mf = other.mf;
		if(mf!=null){
			lblName.setIcon(theme.icon(IconID.Queries.FILTER, 16));
			lblName.setText(mf.getAttribute(MetaFilter.name).get());
			acceptedField.setText(mf.getAttribute(MetaFilter.name).get()+" +");
			rejectedField.setText(mf.getAttribute(MetaFilter.name).get()+" -");
			textPane.setText(mf.toHTML());
			this.fireNextValidityChanged();
		}
		
	}


	
	
	
}

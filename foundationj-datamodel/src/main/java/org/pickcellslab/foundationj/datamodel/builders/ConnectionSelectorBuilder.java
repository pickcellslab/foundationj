package org.pickcellslab.foundationj.datamodel.builders;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.functions.ConnectionSelector;
import org.pickcellslab.foundationj.datamodel.functions.Constant;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.DataTransform;
import org.pickcellslab.foundationj.datamodel.functions.KeySelector;
import org.pickcellslab.foundationj.datamodel.predicates.BiFunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.FilteringBehaviour.Behaviour;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.datamodel.predicates.SimpleFilter;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

/**
 * A builder class to construct a {@link ConnectionSelector} object
 * 
 * @author Guillaume Blin
 *
 */
public class ConnectionSelectorBuilder {

	private SimpleFilter<Link> pOnLink;
	private SimpleFilter<NodeItem> pOnAdjacent;
	private Direction d = Direction.BOTH;
	
	public ConnectionSelectorBuilder(){
		
	}
	
	
	@SafeVarargs
	public final FirstTransition includeLinks(Direction d, String... types){
		this.d = d;
		pOnLink = new SimpleFilter<>(Behaviour.INCLUDE,  types);
		return new FirstTransition(this, pOnLink);
	}
	
	
	@SafeVarargs
	public final FirstTransition excludeLinks(Direction d, String... types){
		this.d = d;
		pOnLink = new SimpleFilter<>(Behaviour.EXCLUDE,  types);
		return new FirstTransition(this, pOnLink);
	}
	
	
	/**
	 * @return A new {@link ConnectionSelector} configurd with the options previously defined
	 */
	private ConnectionSelector create(){
		if(pOnLink == null)
			pOnLink = new SimpleFilter<>(Behaviour.INCLUDE);
		if(pOnAdjacent == null)
			pOnAdjacent = new SimpleFilter<>(Behaviour.INCLUDE);
		
		return 	new ConnectionSelector(d, pOnLink, pOnAdjacent);	
	}
	



	/**
	 * An Intermediate in the process of building a {@link SimpleFilter}
	 * @author Guillaume Blin
	 *
	 */
	public static class FirstTransition{

		private SimpleFilter<Link> f;
		private ConnectionSelectorBuilder builder;
		
		private FirstTransition(ConnectionSelectorBuilder builder, SimpleFilter<Link> f){
			this.f = f;
			this.builder = builder;
		}
		
		public <L,R> NextTransition with(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<Link,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(null, p);
			return new NextTransition(builder, f);
		}
		public NextTransition with(DataTransform<Link> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<Link,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(null, p);
			return new NextTransition(builder, f);
		}
		
		
		public TheOneBeforeLastTransition whereAdjacent(){
			return new TheOneBeforeLastTransition(builder);
		}
		
		public ExplicitFunction<NodeItem, Stream<Link>> ifAcceptedBy(ExplicitPredicate<? super Link> filter) {
			return new ConnectionSelector(builder.d, builder.pOnLink.addConstrainsts(Op.Bool.AND, filter), new SimpleFilter<>(Behaviour.INCLUDE));
		}
		
		
		public ConnectionSelector create() {
			return builder.create();
		}

		


	}

	/**
	 * An Intermediate in the process of building a {@link SimpleFilter}
	 * @author Guillaume Blin
	 */
	public static class NextTransition{

		private SimpleFilter<Link> f;
		private ConnectionSelectorBuilder builder;
		
		private NextTransition(ConnectionSelectorBuilder builder, SimpleFilter<Link> f){
			this.f = f;
			this.builder = builder;
		}
		
		public <L,R> NextTransition OR(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<Link,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.OR, p);
			return this;
		}
		public NextTransition OR(DataTransform<Link> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<Link,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.OR, p);
			return this;
		}

		public <L,R> NextTransition XOR(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<Link,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.XOR, p);
			return this;
		}
		public NextTransition XOR(DataTransform<Link> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<Link,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.XOR, p);
			return this;
		}
		public <L,R> NextTransition AND(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<Link,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.AND, p);
			return this;
		}
		public NextTransition AND(DataTransform<Link> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<Link,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.AND, p);
			return this;
		}

		
		public TheOneBeforeLastTransition andAdjacent(){			
			return new TheOneBeforeLastTransition(builder);
		}
		
		public ConnectionSelector create() {
			return builder.create();
		}

		

	}
	
	
	
	public static class TheOneBeforeLastTransition{
		
		private ConnectionSelectorBuilder builder;
		
		TheOneBeforeLastTransition(ConnectionSelectorBuilder builder){
			this.builder = builder;
		}
		
		
		@SafeVarargs
		public final BeforeLastTransition include(Class<? extends NodeItem>... classes){
			builder.pOnAdjacent = new SimpleFilter<>(Behaviour.INCLUDE, classesToTypeIds(classes));
			return new BeforeLastTransition(builder, builder.pOnAdjacent);
		}
		@SafeVarargs
		public final BeforeLastTransition exclude(Class<? extends NodeItem>... classes){
			builder.pOnAdjacent = new SimpleFilter<>(Behaviour.EXCLUDE, classesToTypeIds(classes));
			return new BeforeLastTransition(builder, builder.pOnAdjacent);
		}
		
		
		
		@SafeVarargs
		private final String[] classesToTypeIds(Class<? extends NodeItem>... classes){		
			String[] strings = new String[classes.length];
			for(int i = 0; i<classes.length; i++)
				strings[i] = DataRegistry.typeIdFor((Class<? extends NodeItem>) classes[i]);
			return strings;
		}
		
		
	}
	
	
	public static class BeforeLastTransition{
		
		
		private SimpleFilter<NodeItem> f;
		private ConnectionSelectorBuilder builder;
		
		private BeforeLastTransition(ConnectionSelectorBuilder builder, SimpleFilter<NodeItem> f){
			this.f = f;
			this.builder = builder;
		}
		
		public <L,R> FinalTransition with(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<NodeItem,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(null, p);
			return new FinalTransition(builder, f);
		}
		public FinalTransition with(DataTransform<NodeItem> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<NodeItem,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(null, p);
			return new FinalTransition(builder, f);
		}
		
		public ConnectionSelector create() {
			return builder.create();
		}
		
	}
	
	
	public static class FinalTransition{
		
		private ConnectionSelectorBuilder builder;
		private SimpleFilter<NodeItem> f;

		FinalTransition(ConnectionSelectorBuilder builder, SimpleFilter<NodeItem> f){
			this.builder = builder;
			this.f = f;
		}
		
		
		
		
		public <L,R> FinalTransition OR(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<NodeItem,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.OR, p);
			return this;
		}
		public FinalTransition OR(DataTransform<NodeItem> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<NodeItem,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.OR, p);
			return this;
		}

		public <L,R> FinalTransition XOR(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<NodeItem,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.XOR, p);
			return this;
		}
		public FinalTransition XOR(DataTransform<NodeItem> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<NodeItem,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.XOR, p);
			return this;
		}
		public <L,R> FinalTransition AND(AKey<  ? extends L> k, ExplicitOperator<L,R> o, R value){
			BiFunctionPredicate<NodeItem,L,R> p = new BiFunctionPredicate<>(
					new KeySelector<>(k, null)
					, new Constant<>(value), 
					o);
			f.addConstrainsts(Op.Bool.AND, p);
			return this;
		}
		public FinalTransition AND(DataTransform<NodeItem> t, ExplicitOperator<Double,Double> o, double value){
			BiFunctionPredicate<NodeItem,Double,Double> p = new BiFunctionPredicate<>(
					t, new Constant<>(value), o);
			f.addConstrainsts(Op.Bool.AND, p);
			return this;
		}
		
		
		public ConnectionSelector create(){
			return builder.create();
		}
		
	}
	
	
}

package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Specifies how an element of <a href="https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)"> graph </a> 
 * (<a href="https://en.wikipedia.org/wiki/Vertex_(graph_theory)">node</a> or Link) should be regarded to
 * define the outcome of a <a href="https://en.wikipedia.org/wiki/Graph_traversal"> traversal </a>
 *
 */
public enum Decision{
	INCLUDE_AND_STOP,
	INCLUDE_AND_CONTINUE,
	EXCLUDE_AND_STOP,
	EXCLUDE_AND_CONTINUE
}
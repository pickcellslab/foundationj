package org.pickcellslab.foundationj.datamodel;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;

/**
 * {@link DataItem}s are meant to be stored to the database. They provide public access to their list of {@link AKey} / value pairs 
 *<br><br>
 *<b>NB: </b>Implementing class must be annotated with {@link Data}.
 *
 * @see WritableDataItem
 * @see Link
 * @see NodeItem
 */
public interface DataItem{

	
	public default String typeId() {
		return getClass().getAnnotation(Data.class).typeId();
	}

	
	/**
	 * @return A Collection of {@link AKey} objects which indicate the minimal list of attributes which must be 
	 * retrieved from the database when reconstructing this DataItem. The list must contain {@link DataItem#idKey}. 
	 * This method should only be useful to the persistence module.
	 */
	public Stream<AKey<?>> minimal();


	/**
	 * This key ("Database ID", Integer.class) is used to obtain the id of this DataItem into the database
	 * Do not remove or set an attribute using this key, the Persistence module is the only module allowed to
	 * set or change this attribute
	 */
	public static final AKey<Integer> idKey = AKey.get("Database ID", Integer.class);
	
	
		
	/**
	 * @return The declared type of this item, in other words a String which represents the concept this instance 
	 * defines (i.e: person, nucleus, protein, etc...) or type of relationships if this DataItem is a {@link Link}.
	 * Different instances of a same class can possess different {@link #declaredType()}, however this string should never change
	 * throughout the life span of the instance.
	 */
	public String declaredType();

	/**
	 * @return A Stream of all the valid AttributeKey objects for this
	 *         DataModelItem Only the listed keys will be stored into the database
	 *         Note: It is the responsibility of the implementing class to ensure
	 *         the consistency between valid keys and keys actually stored into
	 *         the object
	 */
	public Stream<AKey<?>> getValidAttributeKeys();

	/**
	 * @param key
	 * @return An {@link Optional} holding the value for the specified key
	 */
	public <T>  Optional<T> getAttribute(AKey<T> key);
	
	
}

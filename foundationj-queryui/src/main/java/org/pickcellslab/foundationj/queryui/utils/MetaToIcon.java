package org.pickcellslab.foundationj.queryui.utils;

import java.util.Objects;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.dimensions.DataTyped;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaDataSet;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.dbm.meta.MetaTraversal;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.DataToIconID;

public final class MetaToIcon {


	public static Icon get(MetaModel value, DataRegistry registry, UITheme theme, int size){

		if(value == null)
			return null;
		
		Objects.requireNonNull(registry);
		Objects.requireNonNull(theme);
		
		Icon icon = null;

		if(MetaReadable.class.isAssignableFrom(value.getClass()))
			icon = theme.icon(DataToIconID.get(((DataTyped)value)), size);

		else if(MetaClass.class.isAssignableFrom(value.getClass())) {
			Class<? extends NodeItem> clazz = ((MetaClass) value).itemClass(registry);
			icon = DataToIconID.getIcon(theme, clazz, size);
			if(icon == null)
				icon = theme.icon(IconID.Data.NODE, size);
		}
		else if(MetaLink.class.isAssignableFrom(value.getClass()))
			icon = theme.icon(IconID.Data.LINK, size); 
		else if(MetaTag.class.isAssignableFrom(value.getClass()))
			icon = theme.icon(IconID.Misc.TAG, size); 
		else if(MetaDataSet.class.isAssignableFrom(value.getClass()))
			icon = theme.icon(IconID.Misc.DATA_TABLE, size); 
		else if(MetaFilter.class.isAssignableFrom(value.getClass()))
			icon = theme.icon(IconID.Queries.FILTER, size); 
		else if(MetaTraversal.class.isAssignableFrom(value.getClass()))
			icon = theme.icon(IconID.Queries.PATH, size); 

		return icon;
	}



}

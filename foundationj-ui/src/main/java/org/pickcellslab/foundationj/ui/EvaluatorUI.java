package org.pickcellslab.foundationj.ui;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Font;
import java.util.Objects;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.pickcellslab.foundationj.ui.wizard.Copyable;

import com.fathzer.soft.javaluator.AbstractEvaluator;
import com.fathzer.soft.javaluator.StaticVariableSet;

public class EvaluatorUI<E extends AbstractEvaluator<T>, T> implements Copyable<EvaluatorUI<E,T>>{


	//UI fields
	private JScrollPane scrollPane = new JScrollPane();
	private JTextPane textPane;
	private StyledDocument doc;
	private Style style;



	public static final Color number = Color.BLACK, parenthesis = new Color(77,166,25), funct = Color.ORANGE,
			key = Color.BLUE, constant = new Color(188,28,57), operator = new Color(93,100,90);

	//Flag keeping track of whether or not the current expression is valid
	private boolean textIsValid;

	//The evaluator in charge of interpreting the expression
	private final E evaluator;

	//A map of the symbols in the expression and variables associated with them
	private final StaticVariableSet<? super T> variables;

	//The final function to return
	private T function;



	public EvaluatorUI(E evaluator, StaticVariableSet<? super T> variables){

		Objects.requireNonNull(evaluator);
		this.evaluator = evaluator;
		
		Objects.requireNonNull(variables);
		this.variables = variables;
		
		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		textPane.setEditable(false);
		Font font = new Font("Serif", Font.BOLD, 16);
		textPane.setFont(font);

		doc = textPane.getStyledDocument();
		style = textPane.addStyle("Expression Style", null);

	}

	public JScrollPane getDisplay(){
		return scrollPane;
	}



	public boolean insert(String symbol, T variable, Color color){

		StyleConstants.setForeground(style, color);

		try { 
			doc.insertString(doc.getLength(), symbol, style); 
			variables.set(symbol, variable);
			//update isValid
			return checkValid();
		}
		catch (BadLocationException e){
			e.printStackTrace();
		}
		return textIsValid;
	}


	public boolean insert(String constant, Color color){

		StyleConstants.setForeground(style, color);

		try { 
			doc.insertString(doc.getLength(), constant, style); 
			//update isValid
			return checkValid();
		}
		catch (BadLocationException e){
			e.printStackTrace();
		}
		return textIsValid;
	}


	public void delete(String text){

		if(text!=null)
			try {
				doc.remove(doc.getLength()-text.length(),text.length());			

				//update isValid
				checkValid();

			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		//textPane.select(doc.getLength()-text.length(),text.length());
		//textPane.replaceSelection("");
	}


 public String getExpression(){
	 return textPane.getText();
 }

	public T getFunction() throws IllegalStateException{
		if(!textIsValid)
			throw new IllegalStateException("The current expression is not valid");
		return function;
	}

	private boolean checkValid() {
		try{
			function = evaluator.evaluate(textPane.getText());
			this.textIsValid = true;
			return true;
		}
		catch(IllegalArgumentException | NullPointerException i){
			System.out.println("Not valid");
			this.textIsValid = false;
			return false;
		}
	}

	public boolean isValid() {
		return textIsValid;
	}

	@Override
	public void copyState(EvaluatorUI<E, T> other) {
		textPane.setText(other.textPane.getText());
		this.checkValid();
	}

}

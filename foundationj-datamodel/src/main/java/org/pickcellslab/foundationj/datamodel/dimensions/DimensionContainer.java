package org.pickcellslab.foundationj.datamodel.dimensions;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;


/**
 * A container of {@link Dimension}(s)
 * 
 * @author Guillaume Blin
 *
 * @param <T> The input type accepted by the {@link Dimension} this DimensionContainer can generate
 * @param <D> The output type of the {@link Dimension} this DimensionContainer can generate
 */
public interface DimensionContainer<T,D> {

	
	/**
	 * 
	 * An enum to determine if a {@link DimensionContainer} should be used to obtain {@link Dimension} in raw format (an array usually)
	 * or decomposed.
	 * 
	 * @author Guillaume Blin
	 *
	 */
	public enum Mode{
		DECOMPOSED, RAW
	}
	
	
	public Dimension<T,?> getRaw();
	
	/**
	 * @return The number of {@link Dimension} this {@link DimensionContainer} can create
	 */
	public int numDimensions();

	/** 
	 * @param index The index of the desired {@link Dimension}
	 * @return The Dimension located at the specified index
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public Dimension<T,D> getDimension(int index) throws ArrayIndexOutOfBoundsException;

	
	
	public static <I,O> DimensionContainer<I,O> fromOneDimension(Dimension<I,O> single){
		
		assert single != null;
		
		return new DimensionContainer<I,O>(){

			@Override
			public Dimension<I, ?> getRaw() {
				return single;
			}

			@Override
			public int numDimensions() {
				return 1;
			}

			@Override
			public Dimension<I, O> getDimension(int index) throws ArrayIndexOutOfBoundsException {
				if(index!=0)
						throw new ArrayIndexOutOfBoundsException("This dimension container only has one dimension, provided index : "+index); 
				return single;
			}
			
		};
	}
	
	
	/**
	 * @param d
	 * @return {@code true} if this {@link DimensionContainer} collects of compute the specified dimension
	 */
	public default boolean contains(Dimension<T,D> d){
		for(int i = 0; i<numDimensions(); i++)
			if(d.equals(getDimension(i)))
				return true;
		return false;
	};


	/**
	 * @param readables A List of {@link DimensionContainer} from which to extract the {@link Dimension}s
	 * @return A List containing all the {@link Dimension} that can be created by the given {@link DimensionContainer}
	 */
	public static <T,D extends DimensionContainer<T,R>,R> List<Dimension<T,R>> getAllDimensions(Collection<D> readables){
		if(readables == null) return Collections.emptyList();
		List<Dimension<T,R>> dims = new ArrayList<>();		
		for(D mr : readables)
			for(int i = 0 ; i<mr.numDimensions(); i++)
				dims.add(mr.getDimension(i));		
		return dims;
	}
	
	
	public static <T,D extends DimensionContainer<T,R>,R> List<Dimension<T,R>> getAllDimensions(List<D> readables, Predicate<? super Dimension<T,R>> p){
		if(readables == null) return Collections.emptyList();
		List<Dimension<T,R>> dims = new ArrayList<>();		
		for(D mr : readables)
			for(int i = 0 ; i<mr.numDimensions(); i++){
				Dimension<T,R> d = mr.getDimension(i);
				if(p.test(d))
					dims.add(d);
			}
		return dims;
	}
	
	
	
	/**
	 * Finds the MetaReadable associated with the given {@link Dimension}
	 * @param readables The List of {@link MetaReadable} to lookup
	 * @param d The Dimension of interest
	 * @return An {@link Optional} holding or not the result of the search.
	 */
	public static <T,D extends DimensionContainer<T,D>> Optional<D> findOwner(Collection<D> readables, Dimension<T,D> d){
		if(readables == null || d == null) return Optional.empty();
		return readables.stream().filter(p->p.contains(d)).findFirst();
	}

	
	
	
	
}

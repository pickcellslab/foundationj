package org.pickcellslab.foundationj.processors;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;



public class MappingProcessor extends FoundationJProcessor {




	protected boolean contractRespected(TypeElement type) {

		//--------------------------------------------------------------------//
		// First make sure that the annotation is on a concrete class
		//--------------------------------------------------------------------//

		ProcessorsUtils.checkConcrete(Mapping.class, type, processingEnv);

		
		 
		//Does this Mapping have a CustomNonDataConverter
		TypeElement defaultConverter = processingEnv.getElementUtils().getTypeElement(Mapping.Default.class.getCanonicalName());
		Mapping an = type.getAnnotation(Mapping.class);
				
		try {
			an.converter(); // see the 'getAnnotation' javadoc of above line
		}
		catch(MirroredTypeException t){
			//JOptionPane.showMessageDialog(null, "MappingProcessor: Exception thrown");

			TypeMirror tm = t.getTypeMirror();
			if(tm == null) {
				//JOptionPane.showMessageDialog(null, "MappingProcessor: Type Mirror is null");
				return false; // badly formulated converter option error will be thrown by compiler.
			}
			else {
				if(!processingEnv.getTypeUtils().asElement(tm).toString().equals(defaultConverter.toString())) {
					//JOptionPane.showMessageDialog(null, "MappingProcessor: custom converter in use");
					return true;
				}
			}
		}
		



		//Check all fields to see if they are supported
		boolean allValid = true;
		List<VariableElement> fields = ElementFilter.fieldsIn(type.getEnclosedElements());
		for (VariableElement field : fields) {
			boolean isValid = false;			
			if(field.getAnnotation(Ignored.class) != null) {
				isValid = true; 
			}
			else if(field.getAnnotation(Supported.class) != null) {
				isValid = true; 
			}
			else if(checkSupportedByDefault(field)) {
				isValid = true;
			}

			if(!isValid) {
				allValid = false;
			}
		}

		if(!allValid)
			throwError(type);


		return allValid;

	}



	private void throwError(Element el) {
		processingEnv.getMessager().printMessage(Kind.ERROR, "Fields of a class annotated with @Mapping \n "
				+ "must be annotated with either @Ignored or @Supported of from a type which is itself \n"
				+ "annotated with @Mapping or one of the types supported by default", el);
	}



	private boolean checkSupportedByDefault(VariableElement field) {

		// Is this an array?

		try {
			TypeMirror f = field.asType();
			if(TypeKind.ARRAY == f.getKind()) {
				// Component type is primitive?
				//JOptionPane.showMessageDialog(null, "MappingProcessor: Testing ARRAY - Type : "+field);

				if(this.isSupportedBaseType(((ArrayType) f).getComponentType()))
					return true;
				else {
					throwError(field);
				}
			}
			else { // Not an ARRAY


				if(this.isSupportedBaseType(f))
					return true;

				else {
					//Check for Collections

					if(f.getKind() == TypeKind.DECLARED) {

						//Cast
						DeclaredType declared = (DeclaredType)f;

						// Is this a Collection ?
						Types types = processingEnv.getTypeUtils();
						TypeMirror collectionMirror = processingEnv.getElementUtils().getTypeElement(Collection.class.getCanonicalName()).asType();

						if(types.isSubtype(declared, types.erasure(collectionMirror))) {

							// get the type parameter
							List<? extends TypeMirror> typeParams = declared.getTypeArguments();
							if(typeParams.size() == 0) {
								processingEnv.getMessager().printMessage(Kind.ERROR, "Fields of @Mapping class can be collections but "
										+ "the parameters are required", field);
								return false;
							}
							else if(this.isSupportedBaseType(typeParams.get(0)))
								return true;

						}//END of checking for Collection
						else {
							// Is this a Map ? 
							TypeMirror mapMirror = processingEnv.getElementUtils().getTypeElement(Map.class.getCanonicalName()).asType();

							if(types.isSubtype(declared, types.erasure(mapMirror))) {

								// get the type parameter
								List<? extends TypeMirror> typeParams = declared.getTypeArguments();
								if(typeParams.size() == 0) {
									processingEnv.getMessager().printMessage(Kind.ERROR, "Fields of @Mapping class can be Maps but "
											+ "the parameters are required", field);
									return false;
								}
								else {
									if(this.isSupportedBaseType(typeParams.get(0)) && this.isSupportedBaseType(typeParams.get(1)))
										return true;
								}
							}
						}//END of checking for Map

						// Reaching here means params are unsupported
						processingEnv.getMessager().printMessage(Kind.ERROR, "Fields of @Mapping class can be Maps or Collections but "
								+ "the parameters must be supported types", field);
						return false;

					}
				}
			}

		}catch(Exception e) {			
			//JOptionPane.showMessageDialog(null, "MappingProcessor: Error : "+e.toString());
			processingEnv.getMessager().printMessage(Kind.WARNING, "Unknown Exception while processing @Mapping annotation: "+e.toString(), field);
			return false; // other compilation problems 
		}


		return false;
	}





	private boolean isSupportedBaseType(TypeMirror tm) {

		//JOptionPane.showMessageDialog(null, "MappingProcessor: testing base type for "+tm.toString());

		// Primitives are supported
		if(tm.getKind().isPrimitive())
			return true;

		// Enums are supported
		if(processingEnv.getTypeUtils().asElement(tm).getKind() == ElementKind.ENUM)
			return true;

		//JOptionPane.showMessageDialog(null, "MappingProcessor: testing boxing for "+tm.toString());

		// Boxed types are supported
		final Element te = processingEnv.getTypeUtils().asElement(tm);

		switch(te.toString()) {
		case "java.lang.String": return true;
		case "java.lang.Double": return true;
		case "java.lang.Float": return true;
		case "java.lang.Long": return true;
		case "java.lang.Integer": return true;
		case "java.lang.Short": return true;
		case "java.lang.Byte": return true;
		case "java.lang.Boolean": return true;
		case "java.lang.Character": return true;
		}


		//JOptionPane.showMessageDialog(null, "MappingProcessor: Not supported base type");

		// if not supported base, is it @Mapping annotated
		if(processingEnv.getTypeUtils().asElement(tm).getAnnotation(Mapping.class) != null) {
			return true;        	   
		}


		return false;
	}





	@Override
	protected Class<? extends Annotation> annotation() {
		return Mapping.class;
	}


	@Override
	protected String metadatapath() {
		return "META-INF/foundationj/mapping";
	}

}
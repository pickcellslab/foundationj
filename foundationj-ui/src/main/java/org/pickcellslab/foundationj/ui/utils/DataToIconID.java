package org.pickcellslab.foundationj.ui.utils;

import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.dimensions.DataTyped;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;

/**
 * 
 * Provides static methods useful to obtain an {@link IconID} for particular types of data objects
 * 
 * @author Guillaume Blin
 *
 */
public final class DataToIconID {

	private DataToIconID(){}


	public static IconID get(DataTyped type) {
		return get(type.dataType());
	}

	public static IconID get(dType type) {
		switch(type){
		case BOOLEAN : return IconID.Data.BOOLEAN;
		case NUMERIC : return IconID.Data.NUMERIC;
		case TEXT : return IconID.Data.TEXT;
		case MIXED: return IconID.Data.MISC;
		default: return null;     
		}
	}



	public static IconID get(Decision dcs) {
		switch(dcs){
		case EXCLUDE_AND_CONTINUE:  return IconID.Data.EXCLUDE_CONTINUE;
		case EXCLUDE_AND_STOP:  return IconID.Data.EXCLUDE_STOP;
		case INCLUDE_AND_CONTINUE:  return IconID.Data.INCLUDE_CONTINUE;
		case INCLUDE_AND_STOP:  return IconID.Data.INCLUDE_STOP;
		default: return null;     
		}
	}
	
	
	public static IconID get(Direction dir) {
		switch(dir){
		case INCOMING:  return IconID.Data.INCOMING;
		case OUTGOING:  return IconID.Data.OUTGOING;
		case BOTH:  return IconID.Data.BOTH;
		default: return null;     
		}
	}
	
	
	public static IconID get(DataItem data) {		
		return get(data.getClass());
	}
	
	public static IconID get(Class<? extends DataItem> data) {		
		if(NodeItem.class.isAssignableFrom(data))
			return IconID.Data.NODE;
		else if(Link.class.isAssignableFrom(data))
			return IconID.Data.LINK; 
		return IconID.Misc.QUESTION;
	}
	
	
	
	public static Icon getIcon(UITheme theme, Class<? extends DataItem> data, int size) {
		
		final Data d = data.getAnnotation(Data.class);
		
		if(d != null) {
			final String str = d.icon();
			if(!str.isEmpty()) {
				final URL url = data.getResource(str);
				if(url != null)
					return UITheme.resize(new ImageIcon(url), size, size);
			}
		}
		
		return theme.icon(get(data), size);
	}
	
	

}

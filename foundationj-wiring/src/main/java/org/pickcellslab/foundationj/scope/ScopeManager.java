package org.pickcellslab.foundationj.scope;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.foundationj.annotations.Service;

/**
 * 
 * The {@link ScopeManager} takes care of the configuration of the application {@link Scope}d containers. It exposes methods to 
 * initialise new {@link RuntimeScope} instances, to control their lifecycle and to obtain a list of active {@link RuntimeScope}s.
 * <br><br>
 * <b>NB:</b> This is neither a {@link Service}, nor a {@link Core} nor a {@link Modular} interface. A class cannot be
 * injected with a ScopeManager unless this class is an {@link AppController}.
 * <br>
 * Do not Implement, the framework will provide an implementation at runtime.
 * 
 * @author Guillaume Blin
 *
 */

public interface ScopeManager {

	
	public String getRootPath();
	
	public RuntimeScope getApplicationScope();
	
	public void addScopeEventListener(ScopeEventListener l);
	
	/**
	 * @param instance An instance of a class implementing an interface annotated with ethier {@link Service}, {@link Core} or {@link Modular}.
	 * @return The {@link RuntimeScope} containing the given instance or null if the instance was not registered
	 * as a component to any currently active {@link RuntimeScope}
	 */
	public RuntimeScope getScope(Object instance);
	
	/**
	 * @param name
	 * @param id
	 * @return The currently active {@link RuntimeScope} given the provided name and id or null if not found.
	 */
	public RuntimeScope getScope(String name, String id);
	
	/**
	 * Creates a new {@link RuntimeScope} instance if it is possible to do so. This will depend on the {@link ScopePolicy} of the
	 * given scope, whether this scope exists or if its parent is already active.
	 * @param scope The name of the scope for which to create a new instance
	 * @param id An id for the new instance. Exception will be thrown if this id is already taken
	 * @param parent The Scope which should be the parent of the new Scope instance.
	 * @return The new {@link RuntimeScope Instance}
	 * @throws RuntimeException
	 */
	public RuntimeScope newScopeInstance(String scope, String id, RuntimeScope parent) throws RuntimeException;

	public void startScope(RuntimeScope scope, Object... options) throws ScopeConfigurationException, Exception;
	
	public void stopScope(RuntimeScope scope);

	/**
	 * @param scope The desired scope name
	 * @return A {@link Stream} of the currently active {@link RuntimeScope} instances
	 */
	public Stream<RuntimeScope> activeScopeInstances(String scope);
	
		
	public void stopAllAndExit();

	

	
}

package org.pickcellslab.foundationj.dbm.impl;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaStrategy;
import org.pickcellslab.foundationj.dbm.meta.MetaTraversal;
import org.slf4j.LoggerFactory;

abstract class AbstractMetaQueryable extends MetaItem implements MetaQueryable {



	protected AbstractMetaQueryable(String uuid) {
		super(uuid);
	}




	void addKey(MetaKeyImpl<?> mk) {
		if(keys().anyMatch(k->k.equals(mk))){
			LoggerFactory.getLogger(AbstractMetaQueryable.class).warn("Attempting to add a MetaKey which already exists "+mk);
		}
		else
			new DataLink(MetaModel.keyLink, this, mk, true);
	}




	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <E> Optional<MetaKey<E>> metaKey(AKey<E> k){
		return (Optional)this.getLinks(Direction.OUTGOING, MetaModel.keyLink)
				.map(l -> (MetaKey)l.target())
				.filter(mk->{
					return  mk.toAKey().equals( k );
				})
				.findFirst();
	}



	@Override
	public Stream<MetaKey<?>> keys() {
		return this.getLinks(Direction.OUTGOING, MetaModel.keyLink)
				.map(l -> (MetaKey<?>)l.target());    
	}

	/**
	 * @param k The AKey object the presence is to be tested
	 * @return {@code true} if this MetaQueyable possesses a MetaKey representing the specified {@link AKey},
	 * {@code} false otherwise.
	 */
	@Override
	public boolean hasKey(AKey<?> k){
		return keys().anyMatch(mk->mk.toAKey().equals(k));
	}




	@Override
	public Stream<MetaReadable<?>> getReadables(){ 
		final Set<Object> seen = ConcurrentHashMap.newKeySet();
		return Stream.concat(
				getLinks(Direction.INCOMING, MetaReadable.APPLICABLE_TO)
				.map(l->l.source())
				,
				getLinks(Direction.OUTGOING, MetaQueryable.keyLink)
				.map(l->l.target())
				)
				.filter(t->MetaReadable.class.isAssignableFrom(t.getClass()))
				.filter(t->seen.add(t))
				.map(t->(MetaReadable<?>)t);

	}






	/**
	 * @return A Stream holding all the {@link MetaFilter} which can be used on this MetaQueryable
	 */
	@Override
	public Stream<MetaFilter> filters(){
		final Set<Link> set = typeMap.get(MetaStrategy.POINTS_TO);
		if(set==null)return Stream.empty();
		else return set.stream().map(l->l.source())
				.filter(s->MetaFilter.class.isAssignableFrom(s.getClass()))
				.map(s->(MetaFilter)s)
				.filter(mf->!mf.isTransient());
	}

	/**
	 * @return A Stream holding all the {@link MetaTraversal} which can be used on this MetaQueryable
	 */
	@Override
	public Stream<MetaTraversal> traversals(){
		final Set<Link> set = typeMap.get(MetaStrategy.POINTS_TO);
		if(set==null)return Stream.empty();
		else return set.stream().map(l->l.source())
				.filter(s->MetaTraversal.class.isAssignableFrom(s.getClass()))
				.map(s->(MetaTraversal)s);
	}


	@Override
	public int deleteData(DataAccess access) throws DataAccessException {
		return initiateDeletion(access).completely().getAll().run();
	}




}

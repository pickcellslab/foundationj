package org.pickcellslab.foundationj.datamodel.functions;

import java.lang.reflect.Array;
import java.util.Arrays;

import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "Rotation")
public class VectorRotation<T> implements ExplicitFunction<T,double[]>{


	@Supported
	private final ExplicitFunction<T,?> v1, v2, vector;

	public VectorRotation(ExplicitFunction<T,?> vz, ExplicitFunction<T,?> vy, ExplicitFunction<T,?> vector) {
		MappingUtils.exceptionIfNullOrInvalid(vz);
		MappingUtils.exceptionIfNullOrInvalid(vy);
		MappingUtils.exceptionIfNullOrInvalid(vector);
		this.v1 = vz;
		this.v2 = vy;
		this.vector = vector;

	}




	@Override
	public double[] apply(T t) {

		final Object va = v1.apply(t);		
		if(va == null)
			return null;

		final Object vb = v2.apply(t);
		if(vb == null)
			return null;

		final Object vv = vector.apply(t);
		if(vv == null)
			return null;

		int vaLength = Array.getLength(va);
		if(vaLength<1)
			return null;

		if(vaLength != Array.getLength(vb))
			return null;


		try {

			// Build the Rotation operator	
			if(vaLength == 2) {// 2D case
				final Vector3D a = new Vector3D(new double[] {((Number) Array.get(va, 0)).doubleValue(),((Number) Array.get(va, 1)).doubleValue(),0});
				final Vector3D b = new Vector3D(new double[] {((Number) Array.get(vb, 0)).doubleValue(),((Number) Array.get(vb, 1)).doubleValue(),0});
				final Vector3D v = new Vector3D(new double[] {((Number) Array.get(vv, 0)).doubleValue(),((Number) Array.get(vv, 1)).doubleValue(),0});

				return Arrays.copyOf(createAndApplyRotation(a, b, v).toArray(),2);

			}
			else {// 3D case
				final Vector3D a = new Vector3D(new double[] {((Number) Array.get(va, 0)).doubleValue(),((Number) Array.get(va, 1)).doubleValue(),((Number) Array.get(va, 2)).doubleValue()});
				final Vector3D b = new Vector3D(new double[] {((Number) Array.get(vb, 0)).doubleValue(),((Number) Array.get(vb, 1)).doubleValue(),((Number) Array.get(vb, 2)).doubleValue()});
				final Vector3D v = new Vector3D(new double[] {((Number) Array.get(vv, 0)).doubleValue(),((Number) Array.get(vv, 1)).doubleValue(),((Number) Array.get(vv, 2)).doubleValue()});

				return createAndApplyRotation(a, b, v).toArray();


			}


		}catch(MathArithmeticException e) {
			//TODO log.warn();
			return null;
		}


	}



	private Vector3D createAndApplyRotation(Vector3D a, Vector3D b, Vector3D v) throws MathArithmeticException{
		final Rotation rot = new Rotation(Vector3D.PLUS_J, Vector3D.PLUS_K, a, b);
		return rot.applyTo(v);
	}



	@Override
	public String description() {
		return "DIRECTIONAL: Base change of "+vector.description()+" according to "+v1.description()+" and "+v2.description();
	}

	@Override
	public Class<double[]> getReturnType() {
		return double[].class;
	}





}

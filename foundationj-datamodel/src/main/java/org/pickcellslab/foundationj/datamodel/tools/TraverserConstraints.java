package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;

/**
 * Defines the 'rules' to navigate the data graph
 *
 */
public class TraverserConstraints {


	public enum LinksLogic{
		INCLUSION(true),
		EXCLUSION(false);

		private boolean b;

		LinksLogic(boolean b){
			this.b = b;
		}

		public boolean value() {
			return b;
		}
	}


	private final Predicate<Number> depthTest;
	private final Map<String, Direction> included;
	private final BiFunction<Link, Direction, Boolean> linkFilter;
	private final Map<ExplicitPredicate<? super NodeItem>, Decision> filters;
	private final Decision defaultDecision;
	private final boolean noLinkConstraints;
	private final int maxDepth;
	private final LinksLogic logic;




	private TraverserConstraints(ConstraintsBuilder builder){

		this.logic = builder.logic;
		this.depthTest = builder.depthTest;	
		if(depthTest==null)
			throw new NullPointerException("Depth Test is null");
		this.included = builder.included;
		this.filters = builder.filters;
		this.noLinkConstraints = builder.noLinkConstraints;
		this.maxDepth = builder.maxDepth;;
		this.defaultDecision = builder.defaultDecision;
		this.linkFilter = builder.linkFilter;

	}




	public Decision decide(NodeItem item){    
		for(Entry<ExplicitPredicate<? super NodeItem>, Decision> e : filters.entrySet()){
			if(e.getKey().test(item))
				return e.getValue();
		}
		return defaultDecision;
	}


	public boolean accept(int depth){
		if(depthTest==null)
			throw new NullPointerException("Depth Test is null");
		return depthTest.test(depth);
	}

	public boolean accept(Link link, Direction d){

		//1- Test if constraints on link have been added
		if(noLinkConstraints){
			return true;
		}
		else if(linkFilter != null){
			return linkFilter.apply(link, d);
		}
		//2- 
		Direction dir = included.get(link.declaredType());    
		if(dir!=null && (dir == d || dir == Direction.BOTH))
			return logic.value();
		else return !logic.value();

	}


	public int maxDepth(){
		return maxDepth;
	}






	public static class ConstraintsBuilder implements
	FromDepth<ToDepth<LinkDecisions<TraverserConstraints>>>,
	ToDepth<LinkDecisions<TraverserConstraints>>,
	LinkDecisions<TraverserConstraints>,
	IncludeLinksBased<TraverserConstraints>,
	ExcludeLinksBased<TraverserConstraints>,
	EvaluationContainerBuilder<TraverserConstraints>
	{



		private Predicate<Number> depthTest; 
		private Map<String, Direction> included = new HashMap<>();
		private Map<ExplicitPredicate<? super NodeItem>, Decision> filters = new HashMap<>();
		private Decision defaultDecision;
		private boolean noLinkConstraints = false;
		private int minDepth = 0;
		private int maxDepth = Integer.MAX_VALUE;
		private LinksLogic logic;
		private BiFunction<Link, Direction, Boolean> linkFilter;


		ConstraintsBuilder(){/*package protected*/}






		@Override
		public ToDepth<LinkDecisions<TraverserConstraints>> fromMinDepth() {
			return fromDepth(0);
		}



		@Override
		public ToDepth<LinkDecisions<TraverserConstraints>> fromDepth(int min) {
			minDepth = min;
			return this;
		}

		@Override
		public LinkDecisions<TraverserConstraints> toMaxDepth() {
			return toDepth(Integer.MAX_VALUE);
		}

		@Override
		public LinkDecisions<TraverserConstraints> toDepth(int max) {
			if(maxDepth<minDepth)
				throw new IllegalArgumentException("The maximum depth is larger than the minimum depth");
			maxDepth = max;
			depthTest = (d)-> d.intValue()>=minDepth && d.intValue()<=maxDepth;
			return this;
		}

		public IncludeLinksBased<TraverserConstraints> traverseLink(String type, Direction d){  
			Objects.requireNonNull(d,"The given Direction cannot be null");
			logic = LinksLogic.INCLUSION;
			included.put(type, d);      
			return this;
		}



		public ExcludeLinksBased<TraverserConstraints> doNotTraverseLink(String type, Direction d){  
			Objects.requireNonNull(d,"The given Direction cannot be null");
			logic = LinksLogic.EXCLUSION;
			included.put(type,d);     
			return this;
		}


		@Override
		public EvaluationContainerInit<TraverserConstraints> traverseAllLinks() {
			logic = LinksLogic.EXCLUSION;
			noLinkConstraints = true;
			return this;
		}



		@Override
		public ExcludeLinksBased<TraverserConstraints> asWellAs(String type, Direction d) {
			Objects.requireNonNull(d,"The given Direction cannot be null");
			included.put(type, d);      
			return this;
		}


		@Override
		public IncludeLinksBased<TraverserConstraints> and(String type, Direction d) {
			Objects.requireNonNull(d,"The given Direction cannot be null");
			included.put(type, d);      
			return this;
		}



		@Override
		public EvaluationContainerInit<TraverserConstraints> acceptLinks(BiFunction<Link, Direction, Boolean> biPredicate) {
			Objects.requireNonNull(biPredicate,"The given bifunction cannot be null");
			linkFilter = biPredicate;
			return this;
		}



		@Override
		public EvaluationContainerBuilder<TraverserConstraints> withEvaluations(
				ExplicitPredicate<? super NodeItem> test, Decision ifYes, Decision defaultEval) {
			Objects.requireNonNull(test,"The given Predicate cannot be null");
			Objects.requireNonNull(ifYes,"The given Decision if yes cannot be null");
			Objects.requireNonNull(defaultEval,"The default Decision cannot be null");
			filters.put(test,ifYes);   
			defaultDecision = defaultEval;
			return this;
		}

		@Override
		public TraverserConstraints includeAllNodes(){
			filters.put(P.none(),Decision.INCLUDE_AND_CONTINUE); 
			return new TraverserConstraints(this);
		}



		@Override
		public TraverserConstraints lastEvaluation(ExplicitPredicate<? super NodeItem> test, Decision d) {
			withEvaluations(test,d, defaultDecision);
			return new TraverserConstraints(this);
		}






		@Override
		public TraverserConstraints withOneEvaluation(ExplicitPredicate<? super NodeItem> test,
				Decision ifYes, Decision ifNot) {
			withEvaluations(test, ifYes, ifNot);
			return new TraverserConstraints(this);
		}


		@Override
		public TraverserConstraints done() {
			return new TraverserConstraints(this);
		}




		@Override
		public EvaluationContainerBuilder<TraverserConstraints> addEvaluation(
				ExplicitPredicate<? super NodeItem> test, Decision d) {
			withEvaluations(test,d, defaultDecision);
			return this;
		}






	}


}

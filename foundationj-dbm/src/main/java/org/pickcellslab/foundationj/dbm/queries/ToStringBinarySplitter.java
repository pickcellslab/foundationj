package org.pickcellslab.foundationj.dbm.queries;

import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "SinglePredicateGrouping")
@WithService(DataAccess.class)
public class ToStringBinarySplitter<E> implements ExplicitSplit<E, String>{

	@Supported
	private final ExplicitPredicate<E> predicate;
	private final String accept, reject;
	
	
	public ToStringBinarySplitter(ExplicitPredicate<E> predicate, String accept, String reject) {
		
		MappingUtils.exceptionIfNullOrInvalid(predicate);
		Objects.requireNonNull(accept);
		Objects.requireNonNull(reject);
		assert !accept.equals(reject) : "accept cannot be equals to reject";
		
		this.predicate = predicate;
		this.accept = accept;
		this.reject = reject;
		
	}
	

	@Override
	public String description() {
		return "Split in 2 categories based on "+predicate.description();
	}

	@Override
	public String getKeyFor(E i) {
		return predicate.test(i) ? accept : reject;
	}

}

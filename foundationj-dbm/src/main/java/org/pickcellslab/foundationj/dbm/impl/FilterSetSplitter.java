package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="GroupingByPredicateList")
class FilterSetSplitter<T> implements ExplicitSplit<T, ExplicitPredicate<? super T>> {

	@Supported
	private List<ExplicitPredicate<? super T>> filters;

	public FilterSetSplitter(List<ExplicitPredicate<? super T>> filterSet){
		Objects.requireNonNull(filterSet, "the set of filters is null");
		if(filterSet.isEmpty())
			throw new IllegalArgumentException("The set of filters is empty");
		filterSet.forEach(p->MappingUtils.exceptionIfNullOrInvalid(p));
		this.filters = filterSet;
	}

	@Override
	public String description() {
		return "group based on a list of predicates";
	}

	@Override
	public <E> Collection<E> accumulator(){
		return new HashSet<E>();
	};

	@Override
	public ExplicitPredicate<? super T> getKeyFor(T i) {
		for(ExplicitPredicate<? super T> test : filters)
			if(test.test(i))
				return test;
		return null;
	}

}

package org.pickcellslab.foundationj.dbm.queries;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.ExplicitOperation;

/**
 * Assign a Key to a given object. Designed to contain the logic to group objects into different categories
 * 
 * Splitters should also be {@link Mapping} compatible
 * 
 * @author Guillaume Blin
 *
 * @param <I> The input of the Splitter
 * @param <O> The type of keys to identify groups
 */
public interface ExplicitSplit<I,O> extends ExplicitOperation{

	public static final String OTHER = "Other"; 
		
	public default <E> Collection<E> accumulator(){
		return new ArrayList<E>();
	};
	
	public O getKeyFor(I i);

}

package org.pickcellslab.foundationj.bootstrap;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapper;
import org.pickcellslab.foundationj.scope.RuntimeScope;
import org.pickcellslab.foundationj.scope.ScopeEventListener;
import org.pickcellslab.foundationj.scope.ScopeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScopeManagerImpl implements ScopeManager, ScopeEventListener {

	private static final Logger log = LoggerFactory.getLogger(ScopeManagerImpl.class);

	private final AppScopes scopes = new AppScopes();
	private AtScope applicationScope;

	private final List<ScopeEventListener> lstrs = new ArrayList<>();	



	@Override
	public RuntimeScope getApplicationScope() {
		return scopes.getAtScope(Scope.APPLICATION).get().activeWithId(null).get();
	}



	//=======================================================================================//
	//  Package Protected
	//=======================================================================================//

	void setComponents(Map<Scope, List<Class<?>>> scopedComps) {
		// This is where we build the tree of AtScopes

		// Now sort to make sure we create parents before children
		final List<Scope> found = new ArrayList<>(scopedComps.keySet());
		prioritise(found);

		log.debug("----------------------------------------------- ");
		log.debug("Number of Scopes found: "+found.size());
		log.debug("----------------------------------------------- ");
		found.forEach(s->log.debug("Scope : "+s.name()+"\t"+s.parent()));
		log.debug("----------------------------------------------- ");

		// Finally setup AppScopes
		for(Scope s: found) {
			final AbstractAtScope as = scopes.getOrCreate(s.name(), s.parent(), s.policy());
			as.addComponents((List)scopedComps.get(s));
		}

	}


	private void prioritise(List<Scope> list) {
		final LinkedList<Scope> queue = new LinkedList<>(list);
		final List<Scope> prioritised = new ArrayList<>(list.size());
		final Set<String> known = list.stream().map(s->s.name()).collect(Collectors.toSet());
		final Set<String> parents = new HashSet<>();
		while(!queue.isEmpty()) {
			final Scope s = queue.poll();
			if(s.parent().isEmpty() || parents.contains(s.parent())) {
				prioritised.add(s);
				parents.add(s.name());
			}
			else if(!known.contains(s.parent()))
				throw new RuntimeException("The scope configuration is inconsistent: "+s.parent()+" cannot be found");
			else
				queue.addLast(s);
		}
		list.clear();
		list.addAll(prioritised);
	}




	public void setServices(Map<Class<?>, Class<?>> services) {
		// Iterate through scopes from parent to children 		
		scopes.getAll().forEach(as->{
			as.takeServices(services);
		});		
	}



	void setupDataConfiguration(Map<Scope, List<Class<?>>> dataScoped, Map<Class<?>, List<Class<?>>> dataServices) {

		// 
		for(Entry<Scope, List<Class<?>>> e : dataScoped.entrySet()) {
			AbstractAtScope as = scopes.getAtScope(e.getKey().name()).get();
			as.addComponent(new DiscoveredData(e.getValue()));
		}

		// Find containers which require this particular service
		// and add the service as a component
		dataServices.forEach((itfc,list)->{
			scopes.getAll().forEach(as->{
				if(as.isServiceConsumer(itfc)) {
					as.addComponent(new DiscoveredData(list));
				}
			});
		});
	}









	void startApplication(Set<Class<?>> mappingSet, Set<Class<?>> deniedSet) {

		// Check and get the application level
		applicationScope = scopes.getAtScope(Scope.APPLICATION).orElse(null);
		if(applicationScope == null) // no need to check normally @Core system will throw exception
			BootStrapper.logErrorAndExit("No APPLICATION Level Scope could be found", null);


		// Phase 2 start the application scope
		final ScopeImpl appliScope = applicationScope.createAndRegister(null, null);

		// Initialise NonDataMapping
		final NonDataMapper mapper = appliScope.getComponentInstance(NonDataMapper.class);
		new NonDataMappingImpl(mapper, mappingSet, deniedSet);

		// Add this instance as this will be injected in AppView and AppController
		appliScope.addComponentInstance(this);

		// Start
		appliScope.start(this);
	}


	//=======================================================================================//
	//  ScopeManager Impl
	//=======================================================================================//


	@Override
	public void addScopeEventListener(ScopeEventListener l) {
		lstrs.add(l);
	}




	@Override
	public RuntimeScope newScopeInstance(String scope, String id, RuntimeScope parent) throws RuntimeException {

		Optional<AbstractAtScope> atScope = scopes.getAtScope(scope);

		if(!atScope.isPresent())
			throw new RuntimeException(scope + "does not exist");

		if(!atScope.get().parent().isEmpty() && parent==null)
			throw new RuntimeException(scope + "requires a parent scope ("+atScope.get().parent()+"),"
					+ " however, the provided parent RuntimeScope is null.");

		return atScope.get().createAndRegister(id, parent==null ? null : parent.id());
	}




	@Override
	public synchronized void startScope(RuntimeScope scope, Object... options) {
		log.debug("ScopeManager starting scope "+scope.name()+" with id "+scope.id()+" and "+options.length+" option Instance(s)");
		((ScopeImpl)scope).start(this, options);
	}



	@Override
	public synchronized void stopScope(RuntimeScope scope) {
		((ScopeImpl)scope).stopAndRemove(this);		
	}




	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Stream<RuntimeScope> activeScopeInstances(String scope) {
		return (Stream)getOrThrow(scope).active();
	}




	@Override
	public void stopAllAndExit() {
		this.stopScope(this.getApplicationScope());
	}




	@Override
	public RuntimeScope getScope(String scope, String id) {	
		return getOrThrow(scope).activeWithId(id).orElse(null);
	}

	@Override
	public RuntimeScope getScope(Object instance) {

		Optional<AbstractAtScope> opt = scopes.getAll()
				.filter(as->as.containsComponent(instance)).findAny();

		if(opt.isPresent()) {
			Optional<ScopeImpl> siOpt = opt.get().active().filter(si->si.created(instance)).findAny();
			if(siOpt.isPresent())
				return siOpt.get();
		}

		return null;
	}


	private AbstractAtScope getOrThrow(String scope) {
		Optional<AbstractAtScope> atScope = scopes.getAtScope(scope);
		if(!atScope.isPresent())
			throw new RuntimeException(scope + " does not exist");
		else
			return atScope.get();
	}




	@Override
	public String getRootPath() {
		return getRootDir().getPath();
	}

	private File getRootDir() {

		try {

			// Define the location of the root application
			URL u = BootStrapper.class.getProtectionDomain().getCodeSource().getLocation();	
			return new File(u.toURI()).getParentFile().getParentFile();

		} catch (URISyntaxException e1) {			
			BootStrapper.logErrorAndExit("Unable to identify the location of the root of the application... Shuting Down...", e1);;
			return null;
		}
	}



	//=======================================================================================//
	//  ScopeEventConsumer Impl
	//=======================================================================================//




	@Override
	public void beforeStop(RuntimeScope willStop) {
		lstrs.forEach(l->l.beforeStop(willStop));		
	}




	@Override
	public void afterStop(RuntimeScope hasStopped) {
		ScopeImpl scope = scopes.getAtScope(hasStopped.name()).get().remove(hasStopped.id());
		scope.getAllInstancesFor(ScopeEventListener.class).forEach(l->lstrs.remove(l));
		lstrs.forEach(l->l.afterStop(hasStopped));
	}




	@Override
	public void beforeStart(RuntimeScope scopeImpl) {
		lstrs.forEach(l->l.beforeStart(scopeImpl));		
	}




	@Override
	public void afterStart(RuntimeScope scopeImpl) {
		lstrs.forEach(l->l.afterStart(scopeImpl));
	}


}

/**
 * 
 */
package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.pickcellslab.foundationj.mapping.extra.MappingUtils;


/**
 * This annotation can be used on a field whose type is unknown at compile time. It specifies that
 * the developer is certain that the field type will be a supported type and that compilation should be permitted.
 * <br>
 * <br>
 * It is highly recommended to use {@link MappingUtils#exceptionIfNullOrInvalid(Object)} where the field is assigned.
 * 
 * @see Mapping
 * @see MappingUtils
 * 
 * @author Guillaume Blin
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Supported {

}

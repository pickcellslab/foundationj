package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

class StatsAction<T extends DataItem> implements Action<T, Map<AKey<?>,SummaryStatistics>> {


	private final Map<AKey<?>, StatsWrap<? super T, ?>> template;
	private final Map<Object, StatsMechanism> coords = new HashMap<>();

	StatsAction(Map<AKey<?>, StatsWrap<? super T, ?>> todoList){
		this.template = todoList;
	}


	@Override
	public synchronized ActionMechanism<T,Map<AKey<?>, SummaryStatistics>> createCoordinate(Object key) {
		coords.put(key, new StatsMechanism(template));
		return coords.get(key);
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public Map<AKey<?>, SummaryStatistics> createOutput(Object key) throws IllegalArgumentException {
		return coords.get(key).get();
	}

	@Override
	public String description() {
		String description = "Compute Stats for :";
		for(StatsWrap<? super T, ?> sw : template.values())
			description += "|"+sw.description()+"|";
		return description;
	}


	private class StatsMechanism implements ActionMechanism<T,Map<AKey<?>, SummaryStatistics>>{

		private final Map<AKey<?>, StatsWrap<? super T, ?>> map;

		StatsMechanism(Map<AKey<?>, StatsWrap<? super T, ?>> template){
			//Duplicate the map
			Map<AKey<?>,StatsWrap<? super T,?>> map = new HashMap<>(template.size());
			for(Entry<AKey<?>, StatsWrap<? super T, ?>> e : template.entrySet())
				map.put(e.getKey(), e.getValue().duplicate());
			this.map = map;
		}

		@Override
		public void performAction(T i) {
			map.values().forEach(s->s.performAction(i));
		}

	
		public Map<AKey<?>, SummaryStatistics> get(){
			Map<AKey<?>,SummaryStatistics> result = new HashMap<>(map.size());
			for(Entry<AKey<?>, StatsWrap<? super T, ?>> e : map.entrySet())
				result.put(e.getKey(), e.getValue().getStats());
			return result;
		}

	}




}

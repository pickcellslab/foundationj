package org.pickcellslab.foundationj.scope;

import java.util.Objects;

/**
 * 
 * An {@link AppController} is a {@link ScopeEventListener} provided with a reference to a {@link ScopeManager}.
 * It controls the creation of new {@link RuntimeScope} instances and their life cycles.
 * 
 * @author Guillaume Blin
 *
 */
public abstract class AppController implements ScopeEventListener{

	
	/**
	 * The {@link ScopeManager} providing access to the application's {@link RuntimeScope}s
	 */
	protected final ScopeManager scopeMgr;	
	
	/**
	 * References the {@link ScopeManager} and registers this {@link AppController} as a {@link ScopeEventListener}.
	 * @param model The {@link ScopeManager} listened by this {@link AppController}
	 */
	public AppController(ScopeManager model) {
		Objects.requireNonNull(model);
		this.scopeMgr = model;
		model.addScopeEventListener(this); 
	}
	
	
}

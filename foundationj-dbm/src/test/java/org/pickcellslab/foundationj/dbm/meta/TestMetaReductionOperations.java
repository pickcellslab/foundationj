package org.pickcellslab.foundationj.dbm.meta;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DefaultNodeItem;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.reductions.GetOne;

public class TestMetaReductionOperations {

	private final static AKey<String> strProperty = AKey.get("String", String.class);
	private final static AKey<Double> dblProperty = AKey.get("double", double.class);
	private final static AKey<Double> boxedProperty = AKey.get("Double", Double.class);
	private final static AKey<double[]> dblArrProperty = AKey.get("double[]", double[].class);
	
	private final static List<NodeItem> toInclude = new ArrayList<>();
	
	@BeforeAll
	public static void populateData() {
		for(int i = 0; i<10; i++) {
			final NodeItem n1 = new DefaultNodeItem("Node "+ i);
			n1.setAttribute(strProperty, "String "+i);
			n1.setAttribute(dblProperty, (double)i);
			n1.setAttribute(dblArrProperty, new double[] {i,i,i});
			n1.setAttribute(boxedProperty, new Double(i));
			toInclude.add(n1);
		}
	}
	
	
	@Test
	public void testMetaDelegatingReductionStr(){		
		GetOne<DataItem, String> getOne = new GetOne<>(F.select(strProperty, "None Found"));
		MetaDelegatingReduction mdr = new MetaDelegatingReduction(getOne);
		for(NodeItem node : toInclude)
			mdr.include(node);
		final String[] result = (String[]) mdr.getResult();
		Assertions.assertTrue(result.length==1);
		Assertions.assertTrue(result[0].equals(toInclude.get(0).getAttribute(strProperty).get()));
	}
	
	
	@Test
	public void testMetaDelegatingReductionDbl(){		
		GetOne<DataItem, Double> getOne = new GetOne<>(F.select(dblProperty, Double.NaN));
		MetaDelegatingReduction mdr = new MetaDelegatingReduction(getOne);
		for(NodeItem node : toInclude)
			mdr.include(node);
		final double[] result = (double[]) mdr.getResult();
		Assertions.assertTrue(result.length==1);
		Assertions.assertTrue(result[0] == (toInclude.get(0).getAttribute(dblProperty).get()));
	}
	
	@Test
	public void testMetaDelegatingReductionDouble(){		
		GetOne<DataItem, Double> getOne = new GetOne<>(F.select(boxedProperty, Double.NaN));
		MetaDelegatingReduction mdr = new MetaDelegatingReduction(getOne);
		for(NodeItem node : toInclude)
			mdr.include(node);
		final double[] result = (double[]) mdr.getResult();
		Assertions.assertTrue(result.length==1);
		Assertions.assertTrue(result[0] == (toInclude.get(0).getAttribute(dblProperty).get()));
	}
	
	
	@Test
	public void testMetaDelegatingReductionDblArr(){		
		GetOne<DataItem, double[]> getOne = new GetOne<>(F.select(dblArrProperty, new double[] {Double.NaN}));
		MetaDelegatingArrayReduction<double[]> mdr = new MetaDelegatingArrayReduction<>(getOne);
		for(NodeItem node : toInclude)
			mdr.include(node);		
		double[] result = mdr.getResult();
		Assertions.assertTrue(result.length==3);
		Assertions.assertTrue(result[0] == (toInclude.get(0).getAttribute(dblArrProperty).get()[0]));
	}
	
/*
	/**
	 * Tests GetOneOperation class.
	 * What need to be tested are type conversions and casting at runtime in the 
	 * {@link MetaGetOneOperation#include(DataItem)} method.
	 *
	@Test
	public void testGetOneOperation() {

		// Create an AKey for an attribute in a DataItem
		final AKey<Double> doubleKey = AKey.get("Double Key", double.class);

		// Create a Dimension which will ask the GetOneOperation to fetch
		final Dimension<DataItem, Double> doubleDim = new KeyDimension<>(doubleKey, null);//Dimension.create(doubleKey, 2.5d);

		// Create the mock DataItem
		final DataItem mockItem = mock(DataItem.class);


		// Create A DataRegistry
		DataRegistry registry = Mockito.mock(DataRegistry.class);
		
		MetaInstanceDirector director = new MetaInstanceDirector(registry); 

		final MetaReductionOperation<Double> reduc = MetaReductionOperation.getOrCreate(director, required, op, name, lengthIfArray)
				new MetaGetOneOperation<>(doubleDim, double.class);


		// return a double
		when(mockItem.getAttribute(doubleKey)).then((o)-> Optional.of(2.5d));

		reduc.include(mockItem);

		final Double[] result = reduc.getResult();
		Assertions.assertTrue(result[0] == 2.5f);

	}




	/**
	 * Tests {@link MetaGetOneFactory} class.
	 * @throws InstantiationHookException 
	 * 
	 *
	@Test
	public void testMetaGetOneFactory() throws InstantiationHookException {

		//-------------------------------------------------------------------------------------//
		// Here we will test whether GetOneOperation is instantiated with the correct arguments
		// when calling MetaGetOneFactory#createReductionOperation()
		//-------------------------------------------------------------------------------------//

		// We will need a MetaReadable
		MetaReadable mockedMR = mock(MetaReadable.class);
		// with a name
		when(mockedMR.name()).thenReturn("Mocked MetaReadable");

		// And a MetaQueryable (owner of MetaReadable)
		MetaQueryable mockedOwner = mock(MetaQueryable.class);
		// with a name
		when(mockedOwner.name()).thenReturn("Mocked Owner");

		// So when owner() is called we return mockedOwner
		when(mockedMR.owner()).thenReturn(mockedOwner);



		// Now perform the test specific mock configuration


		//-------------------------------------------------------//
		// A- Test with no array - double class
		//-------------------------------------------------------//

		// Raw dimension:
		final AKey<Double> doubleKey = AKey.get("double Key", double.class);
		when(mockedMR.getRaw()).thenReturn(new KeyDimension<>(doubleKey, ""));
		// Data Class
		when(mockedMR.dataClass()).thenReturn(double.class);

		// Create A DataRegistry
		DataRegistry registry = Mockito.mock(DataRegistry.class);
		
		MetaInstanceDirector director = new MetaInstanceDirector(registry); 
		//director.
		
		final MetaGetOneFactory<Double> mgFctry = MetaGetOneFactory.getOrCreate(director, mockedMR);


		// Mocked DataItem
		final DataItem mockItem = mock(DataItem.class);
		// which returns a double
		when(mockItem.getAttribute(doubleKey)).then((o)-> Optional.of(2.5d));


		// Now ask our factory to create our operation
		final MetaReductionOperation<Double> reduc = mgFctry.createReductionOperation();


		// Perform the test
		reduc.include(mockItem);
		final Double[] result = reduc.getResult();
		Assertions.assertTrue(result[0] == 2.5);



		//-------------------------------------------------------//
		// B- Test with array - double[] class
		//-------------------------------------------------------//

		// Raw dimension:
		final AKey<double[]> doubleArrKey = AKey.get("double Array Key", double[].class);	

		when(mockedMR.getRaw()).thenReturn(new KeyArrayDimension<>(doubleArrKey, 2, ""));
		
		when(mockedMR.isArray()).thenReturn(true);
		
		// Data Class
		when(mockedMR.dataClass()).thenReturn(double[].class);


		// Mocked DataItem now returns a double[]
		when(mockItem.getAttribute(doubleArrKey)).then((o)-> Optional.of(new double[] {1.5, 52}));


		final MetaGetOneArrayFactory<Double> mgFctryArr = MetaGetOneArrayFactory.getOrCreate(director, mockedMR);
		
		// Now ask our factory to create our operation
		final MetaReductionOperation<Double> reducWithArray = mgFctryArr.createReductionOperation();


		// Perform the test
		reducWithArray.include(mockItem);
		final Double[] resultArray = reducWithArray.getResult();
		Assertions.assertTrue(resultArray[0] == 1.5);

	}

*/


}

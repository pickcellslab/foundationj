package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Highlightable;
import org.pickcellslab.foundationj.ui.wizard.UserDefinableDocument;
import org.pickcellslab.foundationj.ui.wizard.UserDefinedList;
import org.pickcellslab.foundationj.ui.wizard.UserDefinedListListener;
import org.slf4j.LoggerFactory;

import com.alee.extended.tab.WebDocumentPane;

public class BinaryTreeSplitPanel<T extends Supplier<MetaFilter> & Highlightable> extends SplitterUI<BinaryTreeSplitPanel<T>> {


	private UserDefinedList<UserDefinableDocument<SplitIn2Panel<T>>> filterList;

	public BinaryTreeSplitPanel(UITheme theme, T supplier) {


		WebDocumentPane<UserDefinableDocument<SplitIn2Panel<T>>> tabsPane = new WebDocumentPane<>();
		add(tabsPane, BorderLayout.CENTER);
		tabsPane.setCloseable(false);
		tabsPane.setSplitEnabled(false);


		LongAdder ids = new LongAdder();
		filterList = 
				new UserDefinedList<>(theme, ()-> {
					ids.increment();
					UserDefinableDocument<SplitIn2Panel<T>> udd = new UserDefinableDocument<>(""+ids.intValue(), theme.icon(IconID.Misc.DATA_TABLE, 16), new SplitIn2Panel<>(theme, supplier));
					return udd;
				},"Filter",true);

		filterList.addSelectionListener(new UserDefinedListListener<UserDefinableDocument<SplitIn2Panel<T>>>(){
			@Override
			public void removed(UserDefinableDocument<SplitIn2Panel<T>> t) {
				tabsPane.closeDocument(t);
			}
			@Override
			public void added(UserDefinableDocument<SplitIn2Panel<T>> t) {
				System.out.println("Added recieved");
				tabsPane.openDocument(t);
			}
			@Override
			public void selected(UserDefinableDocument<SplitIn2Panel<T>> t) {
				tabsPane.setSelected(t);
			}
			@Override
			public void renamed(UserDefinableDocument<SplitIn2Panel<T>> t) {
				System.out.println("Renamed!");
				tabsPane.closeDocument(t);
				tabsPane.openDocument(t);
				tabsPane.setSelected(t);
			}

		});

		//Propagate selection changes to the UserDefinedList
		tabsPane.getActivePane().getTabbedPane().addChangeListener(l->{
			filterList.seSelected(tabsPane.getSelectedDocument());
		});


		setLayout(new BorderLayout());
		add(tabsPane, BorderLayout.CENTER);
		add(filterList, BorderLayout.WEST);


		filterList.addItem("Filter 1");
		filterList.addItem("Filter 2");

		filterList.addValidityListener(l->fireNextValidityChanged());

	}


	@Override
	public boolean validity() {
		return filterList.validity() && filterList.numberOfElements()>1;
	}

	@Override
	MetaSplit getSplitter(Meta meta) {

		List<UserDefinableDocument<SplitIn2Panel<T>>> list = new ArrayList<>();
		filterList.getAll(list);
		List<SplitIn2Panel<?>> splits = list.stream().map(udd->udd.getData()).collect(Collectors.toList());


		try {
			BinaryTreeMetaSplit splitter = BinaryTreeMetaSplit.getOrCreate(meta.getInstanceManager(), splits);
			return splitter;


		} catch (InstantiationHookException e) {
			LoggerFactory.getLogger(getClass()).error("Unable to create a MetaModel Object", e);
			JOptionPane.showMessageDialog(this, "Unable to create a MetaModel Object, error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}

	}


	@Override
	public void copyState(BinaryTreeSplitPanel<T> other) {
		// TODO Auto-generated method stub

	}

}

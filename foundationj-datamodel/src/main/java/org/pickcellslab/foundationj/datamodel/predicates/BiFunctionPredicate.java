package org.pickcellslab.foundationj.datamodel.predicates;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.Op.ExplicitOperator;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

/**
 * A {@link ExplicitPredicate} implementation which takes an {@link ExplicitOperator} and two {@link ExplicitFunction} as operands.
 * 
 * @author Guillaume
 *
 * @param <L> the type of the left operand (returned by the left DataFunction)
 * @param <R> the type of the right operand (returned by the right DataFunction)
 */
@Mapping(id = "Proposition")
@Scope
public class BiFunctionPredicate<T,L,R> implements ExplicitPredicate<T>{


	@Supported
	ExplicitFunction<T, ? extends L> leftFunction;
	@Supported
	ExplicitFunction<T, ? extends R> rightFunction;
	@Supported
	ExplicitOperator<? super L,? super R> operator;

	/**
	 * Creates a new BiFunctionPredicate
	 * @param leftF A DataFunction which returned value by {@link java.util.function.Function#apply(Object)} will be used as the left operand during the test
	 * @param rightF A DataFunction which returned value by {@link java.util.function.Function#apply(Object)} will be used as the right operand during the test
	 * @param operator An {@link ExplicitOperator}
	 * 
	 * @author Guillaume Blin
	 * 
	 */
	public BiFunctionPredicate(ExplicitFunction<T,? extends L> leftF, ExplicitFunction<T,? extends R> rightF, ExplicitOperator<? super L,? super R> operator){
		MappingUtils.exceptionIfNullOrInvalid(leftF);
		MappingUtils.exceptionIfNullOrInvalid(operator);
		MappingUtils.exceptionIfNullOrInvalid(rightF);
		this.leftFunction = leftF;
		this.rightFunction = rightF;
		this.operator = operator;
	}

	@Override
	public boolean test(T t) {
		try{
			return operator.apply(leftFunction.apply(t), rightFunction.apply(t));
		}catch(NullPointerException e){
			return false;
		}	
	}

	@Override
	public String description() {
		return leftFunction.description()+" "+operator.toString()+" "+rightFunction.description();
	}

	@Override
	public String toString() {
		return leftFunction.toString()+" "+operator.toString()+" "+rightFunction.toString();
	}

}

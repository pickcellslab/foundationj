package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.Direction;

/** 
 * A step in a stepwise builder pattern to define the 'rules' for navigating a graph. This step defines which links are allowed 
 * in the traversal.
 *
 * @param <T> The type of the next builder
 */
public interface IncludeLinksBased<T> extends EvaluationContainerInit<T>{

	/**
	 * Adds the given type / {@link Direction} combination in the list of links that are allowed to be traversed
	 * @param type The type of links to be traversed
	 * @param d The {@link Direction} allowed to be traversed
	 * @return The next step in the building process
	 */
	public IncludeLinksBased<T> and(String type, Direction d);
	
	

	
}

package org.pickcellslab.foundationj.dbm.meta;

import java.lang.reflect.Array;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id = "MetaReduction")
@WithService(DataAccess.class)
class MetaDelegatingReduction implements ExplicitReduction<DataItem,Object> {

	@Supported
	private ExplicitReduction<DataItem, ?> op;

	public MetaDelegatingReduction(ExplicitReduction<DataItem, ?> op) {
		MappingUtils.exceptionIfNullOrInvalid(op);
		// return type must not be an array
		if(op.getReturnType().isArray())
			throw new IllegalArgumentException("The return type of the provided ReductionOperation must not be an array");
		this.op = op;
	}


	@Override
	public void root(DataItem root, boolean included) {
		op.root(root, included);
	}


	@Override
	public void include(DataItem t) {
		op.include(t);		
	}


	@Override
	public Object getResult() {
		final Object result = op.getResult();
		if(null == result)
			return null;
		final Object arr = Array.newInstance(op.getReturnType(), 1);
		Array.set(arr, 0, result);
		return arr;
	}

	@Override
	public ExplicitReduction<DataItem, Object> factor() {
		return new MetaDelegatingReduction(op.factor());
	}

	@Override
	public Class getReturnType() {
		return Array.newInstance(op.getReturnType(), 1).getClass();
	}

	@Override
	public String description() {
		return op.description();
	}

}

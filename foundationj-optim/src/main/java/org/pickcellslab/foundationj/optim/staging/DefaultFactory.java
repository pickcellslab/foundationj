package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Collections;

import org.pickcellslab.foundationj.datamodel.graph.DefaultNode;
import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;


public class DefaultFactory <E extends Edge<? extends Node<E>>> 
extends DefaultNode<E>
implements Factory<E> {

	private final String name;
	private Collection<Parcel> can;
	private Collection<Parcel> needs;


	public DefaultFactory(String name,Collection<Parcel> requirements, Collection<Parcel> capabilities ){
		this.needs = requirements;
		this.can = capabilities;
		this.name = name;
	}

	@Override
	public Collection<Parcel> requirements() {
		return Collections.unmodifiableCollection(needs);
	}




	@Override
	public double evaluate(Collection<Parcel> contract) {

		if(needs.isEmpty())
			return 0;
		//TODO
		return 0;
	}





	@Override
	public Collection<Parcel> capabilities() {
		return Collections.unmodifiableCollection(can);
	}


	@Override
	public String getName() {
		return name;
	}


}

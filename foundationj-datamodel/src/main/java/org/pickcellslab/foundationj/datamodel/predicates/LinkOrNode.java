package org.pickcellslab.foundationj.datamodel.predicates;

import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;

public enum LinkOrNode implements Predicate<Object>{

	NODE{
		public boolean test(Object item) {
			return item !=null && NodeItem.class.isAssignableFrom(item.getClass());
		}
	},
	LINK{
		public boolean test(Object item) {
			return item !=null && Link.class.isAssignableFrom(item.getClass());
		}
	}
	
}

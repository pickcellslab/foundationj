package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.geom.Point2D;

/**
 * Models a city
 */
public class City extends Point2D {
	
	double x;
	double y;

	// Constructs a randomly placed city
	public City(){
		this.x = Math.random()*500;
		this.y = Math.random()*500;
	}

	// Constructs a city at chosen x, y location
	public City(int x, int y){
		this.x = x;
		this.y = y;
	}

	// Gets city's x coordinate
	public double getX(){
		return this.x;
	}

	// Gets city's y coordinate
	public double getY(){
		return this.y;
	}
	

	@Override
	public String toString(){
		return getX()+", "+getY();
	}

	@Override
	public void setLocation(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	
}

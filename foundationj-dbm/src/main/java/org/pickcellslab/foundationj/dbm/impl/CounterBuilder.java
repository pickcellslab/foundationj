package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.datamodel.reductions.CountOperation;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.dbm.queries.builders.ActionBuilder;

class CounterBuilder<T> extends ActionBuilderImpl<T, Long> implements ActionBuilder<T, Long> {

	private final TargetType<?, ?, T> target;
	
	CounterBuilder(TargetType<?, ?, T> target) {
		this.target = target;
	}
	
	@Override
	protected TargetType<?, ?, T> getType() {
		return target;
	}

	@Override
	protected Action<? super T, Long> getAction() {
		return Actions.reduction(new CountOperation());
	}

}

package org.pickcellslab.foundationj.dbm.queries;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.graph.BinaryTreeNode;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.NamedExplicitPredicate;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="BinaryTreePredicate")
public class CombinedBinary<T> implements BinaryTreeNode<CombinedBinary<T>> {


	@Supported
	private final ExplicitPredicate<? super T> p;
	private final CombinedBinary<T> parent;
	private final boolean result;
	private CombinedBinary<T> left;
	private CombinedBinary<T> right;

	private CombinedBinary(ExplicitPredicate<? super T> p){
		parent = null;
		this.p = p;
		result = false;
	}

	private CombinedBinary(CombinedBinary<T> parent, ExplicitPredicate<? super T> p, boolean b){		
		this.p = p;
		this.parent = parent;
		result = b;
		if(b)
			parent.right = this;
		else
			parent.left = this;
	}

	public Predicate<? super T> predicate(){
		return p;
	}

	public boolean result(){
		return result;
	}

	public CombinedBinary<T> getNext(T toTest){
		if(isLeaf())
			return this;
		if(p.test(toTest))
			return right;
		else
			return left;
	}



	@Override
	public boolean equals(Object o){
		if(o instanceof CombinedBinary){
			if(isRoot())
				return ((CombinedBinary<?>)o).isRoot() && p.equals(((CombinedBinary<?>)o).p);
			if(isLeaf())
				return result == ((CombinedBinary<?>)o).result && parent.equals(((CombinedBinary<?>)o).parent);
			else
				return p.equals(((CombinedBinary<?>)o).p) && result == ((CombinedBinary<?>)o).result;
		}
		else return false;

	}
	
	

	@Override
	public int hashCode(){		
		int hc =  Objects.hashCode(p) + Boolean.hashCode(result);
		if(isRoot()) hc += "Root".hashCode();
		else if(isLeaf()) 
			hc+= "Leaf".hashCode();			
		CombinedBinary<T> par = parent;
		if(null!=parent)
			hc+=par.hashCode();		
		return hc;
	}


	@Override
	public String toString(){
		
		if(isRoot())
			return "Root : "+p;
		
		if(isLeaf()){
			CombinedBinary<T> par = getParent();			
			String s = makeString(par.p,result);			
			while(!par.isRoot()){
				s+=" AND "+makeString(par.getParent().p,par.result);		
				par = par.getParent();
			}			
			return s;
		}
			
		return getParent().p.toString()+" : "+result+" then "+p.toString();
	}

	
	private String makeString(ExplicitPredicate<?> p, boolean result) {
		if(p instanceof NamedExplicitPredicate) {
			return ((NamedExplicitPredicate<?>) p).getName(result);
		}
		else
			return p+" : "+result;
	}
	


	@Override
	public boolean isLeaf() {
		return p == null;
	}

	@Override
	public boolean isRoot() {
		return parent == null;
	}

	@Override
	public CombinedBinary<T> getLeft() {
		return left;
	}

	@Override
	public CombinedBinary<T> getRight() {
		return right;
	}

	@Override
	public CombinedBinary<T> getParent() {
		return parent;
	}




	public static <Q> CombinedBinary<Q> createTree(List<ExplicitPredicate<? super Q>> predicates){

		Objects.requireNonNull(predicates, "The collection of predicates is null!");

		if(predicates.size()<2)
			throw new IllegalArgumentException("Less that 2 entries in predicates!");
		

		predicates.forEach(p->MappingUtils.exceptionIfNullOrInvalid(p));

		final Deque<CombinedBinary<Q>> queue = new LinkedList<>();
		final List<CombinedBinary<Q>> next = new ArrayList<>();

		Iterator<ExplicitPredicate<? super Q>> it = predicates.iterator();

		CombinedBinary<Q> root = new CombinedBinary<>(it.next());
		queue.add(root);

		while(it.hasNext()){
			ExplicitPredicate<? super Q> current = it.next();
			while(!queue.isEmpty()){
				CombinedBinary<Q> node = queue.poll();
				next.add(new CombinedBinary<>(node,current,false));
				next.add(new CombinedBinary<>(node,current,true));
			}
			queue.addAll(next);
			next.clear();
		}

		//Finally add the leaves where the Predicate is null
		while(!queue.isEmpty()){
			CombinedBinary<Q> node = queue.poll();
			next.add(new CombinedBinary<>(node,null,false));
			next.add(new CombinedBinary<>(node,null,true));
		}


		return root;	
	}


	/**
	 * @param root The {@link CombinedBinary} to start from
	 * @param toTest The object to test
	 * @return The leaf {@link CombinedBinary} the provided object should fall into
	 */
	public static <Q> CombinedBinary<Q> get(CombinedBinary<Q> root, Q toTest){

		Objects.requireNonNull(root, "root is Null");
		Objects.requireNonNull(toTest, "The object to be tested is Null");

		CombinedBinary<Q> last = root;

		while(!last.isLeaf())
			last = last.getNext(toTest);

		return last;
	}


}

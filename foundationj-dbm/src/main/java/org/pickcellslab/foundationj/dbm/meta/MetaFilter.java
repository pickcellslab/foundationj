package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.access.RegenerableDataPointer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.AllOrSome;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludeExcludeKeyInit;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.mapping.data.MultitonOutcome;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapping;

@Data(typeId = "MetaFilter", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaFilter extends MetaItem implements MetaStrategy, RegenerableDataPointer{

	private static final AKey<String> predicateKey = AKey.get("Predicate", String.class);

	private boolean isTransient = false;


	private MetaFilter(String uuid){
		super(uuid);
	}



	public static MetaFilter getOrCreate(
			MetaInstanceDirector mgr,
			String name,
			String description,
			ExplicitPredicate<?> predicate,
			MetaQueryable usedOn) throws IllegalArgumentException, NonDataMappingException{

		Objects.requireNonNull(mgr, "The provided MetaInstanceDirector cannot be null");
		Objects.requireNonNull(name, "The provided name cannot be null");
		Objects.requireNonNull(predicate, "The provided predicate cannot be null");
		Objects.requireNonNull(usedOn, "The provided MetaQueryable cannot be null");	


		assert !description.isEmpty() : "The description of a MetaFilter cannot be empty";

		MultitonOutcome<MetaFilter> out = null;
		try {
			out = mgr.getOrCreateWithOutcome(MetaFilter.class, MetaItem.randomUID(MetaFilter.class));
		} catch (InstantiationHookException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(out.wasCreated()){// Check if it was retrieved or if it already existed
			final MetaFilter mf = out.getInstance();
			mf.setAttribute(MetaModel.name, name);
			mf.setAttribute(MetaModel.desc, description == null ? "NA" : description);
			mf.setAttribute(predicateKey, NonDataMapping.convertToString(predicate));		
			new DataLink(POINTS_TO, mf, usedOn, true);
		}

		return out.getInstance();

	}


	/**
	 * Sets this MetaFilter isTransient flag to true.
	 */
	public void setTransient() {
		isTransient = true;
	}


	/**
	 * @return true if this MetaFilter is to be used only transiently, false otherwise.
	 */
	public boolean isTransient() {
		return isTransient;
	}




	@Override
	public MetaQueryable getTarget(){
		return (MetaQueryable) typeMap.get(POINTS_TO).iterator().next().target();
	}



	@SuppressWarnings("unchecked")
	public <T> ExplicitPredicate<T> toFilter(){
		try {
			return (ExplicitPredicate<T>)NonDataMapping.restoreFromString(getAttribute(AKey.get("Predicate", String.class)).get());
		} catch (NonDataMappingException e) {
			throw new RuntimeException("The "+AKey.get("Predicate", String.class).toString()+
					" property of this object has been modified, Unable to rebuild the Predicate", e);
		}	
	}






	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>Filter:</strong>"+
				"<p><ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"</li></p></HTML>";
	}


	@Override
	public String toString(){
		return getAttribute(name).get();
	}





	@Override
	public int deleteData(DataAccess access) throws DataAccessException {
		return getTarget().initiateDeletion(access).completely().useFilter(this.toFilter()).run();
	}


	@Override
	public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
		return 
				getTarget().initiateFindOne(access).using(action)
				.useFilter(F.select(DataItem.idKey).equalsTo(dbId).and(this.toFilter()))
				.run();
	}

	
	@Override
	public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
		return
				getTarget().initiateFindOne(access)
				.using(action)
				.useFilter(this.toFilter())
				.run();
	}
	
	
	@Override
	public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
		return getTarget().initiateRead(access, limit).with(action).inOneSet().useFilter(this.toFilter()).run();
	}

	@Override
	public void updateData(DataAccess access, Updater action) throws DataAccessException {
		getTarget().initiateUpdate(access, action).useFilter(this.toFilter()).run();
	}

	@Override
	public List<MetaQueryable> queriedMetaObjects() {
		return Collections.singletonList(getTarget());
	}


	@Override
	public String name() {
		return getAttribute(name).get();
	}

	@Override
	public String description() {
		return getAttribute(desc).get();
	}



	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.creator, MetaModel.desc, MetaFilter.predicateKey);
	}




	@Override
	public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> initRegeneration(
			DataAccess access) {
		Objects.requireNonNull(access);
		return access.queryFactory().regenerate(getTarget().itemDeclaredType());
	}



	@Override
	public RegeneratedItems finaliseRegeneration(AccessChoice<NodeItem, QueryRunner<RegeneratedItems>> choice) throws DataAccessException {
		Objects.requireNonNull(choice);
		return choice.useFilter(toFilter()).run();
	}


	@Override
	public RegenerableDataPointer subset(ExplicitPredicate<DataItem> predicate) {

		Objects.requireNonNull(predicate);

		final ExplicitPredicate<DataItem> filter = predicate.and(toFilter());

		return new RegenerableDataPointer() {

			@Override
			public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
				return
						getTarget().initiateFindOne(access).using(action)
						.useFilter(filter.and(F.select(DataItem.idKey).equalsTo(dbId)))
						.run();
			}


			@Override
			public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
				return
						getTarget().initiateFindOne(access)
						.using(action)
						.useFilter(filter)
						.run();
			}


			@Override
			public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
				return getTarget().initiateRead(access, limit).with(action).inOneSet().useFilter(filter).run();
			}

			@Override
			public void updateData(DataAccess access, Updater action) throws DataAccessException {
				getTarget().initiateUpdate(access, action).useFilter(filter).run();
			}

			@Override
			public List<MetaQueryable> queriedMetaObjects() {
				return MetaFilter.this.queriedMetaObjects();
			}

			@Override
			public int deleteData(DataAccess access) throws DataAccessException {
				throw new RuntimeException("Forbidden for now");
			}

			@Override
			public RegenerableDataPointer subset(ExplicitPredicate<DataItem> predicate2) {
				Objects.requireNonNull(predicate2);
				return MetaFilter.this.subset(predicate.and(predicate2));
			}

			@Override
			public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> initRegeneration(
					DataAccess access) {
				Objects.requireNonNull(access);
				return access.queryFactory().regenerate(getTarget().itemDeclaredType());
			}



			@Override
			public RegeneratedItems finaliseRegeneration(AccessChoice<NodeItem, QueryRunner<RegeneratedItems>> choice) throws DataAccessException {
				Objects.requireNonNull(choice);
				return choice.useFilter(filter).run();
			}
		};
	}

}


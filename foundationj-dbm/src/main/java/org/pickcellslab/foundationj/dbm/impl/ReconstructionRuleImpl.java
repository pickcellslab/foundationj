package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.dbm.queries.ReconstructionRule;


class ReconstructionRuleImpl implements ReconstructionRule{



	private final String description;

	private final int maxDepth;

	private final Map<String, Direction> included;

	private final Map<ExplicitPredicate<? super NodeItem>, Decision> filters;

	private final Decision defaultDecision;

	private final boolean includeAllKeys;

	private final Set<AKey<?>> includedKeys;

	private final Policy linkPolicy;

	private final boolean includeAllLinks;


	private final boolean includeAllNodes;

	private final Policy keyPolicy;

	private Set<String> excludedStringKeys;



	ReconstructionRuleImpl(int maxDepth){
		this.maxDepth = maxDepth;
		this.filters = null;
		this.defaultDecision = null;
		this.includeAllLinks = true;
		this.includeAllNodes = true;
		this.includeAllKeys = true;
		this.included = null;	
		this.includedKeys = null;
		this.linkPolicy = Policy.INCLUDED;
		this.keyPolicy = Policy.INCLUDED;
		this.description = "All to depth "+maxDepth;
	}



	ReconstructionRuleImpl(RegenerationBuilder<?,?> builder){

		this.maxDepth = builder.maxDepth;
		this.filters = builder.filters;
		this.defaultDecision = builder.defaultDecision;
		this.includeAllLinks = builder.includeAllLinks;
		this.includeAllNodes = builder.includeAllNodes;
		this.includeAllKeys = builder.includeAllKeys;
		this.included = builder.included;	
		this.includedKeys = builder.includedKeys;
		this.linkPolicy = builder.linkPolicy;
		this.keyPolicy = builder.keyPolicy;
		this.description = builder.description;

		if(keyPolicy == Policy.EXCLUDED)
			excludedStringKeys = includedKeys.stream().map(k->k.toString()).collect(Collectors.toSet());
	}






	@Override
	public String description(){
		return description;
	}


	@Override
	public Policy linkPolicy(){
		return linkPolicy;
	}


	@Override
	public Policy keysPolicy(){
		return keyPolicy;
	}



	/**
	 * @return {@code true} if there are no filters defined for the links to retrieve. In other words, return true
	 * if all the links are to be included during the reconstruction process, false otherwise.
	 */
	@Override
	public final boolean noLinkConstraints() {
		return includeAllLinks;
	}


	@Override
	public final boolean noNodeConstraints() {
		return includeAllNodes;
	}


	/**
	 * @return {@code true} if all the existing properties are to be included in the new instance of the DataItem
	 */
	@Override
	public final boolean allKeysIncluded() {
		return includeAllKeys;
	}



	/**
	 * @return The maximum depth of the graph linked to the NodeItem to retrieve
	 */
	@Override
	public final int getMaxDepth() {
		return maxDepth;
	}





	@Override
	public final Map<String, Direction> getDecisionsOnLinks() {
		return included;
	}


	@Override
	public Decision defaultDecision(){
		return defaultDecision;
	}


	@Override
	public final Map<ExplicitPredicate<? super NodeItem>, Decision> getDecisionsOnNodes() {
		return filters;
	}


	private Map<Class<?>, Set<String>> cacheString = new HashMap<>();




	@Override
	public Stream<String> getKeysToRetrieve(DataItem item, Stream<String> availableDefinitions){

		if(this.includeAllKeys)
			return availableDefinitions;

		else if(keyPolicy == Policy.EXCLUDED){
			return availableDefinitions.filter(s->!excludedStringKeys.contains(s));			
		}
		else{
			Set<String> toRetrieve = cacheString.get(item.getClass());
			if(null == toRetrieve){
				toRetrieve = 
						Stream.concat(item.minimal(), includedKeys.stream())
						.map(k->k.toString()).collect(Collectors.toSet());
				cacheString.put(item.getClass(), toRetrieve);
			}
			final Set<String> fSet = toRetrieve;
			return availableDefinitions.filter(s->fSet.contains(s));
		}
		

	}



}

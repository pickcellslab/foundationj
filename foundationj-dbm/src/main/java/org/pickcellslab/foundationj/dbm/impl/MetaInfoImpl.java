package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.Traverser;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link MetaInfo} is a "slice" of the {@link MetaImpl}. It can be built using a {@link MetaBuilder} or
 * a {@link MetaTrackingTraverser}. It may contain several subgraph of the MetaModel and provide access to each subgraph
 * via {@link Traverser}. Importantly, an instance of this class is provided to Persistence implementation when {@link WritableDataItem}
 * need to be stored. This allows the MetaModel to be up to date for every write to the database 
 * @author Guillaume Blin
 *
 */
class MetaInfoImpl implements MetaInfo{

	private static Logger log = LoggerFactory.getLogger(MetaInfoImpl.class);

	private final LinkedList<MetaClass> list = new LinkedList<>();
	private String summary;

	public MetaInfoImpl(MetaBuilder builder){

		list.addAll(builder.classMap.values());

		if(log.isTraceEnabled()){
			buildSummary();
			log.trace(summary);
			log.trace("MetaInfo built");
		}

	}

	private void buildSummary(){
		StringBuilder sb = new StringBuilder();
		sb.append("MetaInfo : ");

		//build the summary
		for(MetaClass mc : list){
			sb.append("\n"+mc.toString()+"\n");
			sb.append("Contains The following Keys: \n");
			mc.keys().forEach(mk->sb.append(mk.toString()+"\n"));
				
			sb.append("Contains The following Links: \n");
			mc.metaLinks().forEach(ml->{
				sb.append(ml.toString()+"\n");
				ml.keys().forEach(mlmk->sb.append("With AKey: "+mlmk.toString()+"\n"));
			});
		}
		summary = sb.toString();
	}


	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.MetaInf#items()
	 */
	@Override
	public List<MetaModel> items(){
		List<MetaModel> items = new ArrayList<>();
		items.addAll(list);
		return items;
	}

	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.MetaInf#traversers()
	 */
	@Override
	public Collection<StepTraverser<NodeItem,Link>> traversers(){

		List<StepTraverser<NodeItem,Link>> result = new ArrayList<>();

		while(!list.isEmpty()){
			DefaultTraversal<NodeItem,Link> t = Traversers.breadthFirst(list.peekFirst()).traverse();
			result.add(Traversers.breadthfirst(list.pollFirst(),
					//Apply constrainst to avoid storing MetaDataset
					Traversers.newConstraints().fromMinDepth().toMaxDepth().traverseLink(MetaModel.fromLink, Direction.BOTH)
					.and(MetaModel.toLink, Direction.BOTH)
					.and(MetaModel.tagLink, Direction.BOTH)
					.and(MetaModel.keyLink, Direction.BOTH)
					.includeAllNodes()	
					));
			list.removeAll(t.nodes);
		}

		return result;
	}


	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.MetaInf#summarize()
	 */
	@Override
	public String summarize(){
		if(summary == null)
			buildSummary();
		return summary;
	}



	// Builder
	//-------------------------------------------------------------------------------------------


	/**
	 * A Builder for {@link MetaInfo}
	 * @author Guillaume
	 *
	 */
	public static class MetaBuilder{

		private final String uk = "Unknown", na = "Not Available";

		
		private final Map<String, MetaClassImpl> classMap = new HashMap<>();
		private final Map<String, Set<MetaKeyImpl<?>>> metaKeyMap = new HashMap<>();
		private final Map<String, Set<AKey<?>>> keyMap = new HashMap<>();
		private final Map<String, Set<AKey<?>>> optionalKeyMap = new HashMap<>();
		private final Map<String, Set<String>> outLinkMap = new HashMap<>();
		private final Map<String, Set<String>> inLinkMap = new HashMap<>();
		private final Map<String, Set<MetaLinkImpl>> metaLinkMap = new HashMap<>();

		private Set<String> documented = new HashSet<>();


		private Session<?, ?> session;
		private final MetaInstanceDirector director;

		public MetaBuilder(Session<?,?> session) {
			this.session = session;
			this.director = session.metaModel().getInstanceManager();
		}

		/**
		 * Use this method if you think there might have been previous call to {@link #feed(Class, String, String)}
		 * and you want to overwrite creator and description
		 * @throws InstantiationHookException 
		 */
		MetaBuilder feedOverride( WritableDataItem prototype, String creator, String description) throws InstantiationHookException{

			//Check creator and description are not null
			if(creator == null){
				log.warn("Fed with a null creator, replaced by \"Unknown\"");
				creator = uk;
			}
			if(description == null){
				log.warn("Fed with a null description, replaced by \"Not Available\"");
				description = na;
			}

			//Create the MetaClass
			MetaClassImpl mc = classMap.get(prototype.declaredType());
			if(mc == null){
				//TODO check generator
				mc = MetaClassImpl.getOrCreate(director, prototype.getClass(), prototype.declaredType(), creator, description);
				classMap.put(prototype.declaredType(), mc);
				documented.add(prototype.declaredType());

				//investigate the tagged interfaces
				for(MetaTag mt :  MetaImpl.getMetaTags(prototype.getClass(), director))
					mc.setTag(mt);
			} 
			else{
				mc.setAttribute(AKey.get("Creator",String.class), creator);
				mc.setAttribute(MetaClass.desc, description);
			}

			return this;
		}



		public MetaBuilder feed(NodeItem prototype, String creator, String description) throws InstantiationHookException{

			Objects.requireNonNull(prototype, "The prototype is null");

			//Check creator and description are not null
			if(creator == null){
				log.warn("Fed with a null creator, replaced by \"Unknown\"");
				creator = uk;
			}
			if(description == null){
				log.warn("Fed with a null description, replaced by \"Not Available\"");
				description = na;
			}


			//Create the MetaClass
			MetaClassImpl mc = classMap.get(prototype.declaredType());
			if(mc == null){
				mc = MetaClassImpl.getOrCreate(director, prototype.getClass(), prototype.declaredType(), creator, description);
				classMap.put(prototype.declaredType(), mc);  
				documented.add(prototype.declaredType());
				//investigate the tagged interfaces
				for(MetaTag mt :  MetaImpl.getMetaTags(prototype.getClass(), director))
					mc.setTag(mt);

				//init all the sets
				keyMap.put(prototype.declaredType(), new HashSet<>());
				optionalKeyMap.put(prototype.declaredType(), new HashSet<>());
				metaKeyMap.put(prototype.declaredType(), new HashSet<>());
				outLinkMap.put(prototype.declaredType(), new HashSet<>());
				inLinkMap.put(prototype.declaredType(), new HashSet<>());
				metaLinkMap.put(prototype.declaredType(), new HashSet<>());


			}     

			return this;
		}


		public MetaBuilder feed(
				Link link,
				String creator,
				String description) throws InstantiationHookException{

			NodeItem sourceClass = link.source();
			NodeItem targetClass = link.target();

			//feed source and target to make sure they exist
			feed(sourceClass, uk, na);
			feed(targetClass, uk, na);

			//if the link is new for any of the linked item then create the MetaLink
			if(outLinkMap.get(sourceClass.declaredType()).add(link.declaredType()) | inLinkMap.get(targetClass.declaredType()).add(link.declaredType())){

				//create the MetaLink
				final MetaLinkImpl ml = MetaLinkImpl.getOrCreate(director, link.declaredType(), classMap.get(sourceClass.declaredType()), classMap.get(targetClass.declaredType()), description);
				ml.setAttribute(AKey.get("Creator",String.class), creator);
				metaLinkMap.get(sourceClass.declaredType()).add(ml);
				metaLinkMap.get(targetClass.declaredType()).add(ml);

				//Create the MetaKey
				link.getValidAttributeKeys().filter(k->k!=DataItem.idKey).forEach(k->{
					MetaKeyImpl<?> mk = MetaKeyImpl.getOrCreate(director, k, -1, ml, uk, na);
					Object o = link.getAttribute(k).get();
					if(o.getClass().isArray()){
						mk.setLength(Array.getLength(o));
					}
				});
				//Now add the dbID!
				MetaKeyImpl.getOrCreate(director, DataItem.idKey, -1, ml, "DataBase", "A Unique id for this type of object in the database");				

			}


			return this;
		}



		public <E> MetaBuilder feed(
				AKey<? extends E> k, 
				E prototype, 
				String creator, 
				String description, 
				NodeItem owner){
			if(owner instanceof MetaClassImpl){ //TODO exception for instances of MetaModel not MetaClass
				final String ownerType = ((MetaClassImpl) owner).itemDeclaredType();
				classMap.put(ownerType, (MetaClassImpl) owner);
				documented.add(ownerType);
				//init all the sets
				keyMap.putIfAbsent(ownerType, new HashSet<>());
				optionalKeyMap.putIfAbsent(ownerType, new HashSet<>());
				metaKeyMap.putIfAbsent(ownerType, new HashSet<>());
				outLinkMap.putIfAbsent(ownerType, new HashSet<>());
				inLinkMap.putIfAbsent(ownerType, new HashSet<>());
				metaLinkMap.putIfAbsent(ownerType, new HashSet<>());
				return feed(k, prototype, creator, description,((MetaClass) owner).itemClass(session.dataRegistry()), ownerType);
			}
			return feed(k, prototype, creator, description, owner.getClass(), owner.declaredType());
		}





		/**
		 * @param k
		 * @param prototype
		 * @param creator
		 * @param description
		 * @param ownerClazz
		 * @param ownerType
		 * @return
		 */
		public <E> MetaBuilder feed(
				AKey<? extends E> k, 
				E prototype, 
				String creator, 
				String description, 
				Class<? extends NodeItem> ownerClazz,
				String ownerType){

			assert k!=null : "k is null";
			assert prototype!=null : "prototype is null";
			assert creator!=null : "creator is null";
			assert description!=null : "description is null";
			assert ownerClazz!=null : "owner class is null";
			assert ownerType!=null : "ownerType is null";

			if(k == WritableDataItem.idKey)
				return this;

			//Test if the owner exists
			MetaClassImpl metaClass = classMap.get(ownerType);

			if(metaClass == null){

				//TODO check generator
				metaClass = MetaClassImpl.getOrCreate(director, ownerClazz, ownerType, uk, na);
				classMap.put(ownerType, metaClass); 
				//Sets a flag for this class to specify that it contains documented keys
				documented.add(ownerType);
				//init all the sets
				keyMap.put(ownerType, new HashSet<>());
				optionalKeyMap.put(ownerType, new HashSet<>());
				metaKeyMap.put(ownerType, new HashSet<>());
				outLinkMap.put(ownerType, new HashSet<>());
				inLinkMap.put(ownerType, new HashSet<>());
				metaLinkMap.put(ownerType, new HashSet<>());


			}

			//If keyMap does not already have the key registered then create a MetaKey
			if(keyMap.get(ownerType).add(k)){
				System.out.println("MetaInfo -> Creating MetaKey for "+ownerType+" ("+k+") with description: "+description);
				//Create the metakey
				int lengthIfArray = prototype.getClass().isArray() ? Array.getLength(prototype) : -1;
				MetaKeyImpl<?> mk = MetaKeyImpl.getOrCreate(director, k, lengthIfArray, metaClass, creator, description);
				      

				//update the map
				metaKeyMap.get(ownerType).add(mk);
			}
			return this;
		}


		/**
		 * Tests the provided {@link WritableDataItem}: If its class is encountered for the first time, then 
		 * the DataItem is used as a "prototype" for the class, each attribute is read to create a {@link MetaKey}.
		 * If the class of the DataItem is already known, then the valid keys of the item are looked up
		 * to see if there exists any optional key. Note that {@link Link} objects attached to the item are ignored
		 * however if the item is itself a link then it will be processed accordingly
		 * @throws InstantiationHookException 
		 */
		synchronized MetaBuilder feed(WritableDataItem item) throws InstantiationHookException {      

			//Check if we have a Link      			
			if(Link.class.isAssignableFrom(item.getClass())){        
				Link l = (Link) item;        
				return feed(l, uk, na);      
			}


			String itemType = item.declaredType(); 
			MetaClassImpl metaClass = classMap.get(itemType);
			boolean firstClassEncounter = metaClass == null;
			if(firstClassEncounter){
				//System.out.println("MetaInfo First Time: "+item.getClass());
				metaClass = MetaClassImpl.getOrCreate(director, item.getClass(), itemType, uk, na);
				classMap.put(itemType, metaClass); 
				//investigate the tagged interfaces
				for(MetaTag mt :  MetaImpl.getMetaTags(item.getClass(), director))
					metaClass.setTag(mt);
				//init all the sets
				keyMap.put(itemType, new HashSet<>());
				optionalKeyMap.put(itemType, new HashSet<>());
				metaKeyMap.put(itemType, new HashSet<>());
				outLinkMap.put(itemType, new HashSet<>());
				inLinkMap.put(itemType, new HashSet<>());
				metaLinkMap.put(itemType, new HashSet<>());
			}



			//If keys were documented
			if(documented.remove(itemType)){
				//add the undocumented keys	
				final MetaClassImpl fmc = metaClass;
				item.getValidAttributeKeys().filter(k->k!=WritableDataItem.idKey).forEach(toAdd->{
					if(!keyMap.get(itemType).contains(toAdd)){
						//Create the metakey
						MetaKeyImpl<?> mk = MetaKeyImpl.getOrCreate(director, toAdd, -1, fmc, uk, na);
						Object prototype = item.getAttribute(toAdd).get();
						if(prototype.getClass().isArray()){
							mk.setLength(Array.getLength(prototype));
						}         
						//update the maps
						metaKeyMap.get(itemType).add(mk);
						keyMap.get(itemType).add(toAdd);
					}
				});
				//Now add the dbID!
				MetaKeyImpl<?> mk = MetaKeyImpl.getOrCreate(director, DataItem.idKey, -1, metaClass, "DataBase", "A Unique id for this type of object in the database");
				metaKeyMap.get(itemType).add(mk);
				keyMap.get(itemType).add(DataItem.idKey);
			}




			//If first time create a MetaKey for each AKey
			else if(firstClassEncounter){
				final MetaClassImpl fmc = metaClass;
				item.getValidAttributeKeys().filter(k->k!=WritableDataItem.idKey).forEach(k->{
					//Create the metakey
					MetaKeyImpl<?> mk = MetaKeyImpl.getOrCreate(director, k, -1, fmc, uk, na);
					Object prototype = item.getAttribute(k).get();
					if(prototype.getClass().isArray()){
						mk.setLength(Array.getLength(prototype));
					}         
					//update the maps
					metaKeyMap.get(itemType).add(mk);
					keyMap.get(itemType).add(k);
				});
				//Now add the dbID!
				MetaKeyImpl<?> mk = MetaKeyImpl.getOrCreate(director, WritableDataItem.idKey, -1, metaClass, "DataBase", "A Unique id for this type of object in the database");
				metaKeyMap.get(itemType).add(mk);
				keyMap.get(itemType).add(WritableDataItem.idKey);
			}

			//If not first time check  for optional keys
			else{

				//Check the registered keys
				final Set<AKey<?>> registered = keyMap.get(itemType);
				final Set<AKey<?>> optional = new HashSet<AKey<?>> (registered);

				//find optional keys
				//among the known keys
				item.getValidAttributeKeys().forEach(k->optional.remove(k));
				optionalKeyMap.get(itemType).addAll(optional);

				//among the new keys
				final Set<AKey<?>> optionalNew = item.getValidAttributeKeys().collect(Collectors.toSet());//FIXME throws concurrent modif if item is modified outside this method
				optionalNew.removeAll(registered);

				
				//create the MetaKeys
				for(AKey<?> k : optionalNew){
					int lengthIfArray = -1;
					Object prototype = item.getAttribute(k).get();
					if(prototype.getClass().isArray()){
						lengthIfArray = Array.getLength(prototype);
					}
					MetaKeyImpl<?> mk = MetaKeyImpl.getOrCreate(director, k, lengthIfArray, metaClass, uk, na);
					
					//update the maps
					metaKeyMap.get(itemType).add(mk);
				}
				optionalKeyMap.get(itemType).addAll(optionalNew);
			}

			return this;
		}



		public MetaInfoImpl buildInfo(){

			//System.out.println("MetaInfo : building!");

			//Set all optional metakeys
			for(Entry<String, Set<AKey<?>>> e : optionalKeyMap.entrySet()){
				String currentClass = e.getKey();
				for(AKey<?> k : e.getValue())
					if(k!=WritableDataItem.idKey)
						for(MetaKeyImpl<?> mk : metaKeyMap.get(currentClass)){
							if(mk.toAKey() == k)
								mk.setIsOptional(true);
						}
			}

			//System.out.println("MetaInfo : built!");

			return new MetaInfoImpl(this);
		}





	}



}

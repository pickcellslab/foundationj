package org.pickcellslab.foundationj.queryui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.functions.ConnectionSelector;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;

@SuppressWarnings("serial")
public class ConnectionSelectorUI extends JDialog {

	private boolean wasCancelled = true;
	private MetaClass node;


	final JLabel lblDirection = new JLabel("Direction");
	final JComboBox<Direction> dirBox = new JComboBox<>();
	final JLabel lblType = new JLabel("Type");
	final JComboBox<MetaLink> typeBox = new JComboBox<>();
	final JLabel lblFilterOnAttributes = new JLabel("Filter");
	final JComboBox<MetaFilter> attFBox = new JComboBox<>();


	private ConnectionSelector selector;

	
	public ConnectionSelectorUI(UITheme theme, DataRegistry registry, MetaClass node) {
		
		this.node = node;

		//Set renderers
		typeBox.setRenderer(new MetaRenderer(theme, registry));
		attFBox.setRenderer(new MetaRenderer(theme, registry));


		//InitContent
		for(Direction d : Direction.values())
			dirBox.addItem(d);
		dirBox.setSelectedItem(Direction.BOTH);

		populateTypeBox();


		//Controllers
		//---------------------------------------------------------------------------------------------------------
		dirBox.addActionListener(l->{
			populateTypeBox();
		});

		typeBox.addActionListener(l->{
			populateAttFBox();
		});

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(l->{

			//Determine the filter on the adjacent item
			MetaLink ml = (MetaLink)typeBox.getSelectedItem();
			String adj = null;
			ExplicitPredicate<NodeItem> p = null;

			if(node == ml.source()){
				adj = ml.target().declaredType();
			}else{
				adj = ml.source().declaredType();				
			}
			p = P.isNodeWithDeclaredType(adj);
			
			//Now find out if there is a filter chosen on the link
			ExplicitPredicate<WritableDataItem> lP = P.isDeclaredType(ml.linkType()); 
			MetaFilter f = (MetaFilter) attFBox.getSelectedItem();
			if(f!=null)
				lP.merge(Op.Bool.AND, f.toFilter());

			selector = new ConnectionSelector((Direction)dirBox.getSelectedItem(), lP, p);

			wasCancelled = false;
			this.dispose();
		});

		
		
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->{
			this.dispose();
		});


		//Layout
		//---------------------------------------------------------------------------------------------------------

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblFilterOnAttributes)
								.addComponent(lblDirection)
								.addComponent(lblType))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(dirBox, 0, 152, Short.MAX_VALUE)
										.addComponent(typeBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(attFBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addContainerGap(20, Short.MAX_VALUE))
										.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
												.addContainerGap(109, Short.MAX_VALUE)
												.addComponent(btnOk)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(btnCancel)
												.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblDirection)
								.addComponent(dirBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblType)
										.addComponent(typeBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
												.addComponent(lblFilterOnAttributes)
												.addComponent(attFBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addPreferredGap(ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(btnOk)
														.addComponent(btnCancel))
														.addContainerGap())
				);
		getContentPane().setLayout(groupLayout);
	}

	private void populateTypeBox() {

		typeBox.removeAllItems();

		node.metaLinks((Direction)dirBox.getSelectedItem()).forEach(l->typeBox.addItem(l));

		populateAttFBox();

	}

	private void populateAttFBox() {

		attFBox.removeAllItems();

		//To populate attFBox, we need to find the filters that can be used on the currently selected
		//MetaLink (+ none)
		attFBox.addItem(null);

		MetaLink ml = (MetaLink) typeBox.getSelectedItem();
		if(ml!=null){
			ml.filters().forEach(f->attFBox.addItem(f));
		}

	}

	public boolean wasCancelled(){
		return wasCancelled ;
	}

	public ConnectionSelector getSelector(){
		return selector;
	}

	public MetaLink getTemplate() {
		return (MetaLink) typeBox.getSelectedItem();
	}
}

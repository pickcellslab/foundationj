package org.pickcellslab.foundationj.dbm.db;

import java.io.File;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Service;
import org.pickcellslab.foundationj.dbm.events.ConnexionFailedException;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

/**
 * 
 * The {@link Core} interface for the graph database back-end.
 * 
 * @author Guillaume Blin
 *
 * @param <V> Th
 * @param <E>
 */
@Core
@Service
public interface DataStoreConnector<V,E> {

	/**
	 * @param file
	 * @return true if the given {@link File} is a valid database, false otherwise.
	 */
	public boolean isValidDatabase(File file);
	
	/**
	 * @param request
	 * @param registry
	 * @return The {@link DataStoreManager}
	 * @throws ConnexionFailedException If the connection fails.
	 */
	public DataStoreManager<V, E> connect(ConnexionRequest request, DataRegistry registry) throws ConnexionFailedException;
	
}

package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;

class MetaDeletionBuilder<V,E> {

	private TargetType<V, E, V> target;


	MetaDeletionBuilder(TargetType<V,E,V> target) {
		this.target = target;
	}


	RegeneratedItems run(TraversalDefinition td, int reconstructionDepth) throws DataAccessException{
		return new RegenerateQBuilder<>(target, null)
				.setAction(
						new ReconstructionAction<>(
								new MetaDeletion<>(
										new ReconstructionRuleImpl(reconstructionDepth)))).useTraversal(td).run();
	}

}

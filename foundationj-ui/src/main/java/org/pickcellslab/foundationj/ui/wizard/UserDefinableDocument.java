package org.pickcellslab.foundationj.ui.wizard;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

import com.alee.extended.tab.DocumentData;

public class UserDefinableDocument<T extends Component & ValidityToggle & Copyable<T>> extends DocumentData<T> implements UserDefinable<UserDefinableDocument<T>>, ValidityListener {

	private final List<FinishListener> lstrs = new ArrayList<>();

	public UserDefinableDocument(String id, Icon icon, T component) {
		super(id, icon, id, component);
		this.setCloseable(false);
		component.addValidityListener(this);
	}

	@Override
	public String getName() {
		return this.getTitle();
	}

	@Override
	public void setName(String name) {
		this.setTitle(name);
	}

	@Override
	public boolean isFinished() {
		return ((ValidityToggle) this.component).validity();
	}

	@Override
	public void addFinishListener(FinishListener l) {
		if(null!=l)
			lstrs.add(l);
	}

	@Override
	public void removeFinishListener(FinishListener l) {
		lstrs.remove(l);
	}

	@Override
	public void nextValidityChanged(ValidityToggle wizardNode) {
		lstrs.forEach(l->l.isFinished(this, isFinished()));
	}
	
	public T getData(){
		return this.component;
	}

	@Override
	public void copyState(UserDefinableDocument<T> other) {
		this.component.copyState(other.component);
	}

	

	
	
}

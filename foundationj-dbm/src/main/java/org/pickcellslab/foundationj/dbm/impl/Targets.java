package org.pickcellslab.foundationj.dbm.impl;

import java.util.Collections;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.FilteredIterator;
import org.pickcellslab.foundationj.datamodel.tools.MorphedIterator;
import org.pickcellslab.foundationj.datamodel.tools.OperationSuplier;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.DatabaseNode;
import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.db.UpdatableLink;
import org.pickcellslab.foundationj.dbm.db.UpdatableNode;
import org.pickcellslab.foundationj.dbm.db.UpdateInfo;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaTag;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition.TraversalPolicy;


final class Targets {


	private Targets(){/*Not instantiable*/};


	public static <V,E> PathTargetType<V,E> getPathTypes(String name, Session<V,E> s){
		return new PathTargetType<>(s,name);
	}

	@Deprecated
	public static <V,E> NodeClass<V,E> get(Class<? extends NodeItem> clazz, Session<V,E> s){
		Objects.requireNonNull(clazz,"The provided class cannot be null");
		return new NodeClass<>(clazz,s,-1);
	};

	public static <V,E> Labels<V,E> getLabel(String label, Session<V,E> s) {
		Objects.requireNonNull(label,"The provided label cannot be null");
		return new Labels<>(label,s,-1);
	}		


	public static <V,E> Links<V,E> getLink(String linkType, Session<V,E> s) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new Links<>(linkType,s,-1);
	}


	public static <V,E> STLinks<V,E> getLink(String linkType, String srcType, String tgType, Session<V,E> s) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new STLinks<>(linkType, srcType, tgType,s,-1);
	}


	@Deprecated
	public static <V,E> NodeClass<V,E> get(Class<? extends NodeItem> clazz, Session<V,E> s, long limit){
		Objects.requireNonNull(clazz,"The provided class cannot be null");
		return new NodeClass<>(clazz, s, limit);
	};

	public static <V,E> Labels<V,E> getLabel(String label, Session<V,E> s, long limit) {
		Objects.requireNonNull(label,"The provided label cannot be null");
		return new Labels<>(label, s, limit);
	}		

	public static <V,E> Links<V,E> getLink(String linkType, Session<V,E> s, long limit) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new Links<>(linkType, s, limit);
	}


	public static <V,E> STLinks<V,E> getLink(String linkType, String srcType, String tgType, Session<V,E> s, long limit) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new STLinks<>(linkType, srcType, tgType, s, limit);
	}









	@Deprecated
	public static <V,E> ImplTypedClass<V,E> getRegenerable(Class<? extends NodeItem> clazz, Session<V,E> s){
		Objects.requireNonNull(clazz,"The provided class cannot be null");
		return new ImplTypedClass<>(clazz,s);
	}

	public static <V,E> ImplTypedLabels<V,E> getRegenerable(String label, Session<V, E> session) {
		Objects.requireNonNull(label,"The provided label cannot be null");
		return new ImplTypedLabels<>(label,session);
	}


	/*
	public static <V,E> ImplTypedLinks<V,E> getRegenerableLinks(String linkType, Session<V, E> session) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new ImplTypedLinks<>(linkType,session);
	}
	 */

	@Deprecated
	public static <V,E> DeletableNodeClass<V,E> getDeletableNode(Class<? extends NodeItem> clazz, Session<V,E> s){
		Objects.requireNonNull(clazz,"The provided class cannot be null");
		return new DeletableNodeClass<>(clazz,s);
	};

	public static <V,E> DeletableLabels<V,E> getDeletableLabel(String label, Session<V,E> s) {
		Objects.requireNonNull(label,"The provided label cannot be null");
		return new DeletableLabels<>(label,s);
	}		

	public static <V,E> LinksDeletable<V,E> getDeletableLink(String linkType, Session<V,E> s) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new LinksDeletable<>(linkType,s);
	}

	public static <V,E> STLinksDeletable<V,E> getDeletableLink(String linkType, String srcType, String tgType, Session<V,E> s) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new STLinksDeletable<>(linkType, srcType, tgType, s);
	}





	@Deprecated
	public static <V,E> UpdatableNodeClass<V,E> getUpdatableNode(Class<? extends NodeItem> clazz, Session<V,E> s, UpdateInfo ui){
		Objects.requireNonNull(clazz,"The provided class cannot be null");
		return new UpdatableNodeClass<>(clazz, s, ui);
	};

	public static <V,E> UpdatableLabels<V,E> getUpdatableLabel(String label, Session<V,E> s, UpdateInfo ui) {
		Objects.requireNonNull(label,"The provided label cannot be null");
		return new UpdatableLabels<>(label, s, ui);
	}		

	public static <V,E> UpdatableLinks<V,E> getUpdatableLink(String linkType, Session<V,E> s, UpdateInfo ui) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new UpdatableLinks<>(linkType, s, ui);
	}


	public static <V,E> STUpdatableLinks<V,E> getUpdatableLink(String linkType, String srcType, String tgType, Session<V,E> s, UpdateInfo ui) {
		Objects.requireNonNull(linkType,"The provided link type cannot be null");
		return new STUpdatableLinks<>(linkType, srcType, tgType, s, ui);
	}

	
	
	
	private static Optional<List<DataPointer>> findNodeMetaTypes(String label, DataAccess access){
		final MetaClass mc = access.metaModel().getMetaModel(label);
		if(mc != null) return Optional.of(Collections.singletonList(mc));
		else { 

			// This happens when a label was used for the query or when the typeId was used 
			// for objects which possess a different declaredtype.

			// Case 1 : There exists a MetaTag for the label()
			final MetaTag mt = access.metaModel().getMetaObjects(MetaTag.class).filter(mtag->mtag.name().equals(label)).findAny().orElse(null);
			if(mt!=null)
				return Optional.of(Collections.singletonList(mt));
			// Case 2 : the typeId was used in the query and the declaredtype of objects is different 
			// (so there are neither MetaTag nor MetaClass)
			else {
				//We need to screen MetaClasses which have an itemTypeId equals to label()
				return Optional.of(
						access.metaModel().getMetaObjects(MetaClass.class).filter(m->m.itemTypeId().equals(label))
						.collect(Collectors.toList())
						);
			}
		}
	};
	




	static class PathTargetType<V,E> implements TargetType<V,E,Path<NodeItem,Link>>{

		private final String name;
		private final Session<V, E> s;

		PathTargetType(Session<V,E> s, String name){
			this.name = name;
			this.s = s;
		}

		@Override 
		public Session<V,E> session(){
			return s;
		}


		public String name() {
			return name;
		}

		@Override
		public Paradigm paradigm() {
			return Paradigm.PATH;
		}


		@Override
		public DatabaseAccessor<V,E,Path<NodeItem,Link>> accessor() {

			return new DatabaseAccessor<V,E,Path<NodeItem,Link>>(){
				@Override
				public Iterator<Path<NodeItem,Link>> iterator(DatabaseAccess<V,E> access) {
					throw new RuntimeException("Unsupported Operation : Paths without a Traversal Definition");
				}						
			};
		}



		@Override
		public Optional<Path<NodeItem, Link>> getOne(DatabaseAccess<V,E> access, int dbId) {
			throw new RuntimeException("Unsupported Operation : getOne for a Path type of query");
		}


		@Override
		public DatabaseAccessor<V,E,Path<NodeItem,Link>> accessor(TraversalDefinition td) {

			return new DatabaseAccessor<V,E,Path<NodeItem,Link>>(){
				@Override
				public Iterator<Path<NodeItem,Link>> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();								
					return new MorphedIterator<>(access.traverser(td).paths(),(p)->new AdaptedPath(p,f));
				}				
			};
		}


		@Override
		public DatabaseAccessor<V,E,Path<NodeItem,Link>> accessor(final SuppliedTraversalOperation ops) {
			return new DatabaseAccessor<V,E,Path<NodeItem,Link>>(){
				@Override
				public Iterator<Path<NodeItem,Link>> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();					
					final OperationSuplier<Set<Path<NodeItem,Link>>,TraversalDefinition> supplier = (td)-> asSet(new MorphedIterator<>(access.traverser(td).paths(),(p)->new AdaptedPath(p,f)));					
					return ops.operate(supplier).iterator();
				}				
			};
		}


		@Override
		public Set<Path<NodeItem,Link>> targets(Traversal<V,E> tr, final FjAdaptersFactory<V,E> f) {
			throw new RuntimeException("Unsupported Operation : Paths from a Traversal");
		}

		@Override
		public Path<NodeItem,Link> first(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return new AdaptedPath(path,f);
		}

		@Override
		public Path<NodeItem,Link> last(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return new AdaptedPath(path,f);
		}

		@Override
		public Optional<List<DataPointer>> metaTypes(){
			//We cannot know beforehand which object are going to be encountered
			//This is anyway  only used in regenerate or read queries
			return Optional.empty();
		};


		private class AdaptedPath implements Path<NodeItem,Link>{

			private final Path<V, E> p;
			private final FjAdaptersFactory<V,E> f;

			public AdaptedPath(Path<V,E> p, FjAdaptersFactory<V,E> f) {
				this.p = p;
				this.f = f;
			}


			@Override
			public NodeItem first() {
				return f.getNodeReadOnly(p.first());
			}

			@Override
			public NodeItem last() {
				return f.getNodeReadOnly(p.last());
			}

			@Override
			public Link firstEdge() {
				return f.getLinkReadOnly(p.firstEdge());
			}

			@Override
			public Link lastEdge() {
				return f.getLinkReadOnly(p.lastEdge());
			}

			@Override
			public Iterator<NodeItem> nodes() {
				return new MorphedIterator<>(p.nodes(),(v)->f.getNodeReadOnly(v));
			}

			@Override
			public Iterator<Link> edges() {
				return new MorphedIterator<>(p.edges(),(e)->f.getLinkReadOnly(e));
			}

			@Override
			public int nodesCount() {
				return p.nodesCount();
			}

			@Override
			public int edgesCount() {
				return p.edgesCount();
			}


			public String toString(){	
				Iterator<NodeItem> nodes = nodes();
				Iterator<Link> edges = edges();

				String s = nodes.next().toString();
				while(edges.hasNext()){
					Link l = edges.next();
					NodeItem t = nodes.next();
					if(l.target().equals(t))
						s+=" --["+l.declaredType()+" "+l.getAttribute(WritableDataItem.idKey).get()+"]--> ";
					else
						s+=" <--["+l.declaredType()+" "+l.getAttribute(WritableDataItem.idKey).get()+"]-- ";
					s+=t.toString();
				}

				return s;//p.toString();
			}




		}





		@Override
		public void checkTraversalDefinition(TraversalDefinition definition) {
			//Nothing to do
		}






	}














	static abstract class AbstractNodesTargetType<V,E,T> implements TargetType<V,E,T>{

		private final String label;
		private final Paradigm par;
		private final Session<V, E> s;

		AbstractNodesTargetType(Session<V,E> s, String label){
			this.label = label;
			this.s = s;			
			// Check which paradigm
			par = s.dataRegistry().classOf(label) == null ? Paradigm.LABEL : Paradigm.CLASS;
		}

		
		@Override 
		public Session<V,E> session(){
			return s;
		}


		public String label() {
			return label;
		}

		@Override
		public Paradigm paradigm() {
			return par;
		}

		protected abstract Function<DatabaseAccess<V,?>,Iterator<V>> fetchFunction();

		protected abstract Function<V,T> nodeTransform(FjAdaptersFactory<V,?> f);

		@Override
		public Optional<T> getOne(DatabaseAccess<V,E> access, int dbId) {
			final Optional<V> opt = access.getOne(label, DataItem.idKey, dbId);
			if(opt!=null)
				return Optional.of(nodeTransform(access.createFactory()).apply(opt.get()));
			else
				return Optional.empty();
		}


		@Override
		public DatabaseAccessor<V,E,T> accessor() {

			return new DatabaseAccessor<V,E,T>(){
				@Override
				public Iterator<T> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();
					return new MorphedIterator<V,T>(fetchFunction().apply(access), nodeTransform(f));
				}				
			};
		}

		@Override
		public DatabaseAccessor<V,E,T> accessor(TraversalDefinition td) {

			return new DatabaseAccessor<V,E,T>(){
				@Override
				public Iterator<T> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();								
					return new MorphedIterator<V,T>(access.traverser(td).traverse().nodes().iterator(), nodeTransform(f));
				}				
			};
		}


		@Override
		public DatabaseAccessor<V,E,T> accessor(final SuppliedTraversalOperation ops) {
			return new DatabaseAccessor<V,E,T>(){
				@Override
				public Iterator<T> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();					
					final OperationSuplier<Set<V>,TraversalDefinition> supplier = (td)-> asSet(access.traverser(td).traverse().nodes());					
					return new MorphedIterator<V,T>(ops.operate(supplier).iterator(), nodeTransform(f));
				}				
			};
		}


		@Override
		public Set<T> targets(Traversal<V, E> tr, final FjAdaptersFactory<V,E> f) {
			return asSet(new MorphedIterator<>(tr.nodes().iterator(),nodeTransform(f)));
		}

		@Override
		public T first(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return nodeTransform(f).apply(path.first());
		}

		@Override
		public T last(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return nodeTransform(f).apply(path.last());
		}

		@Override
		public void checkTraversalDefinition(TraversalDefinition definition) {
			if( definition.traversalPolicy() != TraversalPolicy.NONE )
				definition.setTraversalPolicy(TraversalPolicy.NODES_ONCE);
		}

		@Override
		public Optional<List<DataPointer>> metaTypes(){
			return Targets.findNodeMetaTypes(label(), s);
		};


		@Override
		public String toString() {
			return "Node "+label;
		}

	}










	static abstract class AbstractLinksTargetType<V,E,L extends Link> implements TargetType<V,E,L>{

		private final String linkType;
		protected final Session<V, E> s;

		AbstractLinksTargetType(Session<V,E> s, String linkType){
			this.s = s;
			this.linkType = linkType;
		}

		@Override 
		public Session<V,E> session(){
			return s;
		}


		public String linkType() {
			return linkType;
		}

		@Override
		public Paradigm paradigm() {
			return Paradigm.LINK;
		}


		protected abstract Function<DatabaseAccess<?,E>,Iterator<E>> fetchFunction();

		protected abstract Function<E,L> linkTransform(FjAdaptersFactory<V,E> f);

		@Override
		public Optional<L> getOne(DatabaseAccess<V,E> access, int dbId) {
			final Optional<E> opt = access.getOneLink(linkType(), DataItem.idKey, dbId);
			if(opt!=null)
				return Optional.of(linkTransform(access.createFactory()).apply(opt.get()));
			else
				return Optional.empty();
		}


		@Override
		public DatabaseAccessor<V,E,L> accessor() {

			return new DatabaseAccessor<V,E,L>(){
				@Override
				public Iterator<L> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();
					return new MorphedIterator<E,L>(fetchFunction().apply(access), linkTransform(f));
				}				
			};

		}

		@Override
		public DatabaseAccessor<V,E,L> accessor(TraversalDefinition td) {

			return new DatabaseAccessor<V,E,L>(){
				@Override
				public Iterator<L> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();								
					return //We need to filter encountered links to match the target type
							new FilteredIterator<>(
									new MorphedIterator<E,L>(							
											access.traverser(td).traverse().edges().iterator(), linkTransform(f)), l -> l.declaredType().equals(linkType));
				}				
			};
		}

		@Override
		public DatabaseAccessor<V,E,L> accessor(final SuppliedTraversalOperation ops) {
			return new DatabaseAccessor<V,E,L>(){
				@Override
				public Iterator<L> iterator(DatabaseAccess<V,E> access) {
					final FjAdaptersFactory<V,E> f = access.createFactory();					
					final OperationSuplier<Set<L>,TraversalDefinition> supplier = 
							(td)-> asSet(
									new FilteredIterator<>(
											new MorphedIterator<E,L>(							
													access.traverser(td).traverse().edges().iterator(), linkTransform(f)), l -> l.declaredType().equals(linkType)));					
							return ops.operate(supplier).iterator();
				}				
			};
		}

		@Override
		public Set<L> targets(Traversal<V, E> tr, final FjAdaptersFactory<V,E> f) {
			return asSet(new MorphedIterator<>(tr.edges().iterator(),linkTransform(f)));
		}

		@Override
		public L first(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			E e = path.firstEdge();
			if(null == e)
				return null;
			return linkTransform(f).apply(e);
		}

		@Override
		public L last(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			E e = path.lastEdge();
			if(null == e)
				return null;
			return linkTransform(f).apply(e);
		}

		@Override
		public void checkTraversalDefinition(TraversalDefinition definition) {
			if( definition.traversalPolicy() != TraversalPolicy.NONE )
				definition.setTraversalPolicy(TraversalPolicy.LINKS_ONCE);
		}


		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public Optional<List<DataPointer>> metaTypes(){
			return Optional.ofNullable((List)s.metaModel().getMetaLinks(linkType()));
		};


		@Override
		public String toString() {
			return "Link "+linkType;
		}

	}




	/**
	 * Elements in the database belonging to a specific {@link WritableDataItem} class
	 */
	@Deprecated
	static class NodeClass<V,E> extends AbstractNodesTargetType<V,E,DatabaseNode>{


		private final Class<? extends NodeItem> clazz;
		private final long limit;

		NodeClass(Class<? extends NodeItem> clazz, Session<V,E> s, long limit){
			super(s, clazz.getTypeName());
			this.clazz = clazz;
			this.limit = limit;
		}

		@Override
		protected Function<DatabaseAccess<V, ?>, Iterator<V>> fetchFunction() {
			if(limit>0){
				return (access)->limit(access.getNodes(clazz),limit);
			}
			else
				return (access)->access.getNodes(clazz);
		}

		@Override
		protected Function<V, DatabaseNode> nodeTransform(FjAdaptersFactory<V,?> f) {
			return (v)->f.getNodeReadOnly(v);
		}


	}	



	/**
	 * Elements in the database belonging to a specific {@link WritableDataItem} class
	 */
	@Deprecated
	static class UpdatableNodeClass<V,E> extends AbstractNodesTargetType<V,E,Updatable>{


		private final Class<? extends NodeItem> clazz;
		private final UpdateInfo ui;

		UpdatableNodeClass(Class<? extends NodeItem> clazz, Session<V,E> s, UpdateInfo ui){
			super(s, clazz.getTypeName());
			this.clazz = clazz;
			this.ui = ui;
		}

		@Override
		protected Function<DatabaseAccess<V, ?>, Iterator<V>> fetchFunction() {
			return (access)->access.getNodes(clazz);
		}

		@Override
		protected Function<V, Updatable> nodeTransform(FjAdaptersFactory<V,?> f) {
			return (v)->f.getUpdatableNode(v, ui);
		}

	}	





	@Deprecated
	static class DeletableNodeClass<V,E> extends AbstractNodesTargetType<V,E,DatabaseNodeDeletable>{


		private final Class<? extends NodeItem> clazz;

		DeletableNodeClass(Class<? extends NodeItem> clazz, Session<V,E> s){
			super(s, clazz.getTypeName());
			this.clazz = clazz;
		}

		@Override
		protected Function<DatabaseAccess<V, ?>, Iterator<V>> fetchFunction() {
			return (access)->access.getNodes(clazz);
		}

		@Override
		protected Function<V, DatabaseNodeDeletable> nodeTransform(FjAdaptersFactory<V,?> f) {
			return (v)->f.getNodeDeletable(v);
		}


	}	



	/**
	 * Elements grouped in the database with a specified label
	 */
	public static class Labels<V,E> extends AbstractNodesTargetType<V,E,DatabaseNode>{

		private final long limit;

		Labels(String label, Session<V,E> s, long limit){
			super(s, label);
			this.limit = limit;
		}

		@Override
		protected Function<DatabaseAccess<V, ?>, Iterator<V>> fetchFunction() {
			if(limit>0){
				return (access)-> limit(access.getNodes(label()),limit);
			}
			else
				return (access)->access.getNodes(label());
		}

		@Override
		protected Function<V, DatabaseNode> nodeTransform(FjAdaptersFactory<V, ?> f) {
			return (v)->f.getNodeReadOnly(v);
		}


	}	





	public static class DeletableLabels<V,E> extends AbstractNodesTargetType<V,E,DatabaseNodeDeletable>{


		DeletableLabels(String label, Session<V,E> s){
			super(s, label);
		}

		@Override
		protected Function<DatabaseAccess<V, ?>, Iterator<V>> fetchFunction() {
			return (access)->access.getNodes(label());
		}

		@Override
		protected Function<V, DatabaseNodeDeletable> nodeTransform(FjAdaptersFactory<V, ?> f) {
			return (v)->f.getNodeDeletable(v);
		}

	}


	public static class UpdatableLabels<V,E> extends AbstractNodesTargetType<V,E,UpdatableNode>{


		private UpdateInfo ui;

		UpdatableLabels(String label, Session<V,E> s, UpdateInfo ui){
			super(s, label);
			this.ui = ui;
		}

		@Override
		protected Function<DatabaseAccess<V, ?>, Iterator<V>> fetchFunction() {
			return (access)->access.getNodes(label());
		}

		@Override
		protected Function<V, UpdatableNode> nodeTransform(FjAdaptersFactory<V, ?> f) {
			return (v)->f.getUpdatableNode(v,ui);
		}

	}












	public static class Links<V,E> extends AbstractLinksTargetType<V,E,Link>{

		private final long limit;

		Links(String label, Session<V,E> s, long limit){
			super(s, label);
			this.limit = limit;
		}

		@Override
		protected Function<DatabaseAccess<?, E>, Iterator<E>> fetchFunction() {
			if(limit>0){
				return (access)->limit(access.getLinks(linkType()),limit);
			}
			else
				return (access)->access.getLinks(linkType());
		}

		@Override
		protected Function<E, Link> linkTransform(FjAdaptersFactory<V,E> f) {
			return (v)->f.getLinkReadOnly(v);
		}


	}



	public static class LinksDeletable<V,E> extends AbstractLinksTargetType<V,E,Link>{

		LinksDeletable(String label, Session<V,E> s){
			super(s, label);
		}

		@Override
		protected Function<DatabaseAccess<?, E>, Iterator<E>> fetchFunction() {
			return (access)->access.getLinks(linkType());
		}

		@Override
		protected Function<E, Link> linkTransform(FjAdaptersFactory<V,E> f) {
			return (v)->f.getLinkDeletable(v);
		}


	}



	public static class UpdatableLinks<V,E> extends AbstractLinksTargetType<V,E,UpdatableLink>{

		private UpdateInfo ui;

		UpdatableLinks(String label, Session<V,E> s, UpdateInfo ui){
			super(s, label);
			this.ui = ui;
		}

		@Override
		protected Function<DatabaseAccess<?, E>, Iterator<E>> fetchFunction() {
			return (access)->access.getLinks(linkType());
		}

		@Override
		protected Function<E, UpdatableLink> linkTransform(FjAdaptersFactory<V,E> f) {
			return (v)->f.getUpdatableLink(v, ui);
		}

	}








	public static class STLinks<V,E> extends AbstractLinksTargetType<V,E,Link>{

		private final long limit;
		final String srcType, tgType;

		STLinks(String label, String srcType, String tgType, Session<V,E> s, long limit){
			super(s, label);
			this.srcType = srcType;
			this.tgType = tgType;
			this.limit = limit;
		}

		@Override
		protected Function<DatabaseAccess<?, E>, Iterator<E>> fetchFunction() {
			if(limit>0){
				return (access)->limit(access.getLinks(linkType(), srcType, tgType),limit);
			}
			else
				return (access)->access.getLinks(linkType(), srcType, tgType);
		}

		@Override
		protected Function<E, Link> linkTransform(FjAdaptersFactory<V,E> f) {
			return (v)->f.getLinkReadOnly(v);
		}
		
		@Override
		public Optional<List<DataPointer>> metaTypes(){
			return Optional.ofNullable(Collections.singletonList(s.metaModel().getMetaLinks(linkType(), srcType, tgType)));
		};

	}



	public static class STLinksDeletable<V,E> extends AbstractLinksTargetType<V,E,Link>{

		final String srcType, tgType;

		STLinksDeletable(String label, String srcType, String tgType, Session<V,E> s){
			super(s, label);
			this.srcType = srcType;
			this.tgType = tgType;
		}

		@Override
		protected Function<DatabaseAccess<?, E>, Iterator<E>> fetchFunction() {
			return (access)->access.getLinks(linkType(), srcType, tgType);
		}

		@Override
		protected Function<E, Link> linkTransform(FjAdaptersFactory<V,E> f) {
			return (v)->f.getLinkDeletable(v);
		}
		
		@Override
		public Optional<List<DataPointer>> metaTypes(){
			return Optional.ofNullable(Collections.singletonList(s.metaModel().getMetaLinks(linkType(), srcType, tgType)));
		};
		
	}



	public static class STUpdatableLinks<V,E> extends AbstractLinksTargetType<V,E,UpdatableLink>{

		private final UpdateInfo ui;
		final String srcType, tgType;

		STUpdatableLinks(String label, String srcType, String tgType, Session<V,E> s, UpdateInfo ui){
			super(s, label);
			this.ui = ui;
			this.srcType = srcType;
			this.tgType = tgType;
		}

		@Override
		protected Function<DatabaseAccess<?, E>, Iterator<E>> fetchFunction() {
			return (access)->access.getLinks(linkType(), srcType, tgType);
		}

		@Override
		protected Function<E, UpdatableLink> linkTransform(FjAdaptersFactory<V,E> f) {
			return (v)->f.getUpdatableLink(v, ui);
		}
		
		@Override
		public Optional<List<DataPointer>> metaTypes(){
			return Optional.ofNullable(Collections.singletonList(s.metaModel().getMetaLinks(linkType(), srcType, tgType)));
		};

	}









	@Deprecated
	public static class ImplTypedClass<V,E> implements TargetType<V,E,V>{

		private final Class<? extends NodeItem> clazz;
		private final Session<V,E> s;

		ImplTypedClass(Class<? extends NodeItem> clazz, Session<V,E> s){
			Objects.requireNonNull(clazz,"The provided class cannot be null");
			this.clazz = clazz;
			this.s = s;
		}

		@Override
		public Session<V, E> session() {
			return s;
		}


		public String name() {
			return clazz.getSimpleName();
		}

		@Override
		public Paradigm paradigm() {
			return Paradigm.CLASS;
		}


		@Override
		public Optional<V> getOne(DatabaseAccess<V,E> access, int dbId) {
			return access.getOne(name(), DataItem.idKey, dbId);
		}


		@Override
		public DatabaseAccessor<V,E,V> accessor() {
			return new DatabaseAccessor<V,E,V>(){
				@Override
				public Iterator<V> iterator(DatabaseAccess<V,E> access) {
					return access.getNodes(clazz);
				}
			};
		}

		@Override
		public DatabaseAccessor<V, E, V> accessor(TraversalDefinition td) {
			return new DatabaseAccessor<V,E,V>(){
				@Override
				public Iterator<V> iterator(DatabaseAccess<V,E> access) {					
					return access.traverser(td).traverse().nodes().iterator();
				}				
			};
		}

		@Override
		public DatabaseAccessor<V,E,V> accessor(final SuppliedTraversalOperation ops) {
			return new DatabaseAccessor<V,E,V>(){
				@Override
				public Iterator<V> iterator(DatabaseAccess<V,E> access) {					
					final OperationSuplier<Set<V>,TraversalDefinition> supplier = (td)-> asSet(access.traverser(td).traverse().nodes());					
					return ops.operate(supplier).iterator();
				}				
			};
		}


		@Override
		public Set<V> targets(Traversal<V, E> tr, final FjAdaptersFactory<V,E> f) {
			return asSet(tr.nodes());
		}

		@Override
		public V first(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return path.first();
		}

		@Override
		public V last(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return path.last();
		}

		@Override
		public void checkTraversalDefinition(TraversalDefinition definition) {
			if( definition.traversalPolicy() != TraversalPolicy.NONE )
				definition.setTraversalPolicy(TraversalPolicy.NODES_ONCE);
		}

		@Override
		public Optional<List<DataPointer>> metaTypes(){
			return Targets.findNodeMetaTypes(name(), s);
		};


	}


	public static class ImplTypedLabels<V,E> implements TargetType<V,E,V>{

		private final Session<V,E> s;
		private String label;

		ImplTypedLabels(String label, Session<V,E> s){
			this.label = label;
			this.s = s;
		}

		@Override
		public Session<V, E> session() {
			return s;
		}


		public String name() {
			return label;
		}

		@Override
		public Paradigm paradigm() {
			return Paradigm.CLASS;
		}


		@Override
		public Optional<V> getOne(DatabaseAccess<V,E> access, int dbId) {
			return access.getOne(name(), DataItem.idKey, dbId);
		}


		@Override
		public DatabaseAccessor<V,E,V> accessor() {
			return new DatabaseAccessor<V,E,V>(){
				@Override
				public Iterator<V> iterator(DatabaseAccess<V,E> access) {
					return access.getNodes(label);
				}
			};

		}

		@Override
		public DatabaseAccessor<V, E, V> accessor(TraversalDefinition td) {
			return new DatabaseAccessor<V,E,V>(){
				@Override
				public Iterator<V> iterator(DatabaseAccess<V,E> access) {					
					return access.traverser(td).traverse().nodes().iterator();
				}				
			};
		}

		@Override
		public DatabaseAccessor<V,E,V> accessor(final SuppliedTraversalOperation ops) {
			return new DatabaseAccessor<V,E,V>(){
				@Override
				public Iterator<V> iterator(DatabaseAccess<V,E> access) {					
					final OperationSuplier<Set<V>,TraversalDefinition> supplier = (td)-> asSet(access.traverser(td).traverse().nodes());					
					return ops.operate(supplier).iterator();
				}				
			};
		}


		@Override
		public Set<V> targets(Traversal<V, E> tr, final FjAdaptersFactory<V,E> f) {
			return asSet(tr.nodes());
		}

		@Override
		public V first(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return path.first();
		}

		@Override
		public V last(Path<V, E> path, final FjAdaptersFactory<V,E> f) {
			return path.last();
		}

		@Override
		public void checkTraversalDefinition(TraversalDefinition definition) {
			if( definition.traversalPolicy() != TraversalPolicy.NONE )
				definition.setTraversalPolicy(TraversalPolicy.NODES_ONCE);
		}


		@Override
		public Optional<List<DataPointer>> metaTypes(){
			return Targets.findNodeMetaTypes(name(), s);
		};

	}



	/*

	public static class ImplTypedLinks<V,E> implements TargetType<V,E,E>{

		private final Session<V,E> s;
		private String label;

		ImplTypedLinks(String label, Session<V,E> s){
			this.label = label;
			this.s = s;
		}

		@Override
		public Session<V,E> session() {
			return s;
		}

		@Override
		public String name() {
			return label;
		}

		@Override
		public Paradigm paradigm() {
			return Paradigm.CLASS;
		}



		@Override
		public DatabaseAccessor<V,E,E> accessor() {
			return new DatabaseAccessor<V,E,E>(){
				@Override
				public Iterator<E> iterator(DatabaseAccess<V,E> access) {
					return access.getLinks(label);
				}
			};

		}

		@Override
		public DatabaseAccessor<V, E, E> accessor(TraversalDefinition td) {
			return new DatabaseAccessor<V,E,E>(){
				@Override
				public Iterator<E> iterator(DatabaseAccess<V,E> access) {					
					return access.traverser(td).traverse().edges().iterator();
				}				
			};
		}

		@Override
		public DatabaseAccessor<V,E,E> accessor(final SuppliedTraversalOperation ops) {
			return new DatabaseAccessor<V,E,E>(){
				@Override
				public Iterator<E> iterator(DatabaseAccess<V,E> access) {					
					final OperationSuplier<Set<E>,TraversalDefinition> supplier = (td)-> asSet(access.traverser(td).traverse().edges());					
					return ops.operate(supplier).iterator();
				}				
			};
		}

	}

	 */









	static <T> Set<T> asSet(Iterable<T> it){
		if(Set.class.isAssignableFrom(it.getClass()))
			return (Set<T>) it;
		return asSet(it.iterator());
	}


	static <T> Set<T> asSet(Iterator<T> itr){
		Set<T> set = new HashSet<>();
		while(itr.hasNext())
			set.add(itr.next());
		return set;

	}


	static <T> Iterator<T> limit(Iterator<T> it, long limit){
		return new Iterator<T>(){

			private long counter = 0;

			@Override
			public boolean hasNext() {
				if(it.hasNext())
					return counter<limit;					
				return false;
			}

			@Override
			public T next() {
				counter++;
				return it.next();
			}

		};
	}






	/* TODO add Paths
	public static class Paths implements TargetType<Link>{}	
	 */




}

package org.pickcellslab.foundationj.ui;

public class DoubleVariable extends AbstractVariable<Double> {

	private final double increment, minValue, maxValue;
	
	
	public DoubleVariable(String name, Double defaultValue, String description, double increment, double minValue, double maxValue) {
		super(name, defaultValue, description);
		
		if(minValue>maxValue)
			throw new IllegalArgumentException("Min value > maxValue"); 
		
		
		this.increment = increment;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	
	public double increment() {
		return increment;
	};
	
	public double minValue() {
		return minValue;
	}
	
	public double maxValue() {
		return maxValue;
	}
	
	
	@Override
	protected void check(Double newValue) throws IllegalArgumentException {
		if(newValue<minValue || newValue>maxValue)
				throw new IllegalArgumentException(newValue+ " is out of range"); 
	}
	
	
	
	

}

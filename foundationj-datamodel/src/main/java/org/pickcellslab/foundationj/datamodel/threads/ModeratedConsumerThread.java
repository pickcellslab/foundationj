package org.pickcellslab.foundationj.datamodel.threads;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.concurrent.RejectedExecutionException;
import java.util.function.BiFunction;
import java.util.function.Consumer;


public class ModeratedConsumerThread<T> extends Thread {


	private final Consumer<T> runnable;
	private T nextInput;
	private boolean isBusy = false;
	private BiFunction<T,T,T> combiner;

	public ModeratedConsumerThread(Consumer<T> runnable){
		assert runnable!=null : "runnable is null";
		this.runnable = runnable;
		this.combiner = (i,o)->o;
	}



	@Override
	public void run()
	{
		while ( !isInterrupted() ){
			final T input;
			synchronized ( this ){
				isBusy = true;
				input = nextInput;
				nextInput = null;
			}
			if ( input!=null )
				try{
					runnable.accept(input);
				}
			catch ( final RejectedExecutionException e ){
				e.printStackTrace();
			}
			synchronized ( this ){
				try{
					if ( nextInput==null ) {
						isBusy = false;
						wait();
					}
				}
				catch ( final InterruptedException e ){
					break;
				}
			}
		}
	}


	public void setCombiner(BiFunction<T,T,T> combiner){
		if(combiner==null)
			return;
		this.combiner = combiner;
	};


	public void requestRerun(T input){
		synchronized ( this ){
			if(nextInput==null)
				nextInput = input;
			else
				nextInput = combiner.apply(nextInput,input);
			notify();
		}
		
	}
	
	
	
	/**
	 * @return true if this thread is currently working on its task, false if it is waiting.
	 */
	public boolean isBusy() {
		return isBusy;
	}
	
	

}
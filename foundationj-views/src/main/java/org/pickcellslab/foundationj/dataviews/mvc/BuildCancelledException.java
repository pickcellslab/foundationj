package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * This Exception indicates that the building process has been interrupted
 * somewhere in the building chain. The source of the exception is available in this exception
 * 
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("serial")
public class BuildCancelledException extends Exception {

	private final Object source;

	public BuildCancelledException(Object source, String message){
		super(message);
		this.source = source;
	}
	
	public Object source(){
		return source;
	}
	
}

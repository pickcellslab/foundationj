package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;
import org.pickcellslab.foundationj.dbm.impl.MonolithicSearchDefinition.Builder;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.SchemaQueryBuilder;

public class SchemaQueryBuilderImpl implements SchemaQueryBuilder{

	String description;
	String tag;
	private final TargetType<?, ?, DatabaseNodeDeletable> target;
	private MonolithicSearchDefinition.Builder<?, ?, DatabaseNodeDeletable> delegate;


	SchemaQueryBuilderImpl(TargetType<?,?,DatabaseNodeDeletable> target){
		this.target = target;
		delegate = new Builder<>(target);
	};




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.SchemaQueryBuilder#addLabel(java.lang.String)
	 */
	@Override
	public SchemaQueryBuilder addLabel(String label){
		Objects.requireNonNull(label,"label is Null");
		this.tag = label;
		return this;
	}
	
	
	


	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.SchemaQueryBuilder#getAll()
	 */
	@Override
	public QueryRunner<Void> getAll() {
		return this;
	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.SchemaQueryBuilder#useFilter(org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate)
	 */
	@Override
	public QueryRunner<Void> useFilter(ExplicitPredicate<? super DatabaseNodeDeletable> test) {
		delegate.setFilter(test);
		return this;
	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.SchemaQueryBuilder#useTraversal(org.pickcellslab.foundationj.dbm.queries.builders.TraversalDefinition)
	 */
	@Override
	public QueryRunner<Void> useTraversal(TraversalDefinition definition) {
		assert definition != null;
		target.checkTraversalDefinition(definition);
		delegate.setTraversal(definition);
		return this;
	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.SchemaQueryBuilder#useTraversals(org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation)
	 */
	@Override
	public QueryRunner<Void> useTraversals(SuppliedTraversalOperation operations) {
		delegate.setTraversalOperation(operations);
		return this;
	}




	/* (non-Javadoc)
	 * @see org.pickcellslab.foundationj.dbm.impl.SchemaQueryBuilder#run()
	 */
	@Override
	public Void run() throws DataAccessException {
		target.session().runQuery(new MonolithicSearchQuery<>(delegate.build(), new LabelAction(target,tag),null	));
		return null;
	}
	

	/*TODO ?
	public SchemaQueryBuilder index(AKey<?> key){
		this.indexKeys.add(key);
		return this;
	}

	public SchemaQueryBuilder ensureUnique(AKey<?> key){
		this.uniqueKeys.add(key);
		return this;
	}
	
	*/

	

}
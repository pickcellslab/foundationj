package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.MarkedState;
import org.pickcellslab.foundationj.dbm.queries.OrganisedResult;
import org.pickcellslab.foundationj.dbm.queries.SplitTraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class SplitTraversalQuery<V,E,T,K,O> extends SearchSeveralQueryImpl<V,E,T,K, O, OrganisedResult<K,O>> {

	private final SplitTraversalDefinition<V, E, T, K> definition;

	private final Logger log = LoggerFactory.getLogger(SplitTraversalQuery.class);


	protected SplitTraversalQuery(SplitTraversalDefinition<V,E,T,K> definition, Action<? super T, O> action) {
		super(action);
		this.definition = definition;
	}





	@Override
	public String description() {
		return searchDescription()+" -> "+actionDescription();
	}

	@Override
	public String name() {
		//TODO 
		 throw new RuntimeException("Not implemented yet");
	}

	@Override
	public String shortDescription() {
		//TODO 
		 throw new RuntimeException("Not implemented yet");
	}
	
	

	@Override
	public String searchDescription() {
		return definition.descrition();
	}

	@Override
	public String actionDescription() {
		return action.description();
	}

	@Override
	public OrganisedResult<K,O> createResult(DatabaseAccess<V, E> db) throws DataAccessException {
		
		Objects.requireNonNull(db, "The provided DatabaseAccess is null");

		
		Map<K, Iterable<T>> split = definition.split(db);
		

		ExecutorService exe = Executors.newCachedThreadPool();

		final AtomicBoolean success = new AtomicBoolean(true);		
		for(Entry<K, Iterable<T>> e : split.entrySet()){
			exe.execute(()->{

				try(MarkedState state = db.newThread()){

					//Initialise the ActionMechanism
					final ActionMechanism<? super T,O> m = action.createCoordinate(e.getKey());
					final Iterator<T> itr = e.getValue().iterator();

					while(itr.hasNext())
						m.performAction(itr.next());

					m.lastAction();

					state.success();
					
				}catch (Exception e1){
					success.set(false);
					log.error("An error occured while searching the database",e1);
				}

			});
		}

		try {
			
			if(!success.get()){
				db.fail();
				throw new DataAccessException("An error occured while searching the database, check the log");
			}
			exe.shutdown();
			if(!exe.awaitTermination(20, TimeUnit.MINUTES)){
				db.fail();
				throw new DataAccessException("The query took too long to return (more than 20 min)");
			}
		} catch (InterruptedException e1) {
			db.fail();
			throw new DataAccessException("The query has been interrupted!");
		}


		Map<K,O> content = new HashMap<>(split.size());
		for(K k : split.keySet())
			content.put(k, action.createOutput(k));


		return new OrganisedResult<>(content);
	}

}

package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Test;
import org.pickcellslab.foundationj.optim.staging.Parcel;

public class ParcelTest {

	@Test
	public void testParcel(){
		
		Parcel p1 = new Parcel("Nickel", 50, 600, 600, 0);
		Parcel p2 = new Parcel("Nickel", 50, 500, 0, 25000);
		
		System.out.println("P1 budget -> "+p1.budget());
		System.out.println("P1 quantity -> "+p1.currentQuantity());
		
		System.out.println("P2 budget -> "+p2.budget());
		System.out.println("P2 quantity -> "+p2.currentQuantity());
		
		
		p1.transferTo(p2);
		
		System.out.println("P1 budget -> "+p1.budget());
		System.out.println("P1 quantity -> "+p1.currentQuantity());
		
		System.out.println("P2 budget -> "+p2.budget());
		System.out.println("P2 quantity -> "+p2.currentQuantity());
	}
	
	
}

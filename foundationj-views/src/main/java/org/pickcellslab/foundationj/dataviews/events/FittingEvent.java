package org.pickcellslab.foundationj.dataviews.events;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.mvc.FittableDimensionBroker;


public class FittingEvent {

	private final Object source;
	private final Modif type;
	private final int fittingId;
	private final String series;

	public FittingEvent(Object source, Modif type, String stringId, int id) {
		
		Objects.requireNonNull(source);
		Objects.requireNonNull(type);
		
		this.source = source;	
		this.type = type;
		this.series = stringId;
		this.fittingId = id;
	}

	public Object getSource() {
		return source;
	}

	public Modif getType() {
		return type;
	}

	/**
	 * @return The id of the fit the event was fired for. This id can be used to obtain the {@link FitResult}
	 * from {@link FittableDimensionBroker#getDistributionFit(String)}
	 */
	public String stringId(){
		return series;
	};

	public int getFittingId() {
		return fittingId;
	}


}
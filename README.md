# README #

foundationj is a framework for building a component-structured application around a property graph data model.

To this end, foundationj provides runtime support and a programming model for modularity. 

More information can be found on our [website](https://pickcellslab.frama.io/docs/developers/architecture/foundationj/dependency_injection/)

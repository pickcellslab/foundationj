package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="StreamReduction")
public class StreamReduction<I,M,R> implements ExplicitFunction<I, R> {

	@Supported
	private final ExplicitStream<I,M> stream;
	@Supported
	private final ExplicitReduction<? super M, R> reduction;
	
	public StreamReduction(ExplicitStream<I,M> stream, ExplicitReduction<? super M, R> reduction) {
		MappingUtils.exceptionIfNullOrInvalid(stream);
		MappingUtils.exceptionIfNullOrInvalid(reduction);
		this.stream = stream;
		this.reduction = reduction;
	}
	
	@Override
	public R apply(I i) {
		final ExplicitReduction<? super M, R> r = reduction.factor();
		stream.apply(i).forEach((m)->r.include(m));
		return r.getResult();
	}

	@Override
	public String description() {
		return stream.description()+" -> "+reduction.description();
	}

	@Override
	public Class<R> getReturnType() {
		return reduction.getReturnType();
	}

}

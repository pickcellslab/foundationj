package org.pickcellslab.foundationj.datamodel.predicates;

import java.util.Objects;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="NamedPredicate")
public class NamedExplicitPredicate<T> implements ExplicitPredicate<T> {

	@Supported
	private final ExplicitPredicate<T> delegate;
	private final String nameWhenTrue;
	private final String nameWhenFalse;

	public NamedExplicitPredicate(ExplicitPredicate<T> delegate, String nameWhenTrue) {
		this(delegate, nameWhenTrue, "Not "+nameWhenTrue);
	}
	
	public NamedExplicitPredicate(ExplicitPredicate<T> delegate, String nameWhenTrue, String nameWhenFalse) {
		MappingUtils.exceptionIfNullOrInvalid(delegate);
		Objects.requireNonNull(nameWhenTrue);
		Objects.requireNonNull(nameWhenFalse);
		this.delegate = delegate;
		this.nameWhenTrue = nameWhenTrue;
		this.nameWhenFalse = nameWhenFalse;
	}
	
	@Override
	public boolean test(T input) {
		return delegate.test(input);
	}

	@Override
	public String description() {
		return delegate.description();
	}

	
	public String getName(boolean result) {
		return result ? nameWhenTrue : nameWhenFalse;
	}
	
	@Override
	public String toString() {
		return nameWhenTrue;
	}
}

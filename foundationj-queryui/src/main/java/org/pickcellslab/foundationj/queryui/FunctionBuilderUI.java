package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.functions.DataTransform;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.DimensionChoice;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

@SuppressWarnings("serial")
public class FunctionBuilderUI extends WizardNode {


	//The MetaQueryable defining the target of the DataFunction 
	private final MetaQueryable target;	
	private final MetaInstanceDirector director;


	//Fields related to the function to parse
	private final LinkedList<String> expression = new LinkedList<>();
	private final Map<String, ExplicitFunction<WritableDataItem,? extends Number>> functionMap = new HashMap<>();

	private Color number = Color.BLACK, parenthesis = new Color(77,166,25), funct = Color.ORANGE,
			key = Color.BLUE, constant = new Color(188,28,57), operator = new Color(93,100,90);

	//Flag keeping track of whether or not the current expression is valid
	private boolean textIsValid;

	//The final function to return
	private DataTransform function;


	//UI fields
	private JTextPane textPane;
	private StyledDocument doc;
	private Style style;



	public FunctionBuilderUI(WizardNode parent, MetaInstanceDirector director, MetaQueryable queryable, UITheme theme) {
		super(parent);

		assert queryable != null;

		this.target = queryable;
		this.director = director;
		buildUI(theme);			
	}





	@SuppressWarnings("unchecked")
	private void buildUI(UITheme theme) {

		//Define controllers
		//-------------------------------------------------------------------------------------------------------------------

		JPanel panel = new JPanel();

		JButton button_1 = new JButton("2");

		JButton button_2 = new JButton("3");

		JButton button_10 = new JButton("(");

		JButton button_11 = new JButton(")");


		JComboBox<MetaReadable> readBox = new JComboBox<>();
		readBox.setRenderer(new MetaRenderer(theme, director.registry()));



		JButton btnIns = new JButton("INS");

		btnIns.setToolTipText("Inserts the selected property as a variable");

		JButton button_12 = new JButton("\u2190");

		button_12.setToolTipText("Delete the last entry");
		button_12.setFont(UIManager.getFont("CheckBox.font"));

		JButton button_19 = new JButton(".");

		JButton button_16 = new JButton("+");

		JButton button_17 = new JButton("-");

		JButton btnLn = new JButton("ln");

		btnLn.setToolTipText("natural logarithm");

		JButton btnLog = new JButton("log");

		btnLog.setToolTipText("log base 10");

		JButton btnAbs = new JButton("abs");

		btnAbs.setToolTipText("absolute value");

		JButton btnFloor = new JButton("floor");

		JButton button_6 = new JButton("7");

		JButton button_7 = new JButton("8");

		JButton button_8 = new JButton("9");

		JButton button_14 = new JButton("/");

		JButton button_15 = new JButton("^");
		button_15.setToolTipText("exponent");

		JButton btnua = new JButton("\u03C0");

		JButton btnAcos = new JButton("acos");

		JButton btnAsin = new JButton("asin");

		JButton btnAtan = new JButton("atan");

		JButton btnCeil = new JButton("ceil");

		JButton button_3 = new JButton("4");

		JButton button_4 = new JButton("5");

		JButton button_5 = new JButton("6");

		JButton btnud = new JButton("\u00D7");

		JButton button_13 = new JButton("%");
		button_13.setToolTipText("modulo");

		JButton btnE = new JButton("\u212F");

		JButton btnCos = new JButton("cos");

		JButton btnSin = new JButton("sin");

		JButton btnTan = new JButton("tan");

		JButton btnRound = new JButton("round");


		JScrollPane scrollPane = new JScrollPane();

		JButton button = new JButton("1");


		JButton button_20 = new JButton("\u223C");
		button_20.setToolTipText("Random number [0-1]");


		JButton button_9 = new JButton("0");



		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		textPane.setEditable(false);
		Font font = new Font("Serif", Font.BOLD, 16);
		textPane.setFont(font);

		doc = textPane.getStyledDocument();
		style = textPane.addStyle("DataTransform Style", null);



		JComboBox<String> targetBox = new JComboBox<>();
		if(MetaClass.class.isAssignableFrom(target.getClass())){
			targetBox.addItem("dummy");	
			targetBox.addItem("Item");	
			targetBox.addItem("dummy");	
			targetBox.setSelectedItem("Item");
			targetBox.setEnabled(false);
		}else{
			targetBox.addItem("Source");	
			targetBox.addItem("Link");	
			targetBox.addItem("Target");	
			targetBox.setSelectedItem("Link");
		}

		Predicate<MetaReadable> numericKey = p->/*MetaKey.class.isAssignableFrom(p.getClass()) && */ p.dataType() == dType.NUMERIC;

		target.getReadables().filter(numericKey).forEach(r->readBox.addItem(r));





		//Listeners
		//------------------------------------------------------------------------------------------------------------------



		targetBox.addActionListener(l->{
			//Remove content of the readBox
			readBox.removeAllItems();
			if(targetBox.getSelectedIndex()==0)
				((MetaLink)target).source().getReadables().filter(numericKey).forEach(r->readBox.addItem(r));
			else if(targetBox.getSelectedIndex()==1)
				target.getReadables().filter(numericKey).forEach(r->readBox.addItem(r));
			else if(targetBox.getSelectedIndex()==2)
				((MetaLink)target).target().getReadables().filter(numericKey).forEach(r->readBox.addItem(r));
		});


		//Insertion in the expression
		btnIns.addActionListener(e->{
			MetaReadable mr = (MetaReadable) readBox.getSelectedItem();
			if(mr!=null){

				ExplicitFunction<?, ?> df = null;
				if(mr.numDimensions() == 1)
					df = mr.getDimension(0);
				else{
					List<Dimension> list = new ArrayList<>(1);
					for(int d = 0; d<mr.numDimensions(); d++)
						list.add(mr.getDimension(d));

					DimensionChoice dialog = new DimensionChoice(list, null, theme);
					if(JOptionPane.showConfirmDialog(null, dialog, "Choose One Dimension",JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
						return;

					//If a key was selected
					df = dialog.getSelected();
				}

				ExplicitFunction<WritableDataItem,? extends Number> f = null;

				//Check if it is the node/link itself or the source or the target
				if(targetBox.getSelectedIndex()==0){
					f = (ExplicitFunction) F.applyOnSource((ExplicitFunction)df);
				}
				else if(targetBox.getSelectedIndex()==1)
					f = (ExplicitFunction<WritableDataItem,? extends Number>) df;
				else if(targetBox.getSelectedIndex()==2)
					f = (ExplicitFunction<WritableDataItem,? extends Number>) F.applyOnTarget((ExplicitFunction)df);

				//Remove empty space
				final String noEmptySpace = f.toString().replaceAll("\\s+","");
				functionMap.put(noEmptySpace, f);
				insert(noEmptySpace, this.key);

			}
		});




		button.addActionListener(e->insert("1", number));

		button_9.addActionListener(e->insert("0", number));

		button_20.addActionListener(e->insert("random", constant));

		btnRound.addActionListener(e->insert("round", funct));

		btnTan.addActionListener(e->insert("tan", funct));

		btnSin.addActionListener(e->insert("sin", funct));

		btnCos.addActionListener(e->insert("cos", funct));
		btnE.addActionListener(e->insert("e", constant));
		button_13.addActionListener(e->insert("%", operator));
		btnud.addActionListener(e->insert("*", operator));
		button_5.addActionListener(e->insert("6", number));
		button_4.addActionListener(e->insert("5", number));
		button_3.addActionListener(e->insert("4", number));

		btnCeil.addActionListener(e->insert("ceil", funct));

		btnAtan.addActionListener(e->insert("atan", funct));

		btnAsin.addActionListener(e->insert("asin", funct));

		btnAcos.addActionListener(e->insert("acos", funct));
		btnua.addActionListener(e->insert("pi", constant));
		button_15.addActionListener(e->insert("^", operator));
		button_14.addActionListener(e->insert("/", operator));
		button_8.addActionListener(e->insert("9", number));
		button_7.addActionListener(e->insert("8", number));
		button_6.addActionListener(e->insert("7", number));

		btnFloor.addActionListener(e->insert("floor", funct));
		btnAbs.addActionListener(e->insert("abs", funct));
		btnLog.addActionListener(e->insert("log", funct));
		btnLn.addActionListener(e->insert("ln", funct));
		button_17.addActionListener(e->insert("-", operator));
		button_16.addActionListener(e->insert("+", operator));
		button_19.addActionListener(e->insert(".", number));

		button_11.addActionListener(e->insert(")", parenthesis));
		button_10.addActionListener(e->insert("(", parenthesis));
		button_2.addActionListener(e->insert("3", number));
		button_1.addActionListener(e->insert("2", number));


		//Delete Button
		button_12.addActionListener(e->delete(expression.pollLast()));








		//Layout
		//-------------------------------------------------------------------------------------------------------------------
		setBounds(100, 100, 525, 275);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));




		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 587, Short.MAX_VALUE)
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
												.addGroup(gl_panel.createSequentialGroup()
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
																.addComponent(button_9, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
																.addComponent(button_3)
																.addComponent(button))
														.addPreferredGap(ComponentPlacement.RELATED))
												.addGroup(gl_panel.createSequentialGroup()
														.addComponent(button_6, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
														.addGap(6)))
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
														.addGroup(gl_panel.createSequentialGroup()
																.addComponent(button_4)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(button_5))
														.addGroup(gl_panel.createSequentialGroup()
																.addComponent(button_1)
																.addGap(6)
																.addComponent(button_2)))
												.addGroup(gl_panel.createSequentialGroup()
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(button_7)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(button_8))
												.addGroup(gl_panel.createSequentialGroup()
														.addComponent(button_19)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(button_20)))
										.addGap(35)
										.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(targetBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addGroup(gl_panel.createSequentialGroup()
																.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																		.addComponent(button_16, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE)
																		.addComponent(button_10, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																		.addComponent(button_11)
																		.addComponent(button_17)))
														.addGroup(gl_panel.createSequentialGroup()
																.addComponent(button_15)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(button_13)))
												.addGroup(gl_panel.createSequentialGroup()
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(btnud)
														.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(button_14)))
										.addGap(35)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createSequentialGroup()
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
																.addComponent(readBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addGroup(gl_panel.createSequentialGroup()
																		.addComponent(btnLog)
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addComponent(btnLn)
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addComponent(btnE))
																.addGroup(gl_panel.createSequentialGroup()
																		.addComponent(btnRound)
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addComponent(btnCeil)
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addComponent(btnFloor))
																.addGroup(gl_panel.createSequentialGroup()
																		.addComponent(btnCos)
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addComponent(btnSin)
																		.addPreferredGap(ComponentPlacement.RELATED)
																		.addComponent(btnTan)))
														.addGap(18)
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(btnua)
																.addComponent(button_12)
																.addComponent(btnIns)))
												.addGroup(gl_panel.createSequentialGroup()
														.addComponent(btnAcos)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(btnAsin)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(btnAtan)
														.addGap(18)
														.addComponent(btnAbs)))))
						.addGap(195))
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addGap(6)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
						.addGap(12)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(button_1)
												.addComponent(button_2))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(button_4)
												.addComponent(button_5))
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createSequentialGroup()
														.addGap(4)
														.addComponent(button_8))
												.addGroup(gl_panel.createSequentialGroup()
														.addGap(5)
														.addComponent(button_7)))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(button_19)
												.addComponent(button_20)))
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(button)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(button_3)
										.addGap(5)
										.addComponent(button_6)
										.addGap(7)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(button_9)
												.addComponent(button_15)
												.addComponent(button_13)))
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(button_10)
												.addComponent(button_11))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(button_16)
												.addComponent(button_17))
										.addGap(3)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addComponent(btnud)
												.addComponent(button_14)))
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnLog)
												.addComponent(btnLn)
												.addComponent(btnE)
												.addComponent(button_12))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnCos)
												.addComponent(btnSin)
												.addComponent(btnTan)
												.addComponent(btnua))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnAcos)
												.addComponent(btnAsin)
												.addComponent(btnAtan)
												.addComponent(btnAbs))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnFloor)
												.addComponent(btnCeil)
												.addComponent(btnRound))))
						.addGap(12)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(targetBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
										.addComponent(readBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(btnIns)))
						.addGap(33))
				);
		gl_panel.linkSize(SwingConstants.VERTICAL, new Component[] {button_1, button_2, button_19, button_6, button_7, button_8, button_3, button_4, button_5, button, button_20, button_9});
		gl_panel.linkSize(SwingConstants.VERTICAL, new Component[] {button_10, button_11, button_16, button_17, button_14, button_15, btnud, button_13});
		gl_panel.linkSize(SwingConstants.VERTICAL, new Component[] {btnLn, btnLog, btnFloor, btnAcos, btnAsin, btnAtan, btnCeil, btnE, btnCos, btnSin, btnTan, btnRound});
		gl_panel.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnIns, button_12, btnAbs, btnua});
		gl_panel.linkSize(SwingConstants.HORIZONTAL, new Component[] {button_1, button_2, button_19, button_6, button_7, button_8, button_3, button_4, button_5, button, button_20, button_9});
		gl_panel.linkSize(SwingConstants.HORIZONTAL, new Component[] {button_10, button_11, button_16, button_17, button_14, button_15, btnud, button_13});
		gl_panel.linkSize(SwingConstants.HORIZONTAL, new Component[] {btnLn, btnLog, btnFloor, btnAcos, btnAsin, btnAtan, btnCeil, btnE, btnCos, btnSin, btnTan, btnRound});

		panel.setLayout(gl_panel);


		GroupLayout gl_contentPanel = new GroupLayout(this);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 741, Short.MAX_VALUE)
				);
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 291, Short.MAX_VALUE)
				);
		this.setLayout(gl_contentPanel);
		setLayout(new BorderLayout(0, 0));
		add(panel);



	}


	private void checkValid() {

		try{

			function = new DataTransform(textPane.getText(), functionMap);
			this.textIsValid = true;
			fireNextValidityChanged();

		}
		catch(IllegalArgumentException i){
			this.textIsValid = false;
			fireNextValidityChanged();
		}
	}





	private void insert(String text, Color color) {
		StyleConstants.setForeground(style, color);

		try { 
			doc.insertString(doc.getLength(), text, style); 
			expression.add(text);

			//update isValid
			checkValid();

		}
		catch (BadLocationException e){
			e.printStackTrace();
		}

	}

	private void delete(String text){

		if(text!=null)
			try {
				doc.remove(doc.getLength()-text.length(),text.length());			
				//if it is AKey then remove from map as well
				functionMap.remove(text);			
				//update isValid
				checkValid();

			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		//textPane.select(doc.getLength()-text.length(),text.length());
		//textPane.replaceSelection("");
	}



	@Override
	public boolean validity() {
		return textIsValid;
	}





	@Override
	protected WizardNode getNextStep() {
		return new MetaDescriptionPanel<>(this, director, function, -1, target);
	}





	@Override
	public int numberOfForks() {
		return 1;
	}
}

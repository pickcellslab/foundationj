package org.pickcellslab.foundationj.dbm.meta;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;



/**
 * A {@link MetaItem} useful to annotate the MetaModel with some module specific information
 * 
 * @author Guillaume Blin
 *
 */

@Data(typeId = "MetaAnnotation", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaAnnotation extends MetaItem implements Hideable, ManuallyStored {

	static final String linkType = "NOTE";


	private MetaAnnotation(String uid){
		super(uid);
	}

	static MetaAnnotation getOrCreate(
			MetaInstanceDirector director,
			String name, String creator,
			String description,
			MetaQueryable target) throws InstantiationHookException {
		
		final MetaAnnotation ma = director.getOrCreate(MetaAnnotation.class, MetaItem.randomUID(MetaAnnotation.class));
		if(!ma.getAttribute(MetaModel.name).isPresent()){// Check if it was retrieved or if it already existed
			ma.setAttribute(MetaItem.name, name);
			ma.setAttribute(MetaItem.desc, description);
			ma.setAttribute(AKey.get("Creator",String.class), creator);
			new DataLink(linkType, ma, target, true);
		}
		return ma;
		
	}

	

	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.creator, MetaModel.desc, AKey.get("Creator",String.class));
	}
	
	
	

	@Override
	public String toString() {
		return "Annotation";
	}

	@Override
	public String toHTML() {
		return "<HTML>MetaAnnotation: "+name()
		+ "<br> Created by : "+getAttribute(AKey.get("Creator",String.class)).get()
		+ "<br> Description : "+description()+"</HTML>";
	}



}

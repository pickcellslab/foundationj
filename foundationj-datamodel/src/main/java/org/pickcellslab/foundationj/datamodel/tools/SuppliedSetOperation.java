package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Performs a sequence of {@link SetOperation} on Sets supplied by an {@link OperationSuplier} and returns another Set.
 * Definitions of the Sets to operate on are held in this {@link SuppliedSetOperation} and are used to communicate with
 * the {@link OperationSupplier} during the operation.  
 * 
 * @author Guillaume Blin
 *
 * @param <D> The type of the definitions of the sets
 */
public class SuppliedSetOperation<D> {

	final List<D> descriptions;
	final List<SetOperation> operations;
	
	protected SuppliedSetOperation(Builder<D> b){
		this.descriptions = b.descriptions;
		this.operations = b.operations;
	}
	

	
	public <T> Set<T> operate(OperationSuplier<Set<T>,D> s) {		
		Iterator<D> requests = descriptions.iterator();
		Iterator<SetOperation> ops = operations.iterator();		
		Set<T> result = s.supply(requests.next());
		while(requests.hasNext()){
			Set<T> next = s.supply(requests.next());
			result = ops.next().apply(result, next);
		}				
		return result;
	}
	
	
		
	
	public static class Builder<D>{
		
		final List<D> descriptions = new ArrayList<>();
		final List<SetOperation> operations = new ArrayList<>();
		//private boolean keep;
		
		protected Builder(D first){
			Objects.requireNonNull(first);
			descriptions.add(first);
		}
		
		public Builder<D> union(D next){
			Objects.requireNonNull(next);
			descriptions.add(next);
			operations.add(Sets.union());
			return this;
		}
		
		public Builder<D> intersection(D next){
			Objects.requireNonNull(next);
			descriptions.add(next);
			operations.add(Sets.intersection());
			return this;
		}
		
		public Builder<D> difference(D next){
			Objects.requireNonNull(next);
			descriptions.add(next);
			operations.add(Sets.difference());
			return this;
		}
		
		
		public Builder<D> symetric(D next){
			Objects.requireNonNull(next);
			descriptions.add(next);
			operations.add(Sets.symDifference());
			return this;
		}
		
		
		/*TODO allow to keep all the intermediary sets obtained during the operation
		public Builder<D,T> setKeepAllSets(boolean keep){
			this.keep = keep;
			return this;
		}
		*/
		
		public SuppliedSetOperation<D> build(){
			return new SuppliedSetOperation<>(this);
		};
		
		
	}
	

}

package org.pickcellslab.foundationj.processors;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.Service;
import org.pickcellslab.foundationj.annotations.WithService;



public class WithServiceProcessor extends AbstractProcessor {

	@Override
	public Set<String> getSupportedAnnotationTypes(){
		return Collections.singleton(WithService.class.getName());
	};


	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;
		
		// Find Data elements from the current compilation sources
		for (TypeElement type : (Collection<TypeElement>)roundEnv.getElementsAnnotatedWith(WithService.class)) {

			// Check that the value is annotated with @Service
			WithService ws = type.getAnnotation(WithService.class);
			try {
				ws.value();
			}catch(MirroredTypeException e) {
				Element tm = processingEnv.getTypeUtils().asElement(e.getTypeMirror());
				if(tm.getAnnotation(Service.class) == null){					
					processingEnv.getMessager().printMessage(Kind.ERROR, "The value of @WithService must be a "
							+ "class which is annotated with @Service", type);
				}
			}

		}

		return false;
	}



}

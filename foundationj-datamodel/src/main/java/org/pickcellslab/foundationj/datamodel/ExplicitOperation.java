package org.pickcellslab.foundationj.datamodel;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitCombiner;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.reductions.ExplicitReduction;

/**
 * Root interface for {@link Mapping} compatible data operations.
 * <br><br>
 * implementing class must be annotated with {@link Mapping}
 * 
 * @see ExplicitPredicate
 * @see ExplicitFunction
 * @see ExplicitCombiner
 * @see ExplicitReduction
 * 
 * @author Guillaume Blin
 *
 */
public interface ExplicitOperation {

	public String description();
	
}

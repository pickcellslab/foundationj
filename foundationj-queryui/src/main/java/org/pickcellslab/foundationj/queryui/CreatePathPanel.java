package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.functions.Constant;
import org.pickcellslab.foundationj.datamodel.predicates.BiFunctionPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.EvaluationContainerBuilder;
import org.pickcellslab.foundationj.datamodel.tools.IncludeLinksBased;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaPath;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.MetaReductionOperation;
import org.pickcellslab.foundationj.dbm.meta.MetaSplit;
import org.pickcellslab.foundationj.dbm.meta.MetaTraversal;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition.TraversalPolicy;
import org.pickcellslab.foundationj.dbm.queries.builders.LastLinkEvaluation;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig.PathMode;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.queryui.SplitterUI.SplitID;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.CardPanel;
import org.pickcellslab.foundationj.ui.utils.DataToIconID;
import org.pickcellslab.foundationj.ui.wizard.Copyable;
import org.pickcellslab.foundationj.ui.wizard.ValidityListener;
import org.pickcellslab.foundationj.ui.wizard.ValidityToggle;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

import com.alee.extended.breadcrumb.WebBreadcrumb;
import com.alee.extended.breadcrumb.WebBreadcrumbPanel;
import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.WebAccordion;
import com.alee.extended.panel.WebAccordionStyle;
import com.alee.extended.panel.WebCollapsiblePane;
import com.alee.extended.window.WebPopOver;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebList;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.tooltip.TooltipManager;

@SuppressWarnings("serial")
public class CreatePathPanel<D> extends ValidityTogglePanel implements ValidityListener, Copyable<CreatePathPanel<D>>{




	// ******************************************************** private interfaces ****************************************************



	@FunctionalInterface
	private interface PathFinder{
		void findPath();
	}


	private interface Splittable{
		MetaQueryable getData();
		JLabel getSplitLabel();
	}


	private interface EvalContainer{
		MetaQueryable getData();
		JLabel getEvalLabel();
		ExplicitPredicate<?> getPredicate();
		void setPredicate(ExplicitPredicate<?> p);
		Decision getDecision();
		boolean allowsDecisionToggle();
		void toggleDecision();
	}

	private interface ToggleState{
		boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb);
		Decision getDefault(EvalContainer targetCrumb);
		Decision toggle(EvalContainer c, EvalContainer targetCrumb);
	}


	private enum Toggle implements ToggleState{

		/**
		 * PathMode -> Path; Path object is class included in allowed
		 */
		PathMCPathInclusion{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return true;
			}

			@Override
			public Decision toggle(EvalContainer c,EvalContainer targetCrumb) {
				if(c.getPredicate() == null){
					if(c.getDecision() == Decision.INCLUDE_AND_CONTINUE)
						return Decision.EXCLUDE_AND_CONTINUE;
					else
						return Decision.INCLUDE_AND_CONTINUE;
				}
				else {
					if(c.getDecision() == Decision.INCLUDE_AND_CONTINUE)
						return Decision.EXCLUDE_AND_STOP;
					else
						return Decision.INCLUDE_AND_CONTINUE;	
				}
			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				if(targetCrumb.getData() instanceof MetaLink)
					return Decision.INCLUDE_AND_CONTINUE;
				else
					return Decision.EXCLUDE_AND_CONTINUE;
			}
		},

		/**
		 * PathMode -> Path; Path object is class excluded from allowed
		 */
		PathMCPathExclusion{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return c.getPredicate() != null;
			}

			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {
				if(c.getPredicate() == null)
					return Decision.EXCLUDE_AND_CONTINUE;
				if(c.getDecision() == Decision.EXCLUDE_AND_CONTINUE)
					return Decision.EXCLUDE_AND_STOP;
				else			
					return Decision.EXCLUDE_AND_CONTINUE;
			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				if(targetCrumb.getData() instanceof MetaLink)
					return Decision.INCLUDE_AND_CONTINUE;
				else
					return Decision.EXCLUDE_AND_CONTINUE;
			}
		},

		/**
		 * PathMode -> Path; Path object is link
		 */
		PathLinkPath{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return false;	
				//TODO allow more complex filtering of links 
				//return c.getPredicate() != null;
			}

			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {
				// only allowed when predicate is present
				if(c.getDecision() == Decision.EXCLUDE_AND_CONTINUE)
					return Decision.EXCLUDE_AND_STOP;
				else return Decision.EXCLUDE_AND_CONTINUE;
			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				return Decision.INCLUDE_AND_CONTINUE;
			}
		},


		/**
		 * PathMode -> Path; Target is Class (global default is EC)
		 */
		MCTargetOfPathPath{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return true;
			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				return Decision.INCLUDE_AND_CONTINUE;
			}

			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {
				if(c.getDecision() == Decision.INCLUDE_AND_CONTINUE)
					return Decision.INCLUDE_AND_STOP;
				else
					return Decision.INCLUDE_AND_CONTINUE;
			}

		},

		/**
		 * PathMode -> Path; Target is Link (global default is EC)
		 */
		LinkTargetOfPathPath{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return true;
			}


			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {				
				if(c.getDecision() == Decision.INCLUDE_AND_CONTINUE)
					return Decision.INCLUDE_AND_STOP;
				else
					return Decision.INCLUDE_AND_CONTINUE;				
			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				return Decision.INCLUDE_AND_CONTINUE;
			}

		},



		// =================== PathMode --> Target ===========================








		/**
		 * PathMode -> Target; Path object is class (global default depends on target -> link (IC) / Class (EC))
		 */
		PathMCTarget{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {				
				return false; // otherwise set as default
			}


			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {
				if(c.getPredicate() == null)
					return getDefault(targetCrumb);
				else
					return Decision.EXCLUDE_AND_STOP;
			}



			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				if(targetCrumb.getData() instanceof MetaLink)
					return Decision.INCLUDE_AND_CONTINUE;
				else
					return Decision.EXCLUDE_AND_CONTINUE;
			}
		},

		/**
		 * PathMode -> Target; Path object is link
		 */
		PathLinkTarget{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return false;	
				//TODO allow more complex filtering of links 
				//return c.getPredicate() != null;
			}

			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {
				// only allowed when predicate is present
				if(targetCrumb.getData() instanceof MetaLink)
					return Decision.EXCLUDE_AND_CONTINUE;
				else
					return Decision.INCLUDE_AND_CONTINUE;
			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				if(targetCrumb.getData() instanceof MetaLink)
					return Decision.EXCLUDE_AND_CONTINUE;
				else
					return Decision.INCLUDE_AND_CONTINUE;
			}
		},

		/**
		 * PathMode -> Target; Target is Class
		 */
		MCTargetOfPath{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return true;
			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				return Decision.INCLUDE_AND_CONTINUE;
			}

			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {
				if(c.getDecision() == Decision.INCLUDE_AND_CONTINUE)
					return Decision.INCLUDE_AND_STOP;
				else
					return Decision.INCLUDE_AND_CONTINUE;
			}

		},

		/**
		 * PathMode -> Target; Target is Link
		 */
		LinkTargetOfPath{

			@Override
			public boolean allowsDecisionToggle(EvalContainer c, EvalContainer targetCrumb) {
				return true;
			}


			@Override
			public Decision toggle(EvalContainer c, EvalContainer targetCrumb) {
				if(c.getPredicate() == null){
					if(c.getDecision() == Decision.INCLUDE_AND_CONTINUE)
						return Decision.INCLUDE_AND_STOP;
					else
						return Decision.INCLUDE_AND_CONTINUE;
				}
				else{
					/*
					switch(c.getDecision()){
					case EXCLUDE_AND_CONTINUE: return Decision.EXCLUDE_AND_STOP;
					case EXCLUDE_AND_STOP: return Decision.INCLUDE_AND_CONTINUE;
					case INCLUDE_AND_CONTINUE: return Decision.INCLUDE_AND_STOP;
					case INCLUDE_AND_STOP: return Decision.EXCLUDE_AND_CONTINUE;
					}
					 */
					if(c.getDecision() == Decision.EXCLUDE_AND_CONTINUE)
						return Decision.EXCLUDE_AND_STOP;
					else
						return Decision.EXCLUDE_AND_CONTINUE;
				}

			}

			@Override
			public Decision getDefault(EvalContainer targetCrumb) {
				return Decision.INCLUDE_AND_CONTINUE;
			}

		}


	}


	// ******************************************************** END of private interfaces ****************************************************








	// ******************************************************** Fields ***********************************************************************



	private final WebCollapsiblePane filteringPane;
	private final WebCollapsiblePane pathSplitPane;
	private final WebCollapsiblePane groupingPane;
	private final WebCollapsiblePane mechaPane;
	private final CardPanel<MetaQueryable, SplitterChoicePanel> groupingCard;
	private final CardPanel<MetaQueryable, SplitterChoicePanel> pathSplitCard;
	private final CardPanel<MetaQueryable, CombineFilterPanel> filterCard;
	private final CardPanel<MetaQueryable, MechanismChoicePanel> mechaCard;


	private final UITheme theme;
	private final DataAccess access;

	//Path definition fields
	private final QueryWizardConfig config;	
	private final ChoiceSet choices;


	private final WebBreadcrumb bc;
	private WebBreadcrumbPanel lastSelected;

	private Splittable lastSplitted;

	private final RootCrumb root; 
	private List<PathCrumb> crumbs = new ArrayList<>();
	private final TargetCrumb target;

	private final DefaultListModel<EvalContainer> exclusions;
	private final SpinnerNumberModel minDepthModel, maxDepthModel;
	private final JSpinner minDepthField, maxDepthField;





	// ******************************************************** END of Fields ***********************************************************************







	CreatePathPanel(UITheme theme, DataAccess access, ChoiceSet choices, QueryWizardConfig config, boolean includeReduction) throws DataAccessException {

		this.theme = theme;
		this.access = access;
		this.choices = choices;

		this.setPreferredSize(new Dimension(450,450));

		Objects.requireNonNull(config);


		this.config = config;

		//JScrollPane scrollPane = new JScrollPane();
		//setLayout(new VerticalFlowLayout());
		setLayout(new BorderLayout());

		WebAccordion accordion = new WebAccordion(WebAccordionStyle.accordionStyle);
		accordion.setMultiplySelectionAllowed(false);

		//scrollPane.setViewportView(accordion);



		//Create CardPanel with filtering choice
		//Factory for a new filter choice
		Function<MetaQueryable, CombineFilterPanel> filterPanelFactory = (q)->{
			final ChoicePanel<MetaFilter> master = choices.getFilterChoice(q);
			CombineFilterPanel<Object> cfp = new CombineFilterPanel<>(master);
			cfp.addValidityListener(this);
			return cfp;
		};

		filterCard = new CardPanel<>(filterPanelFactory);
		filteringPane = accordion.addPane(null,"Filter Combination",filterCard);



		//Factory for a new SplitterChoice
		Function<MetaQueryable, SplitterChoicePanel> pathSplitterPanelFactory = (q)->{
			SplitterChoicePanel scp = new SplitterChoicePanel(theme, access.dataRegistry(), choices, q, true, !includeReduction);
			scp.addValidityListener(this);
			return scp;
		};
		pathSplitCard = new CardPanel<>(pathSplitterPanelFactory);
		pathSplitPane = accordion.addPane(null,"Path Splitting", pathSplitCard);



		//Factory for a new SplitterChoice
		Function<MetaQueryable, SplitterChoicePanel> splitterPanelFactory = (q)->{
			SplitterChoicePanel scp = new SplitterChoicePanel(theme, access.dataRegistry(), choices, q, false, !includeReduction);
			scp.addValidityListener(this);
			return scp;
		};
		groupingCard = new CardPanel<>(splitterPanelFactory);
		groupingPane = accordion.addPane("Target Grouping", groupingCard);



		if(!config.isGroupingEnabled()){
			groupingPane.setEnabled(false);
		}
		if(!config.isPathSplittingEnabled())				
			pathSplitPane.setEnabled(false);


		// Add reduction pane if required
		if(includeReduction){
			//Factory for a new SplitterChoice
			Function<MetaQueryable, MechanismChoicePanel> mechaPanelFactory = (q)->{
				MechanismChoicePanel mcp = new MechanismChoicePanel(
						theme,
						access.dataRegistry(),
						choices.getReadableChoice(config.getFixedRootType()),
						choices.getReadableChoice(q), true);
				mcp.addValidityListener(this); 
				return mcp;
			};
			mechaCard = new CardPanel<>(mechaPanelFactory);
			mechaPane = accordion.addPane("Reduction Operation", mechaCard);
		}
		else{
			mechaCard = null;
			mechaPane = null;
		}






		//------------------------------------------------------------------------------------------------------------
		//Path Definition

		// Exclusion list
		JList<EvalContainer> exclList = new JList<>();
		exclList.setCellRenderer(new ListCellRenderer<EvalContainer>(){

			MetaRenderer mr = new MetaRenderer(theme, access.dataRegistry());

			@Override
			public Component getListCellRendererComponent(JList<? extends EvalContainer> list, EvalContainer value,
					int index, boolean isSelected, boolean cellHasFocus) {								
				return mr.getListCellRendererComponent((JList)list, value.getData(), index, isSelected, cellHasFocus);
			}

		});

		exclList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		exclusions = new DefaultListModel<>();
		exclList.setModel(exclusions);
		exclList.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if(!exclusions.isEmpty())
					if ( SwingUtilities.isRightMouseButton(e) )
					{			            
						int row = exclList.locationToIndex(e.getPoint());
						exclList.setSelectedIndex(row);
						JPopupMenu menu = new JPopupMenu();
						JMenuItem addItem = new JMenuItem("Add Back to Path");
						addItem.addActionListener(l->{
							exclusions.remove(exclList.getSelectedIndex());
							updatePath();
						});
						menu.add(addItem);

						JMenuItem leaveItem = new JMenuItem("Leave it here");
						menu.add(leaveItem);

						menu.show(exclList, e.getX(), e.getY());
					}
			}
		});





		//Path UI
		bc = new WebBreadcrumb();

		//Root
		root = new RootCrumb();
		root.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if(SwingUtilities.isLeftMouseButton(e) && root.getRoot()!=null){
					root.setUndecorated(false);
					if(null != lastSelected)
						lastSelected.setUndecorated(true);
					filterCard.show(root.getRoot());
					choices.getQueryableChoice().setSelected(root.getRoot());
					choices.highlightMaster();
					lastSelected = root;
				}
			}
		});

		root.addValidityListener(this);
		root.addPathFinder(()->updatePath());

		bc.add(root);


		//Target
		target = new TargetCrumb();
		target.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if(SwingUtilities.isLeftMouseButton(e) && target.getTarget()!=null){
					target.setUndecorated(false);
					if(null != lastSelected)
						lastSelected.setUndecorated(true);
					filterCard.show(target.getTarget());
					groupingCard.show(target.getTarget());	
					if(mechaPane!=null)
						mechaCard.show(target.getTarget());
					choices.getQueryableChoice().setSelected(target.getTarget());
					choices.highlightMaster();
					groupingPane.expand();
					lastSelected = target;
				}
			}
		});
		target.addValidityListener(this);
		target.addPathFinder(()->updatePath());


		bc.add(target);




		//Path UI layout

		JScrollPane pathScroll = new JScrollPane();
		JLabel defLabel = new JLabel("Path Definition       ");
		JLabel minDepthLabel = new JLabel("From Depth");
		minDepthModel = new SpinnerNumberModel(0, 0, 1000,1);
		minDepthField = new JSpinner(minDepthModel);
		minDepthField.setEnabled(false);

		JLabel maxDepthLabel = new JLabel("To Depth");
		maxDepthModel = new SpinnerNumberModel(0, 0, 1000,1);
		maxDepthField = new JSpinner(maxDepthModel);
		maxDepthField.setEnabled(false);

		GroupPanel gp = new GroupPanel(10, defLabel, minDepthLabel, minDepthField, maxDepthLabel, maxDepthField);


		gp.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		pathScroll.setColumnHeaderView(gp);

		JPanel pathPanel = new JPanel();
		pathPanel.setLayout(new BorderLayout());


		final WebCollapsiblePane exclusionPane = new WebCollapsiblePane ("Excluded From Path", exclList );
		exclusionPane.setTitlePanePostion ( SwingConstants.LEFT );


		pathPanel.add(bc, BorderLayout.CENTER);
		pathPanel.add(exclusionPane, BorderLayout.WEST);

		pathScroll.setViewportView(pathPanel);	
		pathScroll.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

		pathScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		//pathScroll.setPreferredSize(new Dimension(pathScroll.getPreferredSize().width,pathScroll.getPreferredSize().height+15));

		add(pathScroll, BorderLayout.NORTH);
		add(accordion, BorderLayout.CENTER);



	}









	private void updatePath(){

		if(root.validity() && target.validity()){

			//if root = target -> search for self referencing MetaLinks
			bc.removeAll();
			bc.add(root);
			crumbs.clear();


			if(root.getRoot().equals(target.getTarget())){



				List<MetaLink> selfRefs = root.getRoot().metaLinks()
						.filter((ml)->ml.source().equals(ml.target())).collect(Collectors.toList());
				if(selfRefs.isEmpty()){
					WebPopOver pop = new WebPopOver();
					pop.setLayout ( new VerticalFlowLayout () );
					pop.setMargin ( 10 );
					pop.setUndecorated(true);
					pop.setModal(true);


					pop.add(new WebLabel("<HTML>You chose the Root and the target to be the same type<br>"
							+ "However there are no self referencing links. Please change your selection.</br></HTML>"));			

					WebButton okBtn = new WebButton("Ok");
					okBtn.addActionListener(l->pop.dispose());
					pop.add(okBtn);
					pop.show(bc);

					target.resetAll();
					bc.add(target);
					updateDepth();
					this.fireNextValidityChanged();
					return;
				}
				else if(selfRefs.size() == 1 || 
						(selfRefs.size()==2 && selfRefs.get(0).itemDeclaredType().equals(selfRefs.get(1).itemDeclaredType())) // INCOMING and OUTGOING of same type
						){					

					crumbs.add(addLinkCrumb(selfRefs.get(0),Direction.BOTH));
					bc.add(target);
					updateDepth();
					this.fireNextValidityChanged();
					return;
				}				
				else{

					WebPopOver pop = new WebPopOver();
					pop.setMargin ( 10 );
					pop.setLayout ( new VerticalFlowLayout () );
					pop.setModal(true);

					pop.add(new WebLabel("Choose which links to traverse:"));		

					WebList webList = new WebList ( selfRefs );
					webList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					// webList.setVisibleRowCount ( 4 );
					webList.setSelectedIndex ( 0 );
					webList.setEditable ( false );			      

					pop.add(webList);

					WebButton okBtn = new WebButton("Ok");
					okBtn.addActionListener(l->pop.dispose());
					pop.add(okBtn);			        

					pop.setUndecorated(true);

					pop.show(bc);

					crumbs.add(addLinkCrumb((MetaLink) webList.getSelectedValue(),Direction.BOTH));

					bc.add(target);
					updateDepth();
					this.fireNextValidityChanged();
					return;			


				}


			}



			//Impose Constraints on the path if required

			TraverserConstraints tc = null;
			if(exclusions.isEmpty())
				tc = Traversers.newConstraints().fromMinDepth().toMaxDepth()
				.traverseLink(MetaLink.fromLink, Direction.BOTH)
				.and(MetaLink.toLink, Direction.BOTH).includeAllNodes();
			else{
				IncludeLinksBased<TraverserConstraints> ilb= 
						Traversers.newConstraints().fromMinDepth().toMaxDepth()
						.traverseLink(MetaLink.fromLink, Direction.BOTH)
						.and(MetaLink.toLink, Direction.BOTH);


				EvaluationContainerBuilder<TraverserConstraints> ecb = null;
				for(int i = 0; i<exclusions.size(); i++){
					EvalContainer e = exclusions.get(i);
					ecb = ilb.withEvaluations(P.keyTest(MetaModel.recognition, Op.TextOp.EQUALS, e.getData().recognitionString()), 
							Decision.EXCLUDE_AND_STOP, Decision.INCLUDE_AND_CONTINUE);
				}
				tc = ecb.done();
			}






			//Now Find the shortest path given constraints


			MetaPath path = access.metaModel().shortestPath(root.getRoot(), target.getTarget(), tc);
			List<MetaQueryable> list = path.metaPath();

			list.remove(root.getRoot());
			list.remove(target.getTarget());

			if(list.isEmpty()){

				//Check if the target is a link of the root if not return false for no path found
				final Set<MetaLink> set = root.getRoot().metaLinks().collect(Collectors.toSet());
				if(!set.contains(target.getTarget())){					
					WebPopOver pop = new WebPopOver();
					pop.setLayout(new VerticalFlowLayout());
					pop.add(new JLabel("There are no posible path bewteen the selected root and target"));
					JButton okBtn = new JButton("Ok");
					okBtn.addActionListener(l->pop.dispose());
					pop.add(okBtn);
					pop.setModal(true);
					pop.show(bc);

					target.resetAll();
					bc.add(target);	
					updateDepth();
					return;
				}
			}



			MetaQueryable last = root.getRoot();

			//int add = 0;
			//if(target.getTarget() instanceof MetaClass) 
			//	add=1;

			for(int i = 0; i<list.size()/*-add*/; i++){


				MetaQueryable mq = list.get(i);
				if(mq instanceof MetaClass){

					PathCrumb crumb = new PathCrumb((MetaClass)mq, target);

					crumb.addMouseListener(new MouseAdapter(){
						@Override
						public void mouseClicked(MouseEvent e){
							if(SwingUtilities.isLeftMouseButton(e) && crumb.getData()!=null){
								crumb.setUndecorated(false);
								if(null != lastSelected)
									lastSelected.setUndecorated(true);
								lastSelected = crumb;

								choices.getQueryableChoice().setSelected(crumb.getData());
								choices.highlightMaster();
								filterCard.show(crumb.getData());
								if(crumb.getData() instanceof MetaClass){									
									pathSplitCard.show(crumb.getData());
								}
								//TODO disable splitting

							}
						}
					});

					// keep track of the last node
					last = mq;


					bc.add(crumb);
					crumbs.add(crumb);

				}
				else{
					//Check direction
					Direction dir = Direction.OUTGOING;
					//System.out.println("Last : "+last.toString());
					//System.out.println("Last : "+((MetaLink)mq).target().toString());

					if(last.equals(((MetaLink)mq).target()))
						dir = Direction.INCOMING;
					crumbs.add(addLinkCrumb((MetaLink)mq, dir));					
				}


			}

			bc.add(target);

			this.fireNextValidityChanged();
		}

		updateDepth();
		return;


	}




	private void updateDepth(){


		boolean enable = root.getRoot().equals(target.getTarget()) 
				|| 
				(
						target.getTarget() instanceof MetaLink &&
						((MetaLink) target.getTarget()).source().equals(((MetaLink) target.getTarget()).target()))
				;

		minDepthField.setEnabled(enable);
		maxDepthField.setEnabled(enable);

		// set min depth
		int min = getFirstIncludedNode(false);
		minDepthField.setValue(min);
		maxDepthField.setValue(getLastIncludedNode(false));
		minDepthModel.setMinimum(min);
		maxDepthModel.setMinimum(min);


	}







	//--------------------------------------------------------------------------------------------
	//ValidityListener

	private PathCrumb addLinkCrumb(MetaLink ml, Direction dir) {

		PathCrumb crumb = new PathCrumb(ml, dir, target);

		crumb.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				if(SwingUtilities.isLeftMouseButton(e) && crumb.getData()!=null){
					crumb.setUndecorated(false);
					if(null != lastSelected)
						lastSelected.setUndecorated(true);
					lastSelected = crumb;

					choices.getQueryableChoice().setSelected(crumb.getData());
					filterCard.show(crumb.getData());
					if(crumb.getData() instanceof MetaClass){									
						pathSplitCard.show(crumb.getData());
					}
				}
			}
		});

		bc.add(crumb);

		return crumb;
	}




	/**
	 * @param checkFields whether or not to take the the depth input fields into consideration if they are enabled
	 * @return
	 */
	private int getFirstIncludedNode(boolean checkFields) {

		if(checkFields && minDepthField.isEnabled())
			return minDepthModel.getNumber().intValue();

		int first = -1;
		if(config.getPathMode() == PathMode.TARGET){
			if(target.getTarget() instanceof MetaClass){
				first = (crumbs.size()+1)/2;
			}else{
				first = crumbs.size()/2 + 1;
			}
		}
		else{
			first++;
			for(int c = 0; c<crumbs.size(); c++){
				if(crumbs.get(c).getData() instanceof MetaClass){
					if(crumbs.get(c).d != crumbs.get(c).getDefaultDecision())
						break;
					else
						first++;
				}
			}
		}

		return first;
	}



	private int getLastIncludedNode(boolean checkFields) {

		if(checkFields && maxDepthField.isEnabled())
			return maxDepthModel.getNumber().intValue();


		int last = -1;
		//if(config.getPathMode() == PathMode.TARGET){
		if(target.getTarget() instanceof MetaClass){
			last = (crumbs.size()+1)/2;
		}else{
			last = crumbs.size()/2 + 1;
		}

		return last;
	}






	@Override
	public boolean validity() {
		System.out.println("Validity of CreatePathPanel requested!");
		return root.validity() && target.validity();// && !crumbs.isEmpty();
		//TODO && pathSplitterCard.current().validity() && splitterCard.current().validity();
	}



	@Override
	public void nextValidityChanged(ValidityToggle vt) {

		WebCollapsiblePane pane = null;

		if(vt instanceof CombineFilterPanel)
			pane = filteringPane;
		else if(filteringPane.isExpanded())
			pane = groupingPane;
		else if(pathSplitPane.isExpanded())
			pane = pathSplitPane;
		else
			pane = mechaPane;

		if(pane == null){
			System.out.println("CreatePathPanel 1084 : NexValidityChange received but vt unknown -> "+vt.getClass());
			return;
		}

		if(vt.validity())
			pane.setIcon(theme.icon(IconID.Misc.VALID, 16));
		else
			pane.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16));

		this.fireNextValidityChanged();

	}







	//----------------------------------------------------------------------------------------------
	// Custom BreadCrumb elements









	// *****************************************************   Root Crumb   ***********************************************************







	private class RootCrumb extends WebBreadcrumbPanel implements ValidityToggle, Splittable{


		private MetaClass root;
		private ExplicitPredicate<DataItem> mf;
		private ValidityListener l;
		private PathFinder finder;
		private JLabel split;
		private final JComponent filterLabel;
		private JLabel nameLabel;
		private JLabel typeIcon;

		RootCrumb() throws DataAccessException{

			JPanel rootPanel = new JPanel();
			rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
			rootPanel.setBackground(new Color(0,0,0,0));
			rootPanel.setBorder(new EmptyBorder(10,5,10,5));

			JLabel rootFlag = new JLabel("Start", theme.icon(IconID.Misc.START_FLAG, 16), SwingConstants.CENTER);
			rootFlag.setAlignmentX(Component.CENTER_ALIGNMENT);
			TooltipManager.addTooltip(rootFlag,"Starting point of the Path",TooltipWay.left,0);
			rootPanel.add(rootFlag);


			typeIcon = new JLabel();
			typeIcon.setBorder(new EmptyBorder(5,5,5,5));
			typeIcon.setAlignmentX(Component.CENTER_ALIGNMENT);

			nameLabel = new JLabel(); 
			nameLabel.setBorder(new EmptyBorder(0,5,5,5));
			nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

			root = config.getFixedRootType();

			if(root != null){
				typeIcon.setIcon(DataToIconID.getIcon(theme, root.itemClass(access.dataRegistry()), 32));//theme.icon(IconID.Data.NODE, 32));
				nameLabel.setText(root.name());
				choices.getQueryableChoice().setSelected(root);
			}else{
				typeIcon.setIcon(theme.icon(IconID.Misc.QUESTION, 32));
				TooltipManager.addTooltip(typeIcon, "<HTML>Select a type and then double click to assign</HTML>", TooltipWay.up, 0);				
				nameLabel.setText("Select a Type");
			}

			rootPanel.add(typeIcon);




			MetaReadable<Object> mr = config.getFixedRootKey();

			if(mr!=null){

				filterLabel = new JComboBox<Object>();

				List<?> list = null;
				Object value = config.getFixedRootKeyValue();

				if(value == null) {
					list = access.queryFactory().read((MetaQueryable)mr.owner(), 50)
							.makeList(mr.getDimension(0)).inOneSet().getAll().run();
				}else {
					list = access.queryFactory().read((MetaQueryable)mr.owner(), 50)
							.makeList(mr.getDimension(0)).inOneSet().useFilter(mr.getDimension(0).equalsTo(value)).run();
				}

				if(list.isEmpty())
					throw new RuntimeException("No items found to populate roots"); //TODO throw a specific exception (Wizard Exception?)

				Comparator<Object> comp = (s1,s2)->s1.toString().compareTo(s2.toString());
				list.sort( (s1,s2)-> Objects.compare(s1,s2,comp));
				list.forEach(o->((JComboBox<Object>) filterLabel).addItem(o));
			}

			else{

				filterLabel = new JLabel("No Filter");		
				filterLabel.setBorder(new EmptyBorder(5,5,5,5));
				((JLabel) filterLabel).setIcon(theme.icon(IconID.Queries.FILTER, 16));
				TooltipManager.addTooltip(filterLabel, "Filter: right click to edit", TooltipWay.right, 0);





				filterLabel.addMouseListener(new MouseAdapter(){
					@Override
					public void mouseClicked(MouseEvent e){

						if(root == null){
							JOptionPane.showMessageDialog(filterLabel, "Please Choose a type first");
							return;
						}

						if(SwingUtilities.isLeftMouseButton(e)){
							choices.highlightFilter(root);
							filterCard.show(root);
							filteringPane.expand();						
						}
						else if(SwingUtilities.isRightMouseButton(e)){

							choices.highlightFilter(root);
							filterCard.show(root);

							JPopupMenu menu = new JPopupMenu();
							if(mf!=null){
								JMenuItem rItem = new JMenuItem("Remove");
								rItem.addActionListener(l->{
									((JLabel) filterLabel).setText("No Filter");
									mf = null;
								});
								menu.add(rItem);
							}


							JMenuItem listItem = new JMenuItem("Choose from the filter list");
							listItem.addActionListener(l->{
								Optional<MetaFilter> sel = choices.getCurrentFilter(root);
								if(sel.isPresent()){
									mf = sel.get().toFilter();
									((JLabel) filterLabel).setText(sel.get().name());
								}
								else
									JOptionPane.showMessageDialog(null, "No Selection detected");
							});
							menu.add(listItem);


							JMenuItem combItem = new JMenuItem("Choose from the Filter Combination dialog");
							combItem.addActionListener(l->{
								Optional<ExplicitPredicate> sel = (Optional)filterCard.current().getPredicate();
								if(sel.isPresent()){
									mf = sel.get();
									((JLabel) filterLabel).setText("Filter Combination");
								}
								else
									JOptionPane.showMessageDialog(null, "Please define a valid combination of filters first");
							});
							menu.add(combItem);

							menu.show(filterLabel, e.getX(), e.getY());


						}
					}

				});


			}





			if(config.getFixedRootType()==null)
				typeIcon.addMouseListener(new MouseAdapter(){
					@Override
					public void mouseClicked(MouseEvent e){

						if(SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2){

							Optional<MetaQueryable> selected = choices.getQueryableChoice().getSelected();
							if(!selected.isPresent()){
								JOptionPane.showMessageDialog(null, "No Selection detected");
								return;
							}
							if(!(selected.get() instanceof MetaClass)){
								JOptionPane.showMessageDialog(null, "Links cannot be root of a Path");
								return;
							}

							typeIcon.setIcon(DataToIconID.getIcon(theme, ((MetaClass) selected.get())
									.itemClass(access.dataRegistry()), 32));//theme.icon(IconID.Data.NODE, 32));
							nameLabel.setText(selected.get().name());

							if(!selected.get().equals(root)){
								root = (MetaClass) selected.get();
								filterCard.show(root);
								filterCard.current();
								finder.findPath();
								mf = null;
								((JLabel) filterLabel).setText("No Filter");
							}
						}
					}

				});





			//Splitting is allowed on roots

			split = new JLabel(theme.icon(IconID.Queries.NO_SPLIT, 32));
			split.setBorder(new EmptyBorder(5,5,5,5));

			if(config.getFixedRootType()!=null || !config.isPathSplittingEnabled())
				split.setEnabled(false);
			else
				split.addMouseListener(new MouseAdapter(){
					@Override
					public void mouseClicked(MouseEvent e){
						if(null!=root)
							if(SwingUtilities.isRightMouseButton(e)){
								JPopupMenu menu = new JPopupMenu();
								if(lastSplitted != RootCrumb.this){
									JMenuItem addItem = new JMenuItem("Split here");
									addItem.addActionListener(l->{
										pathSplitCard.show(root);
										pathSplitPane.expand();
										split.setIcon(theme.icon(IconID.Queries.SPLIT, 32));
										if(lastSplitted !=null)
											lastSplitted.getSplitLabel().setIcon(theme.icon(IconID.Queries.NO_SPLIT, 32));
										lastSplitted = RootCrumb.this;
									});
									menu.add(addItem);
								}
								else{
									JMenuItem rmItem = new JMenuItem("Remove");
									rmItem.addActionListener(l->{
										split.setIcon(theme.icon(IconID.Queries.NO_SPLIT, 32));
										lastSplitted = null;
									});
									menu.add(rmItem);
								}
								menu.show(split, e.getX(), e.getY());
							}
					}
				});






			setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			rootPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
			nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			filterLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
			split.setAlignmentX(Component.CENTER_ALIGNMENT);
			add(rootPanel);
			add(nameLabel);
			add(filterLabel);
			add(split);


		}




		public void addPathFinder(PathFinder finder){
			this.finder = finder;
		}

		@Override
		public void addValidityListener(ValidityListener l) {
			this.l= l;
		}


		@Override
		public void removeValidityListener(ValidityListener l) {
			// TODO Auto-generated method stub

		}



		@Override
		public boolean validity() {
			return root != null;
		}




		MetaClass getRoot(){
			return root;
		}


		NodeItem getRootInstance(){
			return (NodeItem) ((JComboBox)filterLabel).getSelectedItem();
		}


		ExplicitPredicate<DataItem> getPredicate(){
			if(config.getFixedRootKey() != null){
				MetaReadable<?> mr = config.getFixedRootKey();

				return new BiFunctionPredicate<>((mr.getDimension(0)),
						new Constant<>(((JComboBox)filterLabel).getSelectedItem()),
						Op.ObjectOp.EQUALS);
			}				
			else if(mf == null)
				return P.none();
			return mf;
		}


		@Override
		public MetaQueryable getData() {
			return root;
		}


		@Override
		public JLabel getSplitLabel() {
			return split;
		}



		void copy(RootCrumb other){

			this.root = other.root;
			if(root != null){
				typeIcon.setIcon(MetaToIcon.get(root, access.dataRegistry(), theme, 32));
				nameLabel.setText(root.name());
				choices.getQueryableChoice().setSelected(root);
			}

			this.mf = other.mf;
			if(config.isSynchronizeRoots()){
				((JComboBox)filterLabel).setSelectedIndex(((JComboBox)other.filterLabel).getSelectedIndex());
			}else{
				((JLabel) this.filterLabel).setIcon(((JLabel) other.filterLabel).getIcon());
				((JLabel) this.filterLabel).setText(((JLabel) other.filterLabel).getText());
			}


			this.split.setIcon(other.split.getIcon());

		}



	}



	// *****************************************************   Target Crumb   ***********************************************************





	class TargetCrumb extends WebBreadcrumbPanel implements ValidityToggle, ValidityListener, EvalContainer{


		// Validity Events Handling
		private GroupingChangeListener grpLst;
		private ValidityListener targetListener;

		// Target Definition
		private MetaQueryable target;
		private ExplicitPredicate<?> mf;


		private PathFinder finder;
		private Decision d = Decision.INCLUDE_AND_CONTINUE;

		private Direction direction = null;

		private final JLabel eval;

		private JLabel typeIcon;

		private JLabel nameLabel;

		private JLabel group;

		private Toggle toggle;



		TargetCrumb(){



			JPanel targetPanel = new JPanel();
			targetPanel.setLayout(new BoxLayout(targetPanel, BoxLayout.PAGE_AXIS));
			targetPanel.setBackground(new Color(0,0,0,0));
			targetPanel.setBorder(new EmptyBorder(10,5,10,5));


			JLabel finishFlag = new JLabel("Finish", theme.icon(IconID.Misc.FINISH_FLAG, 16), SwingConstants.CENTER);
			finishFlag.setText("Finish");
			finishFlag.setAlignmentX(Component.CENTER_ALIGNMENT);
			targetPanel.add(finishFlag);

			typeIcon = new JLabel(theme.icon(IconID.Misc.QUESTION, 32));
			typeIcon.setBorder(new EmptyBorder(5,5,5,5));
			typeIcon.setAlignmentX(Component.CENTER_ALIGNMENT);

			TooltipManager.addTooltip(typeIcon, "Double click to change", TooltipWay.down, 0);
			targetPanel.add(typeIcon);



			nameLabel = new JLabel("Select a Type");
			nameLabel.setBorder(new EmptyBorder(0,5,5,5));
			nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);



			// Allow for Grouping
			group = new JLabel("One Group");
			group.setBorder(new EmptyBorder(5,5,5,5));
			group.setIcon(theme.icon(IconID.Queries.ONE_GROUP, 32));
			group.setHorizontalTextPosition(SwingConstants.LEFT);
			group.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent e){
					choices.getQueryableChoice().setSelected(target);
					groupingPane.expand();
				}
			});
			TooltipManager.addTooltip(group,"Pool all objects into one group",TooltipWay.down, 0);
			group.setAlignmentX(Component.CENTER_ALIGNMENT);





			typeIcon.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent e){

					if(SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2){

						Optional<MetaQueryable> selected = choices.getQueryableChoice().getSelected();
						if(!selected.isPresent()){
							JOptionPane.showMessageDialog(null, "No Selection detected");
							return;
						}

						if(config.getAllowedQueryables()!= null 
								&& !config.getAllowedQueryables().test(selected.get())){
							JOptionPane.showMessageDialog(null, "The chosen object cannot be the target of this Path");
							return;
						}



						if((selected.get() instanceof MetaClass)){
							typeIcon.setIcon(DataToIconID.getIcon(theme, ((MetaClass) selected.get()).itemClass(access.dataRegistry()), 32));
							if(config.getPathMode() == PathMode.PATH)
								toggle = Toggle.MCTargetOfPathPath;
							else
								toggle = Toggle.MCTargetOfPath;
							direction = null;
						}
						else{
							direction = Direction.BOTH; //TODO enable choice
							typeIcon.setIcon(theme.icon(DataToIconID.get(direction), 32));
							if(config.getPathMode() == PathMode.PATH)
								toggle = Toggle.LinkTargetOfPathPath;
							else
								toggle = Toggle.LinkTargetOfPath;

						}

						nameLabel.setText(selected.get().name());

						if(selected.get() != target){

							target = selected.get();

							if(grpLst!=null){
								groupingCard.current().removeGroupingMethodListener(grpLst);
								groupingCard.current().removeValidityListener(TargetCrumb.this);
							}


							groupingCard.show(target);

							grpLst = (s) -> {
								if(s == null){
									group.setText("One Group");
									group.setIcon(theme.icon(IconID.Queries.ONE_GROUP, 32));
									TooltipManager.setTooltip(group,"Pool all objects into one group",TooltipWay.down, 0);
								}else{
									group.setText(s.name());
									group.setIcon(theme.icon(IconID.Queries.MULTIPLE_GROUPS, 32));
									TooltipManager.setTooltip(group,"Group objects "+s.name(),TooltipWay.down, 0);
								}
							};

							groupingCard.current().addGroupingMethodListener(grpLst);
							groupingCard.current().addValidityListener(TargetCrumb.this);

							if(mechaCard!=null){
								if(mechaCard.current()!=null)
									mechaCard.current().removeValidityListener(TargetCrumb.this);
								mechaCard.show(target);
								mechaCard.current().addValidityListener(TargetCrumb.this);
							}

							finder.findPath();
						}
					}
				}

			});



			//Allow to add evaluations
			eval = new JLabel("Status: ");
			eval.setBorder(new EmptyBorder(5,5,5,5));
			eval.setIcon(theme.icon(DataToIconID.get(d), 24));
			eval.setHorizontalTextPosition(SwingConstants.LEFT);
			eval.setAlignmentX(Component.CENTER_ALIGNMENT);


			eval.addMouseListener(buildEvalListener(this));
			TooltipManager.addTooltip(eval,"Right Click to edit",TooltipWay.down, 0);





			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			add(targetPanel);
			add(nameLabel);
			add(eval);
			add(group);


		}

		private void resetAll(){
			resetType();
			resetGroup();
			resetEvaluation();
		}


		private void resetType(){
			target = null;
			nameLabel.setText("Choose");
			typeIcon.setIcon(theme.icon(IconID.Misc.QUESTION, 32));
			if(mechaCard!=null)
				if(mechaCard.current()!=null)
					mechaCard.current().removeValidityListener(TargetCrumb.this);
		}


		private void resetGroup(){
			group.setText("One Group");
			group.setIcon(theme.icon(IconID.Queries.ONE_GROUP, 32));
			TooltipManager.setTooltip(group,"Pool all objects into one group",TooltipWay.down, 0);
			if(grpLst!=null){
				groupingCard.current().removeGroupingMethodListener(grpLst);
				groupingCard.current().removeValidityListener(TargetCrumb.this);
			}			
		}

		private void resetEvaluation(){
			eval.setText("Status: ");
		}



		public void addPathFinder(PathFinder finder){
			this.finder = finder;
		}

		@Override
		public void addValidityListener(ValidityListener l) {
			this.targetListener= l;
		}


		@Override
		public boolean validity() {
			return target != null && groupingCard.current().validity() && (mechaPane == null || mechaCard.current().validity());
		}



		MetaQueryable getTarget(){
			return target;
		}



		/**
		 * @return The chosen direction of the target link, null if thetarget is MetaClass
		 */
		Direction getDirection(){
			return direction;
		}



		@Override
		public void nextValidityChanged(ValidityToggle wizardNode) {
			targetListener.nextValidityChanged(this);
		}




		@Override
		public void removeValidityListener(ValidityListener l) {
			if(l == targetListener)
				targetListener = null;
		}



		@Override
		public MetaQueryable getData() {
			return target;
		}



		@Override
		public JLabel getEvalLabel() {
			return eval;
		}

		@Override
		public ExplicitPredicate<?> getPredicate() {
			return mf;
		}


		@Override
		public void setPredicate(ExplicitPredicate<?> p) {
			mf = p;
			toggle.toggle(this, this);
		}

		@Override
		public Decision getDecision() {
			return d;
		}








		public void copy(TargetCrumb other) {

			this.target = other.target;
			if(target!=null){
				if((target instanceof MetaClass))
					typeIcon.setIcon(MetaToIcon.get(target, access.dataRegistry(), theme, 32));
				else
					typeIcon.setIcon(theme.icon(DataToIconID.get(Direction.BOTH), 24));

				nameLabel.setText(target.name());
			}

			this.mf = other.mf;
			this.d = other.d;
			this.toggle = other.toggle;
			this.eval.setIcon(other.eval.getIcon());
			this.eval.setText(other.eval.getText());
			this.group.setText(other.group.getText());
			this.group.setIcon(other.group.getIcon());

		}



		@Override
		public boolean allowsDecisionToggle() {
			return toggle.allowsDecisionToggle(this, this);
		}



		@Override
		public void toggleDecision() {
			d = toggle.toggle(this, this);
			String tt = d.name();
			if(allowsDecisionToggle())
				tt+=" right click for options ";
			TooltipManager.setTooltip(eval, tt, TooltipWay.left, 0);
			eval.setIcon(theme.icon(DataToIconID.get(d), 24));
		}






		/**
		 * @return The default decision in the path if no intermediate evaluations are required
		 */
		public Decision getDefaultDecision() {
			if(target instanceof MetaLink && config.getPathMode() == PathMode.TARGET)
				return Decision.INCLUDE_AND_CONTINUE;
			else
				return Decision.EXCLUDE_AND_CONTINUE;
		}

	}






	// *****************************************************   Path Crumb   ***********************************************************




	class PathCrumb extends WebBreadcrumbPanel implements EvalContainer, Splittable{

		private final MetaQueryable mq;
		private ExplicitPredicate<?> mf;
		private Decision d;
		private final Toggle toggle;
		private JLabel eval;
		private Direction dir; 
		private JLabel split;


		PathCrumb(MetaClass mc, TargetCrumb tc) {

			//ref class
			mq = mc;


			//Determine toggle type
			if(config.getPathMode() == PathMode.PATH){
				Predicate p = config.getAllowedQueryables();
				if(p == null | (p!=null && p.test(mq))){
					toggle = Toggle.PathMCPathInclusion;
				}
				else
					toggle = Toggle.PathMCPathExclusion;

				//Init can be different from default
				d = Decision.EXCLUDE_AND_CONTINUE;
			}
			else{
				toggle = Toggle.PathMCTarget;
				d = toggle.getDefault(tc);
			}



			//Add the Node icon
			JLabel typeIcon = new JLabel(DataToIconID.getIcon(theme, mc.itemClass(access.dataRegistry()), 32));
			typeIcon.setBorder(new EmptyBorder(25,5,10,5));	
			typeIcon.setAlignmentX(Component.CENTER_ALIGNMENT);

			//Set the name
			JLabel name = buildNameLabel();
			name.setAlignmentX(Component.CENTER_ALIGNMENT);






			//Allow to add evaluations
			eval = new JLabel("No filter ");
			eval.setBorder(new EmptyBorder(5,5,5,5));
			eval.setIcon(theme.icon(DataToIconID.get(d), 24));
			eval.setHorizontalTextPosition(SwingConstants.LEFT);
			eval.addMouseListener(buildEvalListener(this));
			eval.setAlignmentX(Component.CENTER_ALIGNMENT);


			split = new JLabel(theme.icon(IconID.Queries.NO_SPLIT, 32));
			split.setBorder(new EmptyBorder(5,5,5,5));
			split.setAlignmentX(Component.CENTER_ALIGNMENT);

			if(!config.isPathEnabled())
				split.setEnabled(false);
			else{
				TooltipManager.addTooltip(split, "Splitting: right click for options",TooltipWay.down,0);
				split.addMouseListener(new MouseAdapter(){
					@Override
					public void mouseClicked(MouseEvent e){
						if(SwingUtilities.isRightMouseButton(e)){
							JPopupMenu menu = new JPopupMenu();
							if(lastSplitted != PathCrumb.this){
								JMenuItem addItem = new JMenuItem("Split here");
								addItem.addActionListener(l->{
									pathSplitCard.show(mq);
									pathSplitPane.expand();
									split.setIcon(theme.icon(IconID.Queries.SPLIT, 32));
									if(lastSplitted !=null)
										lastSplitted.getSplitLabel().setIcon(theme.icon(IconID.Queries.NO_SPLIT, 32));
									lastSplitted = PathCrumb.this;
								});
								menu.add(addItem);
							}
							else{
								JMenuItem rmItem = new JMenuItem("Remove");
								rmItem.addActionListener(l->{
									split.setIcon(theme.icon(IconID.Queries.NO_SPLIT, 32));
									lastSplitted = null;
								});
								menu.add(rmItem);
							}
							menu.show(split, e.getX(), e.getY());
						}
					}
				});
			}

			setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));//new VerticalFlowLayout());

			add(typeIcon);
			add(name);
			add(eval);
			add(split);



		}







		public PathCrumb(MetaLink mc, Direction dir, TargetCrumb tc) {

			//ref type			
			mq = mc;
			this.dir = dir;

			// Determine toggle type
			if(config.getPathMode() == PathMode.PATH)
				toggle = Toggle.PathLinkPath;
			else
				toggle = Toggle.PathLinkTarget;

			d = toggle.getDefault(tc);

			JLabel typeIcon = new JLabel(theme.icon(DataToIconID.get(dir), 32));
			typeIcon.setBorder(new EmptyBorder(25,5,10,5));
			typeIcon.setAlignmentX(Component.CENTER_ALIGNMENT);

			JLabel name = buildNameLabel();
			name.setAlignmentX(Component.CENTER_ALIGNMENT);

			eval = new JLabel("No filter : ");
			eval.setHorizontalAlignment(SwingConstants.LEFT);
			eval.setBorder(new EmptyBorder(5,5,5,5));
			eval.setIcon(theme.icon(DataToIconID.get(d), 24));
			eval.setHorizontalTextPosition(SwingConstants.LEFT);
			eval.setAlignmentX(Component.CENTER_ALIGNMENT);
			TooltipManager.addTooltip(eval, d.name(), TooltipWay.left, 0);			


			split = new JLabel(theme.icon(IconID.Queries.NO_SPLIT, 32));
			split.setEnabled(false);
			split.setBorder(new EmptyBorder(5,5,5,5));
			split.setAlignmentX(Component.CENTER_ALIGNMENT);
			TooltipManager.addTooltip(split, "<HTML>No split selected"
					+ "<br>(Cannot be changed for links)</br></HTML>",TooltipWay.down,0);

			setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
			add(typeIcon);
			add(name);
			add(eval);
			add(split);
		}



		private JLabel buildNameLabel() {
			JLabel name = new JLabel(mq.name());
			name.setBorder(new EmptyBorder(5,5,5,5));
			TooltipManager.addTooltip(name, "Right click to show options", TooltipWay.left,0);
			name.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent e){
					if(SwingUtilities.isRightMouseButton(e)){
						JPopupMenu menu = new JPopupMenu();
						JMenuItem excItem = new JMenuItem("Remove from Path");
						excItem.addActionListener(l->{
							exclusions.addElement(PathCrumb.this);
							updatePath();
						});
						menu.add(excItem);
						JMenuItem leaveItem = new JMenuItem("Leave in place");
						menu.add(leaveItem);

						menu.show(name, e.getX(), e.getY());

					}
				}
			});
			return name;
		}






		@Override
		public MetaQueryable getData() {
			return mq;
		}



		@Override
		public JLabel getEvalLabel() {
			return eval;
		}

		@Override
		public ExplicitPredicate<?> getPredicate() {
			return mf;
		}


		@Override
		public void setPredicate(ExplicitPredicate<?> p) {
			mf = p;
			toggle.toggle(this, target);
		}

		@Override
		public Decision getDecision() {
			return d;
		}




		public Direction getDirection() {
			return dir;
		}







		@Override
		public JLabel getSplitLabel() {
			return split;
		}







		public void copy(PathCrumb other) {


			this.mf = other.mf;
			this.d = other.d;
			this.eval.setText(other.eval.getText());
			this.eval.setIcon(other.eval.getIcon());
			this.split.setText(other.split.getText());
			this.split.setIcon(other.split.getIcon());


		}







		@Override
		public boolean allowsDecisionToggle() {
			return toggle.allowsDecisionToggle(this, target);
		}







		@Override
		public void toggleDecision() {
			d = toggle.toggle(this, target);
			String tt = d.name();
			if(allowsDecisionToggle())
				tt+=" right click for options ";
			TooltipManager.setTooltip(eval, tt, TooltipWay.left, 0);
			eval.setIcon(theme.icon(DataToIconID.get(d), 24));
		}







		public Decision getDefaultDecision() {
			return toggle.getDefault(target);
		}


	}









	private MouseListener buildEvalListener(EvalContainer c) {

		return new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){

				if(c.getData() == null){
					JOptionPane.showMessageDialog(c.getEvalLabel(), "Please Choose a type first");
					return;
				}

				if(SwingUtilities.isLeftMouseButton(e)){
					choices.highlightFilter(c.getData());
					filterCard.show(c.getData());
					filteringPane.expand();						
				}
				else if(SwingUtilities.isRightMouseButton(e)){

					choices.highlightFilter(c.getData());
					filterCard.show(c.getData());

					JPopupMenu menu = new JPopupMenu();
					if(c.getPredicate()!=null){
						JMenuItem rItem = new JMenuItem("Remove Filter");
						rItem.addActionListener(l->{
							c.getEvalLabel().setText("No Filter ");
							c.setPredicate(null);
							c.toggleDecision();
							c.getEvalLabel().setIcon(theme.icon(DataToIconID.get(c.getDecision()), 24));

						});
						menu.add(rItem);
					}


					JMenuItem listItem = new JMenuItem("Choose from the filter list");
					listItem.addActionListener(l->{
						Optional<MetaFilter> sel = choices.getCurrentFilter(c.getData());
						if(sel.isPresent()){
							c.setPredicate(sel.get().toFilter());
							c.toggleDecision();
							c.getEvalLabel().setText(sel.get().name());
							c.getEvalLabel().setIcon(theme.icon(DataToIconID.get(c.getDecision()), 24));
						}
						else
							JOptionPane.showMessageDialog(null, "No Selection detected");
					});
					menu.add(listItem);


					JMenuItem combItem = new JMenuItem("Choose from the Filter Combination dialog");
					combItem.addActionListener(l->{
						Optional<ExplicitPredicate> sel = (Optional)filterCard.current().getPredicate();
						if(sel.isPresent()){
							c.setPredicate(sel.get());
							c.getEvalLabel().setText("Filter Combination");
							c.toggleDecision();
							c.getEvalLabel().setIcon(theme.icon(DataToIconID.get(c.getDecision()), 24));
						}
						else
							JOptionPane.showMessageDialog(null, "Please define a valid combination of filters first");
					});
					menu.add(combItem);



					if(c.allowsDecisionToggle()){
						JMenuItem eItem = new JMenuItem("Toggle Decision");
						menu.add(eItem);
						eItem.addActionListener(l->{
							c.toggleDecision();
						});
					}


					menu.show(c.getEvalLabel(), e.getX(), e.getY());

				}

			}


		};
	}




	private Icon getDirectionIcon(Direction dir) {
		return UITheme.resize(theme.icon(DataToIconID.get(dir), 24), 24, 64);
	}






	public MetaQueryable getTarget() {
		return target.getTarget();
	}


	public MetaQueryable getRoot() {
		return root.getRoot();
	}




	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MetaTraversal getMetaTraversal(Meta meta) {

		TraversalDefinition td = null;


		// ====================== Predicate on root ======================================= 

		LinkDecisions<LastLinkEvaluation<TraversalDefinition>> linkDef = 
				TraversalDefinition.newDefinition().startFrom(root.getRoot().itemDeclaredType())
				.acceptedBy(P.isDeclaredType(root.getRoot().itemDeclaredType()).merge(Op.Bool.AND, root.getPredicate()))
				.fromDepth(getFirstIncludedNode(true)).toDepth(getLastIncludedNode(true)); 




		// ====================== Setting up Included Links ======================================= 

		IncludeLinksBased<LastLinkEvaluation<TraversalDefinition>> inclLinks = null;

		Iterator<PathCrumb> linkCrumbs = crumbs.stream().filter(pc->pc.getData() instanceof MetaLink).iterator();

		if(linkCrumbs.hasNext()){
			PathCrumb pc1 = linkCrumbs.next();		
			inclLinks = linkDef.traverseLink(pc1.getData().itemDeclaredType(), pc1.getDirection());


			while(linkCrumbs.hasNext()){
				pc1 = linkCrumbs.next();
				if(pc1.getData() instanceof MetaLink){
					inclLinks.and(pc1.getData().itemDeclaredType(), pc1.getDirection());
				}
			}
		}

		//If target is a MetaLink then add!
		if(target.getData() instanceof MetaLink)
			if(inclLinks == null)
				inclLinks = linkDef.traverseLink(target.getData().itemDeclaredType(), Direction.BOTH);
			else
				inclLinks.and(target.getData().itemDeclaredType(), Direction.BOTH);





		// ====================== Setting up Nodes Evaluations ====================================

		//Check if there are filters among nodes
		List<PathCrumb> evals = 
				crumbs.stream().filter(pc->pc.getData() instanceof MetaClass)
				.filter(pc -> pc.getPredicate() != null || pc.getDecision() != pc.getDefaultDecision()).collect(Collectors.toList());





		if(evals.isEmpty()){

			if(target.getData() instanceof MetaLink){
				if(target.getPredicate() != null)
					td = inclLinks.includeAllNodes().finallyEvaluateLastLink(target.getData().itemDeclaredType(), (ExplicitPredicate) target.getPredicate(), target.getDecision(), Decision.INCLUDE_AND_STOP);
				else
					td = inclLinks.includeAllNodes().build();		
			}

			// Exclude and stop no filter
			else if(target.getPredicate() == null)
				td = inclLinks.withOneEvaluation(P.isDeclaredType(((MetaClass) target.getData()).itemDeclaredType()),
						target.getDecision(), target.getDefaultDecision()).build();

			else
				td = inclLinks.withOneEvaluation(
						P.isDeclaredType(((MetaClass) target.getData()).itemDeclaredType()).merge(Op.Bool.AND, (ExplicitPredicate<? super DataItem>)target.getPredicate()),
						target.getDecision(), target.getDefaultDecision()).build();
		}

		else{

			System.out.println("CreatePathPanel : Evaluations found");



			EvaluationContainerBuilder<LastLinkEvaluation<TraversalDefinition>> ev = null;
			Iterator<PathCrumb> evalsIt = evals.iterator();
			PathCrumb pc = evalsIt.next();

			if(pc.getPredicate() == null){
				ev = inclLinks.withEvaluations(P.isDeclaredType(((MetaClass) pc.getData()).itemDeclaredType()), pc.getDecision(),  pc.getDefaultDecision());
			}
			else{
				ev = inclLinks.withEvaluations(
						P.isDeclaredType(((MetaClass) pc.getData()).itemDeclaredType()).merge(Op.Bool.AND, (ExplicitPredicate)pc.getPredicate()),
						pc.getDecision(), pc.getDefaultDecision());
			}


			while(evalsIt.hasNext()){
				pc = evalsIt.next();
				if(pc.getPredicate() == null){
					ev.addEvaluation(P.isDeclaredType(((MetaClass) pc.getData()).itemDeclaredType()), pc.getDecision());
				}
				else{
					ev.addEvaluation(
							P.isDeclaredType(((MetaClass) pc.getData()).itemDeclaredType()).merge(Op.Bool.AND, (ExplicitPredicate)pc.getPredicate()),
							pc.getDecision());
				}
			}




			if(target.getData() instanceof MetaLink){
				if(target.getPredicate() != null){

					td = ev.done().finallyEvaluateLastLink(target.getData().itemDeclaredType(),(ExplicitPredicate) target.getPredicate(), target.getDecision(), Decision.INCLUDE_AND_STOP);

				}else
					td = ev.done().build();				
			}

			else if(target.getPredicate() == null)
				td = ev.lastEvaluation(P.isDeclaredType(((MetaClass) target.getData()).itemDeclaredType()), target.getDecision()).build();
			else
				td = ev.lastEvaluation((ExplicitPredicate<? super NodeItem>)target.getPredicate(), target.getDecision()).build();


		}


		//Set Policy on multiple passes 
		if(lastSplitted != null && pathSplitCard.current().selectedId() == SplitID.Depth)
			td.setTraversalPolicy(TraversalPolicy.NONE);
		else
			td.setTraversalPolicy(TraversalPolicy.LINKS_ONCE);

		System.out.println("CreatePathPanel : Traversal Policy -> "+td.traversalPolicy());
		System.out.println("CreatePathPanel TD : "+td.description()); 


		try {
			return MetaTraversal.getOrCreate(meta.getInstanceManager(), root.root.name()+"=>"+target.target.name(),td.description(),td,target.getData()); 
		} catch (NonDataMappingException e) {
			ErrorInfo error = 
					new ErrorInfo(							
							"Custom Annotation Error", 
							"Unable to create the custom filter",
							null, null,
							e,
							Level.SEVERE,
							null);
			JXErrorPane.showDialog(null, error);
			return null;
		}

	}








	public Optional<MetaSplit> getSplitting(Meta meta){

		if(lastSplitted == null)
			return Optional.empty();

		return Optional.of(pathSplitCard.current().getSplitter(meta));

	}



	public Optional<MetaSplit> getGrouping(Meta meta) {		
		return Optional.ofNullable(groupingCard.current().getSplitter(meta));
	}



	public Optional<MetaReductionOperation> getMechanism() {
		return Optional.ofNullable(mechaCard.current().getMechanism(access.metaModel()));
	}





	public List<MetaQueryable> getIncluded() {
		List<MetaQueryable> l = 
				crumbs.stream().filter(pc->
				config.getAllowedQueryables().test(pc.getData()) 
				&& (
						(pc.getDefaultDecision() == Decision.EXCLUDE_AND_CONTINUE && (pc.getDecision() == Decision.INCLUDE_AND_CONTINUE || pc.getDecision() == Decision.INCLUDE_AND_STOP))
						||
						(pc.getDefaultDecision() == Decision.INCLUDE_AND_CONTINUE && pc.getDecision() != Decision.EXCLUDE_AND_CONTINUE )
						)
						)
				.map(pc->pc.getData()).collect(Collectors.toList());
		if(this.target.getData() instanceof MetaClass)
			l.add(this.target.getData());
		return l;
	}





	public ExplicitPredicate<? super NodeItem> getRootInstance() throws DataAccessException {
		return (ExplicitPredicate<? super NodeItem>) root.getPredicate();
	}





	@Override
	public void copyState(CreatePathPanel<D> other) {


		for(int e = 0; e<other.exclusions.size(); e++){
			this.exclusions.addElement(other.exclusions.getElementAt(e));
		}

		this.root.copy(other.root);
		this.target.copy(other.target);

		filterCard.show(other.filterCard.currentKey());
		filterCard.current().copyState(other.filterCard.current());
		groupingCard.show(other.groupingCard.currentKey());
		groupingCard.current().copyState(other.groupingCard.current());

		updatePath();

		for(int i = 0; i<other.crumbs.size(); i++){
			this.crumbs.get(i).copy(other.crumbs.get(i));
		}

		this.lastSplitted = other.lastSplitted;


		if(lastSplitted != null){
			pathSplitCard.show(lastSplitted.getData());
			pathSplitCard.current().copyState(other.pathSplitCard.current());
		}


		if(other.filteringPane.isExpanded())
			this.filteringPane.expand();
		if(other.groupingPane.isExpanded())
			this.groupingPane.expand();
		if(other.pathSplitPane.isExpanded())
			this.pathSplitPane.expand();


	}





}

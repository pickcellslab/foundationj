package org.pickcellslab.foundationj.ui.parsers;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;

import com.fathzer.soft.javaluator.AbstractEvaluator;
import com.fathzer.soft.javaluator.BracketPair;
import com.fathzer.soft.javaluator.Operator;
import com.fathzer.soft.javaluator.Parameters;
import com.fathzer.soft.javaluator.StaticVariableSet;

public class ExplicitPredicateParser<E> extends AbstractEvaluator<ExplicitPredicate<E>> {

	/** The logical NOT operator. */
	public final static Operator NOT = new Operator("NOT", 1, Operator.Associativity.RIGHT, 5);
	/** The logical AND operator. */
	private static final Operator AND = new Operator("AND", 2, Operator.Associativity.LEFT, 2);
	/** The logical OR operator. */
	public final static Operator OR = new Operator("OR", 2, Operator.Associativity.LEFT, 3);
	/** The logical exclusive XOR operator. */
	public final static Operator XOR = new Operator("XOR", 2, Operator.Associativity.LEFT, 4);

	private static final Parameters PARAMETERS;

	static {
		// Create the evaluator's parameters
		PARAMETERS = new Parameters();
		// Add the supported operators
		PARAMETERS.add(NOT);
		PARAMETERS.add(AND);
		PARAMETERS.add(OR);
		PARAMETERS.add(XOR);
		// Add the default parenthesis pair
		PARAMETERS.addExpressionBracket(BracketPair.BRACES);
	}

	//A map of the symbols in the expression and variables associated with them
	private final StaticVariableSet<ExplicitPredicate<E>> variables;


	public ExplicitPredicateParser(StaticVariableSet<ExplicitPredicate<E>> variables) {
		super(PARAMETERS);
		Objects.requireNonNull(variables);
		this.variables = variables;
	}

	@Override
	protected ExplicitPredicate<E> toValue(String literal, Object evaluationContext) {	
		return variables.get(literal);		
	}

	@Override
	protected ExplicitPredicate<E> evaluate(Operator operator,
			Iterator<ExplicitPredicate<E>> operands, Object evaluationContext) {

		ExplicitPredicate<E> t1 = operands.next();		



		if (operator == OR){
			ExplicitPredicate<E> t2 = operands.next();
			return t1.merge(Op.Bool.OR, t2);	
		}
		else if(operator == AND){
			ExplicitPredicate<E> t2 = operands.next();
			return t1.merge(Op.Bool.AND, t2);
		}
		else if(operator == XOR){
			ExplicitPredicate<E> t2 = operands.next();
			return t1.merge(Op.Bool.XOR, t2);
		}
		else if(operator == NOT)
			return t1.negate();
		else 
			return super.evaluate(operator, operands, evaluationContext);

	}


	public ExplicitPredicate<E> getPredicate(String expression) throws IllegalArgumentException{
		return super.evaluate(expression);
	}


}

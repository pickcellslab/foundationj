package org.pickcellslab.foundationj.dbm.meta;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.access.RegenerableDataPointer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.AllOrSome;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludeExcludeKeyInit;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapping;



@Data(typeId = "MetaTraversal", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaTraversal extends MetaItem implements MetaStrategy, RegenerableDataPointer, MetaAutoDeletable{

	private static final AKey<String> traversalKey = AKey.get("Traversal", String.class);
	

	private MetaTraversal(String uid){
		super(uid);
	}

	public static MetaTraversal getOrCreate(
			MetaInstanceDirector director,
			String name,
			String description,
			TraversalDefinition td,
			MetaQueryable target) throws NonDataMappingException{

		

			//System.out.println( "MetaTraversal: Creating...");

			MetaTraversal mt;
			try {
				mt = director.getOrCreate(MetaTraversal.class, "MetaTraversal_" + UUID.randomUUID().getMostSignificantBits());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			if(!mt.getAttribute(MetaItem.name).isPresent()){// Check if it was retrieved or if it already existed


				mt.setAttribute(MetaItem.name, name);
				mt.setAttribute(MetaItem.desc, description);
				mt.setAttribute(traversalKey, NonDataMapping.convertToString(td));

				new DataLink(POINTS_TO, mt, target, true);
				new DataLink(MetaModel.requiresLink, mt, target, true);

			}

			return mt;

		

	}

	
	
	
	
	
	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc, traversalKey);
	}
	
	
	
	
	



	public TraversalDefinition toTraversal() {
		try {
			return (TraversalDefinition) 
					NonDataMapping.restoreFromString(getAttribute(traversalKey).get());
		} catch (NonDataMappingException e) {
			throw new RuntimeException("The "+AKey.get("Traversal", String.class).toString()+
					" property of this object has been modified, Unable to rebuild the traversal", e);
		}
	}




	@Override
	public MetaQueryable getTarget(){
		return (MetaQueryable) typeMap.get(POINTS_TO).iterator().next().target();
	}

	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>Traversal Dataset</strong>"+
				"<p><ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"</li></p></HTML>";
	}

	/*
	@Override
	public TraversalDefinition deletionDefinition() {
		//Delete MetaDataset, MetaSplitTraversal or MetaTraversingFunction which may depend on this MetaTraversal

		return TraversalDefinition.newDefinition()
				.startFrom(DataRegistry.typeIdFor(this.getClass())).having(MetaModel.recognition, recognitionString())
				.fromDepth(0).toDepth(1)
				.traverseLink(MetaSplitTraversal.definition, Direction.INCOMING)
				.and(MetaDataSet.STRATEGY, Direction.INCOMING)
				.includeAllNodes().build();
	}

	
	@Override
	public TraverserConstraints storageConstraints() {
		return Traversers.newConstraints().fromDepth(0).toDepth(1)
				.traverseLink(MetaQueryable.TARGET_OF, Direction.INCOMING)
				.and(MetaModel.requiresLink, Direction.OUTGOING)
				.includeAllNodes();
	}
	*/

	@Override
	public int deleteData(DataAccess access) throws DataAccessException {
		return getTarget().initiateDeletion(access).completely().useTraversal(this.toTraversal()).run();
	}

	
	@Override
	public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
		final TraversalDefinition td = toTraversal().subsetRoots(F.select(DataItem.idKey, -1).equalsTo(dbId));
		return getTarget().initiateFindOne(access).using(action).useTraversal(td).run();
	}


	@Override
	public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
		return getTarget().initiateFindOne(access).using(action).useTraversal(this.toTraversal()).run();
	}
	
	
	@Override
	public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
		return getTarget().initiateRead(access, limit).with(action).inOneSet().useTraversal(this.toTraversal()).run();
	}

	@Override
	public void updateData(DataAccess access, Updater action) throws DataAccessException {
		getTarget().initiateUpdate(access, action).useTraversal(this.toTraversal()).run();
	}

	@Override
	public List<MetaQueryable> queriedMetaObjects() {
		return Collections.singletonList(getTarget());
	}

	
	
	@Override
	public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> initRegeneration(
			DataAccess access) {
		Objects.requireNonNull(access);
		return access.queryFactory().regenerate(this.getTarget().itemDeclaredType());
	}



	@Override
	public RegeneratedItems finaliseRegeneration(AccessChoice<NodeItem, QueryRunner<RegeneratedItems>> choice)
			throws DataAccessException {
		Objects.requireNonNull(choice);
		return choice.useTraversal(toTraversal()).run();
	}


	
	
	
	@Override
	public RegenerableDataPointer subset(ExplicitPredicate<DataItem> predicate) {
		
		Objects.requireNonNull(predicate);
		final TraversalDefinition td = toTraversal().subsetRoots(predicate);
		
		return new RegenerableDataPointer() {

			@Override
			public int deleteData(DataAccess access) throws DataAccessException {
				return getTarget().initiateDeletion(access).completely().useTraversal(td).run();
			}
			
			
			@Override
			public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
				final TraversalDefinition td2 = td.subsetRoots(F.select(DataItem.idKey, -1).equalsTo(dbId));
				return getTarget().initiateFindOne(access).using(action).useTraversal(td2).run();
			}
			

			@Override
			public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
				return getTarget().initiateRead(access, limit).with(action).inOneSet().useTraversal(td).run();
			}


			@Override
			public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
				return getTarget().initiateFindOne(access).using(action).useTraversal(td).run();
			}

			@Override
			public void updateData(DataAccess access, Updater action) throws DataAccessException {
				getTarget().initiateUpdate(access, action).useTraversal(td).run();
			}

			@Override
			public List<MetaQueryable> queriedMetaObjects() {
				return Collections.singletonList(getTarget());
			}

			
			
			@Override
			public ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> initRegeneration(
					DataAccess access) {
				Objects.requireNonNull(access);
				return access.queryFactory().regenerate(MetaTraversal.this.getTarget().itemDeclaredType());
			}



			@Override
			public RegeneratedItems finaliseRegeneration(AccessChoice<NodeItem, QueryRunner<RegeneratedItems>> choice)
					throws DataAccessException {
				Objects.requireNonNull(choice);
				return choice.useTraversal(toTraversal()).run();
			}

			@Override
			public RegenerableDataPointer subset(ExplicitPredicate<DataItem> predicate2) {
				Objects.requireNonNull(predicate2);
				return MetaTraversal.this.subset(predicate.and(predicate2));
			}
			
		};
	}




}

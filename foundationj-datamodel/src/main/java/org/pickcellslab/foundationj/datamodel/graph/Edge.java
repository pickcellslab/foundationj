package org.pickcellslab.foundationj.datamodel.graph;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * The root interface for Edges in a graph
 * 
 * @author Guillaume Blin
 *
 * @param <N> The type of the source {@link Node} used by this Edge
 */
public interface Edge<N extends Node<? extends Edge<N>>> {

	/**
	 * @return the source of this link
	 */
	public N source();

	/**
	 * @return the target of this link
	 */
	public N target();

	/**
	 * Deletes this Link. Both the source and target will have their reference updated and this Link 
	 * will return null for source target and type;
	 */
	public void delete();
	
}

package org.pickcellslab.foundationj.dataviews.fitting;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

/**
 * A wrapper class used as a returned value of fitting processes.
 * It contains the actual fit as well as a unique id and a description
 * about this particular fit.
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 */
public class DefaultFitResult<T> implements FitResult<T> {

	private final String id;
	private final String description;
	private final T fit;
	private final Dimension<?,?>[] dims;
	
	public DefaultFitResult(T fit, String description, String id, Dimension<?,?> dimensions) {
		this.fit = fit;
		this.description = description;
		this.dims = new Dimension[]{dimensions};
		this.id = id;
	}

	public DefaultFitResult(T fit, String description, String id, Dimension<?,?>[] dimensions) {
		this.fit = fit;
		this.description = description;
		this.dims = dimensions;
		this.id = id;
	}
	
	/* (non-Javadoc)
	 * @see net.pickcellslab.foundationJ.dataviews.fitting.FittResult#getId()
	 */
	@Override
	public String getId() {
		return id; 
	}
	
	/* (non-Javadoc)
	 * @see net.pickcellslab.foundationJ.dataviews.fitting.FittResult#getFit()
	 */
	@Override
	public T getFit() {
		return fit;
	}

	/* (non-Javadoc)
	 * @see net.pickcellslab.foundationJ.dataviews.fitting.FittResult#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}
	
	/* (non-Javadoc)
	 * @see net.pickcellslab.foundationJ.dataviews.fitting.FittResult#numDimensions()
	 */
	@Override
	public int numDimensions(){
		return dims.length;
	}
	
	/* (non-Javadoc)
	 * @see net.pickcellslab.foundationJ.dataviews.fitting.FittResult#getDimension(int)
	 */
	@Override
	public Dimension<?,?> getDimension(int i){
		return dims[i];
	}
	
}

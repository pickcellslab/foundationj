package org.pickcellslab.foundationj.dbm.access;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.LinkDecisions;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.ToDepth;
import org.pickcellslab.foundationj.dbm.db.DatabaseNode;
import org.pickcellslab.foundationj.dbm.db.DatabaseNodeDeletable;
import org.pickcellslab.foundationj.dbm.db.UpdatableLink;
import org.pickcellslab.foundationj.dbm.db.UpdatableNode;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.impl.SchemaQueryBuilderImpl;
import org.pickcellslab.foundationj.dbm.meta.ManuallyStored;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;
import org.pickcellslab.foundationj.dbm.queries.ReadAnyBuilder;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.AllOrSome;
import org.pickcellslab.foundationj.dbm.queries.builders.DeletionConfig;
import org.pickcellslab.foundationj.dbm.queries.builders.IncludeExcludeKeyInit;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadItemsBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadOneBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.SchemaQueryBuilder;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;

/**
 * 
 * A Factory for query builders dedicated to generate Create / Read / Update / Delete operations on the graph database.
 * <br>NB: When modifications on the data graph are detected, the {@link MetaModel} is automatically updated to reflect the changes.
 * Notifications of such changes can be obtained by registering a {@link MetaModelListener} to the {@link DataAccess} this {@link QueryFactory}
 * was obtained from.
 * 
 * @author Guillaume Blin
 *
 */
public interface QueryFactory {

	/**
	 * Creates a Builder to define writes to the database. The Builder will take {@link WritableDataItem} as inputs,
	 * NB: Once the query is committed, the DataItem entered are guaranteed to possess a  database id ({@link WritableDataItem#idKey})), it
	 * will be unchanged if the item already possessed an id, it will be a newly generated one if not.
	 * @return A dedicated builder
	 * 
	 */
	StorageBoxBuilder store();

	/**
	 * Creates a new Query to store {@link ManuallyStored} objects
	 * @param item
	 * @return The query to store the provided item
	 */
	MetaBox meta(ManuallyStored item);

	
	/**
	 * Initiate a query to read one specific Node from the database with the given database id.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @param dbId The database Id of the object to read.
	 * @return A dedicated {@link ReadOneBuilder}
	 */
	ReadOneBuilder<DatabaseNode> readOneNode(String label, int dbId);
	
	ReadAnyBuilder<DatabaseNode> readAnyNode(String label);

	/**
	 * Initiate a query to read one specific Link from the database with the given database id.
	 * @param linkType The type of link to be read
	 * @param dbId The database Id of the Link to read.
	 * @return A dedicated {@link ReadOneBuilder}
	 */
	ReadOneBuilder<Link> readOneLink(String linkType, int dbId);
	
	ReadAnyBuilder<Link> readAnyLink(String linkType);
	

	ReadAnyBuilder<Link> readAnyLink(String linkType, String srcType, String tgType);

	/**
	 * Start building a Query which will return {@link Path}(s)
	 * @param name A name for this query.
	 * @return A {@link ReadBuilder} dedicated to build the query
	 */
	ReadBuilder<Path<NodeItem, Link>> readPaths(String name);

	/**
	 * @deprecated use {@link #read(String)} instead
	 */
	ReadItemsBuilder<DatabaseNode> read(Class<? extends NodeItem> clazz);

	/**
	 * Initiate a query to read Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @return A dedicated {@link ReadItemsBuilder}
	 */
	ReadItemsBuilder<DatabaseNode> read(String label);

	/**
	 * Initiate a query to read {@link Link} objects from the database.
	 * @param label The label to use for the search
	 * @return A dedicated {@link ReadItemsBuilder}
	 */
	ReadItemsBuilder<Link> readLinks(String linkType);

	/**
	 * @deprecated use {@link #read(String, long)} instead
	 */
	ReadItemsBuilder<DatabaseNode> read(Class<? extends NodeItem> clazz, long limit);

	/**
	 * Initiate a query to read Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @param long limit The max number of objects to read or -1 for no limit
	 * @return A dedicated {@link ReadItemsBuilder}
	 */
	ReadItemsBuilder<DatabaseNode> read(String label, long limit);

	
	/**
	 * Initiate a query to read {@link Link} objects from the database.
	 * @param label The label to use for the search
	 * @param long limit The max number of objects to read or -1 for no limit
	 * @return A dedicated {@link ReadItemsBuilder}
	 */
	ReadItemsBuilder<Link> readLinks(String linkType, long limit);

	/**
	 * Initiate a query to read {@link Link} objects from the database.
	 * @param label The label to use for the search
	 * @param srcType
	 * @param tgType
	 * @param long limit The max number of objects to read or -1 for no limit
	 * @return A dedicated {@link ReadItemsBuilder}
	 */
	ReadItemsBuilder<Link> readLinks(String linkType, String srcType, String tgType, long limit);

	/**
	 * Initiate a query to read {@link Link} objects from the database.
	 * @param mq The MetaQueryable defining which type of object to read
	 * @param long limit The max number of objects to read or -1 for no limit
	 * @return A dedicated {@link ReadItemsBuilder}
	 */
	ReadItemsBuilder<WritableDataItem> read(MetaQueryable mq, long limit);

	

	/**
	 * Initiate a query to update Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @param Updater The {@link Updater} responsible for the update of each encountered object}
	 * @return A dedicated {@link UpdateBuilder}
	 */
	AccessChoice<UpdatableNode, QueryRunner<Void>> update(String label, Updater action);

	

	AccessChoice<UpdatableLink, QueryRunner<Void>> updateLinks(String linkType, String srcType, String tgType, Updater action);


	/**
	 * @deprecated use {@link #delete(String)} instead
	 */
	DeletionConfig<DatabaseNodeDeletable> delete(Class<? extends NodeItem> clazz);

	/**
	 * Initiate a query to delete Node objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is strongly discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @return A dedicated {@link UpdateBuilder}
	 */
	DeletionConfig<DatabaseNodeDeletable> delete(String label);

	DeletionConfig<Link> deleteLinks(String linkType);

	DeletionConfig<Link> deleteLinks(String linkType, String srcType, String tgType);

	/**
	 * Deletes the provided {@link ManuallyStored} object from the database and objects which depends on it.
	 * @param s
	 * @throws DataAccessException
	 */
	void deleteAnnotation(ManuallyStored s) throws DataAccessException;

	/**
	 * @deprecated use {@link #regenerate(String)} instead
	 */
	ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> regenerate(
			Class<? extends NodeItem> nodeClass);

	/**
	 * Initiate a query to regenerate Node java objects from the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is strongly discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param tag The label to use for the search
	 * @return A dedicated {@link RegenerationBuilder}
	 */
	ToDepth<LinkDecisions<IncludeExcludeKeyInit<AllOrSome<RegeneratedItems, AccessChoice<NodeItem, QueryRunner<RegeneratedItems>>>>>> regenerate(
			String tag);

	/**
	 * @deprecated use {@link #schema(String)} instead
	 */
	SchemaQueryBuilder schema(Class<? extends NodeItem> clazz);

	/**
	 * Initiate a schema query on Nodes within the database.
	 * It is recommended to search objects using the {@link NodeItem#declaredType()} or any label that may have been added using
	 * {@link SchemaQueryBuilderImpl#addLabel(String)}. If querying by java type is desired, use {@link DataRegistry#typeIdFor(Class)} as
	 * input, however this is strongly discouraged unless one is absolutely certain that the declared type of all instances of this Class
	 * is the same and equal to {@link DataRegistry#typeIdFor(Class)}.
	 * @param label The label to use for the search
	 * @return A dedicated {@link SchemaQueryBuilderImpl}
	 */
	SchemaQueryBuilder schema(String label);

}
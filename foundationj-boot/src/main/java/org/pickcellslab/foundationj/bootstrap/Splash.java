package org.pickcellslab.foundationj.bootstrap;

/*-
 * #%L
 * foundationj-boot
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

public class Splash {

	private JFrame frame;
	private JProgressBar progressBar;
	private JLabel lblProgress;

	private int width = 500, height = 400;
	
	

	/**
	 * Create the frame.
	 */
	public Splash(File file) {
		
		frame = new JFrame();
		
		frame.setLocationByPlatform(true);
		frame.setUndecorated(true);
		frame.setResizable(false);
		
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setBounds(100, 100, width, height);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		JPanel panel = new ImagePanel(file);
		contentPane.add(panel);
		
		progressBar = new JProgressBar();
		progressBar.setForeground(Color.ORANGE);
		
		lblProgress = new JLabel("Progress");
		lblProgress.setForeground(Color.DARK_GRAY);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblProgress)
					.addContainerGap(388, Short.MAX_VALUE))
				.addComponent(progressBar, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE)
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap(256, Short.MAX_VALUE)
					.addComponent(lblProgress)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		);
		panel.setLayout(gl_panel);
		
		
	}
	
	/**
	 * Tells the ProgressBar to progress incrementally to the specified progress status
	 * @param progress The final progress status
	 * @param msec The time in milli seconds to wait between each increment
	 * @param step The increment for each step
	 */
	public void progressTo(int progress, long msec, int step){		
		new Thread(){
            public void run() {                
            	while(progressBar.getValue()<progress){
        			try {
        				Thread.sleep(msec);
        				progressBar.setValue(progressBar.getValue()+step);
        			} catch (InterruptedException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        		} 
            }
		}.start();
	}
	
	public void setProgress(String message){
		lblProgress.setText(message);
	}
	
	public void update(int i){
		progressBar.setValue(i);
	}
	
	public void error() {
		frame.setEnabled(false);		
	}
	
	
	public void close(){
		frame.dispose();
	}
	
	
	
	
	class ImagePanel extends JPanel {

		  /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private BufferedImage img;

		  

		  public ImagePanel(File file) {
			  	
				try {
					if(file != null)
					img = ImageIO.read(file);
				} catch (IOException e) {
					// Just ignore it means the file is either not an image or not a valid file
				}
			  
		    Dimension size = new Dimension(width, height);
		    setPreferredSize(size);
		    setMinimumSize(size);
		    setMaximumSize(size);
		    setSize(size);
		    setLayout(null);
		  }

		  public void paintComponent(Graphics g) {
		    g.drawImage(img, 0, 0, width, height, null);
		  }

		}

	
	public void show() {
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);		
	}

	
	
}

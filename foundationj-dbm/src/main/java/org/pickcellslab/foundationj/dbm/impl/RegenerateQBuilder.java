package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.impl.MonolithicSearchDefinition.Builder;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;

class RegenerateQBuilder<V,E> implements AccessChoice<NodeItem,QueryRunner<RegeneratedItems>>, QueryRunner<RegeneratedItems> {


	private TargetType<V, E, V> target;
	private ReconstructionRuleImpl rule;
	private ReconstructionAction<V,E> action;


	private final Builder<V,E,V> delegate;


	RegenerateQBuilder(TargetType<V,E,V> target, ReconstructionRuleImpl rule){
		this.target = target;
		delegate = new Builder<>(target);
		this.rule = rule;
	}

	
	/**
	 * The primary use for this package protected method is to allow MetaDeletions to reuse RecontructionQueries to both
	 * regenerate MetaData and delete them at them from the database at the same time.
	 * @param action
	 * @return
	 */
	RegenerateQBuilder<V,E> setAction(ReconstructionAction<V,E> action){
		this.action = action;
		return this;
	}
	

	@Override
	public QueryRunner<RegeneratedItems> getAll() {
		action = new ReconstructionAction<>(new UnfilteredReconstructionMechanism<V,E>(rule));
		return this;
	}

	
	@Override
	public QueryRunner<RegeneratedItems> useFilter(ExplicitPredicate<? super NodeItem> test) {
		Objects.requireNonNull(test,"The provided predicate cannot be null");
		action = new ReconstructionAction<>(new ReconstructionMechanismWithFilter<>(rule,test));
		return this;
	}
	


	@Override
	public QueryRunner<RegeneratedItems> useTraversal(TraversalDefinition definition) {
		assert definition != null;
		target.checkTraversalDefinition(definition);
		delegate.setTraversal(definition);
		return this;
	}

	@Override
	public QueryRunner<RegeneratedItems> useTraversals(SuppliedTraversalOperation operations) {
		delegate.setTraversalOperation(operations);
		return this;
	}

	@Override
	public RegeneratedItems run() throws DataAccessException {
		if(action == null)
			action = new ReconstructionAction<>(new UnfilteredReconstructionMechanism<>(rule));
		return target.session().runQuery(new RegenerationQuery<>(delegate.build(), action)).get();
	}


}

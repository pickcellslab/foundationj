package org.pickcellslab.foundationj.dbm.meta;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;




@Data(typeId = "MetaSplitTraversal", hook = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class MetaSplitTraversal extends MetaItem implements MetaStrategy, MetaAutoDeletable{

	private MetaSplitTraversal(String uid) {
		super(uid);
	}

	public static final String definition = "DEFINITION", splitter = "SPLITTER";

	public static MetaSplitTraversal getOrCreate(MetaInstanceDirector director, String name, MetaTraversal mt, MetaSplit split) throws InstantiationHookException {

		final MetaSplitTraversal st = director.getOrCreate(MetaSplitTraversal.class, MetaItem.randomUID(MetaSplitTraversal.class));
		if(!st.getAttribute(MetaModel.name).isPresent()){// Check if it was retrieved or if it already existed

			st.setAttribute(MetaItem.name, name);
			st.setAttribute(MetaItem.desc,"Splitted Traversal");
			new DataLink(definition, st, mt, true);
			new DataLink(MetaModel.requiresLink, st, mt, true);
			new DataLink(splitter, st, split, true);
			new DataLink(MetaModel.requiresLink, st, split, true);

		}

		return st;
	}

	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>Splitted Traversal</strong>"+
				"<p><ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Path description:"+metaTraversal().description()+
				"<li>Split description:"+metaSplit().description()+
				"</li></p></HTML>";
	}

	
	
	
	
	
	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc);
	}
	
	



	public MetaSplit metaSplit() {
		return (MetaSplit) typeMap.get(splitter).iterator().next().target();
	}

	public MetaTraversal metaTraversal() {
		return (MetaTraversal) typeMap.get(definition).iterator().next().target();
	}

	@Override
	public MetaQueryable getTarget() {
		return metaTraversal().getTarget();
	}
/*
	@Override
	public TraversalDefinition deletionDefinition() {
		//Remove the MetaDataset upstream
		return TraversalDefinition.newDefinition()
				.startFrom(this.getClass().getSimpleName()).having(MetaModel.recognition, recognitionString())
				.fromDepth(0).toDepth(1)
				.traverseLink(MetaDataSet.STRATEGY, Direction.INCOMING)
				.includeAllNodes().build();
	}

	@Override
	public TraverserConstraints storageConstraints() {
		// Navigate downstream until we get to our target		
		return Traversers.newConstraints().fromDepth(0).toDepth(3)
				.traverseLink(definition, Direction.OUTGOING)
				.and(splitter, Direction.OUTGOING)
				.and("BASED_ON", Direction.OUTGOING)
				.and(MetaModel.requiresLink, Direction.OUTGOING)
				.and(MetaQueryable.TARGET_OF, Direction.INCOMING)
				.includeAllNodes();
	}
*/




}

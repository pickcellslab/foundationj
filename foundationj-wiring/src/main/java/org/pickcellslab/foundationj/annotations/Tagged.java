package org.pickcellslab.foundationj.annotations;

import static java.lang.annotation.ElementType.PARAMETER;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Specifies that a given parameter must be typed with a class which is annotated with either @{@link Tag} or @{@link Data}
 * 
 * @author Guillaume Blin
 *
 */
@Documented
@Retention(RetentionPolicy.CLASS)
@Target(PARAMETER)
public @interface Tagged {

}

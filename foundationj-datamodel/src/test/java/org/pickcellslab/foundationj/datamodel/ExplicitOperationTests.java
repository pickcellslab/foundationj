package org.pickcellslab.foundationj.datamodel;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.builders.P;


public class ExplicitOperationTests {

	@Tag(creator = "Test", description = "Test", tag = "RootInterface")
	private interface RootTaggedInterface extends DataItem	{}
	
	@Tag(creator = "Test", description = "Test", tag = "Child")
	private interface ChildTaggedInterface extends RootTaggedInterface{}
	
	@Tag(creator = "Test", description = "Test", tag = "NotImpl")
	private interface NotImpl extends RootTaggedInterface{}
	
	@Data(typeId = "TaggedNode")
	@Scope
	private class TaggedNode extends DefaultNodeItem implements ChildTaggedInterface{

		@Override
		public Stream<AKey<?>> minimal() {
			return null;
		}
		
	}
	
	@Test
	public void testIsSubType() {
				
		final TaggedNode node = new TaggedNode();
		boolean result1 = P.isSubType(TaggedNode.class).test(node);
		Assertions.assertTrue(result1);
		
		boolean result2 = P.isSubType(ChildTaggedInterface.class).test(node);
		Assertions.assertTrue(result2);
		boolean result3 = P.isSubType(RootTaggedInterface.class).test(node);
		Assertions.assertTrue(result3);
		
		boolean result4 = P.isSubType(DefaultNodeItem.class).test(node);
		Assertions.assertTrue(result4);
		
		boolean result5 = P.isSubType(NotImpl.class).test(node);
		Assertions.assertTrue(!result5);
	}
	
}

package org.pickcellslab.foundationj.optim.staging;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.graph.Edge;

public class Flow implements Edge<Factory<Flow>> {

	
	private Factory<Flow> source;
	private Factory<Flow> target;

	public Flow(Factory<Flow> source, Factory<Flow> target){
		this.source = source;
		this.target = target;
		source.addLink(this);
		target.addLink(this);
	}
	
	

	@Override
	public void delete() {
		source.removeLink(this, true);
		target = null;
		source = null;		
	}

	@Override
	public Factory<Flow> source() {
		return source;
	}

	@Override
	public Factory<Flow> target() {
		return target;
	}

}

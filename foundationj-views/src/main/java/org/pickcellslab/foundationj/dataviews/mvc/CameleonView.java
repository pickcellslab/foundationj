package org.pickcellslab.foundationj.dataviews.mvc;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * An object (often a {@link DataView}) which allows to change the appearances of the series and their visibility
 * 
 * @author Guillaume Blin
 *
 * @param <A> The type of object holding informations about the appearance of a series
 */
public interface CameleonView<A> {
	
	/**
	 * Sets the visibility of the specified data series
	 */
	public void setSeriesVisible(String query, int series, boolean visible);
	
	public A getSeriesAppearance(String query, int series);
	
	public void setSeriesAppearance(String query, int series, A appearance);
	
}

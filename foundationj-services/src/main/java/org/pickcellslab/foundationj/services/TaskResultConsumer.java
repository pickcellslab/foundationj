package org.pickcellslab.foundationj.services;

/**
 * Interface for consumers of {@link TaskProgress} and their result
 * 
 * @author Guillaume Blin
 *
 * @param <V> The type of output of the task beng consumed by this consumer
 */
public interface TaskResultConsumer<V> {

	/**
	 * @param source The {@link TaskProgress} to be consumed
	 * @param result The result associated with the provided {@link TaskProgress}.
	 * Maybe null is the task was cancelled or was interrupted for any reason.
	 */
	public void consume(TaskProgress<V> source, V result);
	
}

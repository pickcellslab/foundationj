package org.pickcellslab.foundationj.datamodel.functions;

public interface DoubleToDoubleBiFunction {
	double apply(double a, double b);
}
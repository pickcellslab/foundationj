package org.pickcellslab.foundationj.datamodel.tools;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;

/** 
 * A step in a stepwise builder pattern to define the 'rules' for navigating a graph. This step defines which nodes 
 * should be traversed.
 *
 * @param <T> The type of the next builder
 */
public interface EvaluationContainerInit<T> {

	/**
	 * Specifies that all nodes are allowed to be traversed 
	 * @return The next step in the building process
	 */
	public T includeAllNodes();

	/**
	 * Specifies a rule to decide which nodes should be traversed.
	 * @param explicitPredicate An {@link ExplicitPredicate} to categorise nodes encountered during the traversal
	 * @param ifYes The {@link Decision} if {@code explicitPredicate} returns {@code true}
	 * @param ifNot The {@link Decision} if {@code explicitPredicate} returns {@code false}
	 * @return The next step in the building process
	 */
	public T withOneEvaluation(ExplicitPredicate<? super NodeItem> explicitPredicate, Decision ifYes, Decision ifNot);

	/**
	 * Specifies the first rule defining which nodes should be traversed
	 * @param explicitPredicate An {@link ExplicitPredicate} to categorise nodes encountered during the traversal
	 * @param ifYes The {@link Decision} if {@code explicitPredicate} returns {@code true}
	 * @param defaultEval The {@link Decision} if {@code explicitPredicate} and any subsequently defined predicate return {@code false}
	 * @return The next step in the building process (which will allow to define additional rules)
	 */
	public EvaluationContainerBuilder<T> withEvaluations(ExplicitPredicate<? super NodeItem> explicitPredicate, Decision ifYes, Decision defaultEval);

}

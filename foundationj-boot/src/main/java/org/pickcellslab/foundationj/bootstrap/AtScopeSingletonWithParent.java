package org.pickcellslab.foundationj.bootstrap;

import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.ScopePolicy;

public class AtScopeSingletonWithParent extends AbstractAtScope {

	private final AbstractAtScope parentAtScope;
	private ScopeImpl active;


	public AtScopeSingletonWithParent(String scope, AbstractAtScope parent) {
		super(scope, parent.scope(), ScopePolicy.SINGLETON);
		this.parentAtScope = parent;
	}
	
	
	
	

	@Override
	public ScopeImpl createAndRegister(String id, String parentId) {
		// check if this is already active
		if(active != null)
			throw new RuntimeException(scope.toString() + "is already active");

		else {
			// check that parent is active
			ScopeImpl parentScope = parentAtScope.activeWithId(parentId).orElse(null);
			if(parentScope==null) 
				throw new RuntimeException("parent is inactive: "+parentAtScope.scope());			

			System.out.println("AtScopeSingletonWithParent: Scope "+id+" created with parent "+parentScope.name()+" and Id "+parentId);

			active = new ScopeImpl(scope, id, parentScope, policy, comps);
						
			return active;
		}		
	}


	@Override
	public ScopeImpl remove(String id) {
		if(active==null)
			throw new RuntimeException("There are no active Scope with "+id);
		else {
			ScopeImpl toReturn = active;
			System.out.println("AtScopeSingletonWithParent: Scope "+id+" removed ! ");
			active = null;
			return toReturn;
		}
	}

	@Override
	public Stream<ScopeImpl> active() {
		if(active==null)
			return Stream.empty();
		else
			return Stream.of(active);
	}


	@Override
	public Optional<ScopeImpl> activeWithId(String id) {
		return active().findAny();
	}
	
	@Override
	boolean hierarchyHasComponent(Class<?> comp) {
		// First check if we already contain the serviceImpl
		if(comps.contains(comp))
			return true;
		final AbstractAtScope parent = parentAtScope;
		if(parent != null)
			return parent.hierarchyHasComponent(comp);
		return false;
	}

	

}

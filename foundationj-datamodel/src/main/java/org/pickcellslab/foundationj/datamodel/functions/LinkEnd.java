package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;

/**
 * Enum constants which are also {@link ExplicitFunction} that can be applied to {@link Link} in order to obtain the source or the target.
 *
 */
@Mapping(id="LinkEnd")
public enum LinkEnd implements ExplicitFunction<Link,NodeItem>{

	Source{

		@Override
		public NodeItem apply(Link t) {
			return t.source();
		}

		@Override
		public String description() {
			return "Link Source";
		}

		@Override
		public Class<NodeItem> getReturnType() {
			return NodeItem.class;
		}
		
	},
	Target{

		@Override
		public NodeItem apply(Link t) {
			return t.target();
		}
		
		@Override
		public String description() {
			return "Link Target";
		}

		@Override
		public Class<NodeItem> getReturnType() {
			return NodeItem.class;
		}
	}
	
}

+++

title ="Foundationj"
weight = 2

date = "2018-07-23"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2018-07-31"

repo = "foundationj"
fileUrl = "https://framagit.org/pickcellslab/foundationj/blob/docs/docs/content/developers/architecture/foundationj/_index.md"

+++

{{% panel theme="warning" header="Draft" %}}

This page is just a draft for now. 

{{% /panel %}}


Foundationj is a framework which was designed initially with PickCells in mind.

In this section, you will find pages which explain how foundationj works

The source code is available on our [GitLab repository](https://framagit.org/pickcellslab/foundationj/tree/feature/refactoring). It contains README files explaining how to get setup if you would like to leverage foundationj for you own application. 

Finally the source documentation of the foundationj pages that you are seeing on this website are located inside the docs folder of the foundationj repository.
package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.FunctionDimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyDimension;
import org.pickcellslab.foundationj.datamodel.functions.LinkEnd;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.queries.ReadAnyBuilder;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.builders.AccessChoice;
import org.pickcellslab.foundationj.dbm.queries.builders.DeletionConfig;
import org.pickcellslab.foundationj.dbm.queries.builders.QueryRunner;
import org.pickcellslab.foundationj.dbm.queries.builders.ReadItemsBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.mapping.data.MultitonOutcome;

@Data(typeId = "MetaLink", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
class MetaLinkImpl extends AbstractMetaQueryable implements MetaLink {


	private MetaLinkImpl(String uid){
		super(uid);
	}


	static MetaLinkImpl getOrCreate(MetaInstanceDirector director, String type,  MetaClass source,  MetaClass target, String description){


		try {

			final MultitonOutcome<MetaLinkImpl> out = director.getOrCreateWithOutcome( MetaLinkImpl.class, type + source.recognitionString() + target.recognitionString());

			if(out.wasCreated()){// Check if it was retrieved or if it already existed
				final MetaLinkImpl metaLink = out.getInstance();

				metaLink.setAttribute(MetaLink.name, type);
				metaLink.setAttribute(MetaLink.desc, description);
				metaLink.setAttribute(MetaLink.type, type);

				Link l1 =  new DataLink(MetaModel.fromLink, metaLink, source, false);  
				Set<Link> typeSet1  = new LinkedHashSet<>();
				metaLink.typeMap.put(l1.declaredType(), typeSet1);
				metaLink.directionMap.get(Direction.OUTGOING).add(l1);
				typeSet1.add(l1);


				Link l2 =  new DataLink(MetaModel.toLink, metaLink, target, false);
				Set<Link> typeSet2  = new LinkedHashSet<>();
				metaLink.typeMap.put(l2.declaredType(), typeSet2);
				metaLink.directionMap.get(Direction.OUTGOING).add(l2);
				typeSet2.add(l2);

				source.addLink(l1);
				target.addLink(l2);

			}

			return out.getInstance();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	@Override
	public String itemDeclaredType(){
		return this.getAttribute(type).get();
	}

	@Override
	public String linkType(){
		return this.getAttribute(type).get();
	}


	@Override
	public MetaClass source(){
		return getLinks(Direction.OUTGOING, MetaModel.fromLink)
				.map(l->(MetaClass)l.target())
				.findFirst().orElse(empty());
	}




	@Override
	public MetaClass target(){
		return getLinks(Direction.OUTGOING, MetaModel.toLink)
				.map(l->(MetaClass)l.target())
				.findFirst().orElse(empty());
	}


	private MetaClass empty() {
		final MetaClass mc = new MetaClassImpl("Absent");
		mc.setAttribute(MetaLink.name, "Deleted");
		mc.setAttribute(MetaLink.desc, "Deleted");
		return mc;
	}




	@Override
	public String toHTML() {
		return "<HTML>"+
				"<strong>Link</strong>"+
				"<p><ul><li>Name:"+getAttribute(name).get()+"</li>"+
				"<li>Connects:"+source().name()+" --> "+target().name()+"</li>"+
				"<li>Description:"+getAttribute(desc).get()+
				"</li></p></HTML>";

	}

	@Override
	public String toString(){
		return source().name()+"--["+linkType()+"]-->"+target().name();
	}




	/**
	 * Helper method to obtain a {@link MetaReadable} describing and providing a {@link Dimension} to the provided {@link AKey}
	 * on the source of this link.
	 * @param sk The AKey to read on the source
	 * @throws IllegalArgumentException if the the provided AKey does not exist in the source
	 * 
	 */
	@Override
	public <E> DimensionContainer<Link, E> readOnSource(AKey<E> sk) throws IllegalArgumentException{
		Optional<MetaKey<E>> mk = source().metaKey(sk);
		if(!mk.isPresent())
			throw new IllegalArgumentException("The provided key does not exist for this source object");		
		return new DetachedFunction<E>(LinkEnd.Source, mk.get());
	}

	/**
	 * Helper method to obtain a {@link MetaReadable} describing and providing a {@link Dimension} to the provided {@link AKey}
	 * on the target of this link.
	 * @param sk The AKey to read on the source
	 * 
	 */
	@Override
	public <E> DimensionContainer<Link, E> readOnTarget(AKey<E> sk){
		Optional<MetaKey<E>> mk = target().metaKey(sk);
		if(!mk.isPresent())
			throw new IllegalArgumentException("The provided key does not exist for this target object");		
		return new DetachedFunction<E>(LinkEnd.Target, mk.get());
	}



	@Override
	public TraversalDefinition deletionDefinition() {
		return TraversalDefinition.newDefinition()
				.startFrom(DataRegistry.typeIdFor(MetaLinkImpl.class)).having(MetaModel.recognition, this.recognitionString())
				.fromDepth(0).toDepth(2)
				.traverseAllLinks()
				.withOneEvaluation(
						P.items().include(MetaClassImpl.class).create(),
						Decision.EXCLUDE_AND_STOP,
						Decision.INCLUDE_AND_CONTINUE).build();
	}



	@Override
	public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
		return access.queryFactory().readOneLink(linkType(), dbId).using(action);
	}


	@Override
	public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
		//We need to check if there exist other MetaLinks with same type in the MetaModel
		if(access.metaModel().hasSeveralSourceTargetCombination(linkType())){
			return access.queryFactory().readAnyLink(linkType(), source().itemDeclaredType(), target().itemDeclaredType())
					.using(action).getAll().run();
		}
		else
			return access.queryFactory().readAnyLink(linkType())
					.using(action).getAll().run();
	}



	@Override
	public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
		return this.initiateRead(access, limit).with(action).inOneSet().getAll().run();
	}


	@Override
	public void updateData(DataAccess access, Updater action) throws DataAccessException {
		this.initiateUpdate(access, action).getAll().run();
	}


	@Override
	public DeletionConfig<? extends WritableDataItem> initiateDeletion(DataAccess access) {
		//We need to check if there exist other MetaLinks with same type in the MetaModel
		if(access.metaModel().hasSeveralSourceTargetCombination(linkType())){
			return access.queryFactory().deleteLinks(linkType(), source().itemDeclaredType(), target().itemDeclaredType());
		}
		else
			return access.queryFactory().deleteLinks(linkType());
	}


	@Override
	public AccessChoice<? extends Updatable, QueryRunner<Void>> initiateUpdate(DataAccess access, Updater updater) {
		return access.queryFactory().updateLinks(linkType(), source().itemDeclaredType(), target().itemDeclaredType(), updater);		
	}



	@Override
	public ReadItemsBuilder<? extends WritableDataItem> initiateRead(DataAccess access, long limit) {
		//We need to check if there exist other MetaLinks with same type in the MetaModel
		if(access.metaModel().hasSeveralSourceTargetCombination(linkType())){
			return access.queryFactory().readLinks(linkType(), source().itemDeclaredType(), target().itemDeclaredType(), limit);
		}
		else
			return access.queryFactory().readLinks(linkType());
	}




	@Override
	public ReadAnyBuilder<? extends WritableDataItem> initiateFindOne(DataAccess access) {
		//We need to check if there exist other MetaLinks with same type in the MetaModel
		if(access.metaModel().hasSeveralSourceTargetCombination(linkType())){
			return access.queryFactory().readAnyLink(linkType(), source().itemDeclaredType(), target().itemDeclaredType());
		}
		else
			return access.queryFactory().readAnyLink(linkType());
	}




	@Override
	public List<MetaQueryable> queriedMetaObjects() {
		return Collections.singletonList(this);
	}



	@Override
	public String name() {
		return getAttribute(MetaModel.name).get();
	}


	@Override
	public String description() {
		return getAttribute(MetaModel.desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.creator, MetaModel.desc, MetaLink.type);
	}








	@Mapping(id = "LinkFunction")
	@WithService(DataAccess.class)
	private class DetachedFunction<E> implements DimensionContainer<Link,E>{


		private final AKey<E> key;
		private final int length;
		private final LinkEnd on;


		DetachedFunction(LinkEnd le, MetaKey<E> mk) {
			this.key = mk.toAKey();
			this.length = mk.length();
			this.on = le;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public Dimension<Link, E> getRaw() {
			return new FunctionDimension((AKey<E>) AKey.get(key.name+" from "+on.toString(), key.clazz), on.next(F.select(key, null)), key.name+" from "+on);
		}

		@Override
		public int numDimensions() {
			return length == -1 ? 1 : length;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public Dimension<Link, E> getDimension(int index) throws ArrayIndexOutOfBoundsException {
			if(length == -1)
				return getRaw();
			else{
				final Dimension<DataItem, E> dim = new KeyDimension(key, index, key.name+" from "+on+ "at index "+index);
				return new FunctionDimension((AKey<E>) AKey.get(key.name+" from "+on.toString(), key.clazz), on.next(dim), dim.description());
			}
		}


	}




	@Override
	public DataPointer subset(ExplicitPredicate<DataItem> predicate) {

		Objects.requireNonNull(predicate);

		return new DataPointer() {

			@Override
			public int deleteData(DataAccess access) throws DataAccessException {
				throw new RuntimeException("Forbidden for now.");
			}

			@Override
			public <O> O readOne(DataAccess access, int dbId, Function<DataItem,O> action)throws DataAccessException {
				return access.queryFactory().readOneLink(linkType(), dbId).using(action);
			}

			@Override
			public <O> O readAny(DataAccess access, Function<DataItem, O> action) throws DataAccessException {
				return MetaLinkImpl.this.initiateFindOne(access).using(action).useFilter(predicate).run();
			}



			@Override
			public <O> O readData(DataAccess access, Action<DataItem, O> action, long limit) throws DataAccessException {
				return MetaLinkImpl.this.initiateRead(access, limit).with(action).inOneSet().useFilter(predicate).run();
			}


			@Override
			public void updateData(DataAccess access, Updater action) throws DataAccessException {
				MetaLinkImpl.this.initiateUpdate(access, action).useFilter(predicate).run();
			}

			@Override
			public List<MetaQueryable> queriedMetaObjects() {
				return MetaLinkImpl.this.queriedMetaObjects();
			}

			@Override
			public DataPointer subset(ExplicitPredicate<DataItem> predicate2) {
				Objects.requireNonNull(predicate2);
				return MetaLinkImpl.this.subset(predicate.and(predicate2));
			}

		};
	}


}

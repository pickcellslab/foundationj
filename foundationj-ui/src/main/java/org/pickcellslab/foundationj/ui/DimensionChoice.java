package org.pickcellslab.foundationj.ui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.renderers.DimensionRenderer;
import org.pickcellslab.foundationj.ui.utils.AlphanumComparator;

/**
 * Allows the user to choose a dimension amongst the Dimensions from a list of DimensionContainer
 * 
 * @author Guillaume Blin
 *
 */
public class DimensionChoice<T,R> extends JPanel {

	private static final long serialVersionUID = -1116254179306797622L;
	private final JPanel contentPanel = new JPanel();

	private List<ChangeListener> lstrs = new ArrayList<>();

	private Dimension<T,R> selectedDim;



	/**
	 * Creates a UI allowing to choose a {@link Dimension} from the given List.
	 * @param keys The list of Dimension to choose from
	 * @param selected The Dimension selected when the UI shows up (null allowed)
	 */
	public DimensionChoice(List<Dimension<T,R>> keys, Dimension<T,R> selected, UITheme theme) {

		if(keys == null || keys.isEmpty())
			throw new IllegalArgumentException("readables cannot be empty or null");

		// TODO remove
		if(selected!=null)
			System.out.println("DimensionChoice : selected = "+selected.description());

		//ComboBox to display available MetaKey
		keys.sort(new AlphanumComparator<>(null));



		JTextPane text = new JTextPane();
		text.setContentType("text/html");
		JScrollPane textPane = new JScrollPane();
		textPane.setViewportView(text);

		JComboBox<Dimension<T,?>> comboBox = new JComboBox<>(keys.toArray(new Dimension[keys.size()]));
		if(selected!=null)
			comboBox.setSelectedItem(selected);
		comboBox.setRenderer(new DimensionRenderer(theme));
		selectedDim = (Dimension<T,R>) comboBox.getSelectedItem();
		text.setText(selectedDim.toHTML());

		//Update slider and arrayBox when chosen MetaKey changes
		comboBox.addItemListener( 
				e->{
					if(e.getStateChange() == ItemEvent.SELECTED){
						selectedDim = (Dimension<T,R>) comboBox.getSelectedItem();
						text.setText(selectedDim.toHTML());
					};
				});













		//Layout
		//--------------------------------------------------------------------------------


		setBounds(100, 100, 370, 251);
		setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(contentPanel, BorderLayout.CENTER);





		JLabel lblPleaseSelectA = new JLabel("Select a Dimension");
		lblPleaseSelectA.setHorizontalAlignment(SwingConstants.RIGHT);


		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(textPane, Alignment.LEADING)
								.addGroup(Alignment.LEADING, gl_contentPanel.createSequentialGroup()
										.addComponent(lblPleaseSelectA)
										.addGap(18)
										.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 203, GroupLayout.PREFERRED_SIZE)))
						//.addContainerGap(497, Short.MAX_VALUE))
						));
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap(26, Short.MAX_VALUE)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGap(5)
										.addComponent(lblPleaseSelectA))
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(7)
						.addComponent(textPane, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
						.addContainerGap())
				);
		contentPanel.setLayout(gl_contentPanel);

	}



	//Listeners
	//---------------------------------------------------------------------------------------------------------

	public void addChangeListener(ChangeListener lst){
		lstrs.add(lst);
	}

	public void removeSliderListener(ChangeListener lst){
		lstrs.remove(lst);
	}

	public void fireChange(){
		lstrs.forEach(l->l.stateChanged(new ChangeEvent(this)));
	}



	public Dimension<T,R> currentDimension(){
		return selectedDim;
	}


	/**
	 * @return An {@link Optional} holding the {@link ExplicitFunction} specified by the user (Extract a value from a DataItem)
	 */

	public Dimension<T,R> getSelected(){
		return selectedDim;
	}







}

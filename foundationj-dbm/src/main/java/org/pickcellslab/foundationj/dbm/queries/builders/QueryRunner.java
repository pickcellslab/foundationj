package org.pickcellslab.foundationj.dbm.queries.builders;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;

/**
 * The last step in the stepwise builder pattern used to build database queries.
 * @param <O> The type of the output of the query
 */
public interface QueryRunner<O> {
	
	/**
	 * Runs the query
	 * @return The result of the query
	 * @throws DataAccessException
	 */
	public O run() throws DataAccessException;
	
}

package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.CardLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaKeyContainer;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;

@SuppressWarnings("serial")
public class AccumulatorChoice extends WizardNode {

	private JRadioButton nodeFnRB, nodeLinksFnRB, nodeAngleFnRB, cFnRB, pathFnRB;

	//public static final int NODE_ITSELF = 0, ON_LINK = 1, ON_CONNECTED = 2, LINK_ITSELF = 3, VECTOR_FUNCTION = 4, LINKS_OF_NODE = 5;
	private JPanel cardPanel;
	private JPanel nodePanel;
	private JPanel panel;
	private JLabel lblTarget;
	private JComboBox<MetaKeyContainer> comboBox;
	private JPanel linkPanel;
	private JRadioButton linkFnRB, linkAngleFnRB;

	private final DataAccess access;
	private final UITheme theme;

	
	public AccumulatorChoice(WizardNode parent, UITheme theme, DataAccess access, Supplier<MetaQueryable> supplier) {
		super(parent);

		this.access = access;	
		this.theme = theme;
		
		setBorder(new TitledBorder(null, "Type of accumulator", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		final ButtonGroup buttonGroup = new ButtonGroup();
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		panel = new JPanel();
		add(panel);

		cardPanel = new JPanel();
		add(cardPanel);
		cardPanel.setLayout(new CardLayout(0, 0));


		lblTarget = new JLabel("Target : ");

		comboBox = new JComboBox<>();
		comboBox.addItem(supplier.get());
		comboBox.setRenderer(new MetaRenderer(theme, access.dataRegistry()));
				
		
		comboBox.addActionListener(l->{
			CardLayout lt = (CardLayout) cardPanel.getLayout();
			if(MetaClass.class.isAssignableFrom(comboBox.getSelectedItem().getClass())){
				lt.show(cardPanel, "node");
				buttonGroup.setSelected(nodeFnRB.getModel(), true);
				pathFnRB.setEnabled(GraphSequencePointer.class.isAssignableFrom(((MetaClass) comboBox.getSelectedItem()).itemClass(access.dataRegistry())));
			}
			else{
				lt.show(cardPanel, "link");
				buttonGroup.setSelected(linkFnRB.getModel(), true);
			}
		});



		nodePanel = new JPanel();
		cardPanel.add(nodePanel, "node");
		nodePanel.setLayout(new GridLayout(0, 1, 10, 10));

		nodeFnRB = new JRadioButton("Create a function on the node's attributes");
		nodePanel.add(nodeFnRB);
		nodeFnRB.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup.add(nodeFnRB);
		nodeFnRB.setSelected(true);
		

	
		nodeAngleFnRB = new JRadioButton("Define a function between spatial or directional vectors of the node");
		nodePanel.add(nodeAngleFnRB);
		nodeAngleFnRB.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup.add(nodeAngleFnRB);
		

		nodeLinksFnRB = new JRadioButton("Collect data from the links of the node");
		nodePanel.add(nodeLinksFnRB);
		nodeLinksFnRB.setHorizontalAlignment(SwingConstants.LEFT);
		buttonGroup.add(nodeLinksFnRB);
		nodeLinksFnRB.setSelected(true);
		

		cFnRB = new JRadioButton("Collect data from other objects connected to the node");
		nodePanel.add(cFnRB);
		buttonGroup.add(cFnRB);
		
		
		
		pathFnRB = new JRadioButton("Create a new attribute from the path(s) defined by this object");		
		nodePanel.add(pathFnRB);
		buttonGroup.add(pathFnRB);
		
		
		

		linkPanel = new JPanel();
		linkPanel.setLayout(new GridLayout(0, 1, 10, 10));
		
		cardPanel.add(linkPanel, "link");

		linkFnRB = new JRadioButton("<HTML>Create a function using the the source, the target "
				+ "<br> or the link's attributes</HTML>");
		buttonGroup.add(linkFnRB);
		linkFnRB.setHorizontalAlignment(SwingConstants.LEFT);
		linkPanel.add(linkFnRB);
		
		linkAngleFnRB = new JRadioButton("<HTML>Define an angle of interest using the the source, the target "
				+ "<br> or the link's vectors</HTML>");
		buttonGroup.add(linkAngleFnRB);
		linkAngleFnRB.setHorizontalAlignment(SwingConstants.LEFT);
		linkPanel.add(linkAngleFnRB);
		
		
		

		if(comboBox.getSelectedItem() instanceof MetaClass){
			((CardLayout) cardPanel.getLayout()).show(cardPanel, "node");
			pathFnRB.setEnabled(GraphSequencePointer.class.isAssignableFrom(((MetaClass) comboBox.getSelectedItem()).itemClass(access.dataRegistry())));
		}
		else{
			((CardLayout) cardPanel.getLayout()).show(cardPanel, "link");
			linkFnRB.setSelected(true);
		}


		//Layout
		//----------------------------------------------------------------------------------------------------
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addGap(90)
						.addComponent(lblTarget)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(103, Short.MAX_VALUE))
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblTarget)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
				);
		panel.setLayout(gl_panel);



	}



	@Override
	public int numberOfForks() {
		return 5;
	}



	@Override
	public boolean validity() {
		return true;
	}



	@Override
	protected WizardNode getNextStep() {
		
		WizardNode wn = null;
		if(nodeFnRB.isSelected() || linkFnRB.isSelected())
			wn = new FunctionBuilderUI(this, access.metaModel().getInstanceManager(), (MetaQueryable)comboBox.getSelectedItem(), theme);
		
		else if (nodeLinksFnRB.isSelected())
			wn = new AttributeFromLinksUI(this, (MetaClass)comboBox.getSelectedItem(), theme, access);
			
		else if(cFnRB.isSelected())
			wn = new MetaTraversingFunctionUI(this, theme, access, (MetaClass)comboBox.getSelectedItem());
		
		else if(pathFnRB.isSelected())
			wn = new GraphSequenceFunctionUI(this, theme, access, (MetaClass)comboBox.getSelectedItem());
		
		else if(nodeAngleFnRB.isSelected() || linkAngleFnRB.isSelected())
			wn = new VectorFunctionUI<>(this, theme, access.metaModel().getInstanceManager(), (MetaQueryable)comboBox.getSelectedItem());
		
		return wn;
	}

	
	
	public static void main(String[] args) {
		List<Object> list = new ArrayList<>();
		list.add(new MetaInstanceDirector(null));
		Class<?> toTest = MetaInstanceDirector.class;
		list.stream().filter(o->toTest.isAssignableFrom(o.getClass())).findAny().ifPresent(System.out::println);
	}
	
}

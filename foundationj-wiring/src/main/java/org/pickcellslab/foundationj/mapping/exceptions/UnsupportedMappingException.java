package org.pickcellslab.foundationj.mapping.exceptions;

public class UnsupportedMappingException extends RuntimeException {

	public UnsupportedMappingException(Object o) {
		super(o.getClass()+" is not an object which can be mapped, see @Mapping documentation");
	}

}

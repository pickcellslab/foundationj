package org.pickcellslab.foundationj.dbm.queries;

import java.util.List;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.Traverser;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;

/**
 * 
 * A {@link Query} to store {@link DataItem}s to the database
 * 
 * @author Guillaume Blin
 *
 */
public interface StorageBox extends Query<Void>{

	/**
	 * @return The maximum number of items and links to be stored per transaction. This maybe set by the caller of the query
	 * when the caller knows that many large objects are to be stored.
	 */
	public int transactionSizeLimit();

	/**
	 * @return {@code true} if the MetaModel has been constructed as a result of this query, {@code false} otherwise.
	 */
	public boolean hasMetaInfo();
	
	
	/**
	 * @return The {@link MetaInfo} for the items to be stored provided by {@link #getStandAlones()}.
	 */
	public MetaInfo getMetaInfoForStandalones();
	
	/**
	 * @param i The index in the List returned by {@link #getTraversers())
	 * @return The {@link MetaInfo} for the graphs to be stored provided by {@link #getTraverser()}.
	 */
	public MetaInfo getMetaInfoForTraverser(int i);

	/**
	 * @return The {@link Traverser} objects allowing to traverse the data graph that need to be updated
	 */
	public List<StepTraverser<NodeItem, Link>> getTraversers();

	/**
	 * @return A Collection of DataItem where each item needs to be stored ignoring any eventual
	 * connected components
	 */
	public List<WritableDataItem> getStandAlones();

	/**
	 * A flag to tell whether or not to force writing objects with a database id which is unknown to this database.
	 */
	boolean forceUnknownId();


}
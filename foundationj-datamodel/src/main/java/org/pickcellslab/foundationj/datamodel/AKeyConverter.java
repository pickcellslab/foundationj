package org.pickcellslab.foundationj.datamodel;

import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.mapping.extra.CustomNonDataConverter;

public class AKeyConverter implements CustomNonDataConverter<AKey<?>> {


	@Override
	public Object fromString(String str) {
		return AKey.get(str);
	}

	@Override
	public String toString(AKey<?> object) throws NonDataMappingException {
			return object.toString();
	}



}

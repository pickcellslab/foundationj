package org.pickcellslab.foundationj.dataviews.fitting;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


/**
 * A {@link Fitter} is capable of creating a new representation of some data given some options.
 *  
 * @author Guillaume Blin
 *
 * @param <D> The type of inputs the fitter takes
 * @param <O> The type of configuration required by this Fitter
 * @param <R> The type of the resulting representation
 */
public interface Fitter<D,O,R> {
	
	/**
	 * Fits the provided data, given the specified configuration.
	 * @param data The data to fit
	 * @param config The configuration for the fitting procedure.
	 * 
	 * @return The new representation
	 */
	public R fit(D data, FitConfig<O> config);
	
	
	/**
	 * @return Some information regarding the last fit performed
	 */
	public String lastFitInfos();
	
	
}

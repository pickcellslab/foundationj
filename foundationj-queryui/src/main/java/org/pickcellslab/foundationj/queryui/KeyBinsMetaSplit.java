package org.pickcellslab.foundationj.queryui;

/*-
 * #%L
 * foundationj-queryui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Multiton;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.MetaInstanceDirector;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.meta.MetaItem;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.meta.PredictableMetaSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitPathSplit;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.dbm.queries.ToStringKeyBinsSplitter;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

@Data(typeId = "MetaBinningOnProperty", hook = MetaInstanceDirector.class)
@Multiton(manager = MetaInstanceDirector.class)
@WithService(DataAccess.class)
public class KeyBinsMetaSplit extends PredictableMetaSplit{

	private static final AKey<double[]> binKey = AKey.get("Bins",double[].class);

	private KeyBinsMetaSplit(String uid) {
		super(uid);
	}

	static KeyBinsMetaSplit getOrCreate(MetaInstanceDirector director, MetaReadable mr, double[] values) throws InstantiationHookException {
		
		final KeyBinsMetaSplit split = director.getOrCreate(KeyBinsMetaSplit.class, "Bins_"+Arrays.toString(values)+"_"+mr.recognitionString());

		if(!split.getAttribute(MetaModel.name).isPresent()){// Check if it was retrieved or if it already existed

			split.setAttribute(MetaItem.name, "Bins for " +mr.name());
			split.setAttribute(MetaItem.desc, "Group "+mr.owner().name()+" in bins based on "+mr.name()+" "+ Arrays.toString(values));
			split.setAttribute(binKey,values);
			
			new DataLink(MetaItem.requiresLink, split, mr, true);

		}

		return split;
	}

	
	@Override
	public String name() {
		return getAttribute(name).get();
	}


	@Override
	public String description() {
		return getAttribute(desc).get();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(MetaModel.name, MetaModel.desc, binKey);
	}
	
	
	


	@Override
	public String toHTML() {
		return "<HTML>"+
				"<p><strong>Grouping by bins</strong>"+
				"<ul><li>Name:"+name()+"</li>"+
				"<li>Description:"+description()+
				"</li></p></HTML>";
	}


	@Override
	public String[] getPrediction() {
		double[] values =  getAttribute(AKey.get("Bins",double[].class)).get();
		String[] bins = new String[values.length+1];
		for(int i = 0; i<values.length; i++)
			bins[i] = "<"+values[i];
		bins[values.length] = ">"+values[values.length-1];
		return bins;
	}


	@Override
	public boolean pathSplitSupported() {
		return true;
	}


	@Override
	public boolean targetSplitSupported() {
		return true;
	}




	@Override
	public <E> ExplicitSplit<E, String> toSplitter() {

		final MetaReadable<? extends Number> mr = (MetaReadable) getLinks(Direction.OUTGOING, MetaModel.requiresLink).iterator().next().target();
		final double[] values = getAttribute(AKey.get("Bins",double[].class)).get();
		ExplicitFunction<E, ? extends Number> f = (ExplicitFunction<E, ? extends Number>) mr.getDimension(0);

		return new ToStringKeyBinsSplitter<>(values, f);
		
	}





	@Override
	public <V, E> ExplicitPathSplit<V, E, String> toPathSplitter() {

		final MetaReadable mr = (MetaReadable) getLinks(Direction.OUTGOING, MetaModel.requiresLink).iterator().next().target();
		final boolean isLink = mr.owner() instanceof MetaLink;

		final double[] values = getAttribute(AKey.get("Bins",double[].class)).get();

		Dimension<DataItem,? extends Number> dim = (Dimension<DataItem,? extends Number>) mr.getDimension(0);
		ExplicitFunction<DataItem, ? extends Number> f = (ExplicitFunction) dim;
		Predicate<DataItem> predicate = P.isDeclaredType(mr.owner().itemDeclaredType());





		return new ExplicitPathSplit<V,E,String>(){

			private FjAdaptersFactory<V,E> fctry;

			@Override
			public String description() {
				return "Split path by bins on "+mr.toString()+" in "+mr.owner().name()+" , bins: "+Arrays.toString(values);
			}

			@Override
			public String getKeyFor(Path<V, E> i) {

				if(isLink){
					Iterator<E> it = i.edges();
					while(it.hasNext()){

						DataItem t = fctry.getLinkReadOnly(it.next());
						if(predicate.test(t)){

							Number n = f.apply(t);
							if(null == n)
								return "NA";

							double m = n.doubleValue();
							double low = Double.NaN; 
							for(double d : values){
								if(m<d)
									break;
								else
									low = d;				
							}
							if(Double.isNaN(low))
								return "<"+values[0];
							else
								return ">="+low;
						}
					}
				}
				else{
					Iterator<V> it = i.nodes();
					while(it.hasNext()){

						DataItem t = fctry.getNodeReadOnly(it.next());
						if(predicate.test(t)){

							Number n = f.apply(t);
							if(null == n)
								return "NA";

							double m = f.apply(t).doubleValue();
							double low = Double.NaN; 
							for(double d : values){
								if(m<d)
									break;
								else
									low = d;				
							}
							if(Double.isNaN(low))
								return "<"+values[0];
							else
								return ">="+low;
						}
					}
				}
				return ExplicitSplit.OTHER;
			}






			@Override
			public void setFactory(FjAdaptersFactory<V, E> f) {
				this.fctry = f;
			}

		};
	}




}

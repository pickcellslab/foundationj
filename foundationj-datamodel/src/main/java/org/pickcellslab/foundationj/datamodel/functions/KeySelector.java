package org.pickcellslab.foundationj.datamodel.functions;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.Supported;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;

/**
 * A {@link ExplicitFunction} returning the value associated with a specific AKey in {@link DataItem}s.
 */
@Mapping(id = "KeySelection")
@Scope
public class KeySelector<T extends DataItem, R> implements ExplicitFunction<T,R>{


	private final AKey<R> key;
	
	@Supported
	private R defaultValue;

	
	/**
	 * Creates a new {@link KeySelector} which will return the value associated with the specified AKey
	 * and a default value set to null
	 * @param key
	 */
	public KeySelector(AKey<R> key){
		this.key = key;
	}
	
	/**
	 * Creates a new {@link KeySelector} which will return the value associated with the specified AKey
	 * or a default value
	 * @param key
	 * @param defaultValue The default value if the key does not exist
	 */
	public KeySelector(AKey<R> key, R defaultValue){
		this.key = key;
		this.defaultValue = defaultValue;
	}
	
	AKey<?> getKey(){
		return key;
	}
	
	@Override
	public  R apply(T t) {
		return t.getAttribute(key).orElse(defaultValue);
	}
	
	
	/**
	 * Sets the default value returned by this function if the DataItem this function is applied to
	 * does not possess the attribute 
	 * @param defaultValue
	 */
	public void withDefaultValue(R defaultValue) {
		this.defaultValue = defaultValue;
	}

	
	@Override
	public String description() {
		return key.name;
	}
	
	
	@Override
	public String toString() {
		return description();
	}

	@Override
	public Class<R> getReturnType() {
		return key.type();
	}


	
}

package org.pickcellslab.foundationj.datamodel.reductions;

import org.pickcellslab.foundationj.annotations.Ignored;
import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Scope;

@Mapping(id="Counting")
@Scope
public class CountOperation<T> implements ExplicitReduction<T,Long>{

	@Ignored
	private long counter = 0;

	@Override
	public ExplicitReduction<T, Long> factor() {
		return new CountOperation<>();
	}

	
	@Override
	public void root(T root, boolean included) {
		if(included)
			include(root);		
	}
	
	
	@Override
	public void include(T t) {
		if(t != null) counter++;
	}

	@Override
	public Long getResult() {
		return counter;
	}

	
	@Override
	public Class<Long> getReturnType() {
		return long.class;
	}

	@Override
	public String description() {
		return "Counting";
	}

}

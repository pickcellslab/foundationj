package org.pickcellslab.foundationj.dbm.db;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ReconstructionRule;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;

public interface Regenerator<V> {


	/**
	 * @param nodes The list of nodes to be regenerated
	 * @param rule The {@link ReconstructionRule} which specifies how items should be regenerated
	 * @param delete Whether or not Items should be deleted from the database after regeneration.
	 * @return A {@link RegeneratedItems} holding the objects corresponding to the given List of nodes.
	 * @throws DataAccessException
	 */
	public RegeneratedItems regenerate(List<V> nodes, ReconstructionRule rule, boolean delete) throws DataAccessException;
	
}

package org.pickcellslab.foundationj.dbm.impl;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.annotations.Supported;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.queries.ExplicitSplit;
import org.pickcellslab.foundationj.mapping.extra.MappingUtils;

@Mapping(id="GroupingByPredicate")
class BinarySplitter<T> implements ExplicitSplit<T,Boolean> {

	@Supported
	private final ExplicitPredicate<? super T> test;

	public BinarySplitter(ExplicitPredicate<? super T> test){
		MappingUtils.exceptionIfNullOrInvalid(test);
		this.test = test;
	}
	
	@Override
	public String description() {
		return "grouping using "+test.description();
	}

	@Override
	public Boolean getKeyFor(T t) {
		return test.test(t);
	}
	
}

package org.pickcellslab.foundationj.mapping.data;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.DenyModularity;
import org.pickcellslab.foundationj.mapping.exceptions.DataTypeException;
import org.pickcellslab.foundationj.mapping.exceptions.InstantiationHookException;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * When extending the data model (by creating new classes annotated with {@link Data}), it is possible to register 
 * an {@link InstantiationHook} which can take care of the specifics of instantiating the new implementation if 
 * the default procedure is not adapted.
 * <br><br>
 * Declaring the type of the implemented {@link InstantiationHook} in a {@link Data} annotated class will enable the discovery of the implementing
 * class at runtime.
 * 
 * @see Data
 * @see DataRegistry
 * @see AbstractInstantiationHook
 */
@DenyModularity
public interface InstantiationHook {
	
	/**
	 * @param clazz The persisted DataElement with the backend implementation to be rebuilt into the corresponding data model implementation.
	 * @return A new DataElement with the data model implementation
	 * @throws DataTypeException
	 * @throws InstantiationHookException 
	 */
	public Object getDataInstance(DbElement dbObject) throws DataTypeException, InstantiationHookException;
	
		
}

package org.pickcellslab.foundationj.annotations;


/**
 * Specifies how a given {@link Scope} should be handled by the framework.
 * 
 * @see #SINGLETON
 * @see #SIBLING
 * @see #MULTIPLE
 * 
 * @author Guillaume Blin
 *
 */
public enum ScopePolicy {

	/**
	 * The framework ensures that only one instance of the given scope exists at any given time for a given parent. 
	 * <br><b>NB:</b> If the parent scope of the SINGLETON scope has a SIBLING ScopePolicy, 
	 * several SINGLETON containers may co-exist within the application, but only one per parent instance. 
	 */
	SINGLETON, 
	
	/**
	 * The framework allows multiple containers with this scope to coexist (different {@link Scope#id()} with the same {@link Scope#scope()}).
	 * However all co-existing containers must possess the same {@link Scope#superScope()} ({@link #equals(Object)}). If a 
	 * scoped process having a distinct super scope from the active one is requested, confirmation of shutting down active 
	 * processes will be requested.
	 */
	SIBLING,
	
	/**
	 * Multiple containers with a given scope can co-exist, regardless of id or superscope
	 */
	MULTIPLE
	
}

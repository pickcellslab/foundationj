package org.pickcellslab.foundationj.dbm.impl;

import java.util.List;
import java.util.Optional;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.db.DatabaseAccess;
import org.pickcellslab.foundationj.dbm.db.FjAdaptersFactory;
import org.pickcellslab.foundationj.dbm.queries.SuppliedTraversalOperation;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition.TraversalPolicy;


/**
 * 
 * {@link TargetType} provides a generic interface for accessing database objects regardless of their {@link Paradigm}.
 * 
 * @author Guillaume Blin
 *
 * @param <V> The type of nodes (vertices) in the database
 * @param <E> The type of links (edges) in the database
 * @param <T> The type of the target of the query (will be consistent with {@link #paradigm()})
 */
interface TargetType<V,E,T>{  
	
	/**
	 * @return The {@link Paradigm} of this {@link TargetType}
	 */
	public Paradigm paradigm();	
	
	/**
	 * @return The {@link Session} from which to use this TargetType.
	 */
	public Session<V,E> session();	
	
	/**
	 * @return A {@link DatabaseAccessor} for all the objects present in the database represented by this {@link TargetType}
	 */
	public DatabaseAccessor<V,E,T> accessor();
	
	
	/**
	 * @param access The {@link DatabaseAccess} where the read should be performed.
	 * @param dbId The database id of the desried object
	 * @return An {@link Optional} holding an Object with the given database id and type defined by this {@link TargetType}
	 */
	public Optional<T> getOne(DatabaseAccess<V,E> access, int dbId); 
	
	/**
	 * @return A {@link DatabaseAccessor} for the objects encountered during the traversal of the database represented by this {@link TargetType}
	 * given the provided {@link TraversalDefinition}
	 */
	public DatabaseAccessor<V,E,T> accessor(TraversalDefinition td);
	
	/**
	 * @return A {@link DatabaseAccessor} for the objects encountered during the traversal of the database represented by this {@link TargetType}
	 * given the provided {@link SuppliedTraversalOperation}
	 */
	public DatabaseAccessor<V,E,T> accessor(SuppliedTraversalOperation ops);
	
	/**
	 * This method takes care of retrieving the target objects from a {@link Traversal} returned by the backend using the backend {@link FjAdaptersFactory} 
	 * @param tr
	 * @param f
	 * @return A Set containing the targets of type <T> given the provided {@link Traversal} and {@link FjAdaptersFactory}
	 */
	public Set<T> targets(final Traversal<V,E> tr, final FjAdaptersFactory<V,E> f);
	
	/**
	 * @param path The {@link Path} returned by the backend
	 * @param f An {@link FjAdaptersFactory} from the backend
	 * @return The first element of the provided Path converted into a target object
	 */
	public T first(Path<V,E> path, final FjAdaptersFactory<V,E> f);
	
	/**
	 * @param path The {@link Path} returned by the backend
	 * @param f An {@link FjAdaptersFactory} from the backend
	 * @return The last element of the provided Path converted into a target object
	 */
	public T last(Path<V,E> path, final FjAdaptersFactory<V,E> f);
	
	

	
	public Optional<List<DataPointer>> metaTypes();

	/**
	 * Checks if a given {@link TraversalDefinition} configuration is compatible with this {@link TargetType}, 
	 * especially regarding the {@link TraversalPolicy}. The implementation is expected to perform the necessary corrections
	 * on the provided TraversalDefinition.
	 * @param definition The {@link TraversalDefinition} to test.
	 */
	public void checkTraversalDefinition(TraversalDefinition definition);

	
	
}
package org.pickcellslab.foundationj.processors;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic.Kind;

import org.pickcellslab.foundationj.annotations.Multiton;



public class MultitonProcessor extends AbstractProcessor {


	@Override
	public Set<String> getSupportedAnnotationTypes(){
		return Collections.singleton(Multiton.class.getName());
	};


	@SuppressWarnings("unchecked")
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		// return false if the round is over
		if (roundEnv.processingOver())      return false;



		// Find Multiton elements from the current compilation sources
		for (TypeElement type : (Collection<TypeElement>)roundEnv.getElementsAnnotatedWith(Multiton.class)) {

			// if the type is abstract skip
			if(type.getModifiers().contains(Modifier.ABSTRACT))
				continue;

			boolean found = false;
			
			
			// get the type of the MultiTonManager
			final Multiton mt = type.getAnnotation(Multiton.class);

			TypeMirror mgr = null;
			try {
				mt.manager();
			}
			catch(MirroredTypeException t){
				
				mgr = t.getTypeMirror();
				//JOptionPane.showMessageDialog(null, "MultitonProcessor: MirroredTypeException!");
				if(mgr == null) {
					//JOptionPane.showMessageDialog(null, "MultitonProcessor: mgr is null!");
					return false; // badly formulated converter option error will be thrown by compiler.
				}
			}
			
			

			// Get the method with name getOrCreate
			for(ExecutableElement method : ElementFilter.methodsIn(type.getEnclosedElements())){

				if(method.getSimpleName().toString().equals("getOrCreate")){
					//JOptionPane.showMessageDialog(null, "MultitonProcessor: getOrCreate found - "+ type.getSimpleName().toString());

					// Check signature (first argument must be the MultitonManager)
					final List<? extends VariableElement> params = method.getParameters();
					if(params.size()>0) {

						// get the type of the first parameter
						TypeMirror firstParam = params.get(0).asType();
											
						//JOptionPane.showMessageDialog(null, "MultitonProcessor: about to check subtype of "+mgr.getKind());
						
						if(processingEnv.getTypeUtils().isSameType(processingEnv.getTypeUtils().erasure(firstParam), mgr)) {
							//JOptionPane.showMessageDialog(null, "MultitonProcessor: subtype!");
							// check static
							if(method.getModifiers().contains(Modifier.STATIC)) {
								//JOptionPane.showMessageDialog(null, "MultitonProcessor: static!");
								found = true;
								break;
							}

						}
					}
				}				

			}

			if(!found) {
				//JOptionPane.showMessageDialog(null, "MultitonProcessor: Will write error");
				processingEnv.getMessager().printMessage(Kind.ERROR, "This class must implement the following method : static getOrCreate("+
				((DeclaredType)mgr).asElement().getSimpleName().toString()+",...)", type);
			}


			//-----------------------------------------//
			// Check that constructors are not public
			//-----------------------------------------//


			for (ExecutableElement c : ElementFilter.constructorsIn(type.getEnclosedElements())) {
				if(c.getModifiers().contains(Modifier.PUBLIC))
					processingEnv.getMessager().printMessage(Kind.ERROR, "A Multiton class cannot use public constructors",c);

			}
				

		}

		
		return false;
	}




}
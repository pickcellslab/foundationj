package org.pickcellslab.foundationj.optim.cspImpl;

/*-
 * #%L
 * foundationj-optim
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.graph.Edge;
import org.pickcellslab.foundationj.datamodel.graph.Node;
import org.pickcellslab.foundationj.optim.csp.Enumerator;
import org.pickcellslab.foundationj.optim.csp.OptimizationPartsFactory;
import org.pickcellslab.foundationj.optim.csp.Solver;
import org.pickcellslab.foundationj.optim.cspImpl.DefaultEnumerator.SpanPolicy;

public class DefaultEnumerationFactory<N extends Node<D>,D extends Edge<N>>
implements OptimizationPartsFactory<Enumerator<N,D>, N, D> {

	
	private final SpanPolicy policy;

	public DefaultEnumerationFactory(SpanPolicy policy){
		this.policy = policy;
	}
	
	@Override
	public Enumerator<N, D> getInstanceFor(Solver<N, D> solver) {
		return new DefaultEnumerator<>(policy, solver);
	}

}

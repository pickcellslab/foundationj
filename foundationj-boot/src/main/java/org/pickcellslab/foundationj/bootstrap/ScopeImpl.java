package org.pickcellslab.foundationj.bootstrap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.foundationj.scope.RuntimeScope;
import org.pickcellslab.foundationj.scope.ScopeEventListener;
import org.picocontainer.ComponentAdapter;
import org.picocontainer.behaviors.Stored;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class ScopeImpl implements RuntimeScope{


	private static final Logger log = LoggerFactory.getLogger(ScopeImpl.class);

	private final String scope, id;
	private ScopeImpl superScope;
	private final ScopePolicy policy;
	private List<ScopeImpl> children;

	//private Map<Class, Object> options = new HashMap<>();

	private FoundationJContainer container;
	// private MutablePicoContainer container;


	ScopeImpl(String scope, String id, ScopePolicy policy, List<Object> comps) {
		this(scope, id, null, policy, comps);
	}


	ScopeImpl(String scope, String id, ScopeImpl superScope, ScopePolicy policy, List<Object> comps) {
		this.scope = scope;
		this.id = id;
		this.superScope = superScope;
		this.policy = policy;

		if(superScope != null) {
			log.debug("Registering "+scope+" as child of "+superScope.name());
			superScope.addChild(this);
			//container = superScope.getContainer().makeChildContainer();
			container = new FoundationJContainer(this, superScope.getContainer());
		}
		else {
			//container = buildOrphanContainer();
			container = new FoundationJContainer(this);
		}		

		log.debug("-------------------- "+scope+" ----------------- ");

		for(Object comp : comps) {
			log.debug(comp.toString());
			container.addComponent(comp, comp);
		}
		log.debug("------------------------ END --------------------- ");

	}




	void start(ScopeEventListener visitor, Object... options) {


		if(!container.getLifecycleState().isStarted()) {
			log.debug(scope + "starting");
			// Ensure parents are active
			//if(superScope!=null)
			//	superScope.start(visitor, options);
			visitor.beforeStart(this);

			try {
				for(Object o : options) {
					container.addComponent(o.getClass(),o);
					log.debug(scope + " received "+o.getClass()+" instance as option");
					//this.options.put(o.getClass(),o);
				}
				container.start();// TODO throw exception
			}catch(IllegalStateException e) {
				log.debug(scope + " threw IllegalState Exception");
				e.printStackTrace();
			}
			log.debug(scope + "started");
			visitor.afterStart(this);
		}
	}






	/**
	 * Stop this scope and removes it from the active tree
	 * @param visitor Consumes ScopeImpl as they are being stopped
	 */
	void stopAndRemove(ScopeEventListener visitor) {		
		if(children!=null) {
			for(ScopeImpl child : new ArrayList<>(children))
				child.stopAndRemove(visitor);
		}
		terminate(visitor);
	}





	private void terminate(ScopeEventListener visitor) {		
		visitor.beforeStop(this);
		container.stop();
		if(superScope!=null)
			superScope.getContainer().removeChildContainer(container);
		visitor.afterStop(this);
		container=null;
		superScope = null;
		children = null;				
	}


	public <T> T getComponentInstance(Class<T> clazz) {
		return container.getComponent(clazz);
	}

	public void addComponentInstance(Object instance) {
		container.addComponent(instance);
	}



	public boolean created(Object instance) {

		ComponentAdapter<?> ca = container.getComponentAdapter(instance.getClass());
		if(ca==null)
			return false;
		else
			return ((Stored<?>)ca).getStoredObject() != null;
	}


	public boolean removeChild(RuntimeScope scope) {
		return children == null ? false : children.remove(scope);
	};



	public List<ScopeImpl> children(){
		return children == null ? Collections.emptyList() : children;
	}



	@Override
	public String name() {
		return scope;
	}



	@Override
	public String id() {
		return id;
	}


	@Override
	public ScopeImpl parent() {
		return superScope;
	}



	@Override
	public ScopePolicy policy() {
		return policy;
	}





	@Override
	public String toString() {
		return "Scope "+scope;
	}

	private FoundationJContainer getContainer() {
		return container;
	}

	private void addChild(ScopeImpl scope) {
		//NB: does not check for cycles.
		if(children == null)
			children = new ArrayList<>();
		if(scope!=null)
			children.add(scope);
	}


	@Override
	public int numberOfChildren(String name) {
		if(children==null)
			return 0;
		return (int) children.stream().filter(c->c.name().equals(name)).count();
	}



	<E> Stream<E> getAllInstancesFor(Class<ScopeEventListener> itfc) {
		if(container==null)	return Stream.empty();
		return (Stream)container.getComponents().stream().filter(c->itfc.isAssignableFrom(c.getClass()));		
	}





}
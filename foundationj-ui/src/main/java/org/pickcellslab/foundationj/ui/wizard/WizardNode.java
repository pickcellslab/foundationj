package org.pickcellslab.foundationj.ui.wizard;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.LongAdder;


/**
 * A {@link WizardNode} defines a node in a tree where the tree represents the path a user can
 * choose to achieve the completion of some creation process.
 * 
 * @see Wizard
 * 
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("serial")
public abstract class WizardNode extends ValidityTogglePanel{


	private static final LongAdder idGenerator = new LongAdder();
	private final long id;

	/**
	 * The list of possible steps following this node
	 */
	protected final List<WizardNode> childSteps;
	/**
	 * The previous step before this node
	 */
	protected final WizardNode parent;

	private Wizard<?> wizard;

	private List<ValidityListener> lstrs = new ArrayList<>();


	protected WizardNode(WizardNode parent){
		this.parent = parent;
		this.childSteps = new ArrayList<>();

		idGenerator.increment();
		id = idGenerator.longValue();
	}


	/**
	 * Creates a new Node in the wizard ui
	 * @param parent The step before this node, maybe null if this is the root
	 * @param childs The next possible steps after this node
	 */
	public WizardNode(WizardNode parent, List<WizardNode> childs){

		Objects.requireNonNull(childs);

		if(childs.size() == 0 && !WizardLeaf.class.isAssignableFrom(this.getClass()))
			throw new IllegalStateException("Only classes extending WizardLeaf can have 0 child steps");

		this.parent = parent;
		this.childSteps = childs;

		idGenerator.increment();
		id = idGenerator.longValue();
	}


	/**
	 * @return The next step in the wizard ui, will be null if this is a leaf
	 */
	protected abstract WizardNode getNextStep();


	/**
	 * @return The previous step in the wizard ui, will be null if this is a root
	 */
	public WizardNode getPreviousStep(){
		return parent;
	}


	/**
	 * @return the number of different outcomes this node can create
	 * in response to the user inputs
	 */
	public abstract int numberOfForks();


	/**
	 * @return {@coed true} if this is a leaf, {@false} otherwise
	 */
	public final boolean isLeaf(){
		return numberOfForks() == 0;
	}

	/**
	 * @return {@code true} if this is a root, {@false} otherwise
	 */
	public boolean isRoot(){
		return parent == null;
	}


	void setWizard(Wizard<?> wizard){
		this.wizard = wizard;
	}



	final long id(){
		return id;
	}



	@Override
	public final void addValidityListener(ValidityListener l){
		lstrs.add(l);
	}

	/**
	 * Notifies listener that the next step is either no longer valid or now valid
	 */
	protected final void fireNextValidityChanged(){
		System.out.println(getClass().getSimpleName()+" firing validity changed -> "+validity());
		if(wizard!=null)
			wizard.nextValidityChanged(this);
		for(ValidityListener l : lstrs)
			l.nextValidityChanged(this);
	}


}

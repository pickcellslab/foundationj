package org.pickcellslab.foundationj.dbm.db;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Optional;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.PathTraverser;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;


/**
 * Provides access to database entities
 * 
 * @author Guillaume Blin
 *
 * @param <V> The type of the database entities which correspond to {@link NodeItem}s.
 * @param <E> The type of the database entities which correspond to {@link Link}s.
 */
public interface DatabaseAccess<V,E> extends MarkedState, Regenerator<V>{
	
	
	/**
	 * Provides access to the database entities which correspond to {@link NodeItem}s of a given class.
	 * @param clazz The class of the {@link NodeItem}s which need to be accessed
	 * @return an {@link Iterator} to iterate through the database counterparts of the desired {@link NodeItem}s
	 */
	public Iterator<V> getNodes(Class<? extends NodeItem> clazz);
	
	/**
	 * Provides access to the database entity which correspond to a specific {@link NodeItem}s defined by its class and 
	 * property value.
	 * @param clazz The class of the {@link NodeItem}s which need to be accessed
	 * @param k The {@link AKey} defining the property to be matched.
	 * @param value The value of the property to be matched.
	 * @return an {@link Optional} containing the counterpart of the desired {@link NodeItem}
	 */
	public <T> Optional<V> getOne(Class<? extends NodeItem> clazz, AKey<T> k, T value );
	
	/**
	 * Provides access to the <b>read-only</b> database entity which correspond to a specific {@link NodeItem}s defined by its class and 
	 * property value.
	 * @param clazz The class of the {@link NodeItem}s which need to be accessed
	 * @param k The {@link AKey} defining the property to be matched.
	 * @param value The value of the property to be matched.
	 * @return an {@link Optional} containing the read-only counterpart of the desired {@link NodeItem}
	 */
	public <T> Optional<NodeItem> getOneReadOnly(Class<? extends NodeItem> clazz, AKey<T> k, T value );
	
	/**
	 * Provides access to the database entities which correspond to {@link NodeItem}s tagged with a given label.
	 * @param label The label tagging the {@link NodeItem}s which need to be accessed
	 * @return an {@link Iterator} to iterate through the database counterparts of the desired {@link NodeItem}s
	 */
	public Iterator<V> getNodes(String label);
	
	/**
	 * Provides access to the database entity which correspond to a specific {@link NodeItem}s defined by its label and 
	 * property value.
	 * @param label The label tagging the {@link NodeItem}s which need to be accessed
	 * @param k The {@link AKey} defining the property to be matched.
	 * @param value The value of the property to be matched.
	 * @return an {@link Optional} containing the counterpart of the desired {@link NodeItem}
	 */
	public <T> Optional<V> getOne(String label, AKey<T> k, T value );
	
	/**
	 * Provides access to the <b>read-only</b> database entity which correspond to a specific {@link NodeItem}s defined by its label and 
	 * property value.
	 * @param label The label of the {@link NodeItem}s which need to be accessed
	 * @param k The {@link AKey} defining the property to be matched.
	 * @param value The value of the property to be matched.
	 * @return an {@link Optional} containing the read-only counterpart of the desired {@link NodeItem}
	 */
	public <T> Optional<NodeItem> getOneReadOnly(String label, AKey<T> k, T value );
	
	/**
	 * Provides access to the database entities which correspond to {@link Link}s of the given type.
	 * @param type The type of the {@link Link}s which need to be accessed
	 * @return An {@link Iterator} to iterate through the database counterparts of the desired {@link Link}s
	 */
	public Iterator<E> getLinks(String type);
	
	/**
	 * Provides access to the database entities which correspond to {@link Link}s matching a given type and
	 * with source and target {@link NodeItem}s matching the given srcType and tgType respectively.
	 * @param type The type of the {@link Link}s which need to be accessed
	 * @param srcType The {@link DataItem#declaredType} of the source {@link NodeItem}
	 * @param tgType The {@link DataItem#declaredType} of the tager {@link NodeItem}
	 * @return An {@link Iterator} to iterate through the database counterparts of the desired {@link Link}s
	 */
	public Iterator<E> getLinks(String type, String srcType, String tgType);
		
	/**
	 * Provides access to the database entity which correspond to a specific {@link Link}s matching a given link type
	 * and property value.
	 * @param linkType The type of the {@link Link}s which need to be accessed
	 * @param k The {@link AKey} defining the property to be matched.
	 * @param value The value of the property to be matched.
	 * @return an {@link Optional} containing the counterpart of the desired {@link Link}
	 */
	public <T> Optional<E> getOneLink(String linkType, AKey<T> k, T value );
	
	/**
	 * Provides access to the <b>read-only</b> database entity which correspond to a specific {@link Link}s matching a given link type
	 * and property value.
	 * @param linkType The type of the {@link Link}s which need to be accessed
	 * @param k The {@link AKey} defining the property to be matched.
	 * @param value The value of the property to be matched.
	 * @return an {@link Optional} containing the read-only counterpart of the desired {@link Link}
	 */
	public <T> Optional<Link> getOneLinkReadOnly(String linkType, AKey<T> k, T value );
	
		
	/**
	 * Creates a {@link PathTraverser} configured with the given {@link TraversalDefinition}
	 * @param td The {@link TraversalDefinition} to configure the {@link PathTraverser}
	 * @return A {@link PathTraverser} configured with the given {@link TraversalDefinition}
	 */
	public PathTraverser<V,E> traverser(TraversalDefinition td);
	
	
	/**
	 * @return a new {@link FjAdaptersFactory} instance.
	 */
	public FjAdaptersFactory<V,E> createFactory();
	
	
	/**
	 * Defines the beginning of a new database transaction. 
	 * @return a {@link MarkedState}
	 */
	public MarkedState newThread();

	
	
	
	
}

package org.pickcellslab.foundationj.dbm.impl;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.functions.ExplicitFunction;
import org.pickcellslab.foundationj.datamodel.functions.KeySelector;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ListAction;


class ListMakerBuilder<T extends WritableDataItem, E> extends ActionBuilderImpl<T, List<E>>{

	
	private final ExplicitFunction<? super T,E> f;
	private final TargetType<?, ?, T> target;
	
	ListMakerBuilder(TargetType<?,?,T> target, AKey<E> k){
		this.target = target; 
		f = new KeySelector<>(k,null);
	}
	
	ListMakerBuilder(TargetType<?,?,T> target, ExplicitFunction<? super T,E> f){
		this.target = target; 
		Objects.requireNonNull(f,"The data function is null");
		this.f = f;
	}
	
	
	@Override
	protected TargetType<?, ?, T> getType() {
		return target;
	}

	@Override
	protected Action<T, List<E>> getAction() {
		return new ListAction<>(f);
	}

}

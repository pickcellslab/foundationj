package org.pickcellslab.foundationj.dbm.queries;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;


public interface MetaBox extends Query<Void> {

	public StepTraverser<NodeItem,Link> get();
	
	@Override
	public String description();
	
	public Void run() throws DataAccessException;

}

package org.pickcellslab.foundationj.dbm.db;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dbm.events.DataUpdateListener;
import org.pickcellslab.foundationj.dbm.events.MetaListenerNotifier;
import org.pickcellslab.foundationj.dbm.queries.MetaBox;
import org.pickcellslab.foundationj.dbm.queries.StorageBox;


/**
 * 
 * A {@link DataStoreManager} creates objects which allow the framework to communicate with the database back-end. 
 * Those include new {@link DatabaseAccess} instances as well as {@link QInterpreter}s. The {@link DataStoreManager}
 * is also responsible for registering listeners and to rpovide methods to either delete a database or create backups.
 *  
 * @author Guillaume Blin
 *
 * @param <V> The type of the database entities which correspond to {@link NodeItem}s.
 * @param <E> The type of the database entities which correspond to {@link Link}s.
 */
public interface DataStoreManager<V,E> {

		
	/**
	 * @return a new {@link DatabaseAccess} instance
	 */
	public DatabaseAccess<V,E> newAccess();
	
		
	public void addUpdateListener(DataUpdateListener lstr);

	public boolean removeUpdateListener(DataUpdateListener lstr);	
	

	/**
	 * Allows to create a copy of the database into another location
	 * 
	 * @param path
	 */
	public void copyDataBase(String path) throws DataAccessException;

	/**
	 * Deletes the database with name dbName
	 * 
	 */
	public void deleteDataBase();

	/**
	 * This call should create a snapshot of the database to which it will be
	 * possible to come back at a later point in time.
	 */
	public void createSavePoint(String name);

	/**
	 * Reverts the changes in the database to the save point with the specified
	 * name
	 */
	public void revertToSavePoint(String name);

	
	public QInterpreter<MetaBox> createMetaStorer(MetaListenerNotifier notifier);

	public QInterpreter<StorageBox> createDataStorer(MetaListenerNotifier notifier);


	public void close();



	public boolean isClosed();


	



}

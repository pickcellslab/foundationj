package org.pickcellslab.foundationj.datamodel.threads;

/*-
 * #%L
 * foundationj-datamodel
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.concurrent.RejectedExecutionException;


public class ModeratedThread extends Thread {

	
	private final Runnable runnable;
	private boolean pleaseRemap;

	public ModeratedThread(Runnable runnable){
		this.runnable = runnable;
	}
	
	
	
	@Override
	public void run()
	{
		while ( !isInterrupted() ){
			final boolean b;
			synchronized ( this ){
				b = pleaseRemap;
				pleaseRemap = false;
			}
			if ( b )
				try{
					runnable.run();
				}
				catch ( final RejectedExecutionException e ){
					e.printStackTrace();
				}
			synchronized ( this ){
				try{
					if ( !pleaseRemap )
						wait();
				}
				catch ( final InterruptedException e ){
					break;
				}
			}
		}
	}

	
	public void requestRerun(){
		synchronized ( this ){
			pleaseRemap = true;
			notify();
		}
	}
	
}
package org.pickcellslab.foundationj.mapping.extra;

import org.pickcellslab.foundationj.annotations.Mapping;
import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;

/**
 * An object responsible for storing a {@link Mapping} object into a String format.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of object this {@link CustomNonDataConverter} supports
 */
public interface CustomNonDataConverter<T> {

	/**
	 * @param object The object to represent as a String
	 * @return A String representation of the object which can be used with {@link #fromXML(String)} to recreate the java object instance
	 * @throws NonDataMappingException if the structure of the object is not supported
	 */
	public String toString(T object) throws NonDataMappingException;

	/**
	 * @param xml A String (obtained with {@link #toXML(Object)}) representing a java object to recreate
	 * @return The java object represented by the given String
	 * @throws NonDataMappingException if the structure of the object is not supported
	 */
	public Object fromString(String string) throws NonDataMappingException;
	
	
	
	
	
}
